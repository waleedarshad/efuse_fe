(function() {
  ArticleEditor.add("plugin", "reorder", {
    defaults: {
      icon:
        '<svg height="16" viewBox="0 0 16 16" width="16" xmlns="http://www.w3.org/2000/svg"><path d="m3 5h10c.5522847 0 1 .44771525 1 1s-.4477153 1-1 1h-10c-.55228475 0-1-.44771525-1-1s.44771525-1 1-1zm0 4h10c.5522847 0 1 .44771525 1 1 0 .5522847-.4477153 1-1 1h-10c-.55228475 0-1-.4477153-1-1 0-.55228475.44771525-1 1-1z" fill-rule="evenodd"/></svg>'
    },
    subscribe: {
      "block.set": function() {
        this._observe();
      }
    },
    init() {
      this.utils = this.app.create("utils");
    },
    start() {
      this.app.control.add("reorder", {
        icon: this.opts.reorder.icon,
        position: "first",
        blocks: "first-level",
        title: "## command.sort ##"
      });
    },
    stop() {
      this.$btn.off(`.arx-reorder-${this.uuid}`);
      this.app.$win.off(`.arx-reorder-${this.uuid}`);
      // this.app.editor.$editor.removeAttr('data-sortable');
    },

    // private
    _observe() {
      this.$btn = this.app.control.get("reorder");
      this.$btn.addClass("arx-handle");

      this._sortable();
    },
    _sortable() {
      this.$scrollTarget = this.utils.isScrollTarget() ? this.utils.getScrollTarget() : this.app.$win;
      this.tolerance = this.$btn.width();
      this.$clickItem = null;
      this.$dragItem = null;
      this.click = {};
      this.dragging = false;

      // this.app.editor.$editor.attr('data-sortable', true);
      this.$btn.on(`mousedown.arx-reorder-${this.uuid} touchstart.arx-reorder-${this.uuid}`, this._press.bind(this));
      this.app.$win.on(`mouseup.arx-reorder-${this.uuid} touchend.arx-reorder-${this.uuid}`, this._release.bind(this));
      this.app.$win.on(`mousemove.arx-reorder-${this.uuid} touchmove.arx-reorder-${this.uuid}`, this._move.bind(this));
    },
    _getPoint(e, client) {
      if (client) {
        return { x: e.clientX + this.tolerance, y: e.clientY };
      }

      return { x: e.pageX + this.tolerance, y: e.pageY };
    },
    _isOver($item, x, y) {
      const box = $item.offset();
      const width = $item.width();
      const height = $item.height();
      const isx = x > box.left && x < box.left + width;
      const isy = y > box.top && y < box.top + height;

      return isx && isy;
    },
    _swapItems(item1, item2) {
      item1 = item1.get();
      const parent1 = item1.parentNode;
      const parent2 = item2.parentNode;

      if (parent1 !== parent2) {
        parent2.insertBefore(item1, item2);
      } else {
        const temp = document.createElement("div");
        parent1.insertBefore(temp, item1);
        parent2.insertBefore(item1, item2);
        parent1.insertBefore(item2, temp);
        parent1.removeChild(temp);
      }
    },
    _moveItem($item, x, y) {
      $item.css("transform", `translateY(${y}px)`);
    },
    _makeDragItem(item) {
      this._trashDragItem();

      const $item = this.dom(item);
      this.$clickItem = $item;
      this.$clickItem.addClass("arx-active");

      const $cloned = $item.clone();
      $cloned.removeClass("arx-active arx-block-focus");
      this.$dragItem = this.dom("<div>");
      this.$dragItem.append($cloned);
      this.$dragItem.addClass("arx-dragging");
      this.$dragItem.css({
        opacity: 0.95,
        position: "absolute",
        "z-index": 999,
        left: `${item.offsetLeft || 0}px`,
        top: `${item.offsetTop || 0}px`,
        width: `${item.offsetWidth || 0}px`
      });

      this.app.editor.$editor.append(this.$dragItem);
    },
    _trashDragItem() {
      if (this.$dragItem && this.$clickItem) {
        this.$clickItem.removeClass("arx-active arx-block-focus");
        this.$clickItem = null;

        this.$dragItem.remove();
        this.$dragItem = null;
      }
    },
    _press(e) {
      const $target = this.dom(e.target).closest(".arx-button");
      if (e && e.target && $target.hasClass("arx-handle")) {
        e.preventDefault();

        const instance = this.app.block.get();
        const item = instance.$block.get();
        this.app.block.unset();

        this.dragging = true;
        this.click = this._getPoint(e);
        this._makeDragItem(item, e.target);
        this.dragTop = this.$dragItem.offset().top;
        this._move(e);
      }
    },
    _release(e) {
      this.dragging = false;
      this.app.observer.trigger = true;
      this._trashDragItem();
    },
    _scroll(step) {
      const $target = this.utils.isScrollTarget() ? this.$scrollTarget : this.app.$win;
      const scrollY = $target.scrollTop();
      $target.scrollTop(scrollY + step);
    },
    _getBox($container, position) {
      const containerOffset =
        this.utils.isScrollTarget() && position !== false ? $container.position() : $container.offset();
      const start = containerOffset.top;
      const end = start + $container.height();

      return {
        start,
        end
      };
    },
    _move(e) {
      if (!this.$dragItem && !this.dragging) {
        return;
      }

      e.preventDefault();
      this.app.observer.trigger = false;

      const point = this._getPoint(e);
      const startPoint = {
        x: point.x - this.click.x,
        y: point.y - this.click.y
      };

      if (startPoint.x === 0 || startPoint.y === 0) {
        return;
      }

      // move
      this._moveItem(this.$dragItem, startPoint.x, startPoint.y);

      // autoscroll
      if (!this.utils.isScrollTarget()) {
        const $box = this.app.editor.$editor;
        const $target = this.app.$win;
        const bound = this._getBox($box);
        const scrollTop = $target.scrollTop();
        const startBox = this.app.editor.$editor.offset().top;
        const endBox = e.clientY + $target.scrollTop();
        const x = $target.height() - 50;
        const y = $target.scrollTop() + 50;
        var direction = "down";
        if (this.dragTop - this.$dragItem.offset().top < 50) {
          // down
          if (e.clientY > $target.height() - 50 && endBox < bound.end) {
            this._scroll(10);
          }
        } else if (this.dragTop - this.$dragItem.offset().top > 50) {
          // up
          direction = "up";
          if (e.clientY < 50 && e.clientY + scrollTop > startBox) {
            this._scroll(-10);
          }
        }
      }

      // place
      const len = this.app.editor.$editor.get().children.length;
      if (this._isOver(this.app.editor.$editor, point.x, point.y) && len === 0) {
        const func = direction === "up" ? "prepend" : "append";
        this.app.editor.$editor[func](this.$clickItem);
        return;
      }

      for (let b = 0; b < len; b++) {
        const subItem = this.app.editor.$editor.get().children[b];
        if (subItem.tagName === "SCRIPT" || subItem === this.$clickItem.get() || subItem === this.$dragItem.get()) {
          continue;
        }

        if (this._isOver(this.dom(subItem), point.x, point.y)) {
          // this.hoverItem = subItem;
          this._swapItems(this.$clickItem, subItem);
        }
      }
    }
  });
})(ArticleEditor);
