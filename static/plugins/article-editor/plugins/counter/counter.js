(function() {
  ArticleEditor.add("plugin", "counter", {
    translations: {
      en: {
        counter: {
          words: "words",
          chars: "chars"
        }
      }
    },
    subscribe: {
      "editor.content.change": function() {
        this._count();
      }
    },
    init() {
      // services
      this.utils = this.app.create("utils");
    },
    start() {
      this._count();
    },

    // private
    _count() {
      let words = 0;
      let characters = 0;
      let spaces = 0;
      let html = this.app.$element.val();

      html = this._clean(html);
      if (html !== "") {
        const arrWords = html.split(/\s+/);
        const arrSpaces = html.match(/\s/g);

        words = arrWords ? arrWords.length : 0;
        spaces = arrSpaces ? arrSpaces.length : 0;

        characters = html.replace(/\s+/g, " ").length;
      }

      const data = { words, characters, spaces };

      // callback
      this.app.broadcast("counter.count", data);

      // statusbar
      this.app.statusbar.add("words", `${this.lang.get("counter.words")}: ${data.words}`);
      this.app.statusbar.add("chars", `${this.lang.get("counter.chars")}: ${data.characters}`);
    },
    _clean(html) {
      html = html.replace(/<\/(.*?)>/gi, " ");
      html = html.replace(/<(.*?)>/gi, "");
      html = html.replace(/\t/gi, "");
      html = html.replace(/\n/gi, " ");
      html = html.replace(/\r/gi, " ");
      html = html.replace(/&nbsp;/g, "1");
      html = html.trim();
      html = this.utils.removeInvisibleChars(html);

      return html;
    }
  });
})(ArticleEditor);
