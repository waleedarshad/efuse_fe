(function() {
  ArticleEditor.add("plugin", "style", {
    translations: {
      en: {
        style: {
          style: "Style",
          "remove-style": "Remove Style"
        }
      }
    },
    defaults: {
      icon:
        '<svg height="16" viewBox="0 0 16 16" width="16" xmlns="http://www.w3.org/2000/svg"><path d="m.23081081 13.7264324c1.69475676-5.08427024 6.13010811-13.55729726 13.55729729-13.55729726-3.4818378 2.79356756-5.0854054 9.32059459-7.62583783 9.32059459h-2.54232432l-2.54194595 4.23783787h-.84718919z" transform="translate(1 1)"/></svg>',
      styles: {
        paragraph: {
          lead: {
            title: '<span style="font-size: 24px; color: #444;">Lead</span>',
            command: "style.toggle",
            params: { classname: "st-lead" }
          },
          note: {
            title: '<span style="background-color: #fffcba; color: #111; display: block;">Note</span>',
            command: "style.toggle",
            params: { classname: "st-note" }
          },
          accent: {
            title: '<span style="font-weight: bold; font-size: 20px;">Accent</span>',
            command: "style.toggle",
            params: { classname: "st-accent" }
          },
          remove: {
            title: "## style.remove-style ##",
            topdivider: true,
            command: "style.remove"
          }
        },
        embed: {
          frame: {
            title: "Frame",
            command: "style.toggle",
            params: { classname: "st-embed-frame" }
          },
          raised: {
            title: "Raised",
            command: "style.toggle",
            params: { classname: "st-embed-raised" }
          },
          remove: {
            title: "## style.remove-style ##",
            topdivider: true,
            command: "style.remove"
          }
        },
        line: {
          "black-extra-height": {
            title: '<span style="display: block; height: 4px; background: #000;"></span>',
            command: "style.toggle",
            params: { classname: "st-line-black-extra-height" }
          },
          "gray-dashed": {
            title: '<span style="display: block; border-top: 2px dashed #ccc;"></span>',
            command: "style.toggle",
            params: { classname: "st-line-gray-dashed" }
          },
          "blue-line": {
            title: '<span style="display: block; height: 2px; background: #458fff;"></span>',
            command: "style.toggle",
            params: { classname: "st-line-blue" }
          },
          remove: {
            title: "## style.remove-style ##",
            topdivider: true,
            command: "style.remove"
          }
        }
      }
    },
    start() {
      const blocks = [];
      for (const key in this.opts.style.styles) {
        blocks.push(key);
      }

      this.app.toolbar.add("style", {
        title: "## style.style ##",
        icon: this.opts.style.icon,
        blocks,
        popup: {
          name: "style",
          list: true,
          style: {
            type: "list",
            builder: "style.popup"
          }
        }
      });
    },
    popup() {
      const instance = this.app.block.get();
      const type = instance.getType();
      const classes = this._getClasses(type);
      const name = instance.getClass(classes);

      // get buttons & set the active item
      if (typeof this.opts.style.styles[type] !== "undefined") {
        const buttons = this.opts.style.styles[type];
        for (const key in buttons) {
          buttons[key].active = buttons[key].params && buttons[key].params.classname === name;
        }

        if (typeof buttons.remove !== "undefined") {
          if (name === false) {
            buttons.remove.hidden = true;
          } else {
            buttons.remove.hidden = false;
          }
        }

        return buttons;
      }
    },
    toggle(args) {
      this.app.popup.close();

      // instance
      const instance = this.app.block.get();
      const type = instance.getType();

      // remove
      this._removeStyles(instance, type);

      // set
      instance.$block.addClass(args.params.classname);

      // rebuild
      this.app.toolbar.build();
      this.app.control.rebuild();
    },
    remove(args) {
      this.app.popup.close();

      // instance
      const instance = this.app.block.get();
      const type = instance.getType();

      // remove
      this._removeStyles(instance, type);

      // rebuild
      this.app.toolbar.build();
      this.app.control.rebuild();
    },

    // private
    _getClasses(type) {
      const styles = this.opts.style.styles[type];
      const classes = [];
      for (const key in styles) {
        if (styles[key].params) {
          classes.push(styles[key].params.classname);
        }
      }

      return classes;
    },
    _removeStyles(instance, type) {
      const classes = this._getClasses(type);
      instance.$block.removeClass(classes.join(" "));
    }
  });
})(ArticleEditor);
