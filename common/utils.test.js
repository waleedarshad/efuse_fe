import mockGetConfig from "next/config";
import { withCdn } from "./utils";

describe("utils", () => {
  describe("withCdn", () => {
    it("returns public cdn when forcePublicCdn is set to true", () => {
      mockGetConfig.mockReturnValue({
        publicRuntimeConfig: {
          environment: "development",
          cdnBaseUrl: "https://cdn.lucho.com"
        }
      });
      expect(withCdn("/static/picture.png", true)).toEqual("https://cdn.efuse.gg/static/picture.png");
    });

    it("returns public cdn when there is no publicRuntimeConfig", () => {
      mockGetConfig.mockReturnValue({});
      expect(withCdn("/static/picture.png", false)).toEqual("https://cdn.efuse.gg/static/picture.png");
    });

    it("returns public cdn when there is no environment", () => {
      mockGetConfig.mockReturnValue({
        publicRuntimeConfig: {
          cdnBaseUrl: "hey-environment-is-missing"
        }
      });
      expect(withCdn("/static/picture.png", false)).toEqual("https://cdn.efuse.gg/static/picture.png");
    });

    it("returns public cdn when cdn base url is missing", () => {
      mockGetConfig.mockReturnValue({
        publicRuntimeConfig: {
          environment: "hey-cdn-base-url-is-missing"
        }
      });
      expect(withCdn("/static/picture.png", false)).toEqual("https://cdn.efuse.gg/static/picture.png");
    });

    it("returns path with cdn base url specified when running in production", () => {
      mockGetConfig.mockReturnValue({
        publicRuntimeConfig: {
          environment: "production",
          cdnBaseUrl: "https://cdn.lucho.com"
        }
      });
      expect(withCdn("/static/picture.png", false)).toEqual("https://cdn.lucho.com/static/picture.png");
    });

    it("returns local path when running in development", () => {
      mockGetConfig.mockReturnValue({
        publicRuntimeConfig: {
          environment: "development",
          cdnBaseUrl: "https://cdn.lucho.com"
        }
      });
      expect(withCdn("/static/picture.png", false)).toEqual("/static/picture.png");
    });
  });
});
