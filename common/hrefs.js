const HREFS = {
  OPPORTUNITIES: "/opportunities",
  OPPORTUNITIES_ALL: "/opportunities/all",
  OPPORTUNITIES_APPLIED: "/opportunities/applied",
  OPPORTUNITIES_OWNED: "/opportunities/owned",
  DISCOVER: "/discover",
  ORGANIZATIONS: "/organizations",
  ORGANIZATIONS_NEW: "/organizations/create",
  LEARNING: "/learning",
  NEWS: "/news",
  ERENA: "/erena",
  PIPELINE: "/pipeline",
  ESPORTS: "/esports"
};

export default HREFS;
