import getConfig from "next/config";

export const withCdn = (path, forcePublicCdn = false) => {
  const config = getConfig();
  const environment = config?.publicRuntimeConfig?.environment;
  const cdnBaseUrl = config?.publicRuntimeConfig?.cdnBaseUrl;

  if (forcePublicCdn || !environment || !cdnBaseUrl) {
    return new URL(path, "https://cdn.efuse.gg/").toString();
  }

  if (environment === "production") {
    return new URL(path, cdnBaseUrl).toString();
  }

  return path;
};

export const isIosMobile = () => /(iPad|iPhone|iPod)/g.test(navigator.userAgent);
