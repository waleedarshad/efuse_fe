/* eslint-disable import/prefer-default-export */
const EXTERNAL_AUTH_SERVICES = {
  STATESPACE: "statespace",
  RIOT: "riot"
};

export { EXTERNAL_AUTH_SERVICES };
