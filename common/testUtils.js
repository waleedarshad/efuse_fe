import { mount } from "enzyme";
import React from "react";
import { Provider } from "react-redux";
import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";

export const mountWithStore = (Component, state = {}) => {
  const mockStore = getMockStore(state);
  const subject = mount(<Provider store={mockStore}>{Component}</Provider>);

  return {
    mockStore,
    subject
  };
};

export const getMockStore = (state = {}) => configureMockStore([thunk])(state);
