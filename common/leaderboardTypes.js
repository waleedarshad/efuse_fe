const LEADERBOARD_TYPE = {
  STREAKS: "streaks",
  VIEWS: "views",
  ENGAGEMENTS: "engagements",
  HYPES: "hypes",
  ALL: "all"
};

export default LEADERBOARD_TYPE;
