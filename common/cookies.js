const COOKIES = {
  DEMO_STATE: "demoState",
  SHOW_LATEST_FEATURES: "1",
  SHOW_PUBLIC_SITE: "0",
  PIPELINE_RECRUITING_AS: "pipelineRecruitingAs",
  PIPELINE_RECRUITING_ID: "pipelineRecruitingId"
};

export default COOKIES;
