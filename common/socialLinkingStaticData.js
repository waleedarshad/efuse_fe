import { faTwitter, faYoutube, faTwitch } from "@fortawesome/free-brands-svg-icons";
import SOCIAL_LINKING from "../store/actions/socialLinkingActions/types";

const SOCIAL_LINKING_STATIC_DATA = {
  TWITTER: {
    socialType: "twitter",
    externalAuth: "twitter",
    icon: faTwitter,
    socialTitle: "twitter",
    linkText: "Twitter",
    supporterTitle: "Followers",
    buttonText: "View",
    accountNameSymbol: "@",
    actionTypes: {
      linkSuccess: SOCIAL_LINKING.TWITTER.LINKED.SUCCESS,
      linkFailure: SOCIAL_LINKING.TWITTER.LINKED.FAILURE,
      accountInfoSuccess: SOCIAL_LINKING.TWITTER.ACCOUNT_INFO.SUCCESS,
      accountInfoFailure: SOCIAL_LINKING.TWITTER.ACCOUNT_INFO.FAILURE,
      unlinkSuccess: SOCIAL_LINKING.TWITTER.UNLINK.SUCCESS,
      unlinkFailure: SOCIAL_LINKING.TWITTER.UNLINK.FAILURE
    },
    socketTitle: "TWITTER_EXTERNAL_ACCOUNT_LINKING"
  },
  YOUTUBE: {
    socialType: "youtube",
    externalAuth: "google",
    icon: faYoutube,
    socialTitle: "YouTube",
    linkText: "YouTube",
    supporterTitle: "Subscribers",
    buttonText: "View",
    actionTypes: {
      linkSuccess: SOCIAL_LINKING.YOUTUBE.LINKED.SUCCESS,
      linkFailure: SOCIAL_LINKING.YOUTUBE.LINKED.FAILURE,
      accountInfoSuccess: SOCIAL_LINKING.YOUTUBE.ACCOUNT_INFO.SUCCESS,
      accountInfoFailure: SOCIAL_LINKING.YOUTUBE.ACCOUNT_INFO.FAILURE,
      unlinkSuccess: SOCIAL_LINKING.YOUTUBE.UNLINK.SUCCESS,
      unlinkFailure: SOCIAL_LINKING.YOUTUBE.UNLINK.FAILURE
    },
    socketTitle: "GOOGLE_EXTERNAL_ACCOUNT_LINKING"
  },
  TWITCH: {
    socialType: "twitch",
    externalAuth: "twitch",
    icon: faTwitch,
    socialTitle: "twitch",
    linkText: "Twitch",
    supporterTitle: "Followers",
    buttonText: "View",
    accountNameSymbol: "@",
    actionTypes: {
      linkSuccess: SOCIAL_LINKING.TWITCH.LINKED.SUCCESS,
      linkFailure: SOCIAL_LINKING.TWITCH.LINKED.FAILURE,
      accountInfoSuccess: SOCIAL_LINKING.TWITCH.ACCOUNT_INFO.SUCCESS,
      accountInfoFailure: SOCIAL_LINKING.TWITCH.ACCOUNT_INFO.FAILURE,
      unlinkSuccess: SOCIAL_LINKING.TWITCH.UNLINK.SUCCESS,
      unlinkFailure: SOCIAL_LINKING.TWITCH.UNLINK.FAILURE
    },
    socketTitle: "TWITCH_EXTERNAL_ACCOUNT_LINKING"
  }
};

export default SOCIAL_LINKING_STATIC_DATA;
