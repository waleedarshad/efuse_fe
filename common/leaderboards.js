import { faEye, faHandshakeAlt, faListUl } from "@fortawesome/pro-solid-svg-icons";
import { faBolt } from "@fortawesome/pro-duotone-svg-icons";
import Style from "../components/General/PageTitle/PageTitle.module.scss";

const LEADERBOARDS = {
  views: {
    iconType: faEye,
    iconColor: Style.blackIcon,
    pageHeaderText: "Views Leaderboard",
    subheaderText: "Views",
    singleText: "View"
  },
  streaks: {
    iconType: faBolt,
    iconColor: Style.purpleBolt,
    pageHeaderText: "Streak Leaderboard",
    subheaderText: "Streaks",
    singleText: "Streak"
  },
  engagements: {
    iconType: faHandshakeAlt,
    iconColor: Style.blackIcon,
    pageHeaderText: "Engagements Leaderboard",
    subheaderText: "Engagements",
    singleText: "Engagement"
  },
  all: {
    iconType: faListUl,
    iconColor: Style.blueList,
    pageHeaderText: "Leaderboards"
  }
};

export default LEADERBOARDS;
