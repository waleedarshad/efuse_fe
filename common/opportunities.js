export const OPPORTUNITY_TYPES = {
  JOB: "Job",
  EVENT: "Event",
  TEAM_OPENING: "Team Opening",
  SCHOLARSHIP: "Scholarship"
};

export const SUBTYPES = {
  JOB: {
    VOLUNTEER: "Volunteer",
    INTERNSHIP: "Internship",
    PART_TIME: "Part-Time",
    FULL_TIME: "Full-Time"
  },
  EVENT: {
    CONFERENCE: "Conference",
    MEETING: "Meeting",
    TOURNAMENT: "Tournament",
    OTHER_EVENT: "Other Event"
  },
  TEAM_OPENING: {
    CASUAL: "Casual",
    AMATEUR: "Amateur",
    CLUB: "Club",
    COLLEGIATE: "Collegiate",
    SEMI_PRO: "Semi-Pro",
    PRO: "Pro"
  },
  SCHOLARSHIP: {
    ATHLETIC: "Athletic",
    SCHOLASTIC: "Scholastic",
    OTHER_SCHOLARSHIP: "Other Scholarship"
  }
};

export const ALL_SUBTYPES = Object.values(SUBTYPES)
  .map(subTypeObj => Object.values(subTypeObj).map(subTypeText => subTypeText))
  .flat();

export const OPPORTUNITIES_HREFS = {
  ALL: "/opportunities",
  BROWSE: "/opportunities/browse",
  APPLIED: "/opportunities/applied",
  OWNED: "/opportunities/owned"
};

export const getSubtypes = opportunityType => {
  const opportunityTypeKey = Object.keys(OPPORTUNITY_TYPES).find(it => OPPORTUNITY_TYPES[it] === opportunityType);
  return opportunityTypeKey ? Object.values(SUBTYPES[opportunityTypeKey]) : [];
};
