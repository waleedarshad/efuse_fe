import {
  getCustomPath,
  getFullOpportunityUrl,
  getOpportunitiesParamsTail,
  getOrganizationsParamsTail,
  getOrganizationUrl
} from "./UrlHelper";

describe("UrlHelper", () => {
  describe("getFullOpportunityUrl", () => {
    it("returns correct url for promoted opportunity", () => {
      const opportunity = {
        promoted_url: "promoted_url",
        shortName: "my-shortname",
        id: "1234567"
      };
      const expected = "promoted_url";
      const actual = getFullOpportunityUrl(opportunity);

      expect(actual).toEqual(expected);
    });

    it("returns correct url for non-promoted opportunity with shortname", () => {
      const opportunity = {
        shortName: "my-shortname",
        id: "1234567"
      };
      const expected = "/o/my-shortname";
      const actual = getFullOpportunityUrl(opportunity);

      expect(actual).toEqual(expected);
    });

    it("returns correct url for non-promoted opportunity without shortname", () => {
      const opportunity = {
        _id: "1234567"
      };
      const expected = "/opportunities/show?id=1234567";
      const actual = getFullOpportunityUrl(opportunity);

      expect(actual).toEqual(expected);
    });
  });

  describe("getOrganizationUrl", () => {
    it("returns the correct url for a promoted organization", () => {
      const organization = {
        _id: "1234567",
        shortName: "sample-shortname",
        promoted_url: "my-promoted-url"
      };
      const expected = "my-promoted-url";
      const actual = getOrganizationUrl(organization);

      expect(actual).toEqual(expected);
    });

    it("returns the correct url for organization with shortName", () => {
      const organization = {
        id: "1234567",
        shortName: "sample-shortname"
      };
      const expected = "/org/sample-shortname";
      const actual = getOrganizationUrl(organization);

      expect(actual).toEqual(expected);
    });

    it("returns the correct url for organizations without a shortname", () => {
      const organization = {
        _id: "1234567"
      };
      const expected = "/organizations/show?id=1234567";
      const actual = getOrganizationUrl(organization);

      expect(actual).toEqual(expected);
    });
  });

  describe("getCustomPath", () => {
    it("returns correct private path if user is logged in", () => {
      const user = {
        id: "1234"
      };
      const expected = "/opportunities";
      const actual = getCustomPath(user, "opportunities");

      expect(actual).toEqual(expected);
    });

    it("returns the correct public path if user is not logged in", () => {
      const user = {};
      const expected = "/public/organizations";
      const actual = getCustomPath(user, "organizations");

      expect(actual).toEqual(expected);
    });
  });

  describe("getOpportunitiesParamsTail", () => {
    it("returns the correct params tail if profile exists and user is logged in", () => {
      const router = {
        pathname: "does/not/matter"
      };
      const profile = true;
      const profileId = "1234";
      const currentUser = {
        id: "123456"
      };
      const expected = ["1234", "/opportunities"];
      const actual = getOpportunitiesParamsTail(router, profile, profileId, currentUser);

      expect(actual).toEqual(expected);
    });

    it("returns the correct params tail if profile exists and user is not logged in", () => {
      const router = {
        pathname: "does/not/matter"
      };
      const profile = true;
      const profileId = "1234";
      const currentUser = {};
      const expected = ["1234", "/public/opportunities"];
      const actual = getOpportunitiesParamsTail(router, profile, profileId, currentUser);

      expect(actual).toEqual(expected);
    });

    it("returns the correct params tail if pathname includes all and user not logged in", () => {
      const router = {
        pathname: "/opportunities/all"
      };
      const currentUser = {};
      const expected = ["", "/public/opportunities"];
      const actual = getOpportunitiesParamsTail(router, false, null, currentUser);

      expect(actual).toEqual(expected);
    });

    it("returns the correct params tail if pathname includes all and user is logged in", () => {
      const router = {
        pathname: "/opportunities/all"
      };
      const currentUser = {
        id: "1234"
      };
      const expected = ["", "/opportunities"];
      const actual = getOpportunitiesParamsTail(router, false, undefined, currentUser);

      expect(actual).toEqual(expected);
    });

    it("returns the correct params tail if no profile and pathname does not include all", () => {
      const router = {
        pathname: "/opportunities/owned"
      };
      const currentUser = {
        id: "1234"
      };
      const expected = ["", "/opportunities/owned"];
      const actual = getOpportunitiesParamsTail(router, false, null, currentUser);

      expect(actual).toEqual(expected);
    });

    it("returns the correct params tail if no profile, user not logged in, and category exists", () => {
      const router = {
        asPath: "/opportunities/events"
      };
      const currentUser = {};
      const expected = ["", "/public/opportunities"];
      const actual = getOpportunitiesParamsTail(router, false, null, currentUser, "events");

      expect(actual).toEqual(expected);
    });

    it("returns correct params tail if no profile, user logged in, and category exists", () => {
      const router = {
        asPath: "/opportunities/events"
      };
      const currentUser = {
        id: "1234"
      };
      const expected = ["", "/opportunities"];
      const actual = getOpportunitiesParamsTail(router, false, null, currentUser, "events");

      expect(actual).toEqual(expected);
    });

    it("returns an empty array if no checks are met", () => {
      const router = {
        pathname: "does/not/matter"
      };
      const currentUser = {};
      const expected = [];
      const actual = getOpportunitiesParamsTail(router, false, null, currentUser);

      expect(actual).toEqual(expected);
    });
  });

  describe("getOrganizationsParamsTail", () => {
    it("returns the correct params tail if user is not logged in and pathname is organizations", () => {
      const router = {
        pathname: "/organizations"
      };
      const currentUser = {};
      const expected = "/public/organizations";
      const actual = getOrganizationsParamsTail(router, currentUser);

      expect(actual).toEqual(expected);
    });

    it("returns the correct params tail if user is logged in and pathname is organizations", () => {
      const router = {
        pathname: "/organizations"
      };
      const currentUser = {
        id: "12345"
      };
      const expected = "/organizations";
      const actual = getOrganizationsParamsTail(router, currentUser);

      expect(actual).toEqual(expected);
    });

    it("returns the correct params tail if pathname is not organizations", () => {
      const router = {
        pathname: "/organizations/owned"
      };
      const currentUser = {
        id: "12345"
      };
      const expected = "/organizations/owned";
      const actual = getOrganizationsParamsTail(router, currentUser);

      expect(actual).toEqual(expected);
    });
  });
});
