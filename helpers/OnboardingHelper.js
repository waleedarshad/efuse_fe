import cloneDeep from "lodash/cloneDeep";
import isEmpty from "lodash/isEmpty";
import Router from "next/router";
import { isEmptyObject } from "./GeneralHelper";
import { setFlash } from "./FlashHelper";

/**
 @module OnboardingHelper
 @category Helpers
*/

/**
 *
 * @param {*} props
 * @param {*} state
 * @param {*} key
 */
const statesTobeUpdated = (props, state, key) => {
  return isEmptyObject(state[key]) && !isEmpty(props[key]) ? { [key]: cloneDeep(props[key]) } : {};
};

/**
 *
 * @param {*} event
 * @param {*} object
 * @param {*} key
 * @param {*} action
 * @param {*} redirectPath
 * @param {*} requestType
 * @param {*} remoteUrl
 */
const storeToLocalStorage = (
  event,
  object,
  key,
  action,
  redirectPath,
  requestType = "patch",
  remoteUrl = onboardingRemoteUrl()
) => {
  if (event.target.checkValidity()) {
    localStorage?.setItem(key, JSON.stringify(object));
    action(object, key, requestType, remoteUrl, Router, redirectPath);
  }
};

/**
 *
 */
const onboardingRemoteUrl = () => {
  return `/onboarding/profile/${localStorage?.email}`;
};

/**
 *
 */
const finishActions = () => {
  setFlash("onboardingFinish");
  ["user", "professional", "profile", "student"].map(key => localStorage?.removeItem(key));
  Router.push("/login");
};

export { statesTobeUpdated, storeToLocalStorage, onboardingRemoteUrl, finishActions };
