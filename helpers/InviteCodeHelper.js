import { getOrganizationUrl } from "./UrlHelper";

export const getNameFromInviteCode = inviteCode => {
  if (inviteCode?.organization) {
    return inviteCode.organization.name;
  }
  if (inviteCode?.user) {
    return inviteCode.user.name;
  }
  if (inviteCode?.team) {
    return inviteCode.team.owner?.name;
  }
  return "";
};

export const getJoinTextFromInviteCode = inviteCode => {
  if (inviteCode?.team) {
    return `their team, ${inviteCode.team.name}`;
  }
  return "eFuse";
};

export const getRouteFromInviteCode = inviteCode => {
  if (inviteCode?.organization) {
    return getOrganizationUrl(inviteCode.organization);
  }
  if (inviteCode?.user) {
    return `/u/${inviteCode.user.username}`;
  }
  if (inviteCode?.team) {
    return getOrganizationUrl(inviteCode.team?.owner); // TODO: change to team URL
  }
  return "";
};

export const getProfileImageFromInviteCode = inviteCode => {
  if (inviteCode?.organization) {
    return inviteCode.organization.profileImage;
  }
  if (inviteCode?.user) {
    return inviteCode.user.profilePicture;
  }
  if (inviteCode?.team) {
    return inviteCode.team.owner?.profileImage;
  }
  return "";
};

export const getNextSeoDescription = pageProps => {
  if (pageProps.inviteCode.organization) {
    return pageProps.inviteCode.organization.description;
  }
  if (pageProps.inviteCode.user?.bio) {
    return pageProps.inviteCode.user.bio;
  }
  return "";
};

export const getNextSeoImageUrl = pageProps => {
  if (pageProps.inviteCode?.organization?.headerImage?.url) {
    return pageProps.inviteCode.organization.headerImage.url;
  }
  if (pageProps.inviteCode?.user?.headerImage?.url) {
    return pageProps.inviteCode.user.headerImage.url;
  }
  return "";
};
