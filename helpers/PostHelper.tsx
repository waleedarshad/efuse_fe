import isEmpty from "lodash/isEmpty";
import { withCdn } from "../common/utils";

export const parsePostText = (text: string, mentions: { display: string; id: string; type: string }[]) => {
  let content = text;
  if (mentions && mentions.length > 0 && content) {
    mentions.forEach(mention => {
      const toBeMatched = buildMentionTag(mention);
      content = content.replace(
        toBeMatched,
        `<a href='${buildMentionHref(mention)}' class='mentionTagDisplay'>${mention.display}</a>`
      );
    });
  }

  return content;
};

export const parseMention = comment => {
  let { content } = comment;
  const { mentions } = comment;
  if (mentions && mentions.length > 0 && content) {
    comment.mentions.forEach(mention => {
      const toBeMatched = buildMentionTag(mention);
      content = content.replace(
        toBeMatched,
        `<a href='${buildMentionHref(mention)}' class='mentionTagDisplay'>${mention.display}</a>`
      );
    });
  }

  return content;
};

export const buildMentionTag = mention => {
  return mention.type
    ? `@[${mention.display.split("@")[1]}](${mention.id}-${mention.type})`
    : `@[${mention.display.split("@")[1]}](${mention.id})`;
};

export const buildMentionHref = mention => {
  const path = mention.type === "org" ? "/organizations/show" : "/users/posts";

  return `${path}?id=${mention.id}`;
};

export const constructPostUrl = (id, isNewPost = false) => {
  const { pathname, search } = window.location;
  let sharePostPath = pathname;

  if (isNewPost) {
    sharePostPath = "/home";
  }

  const urlParams = new URLSearchParams(search);
  const params = urlParams
    .toString()
    .split("&")
    .filter(param => !param.includes("feedId"));
  params.push(`feedId=${id}`);
  const queryParams = `?${params.filter(p => p).join("&")}`;
  return `${sharePostPath}${queryParams}`;
};

export const neverMatchingRegex = () => /($a)/;

export const renderSuggestion = entry => {
  return (
    <span>
      <img
        style={{
          width: "30px",
          height: "auto",
          borderRadius: "5px",
          marginRight: "8px"
        }}
        src={entry.profilePicture}
        alt=""
      />
      {entry.display} {entry.type === "user" && `(${entry.fullName})`} &nbsp;
      {entry.verified && (
        <>
          <img style={{ width: "16px", height: "auto" }} src={withCdn("/static/images/efuse_verify.png")} alt="" />
        </>
      )}
    </span>
  );
};
// parsing content for embedding twitch stream inside post
export const getTwitchChannel = content => {
  const urlMatch = content.match(/\bhttps?:\/\/?(?:www\.)?twitch.tv\S*\s?/g) || [];
  const domainMatch = content.match(/(\s|^)twitch.tv\S*\s?/g) || [];
  const matches = [...urlMatch, ...domainMatch];
  if (!isEmpty(matches) && matches[0].indexOf("/", 9) > 0 && !isEmpty(matches[0].split("/").pop())) {
    return matches[0].split("/").pop();
  }
  return null;
};
export const filterOutTwitchUrls = urls => urls.filter(url => !/\bhttps?:\/\/?(?:www\.)?twitch.tv\S*\s?/g.test(url));
