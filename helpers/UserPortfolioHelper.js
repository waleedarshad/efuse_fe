import isEmpty from "lodash/isEmpty";

/**
 * Defines the user fields checks done in `getUserFilledPortfolioFields`
 */
const USER_FIELD_KINDS = {
  GAMING_SKILLS: "gamingSkills",
  BUSINESS_SKILLS: "businessSkills",
  BUSINESS_EXPERIENCE: "businessExperience",
  GAME_PLATFORM: "gamePlatform",
  EDUCATION_EXPERIENCE: "educationExperience",
  PORTFOLIO_EVENTS: "portfolioEvents",
  PORTFOLIO_HONORS: "portfolioHonors",
  VIDEO_LINKED: "videoLinked",
  STREAM_LINKED: "streamLinked",
  SOCIAL_LINKED: "socialLinked",
  GAME_STATS: "gameStats"
};

/**
 * @summary Retrieves an array of fields the user has completed/filled out
 * @method
 * @param  {User} user
 * @returns {Arraay} Returns an array of string values that exist on the user object
 * @example
 * UserPortfolioHelper.getUserFilledPortfolioFields(user)
 */
const getUserFilledPortfolioFields = user => {
  const userFields = [];
  if (user?.gamingSkills?.length > 0) userFields.push(USER_FIELD_KINDS.GAMING_SKILLS);
  if (user?.businessSkills?.length > 0) userFields.push(USER_FIELD_KINDS.BUSINESS_SKILLS);
  if (user?.businessExperience?.length > 0) userFields.push(USER_FIELD_KINDS.BUSINESS_EXPERIENCE);
  if (user?.gamePlatforms?.length > 0) userFields.push(USER_FIELD_KINDS.GAME_PLATFORM);
  if (user?.educationExperience?.length > 0) userFields.push(USER_FIELD_KINDS.EDUCATION_EXPERIENCE);

  if (user?.portfolioEvents?.length > 0) userFields.push(USER_FIELD_KINDS.PORTFOLIO_EVENTS);
  if (user?.portfolioHonors?.length > 0) userFields.push(USER_FIELD_KINDS.PORTFOLIO_HONORS);

  if (user?.twitchVerified || user?.youtubeChannelId || user?.embeddedLink) {
    userFields.push(USER_FIELD_KINDS.VIDEO_LINKED);
  }

  if (user?.twitchVerified || user?.youtubeChannelId) userFields.push(USER_FIELD_KINDS.STREAM_LINKED);

  if (
    user?.twitchVerified ||
    user?.twitterVerified ||
    user?.discordVerified ||
    user?.snapchat?.displayName ||
    user?.youtube?.displayName ||
    user?.tiktok?.displayName
  ) {
    userFields.push(USER_FIELD_KINDS.SOCIAL_LINKED);
  }

  if (
    !isEmpty(user?.fortnite?.stats) ||
    !isEmpty(user?.csgo?.segments) ||
    !isEmpty(user?.leagueOfLegends?.verified && user.leagueOfLegends?.stats?._id) ||
    !isEmpty(user?.callOfDuty?.platforms) ||
    !isEmpty(user?.pubg?.platforms) ||
    user?.riot ||
    user?.statespace
  ) {
    userFields.push(USER_FIELD_KINDS.GAME_STATS);
  }

  return userFields;
};

/**
 * @summary Get a sorted array of objects by date
 * @method
 * @param  {Array} array Array of objects to be sorted
 * @param  {String} dateProp Name of the date prop in data object
 * @returns {String} Return a descendingly sorted array of objects by date
 * @example
 * GeneralHelper.sortArray(array,"dateProp")
 */
const sortArrayByDate = (array, dateProp) => {
  let sortedArray = array?.sort((a, b) => new Date(b[dateProp]) - new Date(a[dateProp]));
  sortedArray = sortedArray?.sort((a, b) => b.present - a.present);

  return sortedArray;
};

/**
 * @summary Get a formatted array of meta tag objects to be used to configue mugshotbot.com custom OG image
 * @method
 * @param  {Object} user Full user object of portfolio viewed.
 * @returns {Array} Return a descendingly sorted array of objects by date
 * @example
 * UserPortfolioHelper.generateMugShotBotMetaTags(user)
 */
const generateMugShotBotMetaTags = user => {
  const metatags = [
    {
      property: "mugshot:efuse:name",
      content: user?.name
    },
    {
      property: "mugshot:efuse:username",
      content: `@${user?.username}`
    },
    {
      property: "mugshot:efuse:image",
      content: user?.profilePicture?.url
    },
    {
      property: "mugshot:efuse:bio",
      content: user?.bio || `View ${user.name}'s gaming portfolio, social accounts, and games stats on eFuse.`
    }
  ];

  // Generate string with AFFILIATE/PARTNER/EMPLOYEE if user belongs to any. Only one badge can be shown.
  let userBadges;

  // eslint-disable-next-line no-unused-expressions
  user?.badges.forEach(badge => {
    if (badge?.badge?.slug === "AFFILIATE" || badge?.badge?.slug === "PARTNER") {
      // Disabling badges on mugshot preview images until affiliate/partner badges are publicily launched
      // userBadges = badge.badge.slug;
    }
  });

  if (user?.roles.includes("employee")) userBadges = "EMPLOYEE";

  if (userBadges)
    metatags.push({
      property: "mugshot:efuse:badges",
      content: userBadges
    });

  // Generate comma separated string of all social accounts linked to portfolio
  const userSocialAccounts = [];

  if (user?.twitchVerified) userSocialAccounts.push("TWITCH");
  if (user?.twitterVerified) userSocialAccounts.push("TWITTER");
  if (user?.discordVerified) userSocialAccounts.push("DISCORD");
  if (user?.snapchat?.displayName) userSocialAccounts.push("SNAPCHAT");
  if (user?.googleVerified) userSocialAccounts.push("YOUTUBE");
  if (user?.tikTok?.tiktokUsername) userSocialAccounts.push("TIKTOK");

  if (userSocialAccounts.length)
    metatags.push({
      property: "mugshot:efuse:networks",
      content: userSocialAccounts.join(",")
    });

  // Generate comma separated string with games the user plays.
  const userGames = [];

  if (!isEmpty(user?.fortnite?.stats)) userGames.push("Fortnite");
  if (!isEmpty(user?.leagueOfLegends?.stats)) userGames.push("League of Legends");
  if (!isEmpty(user?.callOfDuty?.stats)) userGames.push("Call of Duty");
  if (!isEmpty(user?.pubg?.stats)) userGames.push("PUBG");

  if (userGames.length)
    metatags.push({
      property: "mugshot:efuse:tags",
      content: userGames.join(",")
    });

  return metatags;
};

export { getUserFilledPortfolioFields, sortArrayByDate, USER_FIELD_KINDS, generateMugShotBotMetaTags };
