import {
  generateRangeArray,
  monthsOptions,
  buildMonthOptions,
  yearsOptions,
  convertStatToString,
  trimLastCharacter,
  getArrayFilteredByArray
} from "./GeneralHelper";

describe("GeneralHelper.js", () => {
  test("generateRangeArray(start, stop, step)", () => {
    expect(generateRangeArray(1, 5, 1)).toEqual([1, 2, 3, 4]);
  });

  test("buildMonthOptions(collection, placeholder)", () => {
    const months = ["January", "February"];
    expect(buildMonthOptions(months, "M")).toEqual([
      { label: "M", value: "" },
      { label: "January", value: 1 },
      { label: "February", value: 2 }
    ]);
  });

  test("convertStatToString(num)", () => {
    expect(convertStatToString(1.064231)).toEqual("1.06");
    expect(convertStatToString(1.06643)).toEqual("1.07");
    expect(convertStatToString(1111111)).toEqual("1,111,111");
    expect(convertStatToString(1111111.12231)).toEqual("1,111,111.12");
  });

  test("monthsOptions()", () => {
    expect(monthsOptions()).toEqual([
      { label: "Month", value: "" },
      { label: "January", value: 1 },
      { label: "February", value: 2 },
      { label: "March", value: 3 },
      { label: "April", value: 4 },
      { label: "May", value: 5 },
      { label: "June", value: 6 },
      { label: "July", value: 7 },
      { label: "August", value: 8 },
      { label: "September", value: 9 },
      { label: "October", value: 10 },
      { label: "November", value: 11 },
      { label: "December", value: 12 }
    ]);
  });

  test("yearsOptions", () => {
    expect(yearsOptions()).toEqual([
      { label: "year", value: "" },
      { label: 1920, value: 1920 },
      { label: 1921, value: 1921 },
      { label: 1922, value: 1922 },
      { label: 1923, value: 1923 },
      { label: 1924, value: 1924 },
      { label: 1925, value: 1925 },
      { label: 1926, value: 1926 },
      { label: 1927, value: 1927 },
      { label: 1928, value: 1928 },
      { label: 1929, value: 1929 },
      { label: 1930, value: 1930 },
      { label: 1931, value: 1931 },
      { label: 1932, value: 1932 },
      { label: 1933, value: 1933 },
      { label: 1934, value: 1934 },
      { label: 1935, value: 1935 },
      { label: 1936, value: 1936 },
      { label: 1937, value: 1937 },
      { label: 1938, value: 1938 },
      { label: 1939, value: 1939 },
      { label: 1940, value: 1940 },
      { label: 1941, value: 1941 },
      { label: 1942, value: 1942 },
      { label: 1943, value: 1943 },
      { label: 1944, value: 1944 },
      { label: 1945, value: 1945 },
      { label: 1946, value: 1946 },
      { label: 1947, value: 1947 },
      { label: 1948, value: 1948 },
      { label: 1949, value: 1949 },
      { label: 1950, value: 1950 },
      { label: 1951, value: 1951 },
      { label: 1952, value: 1952 },
      { label: 1953, value: 1953 },
      { label: 1954, value: 1954 },
      { label: 1955, value: 1955 },
      { label: 1956, value: 1956 },
      { label: 1957, value: 1957 },
      { label: 1958, value: 1958 },
      { label: 1959, value: 1959 },
      { label: 1960, value: 1960 },
      { label: 1961, value: 1961 },
      { label: 1962, value: 1962 },
      { label: 1963, value: 1963 },
      { label: 1964, value: 1964 },
      { label: 1965, value: 1965 },
      { label: 1966, value: 1966 },
      { label: 1967, value: 1967 },
      { label: 1968, value: 1968 },
      { label: 1969, value: 1969 },
      { label: 1970, value: 1970 },
      { label: 1971, value: 1971 },
      { label: 1972, value: 1972 },
      { label: 1973, value: 1973 },
      { label: 1974, value: 1974 },
      { label: 1975, value: 1975 },
      { label: 1976, value: 1976 },
      { label: 1977, value: 1977 },
      { label: 1978, value: 1978 },
      { label: 1979, value: 1979 },
      { label: 1980, value: 1980 },
      { label: 1981, value: 1981 },
      { label: 1982, value: 1982 },
      { label: 1983, value: 1983 },
      { label: 1984, value: 1984 },
      { label: 1985, value: 1985 },
      { label: 1986, value: 1986 },
      { label: 1987, value: 1987 },
      { label: 1988, value: 1988 },
      { label: 1989, value: 1989 },
      { label: 1990, value: 1990 },
      { label: 1991, value: 1991 },
      { label: 1992, value: 1992 },
      { label: 1993, value: 1993 },
      { label: 1994, value: 1994 },
      { label: 1995, value: 1995 },
      { label: 1996, value: 1996 },
      { label: 1997, value: 1997 },
      { label: 1998, value: 1998 },
      { label: 1999, value: 1999 },
      { label: 2000, value: 2000 },
      { label: 2001, value: 2001 },
      { label: 2002, value: 2002 },
      { label: 2003, value: 2003 },
      { label: 2004, value: 2004 },
      { label: 2005, value: 2005 },
      { label: 2006, value: 2006 },
      { label: 2007, value: 2007 },
      { label: 2008, value: 2008 },
      { label: 2009, value: 2009 },
      { label: 2010, value: 2010 },
      { label: 2011, value: 2011 },
      { label: 2012, value: 2012 },
      { label: 2013, value: 2013 },
      { label: 2014, value: 2014 },
      { label: 2015, value: 2015 },
      { label: 2016, value: 2016 },
      { label: 2017, value: 2017 },
      { label: 2018, value: 2018 },
      { label: 2019, value: 2019 },
      { label: 2020, value: 2020 },
      { label: 2021, value: 2021 }
    ]);
  });

  describe("trimLastCharacter", () => {
    it("trimLastCharacter returns original string if last character doesn't match one passed as parameter", () => {
      const expected = "original_string&";
      const actual = trimLastCharacter(expected, "*");

      expect(actual).toEqual(expected);
    });

    it("trimLastCharacter trims last character if it matches one passed as parameter", () => {
      const expected = "original_string";
      const actual = trimLastCharacter("original_string*", "*");

      expect(actual).toEqual(expected);
    });
  });

  describe("getArrayFilteredByArray", () => {
    it("getArrayFilteredByArray returns original array if it does not contain any items from filtering array", () => {
      const filteringArray = ["hello", "world"];
      const expected = ["original", "array"];

      const actual = getArrayFilteredByArray(expected, filteringArray);

      expect(actual).toEqual(expected);
    });

    it("getArrayFilteredByArray returns correct array if contains items from filtering array", () => {
      const filteringArray = ["hello", "world"];
      const originalArray = ["original", "world", "array"];

      const actual = getArrayFilteredByArray(originalArray, filteringArray);

      expect(actual).toEqual(["original", "array"]);
    });
  });
});
