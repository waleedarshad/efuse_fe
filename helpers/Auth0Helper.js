import Cookies from "js-cookie";
import { setCookie, parseCookies } from "nookies";
import { fetchJSON } from "refresh-fetch";
import getConfig from "next/config";

const { publicRuntimeConfig } = getConfig();

const { defaultUrl } = publicRuntimeConfig;

export const AUTH_COOKIE_KEYS = {
  eFuseToken: "token",
  eFuseRefreshToken: "refreshToken",
  eFuseCurrentUser: "currentUser",
  segmentUserId: "ajs_user_id"
};

export const TOKEN_TYPE_EFUSE = "efuse";
export const TOKEN_KIND_ACCESS_TOKEN = "accessToken";
export const TOKEN_KIND_REFRESH_TOKEN = "refreshToken";
export const EFUSE_TOKEN_EXPIRY = 14 * 24 * 60 * 60; // 14 Days

export const getEfuseToken = context => parseCookies(context)[AUTH_COOKIE_KEYS.eFuseToken];

export const getEfuseRefreshToken = context => parseCookies(context)[AUTH_COOKIE_KEYS.eFuseRefreshToken];

export const getEfuseCurrentUser = context => parseCookies(context)[AUTH_COOKIE_KEYS.eFuseCurrentUser];

export const getToken = context => getEfuseToken(context);

export const getRefreshToken = context => getEfuseRefreshToken(context);

export const getCurrentUser = context => getEfuseCurrentUser(context);

export const getTokenType = (kind, context) => {
  let eFuseToken;

  switch (kind) {
    case TOKEN_KIND_ACCESS_TOKEN:
      eFuseToken = getEfuseToken(context);
      break;
    case TOKEN_KIND_REFRESH_TOKEN:
      eFuseToken = getEfuseRefreshToken(context);
      break;
    default:
      eFuseToken = getEfuseToken(context);
      break;
  }

  let tokenType = "";

  if (eFuseToken) {
    tokenType = TOKEN_TYPE_EFUSE;
  }

  return tokenType;
};

export const isEfuseTokenUsed = context => getTokenType(TOKEN_KIND_ACCESS_TOKEN, context) === TOKEN_TYPE_EFUSE;
export const isEfuseRefreshTokenUsed = context => getTokenType(TOKEN_KIND_REFRESH_TOKEN, context) === TOKEN_TYPE_EFUSE;

export const removeTokens = () => Object.values(AUTH_COOKIE_KEYS).forEach(key => Cookies.remove(key));

export const setEfuseCookies = (ctx, session) => {
  const options = { maxAge: EFUSE_TOKEN_EXPIRY, path: "/" };

  setCookie(ctx, AUTH_COOKIE_KEYS.eFuseToken, session.token, options);
  setCookie(ctx, AUTH_COOKIE_KEYS.eFuseRefreshToken, session.refreshToken, options);
};

export const attemptToRefreshToken = async context => {
  const refreshToken = getRefreshToken(context);

  if (!refreshToken) {
    throw new Error("Session expired!");
  }

  const ENDPOINT = "/auth/refresh_token";

  const { body } = await fetchJSON(`${defaultUrl}${ENDPOINT}`, {
    method: "POST",
    body: JSON.stringify({ refreshToken })
  });

  // Set Appropriate cookies
  setEfuseCookies(context, body);

  return body;
};
