/**
 @module HeaderHelper
 @category Helpers
*/

/**
 * @method
 * @param {} router
 */
const activeUrl = router => {
  const currentUrl = router.pathname;
  switch (currentUrl.split("/")[1]) {
    case "opportunities":
      return "/opportunities";
    case "home":
      return "/home";
    case "messages":
      return "/messages";
    case "u":
      return router.asPath;
    case "lounge":
      return "/home";
    case "settings":
      return "/settings";
    case "discover":
      return "/discover";
    case "learning":
      return "/learning";
  }
};

export { activeUrl };
