import { getMemberRole } from "./OrganizationHelper";

describe("OrganizationHelper", () => {
  describe("getMemberRole", () => {
    it("returns member by default", () => {
      const expected = "Member";
      const actual = getMemberRole("123", "131", ["111, 121"]);

      expect(expected).toEqual(actual);
    });

    it("returns owner if member id matches owner", () => {
      const expected = "Owner";
      const actual = getMemberRole("131", "131", ["111", "121"]);

      expect(expected).toEqual(actual);
    });

    it("returns captain if member id exists in captains", () => {
      const expected = "Captain";
      const actual = getMemberRole("111", "131", ["111", "121"]);

      expect(expected).toEqual(actual);
    });

    it("returns owner if member id matches both owner and captains", () => {
      const expected = "Owner";
      const actual = getMemberRole("131", "131", ["131", "111"]);

      expect(expected).toEqual(actual);
    });
  });
});
