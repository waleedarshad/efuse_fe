import moment from "moment";

/**
 @module ChatHelper
 @category Helpers
*/

const getChatRoomId = (currentUser, otherPerson) => {
  try {
    const currentUserDate = moment(new Date(currentUser.createdAt));
    const otherPersonDate = moment(new Date(otherPerson.createdAt));
    return currentUserDate.isBefore(otherPersonDate)
      ? `${currentUser.id}_${otherPerson._id}`
      : `${otherPerson._id}_${currentUser.id}`;
  } catch {
    return "";
  }
};

const fetchChatMessages = (id, asWindow, props, name) => {
  props.chatReset();
  props.selectChat(id, name);
  if (asWindow) props.setWindowChatView(true);
};

const joinChatRoom = props => {
  const { getSocket } = require("../websockets");
  const socket = getSocket();
  const { currentUser, otherPerson } = props;
  const chatRoomId = getChatRoomId(currentUser, otherPerson);
  socket.emit("subscribeChat", chatRoomId);
};

export { getChatRoomId, fetchChatMessages, joinChatRoom };
