import capitalize from "lodash/capitalize";
import isEmpty from "lodash/isEmpty";
import toLower from "lodash/toLower";
import dayjs from "dayjs";
import images from "../static/data/images.json";

/**
 @module GeneralHelper
 @category Helpers
*/

/**
 * @summary Generate a range of years from the start to the stop value
 * @method
 * @param  {Integer} start
 * @param  {Integer} stop
 * @param  {Integer} step
 * @returns {Array} Range of integer values from start -> stop
 * @example
 * const years = GeneralHelper.generateRangeArray(currentYear - 30, currentYear + 1, 1);
 */
const generateRangeArray = (start, stop, step) =>
  Array.from({ length: (stop - start) / step }, (_, i) => start + i * step);

/**
 * @summary Creates a static option object
 * @method
 * @param  {String} label
 * @returns {Object} Returns an object in the form of {label, value}
 * @example
 * staticOption(placeholder)
 */
const staticOption = label => {
  return { label, value: "" };
};

/**
 * @summary Create an array of values given a collection, placeholder and index
 * @method
 * @param  {Collection} collection
 * @param  {String} placeholder
 * @param  {Boolean} setIndexAsValue=true
 * @example
 * options={GeneralHelper.buildOptions(years, ("placeholders.graduationYear"), false)}
 */
const buildOptions = (collection, placeholder, setIndexAsValue = true) => {
  let options = collection.map((obj, index) => {
    const objValue = setIndexAsValue ? index + 1 : obj;
    return { label: obj, value: objValue };
  });
  if (placeholder) {
    options = [staticOption(placeholder), ...options];
  }
  return options;
};

/**
 * @summary Create an array of objects given a collection containing objects with {_id, title}
 * @method
 * @param  {String} placeholder
 * @param  {Array} collection Array of objects with {_id, title}
 * @returns {Array} Returns an array of option objects {value, label}
 * @example
 * GeneralHelper.buildOptionsFromCollection(t("createOpportunity:placeholders.selectGame"),games)
 */
const buildOptionsFromCollection = (placeholder, collection) => [
  staticOption(placeholder),
  ...collection.map(obj => {
    return {
      value: obj._id,
      label: obj.title
    };
  })
];

/**
 * @summary Generate a list of months from Janurary to December
 * @method
 * @param  {Array} collection A list of months in the form of strings
 * @param  {String} placeholder A placeholder value for dropdown lists
 * @returns {Array} Returns an array of month objects {value, label}
 * @example
 * buildMonthOptions(months, "Month")
 */
const buildMonthOptions = (collection, placeholder) => {
  let options = collection.map((obj, index) => {
    return { label: obj, value: index + 1 };
  });
  if (placeholder) {
    options = [{ label: placeholder, value: "" }, ...options];
  }
  return options;
};

/**
 * @summary Generate a list of months from Janurary to December
 * @method
 * @returns {Array} An array of object months in the form of {label, value}
 * @example
 * GeneralHelper.monthsOptions()
 */
const monthsOptions = () => {
  const months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
  ];
  return buildMonthOptions(months, "Month");
};

/**
 * @summary Returns an age given a date object
 * @method
 * @param {string} age
 * @returns {Integer} Returns an integer age
 * @example
 * GeneralHelper.getAge(user.dateOfBirth)
 */
const getAge = age => {
  return dayjs().diff(age, "years");
};

/**
 * @summary Returns an array of year objects in the form of {label (year), value}
 * @method
 * @param {Integer} yearsBack
 * @param {Integer} yearsForward
 * @param {String} label
 * @param {Boolean} reverse
 * @param {Integer} label
 * @returns {Array} List of years from (currentYear-100 -> currentYear)
 * @example
 * GeneralHelper.yearsOptions()
 */
const yearsOptions = (yearsBack = 101, yearsForward = 1, label = "year", reverse = false) => {
  const currentYear = new Date().getFullYear();
  const years = generateRangeArray(
    reverse ? currentYear + yearsForward : currentYear - yearsBack,
    reverse ? currentYear - yearsBack : currentYear + yearsForward,
    reverse ? -1 : 1
  );
  return buildOptions(years, label, false);
};

/**
 * @summary Returns an array of day objects in the form of {label (day), value}
 * @method
 * @returns {Array} List of days from (1 -> 31)
 * @example
 * GeneralHelper.daysOptions()
 */
const daysOptions = () => {
  const days = generateRangeArray(1, 32, 1);
  return buildOptions(days, "D", false);
};

/**
 * @summary Checks if an object is empty
 * @method
 * @param  {Object} object
 * @returns {Boolean} Returns true if object is empty
 * @example
 * GeneralHelper.isEmptyObject(props.organizations)
 */
const isEmptyObject = object => {
  return isEmpty(object) || isEmpty(Object.values(object).filter(obj => !isEmpty(obj)));
};

/**
 * @summary Returns an empty string if the given prop and key doesn't exist
 * @method
 * @param  {} prop
 * @param  {} key
 * @param  {} rescueWith=""
 * @returns {} Returns value if it exists and an empty string if it doesn't exist
 * @example
 * GeneralHelper.rescueNil(currentUser, "bio")
 */
const rescueNil = (prop, key, rescueWith = "") => {
  try {
    return prop && prop[key] ? prop[key] : rescueWith;
  } catch (e) {
    return rescueWith;
  }
};

/**
 * @summary Removes all members of an object that are null or have and empty value
 * @method
 * @param {*} obj The object to clean
 * @returns Returns cleaned version of the passed obj param
 */
const removeEmptyOrNull = obj => {
  const localObj = obj;
  Object.keys(localObj).forEach(
    k =>
      (localObj[k] && typeof localObj[k] === "object" && removeEmptyOrNull(localObj[k])) ||
      (!localObj[k] && localObj[k] !== undefined && delete localObj[k])
  );
  return localObj;
};

/**
 * @summary Merges two arrays, labels and values, and returns a new array of options in the form of {label, value}
 * @method
 * @param  {Array} labels
 * @param  {Array} values
 * @returns {Array} Returns a combined array of options
 * @example
 * GeneralHelper.mergeLabelsAndValues(["Opportunity Type","Scholarship","Employment Opening","Team Opening","Event"]), ["","Scholarship","Employment Opening","Team Opening","Event"])
 */
const mergeLabelsAndValues = (labels, values) => {
  const options = [];
  try {
    labels.forEach((label, index) => options.push({ label, value: values[index] }));
    return options;
  } catch (e) {
    return options;
  }
};

/**
 * @summary Creates and returns a list of html options for the last 100 years
 * @method
 * @param  {Boolean} includeCurrentYear
 * @returns {HTMLOptionElement} Returns a list of options (html elements)
 * @example
 * GeneralHelper.buildYearOptions(includeCurrentYear)
 */
const buildYearOptions = (includeCurrentYear = false) => {
  const currentYear = new Date().getFullYear();
  const upperLimit = currentYear - 101;
  const lowerLimit = includeCurrentYear ? currentYear + 1 : currentYear - 12;
  let generatedRange = generateRangeArray(upperLimit, lowerLimit, 1);
  generatedRange = generatedRange.reverse();

  return generatedRange.map(year => (
    <option value={year} key={year}>
      {year}
    </option>
  ));
};

/**
 * @summary Formats a date in the form of "MMMM DD, YYYY"
 * @method
 * @param  {Date} date
 * @returns {dayjs} Returns a formatted date ("MMMM DD[,] YYYY")
 * @example
 * GeneralHelper.formatDate(user.dateOfBirth)
 */
const formatDate = date => dayjs(new Date(date)).format("MMMM DD[,] YYYY");

/**
 * @summary Formats a date in the form of "MMM. DD | h:mm AM/PM"
 * @method
 * @param  {Date} date
 * @returns {dayjs} Returns a formatted date ("MMM. DD | h:mm A")
 * @example
 * GeneralHelper.formatShortDateTime(client.created_at)
 */
const formatShortDateTime = date => dayjs(new Date(date)).format("MMM. DD | h:mm A");

/**
 * @summary Formats a date in the form of "MMMM DD, YYYY HH:mm"
 * @method
 * @param  {Date} date
 * @returns {dayjs} Returns a formatted date ("MMMM DD[,] YYYY HH:mm")
 * @example
 * GeneralHelper.formatDateTime(client.created_at)
 */
const formatDateTime = date => dayjs(new Date(date)).format("MMMM DD[,] YYYY HH:mm");

/**
 * @summary Formats a date in the form of "HH:mm"
 * @method
 * @param  {Date} date
 * @returns {dayjs} Returns a formatted date ("HH:mm")
 * @example
 * GeneralHelper.formatTime(post.scheduledAt)
 */
const formatTime = date => dayjs(new Date(date)).format("HH:mm");

/**
 * @summary Formats a date in the form of "HH:mm"
 * @method
 * @param  {Date} date
 * @returns {dayjs} Returns a formatted date ("hh:mm A")
 * @example
 * GeneralHelper.formatOnlyTime(chat.createdAt)
 */
const formatOnlyTime = date => dayjs(new Date(date)).format("hh:mm A");

/**
 * @summary Formats a date in the form of "MMMM YYYY"
 * @method
 * @param  {Date} date
 * @returns {dayjs} Returns a formatted date ("MMMM YYYY")
 * @example
 * GeneralHelper.formatDateMonthYear(startDate)
 */
const formatDateMonthYear = date => dayjs(new Date(date)).format("MMMM YYYY");

/**
 * @summary Formats a date in the form of "MMMM D, YYYY"
 * @method
 * @param  {Date} date
 * @returns {dayjs} Returns a formatted date ("MMMM D, YYYY")
 * @example
 * GeneralHelper.formatMonth(this.props.date)
 */
const formatMonth = date => dayjs(new Date(date)).format("MMMM D, YYYY");

/**
 * @summary Rounds an integer to 2 decimal places
 * @method
 * @param  {Integer} money
 * @returns {Integer} Returns an integer with 2 decimals places
 * @example
 * GeneralHelper.formatMoney(1000.0000)
 */
const formatMoney = money => parseFloat(money).toFixed(2);

/**
 * @summary Retrieves the image from the given user/org/opportunity object
 * @method
 * @param  {Object} image
 * @param  {String} key
 * @returns {String} If the object is empty, the key will be used to return an image; else, the image url is returned
 * @example
 * GeneralHelper.getImage(user.headerImage, "organization")
 */
const getImage = (image, key) => (isEmpty(image) ? images[key] : image.url);

/**
 * @summary Returns the user's full name by combining the first and last name
 * @method
 * @param  {User} user
 * @returns {String} The full name of the given user
 * @example
 * GeneralHelper.fullName(user)
 */
const fullName = user => rescueNil(user, "name");
const titleCase = string => string.replace(/\b\w/g, txt => txt.toUpperCase());

/**
 * @summary Retrieves a list of the user's roles
 * @method
 * @param  {User} user
 * @returns {Array} An array of string role values that exist on the given user object
 * @example
 * GeneralHelper.formatRoles(currentUser)
 */
const formatRoles = user => {
  const capitalizeRoles = [];
  const roles = rescueNil(user, "roles", []) || [];
  roles.forEach(role => {
    if (role !== "admin") {
      capitalizeRoles.push(capitalize(role));
    }
  });
  return capitalizeRoles.join("-");
};

/**
 * @summary Converts a collection to an object query, used during infinite scroll
 * @method
 * @param  {Collection} filtered
 * @returns {Object} The updated object from a given collection
 * @example
 * GeneralHelper.mapCollectionToObjectQuery(filtered)
 */
const mapCollectionToObjectQuery = filtered => {
  const obj = {};

  if (filtered.length === 0 || isEmpty(filtered)) {
    return obj;
  }

  filtered.forEach(item => {
    obj[item.id] = item.value;
  });

  return obj;
};

/**
 * @summary Creates the admin redirection path if the event is triggered from an admin
 * @method
 * @param  {Boolean} fromAdmin
 * @param  {String} adminPath
 * @param  {String} otherPath
 * @returns {String} Returns the string of the correct redirect path
 * @example GeneralHelper.adminRedirectionPath(this.props.fromAdmin,"/admin","/learning")
 */
const adminRedirectionPath = (fromAdmin, adminPath, otherPath) => {
  return fromAdmin ? adminPath : otherPath;
};

/**
 * @summary If an id exists in the query object, a new path will be created; else, the path parameter will be used
 * @method
 * @param  {String} path
 * @param  {Object} query
 * @returns {URL} Returns the updated url path given a query and path
 * @example
 * GeneralHelper.constructPath("/users/posts", query)
 */
const constructPath = (path, query) => {
  const { id, u } = query;
  let url = id ? `${path}?id=${id}` : path;
  if (u) {
    url = `${url}&u=${u}`;
  }
  return url;
};

/**
 * @summary Formats the user location and returns the updated format
 * @method
 * @param  {User} user
 * @returns {String} Returns the updated location
 * @example
 * GeneralHelper.userLocation(user)
 */
const userLocation = user => {
  try {
    const addressArray = user.address.split(",");
    return addressArray.slice(addressArray.length - 2).join(", ");
  } catch (e) {
    return "";
  }
};

/**
 * @summary Replaces the language paths with empty strings
 * @method
 * @returns {String} Returns the updated pathname
 * @example
 * GeneralHelper.getPathname()
 */
const getPathname = () => {
  let { pathname } = window.location;
  pathname = pathname.replace("/ja", "");
  pathname = pathname.replace("/en", "");
  pathname = pathname.replace("/ko", "");
  pathname = pathname.replace("/zh_CH", "");
  return pathname;
};

/**
 * @summary Checks if the current path and the link path are equal
 * @method
 * @param  {String} currentPath
 * @param  {String} linkPath
 * @param  {Style} Style
 * @returns {Style} Returns the active style if the two given paths are equal; else, returns an empty string
 * @example
 * GeneralHelper.activeLink(router.pathname,requestPath,Style)
 */
const activeLink = (currentPath, linkPath, Style) => (currentPath === linkPath ? Style.active : "");

// I don't know what this is doing
const ordinalize = number => {
  const absNumber = Math.abs(Number(number));
  const mod100 = absNumber % 100;
  let symbol = "";
  if (mod100 === 11 || mod100 === 12 || mod100 === 13) {
    symbol = "th";
  } else {
    switch (absNumber % 10) {
      case 1:
        symbol = "st";
        break;
      case 2:
        symbol = "nd";
        break;
      case 3:
        symbol = "rd";
        break;
      default:
        symbol = "th";
        break;
    }
  }
  return `${number}${symbol}`;
};

/**
 * @summary Returns a default year of 13 years ago (age 13 is required to signup)
 * @method
 * @param  {Date} date
 * @returns {dayjs} Returns a dayjs date value of 13 years ago
 * @example
 * GeneralHelper.defaultDobYear(date)
 */
const defaultDobYear = date => {
  return dayjs(date).year() === dayjs().year() ? dayjs(date).subtract(13, "years") : date;
};

/**
 * @summary Checks if the current tab is active in the applicants queue
 * @method
 * @param  {String} activeTab
 * @param  {String} currentTab
 * @param  {Style} activeClass
 * @returns {Boolean} Returns true if the active tab equals the current tab and an active class exists
 * @example
 * GeneralHelper.conditionalActive(activeTab,currentTab,activeClass)
 */
const conditionalActive = (activeTab, currentTab, activeClass) => activeTab === currentTab && activeClass;

/**
 * @summary Extracts text from an html string
 * @method
 * @param  {String} htmlString
 * @returns {String} Returns a new string stripped of html elements
 * @example
 * GeneralHelper.getTextFromHtmlString(about)
 */
const getTextFromHtmlString = htmlString => htmlString.replace(/<[^>]*>?/gm, "");

/**
 * @summary Retrieves an image url for events and honors
 * @method
 * @param  {Object} prop
 * @param {String} key
 * @returns {String} Returns the url for the proper image to display
 * @example
 * GeneralHelper.getImageForInputDisplay(image, "url")
 */
const getImageForInputDisplay = (prop, key) => {
  const image = rescueNil(prop, key);
  return image && image.includes("/static/images/") && /briefcase.png|no_image.jpg/.test(image) ? "" : image;
};

/**
 * @summary Retrieves the browser information for the current user agent
 * @method
 * @returns {String} Returns a string of the current browser
 * @example
 * GeneralHelper.getBrowserInfo()
 */
const getBrowserInfo = () => {
  try {
    const ua = navigator.userAgent.match(/(opera|chrome|safari|firefox|msie)\/?\s*(\.?\d+(\.\d+)*)/i);
    let userAgent = "";
    if (navigator.userAgent.match(/Edge/i) || navigator.userAgent.match(/Trident.*rv[ :]*11\./i)) {
      userAgent = "msie";
    } else {
      userAgent = toLower(ua[1]);
    }
    return userAgent;
  } catch (e) {
    return "";
  }
};

/**
 * @summary Rounds a number to the nearest 2 decimal places and adds commas when necessary
 * @method
 * @param  {Integer} num
 * @returns {String} Returns a display value string for a rounded number
 * @example
 * GeneralHelper.convertStatToString(warzoneScore)
 */
const convertStatToString = num => {
  const twoDecimals = Math.round(num * 100) / 100;
  return twoDecimals.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

/**
 * @summary Creates a clean percentage value of 2 decimals
 * @method
 * @param  {Integer} num
 * @returns {String} Returns the new percentage value as a string
 * @example
 * GeneralHelper.decimalToCleanPercentage(progressBar)
 */
const decimalToCleanPercentage = num => {
  const twoDecimals = Math.round(num * 100) / 100;
  const s = twoDecimals.toString();
  return s.substring(0, s.indexOf("."));
};

/**
 * @summary Extracts the month from a given date value
 * @method
 * @param  {Date} date
 * @returns {Integer} Returns the integer month
 * @example
 * GeneralHelper.getMonthFromDate(props.experience.startDate)
 */
const getMonthFromDate = date => dayjs(date).month() + 1;

/**
 * @summary Extracts the year from a given date value
 * @method
 * @param  {Date} date
 * @returns {Integer} Returns the integer year
 * @example
 * GeneralHelper.getYearFromDate(props.experience.startDate)
 */
const getYearFromDate = date => dayjs(date).year();

const getAgeInYears = dob => {
  const birthdate = new Date(dob);
  const cur = new Date();
  const diff = cur - birthdate; // This is the difference in milliseconds
  return Math.floor(diff / (1000 * 60 * 60 * 24 * 365.25));
};

const setMentionInfo = (collection, key) => {
  return collection.map(item => {
    const temp = { ...item };
    const manipulated = item[key].split("-");
    // eslint-disable-next-line prefer-destructuring
    temp.id = manipulated[0];
    // eslint-disable-next-line prefer-destructuring
    temp.type = manipulated[1];
    return temp;
  });
};

const truncateString = (string, numberOfCharacters) => {
  return string.length > numberOfCharacters ? `${string.substr(0, numberOfCharacters - 1)}...` : string;
};

const trimLastCharacter = (string, character) => {
  return string[string.length - 1] === character ? string.substring(0, string.length - 1) : string;
};

const getArrayFilteredByArray = (arrayToFilter, filteringArray) =>
  arrayToFilter.filter(value => !filteringArray.includes(value));

const getCurrentYear = () => {
  return new Date().getFullYear();
};

const debounce = (fn, delay) => {
  let timerId;
  return (...args) => {
    if (timerId) {
      clearTimeout(timerId);
    }
    timerId = setTimeout(() => {
      fn(...args);
      timerId = null;
    }, delay);
  };
};

const randomString = (length = 32, chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ") => {
  let result = "";
  for (let i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
  return result;
};

export {
  activeLink,
  adminRedirectionPath,
  buildMonthOptions,
  buildOptions,
  buildOptionsFromCollection,
  buildYearOptions,
  conditionalActive,
  constructPath,
  convertStatToString,
  daysOptions,
  decimalToCleanPercentage,
  defaultDobYear,
  formatDate,
  formatDateMonthYear,
  formatShortDateTime,
  formatDateTime,
  formatMoney,
  formatMonth,
  formatOnlyTime,
  formatRoles,
  formatTime,
  fullName,
  generateRangeArray,
  getAge,
  getAgeInYears,
  getArrayFilteredByArray,
  getBrowserInfo,
  getCurrentYear,
  getImage,
  getImageForInputDisplay,
  getMonthFromDate,
  getPathname,
  getTextFromHtmlString,
  getYearFromDate,
  isEmptyObject,
  mapCollectionToObjectQuery,
  mergeLabelsAndValues,
  monthsOptions,
  ordinalize,
  removeEmptyOrNull,
  rescueNil,
  setMentionInfo,
  staticOption,
  titleCase,
  trimLastCharacter,
  truncateString,
  userLocation,
  yearsOptions,
  debounce,
  randomString
};
