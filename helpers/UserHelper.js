import cloneDeep from "lodash/cloneDeep";
import each from "lodash/each";
import isEmpty from "lodash/isEmpty";

import { isEmptyObject, rescueNil } from "./GeneralHelper";

/**
 @module UserHelper
 @category Helpers
*/

const statesTobeUpdated = (props, state, key, whiteList) => {
  if (isEmptyObject(state[key]) && !isEmpty(props[key])) {
    const Obj = {};
    each(whiteList, item => (Obj[item] = props[key][item]));
    return { [key]: Obj };
  }
  return {};
};

const educationLevelChange = (value, obj, key = "student") => {
  let { gradesRequired, majorRequired } = obj.state;
  const student = obj.state[key];
  educationLevelIncludesSchool(value) ? (gradesRequired = false) : (gradesRequired = true);
  educationLevelIncludesGraduades(value) ? (majorRequired = true) : (majorRequired = false);
  obj.setState({
    gradesRequired,
    majorRequired,
    [key]: {
      ...student,
      educationLevel: value
    }
  });
};

const educationLevelIncludesSchool = value => {
  return ["Elementary School", "Middle School"].includes(value);
};

const educationLevelIncludesGraduades = value => {
  return ["Undergraduate School", "Graduate School"].includes(value);
};
const changeGPAError = (obj, value, key = "student") => {
  const parsedValue = parseFloat(value);
  let { gpaError } = obj.state;
  const student = obj.state[key];
  gpaError = "GPA must be between 0.00 to 4.00";
  if (parsedValue === 5.0 && student.gpaScale !== 5.0) {
    gpaError = "GPA must be between 0.00 to 5.00";
  } else if (isNaN(parsedValue)) {
    gpaError = "GPA is required";
  }
  obj.setState({
    gpaError,
    [key]: {
      ...student,
      gpaScale: value
    }
  });
};
const initStudentState = (key, student) => {
  return {
    [key]: cloneDeep(student),
    gradesRequired: !educationLevelIncludesSchool(rescueNil(student, "educationLevel")),
    majorRequired: educationLevelIncludesGraduades(rescueNil(student, "educationLevel"))
  };
};

const educationLevelOptions = [
  "",
  "Elementary School",
  "Middle School",
  "High School",
  "Undergraduate School",
  "Graduate School"
];

/**
 * @summary Retrieves whether the logged in user is the owner of a portfolio
 * @method
 * @param  {User} loggedInUser
 * @param  {User} userObject
 * @param  {Boolean} mockedUserBoolean
 * @returns {Boolean} Returns true or false if the user is an owner
 * @example
 * userHelper.isOwner(currentUser, user._id, mockExternalUser)
 */
const isOwner = (loggedInUser, userObject, mockedUserBoolean) => {
  if (mockedUserBoolean === true) {
    return false;
  }
  return loggedInUser && loggedInUser._id === userObject._id;
};

export {
  changeGPAError,
  educationLevelChange,
  educationLevelIncludesGraduades,
  educationLevelIncludesSchool,
  educationLevelOptions,
  initStudentState,
  isOwner,
  statesTobeUpdated
};
