import { getCurrentUserPlacement, getLeaderboard, getLeaderboardAction, getUserValue } from "./LeaderboardHelper";
import LEADERBOARD_TYPE from "../common/leaderboardTypes";
import {
  getEngagementsLeaderboard,
  getStreakLeaderboard,
  getViewsLeaderboard
} from "../store/actions/leaderboardActions";

jest.mock("../store/actions/leaderboardActions", () => ({
  getViewsLeaderboard: () => ({
    type: "mockGetViewsLeaderboard"
  }),
  getStreakLeaderboard: () => ({
    type: "mockGetStreakLeaderboard"
  }),
  getEngagementsLeaderboard: () => ({
    type: "mockGetEngagementsLeaderboard"
  })
}));

describe("LeaderboardHelper", () => {
  describe("getCurrentUserPlacement", () => {
    it("returns null if an invalid leaderboardType is passed", () => {
      const currentUser = {
        activeLeaderboard: {
          viewsLeaderboard: {
            placement: 23
          },
          streaksLeaderboard: {
            placement: 5
          },
          engagementsLeaderboard: {
            placement: 55
          }
        }
      };
      const expected = null;
      const actual = getCurrentUserPlacement(currentUser, "blah");
      expect(actual).toEqual(expected);
    });

    it("returns null if the user doesn't have a placement", () => {
      const currentUser = {
        activeLeaderboard: {
          viewsLeaderboard: {
            placement: 23
          },
          streaksLeaderboard: {},
          engagementsLeaderboard: {
            placement: 55
          }
        }
      };
      const expected = null;
      const actual = getCurrentUserPlacement(currentUser, LEADERBOARD_TYPE.STREAKS);
      expect(actual).toEqual(expected);
    });

    it("returns placement if exists and has valid leaderboard type", () => {
      const currentUser = {
        activeLeaderboard: {
          viewsLeaderboard: {
            placement: 23
          },
          streaksLeaderboard: {
            placement: 5
          },
          engagementsLeaderboard: {
            placement: 55
          }
        }
      };
      const expected = 5;
      const actual = getCurrentUserPlacement(currentUser, LEADERBOARD_TYPE.STREAKS);
      expect(actual).toEqual(expected);
    });
  });

  describe("getUserValue", () => {
    it("returns unranked if invalid leaderboard type passed", () => {
      const currentUser = {
        activeLeaderboard: {
          viewsLeaderboard: {
            valueAtTheTimeOfCreation: 12345
          },
          streaksLeaderboard: {
            valueAtTheTimeOfCreation: 123
          },
          engagementsLeaderboard: {
            valueAtTheTimeOfCreation: 12
          }
        }
      };
      const expected = "Unranked";
      const actual = getUserValue(currentUser, "invalid");
      expect(actual).toEqual(expected);
    });

    it("returns unranked if no value", () => {
      const currentUser = {
        activeLeaderboard: {
          viewsLeaderboard: {
            valueAtTheTimeOfCreation: null
          },
          streaksLeaderboard: {
            valueAtTheTimeOfCreation: 123
          },
          engagementsLeaderboard: {
            valueAtTheTimeOfCreation: 12
          }
        }
      };
      const expected = "Unranked";
      const actual = getUserValue(currentUser, LEADERBOARD_TYPE.VIEWS);
      expect(actual).toEqual(expected);
    });

    it("returns value if exists and valid leaderboard type", () => {
      const currentUser = {
        activeLeaderboard: {
          viewsLeaderboard: {
            valueAtTheTimeOfCreation: 12345
          },
          streaksLeaderboard: {
            valueAtTheTimeOfCreation: 123
          },
          engagementsLeaderboard: {
            valueAtTheTimeOfCreation: 12
          }
        }
      };
      const expected = 12345;
      const actual = getUserValue(currentUser, LEADERBOARD_TYPE.VIEWS);
      expect(actual).toEqual(expected);
    });
  });

  describe("getLeaderboard", () => {
    let expected;

    beforeEach(() => {
      expected = {
        leaderboard: {
          views: {
            docs: [{ _id: "111111" }, { _id: "222222" }]
          },
          streak: {
            docs: [{ _id: "333333" }, { _id: "444444" }]
          },
          engagements: {
            docs: [{ _id: "555555" }, { _id: "777777" }]
          }
        }
      };
    });

    it("returns views if leaderboardType is views", () => {
      const actual = getLeaderboard(LEADERBOARD_TYPE.VIEWS, expected.leaderboard);

      expect(expected.leaderboard.views).toEqual(actual);
    });

    it("returns streaks if leaderboardType is streaks", () => {
      const actual = getLeaderboard(LEADERBOARD_TYPE.STREAKS, expected.leaderboard);

      expect(expected.leaderboard.streak).toEqual(actual);
    });

    it("returns engagements if leaderboardType is engagements", () => {
      const actual = getLeaderboard(LEADERBOARD_TYPE.ENGAGEMENTS, expected.leaderboard);

      expect(expected.leaderboard.engagements).toEqual(actual);
    });

    it("returns leaderboard by default if type does not match", () => {
      const actual = getLeaderboard(LEADERBOARD_TYPE.ALL, expected.leaderboard);

      expect(expected.leaderboard).toEqual(actual);
    });
  });

  describe("getLeaderboardAction", () => {
    it("returns views action if leaderboard is views", () => {
      const expected = getViewsLeaderboard(1, 20);
      const actual = getLeaderboardAction(1, 20, LEADERBOARD_TYPE.VIEWS);

      expect(expected).toEqual(actual);
    });

    it("returns streak action if leaderboard is streak", () => {
      const expected = getStreakLeaderboard(2, 50);
      const actual = getLeaderboardAction(2, 50, LEADERBOARD_TYPE.STREAKS);

      expect(expected).toEqual(actual);
    });

    it("returns engagements action by default", () => {
      const expected = getEngagementsLeaderboard(5, 100);
      const actual = getLeaderboardAction(5, 100, LEADERBOARD_TYPE.ALL);

      expect(expected).toEqual(actual);
    });
  });
});
