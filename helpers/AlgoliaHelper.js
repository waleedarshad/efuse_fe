import algoliasearch from "algoliasearch";
import getConfig from "next/config";
import { print } from "graphql";

import { debounce } from "./GeneralHelper";
import { getToken } from "./Auth0Helper";

const { publicRuntimeConfig } = getConfig();
const { algoliaApplicationId, algoliaSearchApiKey, defaultUrl } = publicRuntimeConfig;

const INDEXES = {
  OPPORTUNITIES: "opportunities",
  ORGANIZATIONS: "organizations",
  NEWS: "learning_articles"
};

export const returnAlgoliaSearchClient = () => {
  return algoliasearch(algoliaApplicationId, algoliaSearchApiKey);
};

export const triggerDebouncedSegmentEvent = debounce((hits, eventName, indexName) => {
  const products = hits.map(hit => ({
    objectID: hit.objectID
  }));

  if (products.length > 0) {
    // mapping to Product List Viewed in Algolia Insights destination
    analytics.track(eventName, {
      products,
      index: indexName
    });
  }
}, 500);

export const getIndexName = indexType => {
  const { environment } = publicRuntimeConfig;
  const prefix = environment === "production" ? "production_" : "staging_";

  return `${prefix}${INDEXES[indexType]}`;
};

export const algoliaServerSideSearch = (query, variables, queryName) => {
  const graphqlEndpoint = defaultUrl ? `${defaultUrl}/graphql` : "http://localhost:5000/api/graphql";

  return fetch(graphqlEndpoint, {
    method: "post",
    headers: { "Content-Type": "application/json", Authorization: getToken() },
    body: JSON.stringify({ query: print(query), variables })
  }).then(async res => {
    const json = await res.json();

    return json.data[queryName];
  });
};
