/**
 @module SegmentHelper
 @category Helpers
*/

const initSegment = store => {
  if (store) {
    if (typeof window.analytics === "undefined") {
      console.error("Segment error: window.analytics is undefined");
    }

    const segment = window.analytics;
    const { currentUser } = store.getState().auth;

    if (currentUser && currentUser.id) {
      const traits = {
        email: currentUser.email,
        name: currentUser.name,
        avatar: currentUser.profilePicture?.url,
        createdAt: currentUser.createdAt,
        username: currentUser.username,
        pathway: currentUser.pathway,
        birthday: currentUser.dateOfBirth
      };

      segment.identify(currentUser.id, traits);
    }
  } else {
    console.error("Segment error: store is undefined");
  }
};

export { initSegment };
