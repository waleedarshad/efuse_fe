import sanitizeHtml from "sanitize-html";

const validateHtml = html =>
  sanitizeHtml(html, {
    allowedTags: [
      "h1",
      "h2",
      "h3",
      "h4",
      "h5",
      "h6",
      "p",
      "b",
      "i",
      "u",
      "strong",
      "a",
      "ul",
      "li",
      "ol",
      "div",
      "hr",
      "br",
      "nl",
      "span"
    ],
    allowedAttributes: {
      a: ["href", "target"]
    },
    selfClosing: ["br", "hr"],
    allowedSchemes: ["http", "https", "mailto"],
    allowedSchemesAppliedToAttributes: ["href"]
  });

const validateLearningPageHtml = html =>
  sanitizeHtml(html, {
    allowedTags: [
      "h1",
      "h2",
      "h3",
      "h4",
      "h5",
      "h6",
      "blockquote",
      "p",
      "a",
      "ul",
      "ol",
      "nl",
      "li",
      "b",
      "i",
      "strong",
      "em",
      "strike",
      "code",
      "hr",
      "br",
      "div",
      "table",
      "thead",
      "caption",
      "tbody",
      "tr",
      "th",
      "td",
      "pre",
      "iframe",
      "img",
      "figure",
      "figcaption"
    ],
    allowedAttributes: {
      a: ["href", "name", "target"],
      img: ["src"],
      iframe: ["src", "frameborder", "allow", "allowfullscreen"],
      "*": ["class", "width", "height"]
    },
    selfClosing: ["img", "br", "hr", "area", "base", "basefont", "input", "link", "meta"],
    allowedSchemes: ["http", "https", "mailto"],
    allowedSchemesAppliedToAttributes: ["href"]
  });

export { validateHtml, validateLearningPageHtml };
