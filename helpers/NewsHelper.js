import axios from "axios";
import { withCdn } from "../common/utils";

/**
 @module NewsHelper
 @category Helpers
*/

const returnNewsObject = learningArticle => {
  return {
    type: "News",
    imageUrl: learningArticle?.image?.url,
    title: learningArticle?.title,
    subTitle: learningArticle?.author?.name,
    redirectLink: learningArticle?.url,
    internalRedirect: true
  };
};

// This method helps allow only one embedded YouTube video to play at a time on the learning article

const onYouTubeIframeAPIReady = iframes => {
  var players = [];

  if (!iframes) {
    return;
  }

  Array.from(iframes).forEach((frame, index) => {
    if (frame.src.includes("www.youtube.com/")) {
      if (!frame.id) {
        frame.id = "embeddedvideoiframe" + index;
        if (!frame.src.includes("?enablejsapi=1")) {
          frame.src = frame.src + "?enablejsapi=1";
        }
      }

      players.push(
        new YT.Player(frame.id, {
          events: {
            onStateChange: function(event) {
              if (event.data == YT.PlayerState.PLAYING) {
                players.forEach((player, playerIndex) => {
                  if (player.getIframe().id != event.target.getIframe().id) {
                    player.pauseVideo();
                  }
                });
              }
            }
          }
        })
      );
    }
  });
};

const calculateReadingTime = text => {
  return Math.ceil(text.replace(/<\/?[^>]+(>|$)/g, " ").split(" ").length / 160);
};

const isArticleOwner = (currentUser, authorId) => {
  return currentUser?.id === authorId || currentUser?.roles?.includes("admin");
};

const isArticleOrganizationAdmin = (currentUserOrgs, authorId) => {
  return currentUserOrgs && currentUserOrgs.filter(org => org._id === authorId).length === 1;
};

const initArticleEditor = (ArticleEditor, updateBody, body, updateArticle) => {
  let updatedHtml;
  const editor = ArticleEditor("#entry", {
    plugins: ["reorder", "inlineformat"],
    grid: {
      classname: "row",
      columns: {
        classname: "col",
        prefix: "col-"
      }
    },
    subscribe: {
      "editor.content.change": formattedHTML => {
        updatedHtml = formattedHTML.get("html");
        updateBody(formattedHTML.get("html"));
      },
      "editor.blur": () => updateArticle(updatedHtml)
    },
    editor: {
      minHeight: "500px"
    },
    image: {
      upload(upload, data) {
        for (const key in data.files) {
          if (typeof data.files[key] === "object") {
            const formData = new FormData();
            formData.append("image", data.files[key]);

            axios({
              method: "POST",
              config: {
                headers: { "Content-Type": "multipart/form-data" }
              },
              url: "/learning/image-upload",
              data: formData
            }).then(response => {
              var response = {
                file: {
                  url: response.data.image.url,
                  id: ""
                }
              };

              upload.complete(response, data.e);
            });
          }
        }
      }
    }
  });
  if (editor) editor.editor.content.set(body);

  // load in plugins
  var tag = document.createElement("script");
  tag.async = true;
  tag.src = withCdn("/static/plugins/article-editor/plugins/reorder/reorder.js");
  document.body.appendChild(tag);

  var tag = document.createElement("script");
  tag.async = true;
  tag.src = withCdn("/static/plugins/article-editor/plugins/inlineformat/inlineformat.js");
  document.body.appendChild(tag);
};

export {
  returnNewsObject,
  calculateReadingTime,
  isArticleOwner,
  isArticleOrganizationAdmin,
  initArticleEditor,
  onYouTubeIframeAPIReady
};
