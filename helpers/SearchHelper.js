import axios from "axios";
import { GLOBAL_SEARCH_EVENTS } from "../common/analyticEvents";

const UserType = {
  USER: "user",
  ORG: "org"
};

const getSearchParams = (term, kind) => {
  let base = `?term=${term}`;

  if (kind) {
    base += `&kind=${kind}`;
  }

  return base;
};

const search = async term => {
  const endpoint = "/search";
  const params = getSearchParams(term, null);
  const url = `${endpoint}${params}`;

  return new Promise((resolve, reject) => {
    axios
      .get(url)
      .then(response => {
        analytics.track(GLOBAL_SEARCH_EVENTS.QUERY, {
          term
        });
        resolve(response);
      })
      .catch(error => reject(error));
  });
};

const searchForUsers = async term => {
  const endpoint = "/search";
  const params = getSearchParams(term, UserType.USER);
  const url = `${endpoint}${params}`;

  return new Promise((resolve, reject) => {
    axios
      .get(url)
      .then(response => {
        analytics.track(GLOBAL_SEARCH_EVENTS.QUERY, {
          term
        });
        resolve(response);
      })
      .catch(error => reject(error));
  });
};

const searchForOrganizations = async term => {
  const endpoint = "/search";
  const params = getSearchParams(term, UserType.ORG);
  const url = `${endpoint}${params}`;

  return new Promise((resolve, reject) => {
    axios
      .get(url)
      .then(response => {
        analytics.track(GLOBAL_SEARCH_EVENTS.QUERY, {
          term
        });
        resolve(response);
      })
      .catch(error => reject(error));
  });
};

export { search, searchForUsers, searchForOrganizations };
