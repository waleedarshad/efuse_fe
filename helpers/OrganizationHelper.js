/**
 @module OrganizationHelper
 @category Helpers
*/
import { getOrganizationUrl } from "./UrlHelper";

const loggedInUserIsOwner = (currentUser, organization) => currentUser && currentUser?.id === organization?.user;

const loggedInUserIsCaptain = (currentUser, organization) =>
  currentUser && organization?.captains && organization?.captains.includes(currentUser?.id);

const getOwnerAndCaptain = (currentUser, organization) => {
  return {
    owner: loggedInUserIsOwner(currentUser, organization),
    captain: loggedInUserIsCaptain(currentUser, organization)
  };
};

const getMemberRole = (memberId, owner, captains) => {
  if (memberId === owner) {
    return "Owner";
  }
  if (captains?.includes(memberId)) {
    return "Captain";
  }
  return "Member";
};

const returnOrganizationObject = organization => {
  return {
    type: "Organizations",
    imageUrl: organization?.headerImage?.url,
    description: organization?.description,
    title: organization?.name,
    subTitle: organization?.organizationType,
    stat: `${organization?.followersCount} Followers`,
    redirectLink: getOrganizationUrl(organization),
    internalRedirect: true
  };
};

/**
 * @summary Retrieves an array of fields the organization has completed/filled out
 * @method
 * @param  {organization} organization
 * @returns {Array} Returns an array of string values that exist on the organization object
 * @example
 * OrganizationHelper.getOrganizationFilledFields(organization)
 */
const getOrganizationFilledFields = org => {
  const organizationFields = [];
  if (org?.description?.length > 0) {
    organizationFields.push("description");
  }
  if (org?.about?.length > 0) {
    organizationFields.push("about");
  }
  if (org?.videoCarousel?.length > 0) {
    organizationFields.push("videoCarousel");
  }
  if (org?.playerCards?.length > 0) {
    organizationFields.push("playerCards");
  }
  if (org?.promoVideo?.title?.length > 0) {
    organizationFields.push("promoVideo");
  }
  if (org?.honors?.length > 0) {
    organizationFields.push("honors");
  }
  if (org?.events?.length > 0) {
    organizationFields.push("events");
  }
  if (org?.description !== "") {
    organizationFields.push("description");
  }

  return organizationFields;
};

export {
  loggedInUserIsOwner,
  loggedInUserIsCaptain,
  returnOrganizationObject,
  getOwnerAndCaptain,
  getMemberRole,
  getOrganizationFilledFields
};
