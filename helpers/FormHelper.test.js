import { generateCustomUrl } from "./FormHelper";

describe("FormHelper", () => {
  describe("generatesCustomUrl", () => {
    it("returns a string that replaces spaces with hyphens", () => {
      const expected = "my-url";
      const actual = generateCustomUrl("my url");

      expect(actual).toEqual(expected);
    });

    it("returns a string without special characters", () => {
      const expected = "my-amazing-url";
      const actual = generateCustomUrl("my ~!@#$%^&*()-+={}[]/|:;<>,.?\\(amazing) url");

      expect(actual).toEqual(expected);
    });

    it("returns a string without filler words", () => {
      const expected = "boys-girls-scholarship";
      const actual = generateCustomUrl("it is a boys and the girl's scholarship");

      expect(actual).toEqual(expected);
    });

    it("returns a string up to max number of characters", () => {
      const expected = "amazing-scholarship-wome";
      const actual = generateCustomUrl("this is an amazing scholarship for women in gaming");

      expect(actual).toEqual(expected);
    });

    it("makes sure not to end in hyphen", () => {
      const expected = "aaa-aaaaaa-aaaaaaaaaaaa";
      const actual = generateCustomUrl("aaa aaaaaa aaaaaaaaaaaa a");

      expect(actual).toEqual(expected);
    });

    it("returns a string all in lowercase", () => {
      const expected = "1-hour-warzone-session-w";
      const actual = generateCustomUrl("1 Hour Warzone Session with Elaine");

      expect(actual).toEqual(expected);
    });
  });
});
