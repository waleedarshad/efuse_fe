import React from "react";
import PortfolioScores from "../components/Portfolio/PortfolioScores/PortfolioScores";
import UserAttributes from "../components/UserAttributes/UserAttributes";

export const getDefaultCarouselSettings = (prevArrow, nextArrow, items) => {
  return {
    dots: false,
    infinite: items > 3,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 3,
    swipeToSlide: true,
    prevArrow,
    nextArrow,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          infinite: items > 3,
          swipeToSlide: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          swipeToSlide: true,
          slidesToShow: 1,
          slidesToScroll: 1,
          initialSlide: 1,
          arrows: false,
          infinite: items > 3
        }
      }
    ]
  };
};

export const getUserAttributeSettings = (prevArrow, nextArrow) => {
  return {
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    swipeToSlide: true,
    prevArrow,
    nextArrow,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: false,
          swipeToSlide: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          swipeToSlide: true,
          slidesToShow: 1,
          slidesToScroll: 1,
          initialSlide: 1,
          arrows: false,
          infinite: false
        }
      }
    ]
  };
};

export const getSimpleCarouselSettings = (prevArrow, nextArrow, customStyle) => {
  return {
    // we need to set a min width for the talent card component (else the carousel does not load for modals/hidden events)
    className: customStyle,
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    nextArrow,
    prevArrow
  };
};

export const getPortfolioHeaderCarouselItems = (stats, streak, user, alignRight) => {
  const items = [];
  if (user.publicTraitsMotivations) {
    items.push(<PortfolioScores key={0} stats={stats} streak={streak} />);
    if (user.traits && user.traits.length > 0) {
      items.push(<UserAttributes key={1} userAttributes={user.traits} />);
    }
    if (user.motivations && user.motivations.length > 0) {
      items.push(<UserAttributes key={2} userAttributes={user.motivations} />);
    }
  } else {
    items.push(<PortfolioScores key={0} stats={stats} streak={streak} alignRight={alignRight} />);
  }
  return items;
};
