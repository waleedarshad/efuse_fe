import { store } from "react-notifications-component";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck, faExclamationTriangle, faTimesOctagon, faInfoCircle } from "@fortawesome/pro-regular-svg-icons";
import { faFire } from "@fortawesome/pro-light-svg-icons";

import flashMessages from "../static/data/flash";

// /**
//  @module FlashHelper
//  @category Helpers
// */
const notification = {
  insert: "top",
  container: "top-right",
  animationIn: ["animated", "fadeIn"],
  animationOut: ["animated", "fadeOut"],
  dismiss: {
    duration: 3000
  },
  showIcon: true
};
/**
 * @param {*} key
 */
export const setFlash = key => {
  const flash = flashMessages[key];
  sendNotification(flash.message, flash.type, flash.title);
};

/**
 *
 * @param {*} message
 * @param {*} type
 * @param {*} title
 */

export const sendNotification = (message, type, title) => {
  let icon;
  switch (type) {
    case "success":
      icon = faCheck;
      break;
    case "warning":
      icon = faExclamationTriangle;
      break;
    case "danger":
      icon = faTimesOctagon;
      break;
    case "hype":
      icon = faFire;
      break;
    default:
      icon = faInfoCircle;
      break;
  }
  store.addNotification({
    ...notification,
    content: (
      <div className={`notification-custom-${type}`}>
        <div className={`notification-${type}-icon text-center rounded-circle`}>
          <FontAwesomeIcon className="notification-custom-icon" icon={icon} />
        </div>
        <div className="notification-custom-content">
          <p className="notification-title">{title}</p>
          <p className="notification-message" dangerouslySetInnerHTML={{ __html: message }} />
        </div>
      </div>
    )
  });
};
