import OneLinkUrlGenerator from "./AppsFlyerOneLinkSmartScript";

/**
 @module AppsFlyerHelper
 @category Helpers
*/

export const LINKS = {
  DOWNLOAD_APP: "https://efuse.onelink.me/OZoN/fa270ee4"
};

/**
 * @summary Generate a new URL with mapped UTM params for mobile attribution
 * @method
 * @param  {string} originalOneLink
 * @returns {string} New onelink url with mapped values
 * @example
 * const url = AppsFlyerHelper.getOneLink(LINKS.DOWNLOAD_APP);
 */
export const getOneLink = (originalOneLink: string): string => {
  let mappedUrl;

  // must be done client side because OneLinkUrlGenerator uses `window` object
  if (typeof window !== "undefined") {
    const onelinkGenerator = new OneLinkUrlGenerator({
      oneLinkURL: originalOneLink,
      pidKeysList: ["af_pid", "utm_source"],
      pidOverrideList: {
        Google: "googleadwords_int",
        Facebook: "Facebook Ads",
        Snapchat: "snapchat_int",
        Twitter: "twitter"
      },
      campaignKeysList: ["af_c", "utm_campaign"],
      gclIdParam: "af_sub5"
    });
    mappedUrl = onelinkGenerator.generateUrl();
  }

  return mappedUrl || originalOneLink;
};
