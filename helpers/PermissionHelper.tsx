import { ApolloClient, ApolloQueryResult, NormalizedCacheObject } from "@apollo/client";
import { PermissionActionEnum } from "../enums";
import { ENTITY_HAS_PERMISSION, ENTITY_HAS_ROLE } from "../graphql/permissions/PermissionQuery";
import { HasRoleInput, PermissionEntityBody, ResourceOwnerBody } from "../interfaces";
import { setErrors } from "../store/actions/errorActions";
import PermissionResourceType from "../types/permission-resource.type";

interface IEntityHasPermission {
  entityHasPermission: boolean;
}

interface IEntityHasRole {
  entityHasRole: boolean;
}

/**
 * This should be used server side when doing a check for an entities permissions.
 * This was setup to try and standardize the server side permission query.
 *
 * If this is updated then "usePermission.tsx" may need updated.
 *
 * @param apolloClient - The server sides apollo client to be used.
 * @param action - In what way is the resource being used.
 * @param resource - What resource is being accessed. (This could be to change an organizations name.)
 * @param entityBody - ID of entity and what the enity. (Could be ID of a user). If not set then will use authenticated user.
 * @param resourceOwnerBody - Id of resource owner. This could be an Organization ID. Not required for every resource.
 */
export const entityHasPermission = async (
  apolloClient: ApolloClient<NormalizedCacheObject>,
  action: PermissionActionEnum,
  resource: PermissionResourceType,
  permissionEntityBody?: PermissionEntityBody,
  resourceOwnerBody?: ResourceOwnerBody
): Promise<boolean> => {
  try {
    const response: ApolloQueryResult<IEntityHasPermission> = await apolloClient.query({
      query: ENTITY_HAS_PERMISSION,
      variables: {
        params: { action, resource, resourceOwnerData: resourceOwnerBody, entityData: permissionEntityBody }
      },
      fetchPolicy: "no-cache"
    });

    return response?.data?.entityHasPermission ?? false;
  } catch (error) {
    setErrors(error.response.data);

    return false;
  }
};

/**
 * This should be used server side when doing a check for an entities roles.
 * This was setup to try and standardize the server side role query.
 *
 * If this is updated then "useRole.tsx" may need updated.
 *
 * @param hasRoleInput Many of the variables in this are optional.
 * If entityId is not set then the authorized user will be used.
 * For roleValues and roleIds one of them needs to be set, but only one of them needs to be. If both are set, then roleIds will be used.
 */
export const entityHasRole = async (
  apolloClient: ApolloClient<NormalizedCacheObject>,
  hasRoleInput: HasRoleInput
): Promise<boolean> => {
  try {
    const { entityId, roleIds, roleValues } = hasRoleInput;

    const response: ApolloQueryResult<IEntityHasRole> = await apolloClient.query({
      query: ENTITY_HAS_ROLE,
      variables: {
        params: { entityId, roleIds, roleValues }
      },
      fetchPolicy: "no-cache"
    });

    return response?.data?.entityHasRole ?? false;
  } catch (error) {
    setErrors(error.response.data);

    return false;
  }
};
