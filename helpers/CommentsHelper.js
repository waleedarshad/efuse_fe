import cloneDeep from "lodash/cloneDeep";
import isEmpty from "lodash/isEmpty";
import uniqBy from "lodash/uniqBy";

/**
 @module CommentsHelper
 @category Helpers
*/

const STATES = {
  homeFeed: {
    collection: "homeFeed",
    expandedObject: "post"
  },
  loungeFeed: {
    collection: "docs",
    expandedObject: "post"
  }
};

const enableCommentsCreation = (state, action, type) => {
  const { targetedObjectNotFound, clonedCollection, targetedObject, expandedPostNotFound } = getObjectFromCollection(
    state,
    action,
    type
  );
  if (targetedObjectNotFound || !expandedPostNotFound) {
    return {};
  }
  analytics.track(`COMMENT_FEED_${!existingValue ? "OPEN" : "CLOSE"}`);
  const existingValue = targetedObject.associatedFeed.displayComments || false;
  targetedObject.associatedFeed.displayComments = !existingValue;
  return { [collectionKey(type)]: clonedCollection };
};

const addCommentsToFeed = (state, action, type) => {
  const {
    targetedObjectNotFound,
    clonedCollection,
    targetedObject,
    expandedPostNotFound,
    clonedExpandedPost
  } = getObjectFromCollection(state, action, type);
  if (targetedObjectNotFound && expandedPostNotFound) {
    return {};
  }
  if (!targetedObjectNotFound) {
    feedObjectLiberatedWithComments(targetedObject, action);
  }
  if (!expandedPostNotFound) {
    feedObjectLiberatedWithComments(clonedExpandedPost, action);
  }
  return responseObject(type, clonedCollection, clonedExpandedPost);
};

const removeComment = (state, action, type) => {
  const {
    targetedObjectNotFound,
    clonedCollection,
    targetedObject,
    expandedPostNotFound,
    clonedExpandedPost
  } = getObjectFromCollection(state, action, type, "feedId");
  if (targetedObjectNotFound && expandedPostNotFound) {
    return {};
  }
  if (!targetedObjectNotFound) {
    removeCommentFromObject(targetedObject, action);
  }
  if (!expandedPostNotFound) {
    removeCommentFromObject(clonedExpandedPost, action);
  }
  return responseObject(type, clonedCollection, clonedExpandedPost);
};

const addCommentToFeed = (state, action, type) => {
  const {
    targetedObjectNotFound,
    clonedCollection,
    targetedObject,
    expandedPostNotFound,
    clonedExpandedPost
  } = getObjectFromCollection(state, action, type);
  if (targetedObjectNotFound && expandedPostNotFound) {
    return {};
  }
  if (!targetedObjectNotFound) {
    updateCommentDocs(targetedObject, action);
  }
  if (!expandedPostNotFound) {
    updateCommentDocs(clonedExpandedPost, action);
  }
  return responseObject(type, clonedCollection, clonedExpandedPost);
};

const editComment = (state, action, type) => {
  const {
    targetedObjectNotFound,
    clonedCollection,
    targetedObject,
    expandedPostNotFound,
    clonedExpandedPost
  } = getObjectFromCollection(state, action, type, "feedId");
  if (targetedObjectNotFound && expandedPostNotFound) {
    return {};
  }
  if (!targetedObjectNotFound) {
    updateEditedComment(targetedObject, action);
  }
  if (!expandedPostNotFound) {
    updateEditedComment(clonedExpandedPost, action);
  }
  return responseObject(type, clonedCollection, clonedExpandedPost);
};

const instantCommentsLike = (state, action, type) => {
  const {
    targetedObjectNotFound,
    clonedCollection,
    targetedObject,
    expandedPostNotFound,
    clonedExpandedPost
  } = getObjectFromCollection(state, action, type);
  if (targetedObjectNotFound && expandedPostNotFound) {
    return {};
  }
  if (!targetedObjectNotFound) {
    updatedCommentFeedObject(targetedObject, action);
  }
  if (!expandedPostNotFound) {
    updatedCommentFeedObject(clonedExpandedPost, action);
  }
  return responseObject(type, clonedCollection, clonedExpandedPost);
};

const enableReply = (state, action, type) => {
  const {
    targetedObjectNotFound,
    clonedCollection,
    targetedObject,
    expandedPostNotFound,
    clonedExpandedPost
  } = getObjectFromCollection(state, action, type, "feedId");
  if (targetedObjectNotFound && expandedPostNotFound) {
    return {};
  }
  if (!targetedObjectNotFound) {
    enableReplyOnThread(targetedObject, action);
  }
  if (!expandedPostNotFound) {
    enableReplyOnThread(clonedExpandedPost, action);
  }
  return responseObject(type, clonedCollection, clonedExpandedPost);
};

const disableViewMore = (state, action, type) => {
  const {
    targetedObjectNotFound,
    clonedCollection,
    targetedObject,
    expandedPostNotFound,
    clonedExpandedPost
  } = getObjectFromCollection(state, action, type, "feedId");
  if (targetedObjectNotFound && expandedPostNotFound) {
    return {};
  }
  if (!targetedObjectNotFound) {
    disableViewMoreOnThread(targetedObject, action);
  }
  if (!expandedPostNotFound) {
    disableViewMoreOnThread(clonedExpandedPost, action);
  }
  return responseObject(type, clonedCollection, clonedExpandedPost);
};

const loadMoreThreadComments = (state, action, type) => {
  const {
    targetedObjectNotFound,
    clonedCollection,
    targetedObject,
    expandedPostNotFound,
    clonedExpandedPost
  } = getObjectFromCollection(state, action, type, "feedId");
  if (targetedObjectNotFound && expandedPostNotFound) {
    return {};
  }
  if (!targetedObjectNotFound) {
    liberateThreadWithMoreComments(targetedObject, action);
  }
  if (!expandedPostNotFound) {
    liberateThreadWithMoreComments(clonedExpandedPost, action);
  }
  return responseObject(type, clonedCollection, clonedExpandedPost);
};

// Private Methods

const liberateThreadWithMoreComments = (targetedFeed, action) => {
  const targetedComment = getTargetedCommentFromParent(targetedFeed, action);
  if (targetedComment) {
    const { docs, ...pagination } = action.thread;
    targetedComment.thread = {
      docs: uniqBy([...targetedComment.thread.docs, ...docs], "_id"),
      ...pagination
    };
    targetedComment.disableViewMore = false;
  }
};

const disableViewMoreOnThread = (targetedFeed, action) => {
  const targetedComment = getTargetedCommentFromParent(targetedFeed, action);
  if (targetedComment) {
    targetedComment.disableViewMore = true;
  }
};

const getTargetedCommentFromParent = (targetedFeed, action) => {
  const feedObject = targetedFeed.associatedFeed;
  const { parent } = action;
  const { commentDocs } = feedObject;
  const targetedComment = commentDocs.find(comment => comment._id === parent);
  return targetedComment;
};

const enableReplyOnThread = (targetedFeed, action) => {
  const targetedComment = getTargetedCommentFromParent(targetedFeed, action);
  if (targetedComment) {
    targetedComment.enableReply = true;
  }
};

const updatedCommentFeedObject = (targetedFeed, action) => {
  const feedObject = targetedFeed.associatedFeed;
  const { commentId } = action;
  const { commentDocs } = feedObject;
  const targetedComment = commentDocs.find(comment => comment._id === commentId);
  if (targetedComment) {
    handleLikeUnlikeScenario(targetedComment, action);
  } else {
    commentDocs.filter(comment => {
      const { thread } = comment;
      if (thread) {
        const targetedComment = comment.thread.docs.find(doc => doc._id === commentId);
        if (targetedComment) {
          handleLikeUnlikeScenario(targetedComment, action);
          return true;
        }
      }
    });
  }
};

const handleLikeUnlikeScenario = (targetedComment, action) => {
  const { isLiked, like, value } = action;
  isLiked ? (targetedComment.like = { likeable: targetedComment._id, ...like }) : delete targetedComment.like;
  targetedComment.likes += value;
};

const updateEditedComment = (targetedFeed, action) => {
  const { payload, updated, commentData, commentId } = action;
  const params = {
    updated: updated || payload?.updated,
    commentId: commentId || payload?.commentId,
    commentData: commentData || payload?.commentData
  };
  const feedObject = targetedFeed.associatedFeed;
  const { commentDocs } = feedObject;
  const targetedComment = commentDocs.find(comment => comment._id === params.commentId);
  if (targetedComment) {
    updateOrEdit(params, targetedComment);
  } else {
    commentDocs.filter(comment => {
      const { thread } = comment;
      if (thread) {
        const targetedComment = comment.thread.docs.find(doc => doc._id === commentId);
        if (targetedComment) {
          updateOrEdit(params, targetedComment);
          return true;
        }
      }
    });
  }
};

const updateOrEdit = (params, targetedComment) => {
  if (params.updated) {
    analytics.track("COMMENT_EDIT", {
      commentId: params.commentId
    });
    targetedComment.enableEdit = false;
    targetedComment.content = params.commentData.content;
    targetedComment.mentions = params.commentData.mentions;
    const { file, removeMedia } = params.commentData;
    if (removeMedia && isEmpty(file)) {
      delete targetedComment.associatedMedia;
    } else if (!isEmpty(file)) {
      targetedComment.associatedMedia = { file };
    }
  } else {
    targetedComment.enableEdit = !targetedComment.enableEdit;
  }
};

const updateCommentDocs = (targetedFeed, action) => {
  const feedObject = targetedFeed.associatedFeed;
  feedObject.comments += 1;
  const { commentDocs } = feedObject;
  const comment = action.comment || action.payload.comment;
  if (!isEmpty(comment.parent)) {
    if (commentDocs) {
      const parentComment = commentDocs.find(doc => doc._id === comment.parent);
      if (parentComment) {
        const { thread } = parentComment;
        thread
          ? (thread.docs = uniqBy([...thread.docs, comment], "_id"))
          : (parentComment.thread = {
              docs: [comment],
              hasNextPage: false,
              page: 1,
              nextPage: 2
            });
      }
    }
  } else {
    commentDocs
      ? (feedObject.commentDocs = uniqBy([comment, ...commentDocs], "_id"))
      : (feedObject.commentDocs = [comment]);
  }
  return feedObject;
};

const removeCommentFromObject = (targetedFeed, action) => {
  const feedObject = targetedFeed.associatedFeed;
  feedObject.comments -= 1;
  const commentId = action.commentId || action.payload.commentId;
  const { commentDocs } = feedObject;
  if (action.singleParentDeleted) {
    const updatedCommentDocs = commentDocs.filter(comment => comment._id != commentId);
    feedObject.commentDocs = updatedCommentDocs;
    return feedObject;
  }
  const targetedComment = commentDocs.find(comment => comment._id === commentId);
  if (targetedComment) {
    handleCommentDeletion(targetedComment);
  } else {
    commentDocs.filter(comment => {
      const { thread } = comment;
      if (thread) {
        const targetedComment = thread.docs.find(doc => doc._id === commentId);
        if (targetedComment) {
          handleCommentDeletion(targetedComment);
          return true;
        }
      }
    });
  }

  return feedObject;
};

const handleCommentDeletion = targetedComment => {
  targetedComment.deleted = true;
  delete targetedComment.content;
  delete targetedComment.mentions;
  delete targetedComment.likes;
  delete targetedComment.associatedMedia;
  delete targetedComment.giphy;
  delete targetedComment.associatedGiphy;
  delete targetedComment.like;
};

const feedObjectLiberatedWithComments = (targetedFeed, action) => {
  const feed = targetedFeed.associatedFeed;
  const { commentDocs } = feed;
  feed.commentDocs = commentDocs ? uniqBy([...commentDocs, ...action.comments], "_id") : [...action.comments];
  feed.commentPagination = action.pagination;
  return targetedFeed;
};

const getObjectFromCollection = (state, action, type, identifierKey = "homeFeedId") => {
  const collection = state[collectionKey(type)];
  const expandedPost = state[expandedObjectKey(type)];
  const clonedCollection = cloneDeep(collection);
  let clonedExpandedPost = {};
  const expandedPostNotFound = isEmpty(expandedPost);
  let targetedObject = null;
  if (identifierKey === "feedId") {
    const identifier = action.feedId || action.payload?.feedId;
    targetedObject = clonedCollection.find(obj => obj.feed === identifier);
    if (!expandedPostNotFound && expandedPost.feed === identifier) {
      clonedExpandedPost = cloneDeep(expandedPost);
    }
  } else {
    const identifier = action.homeFeedId || action.payload?.homeFeedId;
    targetedObject = clonedCollection.find(obj => obj._id === identifier);
    if (!expandedPostNotFound && expandedPost._id === identifier) {
      clonedExpandedPost = cloneDeep(expandedPost);
    }
  }
  return {
    clonedCollection,
    targetedObject,
    clonedExpandedPost,
    targetedObjectNotFound: !targetedObject,
    expandedPostNotFound: isEmpty(clonedExpandedPost)
  };
};

const collectionKey = type => STATES[type].collection;
const expandedObjectKey = type => STATES[type].expandedObject;
const responseObject = (type, clonedCollection, clonedExpandedPost) => {
  const response = {
    [expandedObjectKey(type)]: clonedExpandedPost,
    [collectionKey(type)]: clonedCollection
  };
  if (isEmpty(clonedExpandedPost)) delete response[expandedObjectKey(type)];
  return response;
};

export {
  enableCommentsCreation,
  addCommentsToFeed,
  removeComment,
  addCommentToFeed,
  editComment,
  instantCommentsLike,
  enableReply,
  disableViewMore,
  loadMoreThreadComments
};
