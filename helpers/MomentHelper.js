import moment from "moment";

/**
 * @summary Converts a date value to a moment date
 * @method
 * @param  {Date} date
 * @returns {moment} Returns a moment date value
 * @example
 * MomentHelper.toMoment(date)
 */
const toMoment = date => moment(new Date(date));

/**
 * @summary Check if the given date is compatible for the datepicker (moment date)
 * @method
 * @param  {Date} date
 * @returns {moment} Returns a moment date value
 * @example
 * MomentHelper.compatibleForDatepicker(this.props.date)
 */
const compatibleForDatepicker = date => (date ? toMoment(date) : null);

export { toMoment, compatibleForDatepicker };
