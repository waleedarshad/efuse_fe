/* eslint-disable no-console */

import Cookies from "js-cookie";
import getConfig from "next/config";
import { randomString } from "./GeneralHelper";
import COOKIES from "../common/cookies";
import { toggleFeatures } from "../store/actions/flagSmithActions";

/**
 * @module FlagSmithHelper
 * @category Helpers
 */

const { publicRuntimeConfig } = getConfig();

const initializeFlagSmith = async (store, flagSmithDataSSR, demoState) => {
  const flagSmith = (await import("flagsmith")).default;

  const { bulletTrainEnvironment, bulletTrainApi } = publicRuntimeConfig;
  const { currentUser } = store.getState().auth;

  // prepare to set identify
  let identifyId;

  if (currentUser?.id) {
    if (demoState === COOKIES.SHOW_PUBLIC_SITE) {
      // employee setting traits to public view
      identifyId = `${currentUser.id}--public`;
    } else if (demoState === COOKIES.SHOW_LATEST_FEATURES) {
      // logged in user
      identifyId = currentUser.id;
    }
  } else {
    const efuseAnonymousIdCookie = Cookies.get("efuse_anonymous_id");
    if (efuseAnonymousIdCookie) {
      // existing anonymous user from cache
      identifyId = `anonymous-${efuseAnonymousIdCookie}`;
    } else {
      // new anonymous user
      const random = randomString();
      Cookies.set("efuse_anonymous_id", random, { expires: 365 });
      identifyId = `anonymous-${random}`;
    }
  }

  flagSmith.identify(identifyId).catch(error => console.error(error));

  // initialize flagsmith
  flagSmith
    .init({
      environmentID: bulletTrainEnvironment,
      api: bulletTrainApi,
      state: {
        identity: identifyId,
        flags:
          flagSmithDataSSR?.flags &&
          Object.fromEntries(
            flagSmithDataSSR.flags.map(flag => [
              flag.feature.name,
              { enabled: flag.enabled, value: flag.feature_state_value }
            ])
          )
      },
      onChange: () => {
        const state = flagSmith.getState();
        const formattedFlagsForRedux = {};

        // format flags into a clean object
        Object.entries(state.flags).forEach(([flagName, flag]) => {
          const flagValue = flag.value || flag.enabled;
          formattedFlagsForRedux[flagName] = flagValue;
        });

        // store all flags in redux
        store.dispatch(toggleFeatures(formattedFlagsForRedux));
      },
      onError: data => {
        console.error(data);
      }
    })
    .catch(error => console.error(error));
};

// eslint-disable-next-line import/prefer-default-export
export { initializeFlagSmith };

/* eslint-enable no-console */
