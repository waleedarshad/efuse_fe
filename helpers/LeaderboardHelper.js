import LEADERBOARD_TYPE from "../common/leaderboardTypes";
import {
  getEngagementsLeaderboard,
  getStreakLeaderboard,
  getViewsLeaderboard
} from "../store/actions/leaderboardActions";

const getCurrentUserPlacement = (currentUser, leaderboardType) => {
  let placement = null;
  if (leaderboardType === LEADERBOARD_TYPE.VIEWS) {
    if (currentUser?.activeLeaderboard?.viewsLeaderboard?.placement) {
      placement = currentUser.activeLeaderboard.viewsLeaderboard.placement;
    }
  }
  if (leaderboardType === LEADERBOARD_TYPE.STREAKS) {
    if (currentUser?.activeLeaderboard?.streaksLeaderboard?.placement) {
      placement = currentUser.activeLeaderboard.streaksLeaderboard.placement;
    }
  }
  if (leaderboardType === LEADERBOARD_TYPE.ENGAGEMENTS) {
    if (currentUser?.activeLeaderboard?.engagementsLeaderboard?.placement) {
      placement = currentUser.activeLeaderboard.engagementsLeaderboard.placement;
    }
  }
  return placement;
};

const getUserValue = (currentUser, leaderboardType) => {
  let value = "Unranked";
  if (leaderboardType === LEADERBOARD_TYPE.VIEWS) {
    if (currentUser?.activeLeaderboard?.viewsLeaderboard?.valueAtTheTimeOfCreation) {
      value = currentUser.activeLeaderboard.viewsLeaderboard.valueAtTheTimeOfCreation;
    }
  }
  if (leaderboardType === LEADERBOARD_TYPE.STREAKS) {
    if (currentUser?.activeLeaderboard?.streaksLeaderboard?.valueAtTheTimeOfCreation) {
      value = currentUser.activeLeaderboard.streaksLeaderboard.valueAtTheTimeOfCreation;
    }
  }
  if (leaderboardType === LEADERBOARD_TYPE.ENGAGEMENTS) {
    if (currentUser?.activeLeaderboard?.engagementsLeaderboard?.valueAtTheTimeOfCreation) {
      value = currentUser.activeLeaderboard.engagementsLeaderboard.valueAtTheTimeOfCreation;
    }
  }
  return value;
};

const getLeaderboard = (leaderboardType, leaderboard) => {
  switch (leaderboardType) {
    case LEADERBOARD_TYPE.VIEWS:
      return leaderboard.views;
    case LEADERBOARD_TYPE.STREAKS:
      return leaderboard.streak;
    case LEADERBOARD_TYPE.ENGAGEMENTS:
      return leaderboard.engagements;
    default:
      return leaderboard;
  }
};

const getLeaderboardAction = (page, maxPages, leaderboardType) => {
  if (leaderboardType === LEADERBOARD_TYPE.VIEWS) {
    return getViewsLeaderboard(page, maxPages);
  }
  if (leaderboardType === LEADERBOARD_TYPE.STREAKS) {
    return getStreakLeaderboard(page, maxPages);
  }
  return getEngagementsLeaderboard(page, maxPages);
};

export { getCurrentUserPlacement, getUserValue, getLeaderboard, getLeaderboardAction };
