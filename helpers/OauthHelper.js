import Router from "next/router";

/**
 @module OauthHelper
 @category Helpers
*/

const redirect = (endPoint, client_id, scope, redirect_uri) => {
  Router.push(`/auth/${endPoint}?client_id=${client_id}&redirect_uri=${redirect_uri}&scope=${scope}`);
};

export { redirect };
