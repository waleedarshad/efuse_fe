import { filterOutTwitchUrls } from "./PostHelper";

describe("PostHelper", () => {
  describe("filterOutTwitchUrls", () => {
    it("filters out twitch urls from list of urls", () => {
      expect(
        filterOutTwitchUrls(["https://twitch.tv/gaming", "https://www.twitch.tv/mypage", "https://not-twitch.com"])
      ).toEqual(["https://not-twitch.com"]);
    });
  });
});
