import cloneDeep from "lodash/cloneDeep";
import findIndex from "lodash/findIndex";
import isEmpty from "lodash/isEmpty";
import isEqual from "lodash/isEqual";
import startCase from "lodash/startCase";
import Router from "next/router";

/**
 @module FilterHelper
 @category Helpers
*/

const applyFilters = (
  key,
  value,
  label,
  filtersApplied,
  method,
  remove = false,
  id = "",
  regex = false,
  forceRedirectToOpportunity = false,
  limit = 10
) => {
  const updatedFilters = cloneDeep(filtersApplied);
  const index = findIndex(updatedFilters, filter => filter.key === key);
  if (remove || isEmpty(value)) {
    updatedFilters.splice(index, 1);
  } else {
    const object = { key, value, label };
    index === -1 ? updatedFilters.push(object) : (updatedFilters[index] = object);
  }
  let searchObject = {};

  updatedFilters.forEach(obj => {
    const splittedKeys = obj.key.split(".");
    switch (splittedKeys.length) {
      case 4:
        searchObject = {
          ...searchObject,
          [`${splittedKeys[0]}.${[splittedKeys[1]]}`]: applyRegex(regex, obj.value)
        };
        break;
      case 3:
        searchObject = {
          ...searchObject,
          [splittedKeys[0]]: {
            [splittedKeys[1]]: obj.value[0],
            [splittedKeys[2]]: obj.value[1]
          }
        };
        break;
      case 2:
        searchObject = {
          ...searchObject,
          [splittedKeys[0]]: {
            [splittedKeys[1]]: applyRegex(regex, obj.value)
          }
        };
        break;
      default:
        searchObject = {
          ...searchObject,
          [obj.key]: applyRegex(regex, obj.value)
        };
        break;
    }
  });
  const defaultParams = [searchObject, updatedFilters, limit];
  const params = id ? [id, ...defaultParams] : defaultParams;

  method(...params);

  // need to do this to ensure the infinite scroll event isn't triggered
  if (forceRedirectToOpportunity) {
    window.scrollTo(0, 0);
    Router.push("/opportunities/all");
  }
};

const updateStateFromProps = (props, state) => {
  return isEqual(props.searchFilters, state.searchFilters) ? {} : { searchFilters: props.searchFilters };
};

const onFilterChange = event => {
  const { name, value } = event.target;
  let label = value === "true" || value === "false" ? `${startCase(name.split("?")[0])}: ${value}` : value;

  if (name.includes("retain.key")) {
    label = `${startCase(
      name
        .split(".")
        .slice(0, 2)
        .join(".")
    )}: ${value}`;
  }

  // special cases / overrides
  if (name === "moneyIncluded?" && value === "true") label = "Money Included";

  if (name === "moneyIncluded?" && value === "false") label = "No Money Included";

  if (name === "verified.status.retain.key" && value === "true") label = "Verified";

  if (name === "verified.status.retain.key" && value === "false") label = "Not Verified";

  return { name, value, label };
};

const applyRegex = (regex, value) => {
  return regex ? { $regex: new RegExp(value, "i") } : convertToBool(value);
};
const convertToBool = val => {
  if (val === "true") {
    return true;
  }
  if (val === "false") {
    return false;
  }
  return val;
};
const replacer = (key, value) => {
  if (value instanceof RegExp) return `__REGEXP ${value.toString()}`;
  return value;
};

const jsonStringifier = data => JSON.stringify(data, replacer, 2);

export { applyFilters, updateStateFromProps, onFilterChange, jsonStringifier };
