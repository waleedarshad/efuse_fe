import Router from "next/router";
import isEmpty from "lodash/isEmpty";

import { getToken } from "./Auth0Helper";

/**
 * @module AuthHelper
 * @category Helpers
 */

const applyRedirection = (accessType, context) => {
  const token = getToken(context);

  switch (accessType) {
    case "private":
      if (!token) {
        const redirectURL = encodeURIComponent(context.asPath);
        redirect(context, `/login?redirect_url=${redirectURL}`);
      }
      break;
    case "public":
      if (token) {
        redirect(context, "/lounge/featured");
      }
      break;
    default:
      break;
  }
};

const redirect = (context, path) => {
  const { req, res } = context;
  if (req) {
    res.writeHead(302, { Location: path });
    res.end();
    return;
  }
  Router.push(path);
};

/**
 * @summary Simple check to see if the user has an auth token stored as a cookie. Does not check if token is valid. Primarly used to determine if public API endpoint should be called.
 * @method
 * @returns {Boolean} Whether the user has a token cookie stored in browser
 */

const isUserLoggedIn = () => !isEmpty(getToken());

export { applyRedirection, isUserLoggedIn };
