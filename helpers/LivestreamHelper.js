/**
 @module LiveStreamHelper
 @category Helpers
*/

const returnLivestreamObject = livestream => {
  return {
    type: "Live",
    data: livestream?.data,
    imageUrl: livestream?.data?.thumbnailUrl,
    title: livestream?.data?.streamTitle,
    description: livestream?.data?.associatedUser?.twitch?.description,
    subTitle: `${livestream?.data?.platform} | ${livestream?.data?.game?.name}`,
    redirectLink: `/stream/${livestream?.data?.associatedUser?.twitch?.displayName}`,
    stream: livestream?.data?.associatedUser?.twitch?.displayName,
    internalRedirect: true
  };
};

export { returnLivestreamObject };
