import { getArrayFilteredByArray, trimLastCharacter } from "./GeneralHelper";
import { VALIDATE_SLUG } from "../graphql/ValidateSlugQuery";
import { initializeApollo } from "../config/apolloClient";

const apolloClient = initializeApollo();

export const generateCustomUrl = title => {
  const MAX_NUMBER_OF_CHARACTERS = 24;
  const fillerWords = ["and", "it", "the", "if", "an", "but", "for", "is", "of", "a", "in", "this"];

  const array = title
    .toLowerCase()
    .replace(/[-~!@#$%^&*()+={}[\]|\\:;"'<,>.?_/]/g, "")
    .split(" ");

  const result = array.filter(item => item.length > 0);

  const arrayWithoutFillerWords = getArrayFilteredByArray(result, fillerWords);

  let trimmedString = arrayWithoutFillerWords.join("-").substring(0, MAX_NUMBER_OF_CHARACTERS);

  trimmedString = trimLastCharacter(trimmedString, "-");

  return trimmedString;
};

export const validateSlug = async (slug, slugType) => {
  const { data } = await apolloClient.query({
    query: VALIDATE_SLUG,
    variables: { slug, slugType }
  });

  return data.ValidateSlug;
};
