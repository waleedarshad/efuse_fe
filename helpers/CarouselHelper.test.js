import React from "react";
import { getPortfolioHeaderCarouselItems } from "./CarouselHelper";
import PortfolioScores from "../components/Portfolio/PortfolioScores/PortfolioScores";
import UserAttributes from "../components/UserAttributes/UserAttributes";

describe("CarouselHelper", () => {
  const stats = {
    hypeScore: 4,
    totalEngagements: 100,
    postViews: 10000
  };
  const streak = 1500;

  it("returns the correct items for portfolio header carousel when there are no traits", () => {
    const user = {
      traits: [],
      motivations: ["Motivation 1", "Motivation 2"],
      publicTraitsMotivations: true
    };
    const actualItems = getPortfolioHeaderCarouselItems(stats, streak, user);
    const expectedItems = [
      <PortfolioScores key={0} stats={stats} streak={streak} />,
      <UserAttributes key={2} userAttributes={user.motivations} />
    ];

    expect(actualItems.length).toEqual(2);
    expect(actualItems).toEqual(expectedItems);
  });

  it("returns the correct items for portfolio header carousel when there are no motivations", () => {
    const user = {
      traits: ["Trait 1", "Trait 2"],
      motivations: [],
      publicTraitsMotivations: true
    };
    const actualItems = getPortfolioHeaderCarouselItems(stats, streak, user);
    const expectedItems = [
      <PortfolioScores key={0} stats={stats} streak={streak} />,
      <UserAttributes key={1} userAttributes={user.traits} />
    ];

    expect(actualItems.length).toEqual(2);
    expect(actualItems).toEqual(expectedItems);
  });

  it("does not return traits or motivations if user has privacy set", () => {
    const user = {
      traits: ["Trait 1", "Trait 2"],
      motivations: ["Motivations 1", "Motivations 2"],
      publicTraitsMotivations: false
    };
    const actualItems = getPortfolioHeaderCarouselItems(stats, streak, user, true);
    const expectedItems = [<PortfolioScores key={0} stats={stats} streak={streak} alignRight />];

    expect(actualItems).toEqual(expectedItems);
  });
});
