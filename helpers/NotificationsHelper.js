import { withCdn } from "../common/utils";
import { rescueNil, getImage } from "./GeneralHelper";

/**
 @module NotificationsHelper
 @category Helpers
*/

/**
 *
 * @param {*} notification
 */

const buildUrl = notification => {
  const dontOpen = "#.";
  try {
    const { notifyable, notifyableType, timeline } = notification;
    const follower = rescueNil(notifyable, "follower");
    const organization = rescueNil(notifyable, "organization");
    const notifyableId = rescueNil(notifyable, "_id");
    const timelineUrl = timeline ? `/home?feedId=${timeline}` : dontOpen;
    const tournament = rescueNil(notifyable, "slug");
    let url = "";

    switch (notifyableType) {
      case "friends":
        url = follower ? `/users/posts?id=${follower}` : dontOpen;
        break;
      case "hypes":
      case "likes":
        url = timelineUrl;
        break;
      case "comments":
        url = timelineUrl;
        break;
      case "homeFeeds":
        url = notifyableId ? `/home?feedId=${notifyableId}` : dontOpen;
        break;
      case "opportunities":
        url = notifyableId ? `/opportunities/show/${notifyableId}` : dontOpen;
        break;
      case "organizationInvitations":
        url = organization ? `/organizations/show?id=${organization}` : dontOpen;
        break;
      case "erenatournaments":
        url = tournament ? `/e/${tournament}/manage?scoring=true` : dontOpen;
        break;
      case "messages":
        url = "/messages";
        break;
      default:
        url = dontOpen;
        break;
    }
    return url;
  } catch (error) {
    return dontOpen;
  }
};

/**
 *
 * @param {*} notification
 */
const getAvatar = notification => {
  const { notifyableType, notifyable, sender } = notification;

  let avatar = {};
  try {
    if (notifyableType === "opportunities") {
      avatar = getImage(notifyable.organization.profileImage, "avatar");
    } else if (notifyableType === "homeFeeds" && notifyable.timelineableType === "organizations") {
      avatar = getImage(notifyable.timelineable.profileImage, "avatar");
    } else if (notifyableType === "erenatournaments") {
      avatar = notifyable.brandLogoUrl ? notifyable.brandLogoUrl : withCdn("/static/images/eRenaLogo.png");
    } else {
      avatar = getImage(sender.profilePicture, "avatar");
    }
  } catch (e) {
    avatar = getImage(sender.profilePicture, "avatar");
  }
  return avatar;
};

/**
 *
 * @param {*} notification
 */
const buildProfileUrl = notification => {
  const { notifyableType, notifyable, sender } = notification;
  let url = `/u/${sender.username}`;
  try {
    if (notifyableType === "homeFeeds" && notifyable.timelineableType === "organizations") {
      url = `/organizations/show?id=${notifyable.timelineable._id}`;
    }
  } catch (e) {
    url = `/u/${sender.username}`;
  }
  return encodeURI(url);
};

/**
 *
 * @param {*} notification
 */
const buildContent = notification => {
  const { notifyableType, notifyable, sender, content } = notification;
  let fullName = sender.name;
  try {
    if (notifyableType === "homeFeeds" && notifyable.timelineableType === "organizations") {
      fullName = notifyable.timelineable.name;
    }
  } catch (e) {
    fullName = sender.name;
  }

  return content.replace(fullName, `<a href=${buildProfileUrl(notification)}>${fullName}</a>`);
};

export { buildUrl, getAvatar, buildProfileUrl, buildContent };
