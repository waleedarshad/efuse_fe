export const getFullOpportunityUrl = opportunity => {
  if (opportunity?.promoted_url) {
    return opportunity.promoted_url;
  }
  if (opportunity.shortName) {
    return `/o/${opportunity.shortName}`;
  }
  return `/opportunities/show?id=${opportunity._id}`;
};

export const getOrganizationUrl = organization => {
  if (organization?.promoted_url) {
    return organization.promoted_url;
  }
  if (organization.shortName) {
    return `/org/${organization.shortName}`;
  }
  return `/organizations/show?id=${organization._id}`;
};

const getCustomPath = (currentUser, type) => {
  return currentUser?.id ? `/${type}` : `/public/${type}`;
};

const getOpportunitiesParamsTail = (router, profile, profileId, currentUser, category) => {
  if (profile) {
    if (router.pathname === "/organizations/opportunities") {
      return [profileId, router.pathname];
    }
    return [profileId, getCustomPath(currentUser, "opportunities")];
  }

  if (router.pathname === "/opportunities/owned" || router.pathname === "/opportunities/applied") {
    return ["", router.pathname];
  }

  if (router.pathname === "/opportunities/all" || router.asPath === `/opportunities/${category}`) {
    return ["", getCustomPath(currentUser, "opportunities")];
  }
  return [];
};

const getOrganizationsParamsTail = (router, currentUser) => {
  return router.pathname === "/organizations" ? getCustomPath(currentUser, "organizations") : router.pathname;
};

export { getCustomPath, getOpportunitiesParamsTail, getOrganizationsParamsTail };
