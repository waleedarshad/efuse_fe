/* eslint-disable no-console */
require("dotenv").config();

const datadogTracer = require("dd-trace").init({
  analytics: true,
  env: process.env.ENVIRONMENT,
  service: "fe"
});
// Adding a hook to the datadog tracing libs to make sure that we are
// setting the route because the "*" matching below messes this up for APM tracing
datadogTracer.use("express", {
  hooks: {
    request: (span, req) => {
      span.setTag("http.route", req.originalUrl);
    },
    blacklist: ["/__status__"]
  }
});

const pinoHttp = require("pino-http")({
  redact: ["req.headers.cookie"]
});

const express = require("express");
const next = require("next");
const StatusModule = require("./routes/status");
const DeepLinkConfigModule = require("./routes/deep_link_config");

const port = process.env.PORT || 3000;
const app = next({
  dev: process.env.NODE_ENV === "development"
});
const handle = app.getRequestHandler();

// Using a single function to handle multiple signals
const sighandler = signal => {
  // eslint-disable-next-line no-console
  console.log(`Received ${signal}`);
  process.exit(1);
};

process.on("SIGINT", sighandler);
process.on("SIGTERM", sighandler);

if (!process.env.DEFAULT_URL) {
  console.warn(`
*******WARNING*******

BASE_URL ENV variable not found.  Pull in env files with:
aws secretsmanager get-secret-value --secret-id 'fe.env.development' --output=text --query=SecretString > .env

Exiting`);
  process.exit(0);
}
app
  .prepare()
  .then(() => {
    const server = express();
    server.use(pinoHttp);

    // Status page for healthchecks
    StatusModule(server);

    // IOS Deeplink config
    DeepLinkConfigModule(server);

    server.get("/__gitsha__", (req, res) => {
      res.status(200).send(process.env.GIT_SHA);
    });

    server.get("*", (req, res) => {
      return handle(req, res);
    });

    server
      .listen(port, err => {
        if (err) {
          console.log("Caught an error:");
          console.error(err);
          throw err;
        }
        console.info(`> Ready on http://localhost:${port}`);
      })
      .on("error", error => {
        pinoHttp.logger.error({ error }, "Caught an error in service.listen's on error handler.");
      });
  })
  .catch(ex => {
    console.log("Caught an unhandled error. Exiting.");
    console.error(ex.stack);
    process.exit(1);
  });
