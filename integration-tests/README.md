# integration-tests

# Running

## Install dependencies

- npm install

### Run cypress tests with GUI

- npm run cy:open

### Run cypress tests in CLI

- npm run cy:run
