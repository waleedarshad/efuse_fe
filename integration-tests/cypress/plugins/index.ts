/// <reference types="cypress" />
import { beforeRunHook, afterRunHook } from "cypress-mochawesome-reporter/lib";

export default (
  on: Cypress.PluginEvents,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  config: Cypress.PluginConfigOptions
): void | Cypress.ConfigOptions | Promise<Cypress.ConfigOptions> => {
  on("before:run", async details => {
    await beforeRunHook(details);
  });

  on("after:run", async () => {
    await afterRunHook();
  });
};
