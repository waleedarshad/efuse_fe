export const graphqlNewsCategoriesResponse = {
  data: {
    getCategories: [
      {
        _id: "5e65ae24d7c95b334ef0e7ab",
        slug: "health-and-wellness",
        name: "Health and Wellness"
      },
      {
        _id: "5e6ba07d0d74c01b2f14aa61",
        slug: "efuse",
        name: "eFuse"
      },
      {
        _id: "60b931347519bd0012fdf870",
        slug: "pipeline",
        name: "Pipeline"
      },
      {
        _id: "60b93150ed4f810011d71c82",
        slug: "esports-business",
        name: "Esports Business"
      },
      {
        _id: "60b9316871d51300120e4613",
        slug: "professional-development",
        name: "Professional Development"
      },
      {
        _id: "60b93174de96430011bd23eb",
        slug: "other",
        name: "Other"
      }
    ]
  }
};

export const newsArticlesResponse = {
  articles: {
    docs: [
      {
        _id: "60be50841f1e71001c7b9950",
        image: {
          filename: "bird_16x9_3.jpeg",
          contentType: "image/jpeg",
          url: "https://staging-efuse.s3.amazonaws.com/uploads/learning-articles/1623085222236-bird_16x9_3.jpeg"
        },
        status: "Published",
        isActive: true,
        promoted: false,
        traits: [],
        motivations: [],
        slug: "333",
        category: {
          _id: "60b931347519bd0012fdf870",
          name: "Pipeline",
          slug: "pipeline",
          createdAt: "2021-06-03T19:44:51.347Z",
          image: {
            filename: "no_image.jpg",
            contentType: "image/jpg",
            url: "https://efuse.gg/static/images/no_image.jpg"
          },
          user: "5de9167a52cc7602284bb545",
          isActive: true
        },
        summary: "New Updated Summary",
        body:
          "<h2>New Learning Article</h2><p></p><p>Start typing here to create your article. You can use the  button above to add images, headers, lists, videos or other components.&nbsp;</p><p>Your changes will be saved as you go, so you can start and stop anytime.</p><p>When you're ready, publish your article for the world to see.</p>",
        title:
          "This is the longest news title that ever existed on the planet earth just so we can test a long news title",
        author: {
          _id: "609c17fc7a329a002a4ed3ee",
          profilePicture: {
            filename: "mindblown_white.png",
            contentType: "image/png",
            url: "https://cdn.efuse.gg/uploads/static/global/mindblown_white.png"
          },
          roles: [],
          showOnline: true,
          showOnlineMobile: true,
          goldSubscriber: false,
          online: true,
          publicTraitsMotivations: false,
          verified: false,
          firstName: "Yasi",
          lastName: "M",
          name: "Yasi M",
          username: "yasi",
          pathway: "5ff85dd9de9442939f1e6bb8",
          bio: "",
          twitchUserName: "confetti98",
          twitch: "60a805bba5cd71001bc98f71",
          stats: "60be2e988daefffec329b0f6"
        },
        authorType: "users",
        createdAt: "2021-06-07T16:59:48.230Z",
        updatedAt: "2021-08-04T18:49:48.307Z",
        __v: 0,
        publishDate: "2021-06-07T17:00:36.436Z",
        id: "60be50841f1e71001c7b9950",
        url: "/news/pipeline/333"
      },
      {
        _id: "60be50241f1e71001c7b994f",
        image: {
          filename: "000107506.jpeg",
          contentType: "image/jpeg",
          url: "https://staging-efuse.s3.amazonaws.com/uploads/learning-articles/1623085122107-000107506.jpeg"
        },
        status: "Published",
        isActive: true,
        promoted: false,
        traits: [],
        motivations: [],
        slug: "222",
        category: {
          _id: "5e65ae24d7c95b334ef0e7ab",
          name: "Health and Wellness",
          image: {
            filename: "",
            contentType: "",
            url: "https://efuse.gg/static/images/no_image.jpg"
          },
          isActive: true,
          user: "test",
          createdAt: "",
          slug: "health-and-wellness"
        },
        summary: "Test News",
        body:
          "<h2>New Learning Article</h2><p>Start typing here to create your article. You can use the  button above to add images, headers, lists, videos or other components. ja;slkdjf</p><p>a;sldfj;laksjdf;lkasjdf</p><p>a;lskdfn;aklsdjfn;alksdnf;klansdf;ajksndf;kajns</p><p>asdfas</p><p>Your changes will be saved as you go, so you can start and stop anytime.</p><p>When you're ready, publish your article for the world to see.</p>",
        title: "Another newsuhuoyhuyhbyuigtytugoybhbhkjbkjhbkjhbkjhbkjhbkhbkjhbkjhbkjhb",
        author: {
          _id: "609c17fc7a329a002a4ed3ee",
          profilePicture: {
            filename: "mindblown_white.png",
            contentType: "image/png",
            url: "https://cdn.efuse.gg/uploads/static/global/mindblown_white.png"
          },
          roles: [],
          showOnline: true,
          showOnlineMobile: true,
          goldSubscriber: false,
          online: true,
          publicTraitsMotivations: false,
          verified: false,
          firstName: "Yasi",
          lastName: "M",
          name: "Yasi M",
          username: "yasi",
          pathway: "5ff85dd9de9442939f1e6bb8",
          bio: "",
          twitchUserName: "confetti98",
          twitch: "60a805bba5cd71001bc98f71",
          stats: "60be2e988daefffec329b0f6"
        },
        authorType: "users",
        createdAt: "2021-06-07T16:58:12.874Z",
        updatedAt: "2021-08-16T17:39:41.274Z",
        __v: 0,
        publishDate: "2021-06-07T16:58:50.154Z",
        id: "60be50241f1e71001c7b994f",
        url: "/news/health-and-wellness/222"
      },
      {
        _id: "60be2f6c8f1ca9001db92d9a",
        image: {
          filename: "0_E031YgZzn21iOf4Q.gif",
          contentType: "image/jpeg",
          url: "https://staging-efuse.s3.amazonaws.com/uploads/learning-articles/1623076801863-0_E031YgZzn21iOf4Q.gif"
        },
        status: "Published",
        isActive: true,
        promoted: false,
        traits: [],
        motivations: [],
        slug: "thenews",
        category: {
          _id: "60b93150ed4f810011d71c82",
          name: "Esports Business",
          slug: "esports-business",
          createdAt: "2021-06-03T19:45:19.770Z",
          image: {
            filename: "no_image.jpg",
            contentType: "image/jpg",
            url: "https://efuse.gg/static/images/no_image.jpg"
          },
          user: "5de9167a52cc7602284bb545",
          isActive: true
        },
        summary: "Testing News",
        body:
          "<h2>Here's some esports news</h2><p>Lets go</p><p>Your changes will be saved as you go, so you can start and stop anytime.</p><p>When you're ready, publish your article for the world to see.</p>",
        title: "Newwwwssss",
        author: {
          _id: "609c17fc7a329a002a4ed3ee",
          profilePicture: {
            filename: "mindblown_white.png",
            contentType: "image/png",
            url: "https://cdn.efuse.gg/uploads/static/global/mindblown_white.png"
          },
          roles: [],
          showOnline: true,
          showOnlineMobile: true,
          goldSubscriber: false,
          online: true,
          publicTraitsMotivations: false,
          verified: false,
          firstName: "Yasi",
          lastName: "M",
          name: "Yasi M",
          username: "yasi",
          pathway: "5ff85dd9de9442939f1e6bb8",
          bio: "",
          twitchUserName: "confetti98",
          twitch: "60a805bba5cd71001bc98f71",
          stats: "60be2e988daefffec329b0f6"
        },
        authorType: "users",
        createdAt: "2021-06-07T14:38:36.822Z",
        updatedAt: "2021-06-07T16:57:22.045Z",
        __v: 0,
        publishDate: "2021-06-07T14:40:20.374Z",
        id: "60be2f6c8f1ca9001db92d9a",
        url: "/news/esports-business/thenews"
      },
      {
        _id: "60a66561f707d67c0c6973af",
        image: {
          filename: "ea90e808b83da220f355c588b255e565.jpg",
          contentType: "image/jpeg",
          url:
            "https://staging-efuse.s3.amazonaws.com/uploads/learning-articles/1621517728384-ea90e808b83da220f355c588b255e565.jpg"
        },
        status: "Published",
        isActive: true,
        promoted: false,
        traits: [],
        motivations: [],
        slug: "fabulous",
        category: {
          _id: "60b93174de96430011bd23eb",
          name: "Other",
          slug: "other",
          createdAt: "2021-06-03T19:45:56.070Z",
          image: {
            filename: "no_image.jpg",
            contentType: "image/jpg",
            url: "https://efuse.gg/static/images/no_image.jpg"
          },
          user: "5de9167a52cc7602284bb545",
          isActive: true
        },
        summary: "My great summary",
        body:
          '<p>Because of esports’ infancy in the collegiate scene, most colleges that have adopted, and succeeded at competitive gaming have been small, without a reputation or history in traditional sports. Louisiana State University is one of the largest schools to have adopted esports at the competitive level and succeeded in a variety of titles. The public school, located in the heart of Baton Rouge, Louisiana houses more than 25,000 undergraduate students on 2,000 acres. Their beautiful campus combined with top of line research and educational facilities have earned them a spot in the top 100 public universities in the United States consistently over the last 3 years. Their esports program has consistently grown since its founding and has now become a powerhouse in the South.</p>  \n<p>The club, founded in 2016, hosts competitive and casual players in Rocket League, League of Legends, VALORANT, Fortnite, and Overwatch. More recently, their teams were recognized by LSU Recreation as an official sport club, an accomplishment that few universities around the nation have been able to receive. With the recognition, they have been able to focus on competitive dominance and have brought home countless trophies at competitions all around the states. Their most recent accomplishment was a 2nd place finish in the Collegiate Rocket League Spring Season.  \nRocket League (Spring and internal competitions) – Out of their 5 competitive titles, LSU has seen the most success in Rocket League. Last month, Louisiana State University surprised many with their win over Northwood University because of the latter\'s dominance in League Play with a 17-1 record to Louisiana State University’s 13-5 record. Their coach, Tyler "TBates" Bates mentioned before the match that they weren’t afraid of Northwood University and it appeared the RLCS experience from their leader Jackson "Ayjacks" Carter helped carry the Tigers to the Grand Finals. Although they lost to Akron in the finals of the playoffs, the 2nd place finish is an impressive feat for such a large university.</p>  \n<p>Their success in Rocket League is not newfound. Their school became a host of so many talented Carball players that in 2019, they held the LSU Esports Rocket League Qualifying Tournament to select the three-student team that would represent at the Power Five Esports Invitational. This level of popularity and success has not gone unnoticed by university administration. Unlike most universities with esports programs, LSU has adopted esports into its campus culture and student life. Stacia Haynie, the Executive Vice President and Provost at the university has continued to support their success noting that, “We have successful esports clubs already on campus, an esports room recently opened at the UREC and now a qualifying tournament for our students to move on to compete nationally against other Power Five schools. We wish all the students competing good luck, and the LSU community will be cheering them throughout the competition.”</p>\n<p>Through these successes, the esports program has been able to receive significant investments to build an Esports Room. The recreational facility at LSU noticed that “esports [was] a blooming and budding phenomenon across the country and [knew] it\'s something people play recreationally. [They] wanted to make sure [they] had it,” at the university for students to play. With one room dedicated to console play decked out with PlayStations and Xboxs and other dedicated solely to PC gamers, the LSU Esports Room is the hub for gaming on campus.</p>\n<p>Coming off some massive success competitively, LSU esports is looking to replicate this season in their other titles they support at the varsity level. Furthermore, LSU hopes to advance its professional opportunities for their students including networking opportunities, event management, and social media management. Being one of the largest universities to offer a competitive esports, Louisiana State University has become a dominant powerhouse in collegiate esports.</p>',
        title: "Fabulous",
        author: {
          _id: "5ca5df6b1608075701928ebb",
          firstName: "Fakhir",
          lastName: "Shad",
          bio:
            "My Classical Bio: Hello this is my bio. I hope you like my bio. Smile if you enjoy this bio. Bio time! Cool Bio! I can be found here at: youtube.com.bio.bio.bio :)  😂🤣😜🙂😂🤣😜🙂",
          profilePicture: {
            filename: "1554374592876-11392926_915755788485762_4748794089000750308_n.jpg",
            contentType: "image/png",
            url:
              "https://scontent.efcdn.io/uploads/users/1554374592876-11392926_915755788485762_4748794089000750308_n.jpg"
          },
          roles: ["admin"],
          online: true,
          verified: true,
          username: "vantsome",
          goldSubscriber: true,
          goldSubscriberSince: "2019-04-04T10:41:47.787Z",
          showOnline: true,
          showOnlineMobile: true,
          stats: "5f16e175777abd6fc2fe1016",
          publicTraitsMotivations: false,
          pathway: "5ff85dd9de9442939f1e6bb8",
          name: "Fakhir Shad",
          discordUserUniqueId: "fakhir-vf#8535",
          twitchUserName: "fakhir1234"
        },
        authorType: "users",
        createdAt: "2021-05-20T13:34:25.640Z",
        updatedAt: "2021-08-04T18:47:28.413Z",
        __v: 0,
        publishDate: "2021-05-20T13:36:14.574Z",
        id: "60a66561f707d67c0c6973af",
        url: "/news/other/fabulous"
      },
      {
        _id: "60a618ddc0ef62f519a74aee",
        image: {
          filename: "97510062_3355932114425225_5175729015255728128_n.jpg",
          contentType: "image/jpeg",
          url:
            "https://staging-efuse.s3.amazonaws.com/uploads/learning-articles/1621498253617-97510062_3355932114425225_5175729015255728128_n.jpg"
        },
        status: "Published",
        isActive: true,
        promoted: false,
        traits: [],
        motivations: [],
        slug: "testing-model",
        category: {
          _id: "5e6ba07d0d74c01b2f14aa61",
          name: "eFuse",
          image: {
            filename: "",
            contentType: "",
            url: "https://efuse.gg/static/images/no_image.jpg"
          },
          isActive: true,
          user: "test",
          createdAt: "",
          slug: "efuse"
        },
        summary: "Fantastic article. Must read",
        body:
          '<h2 style="">My New Learning Article</h2><p style="">What a great start</p><p style="">Nice work</p><p style="">Awesome</p>',
        title: "Testing model ",
        author: {
          _id: "5ca5df6b1608075701928ebb",
          firstName: "Fakhir",
          lastName: "Shad",
          bio:
            "My Classical Bio: Hello this is my bio. I hope you like my bio. Smile if you enjoy this bio. Bio time! Cool Bio! I can be found here at: youtube.com.bio.bio.bio :)  😂🤣😜🙂😂🤣😜🙂",
          profilePicture: {
            filename: "1554374592876-11392926_915755788485762_4748794089000750308_n.jpg",
            contentType: "image/png",
            url:
              "https://scontent.efcdn.io/uploads/users/1554374592876-11392926_915755788485762_4748794089000750308_n.jpg"
          },
          roles: ["admin"],
          online: true,
          verified: true,
          username: "vantsome",
          goldSubscriber: true,
          goldSubscriberSince: "2019-04-04T10:41:47.787Z",
          showOnline: true,
          showOnlineMobile: true,
          stats: "5f16e175777abd6fc2fe1016",
          publicTraitsMotivations: false,
          pathway: "5ff85dd9de9442939f1e6bb8",
          name: "Fakhir Shad",
          discordUserUniqueId: "fakhir-vf#8535",
          twitchUserName: "fakhir1234"
        },
        authorType: "users",
        createdAt: "2021-05-20T08:07:57.687Z",
        updatedAt: "2021-09-03T09:34:45.895Z",
        __v: 0,
        publishDate: "2021-05-20T08:11:00.934Z",
        id: "60a618ddc0ef62f519a74aee",
        url: "/news/efuse/testing-model"
      },
      {
        _id: "6087d49f3aaed6002bf9cd79",
        image: {
          filename: "2.jpeg",
          contentType: "image/jpeg",
          url: "https://staging-efuse.s3.amazonaws.com/uploads/learning-articles/1619514842416-2.jpeg"
        },
        status: "Published",
        isActive: true,
        promoted: false,
        traits: ["GAMER"],
        motivations: [],
        slug: "gaming-industry",
        category: {
          _id: "5e65ae24d7c95b334ef0e7ab",
          name: "Health and Wellness",
          image: {
            filename: "",
            contentType: "",
            url: "https://efuse.gg/static/images/no_image.jpg"
          },
          isActive: true,
          user: "test",
          createdAt: "",
          slug: "health-and-wellness"
        },
        summary: "QA Engineer",
        body:
          "<h2>New Learning Article</h2><p>Start typing here to create your article. You can use the  button above to add images, headers, lists, videos or other components.&nbsp;</p><p>Your changes will be saved as you go, so you can start and stop anytime.</p><p>When you're ready, publish your article for the world to see.</p>",
        title: "QA Engineer",
        author: {
          _id: "603782a47737730011c41aef",
          profilePicture: {
            filename: "larki.jpg",
            contentType: "image/jpeg",
            url: "https://staging-cdn.efuse.gg/uploads/users/1617088563788-larki.jpg"
          },
          roles: [],
          showOnline: true,
          showOnlineMobile: true,
          goldSubscriber: false,
          online: false,
          publicTraitsMotivations: false,
          verified: false,
          firstName: "!!!@@@",
          lastName: "!!!@@@",
          username: "UmerChaudhary",
          pathway: "5ff85e8fde9442939f1e7937",
          stats: "6049f1c1f57cf0595a5e7bbd",
          name: "Umer Chaudhary",
          bio:
            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
          twitch: "60c1dbf64703dd0029332088"
        },
        authorType: "users",
        createdAt: "2021-04-27T09:08:47.325Z",
        updatedAt: "2021-06-30T13:23:05.782Z",
        __v: 1,
        publishDate: "2021-04-27T09:14:09.221Z",
        video: null,
        id: "6087d49f3aaed6002bf9cd79",
        url: "/news/health-and-wellness/gaming-industry"
      },
      {
        _id: "6037aeb4435ce31e2ebc6dfc",
        image: {
          filename: "no_image.jpg",
          contentType: "image/jpg",
          url: "https://efuse.gg/static/images/no_image.jpg"
        },
        status: "Published",
        isActive: true,
        promoted: false,
        traits: [],
        motivations: [],
        slug: "aeiou",
        category: {
          _id: "60b931347519bd0012fdf870",
          name: "Pipeline",
          slug: "pipeline",
          createdAt: "2021-06-03T19:44:51.347Z",
          image: {
            filename: "no_image.jpg",
            contentType: "image/jpg",
            url: "https://efuse.gg/static/images/no_image.jpg"
          },
          user: "5de9167a52cc7602284bb545",
          isActive: true
        },
        summary: "",
        body:
          "<h2>New Learning Article</h2><p>Start typing here to create your article. You can use the  button above to add images, headers, lists, videos or other components.&nbsp;</p><p>Your changes will be saved as you go, so you can start and stop anytime.</p><p>When you're ready, publish your article for the world to see.</p>",
        title: "Testing after conversion as org ",
        author: {
          _id: "5ff2a7e36ddbff20b1ac9400",
          verified: {
            status: false,
            verifiedAt: "2021-01-04T05:30:11.466Z"
          },
          profileImage: {
            filename: "eFuse Top100 Panel Valorant.png",
            contentType: "image/jpeg",
            url:
              "https://staging-efuse.s3.amazonaws.com/uploads/organizations/1609738208904-eFuse%20Top100%20Panel%20Valorant.png"
          },
          headerImage: {
            filename: "sun_9-wallpaper-960x540.jpg",
            contentType: "image/jpeg",
            url:
              "https://staging-efuse.s3.amazonaws.com/uploads/organizations/1609738208906-sun_9-wallpaper-960x540.jpg"
          },
          promoVideo: {
            title: "Let you love me",
            description: "By Rita Ora",
            embeddedUrl:
              "https://www.youtube.com/watch?v=XCQK6LmhYqc&list=RDGMEMQ1dJ7wXfLlqCjwV0xfSNbAVM3HbKnQxd0_E&index=21&ab_channel=RitaOra",
            isVideo: false,
            videoFile: {}
          },
          captains: [],
          publishStatus: "visible",
          followersCount: 0,
          promoted: false,
          deleted: false,
          name: "Testing new organization",
          scale: "1",
          email: "",
          location: "Dasmariñas, Cavite, Philippines",
          phoneNumber: "+5 (105)-356-1561623",
          website: "https://www.youtube.com/watch?v=WstAR10icko&list=RDd_aePWBRQ5I&index=6&ab_channel=AsimAzhar",
          organizationType: "Collegiate Team",
          status: "Open",
          description: "ultra testing",
          about: "<p>shivaji the boss.</p>",
          shortName: "testing-new-organization",
          toBeVerified: false,
          user: "5ecf54d2343c90783cffb06e",
          events: [
            {
              image: {
                filename: "briefcase.png",
                contentType: "image/png",
                url: "https://efuse.gg/static/images/briefcase.png"
              },
              _id: "600e681458539c2a31808044",
              title: "larho mujhe",
              subTitle: "Bilal Khan",
              eventDate: "2021-01-06T07:00:00.000Z",
              description: "<p>In case of fighter</p>"
            }
          ],
          honors: [
            {
              image: {
                filename: "briefcase.png",
                contentType: "image/png",
                url: "https://efuse.gg/static/images/briefcase.png"
              },
              _id: "600e68dc58539c2a31808047",
              title: "Liggi",
              subTitle: "Quill",
              description: "<p>qwerew</p>"
            }
          ],
          playerCards: [],
          videoCarousel: [
            {
              uploaded: false,
              _id: "5ff5a5598eaa973b2e005663",
              title: "Hall of Fame",
              url:
                "https://www.youtube.com/watch?v=mk48xRzuNvA&list=RDGMEMQ1dJ7wXfLlqCjwV0xfSNbAVM3HbKnQxd0_E&index=22&ab_channel=TheScriptVEVO"
            }
          ],
          createdAt: "2021-01-04T05:30:11.466Z",
          updatedAt: "2021-01-25T06:44:44.205Z",
          __v: 9
        },
        authorType: "organizations",
        createdAt: "2021-02-25T14:05:40.122Z",
        updatedAt: "2021-02-25T14:13:40.390Z",
        __v: 0,
        id: "6037aeb4435ce31e2ebc6dfc",
        url: "/news/pipeline/aeiou"
      },
      {
        _id: "6034b5d771288626ec9e8c19",
        image: {
          filename: "bdd2bb3d6d2776d220f98715e41808ee.jpg",
          contentType: "image/jpeg",
          url:
            "https://staging-efuse.s3.amazonaws.com/uploads/learning-articles/1614072737274-bdd2bb3d6d2776d220f98715e41808ee.jpg"
        },
        status: "Published",
        isActive: true,
        promoted: false,
        traits: [],
        motivations: [],
        slug: "test-article",
        category: {
          _id: "5e6ba07d0d74c01b2f14aa61",
          name: "eFuse",
          image: {
            filename: "",
            contentType: "",
            url: "https://efuse.gg/static/images/no_image.jpg"
          },
          isActive: true,
          user: "test",
          createdAt: "",
          slug: "efuse"
        },
        summary: "My test summary",
        body:
          '<h2 style="">Awesome</h2><p style="">My great and awesome article. It looks so great</p><p style="">My great and awesome article. It looks so great</p><p style="">My great and awesome article. It looks so great</p>',
        title: "Test Article",
        author: {
          _id: "5ca5df6b1608075701928ebb",
          firstName: "Fakhir",
          lastName: "Shad",
          bio:
            "My Classical Bio: Hello this is my bio. I hope you like my bio. Smile if you enjoy this bio. Bio time! Cool Bio! I can be found here at: youtube.com.bio.bio.bio :)  😂🤣😜🙂😂🤣😜🙂",
          profilePicture: {
            filename: "1554374592876-11392926_915755788485762_4748794089000750308_n.jpg",
            contentType: "image/png",
            url:
              "https://scontent.efcdn.io/uploads/users/1554374592876-11392926_915755788485762_4748794089000750308_n.jpg"
          },
          roles: ["admin"],
          online: true,
          verified: true,
          username: "vantsome",
          goldSubscriber: true,
          goldSubscriberSince: "2019-04-04T10:41:47.787Z",
          showOnline: true,
          showOnlineMobile: true,
          stats: "5f16e175777abd6fc2fe1016",
          publicTraitsMotivations: false,
          pathway: "5ff85dd9de9442939f1e6bb8",
          name: "Fakhir Shad",
          discordUserUniqueId: "fakhir-vf#8535",
          twitchUserName: "fakhir1234"
        },
        authorType: "users",
        createdAt: "2021-02-23T07:59:19.378Z",
        updatedAt: "2021-06-30T10:42:18.487Z",
        __v: 0,
        publishDate: "2021-02-23T07:59:19.378Z",
        startDate: "true",
        id: "6034b5d771288626ec9e8c19",
        url: "/news/efuse/test-article"
      },
      {
        _id: "5fe0a7334c161b8c11d83524",
        image: {
          filename: "Screenshot from 2021-01-27 14-22-52.png",
          contentType: "image/jpeg",
          url:
            "https://staging-efuse.s3.amazonaws.com/uploads/learning-articles/1611749729040-Screenshot%20from%202021-01-27%2014-22-52.png"
        },
        status: "Published",
        isActive: true,
        promoted: false,
        traits: ["GRAPHIC_DESIGNER"],
        motivations: [],
        slug: "intercom",
        category: {
          _id: "60b9316871d51300120e4613",
          name: "Professional Development",
          slug: "professional-development",
          createdAt: "2021-06-03T19:45:44.017Z",
          image: {
            filename: "no_image.jpg",
            contentType: "image/jpg",
            url: "https://efuse.gg/static/images/no_image.jpg"
          },
          user: "5de9167a52cc7602284bb545",
          isActive: true
        },
        summary: "ascda",
        body:
          '<h2 data-cached-aria-disabled="" aria-disabled="true" data-cached-tabindex="" data-inert-pointer-events="" data-ally-disabled="true" style="pointer-events: none;">New Learning Article</h2><p data-cached-aria-disabled="" aria-disabled="true" data-cached-tabindex="" data-inert-pointer-events="" data-ally-disabled="true" style="pointer-events: none;">Start typing here to create your article. You can use the  button above to</p><figure><div class="embed-responsive"><iframe width="560" height="315" src="//www.youtube.com/embed/XYtyeqVQnRI?enablejsapi=1" frameborder="0" allowfullscreen=""></iframe></div><figcaption data-cached-aria-disabled="" aria-disabled="true" data-cached-tabindex="" data-inert-pointer-events="" data-ally-disabled="true" style="pointer-events: none;">hunya</figcaption></figure><p data-cached-aria-disabled="" aria-disabled="true" data-cached-tabindex="" data-inert-pointer-events="" data-ally-disabled="true" style="pointer-events: none;">testsavdasdas</p><hr><hr><p data-cached-aria-disabled="" aria-disabled="true" data-cached-tabindex="" data-inert-pointer-events="" data-ally-disabled="true" style="pointer-events: none;">asdasdasdasdas</p><p data-cached-aria-disabled="" aria-disabled="true" data-cached-tabindex="" data-inert-pointer-events="" data-ally-disabled="true" style="pointer-events: none;"> add images, headers, lists, videos or other components.&nbsp;</p><p data-cached-aria-disabled="" aria-disabled="true" data-cached-tabindex="" data-inert-pointer-events="" data-ally-disabled="true" style="pointer-events: none;">Your changes will be saved as you go, so you can start and stop anytime.</p><p data-cached-aria-disabled="" aria-disabled="true" data-cached-tabindex="" data-inert-pointer-events="" data-ally-disabled="true" style="pointer-events: none;">When you\'re ready, publish your article for the world to see.</p><figure><div class="embed-responsive"><iframe width="560" height="315" src="//www.youtube.com/embed/ssrNcwxALS4?enablejsapi=1" frameborder="0" allowfullscreen=""></iframe></div><figcaption data-cached-aria-disabled="" aria-disabled="true" data-cached-tabindex="" data-inert-pointer-events="" data-ally-disabled="true" style="pointer-events: none;">jaan</figcaption></figure><figure><div class="embed-responsive"><iframe width="560" height="315" src="//www.youtube.com/embed/C19ap2M7DDE?enablejsapi=1" frameborder="0" allowfullscreen=""></iframe></div><figcaption data-cached-aria-disabled="" aria-disabled="true" data-cached-tabindex="" data-inert-pointer-events="" data-ally-disabled="true" style="pointer-events: none;">gaaa</figcaption></figure><p data-cached-aria-disabled="" aria-disabled="true" data-cached-tabindex="" data-inert-pointer-events="" data-ally-disabled="true" style="pointer-events: none;"></p>',
        title: "testing new button",
        author: {
          _id: "605aeb4bfd9c8a25468a9a60",
          verified: {
            status: false,
            verifiedAt: "2021-03-24T07:33:31.059Z"
          },
          profileImage: {
            filename: "efuse-icon.png",
            contentType: "image/jpeg",
            url: "https://staging-efuse.s3.amazonaws.com/uploads/organizations/1616571207292-efuse-icon.png"
          },
          headerImage: {
            filename: "awesome-background-wallpaper-1.jpg",
            contentType: "image/jpeg",
            url:
              "https://staging-efuse.s3.amazonaws.com/uploads/organizations/1616571207293-awesome-background-wallpaper-1.jpg"
          },
          promoVideo: {
            isVideo: false
          },
          captains: [],
          publishStatus: "visible",
          followersCount: 0,
          promoted: false,
          games: [],
          deleted: false,
          name: "Demon Slayer",
          scale: "1-99",
          email: "",
          location: "",
          phoneNumber: "",
          website: "",
          organizationType: "Brand",
          status: "Open",
          description: "This is bio",
          shortName: "demon-slayer",
          toBeVerified: false,
          user: "5ecf54d2343c90783cffb06e",
          events: [],
          honors: [
            {
              image: {
                filename: "briefcase.png",
                contentType: "image/png",
                url: "https://efuse.gg/static/images/briefcase.png"
              },
              _id: "605b19a9404d2b1144601555",
              title: "Testing",
              subTitle: "event",
              description: "<p>ertkl;lkhgfdxcnm,nppujf</p>"
            }
          ],
          playerCards: [],
          videoCarousel: [],
          createdAt: "2021-03-24T07:33:31.061Z",
          updatedAt: "2021-03-24T10:54:23.342Z",
          __v: 1,
          about:
            '<p><span style="color: rgb(32, 33, 36);">What is Lorem Ipsum Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has?</span></p>'
        },
        authorType: "organizations",
        createdAt: "2020-12-21T13:46:27.784Z",
        updatedAt: "2021-07-12T10:03:33.435Z",
        __v: 1,
        publishDate: "2020-12-21T13:46:27.784Z",
        id: "5fe0a7334c161b8c11d83524",
        url: "/news/professional-development/intercom"
      }
    ],
    totalDocs: 85,
    limit: 9,
    totalPages: 10,
    page: 1,
    pagingCounter: 1,
    hasPrevPage: false,
    hasNextPage: true,
    prevPage: null,
    nextPage: 2
  }
};
