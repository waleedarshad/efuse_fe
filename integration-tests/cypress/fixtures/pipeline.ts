export const validGames = ["League of Legends", "Valorant", "AimLab", "Overwatch", "Rocket League", "Fortnite"];

export const paginatedLOLLeaderboardGraphql = {
  data: {
    getPaginatedLeagueOfLegendsLeaderboard: {
      docs: [
        {
          rank: 1,
          user: {
            name: "perrylol",
            username: "perrylol",
            profilePicture: {
              filename: "headshots.PNG",
              url: "https://scontent.efcdn.io/uploads/users/1623110784120-headshots.PNG",
              __typename: "File"
            },
            __typename: "User"
          },
          recruitmentProfile: {
            highSchool: "Grand Arts High School",
            graduationClass: "2022",
            city: "Monterey Park",
            state: "California",
            __typename: "RecruitmentProfile"
          },
          stats: {
            leagueInfo: [
              {
                summonerName: "perryjg",
                rank: "I",
                tier: "CHALLENGER",
                queueType: "RANKED_SOLO_5x5",
                __typename: "LeagueInfo"
              }
            ],
            __typename: "LeagueOfLegendsStats"
          },
          __typename: "PipelineLeaderboard"
        },
        {
          rank: 2,
          user: {
            name: "Airawn",
            username: "Airawn",
            profilePicture: {
              filename: "mindblown_white.png",
              url: "https://cdn.efuse.gg/uploads/static/global/mindblown_white.png",
              __typename: "File"
            },
            __typename: "User"
          },
          recruitmentProfile: {
            highSchool: "eFuse High School",
            graduationClass: "2023",
            city: "St. Louis",
            state: "Missouri",
            __typename: "RecruitmentProfile"
          },
          stats: {
            leagueInfo: [
              {
                summonerName: "Airawn",
                rank: "I",
                tier: "PLATINUM",
                queueType: "RANKED_FLEX_SR",
                __typename: "LeagueInfo"
              },
              {
                summonerName: "Airawn",
                rank: "I",
                tier: "GRANDMASTER",
                queueType: "RANKED_SOLO_5x5",
                __typename: "LeagueInfo"
              }
            ],
            __typename: "LeagueOfLegendsStats"
          },
          __typename: "PipelineLeaderboard"
        },
        {
          rank: 3,
          user: {
            name: "Steven Hua",
            username: "Katsuna",
            profilePicture: {
              filename: "mindblown_white.png",
              url: "https://cdn.efuse.gg/uploads/static/global/mindblown_white.png",
              __typename: "File"
            },
            __typename: "User"
          },
          recruitmentProfile: {
            highSchool: "Pompton Lakes High School",
            graduationClass: "2024",
            city: "Pompton Lakes",
            state: "New Jersey",
            __typename: "RecruitmentProfile"
          },
          stats: {
            leagueInfo: [
              {
                summonerName: "bard for u",
                rank: "IV",
                tier: "DIAMOND",
                queueType: "RANKED_FLEX_SR",
                __typename: "LeagueInfo"
              },
              {
                summonerName: "bard for u",
                rank: "I",
                tier: "MASTER",
                queueType: "RANKED_SOLO_5x5",
                __typename: "LeagueInfo"
              }
            ],
            __typename: "LeagueOfLegendsStats"
          },
          __typename: "PipelineLeaderboard"
        },
        {
          rank: 4,
          user: {
            name: "Andy Wu",
            username: "Aleakath",
            profilePicture: {
              filename: "mindblown_white.png",
              url: "https://cdn.efuse.gg/uploads/static/global/mindblown_white.png",
              __typename: "File"
            },
            __typename: "User"
          },
          recruitmentProfile: {
            highSchool: "Alhambra High School",
            graduationClass: "2022",
            city: "Alhambra",
            state: "California",
            __typename: "RecruitmentProfile"
          },
          stats: {
            leagueInfo: [
              {
                summonerName: "Aleakath",
                rank: "I",
                tier: "MASTER",
                queueType: "RANKED_SOLO_5x5",
                __typename: "LeagueInfo"
              },
              {
                summonerName: "Aleakath",
                rank: "I",
                tier: "PLATINUM",
                queueType: "RANKED_FLEX_SR",
                __typename: "LeagueInfo"
              }
            ],
            __typename: "LeagueOfLegendsStats"
          },
          __typename: "PipelineLeaderboard"
        },
        {
          rank: 5,
          user: {
            name: "EnSaneZ",
            username: "EnsaneZ",
            profilePicture: {
              filename: "mindblown_white.png",
              url: "https://cdn.efuse.gg/uploads/static/global/mindblown_white.png",
              __typename: "File"
            },
            __typename: "User"
          },
          recruitmentProfile: {
            highSchool: "Bayside High School",
            graduationClass: "2025",
            city: "Whitestone",
            state: "New York",
            __typename: "RecruitmentProfile"
          },
          stats: {
            leagueInfo: [
              {
                summonerName: "EnSaneZ",
                rank: "I",
                tier: "MASTER",
                queueType: "RANKED_SOLO_5x5",
                __typename: "LeagueInfo"
              },
              {
                summonerName: "EnSaneZ",
                rank: "I",
                tier: "SILVER",
                queueType: "RANKED_FLEX_SR",
                __typename: "LeagueInfo"
              }
            ],
            __typename: "LeagueOfLegendsStats"
          },
          __typename: "PipelineLeaderboard"
        },
        {
          rank: 6,
          user: {
            name: "Samuel Chen",
            username: "BenefactorSam",
            profilePicture: {
              filename: "mindblown_white.png",
              url: "https://cdn.efuse.gg/uploads/static/global/mindblown_white.png",
              __typename: "File"
            },
            __typename: "User"
          },
          recruitmentProfile: {
            highSchool: "Troy High School",
            graduationClass: "2022",
            city: "Chino Hills",
            state: "California",
            __typename: "RecruitmentProfile"
          },
          stats: {
            leagueInfo: [
              {
                summonerName: "Benefactor Sam",
                rank: "III",
                tier: "GOLD",
                queueType: "RANKED_FLEX_SR",
                __typename: "LeagueInfo"
              },
              {
                summonerName: "Benefactor Sam",
                rank: "I",
                tier: "MASTER",
                queueType: "RANKED_SOLO_5x5",
                __typename: "LeagueInfo"
              }
            ],
            __typename: "LeagueOfLegendsStats"
          },
          __typename: "PipelineLeaderboard"
        },
        {
          rank: 7,
          user: {
            name: "Alex Chea",
            username: "Toastyalex",
            profilePicture: {
              filename: "mindblown_white.png",
              url: "https://cdn.efuse.gg/uploads/static/global/mindblown_white.png",
              __typename: "File"
            },
            __typename: "User"
          },
          recruitmentProfile: {
            highSchool: "Upper Dublin High School",
            graduationClass: "2024",
            city: "Ambler",
            state: "Pennsylvania",
            __typename: "RecruitmentProfile"
          },
          stats: {
            leagueInfo: [
              {
                summonerName: "ToastyAlex",
                rank: "I",
                tier: "MASTER",
                queueType: "RANKED_SOLO_5x5",
                __typename: "LeagueInfo"
              },
              {
                summonerName: "ToastyAlex",
                rank: "II",
                tier: "PLATINUM",
                queueType: "RANKED_FLEX_SR",
                __typename: "LeagueInfo"
              }
            ],
            __typename: "LeagueOfLegendsStats"
          },
          __typename: "PipelineLeaderboard"
        },
        {
          rank: 8,
          user: {
            name: "billy",
            username: "billyworth",
            profilePicture: {
              filename: "janna.PNG",
              url: "https://scontent.efcdn.io/uploads/users/1627174601595-janna.PNG",
              __typename: "File"
            },
            __typename: "User"
          },
          recruitmentProfile: {
            highSchool: "Cohasset High School",
            graduationClass: "2023",
            city: "Cohasset",
            state: "Massachusetts",
            __typename: "RecruitmentProfile"
          },
          stats: {
            leagueInfo: [
              {
                summonerName: "billy worth",
                rank: "I",
                tier: "MASTER",
                queueType: "RANKED_SOLO_5x5",
                __typename: "LeagueInfo"
              },
              {
                summonerName: "billy worth",
                rank: "II",
                tier: "PLATINUM",
                queueType: "RANKED_FLEX_SR",
                __typename: "LeagueInfo"
              }
            ],
            __typename: "LeagueOfLegendsStats"
          },
          __typename: "PipelineLeaderboard"
        },
        {
          rank: 9,
          user: {
            name: "Shockey",
            username: "Shockey",
            profilePicture: {
              filename: "mindblown_white.png",
              url: "https://cdn.efuse.gg/uploads/static/global/mindblown_white.png",
              __typename: "File"
            },
            __typename: "User"
          },
          recruitmentProfile: {
            highSchool: "Dr. Norman Bethune Collegiate Institute",
            graduationClass: "2024",
            city: "Toronto",
            state: "Ontario",
            __typename: "RecruitmentProfile"
          },
          stats: {
            leagueInfo: [
              {
                summonerName: "Shockey",
                rank: "I",
                tier: "MASTER",
                queueType: "RANKED_SOLO_5x5",
                __typename: "LeagueInfo"
              },
              {
                summonerName: "Shockey",
                rank: "II",
                tier: "DIAMOND",
                queueType: "RANKED_FLEX_SR",
                __typename: "LeagueInfo"
              }
            ],
            __typename: "LeagueOfLegendsStats"
          },
          __typename: "PipelineLeaderboard"
        },
        {
          rank: 10,
          user: {
            name: "Ryan Phu",
            username: "LordPho",
            profilePicture: {
              filename: "mindblown_white.png",
              url: "https://cdn.efuse.gg/uploads/static/global/mindblown_white.png",
              __typename: "File"
            },
            __typename: "User"
          },
          recruitmentProfile: {
            highSchool: "MarkKeppel HighSchool",
            graduationClass: "2022",
            city: "Rosemead",
            state: "California",
            __typename: "RecruitmentProfile"
          },
          stats: {
            leagueInfo: [
              {
                summonerName: "Phushimi",
                rank: "I",
                tier: "MASTER",
                queueType: "RANKED_SOLO_5x5",
                __typename: "LeagueInfo"
              }
            ],
            __typename: "LeagueOfLegendsStats"
          },
          __typename: "PipelineLeaderboard"
        }
      ],
      limit: 10,
      page: 1,
      totalPages: 9,
      nextPage: 2,
      prevPage: null,
      hasPrevPage: false,
      hasNextPage: true,
      totalDocs: 84,
      __typename: "PaginatedPipelineLeaderboard"
    }
  }
};
