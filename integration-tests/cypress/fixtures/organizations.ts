export const organizationsGraphqlResponse = {
  data: {
    OrganizationAlgoliaSearch: {
      results: [
        {
          exhaustiveFacetsCount: true,
          exhaustiveNbHits: true,
          facets: {
            scale: {
              "1": 65,
              "2": 102,
              "3": 25,
              "4": 22,
              "5": 5,
              "6": 30,
              "1-99": 732,
              "100-999": 74,
              "1000+": 48
            },
            status: {
              Request: 625,
              Open: 500,
              Closed: 165,
              Invite: 9
            },
            "verified.status": {
              false: 1203,
              true: 96
            },
            organizationType: {
              "Professional Team": 468,
              Community: 232,
              Other: 129,
              "Scholastic (HS) Team": 98,
              Brand: 80,
              "Collegiate Team": 76,
              "Amateur Team": 67,
              Company: 45,
              "Non-Profit Corp.": 38,
              "Educational Institution": 18
            }
          },
          facets_stats: {
            scale: {
              min: 1,
              max: 6,
              avg: 2,
              sum: 637
            }
          },
          hitsPerPage: 9,
          index: "production_organizations",
          nbHits: 1299,
          nbPages: 112,
          page: 0,
          params:
            "analytics=false&distinct=false&enablePersonalization=true&facets=%5B%22organizationType%22%2C%22status%22%2C%22scale%22%2C%22verified.status%22%5D&filters=&highlightPostTag=%3C%2Fais-highlight-0000000000%3E&highlightPreTag=%3Cais-highlight-0000000000%3E&hitsPerPage=9&maxValuesPerFacet=10&query=&tagFilters=",
          processingTimeMS: 3,
          query: "",
          renderingContent: {},
          hits: [
            {
              _id: "5de96ece2675c74369af2ae3",
              name: "eFuse",
              about:
                "<p>Ever want to game for a living? What about going to college on a scholarship to play on a varsity esports team? Want to become a sponsored streamer? Building an eFuse portfolio lets you showcase your skills and apply for all sorts of esports and gaming opportunities, and not just those related to playing. We have jobs for those who might not play professionally but want to build a career in the industry. We also have tons of tools for you to build and manage your community or organization. Regardless of why, if gaming is a part of who you are, eFuse has what you need.</p>",
              description: "Where Gamers Get Discovered",
              followersCount: 12305,
              verified: {
                status: true
              },
              _highlightResult: {
                name: {
                  value: "eFuse",
                  matchLevel: "none",
                  matchedWords: []
                },
                description: {
                  value: "Where Gamers Get Discovered",
                  matchLevel: "none",
                  matchedWords: []
                }
              },
              membersCount: 27,
              headerImage: {
                url: "https://efuse.s3.amazonaws.com/uploads/organizations/1616512118669-eFuseOrgHeader1.png"
              },
              profileImage: {
                url:
                  "https://efuse.s3.amazonaws.com/uploads/organizations/1575579342062-Profile%20picture%205%20CENTEREDD%20small.png"
              },
              status: "Closed",
              shortName: "efuse",
              publishStatus: "visible",
              user: {
                _id: "5ddd959ab865946865a20117",
                username: "CommunityCarl",
                name: "Carl Klamar"
              },
              isCurrentUserAFollower: null,
              isCurrentUserAMember: null,
              currentUserRequestToJoin: null
            },
            {
              _id: "5e25e915b49339ba39f8c37b",
              name: "EasternMediaGG",
              about:
                "<p>We are a streamer based community and team but even more so we are a gaming community. We are a group dedicated to growing our members' streams and social media and to making a footprint as one of the best streaming communities to be part of for Twitch and Mixer. We supply advice on how to improve livestreams for both quality and content as well as working with new members to become affiliates on Twitch more quickly and easily. Our roster is growing daily with more streamers reaching affiliate and partnership on Twitch and partnership on Mixer. We are constantly growing together and organizing community events, collaborations, and providing entertainment you can't get anywhere else. Whether just starting streaming or looking to continue to build your broadcasting brand, we are home to a team with various backgrounds to network with. Expect to find a community member willing to help you with everything from marketing, branding, growing your audience, networking and all the way to learning how to build a website for your brand.</p>",
              description: "Multi-platform entertainment organization and leader in professional branding and media.",
              followersCount: 3468,
              verified: {
                status: true
              },
              _highlightResult: {
                name: {
                  value: "EasternMediaGG",
                  matchLevel: "none",
                  matchedWords: []
                },
                description: {
                  value: "Multi-platform entertainment organization and leader in professional branding and media.",
                  matchLevel: "none",
                  matchedWords: []
                }
              },
              membersCount: 126,
              headerImage: {
                url: "https://efuse.s3.amazonaws.com/uploads/organizations/1629158200570-2021PINKbanner.png"
              },
              profileImage: {
                url: "https://efuse.s3.amazonaws.com/uploads/organizations/1629158200557-eMAVI2019_PINK.png"
              },
              status: "Request",
              shortName: "easternmediagg",
              publishStatus: "visible",
              user: {
                _id: "5e1e0ebbd5dbf382a540113c",
                username: "zimm",
                name: "Steven Zimmer"
              },
              isCurrentUserAFollower: null,
              isCurrentUserAMember: null,
              currentUserRequestToJoin: null
            },
            {
              _id: "5e29e01a6b69704e2ddf7955",
              name: "Omnipotence",
              about: "<p>coming soon..</p>",
              description: "Team Omnipotence.....",
              followersCount: 2348,
              verified: {
                status: true
              },
              _highlightResult: {
                name: {
                  value: "Omnipotence",
                  matchLevel: "none",
                  matchedWords: []
                },
                description: {
                  value: "Team Omnipotence.....",
                  matchLevel: "none",
                  matchedWords: []
                }
              },
              membersCount: 1,
              headerImage: {
                url:
                  "https://efuse.s3.amazonaws.com/uploads/organizations/1629260096846-5429061-blank-empty-rectangle-round-shape-square-icon-round-square-png-512_512_preview.png"
              },
              profileImage: {
                url: "https://efuse.s3.amazonaws.com/uploads/organizations/1629260096845-rounded-square-459344.png"
              },
              status: "Request",
              shortName: "omnipotence",
              publishStatus: "visible",
              user: {
                _id: "5df40e71cf045b7fb398617d",
                username: "Riou",
                name: "Nicholas Sallot"
              },
              isCurrentUserAFollower: null,
              isCurrentUserAMember: null,
              currentUserRequestToJoin: null
            },
            {
              _id: "5e55a16ffad21feb2b5a5179",
              name: "Streamers Connected",
              about:
                "<p>WELCOME TO STREAMERS CONNECTED</p><p>A community for streamers by streamers to bring together content creators, developers, artists, company reps, and even viewers and fans. By joining us, you've taken your first step in making friends with like-minded and passionate people who want to support, network, and collaborate with each other.</p>",
              description:
                "Streamers Connected, a community “For Streamers by Streamers” to bring together content creators, developers, artists, company reps and even viewers and fans.",
              followersCount: 2181,
              verified: {
                status: false
              },
              _highlightResult: {
                name: {
                  value: "Streamers Connected",
                  matchLevel: "none",
                  matchedWords: []
                },
                description: {
                  value:
                    "Streamers Connected, a community “For Streamers by Streamers” to bring together content creators, developers, artists, company reps and even viewers and fans.",
                  matchLevel: "none",
                  matchedWords: []
                }
              },
              membersCount: 1124,
              headerImage: {
                url: "https://efuse.gg/static/images/mindblown.png"
              },
              profileImage: {
                url:
                  "https://efuse.s3.amazonaws.com/uploads/organizations/1582670191140-streamers_connected_dark_logo.png"
              },
              status: "Open",
              shortName: "streamers-connected",
              publishStatus: "visible",
              user: {
                _id: "5e499a8b3f30c480a99dce9d",
                username: "streamersconnected",
                name: "Streamers Connected"
              },
              isCurrentUserAFollower: null,
              isCurrentUserAMember: null,
              currentUserRequestToJoin: null
            },
            {
              _id: "5e31fa58fa4a3bd1338ee00c",
              name: "Down Two Earth ",
              about: "<p><br></p>",
              description:
                "Down Two Earth is a rising esports organization established and based in Pittsburgh, PA part owned by NFL Star Ryan Shazier. While maintaining classic roots to the esports world, D2E's leading focus is to enter the scene as a lifestyle brand. ",
              followersCount: 1613,
              verified: {
                status: true
              },
              _highlightResult: {
                name: {
                  value: "Down Two Earth ",
                  matchLevel: "none",
                  matchedWords: []
                },
                description: {
                  value:
                    "Down Two Earth is a rising esports organization established and based in Pittsburgh, PA part owned by NFL Star Ryan Shazier. While maintaining classic roots to the esports world, D2E's leading focus is to enter the scene as a lifestyle brand. ",
                  matchLevel: "none",
                  matchedWords: []
                }
              },
              membersCount: 9,
              headerImage: {
                url: "https://efuse.s3.amazonaws.com/uploads/organizations/1602474415396-Header-2.0.png"
              },
              profileImage: {
                url: "https://efuse.s3.amazonaws.com/uploads/organizations/1602474415366-AVI.png"
              },
              status: "Request",
              shortName: "down-two-earth",
              publishStatus: "visible",
              user: {
                _id: "5e31f170b2fc8b71c441cd83",
                username: "MasonBush",
                name: "Mason Bush"
              },
              isCurrentUserAFollower: null,
              isCurrentUserAMember: null,
              currentUserRequestToJoin: null
            },
            {
              _id: "5e25ee0e6716a25e346ade67",
              name: "Rectify Esports",
              about:
                "<p>Rectify Esports was fully established in 2017 which was founded through media outlet, Rectify Gaming.</p><p>Rectify provides entertainment with their stream team across Twitch. We aim to compete at a high level through Brawlhalla, Super Smash Bros. and CS:GO</p><p><br></p><p>#GetRectified</p>",
              description:
                "Professional North American Esports Organization. Competing in Brawlhalla, Super Smash Bros, Tekken 7 and CS:GO\n",
              followersCount: 1433,
              verified: {
                status: true
              },
              _highlightResult: {
                name: {
                  value: "Rectify Esports",
                  matchLevel: "none",
                  matchedWords: []
                },
                description: {
                  value:
                    "Professional North American Esports Organization. Competing in Brawlhalla, Super Smash Bros, Tekken 7 and CS:GO\n",
                  matchLevel: "none",
                  matchedWords: []
                }
              },
              membersCount: 4,
              headerImage: {
                url: "https://efuse.s3.amazonaws.com/uploads/organizations/1627660694821-RCG-2021-Header%20%281%29.jpg"
              },
              profileImage: {
                url: "https://efuse.s3.amazonaws.com/uploads/organizations/1627660694759-RCG-2021-Avi.jpg"
              },
              status: "Closed",
              shortName: "rectify-esports",
              publishStatus: "visible",
              user: {
                _id: "5defcc54c8a05bc3ea4e1f7d",
                username: "Tyboy",
                name: "Tyler Nienburg"
              },
              isCurrentUserAFollower: null,
              isCurrentUserAMember: null,
              currentUserRequestToJoin: null
            },
            {
              _id: "5e4d5b0b56580fdafe0327d6",
              name: "Mystic Gang ",
              about: "<p>(Coming Soon)</p>",
              description:
                "Content Creation & E-Sports Team With Fun Loving People And A Community For Gamers All Over The World! #MysticON\n",
              followersCount: 1375,
              verified: {
                status: false
              },
              _highlightResult: {
                name: {
                  value: "Mystic Gang ",
                  matchLevel: "none",
                  matchedWords: []
                },
                description: {
                  value:
                    "Content Creation & E-Sports Team With Fun Loving People And A Community For Gamers All Over The World! #MysticON\n",
                  matchLevel: "none",
                  matchedWords: []
                }
              },
              membersCount: 6,
              headerImage: {
                url:
                  "https://efuse.s3.amazonaws.com/uploads/organizations/1596122610771-1595877054684-1595865476700-20200727_093604_0000%5B1%5D%5B1%5D%5B1%5D.png"
              },
              profileImage: {
                url:
                  "https://efuse.s3.amazonaws.com/uploads/organizations/1596122610584-1595877054520-mt%2520logo%2520white%2520stroke%2520black%2520bg%2520%281%29%2520fixed%5B1%5D.png"
              },
              status: "Request",
              shortName: "mystic-gang",
              publishStatus: "visible",
              user: {
                _id: "5dfd0926c02b4e17dd4f819c",
                username: "clashwitha_YT",
                name: "Clashwitha _YT"
              },
              isCurrentUserAFollower: null,
              isCurrentUserAMember: null,
              currentUserRequestToJoin: null
            },
            {
              _id: "5e25f50481b28d81eb276a5c",
              name: "Rectify Gaming Arena",
              about:
                "<p>The Rectify Gaming Arena is a tournament host related to Rectify Esports &amp; Rectify Gaming. We host games in a variety of titles across PUBG, Halo, Call of Duty, Brawlhalla, Super Smash Bros, Mortal Kombat and more!</p>",
              description:
                "The official Tournament platform for Rectify Gaming providing online tournaments for a variety of games through all platforms. Established 2019. ",
              followersCount: 1279,
              verified: {
                status: true
              },
              _highlightResult: {
                name: {
                  value: "Rectify Gaming Arena",
                  matchLevel: "none",
                  matchedWords: []
                },
                description: {
                  value:
                    "The official Tournament platform for Rectify Gaming providing online tournaments for a variety of games through all platforms. Established 2019. ",
                  matchLevel: "none",
                  matchedWords: []
                }
              },
              membersCount: 4,
              headerImage: {
                url: "https://efuse.s3.amazonaws.com/uploads/organizations/1579545860612-600x200%20%281%29.jpg"
              },
              profileImage: {
                url: "https://efuse.s3.amazonaws.com/uploads/organizations/1579545860571-KWAKRcRt_400x400.jpg"
              },
              status: "Invite",
              shortName: "rectify-gaming-arena",
              publishStatus: "visible",
              user: {
                _id: "5defcc54c8a05bc3ea4e1f7d",
                username: "Tyboy",
                name: "Tyler Nienburg"
              },
              isCurrentUserAFollower: null,
              isCurrentUserAMember: null,
              currentUserRequestToJoin: null
            },
            {
              _id: "5ea723c5593e7a1aae8ccb26",
              name: "Misfits Gaming",
              about:
                '<p><span style="color: rgba(0, 0, 0, 0.6);">Misfits Gaming Group is a premier professional esports organization that owns and operates a portfolio of competitive esports teams across multiple global video games including Riot Games’ League of Legends European Championship, Epic Games’ Fortnite, through its brand Florida Mayhem, has a team franchise in Blizzard’s Overwatch League as well as the Florida Mutineers franchise in Activision-Blizzard’s upcoming Call of Duty League. </span></p>',
              description:
                "Premier professional esports organization that owns and operates Misfits Gaming (League of Legends and Fortnite), Florida Mayhem (Overwatch), and Florida Mutineers (Call of Duty). ",
              followersCount: 1123,
              verified: {
                status: true
              },
              _highlightResult: {
                name: {
                  value: "Misfits Gaming",
                  matchLevel: "none",
                  matchedWords: []
                },
                description: {
                  value:
                    "Premier professional esports organization that owns and operates Misfits Gaming (League of Legends and Fortnite), Florida Mayhem (Overwatch), and Florida Mutineers (Call of Duty). ",
                  matchLevel: "none",
                  matchedWords: []
                }
              },
              membersCount: 3,
              headerImage: {
                url: "https://efuse.s3.amazonaws.com/uploads/organizations/1588011973333-Header-twitter.png"
              },
              profileImage: {
                url:
                  "https://efuse.s3.amazonaws.com/uploads/organizations/1588011973215-67244499_919984768361364_1488647402939219968_o.jpg"
              },
              status: "Request",
              shortName: "misfits-gaming",
              publishStatus: "visible",
              user: {
                _id: "5ea70c649857671c3055e7fd",
                username: "annielian",
                name: "Annie Lian"
              },
              isCurrentUserAFollower: null,
              isCurrentUserAMember: null,
              currentUserRequestToJoin: null
            }
          ]
        }
      ]
    }
  }
};

export const organizationSlugResponse = {
  organization: {
    _id: "5de96ece2675c74369af2ae3",
    verified: {
      status: true,
      verifiedAt: "2019-12-20T14:23:42.410Z",
      verifiedBy: "5d91497ee357285a46187deb"
    },
    captains: [
      "5d91497ee357285a46187deb",
      "5ddd9967a3980582bcf3fead",
      "5de9167a52cc7602284bb545",
      "5d914ec3e357285a46187dec",
      "5ddd9df9a3980593e6f3feaf",
      "5e477264f7fe74c3ace22927",
      "5da76a23226cef44e5464eb1",
      "5eea1a87b55eefcce23c3d67",
      "5f5bb81f6af87d0011e57f00",
      "5e44b62312452335ad0f64ba",
      "5ddd9e9a1ce1d4367bc6d994",
      "5f5a5617b7c11f00157f855f",
      "5f2832578a3286000b4e2ea2",
      "5e9922386327934c0e7e97bb",
      "5eea5eb0ef712eaab6aca6ac",
      "5e1ecc7ea8a6c15ae372c08c",
      "5e384e268ddd11cbe733f36b",
      "605e61908092290012c49283",
      "60abfde716362a0029563515",
      "5dfbdb15fec4a5f1528d9af3",
      "602ede67fcb51d0013b8213f",
      "6058d1025d374a0012cd0261"
    ],
    name: "eFuse",
    scale: "2",
    email: "info@efuse.io",
    location: "Columbus, OH, USA",
    phoneNumber: "+1 (740)-804-5445",
    website: "https://efuse.gg/",
    organizationType: "Company",
    status: "Closed",
    description: "Where Gamers Get Discovered",
    toBeVerified: true,
    externalAccountLink: "",
    profileImage: {
      filename: "Profile picture 5 CENTEREDD small.png",
      contentType: "image/png",
      url:
        "https://efuse.s3.amazonaws.com/uploads/organizations/1575579342062-Profile%20picture%205%20CENTEREDD%20small.png"
    },
    headerImage: {
      filename: "eFuseOrgHeader1.png",
      contentType: "image/jpeg",
      url: "https://efuse.s3.amazonaws.com/uploads/organizations/1616512118669-eFuseOrgHeader1.png"
    },
    user: "5ddd959ab865946865a20117",
    createdAt: "2019-12-05T20:55:42.522Z",
    __v: 55,
    promoVideo: {
      isVideo: false,
      title: "Further YOUR career in Esports! ",
      description:
        "By focusing on building features for gamers and specifically those who want to make gaming a career, eFuse has built a platform that cuts through the noise and provides the most and highest quality gaming opportunities anywhere. ",
      embeddedUrl: "https://www.youtube.com/watch?v=ayeo1WXRDWM"
    },
    about:
      "<p>Ever want to game for a living? What about going to college on a scholarship to play on a varsity esports team? Want to become a sponsored streamer? Building an eFuse portfolio lets you showcase your skills and apply for all sorts of esports and gaming opportunities, and not just those related to playing. We have jobs for those who might not play professionally but want to build a career in the industry. We also have tons of tools for you to build and manage your community or organization. Regardless of why, if gaming is a part of who you are, eFuse has what you need.</p>",
    events: [
      {
        image: {
          filename: "briefcase.png",
          contentType: "image/png",
          url: "https://efuse.gg/static/images/briefcase.png"
        },
        _id: "5e1ce9839a7ddde2bc64deb8",
        title: "GDEX 2019",
        subTitle: "chocoTaco (Partner) Meetup",
        eventDate: "2019-10-13T16:00:00.000Z",
        description:
          "<p>Attended ChocoTaco fan meetup under eFuse branding. ChocoTaco is a streamer for TSM and also an amazing partner of ours.</p>"
      },
      {
        image: {
          filename: "warzone-wednesdays-bracket-time-standings-rules-week.png",
          contentType: "image/png",
          url:
            "https://efuse.s3.amazonaws.com/uploads/organization-accolades/1588368894451-warzone-wednesdays-bracket-time-standings-rules-week.png"
        },
        _id: "5eac95ff5ab612b3378e6548",
        title: "Warzone Wednesday",
        subTitle: "Leaderboards Provider",
        eventDate: "2020-03-01T17:00:00.000Z",
        description:
          "<p>eFuse is proud to provide the leaderboard and bracket for Warzone Wednesday via the eRena service</p>"
      },
      {
        image: {
          filename: "briefcase.png",
          contentType: "image/png",
          url: "https://efuse.gg/static/images/briefcase.png"
        },
        _id: "5eac96ecfab3534df0e66c73",
        title: "Fandom Legends Warzone Pro-Am",
        subTitle: "Event Host",
        eventDate: "2020-04-19T16:00:00.000Z",
        description:
          "<p>eFuse was happy to host the Fandom Legends Warzone eFuse Pro-Am with Leaderboard and Bracket Standings and the full event taking place with the eRena service</p><p>eFuse also hosted opportunities for one community member to participate on each team with one pro streamer and two pro athletes!</p>"
      }
    ],
    honors: [
      {
        image: {
          filename: "briefcase.png",
          contentType: "image/png",
          url: "https://efuse.gg/static/images/briefcase.png"
        },
        _id: "5e1cea2a537b5ab1396ecdb7",
        title: "Early-Stage Startup Finalist",
        subTitle: "Ohio Startup Culture Awards",
        description:
          '<ul><li>eFuse was nominated as having one of the best start-up cultures in Ohio in 2019</li><li><a href="https://weare.techohio.ohio.gov/2019/10/23/the-nominations-are-in-vote-now-for-ohios-best-startup-culture/" target="_blank">https://weare.techohio.ohio.gov/2019/10/23/the-nominations-are-in-vote-now-for-ohios-best-startup-culture/</a></li></ul>'
      }
    ],
    playerCards: [
      {
        image: {
          filename: "BensonHeadshot.jpg",
          contentType: "image/jpeg",
          url: "https://efuse.s3.amazonaws.com/uploads/player-card/1579544606176-BensonHeadshot.jpg"
        },
        _id: "5e25f01ee3e7ae2ff765708b",
        title: "Matt Benson",
        subTitle: "Founder & CEO",
        url: "https://efuse.gg/users/portfolio?id=5ddd9df9a3980593e6f3feaf"
      },
      {
        image: {
          filename: "IMG_4077.jpg",
          contentType: "image/jpeg",
          url: "https://efuse.s3.amazonaws.com/uploads/player-card/1588365559475-IMG_4077.jpg"
        },
        _id: "5e2733610354cc76b186acf7",
        title: "Austin May",
        subTitle: "Software Engineer",
        url: "https://efuse.gg/users/portfolio?id=5d914ec3e357285a46187dec"
      },
      {
        image: {
          filename: "JamesHeadshot.jpg",
          contentType: "image/jpeg",
          url: "https://efuse.s3.amazonaws.com/uploads/player-card/1579627399844-JamesHeadshot.jpg"
        },
        _id: "5e27338bad928b1e2fe3f190",
        title: "James Bevins",
        subTitle: "Creative Director",
        url: "https://efuse.gg/users/portfolio?id=5ddd9e9a1ce1d4367bc6d994"
      },
      {
        image: {
          filename: "shuff.jpeg",
          contentType: "image/jpeg",
          url: "https://efuse.s3.amazonaws.com/uploads/player-card/1588366963524-shuff.jpeg"
        },
        _id: "5e2733a38b1e332fb2e2e59c",
        title: "Patrick Shuff",
        subTitle: "Chief Technology Officer",
        url: "https://efuse.gg/users/portfolio?id=5d91497ee357285a46187deb"
      },
      {
        image: {
          filename: "Tom Profile Pic.png",
          contentType: "image/png",
          url: "https://efuse.s3.amazonaws.com/uploads/player-card/1584319916717-Tom%20Profile%20Pic.png"
        },
        _id: "5e2733c8d1064585cf1f43c4",
        title: "Tom Newton",
        subTitle: "Director of Operations",
        url: "https://efuse.gg/users/portfolio?id=5da76a23226cef44e5464eb1"
      },
      {
        image: {
          filename: "CarlHeadshot.jpg",
          contentType: "image/jpeg",
          url: "https://efuse.s3.amazonaws.com/uploads/player-card/1579628278507-CarlHeadshot.jpg"
        },
        _id: "5e2734058b1e338dbce2e59f",
        title: "Carl Klamar",
        subTitle: "Community Engagement",
        url: "https://efuse.gg/users/portfolio?id=5ddd959ab865946865a20117"
      },
      {
        image: {
          filename: "Screen Shot 2020-02-14 at 10.47.24 AM.png",
          contentType: "image/png",
          url:
            "https://efuse.s3.amazonaws.com/uploads/player-card/1581697083807-Screen%20Shot%202020-02-14%20at%2010.47.24%20AM.png"
        },
        _id: "5e27346fad928b2961e3f191",
        title: "Brian Gorham",
        subTitle: "Sr. Software Engineer",
        url: "https://efuse.gg/users/portfolio?id=5de9167a52cc7602284bb545"
      },
      {
        image: {
          filename: "KleinHeadShot.jpg",
          contentType: "image/jpeg",
          url: "https://efuse.s3.amazonaws.com/uploads/player-card/1579627671646-KleinHeadShot.jpg"
        },
        _id: "5e273498e0fbc112bc454d92",
        title: "Patrick Klein",
        subTitle: "Chief Strategy Officer",
        url: "https://efuse.gg/users/portfolio?id=5ddd9967a3980582bcf3fead"
      },
      {
        image: {
          filename: "Image from iOS (5).jpg",
          contentType: "image/jpeg",
          url: "https://efuse.s3.amazonaws.com/uploads/player-card/1602613705845-Image%20from%20iOS%20%285%29.jpg"
        },
        _id: "5eac8d4bfab353e724e66c69",
        title: "Mitch Long",
        subTitle: "Business Dev/Marketing",
        url: "https://efuse.gg/users/portfolio?id=5deea60602db500487497a83"
      },
      {
        image: {
          filename: "varun.jpeg",
          contentType: "image/jpeg",
          url: "https://efuse.s3.amazonaws.com/uploads/player-card/1588367635932-varun.jpeg"
        },
        _id: "5eac91144880e7bed1d2b546",
        title: "Varun Verma",
        subTitle: "Intern",
        url: "https://efuse.gg/users/portfolio?id=5e44b62312452335ad0f64ba"
      },
      {
        image: {
          filename: "Aditya.jpeg",
          contentType: "image/jpeg",
          url: "https://efuse.s3.amazonaws.com/uploads/player-card/1588367770493-Aditya.jpeg"
        },
        _id: "5eac919afab353435de66c6d",
        title: "Aditya Joshi",
        subTitle: "Intern",
        url: "https://efuse.gg/users/portfolio?id=5e477264f7fe74c3ace22927"
      }
    ],
    publishStatus: "visible",
    slug: "efuse",
    discordServer: "553235458830893067",
    videoCarousel: [
      {
        uploaded: false,
        _id: "5f1f1bb7fc41de0006fc82b0",
        title: "The Weekly Spark 10",
        url: "https://youtu.be/Gd6QmD81G8k"
      },
      {
        uploaded: false,
        _id: "5f1f1bd451647500061a289f",
        title: "The Weekly Spark 09",
        url: "https://youtu.be/ty9xP4F9xuQ"
      },
      {
        uploaded: false,
        _id: "5f1f1c259dec8a0006b51c1e",
        title: "The Weekly Spark 08",
        url: "https://youtu.be/HYjpT9D_XnU"
      }
    ],
    updatedAt: "2021-09-23T08:35:04.389Z",
    followersCount: 12311,
    promoted: false,
    deleted: false,
    shortName: "efuse",
    games: [],
    associatedUser: {
      _id: "5ddd959ab865946865a20117",
      roles: ["admin", "employee"],
      verified: true,
      firstName: "Carl",
      lastName: "Klamar",
      username: "CommunityCarl",
      profilePicture: {
        filename: "Profile Pic.JPG",
        contentType: "image/jpeg",
        url: "https://cdn.efuse.gg/uploads/users/1577116289210-Profile%20Pic.JPG"
      },
      name: "Carl Klamar"
    },
    membersCount: 27,
    associatedGames: []
  }
};
