export const validatorResponse = {
  valid: true,
  error: ""
};

export const signupUserDetails = {
  email: "integrationtests@efuse.gg",
  username: "integrationtests",
  password: "Integration@Tests#1234",
  firstName: "Integration",
  lastName: "Tests",
  dateOfBirth: "01/01/2000"
};

export const pathwaysResponse = [
  {
    _id: "5ff85e8fde9442939f1e7937",
    name: "Team or Organization Owner",
    description: "",
    active: false,
    webRedirectUrl: "/organizations/new",
    icon: "https://cdn.efuse.gg/uploads/pathways/Owner.svg"
  },
  {
    _id: "5ff85e16de9442939f1e6fc9",
    name: "Casual Gamer",
    description: "",
    active: false,
    webRedirectUrl: "/opportunities/jobs",
    icon: "https://cdn.efuse.gg/uploads/pathways/Casual.svg"
  },
  {
    _id: "5ff85dfade9442939f1e6e1e",
    name: "Competitive Gamer",
    description: "",
    active: true,
    webRedirectUrl: "/organizations",
    icon: "https://cdn.efuse.gg/uploads/pathways/Competitive.svg"
  },
  {
    _id: "5ff85dd9de9442939f1e6bb8",
    name: "Streamer or Content Creator",
    description: "",
    active: true,
    webRedirectUrl: "/lounge/featured",
    icon: "https://cdn.efuse.gg/uploads/pathways/Streamer.svg"
  }
];

export const loginSuccess = {
  refreshToken: "absolutely_fake_refresh_token",
  token: "Bearer token is not about bears",
  user: {
    id: "random_user_id",
    ...signupUserDetails,
    firstRun: false,
    profilePicture: {
      filename: "mindblown_white.png",
      contentType: "image/png",
      url: "https://cdn.efuse.gg/uploads/static/global/mindblown_white.png"
    },
    roles: [],
    traits: [],
    motivations: [],
    locale: "en",
    organizations: [],
    createdAt: new Date(),
    goldSubscriber: false,
    publicTraitsMotivations: false,
    verified: false,
    name: signupUserDetails.firstName + signupUserDetails.lastName
  },
  message: "Logged in successfully!"
};
