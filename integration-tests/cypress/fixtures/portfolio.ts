export const mjbPortfolioResponse = {
  user: {
    _id: "5ddd9df9a3980593e6f3feaf",
    fortnite: {
      epicNickname: "mattchub16",
      accountId: "685e0525-61f7-42bc-9bfd-69ddf5adf12a",
      stats: [
        {
          _id: "5df2d4902b04d10008bfff4f",
          stats: {
            p2: {
              score: {
                label: "Score",
                field: "Score",
                category: "General",
                valueInt: 14728,
                value: "14728",
                percentile: 52,
                displayValue: "14,728"
              },
              top1: {
                label: "Wins",
                field: "Top1",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              top3: {
                label: "Top 3",
                field: "Top3",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              top5: {
                label: "Top 5",
                field: "Top5",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              top6: {
                label: "Top 6",
                field: "Top6",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              top10: {
                label: "Top 10",
                field: "Top10",
                category: "Tops",
                valueInt: 7,
                value: "7",
                percentile: 58,
                displayValue: "7"
              },
              top12: {
                label: "Top 12",
                field: "Top12",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              top25: {
                label: "Top 25",
                field: "Top25",
                category: "Tops",
                valueInt: 29,
                value: "29",
                percentile: 53,
                displayValue: "29"
              },
              kd: {
                label: "K/d",
                field: "KD",
                category: "General",
                valueDec: 0.83,
                value: "0.83",
                percentile: 56,
                displayValue: "0.83"
              },
              winRatio: {
                label: "Win %",
                field: "WinRatio",
                category: "General",
                valueDec: 0,
                value: "0",
                displayValue: "0.00"
              },
              matches: {
                label: "Matches",
                field: "Matches",
                category: "General",
                valueInt: 112,
                value: "112",
                percentile: 54,
                displayValue: "112"
              },
              kills: {
                label: "Kills",
                field: "Kills",
                category: "General",
                valueInt: 93,
                value: "93",
                percentile: 50,
                displayValue: "93"
              },
              minutesPlayed: {
                label: "Time Played",
                field: "MinutesPlayed",
                category: "General",
                valueInt: 0,
                value: "0",
                displayValue: ""
              },
              kpm: {
                label: "Kills/Min",
                field: "KPM",
                category: "General",
                valueDec: 0,
                value: "0",
                displayValue: "0.00"
              },
              kpg: {
                label: "Kills/Match",
                field: "KPG",
                category: "General",
                valueDec: 0.83,
                value: "0.83",
                percentile: 55,
                displayValue: "0.83"
              },
              avgTimePlayed: {
                label: "Avg. Match Time",
                field: "AvgTimePlayed",
                category: "General",
                valueDec: 0,
                value: "0",
                displayValue: ""
              },
              scorePerMatch: {
                label: "Score/Match",
                field: "ScorePerMatch",
                category: "General",
                valueDec: 131.5,
                value: "131.5",
                percentile: 43,
                displayValue: "131.50"
              },
              scorePerMin: {
                label: "Score/Min",
                field: "ScorePerMin",
                category: "General",
                valueDec: 0,
                value: "0",
                displayValue: "0.00"
              }
            },
            p10: {
              score: {
                label: "Score",
                field: "Score",
                category: "General",
                valueInt: 74557,
                value: "74557",
                percentile: 28,
                displayValue: "74,557"
              },
              top1: {
                label: "Wins",
                field: "Top1",
                category: "Tops",
                valueInt: 18,
                value: "18",
                percentile: 17,
                displayValue: "18"
              },
              top3: {
                label: "Top 3",
                field: "Top3",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              top5: {
                label: "Top 5",
                field: "Top5",
                category: "Tops",
                valueInt: 69,
                value: "69",
                percentile: 20,
                displayValue: "69"
              },
              top6: {
                label: "Top 6",
                field: "Top6",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              top10: {
                label: "Top 10",
                field: "Top10",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              top12: {
                label: "Top 12",
                field: "Top12",
                category: "Tops",
                valueInt: 129,
                value: "129",
                percentile: 25,
                displayValue: "129"
              },
              top25: {
                label: "Top 25",
                field: "Top25",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              kd: {
                label: "K/d",
                field: "KD",
                category: "General",
                valueDec: 1.06,
                value: "1.06",
                percentile: 33,
                displayValue: "1.06"
              },
              winRatio: {
                label: "Win %",
                field: "WinRatio",
                category: "General",
                valueDec: 4.7,
                value: "4.7",
                percentile: 13,
                displayValue: "4.70"
              },
              matches: {
                label: "Matches",
                field: "Matches",
                category: "General",
                valueInt: 379,
                value: "379",
                percentile: 34,
                displayValue: "379"
              },
              kills: {
                label: "Kills",
                field: "Kills",
                category: "General",
                valueInt: 383,
                value: "383",
                percentile: 30,
                displayValue: "383"
              },
              minutesPlayed: {
                label: "Time Played",
                field: "MinutesPlayed",
                category: "General",
                valueInt: 0,
                value: "0",
                displayValue: ""
              },
              kpm: {
                label: "Kills/Min",
                field: "KPM",
                category: "General",
                valueDec: 0,
                value: "0",
                displayValue: "0.00"
              },
              kpg: {
                label: "Kills/Match",
                field: "KPG",
                category: "General",
                valueDec: 1.01,
                value: "1.01",
                percentile: 34,
                displayValue: "1.01"
              },
              avgTimePlayed: {
                label: "Avg. Match Time",
                field: "AvgTimePlayed",
                category: "General",
                valueDec: 0,
                value: "0",
                displayValue: ""
              },
              scorePerMatch: {
                label: "Score/Match",
                field: "ScorePerMatch",
                category: "General",
                valueDec: 196.72,
                value: "196.72",
                percentile: 16,
                displayValue: "196.72"
              },
              scorePerMin: {
                label: "Score/Min",
                field: "ScorePerMin",
                category: "General",
                valueDec: 0,
                value: "0",
                displayValue: "0.00"
              }
            },
            p9: {
              score: {
                label: "Score",
                field: "Score",
                category: "General",
                valueInt: 160294,
                value: "160294",
                percentile: 19,
                displayValue: "160,294"
              },
              top1: {
                label: "Wins",
                field: "Top1",
                category: "Tops",
                valueInt: 63,
                value: "63",
                percentile: 10,
                displayValue: "63"
              },
              top3: {
                label: "Top 3",
                field: "Top3",
                category: "Tops",
                valueInt: 138,
                value: "138",
                percentile: 11,
                displayValue: "138"
              },
              top5: {
                label: "Top 5",
                field: "Top5",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              top6: {
                label: "Top 6",
                field: "Top6",
                category: "Tops",
                valueInt: 228,
                value: "228",
                percentile: 13,
                displayValue: "228"
              },
              top10: {
                label: "Top 10",
                field: "Top10",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              top12: {
                label: "Top 12",
                field: "Top12",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              top25: {
                label: "Top 25",
                field: "Top25",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              kd: {
                label: "K/d",
                field: "KD",
                category: "General",
                valueDec: 1.11,
                value: "1.11",
                percentile: 32,
                displayValue: "1.11"
              },
              winRatio: {
                label: "Win %",
                field: "WinRatio",
                category: "General",
                valueDec: 9,
                value: "9",
                percentile: 10,
                displayValue: "9.00"
              },
              matches: {
                label: "Matches",
                field: "Matches",
                category: "General",
                valueInt: 698,
                value: "698",
                percentile: 24,
                displayValue: "698"
              },
              kills: {
                label: "Kills",
                field: "Kills",
                category: "General",
                valueInt: 708,
                value: "708",
                percentile: 22,
                displayValue: "708"
              },
              minutesPlayed: {
                label: "Time Played",
                field: "MinutesPlayed",
                category: "General",
                valueInt: 0,
                value: "0",
                displayValue: ""
              },
              kpm: {
                label: "Kills/Min",
                field: "KPM",
                category: "General",
                valueDec: 0,
                value: "0",
                displayValue: "0.00"
              },
              kpg: {
                label: "Kills/Match",
                field: "KPG",
                category: "General",
                valueDec: 1.01,
                value: "1.01",
                percentile: 34,
                displayValue: "1.01"
              },
              avgTimePlayed: {
                label: "Avg. Match Time",
                field: "AvgTimePlayed",
                category: "General",
                valueDec: 0,
                value: "0",
                displayValue: ""
              },
              scorePerMatch: {
                label: "Score/Match",
                field: "ScorePerMatch",
                category: "General",
                valueDec: 229.65,
                value: "229.65",
                percentile: 17,
                displayValue: "229.65"
              },
              scorePerMin: {
                label: "Score/Min",
                field: "ScorePerMin",
                category: "General",
                valueDec: 0,
                value: "0",
                displayValue: "0.00"
              }
            },
            curr_p2: {
              score: {
                label: "Score",
                field: "Score",
                category: "General",
                valueInt: 1670,
                value: "1670",
                percentile: 72,
                displayValue: "1,670"
              },
              top1: {
                label: "Wins",
                field: "Top1",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              top3: {
                label: "Top 3",
                field: "Top3",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              top5: {
                label: "Top 5",
                field: "Top5",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              top6: {
                label: "Top 6",
                field: "Top6",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              top10: {
                label: "Top 10",
                field: "Top10",
                category: "Tops",
                valueInt: 1,
                value: "1",
                percentile: 83,
                displayValue: "1"
              },
              top12: {
                label: "Top 12",
                field: "Top12",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              top25: {
                label: "Top 25",
                field: "Top25",
                category: "Tops",
                valueInt: 3,
                value: "3",
                percentile: 73,
                displayValue: "3"
              },
              kd: {
                label: "K/d",
                field: "KD",
                category: "General",
                valueDec: 1.71,
                value: "1.71",
                percentile: 50,
                displayValue: "1.71"
              },
              winRatio: {
                label: "Win %",
                field: "WinRatio",
                category: "General",
                valueDec: 0,
                value: "0",
                displayValue: "0.00"
              },
              matches: {
                label: "Matches",
                field: "Matches",
                category: "General",
                valueInt: 7,
                value: "7",
                percentile: 77,
                displayValue: "7"
              },
              kills: {
                label: "Kills",
                field: "Kills",
                category: "General",
                valueInt: 12,
                value: "12",
                percentile: 76,
                displayValue: "12"
              },
              minutesPlayed: {
                label: "Time Played",
                field: "MinutesPlayed",
                category: "General",
                valueInt: 0,
                value: "0",
                displayValue: ""
              },
              kpm: {
                label: "Kills/Min",
                field: "KPM",
                category: "General",
                valueDec: 0,
                value: "0",
                displayValue: "0.00"
              },
              kpg: {
                label: "Kills/Match",
                field: "KPG",
                category: "General",
                valueDec: 1.71,
                value: "1.71",
                percentile: 47,
                displayValue: "1.71"
              },
              avgTimePlayed: {
                label: "Avg. Match Time",
                field: "AvgTimePlayed",
                category: "General",
                valueDec: 0,
                value: "0",
                displayValue: ""
              },
              scorePerMatch: {
                label: "Score/Match",
                field: "ScorePerMatch",
                category: "General",
                valueDec: 238.57,
                value: "238.57",
                percentile: 23,
                displayValue: "238.57"
              },
              scorePerMin: {
                label: "Score/Min",
                field: "ScorePerMin",
                category: "General",
                valueDec: 0,
                value: "0",
                displayValue: "0.00"
              }
            },
            curr_p10: {
              score: {
                label: "Score",
                field: "Score",
                category: "General",
                valueInt: 10041,
                value: "10041",
                percentile: 44,
                displayValue: "10,041"
              },
              top1: {
                label: "Wins",
                field: "Top1",
                category: "Tops",
                valueInt: 2,
                value: "2",
                percentile: 54,
                displayValue: "2"
              },
              top3: {
                label: "Top 3",
                field: "Top3",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              top5: {
                label: "Top 5",
                field: "Top5",
                category: "Tops",
                valueInt: 7,
                value: "7",
                percentile: 52,
                displayValue: "7"
              },
              top6: {
                label: "Top 6",
                field: "Top6",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              top10: {
                label: "Top 10",
                field: "Top10",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              top12: {
                label: "Top 12",
                field: "Top12",
                category: "Tops",
                valueInt: 17,
                value: "17",
                percentile: 43,
                displayValue: "17"
              },
              top25: {
                label: "Top 25",
                field: "Top25",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              kd: {
                label: "K/d",
                field: "KD",
                category: "General",
                valueDec: 2.19,
                value: "2.19",
                percentile: 37,
                displayValue: "2.19"
              },
              winRatio: {
                label: "Win %",
                field: "WinRatio",
                category: "General",
                valueDec: 4.1,
                value: "4.1",
                percentile: 48,
                displayValue: "4.10"
              },
              matches: {
                label: "Matches",
                field: "Matches",
                category: "General",
                valueInt: 49,
                value: "49",
                percentile: 43,
                displayValue: "49"
              },
              kills: {
                label: "Kills",
                field: "Kills",
                category: "General",
                valueInt: 103,
                value: "103",
                percentile: 40,
                displayValue: "103"
              },
              minutesPlayed: {
                label: "Time Played",
                field: "MinutesPlayed",
                category: "General",
                valueInt: 0,
                value: "0",
                displayValue: ""
              },
              kpm: {
                label: "Kills/Min",
                field: "KPM",
                category: "General",
                valueDec: 0,
                value: "0",
                displayValue: "0.00"
              },
              kpg: {
                label: "Kills/Match",
                field: "KPG",
                category: "General",
                valueDec: 2.1,
                value: "2.1",
                percentile: 36,
                displayValue: "2.10"
              },
              avgTimePlayed: {
                label: "Avg. Match Time",
                field: "AvgTimePlayed",
                category: "General",
                valueDec: 0,
                value: "0",
                displayValue: ""
              },
              scorePerMatch: {
                label: "Score/Match",
                field: "ScorePerMatch",
                category: "General",
                valueDec: 204.92,
                value: "204.92",
                percentile: 49,
                displayValue: "204.92"
              },
              scorePerMin: {
                label: "Score/Min",
                field: "ScorePerMin",
                category: "General",
                valueDec: 0,
                value: "0",
                displayValue: "0.00"
              }
            },
            curr_p9: {
              score: {
                label: "Score",
                field: "Score",
                category: "General",
                valueInt: 2125,
                value: "2125",
                percentile: 80,
                displayValue: "2,125"
              },
              top1: {
                label: "Wins",
                field: "Top1",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              top3: {
                label: "Top 3",
                field: "Top3",
                category: "Tops",
                valueInt: 1,
                value: "1",
                percentile: 89,
                displayValue: "1"
              },
              top5: {
                label: "Top 5",
                field: "Top5",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              top6: {
                label: "Top 6",
                field: "Top6",
                category: "Tops",
                valueInt: 3,
                value: "3",
                percentile: 82,
                displayValue: "3"
              },
              top10: {
                label: "Top 10",
                field: "Top10",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              top12: {
                label: "Top 12",
                field: "Top12",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              top25: {
                label: "Top 25",
                field: "Top25",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              kd: {
                label: "K/d",
                field: "KD",
                category: "General",
                valueDec: 3.71,
                value: "3.71",
                percentile: 11,
                displayValue: "3.71"
              },
              winRatio: {
                label: "Win %",
                field: "WinRatio",
                category: "General",
                valueDec: 0,
                value: "0",
                displayValue: "0.00"
              },
              matches: {
                label: "Matches",
                field: "Matches",
                category: "General",
                valueInt: 7,
                value: "7",
                percentile: 86,
                displayValue: "7"
              },
              kills: {
                label: "Kills",
                field: "Kills",
                category: "General",
                valueInt: 26,
                value: "26",
                percentile: 73,
                displayValue: "26"
              },
              minutesPlayed: {
                label: "Time Played",
                field: "MinutesPlayed",
                category: "General",
                valueInt: 0,
                value: "0",
                displayValue: ""
              },
              kpm: {
                label: "Kills/Min",
                field: "KPM",
                category: "General",
                valueDec: 0,
                value: "0",
                displayValue: "0.00"
              },
              kpg: {
                label: "Kills/Match",
                field: "KPG",
                category: "General",
                valueDec: 3.71,
                value: "3.71",
                percentile: 7,
                displayValue: "3.71"
              },
              avgTimePlayed: {
                label: "Avg. Match Time",
                field: "AvgTimePlayed",
                category: "General",
                valueDec: 0,
                value: "0",
                displayValue: ""
              },
              scorePerMatch: {
                label: "Score/Match",
                field: "ScorePerMatch",
                category: "General",
                valueDec: 303.57,
                value: "303.57",
                percentile: 10,
                displayValue: "303.57"
              },
              scorePerMin: {
                label: "Score/Min",
                field: "ScorePerMin",
                category: "General",
                valueDec: 0,
                value: "0",
                displayValue: "0.00"
              }
            }
          },
          accountId: "685e0525-61f7-42bc-9bfd-69ddf5adf12a",
          platformId: 2,
          platformName: "psn",
          platformNameLong: "PlayStation 4",
          epicUserHandle: "mattchuB16",
          lifeTimeStats: [
            {
              _id: "5df2d4902b04d10008bfff5b",
              key: "Top 5s",
              value: "69"
            },
            {
              _id: "5df2d4902b04d10008bfff5a",
              key: "Top 3s",
              value: "138"
            },
            {
              _id: "5df2d4902b04d10008bfff59",
              key: "Top 6s",
              value: "228"
            },
            {
              _id: "5df2d4902b04d10008bfff58",
              key: "Top 10",
              value: "7"
            },
            {
              _id: "5df2d4902b04d10008bfff57",
              key: "Top 12s",
              value: "129"
            },
            {
              _id: "5df2d4902b04d10008bfff56",
              key: "Top 25s",
              value: "29"
            },
            {
              _id: "5df2d4902b04d10008bfff55",
              key: "Score",
              value: "249,579"
            },
            {
              _id: "5df2d4902b04d10008bfff54",
              key: "Matches Played",
              value: "1189"
            },
            {
              _id: "5df2d4902b04d10008bfff53",
              key: "Wins",
              value: "81"
            },
            {
              _id: "5df2d4902b04d10008bfff52",
              key: "Win%",
              value: "7%"
            },
            {
              _id: "5df2d4902b04d10008bfff51",
              key: "Kills",
              value: "1184"
            },
            {
              _id: "5df2d4902b04d10008bfff50",
              key: "K/d",
              value: "1.07"
            }
          ],
          recentMatches: [
            {
              _id: "5df2d4902b04d10008bfff5f",
              id: 2052743583,
              accountId: "685e0525-61f7-42bc-9bfd-69ddf5adf12a",
              playlist: "p10",
              kills: 16,
              minutesPlayed: 56,
              top1: 0,
              top5: 0,
              top6: 0,
              top10: 0,
              top12: 3,
              top25: 0,
              matches: 7,
              top3: 0,
              dateCollected: "2019-12-11T02:35:08.000Z",
              score: 1350,
              platform: 101,
              trnRating: 1445.6,
              trnRatingChange: 26.859722217411235,
              playlistId: 31,
              playersOutlived: 501
            },
            {
              _id: "5df2d4902b04d10008bfff5e",
              id: 2050417428,
              accountId: "685e0525-61f7-42bc-9bfd-69ddf5adf12a",
              playlist: "p10",
              kills: 0,
              minutesPlayed: 6,
              top1: 0,
              top5: 0,
              top6: 0,
              top10: 0,
              top12: 0,
              top25: 0,
              matches: 1,
              top3: 0,
              dateCollected: "2019-12-10T05:28:18.000Z",
              score: 86,
              platform: 101,
              trnRating: 1281.4,
              trnRatingChange: -99.52525,
              playlistId: 31,
              playersOutlived: 64
            },
            {
              _id: "5df2d4902b04d10008bfff5d",
              id: 2050399338,
              accountId: "685e0525-61f7-42bc-9bfd-69ddf5adf12a",
              playlist: "p10",
              kills: 9,
              minutesPlayed: 17,
              top1: 0,
              top5: 1,
              top6: 0,
              top10: 0,
              top12: 1,
              top25: 0,
              matches: 1,
              top3: 0,
              dateCollected: "2019-12-10T05:04:14.000Z",
              score: 535,
              platform: 101,
              trnRating: 1380.9,
              trnRatingChange: 83.304,
              playlistId: 31,
              playersOutlived: 95
            },
            {
              _id: "5df2d4902b04d10008bfff5c",
              id: 2050088406,
              accountId: "685e0525-61f7-42bc-9bfd-69ddf5adf12a",
              playlist: "misc",
              kills: 3,
              minutesPlayed: 5,
              top1: 0,
              top5: 0,
              top6: 0,
              top10: 0,
              top12: 0,
              top25: 0,
              matches: 2,
              top3: 0,
              dateCollected: "2019-12-06T03:16:18.000Z",
              score: 133,
              platform: 101,
              playlistId: 264,
              playersOutlived: 84
            }
          ],
          createdAt: "2019-12-13T00:00:16.866Z",
          __v: 0
        },
        {
          _id: "5e9272f65fda3a00071ab276",
          stats: {
            p2: {
              score: {
                label: "Score",
                field: "Score",
                category: "General",
                valueInt: 29927,
                value: "29927",
                percentile: 29,
                displayValue: "29,927"
              },
              top1: {
                label: "Wins",
                field: "Top1",
                category: "Tops",
                valueInt: 1,
                value: "1",
                percentile: 48,
                displayValue: "1"
              },
              top3: {
                label: "Top 3",
                field: "Top3",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              top5: {
                label: "Top 5",
                field: "Top5",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              top6: {
                label: "Top 6",
                field: "Top6",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              top10: {
                label: "Top 10",
                field: "Top10",
                category: "Tops",
                valueInt: 26,
                value: "26",
                percentile: 26,
                displayValue: "26"
              },
              top12: {
                label: "Top 12",
                field: "Top12",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              top25: {
                label: "Top 25",
                field: "Top25",
                category: "Tops",
                valueInt: 56,
                value: "56",
                percentile: 28,
                displayValue: "56"
              },
              kd: {
                label: "K/d",
                field: "KD",
                category: "General",
                valueDec: 1.18,
                value: "1.18",
                percentile: 36,
                displayValue: "1.18"
              },
              winRatio: {
                label: "Win %",
                field: "WinRatio",
                category: "General",
                valueDec: 0.5,
                value: "0.5",
                percentile: 44,
                displayValue: "0.50"
              },
              matches: {
                label: "Matches",
                field: "Matches",
                category: "General",
                valueInt: 189,
                value: "189",
                percentile: 32,
                displayValue: "189"
              },
              kills: {
                label: "Kills",
                field: "Kills",
                category: "General",
                valueInt: 222,
                value: "222",
                percentile: 29,
                displayValue: "222"
              },
              minutesPlayed: {
                label: "Time Played",
                field: "MinutesPlayed",
                category: "General",
                valueInt: 1306,
                value: "1306",
                percentile: 0.1,
                displayValue: "21h 46m "
              },
              kpm: {
                label: "Kills/Min",
                field: "KPM",
                category: "General",
                valueDec: 0.17,
                value: "0.17",
                percentile: 95,
                displayValue: "0.17"
              },
              kpg: {
                label: "Kills/Match",
                field: "KPG",
                category: "General",
                valueDec: 1.17,
                value: "1.17",
                percentile: 35,
                displayValue: "1.17"
              },
              avgTimePlayed: {
                label: "Avg. Match Time",
                field: "AvgTimePlayed",
                category: "General",
                valueDec: 414.6,
                value: "414.6",
                percentile: 0.4,
                displayValue: "6m 54s"
              },
              scorePerMatch: {
                label: "Score/Match",
                field: "ScorePerMatch",
                category: "General",
                valueDec: 158.34,
                value: "158.34",
                percentile: 32,
                displayValue: "158.34"
              },
              scorePerMin: {
                label: "Score/Min",
                field: "ScorePerMin",
                category: "General",
                valueDec: 22.92,
                value: "22.92",
                percentile: 98,
                displayValue: "22.92"
              }
            },
            p10: {
              score: {
                label: "Score",
                field: "Score",
                category: "General",
                valueInt: 129218,
                value: "129218",
                percentile: 1.9,
                displayValue: "129,218"
              },
              top1: {
                label: "Wins",
                field: "Top1",
                category: "Tops",
                valueInt: 27,
                value: "27",
                percentile: 4.7,
                displayValue: "27"
              },
              top3: {
                label: "Top 3",
                field: "Top3",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              top5: {
                label: "Top 5",
                field: "Top5",
                category: "Tops",
                valueInt: 119,
                value: "119",
                percentile: 1.3,
                displayValue: "119"
              },
              top6: {
                label: "Top 6",
                field: "Top6",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              top10: {
                label: "Top 10",
                field: "Top10",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              top12: {
                label: "Top 12",
                field: "Top12",
                category: "Tops",
                valueInt: 226,
                value: "226",
                percentile: 1,
                displayValue: "226"
              },
              top25: {
                label: "Top 25",
                field: "Top25",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              kd: {
                label: "K/d",
                field: "KD",
                category: "General",
                valueDec: 1.52,
                value: "1.52",
                percentile: 23,
                displayValue: "1.52"
              },
              winRatio: {
                label: "Win %",
                field: "WinRatio",
                category: "General",
                valueDec: 4.2,
                value: "4.2",
                percentile: 21,
                displayValue: "4.20"
              },
              matches: {
                label: "Matches",
                field: "Matches",
                category: "General",
                valueInt: 636,
                value: "636",
                percentile: 2.5,
                displayValue: "636"
              },
              kills: {
                label: "Kills",
                field: "Kills",
                category: "General",
                valueInt: 926,
                value: "926",
                percentile: 3.9,
                displayValue: "926"
              },
              minutesPlayed: {
                label: "Time Played",
                field: "MinutesPlayed",
                category: "General",
                valueInt: 5181,
                value: "5181",
                percentile: 0.1,
                displayValue: "3d 14h 21m "
              },
              kpm: {
                label: "Kills/Min",
                field: "KPM",
                category: "General",
                valueDec: 0.18,
                value: "0.18",
                percentile: 94,
                displayValue: "0.18"
              },
              kpg: {
                label: "Kills/Match",
                field: "KPG",
                category: "General",
                valueDec: 1.46,
                value: "1.46",
                percentile: 23,
                displayValue: "1.46"
              },
              avgTimePlayed: {
                label: "Avg. Match Time",
                field: "AvgTimePlayed",
                category: "General",
                valueDec: 488.77,
                value: "488.77",
                percentile: 0.3,
                displayValue: "8m 8s"
              },
              scorePerMatch: {
                label: "Score/Match",
                field: "ScorePerMatch",
                category: "General",
                valueDec: 203.17,
                value: "203.17",
                percentile: 20,
                displayValue: "203.17"
              },
              scorePerMin: {
                label: "Score/Min",
                field: "ScorePerMin",
                category: "General",
                valueDec: 24.94,
                value: "24.94",
                percentile: 99,
                displayValue: "24.94"
              }
            },
            p9: {
              score: {
                label: "Score",
                field: "Score",
                category: "General",
                valueInt: 190399,
                value: "190399",
                percentile: 3.1,
                displayValue: "190,399"
              },
              top1: {
                label: "Wins",
                field: "Top1",
                category: "Tops",
                valueInt: 70,
                value: "70",
                percentile: 2.9,
                displayValue: "70"
              },
              top3: {
                label: "Top 3",
                field: "Top3",
                category: "Tops",
                valueInt: 159,
                value: "159",
                percentile: 1.7,
                displayValue: "159"
              },
              top5: {
                label: "Top 5",
                field: "Top5",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              top6: {
                label: "Top 6",
                field: "Top6",
                category: "Tops",
                valueInt: 263,
                value: "263",
                percentile: 1.4,
                displayValue: "263"
              },
              top10: {
                label: "Top 10",
                field: "Top10",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              top12: {
                label: "Top 12",
                field: "Top12",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              top25: {
                label: "Top 25",
                field: "Top25",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              kd: {
                label: "K/d",
                field: "KD",
                category: "General",
                valueDec: 1.18,
                value: "1.18",
                percentile: 36,
                displayValue: "1.18"
              },
              winRatio: {
                label: "Win %",
                field: "WinRatio",
                category: "General",
                valueDec: 8.3,
                value: "8.3",
                percentile: 16,
                displayValue: "8.30"
              },
              matches: {
                label: "Matches",
                field: "Matches",
                category: "General",
                valueInt: 848,
                value: "848",
                percentile: 2.9,
                displayValue: "848"
              },
              kills: {
                label: "Kills",
                field: "Kills",
                category: "General",
                valueInt: 916,
                value: "916",
                percentile: 8,
                displayValue: "916"
              },
              minutesPlayed: {
                label: "Time Played",
                field: "MinutesPlayed",
                category: "General",
                valueInt: 7253,
                value: "7253",
                percentile: 0.1,
                displayValue: "5d 53m "
              },
              kpm: {
                label: "Kills/Min",
                field: "KPM",
                category: "General",
                valueDec: 0.13,
                value: "0.13",
                percentile: 96,
                displayValue: "0.13"
              },
              kpg: {
                label: "Kills/Match",
                field: "KPG",
                category: "General",
                valueDec: 1.08,
                value: "1.08",
                percentile: 38,
                displayValue: "1.08"
              },
              avgTimePlayed: {
                label: "Avg. Match Time",
                field: "AvgTimePlayed",
                category: "General",
                valueDec: 513.18,
                value: "513.18",
                percentile: 0.2,
                displayValue: "8m 33s"
              },
              scorePerMatch: {
                label: "Score/Match",
                field: "ScorePerMatch",
                category: "General",
                valueDec: 224.53,
                value: "224.53",
                percentile: 28,
                displayValue: "224.53"
              },
              scorePerMin: {
                label: "Score/Min",
                field: "ScorePerMin",
                category: "General",
                valueDec: 26.25,
                value: "26.25",
                percentile: 99,
                displayValue: "26.25"
              }
            },
            curr_p2: {
              score: {
                label: "Score",
                field: "Score",
                category: "General",
                valueInt: 6568,
                value: "6568",
                percentile: 0.8,
                displayValue: "6,568"
              },
              top1: {
                label: "Wins",
                field: "Top1",
                category: "Tops",
                valueInt: 1,
                value: "1",
                percentile: 27,
                displayValue: "1"
              },
              top3: {
                label: "Top 3",
                field: "Top3",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              top5: {
                label: "Top 5",
                field: "Top5",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              top6: {
                label: "Top 6",
                field: "Top6",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              top10: {
                label: "Top 10",
                field: "Top10",
                category: "Tops",
                valueInt: 8,
                value: "8",
                percentile: 1.1,
                displayValue: "8"
              },
              top12: {
                label: "Top 12",
                field: "Top12",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              top25: {
                label: "Top 25",
                field: "Top25",
                category: "Tops",
                valueInt: 13,
                value: "13",
                percentile: 0.9,
                displayValue: "13"
              },
              kd: {
                label: "K/d",
                field: "KD",
                category: "General",
                valueDec: 1.73,
                value: "1.73",
                percentile: 44,
                displayValue: "1.73"
              },
              winRatio: {
                label: "Win %",
                field: "WinRatio",
                category: "General",
                valueDec: 3.2,
                value: "3.2",
                percentile: 26,
                displayValue: "3.20"
              },
              matches: {
                label: "Matches",
                field: "Matches",
                category: "General",
                valueInt: 31,
                value: "31",
                percentile: 1.9,
                displayValue: "31"
              },
              kills: {
                label: "Kills",
                field: "Kills",
                category: "General",
                valueInt: 52,
                value: "52",
                percentile: 3,
                displayValue: "52"
              },
              minutesPlayed: {
                label: "Time Played",
                field: "MinutesPlayed",
                category: "General",
                valueInt: 259,
                value: "259",
                displayValue: "4h 19m "
              },
              kpm: {
                label: "Kills/Min",
                field: "KPM",
                category: "General",
                valueDec: 0.2,
                value: "0.2",
                displayValue: "0.20"
              },
              kpg: {
                label: "Kills/Match",
                field: "KPG",
                category: "General",
                valueDec: 1.68,
                value: "1.68",
                percentile: 43,
                displayValue: "1.68"
              },
              avgTimePlayed: {
                label: "Avg. Match Time",
                field: "AvgTimePlayed",
                category: "General",
                valueDec: 501.29,
                value: "501.29",
                displayValue: "8m 21s"
              },
              scorePerMatch: {
                label: "Score/Match",
                field: "ScorePerMatch",
                category: "General",
                valueDec: 211.87,
                value: "211.87",
                percentile: 41,
                displayValue: "211.87"
              },
              scorePerMin: {
                label: "Score/Min",
                field: "ScorePerMin",
                category: "General",
                valueDec: 25.36,
                value: "25.36",
                displayValue: "25.36"
              }
            },
            curr_p10: {
              score: {
                label: "Score",
                field: "Score",
                category: "General",
                valueInt: 36135,
                value: "36135",
                percentile: 0.1,
                displayValue: "36,135"
              },
              top1: {
                label: "Wins",
                field: "Top1",
                category: "Tops",
                valueInt: 6,
                value: "6",
                percentile: 0.5,
                displayValue: "6"
              },
              top3: {
                label: "Top 3",
                field: "Top3",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              top5: {
                label: "Top 5",
                field: "Top5",
                category: "Tops",
                valueInt: 35,
                value: "35",
                percentile: 0.1,
                displayValue: "35"
              },
              top6: {
                label: "Top 6",
                field: "Top6",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              top10: {
                label: "Top 10",
                field: "Top10",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              top12: {
                label: "Top 12",
                field: "Top12",
                category: "Tops",
                valueInt: 68,
                value: "68",
                percentile: 0.1,
                displayValue: "68"
              },
              top25: {
                label: "Top 25",
                field: "Top25",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              kd: {
                label: "K/d",
                field: "KD",
                category: "General",
                valueDec: 2.04,
                value: "2.04",
                percentile: 37,
                displayValue: "2.04"
              },
              winRatio: {
                label: "Win %",
                field: "WinRatio",
                category: "General",
                valueDec: 3.6,
                value: "3.6",
                percentile: 34,
                displayValue: "3.60"
              },
              matches: {
                label: "Matches",
                field: "Matches",
                category: "General",
                valueInt: 167,
                value: "167",
                percentile: 0.1,
                displayValue: "167"
              },
              kills: {
                label: "Kills",
                field: "Kills",
                category: "General",
                valueInt: 329,
                value: "329",
                percentile: 0.1,
                displayValue: "329"
              },
              minutesPlayed: {
                label: "Time Played",
                field: "MinutesPlayed",
                category: "General",
                valueInt: 1505,
                value: "1505",
                displayValue: "1d 1h 5m "
              },
              kpm: {
                label: "Kills/Min",
                field: "KPM",
                category: "General",
                valueDec: 0.22,
                value: "0.22",
                displayValue: "0.22"
              },
              kpg: {
                label: "Kills/Match",
                field: "KPG",
                category: "General",
                valueDec: 1.97,
                value: "1.97",
                percentile: 39,
                displayValue: "1.97"
              },
              avgTimePlayed: {
                label: "Avg. Match Time",
                field: "AvgTimePlayed",
                category: "General",
                valueDec: 540.72,
                value: "540.72",
                displayValue: "9m 0s"
              },
              scorePerMatch: {
                label: "Score/Match",
                field: "ScorePerMatch",
                category: "General",
                valueDec: 216.38,
                value: "216.38",
                percentile: 46,
                displayValue: "216.38"
              },
              scorePerMin: {
                label: "Score/Min",
                field: "ScorePerMin",
                category: "General",
                valueDec: 24.01,
                value: "24.01",
                displayValue: "24.01"
              }
            },
            curr_p9: {
              score: {
                label: "Score",
                field: "Score",
                category: "General",
                valueInt: 9580,
                value: "9580",
                percentile: 0.2,
                displayValue: "9,580"
              },
              top1: {
                label: "Wins",
                field: "Top1",
                category: "Tops",
                valueInt: 4,
                value: "4",
                percentile: 6,
                displayValue: "4"
              },
              top3: {
                label: "Top 3",
                field: "Top3",
                category: "Tops",
                valueInt: 8,
                value: "8",
                percentile: 2,
                displayValue: "8"
              },
              top5: {
                label: "Top 5",
                field: "Top5",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              top6: {
                label: "Top 6",
                field: "Top6",
                category: "Tops",
                valueInt: 13,
                value: "13",
                percentile: 0.5,
                displayValue: "13"
              },
              top10: {
                label: "Top 10",
                field: "Top10",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              top12: {
                label: "Top 12",
                field: "Top12",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              top25: {
                label: "Top 25",
                field: "Top25",
                category: "Tops",
                valueInt: 0,
                value: "0",
                displayValue: "0"
              },
              kd: {
                label: "K/d",
                field: "KD",
                category: "General",
                valueDec: 2.52,
                value: "2.52",
                percentile: 41,
                displayValue: "2.52"
              },
              winRatio: {
                label: "Win %",
                field: "WinRatio",
                category: "General",
                valueDec: 11.4,
                value: "11.4",
                percentile: 40,
                displayValue: "11.40"
              },
              matches: {
                label: "Matches",
                field: "Matches",
                category: "General",
                valueInt: 35,
                value: "35",
                percentile: 0.1,
                displayValue: "35"
              },
              kills: {
                label: "Kills",
                field: "Kills",
                category: "General",
                valueInt: 78,
                value: "78",
                percentile: 1,
                displayValue: "78"
              },
              minutesPlayed: {
                label: "Time Played",
                field: "MinutesPlayed",
                category: "General",
                valueInt: 390,
                value: "390",
                displayValue: "6h 30m "
              },
              kpm: {
                label: "Kills/Min",
                field: "KPM",
                category: "General",
                valueDec: 0.2,
                value: "0.2",
                displayValue: "0.20"
              },
              kpg: {
                label: "Kills/Match",
                field: "KPG",
                category: "General",
                valueDec: 2.23,
                value: "2.23",
                percentile: 39,
                displayValue: "2.23"
              },
              avgTimePlayed: {
                label: "Avg. Match Time",
                field: "AvgTimePlayed",
                category: "General",
                valueDec: 668.57,
                value: "668.57",
                displayValue: "11m 8s"
              },
              scorePerMatch: {
                label: "Score/Match",
                field: "ScorePerMatch",
                category: "General",
                valueDec: 273.71,
                value: "273.71",
                percentile: 40,
                displayValue: "273.71"
              },
              scorePerMin: {
                label: "Score/Min",
                field: "ScorePerMin",
                category: "General",
                valueDec: 24.56,
                value: "24.56",
                displayValue: "24.56"
              }
            }
          },
          accountId: "685e0525-61f7-42bc-9bfd-69ddf5adf12a",
          platformId: 101,
          platformName: "gamepad",
          platformNameLong: "Gamepad",
          epicUserHandle: "mattchuB16",
          lifeTimeStats: [
            {
              _id: "5e92b3c5b7e7240008474b39",
              key: "Top 5s",
              value: "120"
            },
            {
              _id: "5e92b3c5b7e7240008474b3a",
              key: "Top 3s",
              value: "161"
            },
            {
              _id: "5e92b3c5b7e7240008474b3b",
              key: "Top 6s",
              value: "267"
            },
            {
              _id: "5e92b3c5b7e7240008474b3c",
              key: "Top 10",
              value: "26"
            },
            {
              _id: "5e92b3c5b7e7240008474b3d",
              key: "Top 12s",
              value: "228"
            },
            {
              _id: "5e92b3c5b7e7240008474b3e",
              key: "Top 25s",
              value: "56"
            },
            {
              _id: "5e92b3c5b7e7240008474b3f",
              key: "Score",
              value: "375,598"
            },
            {
              _id: "5e92b3c5b7e7240008474b40",
              key: "Matches Played",
              value: "1700"
            },
            {
              _id: "5e92b3c5b7e7240008474b41",
              key: "Wins",
              value: "98"
            },
            {
              _id: "5e92b3c5b7e7240008474b42",
              key: "Win%",
              value: "6%"
            },
            {
              _id: "5e92b3c5b7e7240008474b43",
              key: "Kills",
              value: "2181"
            },
            {
              _id: "5e92b3c5b7e7240008474b44",
              key: "K/d",
              value: "1.36"
            }
          ],
          recentMatches: [
            {
              _id: "5e92b3c5b7e7240008474b37",
              id: 2255018422,
              accountId: "685e0525-61f7-42bc-9bfd-69ddf5adf12a",
              playlist: "p9",
              kills: 28,
              minutesPlayed: 172,
              top1: 0,
              top5: 0,
              top6: 3,
              top10: 0,
              top12: 0,
              top25: 0,
              matches: 19,
              top3: 1,
              dateCollected: "2020-04-11T19:35:27.000Z",
              score: 3604,
              platform: 101,
              trnRating: 1191.2,
              trnRatingChange: -0.019776450317639048,
              playlistId: 33,
              playersOutlived: 1155
            },
            {
              _id: "5e92b3c5b7e7240008474b38",
              id: 2255018418,
              accountId: "685e0525-61f7-42bc-9bfd-69ddf5adf12a",
              playlist: "ltm",
              kills: 0,
              minutesPlayed: 0,
              top1: 0,
              top5: 0,
              top6: 0,
              top10: 0,
              top12: 0,
              top25: 0,
              matches: 1,
              top3: 0,
              dateCollected: "2020-04-06T02:23:21.000Z",
              score: 97,
              platform: 101,
              playlistId: 161,
              playersOutlived: 0
            }
          ],
          createdAt: "2020-04-12T01:46:31.604Z",
          __v: 0
        }
      ],
      lastUpdate: 1586672583541,
      lastResult: "Success",
      updateTime: "2021-03-31T18:52:01.792Z"
    },
    organizations: [],
    firstRun: false,
    roles: ["admin", "employee"],
    facebookVerified: false,
    googleVerified: true,
    discordVerified: true,
    twitchVerified: true,
    online: false,
    verified: true,
    steamVerified: false,
    mixerVerified: false,
    linkedinVerified: false,
    battlenetVerified: false,
    firstName: "Matthew",
    lastName: "Benson",
    username: "mjb",
    createdAt: "2019-11-26T21:49:45.712Z",
    affiliatedGames: [],
    socialExperience: [],
    portfolioEvents: [
      {
        _id: "5ea84fbd70aad12dfa140ae9",
        title: "Warzone Wednesday",
        subTitle: "Technical Liaison & PM",
        eventDate: "2020-03-18T16:00:00.000Z",
        description:
          "<p>eFuse assisted in hosting and building KeemPark which housed the Warzone Wednesday tournament.I acted as the technical liaison between the teams and as the project manager on the eFuse team.</p>"
      }
    ],
    portfolioHonors: [],
    businessExperience: [
      {
        present: true,
        _id: "5deff2fe5767b723b80466f4",
        title: "CEO & Founder",
        subTitle: "eFuse",
        startDate: "2018-08-01T16:00:00.000Z",
        endDate: null,
        description:
          "<ul><li>Founded in August 2018</li><li>Left my day job as Venture Capitalist</li><li>Raised a Seed round of investment, built an elite team, &amp; now we are working to build elite products for esport &amp; video game community.</li></ul>",
        image: {
          filename: "eFuse Alternate Logo - No Background (2).png",
          contentType: "image/png",
          url:
            "https://efuse.s3.amazonaws.com/uploads/work-experience/1577820200328-eFuse%20Alternate%20Logo%20-%20No%20Background%20%282%29.png"
        }
      },
      {
        image: {
          filename: "BetterOIF.jpg",
          contentType: "image/jpeg",
          url: "https://efuse.s3.amazonaws.com/uploads/work-experience/1588088710404-BetterOIF.jpg"
        },
        present: false,
        _id: "5ea84f6ff36f8049ae18bcb7",
        title: "Investment Analyst",
        subTitle: "Ohio Innovation Fund",
        startDate: "2018-05-01T16:00:00.000Z",
        endDate: "2018-12-28T17:00:00.000Z",
        description:
          "<p>As a full-time investment analyst with OIF my work transitioned to working on:</p><ul><li>Deal flow (Sourcing &amp; Vetting)</li><li>Financial Analysis of Portfolio Companies</li><li>Term Sheet &amp; Corporate Document review</li><li>Portfolio Waterfall Analysis</li></ul>"
      }
    ],
    educationExperience: [
      {
        present: false,
        _id: "5dfa917f80ee93e8f2517e16",
        level: 0,
        school: "Ohio University",
        degree: "Bachelor's",
        fieldofstudy: "Entrepreneurship & Finance",
        grade: "",
        startDate: "2016-08-18T00:00:00.000Z",
        endDate: "2018-05-01T00:00:00.000Z",
        description:
          "<ul><li>Jack &amp; Sue Ellis Cutler Scholar</li><li>Honors College of Business</li><li>Student Alumni Board</li><li>Presidential Leadership Society</li></ul>",
        image: {
          filename: "image",
          contentType: "image/jpeg",
          url: "https://efuse.s3.amazonaws.com/uploads/education-experience/1599236443175-image"
        }
      }
    ],
    esportsExperience: [],
    __v: 398,
    bio: "CEO and Co-Founder of eFuse 🎮",
    headerImage: {
      filename: "Matt Header.png",
      contentType: "image/jpeg",
      url: "https://cdn.efuse.gg/uploads/users/1576602949040-Matt%20Header.png"
    },
    profilePicture: {
      filename: "IMG_8420.jpg",
      contentType: "image/jpeg",
      url: "https://cdn.efuse.gg/uploads/users/1582224469245-IMG_8420.jpg"
    },
    businessSkills: [
      {
        _id: "5def03368b9f3361edcf1768",
        value: 29,
        order: 0
      },
      {
        _id: "5def03368b9f330247cf176a",
        value: 13,
        order: 2
      },
      {
        _id: "5def03368b9f336449cf176b",
        value: 4,
        order: 3
      }
    ],
    linkedinLink: "https://www.linkedin.com/in/matthewjbenson1/",
    twitterLink: "https://twitter.com/Matthew__Benson",
    twitchID: "432184573",
    twitchUserName: "matthewjbenson",
    gamingSkills: [
      {
        _id: "5def034ed574e26d97af6fb7",
        value: 18,
        order: 0
      },
      {
        _id: "5def034ed574e27837af6fb8",
        value: 17,
        order: 1
      },
      {
        _id: "5def034ed574e25832af6fb9",
        value: 11,
        order: 2
      }
    ],
    instagramLink: "https://www.instagram.com/matthewjbenson/?hl=en",
    twitchLink: "https://www.twitch.tv/matthewjbenson",
    facebookLink: "https://www.facebook.com/matthew.j.benson.9?ref=bookmarks",
    og: {
      show: true,
      number: 1
    },
    resume: {
      contentType: "image/png",
      filename: "mindblown.png",
      url: "https://efuse.gg/static/images/mindblown.png"
    },
    twitch: {
      _id: "5e0be1daf6a2280008608e52",
      updatedAt: null,
      viewCount: 1754,
      displayName: "matthewjbenson",
      profileImageUrl:
        "https://static-cdn.jtvnw.net/user-default-pictures-uv/294c98b5-e34d-42cd-a8f0-140b72fba9b0-profile_image-300x300.png",
      description: "",
      type: "",
      broadcasterType: "",
      createdAt: "2020-01-01T00:03:38.497Z",
      __v: 0
    },
    rocketleague: {
      platforms: [
        {
          _id: "5e540cf908fd623271d1a865",
          platform: "ps4",
          username: "MattchuB16"
        }
      ],
      stats: [
        {
          _id: "5f4699c8fac072d173ea2998",
          trnScore: 3619.5,
          goalShotRatio: 65,
          wins: 47,
          goals: 147,
          saves: 50,
          shots: 226,
          mvps: 24,
          assists: 49,
          mvpWinsRatio: 51.1,
          ranks: [
            {
              _id: "5f4699c8fac072f640ea2999",
              season: "season-14",
              ranks: [
                {
                  _id: "5f4699c8fac072da94ea299a",
                  title: "Un-Ranked",
                  subTitle: "Unranked Division I",
                  ratingScore: 579,
                  ratingPercent: "Top 78%",
                  winStreak: null
                },
                {
                  _id: "5f4699c8fac0721166ea299b",
                  title: "Ranked Doubles 2v2",
                  subTitle: "Bronze I Division II",
                  ratingScore: 137,
                  ratingPercent: "Top 100%",
                  games: 0,
                  winStreak: null
                }
              ]
            },
            {
              _id: "5f4699c8fac0726143ea299c",
              season: "season-13",
              ranks: [
                {
                  _id: "5f4699c8fac072682fea299d",
                  title: "Un-Ranked",
                  subTitle: "Division IUnranked",
                  ratingPercent: "",
                  winStreak: null
                },
                {
                  _id: "5f4699c8fac0721517ea299e",
                  title: "Ranked Doubles 2v2",
                  subTitle: "Division IIBronze I",
                  ratingScore: 91,
                  ratingPercent: "",
                  winStreak: null
                }
              ]
            }
          ],
          platform: "ps4"
        }
      ]
    },
    callOfDuty: {
      platforms: [
        {
          _id: "5e5bdd3387713b168c4b49c4",
          platform: "psn",
          username: "MattchuB16"
        }
      ],
      stats: [
        {
          _id: "5ffc92d6a9e7eacf434d3093",
          jobRanAt: "2021-01-11T18:03:02.819Z",
          createdAt: "2021-01-11T18:03:02.819Z",
          __v: 0
        }
      ]
    },
    slug: "mjb",
    apexLegendsStat: {
      platforms: [],
      stats: []
    },
    twitterVerified: true,
    pubg: {
      platforms: [],
      stats: []
    },
    twitterUsername: "mjb_OH",
    facebookPages: [],
    twitterfollowersCount: 1495,
    componentLayout: [
      {
        _id: "60a566b083221500215b7df9",
        name: "stats-component"
      },
      {
        _id: "5ff8999ca76b1e3c30979234",
        name: "social-cards-component"
      },
      {
        _id: "5ff8999ca76b1e3b18979235",
        name: "platforms-component"
      },
      {
        _id: "5ff8999ca76b1ebf10979237",
        name: "experience-component"
      },
      {
        _id: "5ff8999ca76b1e421b979238",
        name: "education-component"
      },
      {
        _id: "5ff8999ca76b1e4bed979239",
        name: "accolades-component"
      },
      {
        _id: "5ff8999ca76b1e9b2f97923a",
        name: "skills-component"
      }
    ],
    twitchScope: [],
    updatedAt: "2021-09-20T13:15:03.199Z",
    weeklyNewsLetter: true,
    portfolioProgress: 71,
    portfolioProgressUpdatedAt: "2020-07-06T07:23:59.761Z",
    goldSubscriber: false,
    stats: {
      _id: "5f0f37d3d258f351c95a2f87",
      hypeScoreRanking: 5,
      hypeScorePercentile: 10,
      createdAt: "2020-07-15T17:07:31.958Z",
      user: "5ddd9df9a3980593e6f3feaf",
      hypeScore: 4306,
      hypeScoreUpdatedAt: 1631316668486,
      postViews: 150484,
      totalEngagements: 4306,
      updatedAt: "2021-09-10T23:31:08.487Z",
      __v: 0,
      totalHypes: 17444
    },
    twitchFollowersCount: 18,
    showOnlineMobile: true,
    xboxVerified: false,
    snapchatVerified: false,
    deleted: false,
    publicTraitsMotivations: false,
    embeddedLink: "",
    youtubeChannelId: "",
    headerImageOriginalUrl: "https://efuse.s3.amazonaws.com/uploads/users/1576602949040-Matt%20Header.png",
    profilePictureOriginalUrl: "https://efuse.s3.amazonaws.com/uploads/users/1582224469245-IMG_8420.jpg",
    pathway: {
      _id: "5fd92c426c8725000f277548",
      name: "Find a Gaming Job or Scholarship",
      active: false
    },
    name: "Matthew Benson",
    google: {
      name: "Matthew Benson",
      profile: null,
      picture: "https://lh3.googleusercontent.com/a-/AOh14GjMC1dCL8a_M2m8XEeZYBUlC_8pcn_mLswj9eot=s96-c",
      gender: null,
      locale: "en"
    },
    youtubeChannel: {
      _id: "608da0d21fc1109eea2b2414",
      owner: "5ddd9df9a3980593e6f3feaf",
      ownerKind: "users",
      __v: 0,
      contentDetails: {
        relatedPlaylists: {
          likes: "LL",
          favorites: "FLkJvNC1WM6j3olnyRySAf-g",
          uploads: "UUkJvNC1WM6j3olnyRySAf-g"
        }
      },
      description: "",
      etag: "CKkZf3OkJSuWXfADbb-AZ3-o2lk",
      id: "UCkJvNC1WM6j3olnyRySAf-g",
      kind: "youtube#channel",
      publishedAt: "2015-03-19T01:31:20Z",
      statistics: {
        viewCount: "17",
        subscriberCount: "0",
        hiddenSubscriberCount: false,
        videoCount: "1"
      },
      thumbnails: {
        default: {
          url: "https://yt3.ggpht.com/ytc/AAUvwnhQzTAPS6q_JAkglsdS_LoLOgCDAyzMcxD4ZcO_=s88-c-k-c0x00ffffff-no-rj",
          width: 88,
          height: 88
        },
        medium: {
          url: "https://yt3.ggpht.com/ytc/AAUvwnhQzTAPS6q_JAkglsdS_LoLOgCDAyzMcxD4ZcO_=s240-c-k-c0x00ffffff-no-rj",
          width: 240,
          height: 240
        }
      },
      title: "Matthew Benson",
      updatedAt: "2021-05-01T18:41:46.162Z"
    },
    linkedin: {
      photos: []
    },
    discordUserId: "553233659369488395",
    discordUserName: "MJB",
    discordUserUniqueId: "MJB#9069",
    discordWebhooks: [],
    streaks: {
      daily: {
        isActive: true,
        streakDuration: 3
      },
      dailyComment: {
        isActive: false
      }
    },
    gamePlatforms: [
      {
        _id: "5f739ac5abc98d0011b17c3c",
        platform: "psn",
        displayName: "MattchuB16"
      }
    ],
    fnbrStats: {
      keyboardMouse: {
        solo: {
          wins: 0,
          kills: 0,
          top10: 0,
          matchesPlayed: 0,
          top25: 0,
          lastModified: 0,
          playerSoutLived: 0,
          minutesPlayed: 0,
          score: 0,
          winPercent: 0,
          killsPerMatch: 0,
          scorePerMatch: 0,
          kdRatio: 0
        },
        duos: {
          kills: 0,
          minutesPlayed: 0,
          wins: 0,
          score: 0,
          matchesPlayed: 0,
          lastModified: 0,
          top12: 0,
          playerSoutLived: 0,
          top5: 0,
          winPercent: 0,
          killsPerMatch: 0,
          scorePerMatch: 0,
          kdRatio: 0
        },
        squad: {
          top3: 0,
          kills: 0,
          playerSoutLived: 0,
          score: 0,
          matchesPlayed: 0,
          wins: 0,
          minutesPlayed: 0,
          lastModified: 0,
          top6: 0,
          winPercent: 0,
          killsPerMatch: 0,
          scorePerMatch: 0,
          kdRatio: 0
        }
      },
      touch: {
        solo: {
          wins: 0,
          kills: 0,
          top10: 0,
          matchesPlayed: 0,
          top25: 0,
          lastModified: 0,
          playerSoutLived: 0,
          minutesPlayed: 0,
          score: 0,
          winPercent: 0,
          killsPerMatch: 0,
          scorePerMatch: 0,
          kdRatio: 0
        },
        duos: {
          kills: 0,
          minutesPlayed: 0,
          wins: 0,
          score: 0,
          matchesPlayed: 0,
          lastModified: 0,
          top12: 0,
          playerSoutLived: 0,
          top5: 0,
          winPercent: 0,
          killsPerMatch: 0,
          scorePerMatch: 0,
          kdRatio: 0
        },
        squad: {
          top3: 0,
          kills: 0,
          playerSoutLived: 0,
          score: 0,
          matchesPlayed: 0,
          wins: 0,
          minutesPlayed: 0,
          lastModified: 0,
          top6: 0,
          winPercent: 0,
          killsPerMatch: 0,
          scorePerMatch: 0,
          kdRatio: 0
        }
      },
      gamepad: {
        solo: {
          wins: 1,
          kills: 213,
          top10: 23,
          matchesPlayed: 178,
          top25: 51,
          lastModified: 1592880636,
          playerSoutLived: 8981,
          minutesPlayed: 1171,
          score: 27413,
          winPercent: 0.56,
          killsPerMatch: 1.2,
          scorePerMatch: 154.01,
          kdRatio: 1.2
        },
        duos: {
          kills: 2076,
          minutesPlayed: 8388,
          wins: 53,
          score: 215163,
          matchesPlayed: 958,
          lastModified: 1617159413,
          top12: 373,
          playerSoutLived: 57044,
          top5: 199,
          winPercent: 5.53,
          killsPerMatch: 2.17,
          scorePerMatch: 224.6,
          kdRatio: 2.29
        },
        squad: {
          top3: 188,
          kills: 1485,
          playerSoutLived: 51104,
          score: 213545,
          matchesPlayed: 897,
          wins: 83,
          minutesPlayed: 8091,
          lastModified: 1613356380,
          top6: 310,
          winPercent: 9.25,
          killsPerMatch: 1.66,
          scorePerMatch: 238.07,
          kdRatio: 1.82
        }
      },
      _id: "5f3a9948cb49180fab31a295",
      user: "5ddd9df9a3980593e6f3feaf",
      __v: 0,
      accountId: "685e052561f742bc9bfd69ddf5adf12a",
      createdAt: "2020-08-17T14:50:48.162Z",
      endTime: 9223372036854776000,
      startTime: 0,
      updatedAt: "2021-03-31T18:52:01.778Z"
    },
    leaderboard: {
      isActive: true,
      valueAtTheTimeOfCreation: 2,
      _id: "614bd1b5f18651003bdbee15",
      placement: 185,
      placementPercent: 30,
      user: "5ddd9df9a3980593e6f3feaf",
      boardType: "streaks",
      streak: "61488ad4f1c2f00040af16ce",
      streakDuration: 2,
      createdAt: "2021-09-23T01:00:37.400Z",
      updatedAt: "2021-09-23T01:00:37.401Z",
      __v: 0
    },
    activeLeaderboard: {
      engagementsLeaderboard: {
        isActive: true,
        valueAtTheTimeOfCreation: 4306,
        _id: "614bc831f18651003bdadbf2",
        placement: 4,
        placementPercent: 0,
        user: "5ddd9df9a3980593e6f3feaf",
        boardType: "engagements",
        stats: "5f0f37d3d258f351c95a2f87",
        streakDuration: 4306,
        createdAt: "2021-09-23T00:20:01.090Z",
        updatedAt: "2021-09-23T00:20:01.094Z",
        __v: 0
      },
      streaksLeaderboard: {
        isActive: true,
        valueAtTheTimeOfCreation: 2,
        _id: "614bd1b5f18651003bdbee15",
        placement: 185,
        placementPercent: 30,
        user: "5ddd9df9a3980593e6f3feaf",
        boardType: "streaks",
        streak: "61488ad4f1c2f00040af16ce",
        streakDuration: 2,
        createdAt: "2021-09-23T01:00:37.400Z",
        updatedAt: "2021-09-23T01:00:37.401Z",
        __v: 0
      },
      viewsLeaderboard: {
        isActive: true,
        valueAtTheTimeOfCreation: 150484,
        _id: "614bc70cf18651003bdabff8",
        placement: 7,
        placementPercent: 0,
        user: "5ddd9df9a3980593e6f3feaf",
        boardType: "views",
        stats: "5f0f37d3d258f351c95a2f87",
        streakDuration: 150484,
        createdAt: "2021-09-23T00:15:08.459Z",
        updatedAt: "2021-09-23T00:15:08.460Z",
        __v: 0
      }
    },
    badges: [],
    associatedGames: []
  }
};
