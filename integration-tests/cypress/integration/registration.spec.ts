import {
  learningArticlesRecommendations,
  followerRecommendations,
  validatorResponse,
  signupUserDetails,
  pathwaysResponse,
  loginSuccess
} from "../fixtures";

describe("eFuse sign-up test", () => {
  before(() => {
    cy.intercept("GET", "/api/public/recommendations/learning*", learningArticlesRecommendations);
    cy.intercept("GET", "/api/public/recommendations/followers*", followerRecommendations);
    cy.visit("/signup");
  });

  it("Sign-up flow first step", () => {
    cy.intercept("POST", "/api/auth/validate_*", validatorResponse);

    // Enter email, username and password
    cy.dataCy("signup_email_input")
      .first()
      .type(signupUserDetails.email);
    cy.dataCy("signup_username_input")
      .first()
      .type(signupUserDetails.username);
    cy.get("#password-strength")
      .first()
      .type(signupUserDetails.password);

    // Click Next to go to next step
    cy.get('button:contains("Next")')
      .first()
      .click();
  });

  it("Sign-up flow second step", () => {
    cy.get("#firstName").type(signupUserDetails.firstName);
    cy.get("#lastName").type(signupUserDetails.lastName);
    cy.get("#dob").type(signupUserDetails.dateOfBirth);
    cy.dataCy("privacy_policy_checkbox").click(); // Agree to privacy policy

    // Click Next to go to next step
    cy.get('button:contains("Next")')
      .first()
      .click();
  });

  it("Sign-up flow third step", () => {
    cy.intercept("GET", "/api/public/pathways", pathwaysResponse).as("getPathways");
    cy.intercept("POST", "/api/auth/register", loginSuccess);
    cy.wait("@getPathways");
    cy.dataCy(pathwaysResponse.filter(pathway => pathway.active)[0].name).click();
    cy.get("button:contains('Complete')")
      .first()
      .click();
  });
});
