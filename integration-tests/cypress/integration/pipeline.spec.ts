import { validGames, loginDetails } from "../fixtures";

describe("eFuse Pipeline related tests", () => {
  before(() => {
    // Visit eFuse Pipeline page
    cy.visit("/pipeline");
  });

  it("Validate all of the games exist", () => {
    validGames.forEach(game => cy.get(`button:contains(${game})`).should("be.visible"));
  });
  it("Validate Join the pipeline button works", () => {
    cy.get('button:contains("JOIN THE PIPELINE")')
      .first()
      .click();
  });
  it("Validate login button works", () => {
    cy.get("button:contains('Login')")
      .first()
      .click();
    cy.get('[placeholder="Email Address"]').type(loginDetails.username);
    cy.get('[placeholder="Password"]').type(loginDetails.password);
    cy.get('button:contains("Log in")')
      .first()
      .click();
  });
  it("Make sure recruit profile button exists", () => {
    cy.get('button:contains("VIEW RECRUIT PROFILE")')
      .first()
      .click();
  });
});
