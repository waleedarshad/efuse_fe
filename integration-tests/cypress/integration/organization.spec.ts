describe("eFuse Organization page tests", () => {
  before(() => {
    // Visit the organization page
    cy.visit("/organizations");
  });

  it("Go to eFuse org page", () => {
    cy.dataCy("org", "contains")
      .first()
      .click();
    cy.url().should("include", "/org/");
  });
  it("Validate org name & bio", () => {
    cy.dataCy("org_name")
      .first()
      .should("be.visible");
    cy.dataCy("org_description")
      .first()
      .should("be.visible");
  });
});
