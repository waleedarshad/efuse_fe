describe("eFuse Portfolio Tests", () => {
  before(() => {
    // Visit MJB's portfolio
    cy.visit("/u/mjb");
  });

  it("Validate bio text", () => {
    cy.dataCy("portfolio_user_name").should("be.visible");
    cy.dataCy("portfolio_bio_text").should("be.visible");
  });
  it("Validate Experience Section", () => {
    cy.get('[data-rbd-draggable-id="experience-component"]').should("be.visible");
    cy.contains("CEO & Founder"); // Experience title
    cy.contains("Founded in August 2018"); // Experience description
  });

  it("Validate Education Section", () => {
    cy.get('[data-rbd-draggable-id="education-component"]').should("be.visible");
    cy.contains("Ohio University");
    cy.contains("Student Alumni Board");
  });
  it("Validate Skills Section", () => {
    cy.get('[data-rbd-draggable-id="skills-component"]').should("be.visible");
    cy.contains("Finance");
    cy.contains("Teamwork");
  });
  it("Validate Follow button works", () => {
    cy.get('button:contains("Follow")')
      .first()
      .click();
    cy.get("button:contains('Login')")
      .first()
      .click();
  });
});
