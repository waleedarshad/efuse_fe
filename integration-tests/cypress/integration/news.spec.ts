describe("eFuse news articles page tests", () => {
  before(() => {
    // Visit eFuse News
    cy.visit("/news");
  });

  it("Apply a filter", () => {
    cy.dataCy("filter", "contains")
      .eq(0) // Choose a news filter. Ex: Pipeline
      .click()
      .invoke("text") // Gets the text of the chosen filter
      .then(
        text => cy.url().should("contain", text.toLowerCase()) // Check the url after click
      );
  });

  it("Click on a news article", () => {
    cy.dataCy("widecard_link")
      .eq(2)
      .click();
    cy.dataCy("news_article_title").should("be.visible");
  });
});
