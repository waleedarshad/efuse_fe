import { loginDetails } from "../fixtures";

describe("eFuse login tests", () => {
  before(() => {
    // Visit login page
    cy.visit("/login");
  });

  it("Enter email address", () => {
    cy.dataCy("login_email_input").type(loginDetails.username);
  });
  it("Enter password", () => {
    cy.dataCy("login_password_input").type(loginDetails.password);
  });
  it("Click Login", () => {
    cy.get('button:contains("Log in")')
      .first()
      .click();
    cy.url().should("contain", "lounge/featured");
  });
});
