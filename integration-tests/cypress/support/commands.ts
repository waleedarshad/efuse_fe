// ***********************************************
// This example commands.ts shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************

// eslint-disable @typescript/interface-name
declare global {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace Cypress {
    interface Chainable {
      /**
       * Custom command to select DOM element by data-cy attribute.
       * @param value Name of the attribute
       * @param condition Can be either `eq` or `contains`. Defaults to `eq`
       *
       * @example
       * cy.dataCy("org_name").first()
       * cy.dateCy("org", "contains") // Gets all data-cy attributes starting with org
       */
      dataCy(value: string, condition?: "eq" | "contains"): Chainable<Element>;
    }
  }
}

Cypress.Commands.add("dataCy", (value: string, condition: "eq" | "contains" = "eq") => {
  const operator = condition === "eq" ? "=" : "*=";
  return cy.get(`[data-cy${operator}"${value}"]`);
});

export {};
