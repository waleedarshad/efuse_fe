const opentracing = require("opentracing");

const url = require("url");

exports.default = options => {
  const { tracer } = options;

  return (req, res, next) => {
    const wireCtx = tracer.extract(opentracing.FORMAT_HTTP_HEADERS, req.headers);
    const { pathname } = url.parse(req.url);
    const span = tracer.startSpan(pathname, { childOf: wireCtx });
    span.logEvent("request_received");

    // include some useful tags on the trace
    span.setTag("http.method", req.method);
    span.setTag("span.kind", "server");
    span.setTag("http.url", req.url);
    span.setTag("http.route", pathname);

    // include trace ID in headers so that we can debug slow requests we see in
    // the browser by looking up the trace ID found in response headers
    const responseHeaders = {};
    tracer.inject(span, opentracing.FORMAT_TEXT_MAP, responseHeaders);

    Object.keys(responseHeaders).forEach(key => res.setHeader(key, responseHeaders[key]));

    // add the span to the request object for handlers to use
    Object.assign(req, { span });

    // finalize the span when the response is completed
    const finishSpan = function finishSpan() {
      span.logEvent("request_finished");

      span.setOperationName(pathname);
      span.setTag("http.status_code", res.statusCode);

      if (res.statusCode >= 500) {
        span.setTag("error", true);
        span.setTag("sampling.priority", 1);
      }
      span.finish();
    };
    res.on("close", finishSpan);
    res.on("finish", finishSpan);

    next();
  };
};
