/**
 * These resources will generally be associated with the actions CREATE, READ, UPDATE, DELETE or ALL
 */
export enum EFuseResourceEnum {
  ERENA = "erena",
  LEAGUE = "league",
  NEWS_ARTICLE = "news_article",
  ORGANIZATION = "organization",
  PIPELINE_RECRUITMENT = "pipeline_recruitment"
}

/**
 * These clippy resources will generally be associated with the action "ACCESS"
 */
export enum ClippyResourceEnum {
  CLIPPY_MUTE = "clippy_mute",
  CLIPPY_REPLAY = "clippy_replay",
  CLIPPY_RESET = "clippy_reset",
  CLIPPY_SHOUTOUT = "clippy_shoutout",
  CLIPPY_STOP = "clippy_stop",
  CLIPPY_STOP_ALL = "clippy_stop_all",
  CLIPPY_WATCH = "clippy_watch",
  CLIPPY_UNMUTE = "clippy_unmute"
}
