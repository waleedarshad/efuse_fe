export enum ClippyCommandKindEnum {
  MUTE = "MUTE",
  REPLAY = "REPLAY",
  RESET = "RESET",
  SHOUTOUT = "SHOUTOUT",
  STOP = "STOP",
  STOP_ALL = "STOP_ALL",
  UNMUTE = "UNMUTE",
  WATCH = "WATCH"
}

export enum ClippyAllowedRolesEnum {
  BROADCASTER = "BROADCASTER",
  MODERATOR = "MODERATOR",
  SUBSCRIBER = "SUBSCRIBER",
  VIP = "VIP",
  EVERYONE = "EVERYONE"
}

export enum ClippyDateRangeEnum {
  DAY = "DAY",
  WEEK = "WEEK",
  MONTH = "MONTH",
  YEAR = "YEAR",
  ALL = "ALL"
}

export enum ClippySortOrderEnum {
  TOP = "TOP",
  RANDOM = "RANDOM"
}

export enum RolesEnum {
  ADMIN = "admin",
  EMPLOYEE = "employee"
}

export enum StatusSwitch {
  pending = "Request Sent",
  banned = "Banned",
  approved = "Joined",
  kicked = "Join",
  new = "Join",
  owner = "Owned"
}
