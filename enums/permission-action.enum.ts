export enum PermissionActionEnum {
  ACCESS = "ACCESS",
  ALL = "ALL",
  CREATE = "CREATE",
  DELETE = "DELETE",
  READ = "READ",
  UPDATE = "UPDATE"
}
