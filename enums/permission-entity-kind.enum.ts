export enum PermissionEntityKindEnum {
  ORGANIZATIONS = "ORGANIZATIONS",
  USERS = "USERS"
}
