export enum ResourceOwnerKindEnum {
  ORGANIZATIONS = "ORGANIZATIONS",
  USERS = "USERS"
}
