import { configure } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import "jest-canvas-mock";
import enableHooks from "jest-react-hooks-shallow";

configure({ adapter: new Adapter() });
enableHooks(jest, { dontMockByDefault: true });

jest.mock("next/router", () => ({
  push: jest.fn(),
  useRouter: jest.fn(() => ({ push: jest.fn(), asPath: "/" })),
  withRouter: component => {
    // eslint-disable-next-line no-param-reassign
    component.defaultProps = {
      ...component.defaultProps,
      router: {
        pathname: "some-path",
        query: "some-query"
      }
    };

    return component;
  }
}));

jest.mock("next/config", () =>
  jest.fn(() => ({
    publicRuntimeConfig: {
      environment: "development",
      cdnBaseUrl: "cdn.my.house"
    }
  }))
);

jest.mock("axios");

window.analytics = {
  track: jest.fn(),
  page: jest.fn()
};

window.VL = {
  load: jest.fn()
};

window.matchMedia =
  window.matchMedia ||
  (() => ({
    matches: false,
    addListener: () => {},
    removeListener: () => {}
  }));
