import dynamic from "next/dynamic";
import { useEffect } from "react";
import { Col } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { toggleLoader } from "../../../store/actions/loaderActions";
import { resetUnreadMessageCount } from "../../../store/actions/messageActions";
import Internal from "../../Layouts/Internal/Internal";
import ChatWindow from "./ChatWindow/ChatWindow";

const SingleChat = dynamic(import("./SingleChat"), { ssr: false });

const Inbox = () => {
  const isWindowView = useSelector(state => state.messages.isWindowView);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(toggleLoader(false));
    dispatch(resetUnreadMessageCount());

    analytics.page("Messaging");
  }, []);

  return (
    <Internal metaTitle="eFuse | Messages" isWindowView={isWindowView} fluid>
      <Col md={4}>
        <ChatWindow />
      </Col>
      <Col md={8}>
        <SingleChat />
      </Col>
    </Internal>
  );
};

export default Inbox;
