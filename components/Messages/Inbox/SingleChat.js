import React from "react";
import dynamic from "next/dynamic";
import { connect } from "react-redux";
import { Card } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { StreamChat } from "stream-chat";
import { faChevronLeft } from "@fortawesome/pro-solid-svg-icons";
import getConfig from "next/config";
import "stream-chat-css/dist/css/index.css";

import { getChatChannels, clearChat } from "../../../store/actions/messageActions";
import { logoutUser } from "../../../store/actions/authActions";
import Style from "./ChatWindow/ChatWindow.module.scss";
import { getUserById } from "../../../store/actions/common/userAuthActions";
import { UploadTypes } from "../../DirectUpload/DirectUpload";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";
import Styles from "./SingleChat.module.scss";

const Chat = dynamic(() => import("stream-chat-react").then(module => module.Chat), { ssr: false });
const Channel = dynamic(() => import("stream-chat-react").then(module => module.Channel), { ssr: false });
const Thread = dynamic(() => import("stream-chat-react").then(module => module.Thread), { ssr: false });
const Window = dynamic(() => import("stream-chat-react").then(module => module.Window), { ssr: false });
const MessageList = dynamic(() => import("stream-chat-react").then(module => module.MessageList), { ssr: false });
const MessageInput = dynamic(() => import("stream-chat-react").then(module => module.MessageInput), { ssr: false });

const { publicRuntimeConfig } = getConfig();

const { streamChatKey } = publicRuntimeConfig;

class SingleChat extends React.Component {
  constructor() {
    super();

    this.state = {
      chatClient: null,
      channel: null,
      show: false,
      loadedChatId: ""
    };
  }

  componentDidMount() {
    const { loadedChatId } = this.state;
    const { selectedChatId, currentUser } = this.props;

    if (selectedChatId && selectedChatId !== loadedChatId && currentUser && currentUser.streamChatToken) {
      this.loadChat(selectedChatId);
    }
  }

  componentDidUpdate() {
    const { loadedChatId } = this.state;
    const { selectedChatId, currentUser } = this.props;

    if (selectedChatId && selectedChatId !== loadedChatId && currentUser && currentUser.streamChatToken) {
      this.loadChat(selectedChatId);
    }
  }

  componentWillUnmount() {
    if (this.state.chatClient) this.state.chatClient.disconnect();
  }

  async loadChat(selectedChatId) {
    const { getChatChannels: getChatCHannelsMethod, currentUser } = this.props;

    analytics.track("STREAM_CHAT_CONNECTION");

    const chatClient = new StreamChat(streamChatKey);
    if (currentUser.streamChatToken) {
      const userToken = currentUser.streamChatToken;

      chatClient.setUser(
        {
          id: currentUser.id,
          name: currentUser.name,
          image: currentUser.profilePicture.url
        },
        userToken
      );

      const channel = chatClient.channel("messaging", selectedChatId);
      const state = await channel.watch();
      let otherUser = null;

      state.members.forEach(member => {
        if (member.user.id !== currentUser.id) otherUser = member.user.id;
      });

      if (otherUser) this.props.getUserById(otherUser);

      // update channel list on left when new message is sent -> this way current channel moves to top of channel list
      // track in analytics when new message is created
      channel.on("message.new", () => {
        getChatCHannelsMethod(currentUser);
        analytics.track("MESSAGES_CREATE");
      });

      this.setState({
        chatClient,
        channel,
        show: true,
        loadedChatId: selectedChatId
      });
    }
  }

  render() {
    const { chatClient, channel, show } = this.state;
    const { selectedChatId, selectedChatName, selectedChatUser } = this.props;
    // No chat selected
    if (!selectedChatId) return <div />;

    // Chat selected, but loading
    if (!show || !chatClient || !channel) return <div>Loading chat...</div>;

    return (
      <Card className={Styles.chatContainer}>
        <Chat client={chatClient} theme="messaging light">
          <Channel channel={channel} acceptedFiles={UploadTypes.imageAndVideo}>
            <Window>
              <Card.Header className={Style.channelListHeader}>
                <FontAwesomeIcon
                  style={{ marginRight: "20px" }}
                  icon={faChevronLeft}
                  className={Style.mobileBackArrow}
                  onClick={() => {
                    this.props.clearChat();
                  }}
                />
                <div className={Style.imageContainer}>
                  <img
                    src={
                      selectedChatUser.profilePicture
                        ? selectedChatUser.profilePicture.url
                        : "https://cdn.efuse.gg/uploads/static/global/mindblown_white.png"
                    }
                    alt="profile"
                  />
                </div>
                <div className={Style.nameContainer}>
                  <h5 className="headerTitle">{selectedChatName}</h5>
                  <span>{selectedChatUser.username ? `@${selectedChatUser.username}` : " "}</span>
                </div>
                <div className={Style.viewBtnContainer}>
                  <EFRectangleButton
                    internalHref="/u/[u]"
                    internalAs={`/u/${selectedChatUser.username}`}
                    text="View Portfolio"
                    disabled={!selectedChatUser.username}
                  />
                </div>
              </Card.Header>
              <MessageList />
              <MessageInput />
            </Window>
            <Thread />
          </Channel>
        </Chat>
      </Card>
    );
  }
}

const mapStateToProps = state => ({
  selectedChatId: state.messages.selectedChatId,
  selectedChatName: state.messages.selectedChatName,
  currentUser: state.auth.currentUser,
  selectedChatUser: state.user.currentUser
});

export default connect(mapStateToProps, {
  getChatChannels,
  logoutUser,
  clearChat,
  getUserById
})(SingleChat);
