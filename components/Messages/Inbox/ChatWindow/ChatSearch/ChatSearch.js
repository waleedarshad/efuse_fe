import React, { Component } from "react";
import { FormGroup } from "react-bootstrap";

import SearchField from "../../../../SearchField/SearchField";
import Hr from "../../../../Hr/Hr";

class ChatSearch extends Component {
  handleChange(event) {
    const query = event.target.value;
    setTimeout(() => {
      this.props.searchFollower(query);
    }, 500);
  }

  render() {
    return (
      <>
        <FormGroup className="p-2 mb-0 pl-4">
          <SearchField placeholder="Search" onChange={this.handleChange.bind(this)} />
        </FormGroup>
        <Hr customClass="m-0" />
      </>
    );
  }
}

export default ChatSearch;
