import React, { useEffect, useState } from "react";
import { Card } from "react-bootstrap";
import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus, faWindowMaximize } from "@fortawesome/pro-solid-svg-icons";
import Link from "next/link";
import { connect, useSelector, useDispatch } from "react-redux";

import ChatListing from "./ChatListing/ChatListing";
import FollowingListModal from "../FollowingListModal";
import {
  selectChat,
  setWindowChatView,
  chatReset,
  toggleFollowingListModal,
  getChatChannels
} from "../../../../store/actions/messageActions";
import Style from "./ChatWindow.module.scss";
import { fetchChatMessages, joinChatRoom } from "../../../../helpers/ChatHelper";
import EFTooltip from "../../../tooltip/EFTooltip/EFTooltip";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";

const ChatWindow = ({ asWindow, height, minHeight, selectChat, setWindowChatView, chatReset }) => {
  const dispatch = useDispatch();

  const [show, setShow] = useState(false);
  const [fetchRecord, setFetchRecord] = useState(false);
  const [page, setPage] = useState(1);

  const currentUser = useSelector(state => state.auth.currentUser);
  const otherPerson = useSelector(state => state.messages.otherPerson);
  const showNewConversationModal = useSelector(state => state.messages.showFollowingListModal);
  const channels = useSelector(state => state.messages.channels);
  const selectedChatId = useSelector(state => state.messages.selectedChatId);

  const toggleWindow = () => {
    setShow(!show);
  };

  const loadMore = () => {
    setPage(page + 1);
  };

  const getChat = (id, asWindow, name) => {
    fetchChatMessages(id, asWindow, { selectChat, setWindowChatView, chatReset }, name);
    setTimeout(() => joinChatRoom({ currentUser, otherPerson }), 500);
  };

  useEffect(() => {
    if (currentUser && !fetchRecord) {
      setFetchRecord(true);
      dispatch(getChatChannels(currentUser));
    }
  }, [currentUser]);

  const openModal = () => {
    dispatch(toggleFollowingListModal(true));
  };

  return (
    <Card
      className={`customCard pb-0 ${selectedChatId ? Style.hideElement : ""} ${!asWindow && "mb-3"} ${asWindow &&
        Style.chatWindowWrapper}`}
    >
      {showNewConversationModal && <FollowingListModal />}
      {asWindow ? (
        <div className={Style.windowHeader}>
          <h5>
            <EFRectangleButton
              text="Messaging"
              className={Style.toggleBtn}
              onClick={() => toggleWindow()}
              size="small"
            />
          </h5>
          <Link href="/messages">
            <FontAwesomeIcon className={Style.maximizeLink} icon={faWindowMaximize} />
          </Link>
        </div>
      ) : (
        <Card.Header className={`${Style.channelListHeader} pb-2`}>
          <h5 className="headerTitle">Messaging</h5>
          <EFTooltip tooltipPlacement="top" tooltipContent="Start a new conversation">
            <FontAwesomeIcon icon={faPlus} onClick={() => openModal()} />
          </EFTooltip>
        </Card.Header>
      )}
      <div className={show ? null : Style.chatWindowBody}>
        <ChatListing
          height={height}
          minHeight={minHeight}
          channels={channels}
          loadMore={loadMore}
          pagination={null}
          getChat={getChat}
          asWindow={asWindow}
        />
      </div>
    </Card>
  );
};

ChatWindow.propTypes = {
  asWindow: PropTypes.bool,
  height: PropTypes.string,
  minHeight: PropTypes.string
};

ChatWindow.defaultProps = {
  asWindow: false,
  height: "600px",
  minHeight: "500px"
};

export default connect(null, {
  selectChat,
  setWindowChatView,
  chatReset
})(ChatWindow);
