import { ListGroupItem } from "react-bootstrap";
import React from "react";
import Style from "./ChatListItem.module.scss";

import OnlineCircle from "../../../../../OnlineCircle/OnlineCircle";
import EFButton from "../../../../../Buttons/EFButton/EFButton";

class ChatListItem extends React.PureComponent {
  getProfilePicture(otherUser) {
    const defaultPicture = "https://cdn.efuse.gg/uploads/static/global/mindblown_white.png";

    if (typeof otherUser.image === "string") return otherUser.image;

    if (typeof otherUser.image === "object" && otherUser.image?.url) return otherUser.asMutable().image.url;

    return defaultPicture;
  }

  render() {
    const { chat, onClick, channel, unreadMessages, selectedChat, currentUser } = this.props;
    let otherUser = null;

    for (const userId in channel.state.members) {
      if (userId != currentUser.id) otherUser = userId;
    }
    const online = channel.state.members[otherUser].user.online && !channel.state.members[otherUser].user.invisible;
    const unreads = channel.countUnread();

    return (
      <ListGroupItem className={`${Style.chatItemWrapper} ${selectedChat ? Style.selected : ""}`}>
        <EFButton className={Style.chatItemBtn} onClick={!selectedChat && onClick}>
          <div className={Style.contentWrapper}>
            <div className={Style.imageContainer}>
              <img alt="profile picture" src={this.getProfilePicture(channel.state.members[otherUser].user)} />
            </div>
            <div className={Style.nameContainer}>
              <strong className="mr-1">{channel.state.members[otherUser].user.name}</strong>
              <OnlineCircle online={online} />
              <p className={Style.description}>
                {channel.state.messages.length > 0
                  ? channel.state.messages[channel.state.messages.length - 1].text
                  : "No messages yet."}
              </p>
            </div>
            {unreads > 0 && <span className={Style.readBadge}>{unreads}</span>}
          </div>
        </EFButton>
      </ListGroupItem>
    );
  }
}

export default ChatListItem;
