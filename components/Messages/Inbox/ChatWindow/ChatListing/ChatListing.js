import React, { Component } from "react";
import { connect } from "react-redux";
import { ListGroup } from "react-bootstrap";
import { Scrollbar } from "react-scrollbars-custom";

import ChatListItem from "./ChatListItem/ChatListItem";
import s from "./ChatListing.module.scss";
import AnimatedLogo from "../../../../AnimatedLogo";
import EFRectangleButton from "../../../../Buttons/EFRectangleButton/EFRectangleButton";

class ChatListing extends Component {
  state = {
    triggeredFirstChat: false
  };

  componentDidUpdate() {
    const { currentUser } = this.props;

    if (
      this.props.channels &&
      this.props.channels.length > 0 &&
      !this.state.triggeredFirstChat &&
      this.props.getChat &&
      !this.props.selectedChatId
    ) {
      const firstChannel = this.props.channels[0];
      let otherUser = null;
      for (const userId in firstChannel.state.members) {
        if (userId != currentUser.id) otherUser = userId;
      }
      this.props.getChat(firstChannel.id, false, firstChannel.state.members[otherUser].user.name || " ");
      this.setState({ triggeredFirstChat: true });
    }
  }

  render() {
    const { height, minHeight, channels, asWindow, contentInstances, selectedChatId, currentUser } = this.props;

    const chatList = channels ? (
      channels.map((c, i) => (
        <ChatListItem
          key={i}
          channel={c}
          currentUser={currentUser}
          selectedChat={c.id == selectedChatId}
          unreadMessages={false}
          onClick={e => {
            let otherUser = null;
            for (const userId in c.state.members) {
              if (userId != currentUser.id) otherUser = userId;
            }
            this.props.getChat(c.id, asWindow, c.state.members[otherUser].user.name || " ");
          }}
        />
      ))
    ) : (
      <></>
    );

    return (
      <Scrollbar style={{ height, minHeight }}>
        <ListGroup>
          {!channels ? (
            <div className={s.loadMoreLogo}>
              <AnimatedLogo theme="inline" />
            </div>
          ) : (
            <>
              {channels.length > 0 ? (
                chatList
              ) : (
                <h6 className={s.noChannels}>
                  You don't have any
                  <br />
                  messages at the moment
                </h6>
              )}
              {/* Hiding load more button until functional. Chat will be restricted to 30 for launch */}
              {false && this.props.pagination.hasNextPage && (
                <EFRectangleButton
                  text=" Show More..."
                  className={s.showMore}
                  onClick={this.props.loadMore}
                  width="fullWidth"
                  colorTheme="light"
                />
              )}
            </>
          )}
        </ListGroup>
      </Scrollbar>
    );
  }
}

const mapStateToProps = state => ({
  currentUser: state.auth.currentUser,
  contentInstances: state.loader.contentInstances,
  selectedChatId: state.messages.selectedChatId
});

export default connect(mapStateToProps)(ChatListing);
