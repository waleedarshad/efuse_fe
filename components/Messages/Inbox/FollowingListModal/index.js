import React from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  toggleFollowingListModal,
  getChatChannels,
  startChatWith,
  searchUsersToChat
} from "../../../../store/actions/messageActions";
import Style from "./index.module.scss";
import SearchBox from "./SearchBox";
import EmptyComponent from "../../../EmptyComponent/EmptyComponent";
import EFPrimaryModal from "../../../Modals/EFPrimaryModal/EFPrimaryModal";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";

const FollowingListModal = () => {
  const dispatch = useDispatch();
  const currentUser = useSelector(state => state.auth.currentUser);
  const followers = useSelector(state => state.messages.chatFollowers);

  const handleClose = () => {
    dispatch(toggleFollowingListModal(false));
  };

  const initConversation = (userId, userName) => {
    dispatch(startChatWith(userId, userName));
    dispatch(getChatChannels(currentUser));
    dispatch(toggleFollowingListModal(false));
  };

  const searchQuery = query => {
    dispatch(searchUsersToChat(query));
  };

  const content = (
    <div className={Style.followingUserList}>
      {followers.map((user, i) => {
        return (
          <div key={i} className={Style.userRow}>
            <div className={Style.photo}>
              <img
                width="80"
                src={
                  user.profilepicture
                    ? user.profilepicture
                    : "https://cdn.efuse.gg/uploads/static/global/mindblown_white.png"
                }
                alt={user.name}
              />
              <div className={Style.username}>
                <b style={{ marginTop: "15px", display: "block" }}>{user.name}</b>@
                {user.username ? user.username : "username"}
              </div>
            </div>
            <div className="button">
              <EFRectangleButton
                text="Message"
                onClick={() => {
                  initConversation(user._id, user.name);
                }}
              />
            </div>
          </div>
        );
      })}
    </div>
  );

  return (
    <EFPrimaryModal
      title="Start New Conversation"
      widthTheme="medium"
      isOpen
      onClose={handleClose}
      allowBackgroundClickClose={false}
    >
      <div>
        <SearchBox searchFollower={query => searchQuery(query)} />
        {followers.length === 0 ? <EmptyComponent text="No gamers available to message." /> : <div>{content}</div>}
      </div>
    </EFPrimaryModal>
  );
};

export default FollowingListModal;
