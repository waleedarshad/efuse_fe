import { FormControl } from "react-bootstrap";
import React from "react";
import uniqueId from "lodash/uniqueId";
import Style from "./PasswordSuggestionsList.module.scss";

const PasswordSuggestionsList = ({ suggestions }) => {
  return (
    <ul className={Style.suggestionUl}>
      {suggestions.map(suggestion => (
        <li key={uniqueId()}>
          <FormControl.Feedback type="invalid" style={{ display: "inline" }}>
            {suggestion}
          </FormControl.Feedback>
        </li>
      ))}
    </ul>
  );
};

export default PasswordSuggestionsList;
