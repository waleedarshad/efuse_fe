import React from "react";
import PropTypes from "prop-types";
import dynamic from "next/dynamic";
import Style from "./PasswordStrengthIndicator.module.scss";

const ReactPasswordStrength = dynamic(() => import("react-password-strength"), {
  ssr: false
});

const PasswordStrengthIndicator = ({ defaultValue, placeholder, displayType, passwordChangeCallback }) => {
  const minimumPasswordCharacters = 8;
  const minimumPasswordScore = 2;

  return (
    <ReactPasswordStrength
      className={Style.passwordStrength}
      defaultValue={defaultValue}
      minLength={minimumPasswordCharacters}
      minScore={minimumPasswordScore}
      scoreWords={["very weak", "weak", "okay", "good", "strong"]}
      changeCallback={passwordChangeCallback}
      inputProps={{
        name: "password",
        autoComplete: "off",
        placeholder,
        type: displayType,
        required: true,
        id: "password-strength"
      }}
    />
  );
};

PasswordStrengthIndicator.propTypes = {
  placeholder: PropTypes.string
};

PasswordStrengthIndicator.defaultProps = {
  placeholder: "eg. Password!234"
};

export default PasswordStrengthIndicator;
