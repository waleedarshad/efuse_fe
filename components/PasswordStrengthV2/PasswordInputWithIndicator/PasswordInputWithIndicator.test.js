import { mount } from "enzyme";
import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye, faEyeSlash } from "@fortawesome/pro-solid-svg-icons";
import { FormControl } from "react-bootstrap";
import PasswordInputWithIndicator from "./PasswordInputWithIndicator";
import PasswordStrengthIndicator from "../PasswordStrengthIndicator/PasswordStrengthIndicator";
import PasswordSuggestionsList from "../PasswordSuggestionsList/PasswordSuggestionsList";

const mockOnPasswordChange = jest.fn();

describe("PasswordInputWithIndicator", () => {
  it("renders the correct icon by default", () => {
    const subject = mount(<PasswordInputWithIndicator onPasswordChange={mockOnPasswordChange} />);

    expect(subject.find(FontAwesomeIcon).prop("icon")).toEqual(faEye);
  });

  it("toggles the icon and password display type on click", () => {
    const subject = mount(<PasswordInputWithIndicator onPasswordChange={mockOnPasswordChange} />);

    expect(subject.find(FontAwesomeIcon).prop("icon")).toEqual(faEye);
    expect(subject.find(PasswordStrengthIndicator).prop("displayType")).toEqual("password");

    subject.find(FontAwesomeIcon).simulate("click");

    expect(subject.find(FontAwesomeIcon).prop("icon")).toEqual(faEyeSlash);
    expect(subject.find(PasswordStrengthIndicator).prop("displayType")).toEqual("text");

    subject.find(FontAwesomeIcon).simulate("click");

    expect(subject.find(FontAwesomeIcon).prop("icon")).toEqual(faEye);
    expect(subject.find(PasswordStrengthIndicator).prop("displayType")).toEqual("password");
  });

  it("displays a list of password suggestions if below minimum score and has result", () => {
    const subject = mount(
      <PasswordInputWithIndicator defaultValue="123hello" onPasswordChange={mockOnPasswordChange} />
    );

    expect(subject.find(PasswordSuggestionsList).children()).toHaveLength(1);
    expect(subject.find(PasswordSuggestionsList).prop("suggestions")).toEqual([
      "Add another word or two. Uncommon words are better."
    ]);
  });

  it("displays an empty list if no result", () => {
    const subject = mount(<PasswordInputWithIndicator defaultValue="123" onPasswordChange={mockOnPasswordChange} />);

    expect(subject.find(PasswordSuggestionsList).prop("suggestions")).toEqual([]);
  });

  it("displays a warning if result", () => {
    const subject = mount(
      <PasswordInputWithIndicator defaultValue="123hello" onPasswordChange={mockOnPasswordChange} />
    );

    expect(
      subject
        .find(FormControl.Feedback)
        .at(1)
        .prop("children")
    ).toEqual("Add another word or two. Uncommon words are better.");
  });

  it("displays correct message if password is empty", () => {
    const subject = mount(<PasswordInputWithIndicator defaultValue="" onPasswordChange={mockOnPasswordChange} />);

    expect(
      subject
        .find(FormControl.Feedback)
        .at(0)
        .text()
    ).toEqual("Password is required");
  });
});
