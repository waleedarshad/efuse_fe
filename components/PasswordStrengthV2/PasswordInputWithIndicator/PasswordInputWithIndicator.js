import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEyeSlash, faEye } from "@fortawesome/pro-solid-svg-icons";
import { FormControl } from "react-bootstrap";

import PasswordStrengthIndicator from "../PasswordStrengthIndicator/PasswordStrengthIndicator";
import PasswordSuggestionsList from "../PasswordSuggestionsList/PasswordSuggestionsList";
import Style from "./PasswordInputWithIndicator.module.scss";

const PasswordInputWithIndicator = ({ defaultValue, onPasswordChange, dark }) => {
  const minimumPasswordScore = 2;

  const [passwordDisplayType, setPasswordDisplayType] = useState("password");
  const [passwordIcon, setPasswordIcon] = useState(faEye);
  const [currentPassword, setCurrentPassword] = useState("");
  const [passwordWarning, setPasswordWarning] = useState("");
  const [passwordSuggestions, setPasswordSuggestions] = useState([]);

  const handlePasswordDisplay = () => {
    setPasswordDisplayType(passwordDisplayType === "password" ? "text" : "password");
    setPasswordIcon(passwordIcon === faEye ? faEyeSlash : faEye);
  };

  const handlePasswordStrengthOnChange = (currentPasswordState, result) => {
    const { isValid, password, score } = currentPasswordState;

    setCurrentPassword(password);
    onPasswordChange(password, isValid);

    if (result && score < minimumPasswordScore) {
      const { warning, suggestions } = result.feedback;
      setPasswordWarning(warning);
      setPasswordSuggestions(suggestions);
    } else {
      setPasswordWarning("");
      setPasswordSuggestions([]);
    }
  };

  return (
    <>
      <PasswordStrengthIndicator
        defaultValue={defaultValue}
        displayType={passwordDisplayType}
        passwordChangeCallback={handlePasswordStrengthOnChange}
      />
      <FontAwesomeIcon
        icon={passwordIcon}
        className={`${Style.eye} ${dark && Style.dark}`}
        onClick={handlePasswordDisplay}
      />
      {currentPassword?.length === 0 && (
        <FormControl.Feedback type="invalid" style={{ display: "block" }}>
          Password is required
        </FormControl.Feedback>
      )}
      {passwordWarning && (
        <FormControl.Feedback type="invalid" style={{ display: "block" }}>
          {passwordWarning}
        </FormControl.Feedback>
      )}
      <PasswordSuggestionsList suggestions={passwordSuggestions} />
    </>
  );
};

export default PasswordInputWithIndicator;
