import React from "react";
import EFCard from "../../Cards/EFCard/EFCard";
import Style from "./Card.module.scss";
import CardContent from "./CardContent/CardContent";

const Card = ({ card, onChange, value, valueKey, size }) => {
  const isSelected = value === card[valueKey];

  return (
    <div aria-hidden="true" className={`${Style.cardWrapper} ${Style[size]}`} onClick={() => onChange(card[valueKey])}>
      <EFCard widthTheme="fullWidth" isSelected={isSelected} heightTheme="fullHeight">
        <div className={`${Style.contentWrapper} ${isSelected && Style.selected}`} data-cy={card.name}>
          <CardContent icon={card.icon} name={card.name} description={card.description} isSelected={isSelected} />
        </div>
      </EFCard>
    </div>
  );
};

export default Card;
