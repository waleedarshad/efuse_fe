import EFImage from "../../../EFImage/EFImage";
import Style from "./CardContent.module.scss";

const SimpleContent = ({ icon, name, description, isSelected }) => {
  return (
    <div className={Style.cardContent}>
      <div className={Style.textAndIconContainer}>
        {icon && <EFImage src={icon} className={Style.leftIcon} />}
        <div className={Style.centerTextContainer}>
          <div className={`${Style.name} ${isSelected && Style.selected}`}>{name}</div>
          {description && <div className={`${Style.description} ${isSelected && Style.selected}`}>{description}</div>}
        </div>
      </div>
    </div>
  );
};

export default SimpleContent;
