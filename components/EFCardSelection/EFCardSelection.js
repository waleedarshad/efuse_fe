import React from "react";
import Card from "./Card/Card";
import Style from "./EFCardSelection.module.scss";

const EFCardSelection = ({ cards, onChange, size, value, valueKey }) => {
  return (
    <div className={Style.cardContainer}>
      {cards?.map((card, i) => {
        // eslint-disable-next-line react/no-array-index-key
        return <Card card={card} onChange={onChange} value={value} valueKey={valueKey} size={size} key={i} />;
      })}
    </div>
  );
};

EFCardSelection.defaultProps = {
  valueKey: "value",
  size: "large"
};

export default EFCardSelection;
