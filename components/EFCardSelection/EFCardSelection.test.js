import React from "react";
import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import EFCardSelection from "./EFCardSelection";

describe("EFSelectionCard", () => {
  it("Renders equal number of cards passed", () => {
    const cards = [
      {
        name: "Abc",
        description: "description 1"
      },
      {
        name: "XYZ",
        description: "description 2"
      }
    ];

    render(<EFCardSelection cards={cards} maxSelectableCards={1} />);
    expect(screen.getByText("Abc"));
    expect(screen.getByText("XYZ"));
    expect(screen.getByText(cards[0].description));
    expect(screen.getByText(cards[1].description));
  });
});
