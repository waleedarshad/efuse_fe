import React from "react";
import EFCardSelection from "./EFCardSelection";

export default {
  title: "Shared/EFCardSelection",
  component: EFCardSelection,
  argTypes: {
    cards: {
      control: {
        type: "array"
      }
    },
    maxSelectableCards: {
      control: {
        type: "number"
      }
    }
  }
};

const Story = args => <EFCardSelection {...args} />;

export const SelectOnlyTwo = Story.bind({});
SelectOnlyTwo.args = {
  cards: [
    {
      title: "Abc",
      description:
        "This is used for testing the EF cards selection component on first try hope this will work without any errors i.e like a charm."
    },
    {
      title: "XYZ",
      description:
        "This is used for testing the EF cards selection component on first try hope this will work without any errors i.e like a charm."
    },
    {
      title: "I am a gamer",
      description:
        "This is used for testing the EF cards selection component on first try hope this will work without any errors i.e like a charm."
    },
    {
      title: "I am a gamer",
      description:
        "This is used for testing the EF cards selection component on first try hope this will work without any errors i.e like a charm."
    }
  ],
  maxSelectableCards: 2
};
