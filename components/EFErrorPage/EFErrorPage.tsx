import React from "react";
import Tetris from "react-tetris";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSadCry } from "@fortawesome/pro-regular-svg-icons";

import HeaderWrapper from "../Layouts/Internal/HeaderWrapper/HeaderWrapper";
import Styles from "./EFErrorPage.module.scss";

const ErrorIcon = () => {
  return (
    <div className={Styles.errorImage}>
      <FontAwesomeIcon size="8x" icon={faSadCry} />
    </div>
  );
};

const FORBIDDEN_ERROR_CODE = 403;
const EFErrorPage = ({ statusCode }) => {
  const forbiddenView = (
    <>
      <ErrorIcon />
      <h1>{FORBIDDEN_ERROR_CODE} - Access Denied</h1>
      <p>Unfortunately, you are not authorized to access this page.</p>
    </>
  );

  const standardErrorView = (
    <>
      <ErrorIcon />
      <p>
        Unfortunately, eFuse has encountered a <b>{statusCode || "client"}</b> error.
      </p>
      <p>We are sorry for the inconvenience.</p>
    </>
  );

  return (
    <div className={Styles.errorWrapper}>
      <HeaderWrapper />
      <div className={Styles.tetrisContainer}>
        <div className={Styles.tetrisLeftContainer}>
          <div className={Styles.tetrisTextCenter}>
            {statusCode === FORBIDDEN_ERROR_CODE ? forbiddenView : standardErrorView}
          </div>
        </div>
        <div>
          <Tetris>
            {({ Gameboard, PieceQueue, points, linesCleared }) => {
              return (
                <div className={Styles.tetrisParts}>
                  <h1 className={Styles.tetrisTitle}>Tetris</h1>
                  <div className={Styles.tetrisPoints}>
                    <p className={Styles.tetrisPPoints}>Points: {points}</p>
                    <p className={Styles.tetrisPLines}>Lines Cleared: {linesCleared}</p>
                  </div>
                  <div className={Styles.tetrisGameQueue}>
                    <Gameboard />
                    <PieceQueue />
                  </div>
                </div>
              );
            }}
          </Tetris>
        </div>
      </div>
    </div>
  );
};

export default EFErrorPage;
