import Style from "./Ribbon.module.scss";

const Ribbon = ({ text }) => (
  <div className={Style.ribbon}>
    <span>{text}</span>
  </div>
);

export default Ribbon;
