import EFProgressWidget from "./EFProgressWidget";

export default {
  title: "Shared/EFProgressWidget",
  component: EFProgressWidget,
  argTypes: {
    steps: {
      control: {
        type: "string[]"
      }
    },
    currentStepIndex: {
      control: {
        type: "number"
      }
    }
  }
};

const Story = args => <EFProgressWidget {...args} />;

export const Complete = Story.bind({});
Complete.args = {
  steps: ["Org Invited", "Teams Confirmed", "Pools Created", "Extra Step", "Another Extra Step"],
  currentStepIndex: "5"
};

export const Halfway = Story.bind({});
Halfway.args = {
  steps: ["Org Invited", "Teams Confirmed", "Pools Created", "Extra Step", "Another Extra Step"],
  currentStepIndex: "2"
};

export const NoneComplete = Story.bind({});
NoneComplete.args = {
  steps: ["Org Invited", "Teams Confirmed", "Pools Created", "Extra Step", "Another Extra Step"],
  currentStepIndex: "0"
};
