import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheckCircle, faDotCircle } from "@fortawesome/pro-solid-svg-icons";
import ProgressBar from "react-bootstrap/ProgressBar";
import Style from "./EFProgressWidget.module.scss";

interface EFProgressWidgetProps {
  steps: string[];
  currentStepIndex: number;
}

const EFProgressWidget = ({ steps, currentStepIndex }: EFProgressWidgetProps) => {
  const stepState = steps.map((step, index) => {
    const currentStepObj = {
      text: step,
      isLastStep: index === steps.length - 1
    };

    if (index < currentStepIndex) {
      return { ...currentStepObj, progression: "completed", icon: faCheckCircle, now: 100 };
    }
    if (index === currentStepIndex) {
      return { ...currentStepObj, progression: "inProgress", icon: faDotCircle, now: 0 };
    }

    return { ...currentStepObj, progression: "needsCompleted", icon: faDotCircle, now: 0 };
  });

  return (
    <div className={Style.alignItems}>
      {stepState.map((step, index) => {
        return (
          <div className={Style.itemWrapper} key={index}>
            <FontAwesomeIcon className={Style[step.progression]} icon={step.icon} />

            {!step.isLastStep && <ProgressBar className={Style.progressBar} now={step.now} />}

            <div className={Style.stepText}>{step.text}</div>
          </div>
        );
      })}
    </div>
  );
};

export default EFProgressWidget;
