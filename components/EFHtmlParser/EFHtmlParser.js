/* eslint-disable efuse-lint/no-react-html-parser */
import ReactHtmlParser from "react-html-parser";
import { validateHtml, validateLearningPageHtml } from "../../helpers/ValidationHelper";

/**
 * EFHtmlParser is a drop in replacement for `ReactHtmlParser` but with added validation for XSS attacks.
 *
 * By default the standard validation parser is used, but alternative parsers can be defined in `ValidationHelper.js`
 * and used within this component.
 */
export const EFHtmlParser = ({ children, validationType = ValidationParserTypes.STANDARD }) => {
  if (!isValidationTypeValid(validationType)) {
    throw new Error("Invalid validationType, please use the ValidationParserTypes.");
  }

  return <>{ReactHtmlParser(validationType(children))}</>;
};

/**
 * Checks the passed validation type against the allowed functions
 */
const isValidationTypeValid = validationType => {
  return Object.values(ValidationParserTypes).find(val => val.name === validationType.name) !== undefined;
};

/**
 * ValidationParserTypes is used to choose what type of html validation to use.
 *
 * `STANDARD`: Basic HTML validation that allows for the simple components, but prevents scripts, images, and a few other "dangerous" elements
 *
 * `LEARNING_ARTICLE`: HTML validation that allows for a few more elements including images
 */
export const ValidationParserTypes = {
  STANDARD: validateHtml,
  LEARNING_ARTICLE: validateLearningPageHtml
};
/* eslint-enable efuse-lint/no-react-html-parser */
