import React from "react";
import EFSmallImageCard from "./EFSmallImageCard";

export default {
  title: "Cards/EFSmallImageCard",
  component: EFSmallImageCard,
  argsTypes: {
    title: { control: { type: "text" } },
    image: { control: { type: "text" } }
  }
};

const Story = args => (
  <div style={{ width: "280px", margin: "3rem" }}>
    <EFSmallImageCard {...args} />
  </div>
);

export const Config = Story.bind({});

Config.args = {
  image: "https://i.ytimg.com/vi/0E44DClsX5Q/maxresdefault.jpg",
  title: "title text"
};
