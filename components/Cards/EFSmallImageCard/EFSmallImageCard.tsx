import React from "react";
import styled from "styled-components";
import EFCard from "../EFCard/EFCard";
import EFSquareImage from "../../EFSquareImage/EFSquareImage";

interface EFSmallImageCardProps {
  title: string;
  image: string;
  className?: string;
}

const EFSmallImageCard = ({ className, title, image }: EFSmallImageCardProps) => (
  <div className={className}>
    <EFCard widthTheme="fullWidth" shadow="none">
      <SmallCard>
        <EFSquareImage src={image} size={15} spaceRight />
        <Name>{title}</Name>
      </SmallCard>
    </EFCard>
  </div>
);

const SmallCard = styled.div`
  height: 30px;
  padding: 0 7px;
  display: flex;
  align-items: center;
  width: 100%;
`;

const Name = styled.div`
  font-family: Open Sans, sans-serif;
  font-size: 10px;
  line-height: 16px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;

export default EFSmallImageCard;
