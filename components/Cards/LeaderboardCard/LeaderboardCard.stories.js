import React from "react";
import { faFlame } from "@fortawesome/pro-solid-svg-icons";
import { faBolt as fadBolt } from "@fortawesome/pro-duotone-svg-icons";

import LeaderboardCard from "./LeaderboardCard";

export default {
  title: "Cards/LeaderboardCard",
  component: LeaderboardCard,
  argTypes: {
    title: {
      control: {
        type: "text"
      }
    },
    titleClickLink: {
      control: {
        type: "text"
      }
    },
    subTitle: {
      control: {
        type: "text"
      }
    },
    image: {
      control: {
        type: "text"
      }
    },
    icon: {
      control: {
        type: "select",
        options: [faFlame, fadBolt]
      }
    },
    value: {
      control: {
        type: "number"
      }
    },
    footer: {
      control: {
        type: "boolean"
      }
    },
    position: {
      control: {
        type: "number"
      }
    }
  }
};

const Story = args => <LeaderboardCard {...args} />;

export const Card = Story.bind({});
Card.args = {
  title: "Mr. Beast",
  titleClickLink: "https://www.efuse.gg",
  subTitle: "@mrbeast",
  image: "https://yt3.ggpht.com/a/AATXAJynkPnPVO1teU1292tS2f52lj5Tb0ZwM_ML4ounGg=s900-c-k-c0xffffffff-no-rj-mo",
  icon: faFlame,
  value: "127",
  footer: false,
  position: 3
};

export const Footer = Story.bind({});
Footer.args = {
  title: "Mr. Beast",
  titleClickLink: "https://www.efuse.gg",
  subTitle: "@mrbeast",
  image: "https://yt3.ggpht.com/a/AATXAJynkPnPVO1teU1292tS2f52lj5Tb0ZwM_ML4ounGg=s900-c-k-c0xffffffff-no-rj-mo",
  icon: faFlame,
  value: "127",
  footer: true,
  position: 3
};
