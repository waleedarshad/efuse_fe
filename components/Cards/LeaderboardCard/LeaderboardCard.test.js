import { mount } from "enzyme";
import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBolt } from "@fortawesome/pro-duotone-svg-icons";
import { faEye } from "@fortawesome/pro-solid-svg-icons";
import LeaderboardCard from "./LeaderboardCard";
import LEADERBOARD_TYPE from "../../../common/leaderboardTypes";

describe("LeaderboardCard", () => {
  it("displays the correct icon color for views", () => {
    const subject = mount(
      <LeaderboardCard
        leaderboardType={LEADERBOARD_TYPE.VIEWS}
        title="Stephen King"
        titleClickLink="/u/stephenking"
        subTitle="@stephenking"
        icon={faEye}
        value={12000}
        position={55}
        image="url"
      />
    );
    expect(subject.find(FontAwesomeIcon).prop("className")).toEqual("icon blackIcon");
    expect(
      subject
        .find("p")
        .at(0)
        .prop("className")
    ).toEqual("iconText blackIcon");
  });

  it("displays the correct icon color for streaks", () => {
    const subject = mount(
      <LeaderboardCard
        leaderboardType={LEADERBOARD_TYPE.STREAKS}
        title="Stephen King"
        titleClickLink="/u/stephenking"
        subTitle="@stephenking"
        icon={faBolt}
        value={12000}
        position={55}
        image="url"
      />
    );
    expect(subject.find(FontAwesomeIcon).prop("className")).toEqual("icon purpleBolt");
    expect(
      subject
        .find("p")
        .at(0)
        .prop("className")
    ).toEqual("iconText purpleBolt");
  });
});
