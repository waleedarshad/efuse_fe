import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Link from "next/link";
import Style from "./LeaderboardCard.module.scss";
import LEADERBOARD_TYPE from "../../../common/leaderboardTypes";

const LeaderboardCard = ({
  leaderboardType,
  title,
  titleClickLink,
  subTitle,
  image,
  icon,
  value,
  footer,
  position
}) => {
  const getIconColor = () => {
    if (leaderboardType === LEADERBOARD_TYPE.STREAKS) {
      return Style.purpleBolt;
    }
    if (leaderboardType === LEADERBOARD_TYPE.VIEWS || LEADERBOARD_TYPE.ENGAGEMENTS) {
      return Style.blackIcon;
    }
    return Style.blueList;
  };
  return (
    <div className={footer ? Style.leaderboardFooter : Style.leaderboardItem}>
      {position && <div className={Style.position}>{position}</div>}
      <img className={Style.itemAvatar} src={image} alt="Avatar" />
      <div className={Style.itemText}>
        <div className={Style.itemUsername}>
          <Link href="/u/[u]" as={titleClickLink}>
            <span className={Style.title}>{title}</span>
          </Link>
          <FontAwesomeIcon className={`${Style.icon} ${getIconColor(leaderboardType)}`} icon={icon} />
          <p className={`${Style.iconText} ${getIconColor(leaderboardType)}`}>{value}</p>
        </div>
        <p className={Style.itemSubtitle}>{subTitle}</p>
      </div>
    </div>
  );
};
export default LeaderboardCard;
