import React from "react";
import EFSimpleDetailCheckboxCard from "./EFSimpleDetailCheckboxCard";

export default {
  title: "Cards/EFSimpleDetailCheckboxCard",
  component: EFSimpleDetailCheckboxCard,
  argsTypes: {
    id: { control: { type: "text" } },
    name: { control: { type: "text" } },
    title: { control: { type: "text" } },
    bodyText: { control: { type: "text" } },
    isChecked: { control: { type: "boolean" } },
    disabled: { control: { type: "boolean" } },
    imageURL: { control: { type: "text" } }
  }
};

const Story = args => (
  <div style={{ width: "280px", margin: "3rem" }}>
    <EFSimpleDetailCheckboxCard {...args} />
  </div>
);

export const Config = Story.bind({});

Config.args = {
  imageURL: "https://i.ytimg.com/vi/0E44DClsX5Q/maxresdefault.jpg",
  name: "selectedOrganization",
  title: "Faze",
  bodyText: "2 Teams",
  isChecked: false,
  disabled: false,
  id: "org-faze"
};
