import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck } from "@fortawesome/pro-solid-svg-icons";

import EFImage from "../../EFImage/EFImage";
import Style from "./EFSimpleDetailCheckboxCard.module.scss";

interface EFSimpleDetailCheckboxCardProps {
  id: string;
  name: string;
  title: string;
  bodyText: string;
  imageURL: string;
  isChecked: boolean;
  onChange: () => void;
  disabled: boolean;
}

const EFSimpleDetailCheckboxCard: React.FC<EFSimpleDetailCheckboxCardProps> = ({
  id,
  name,
  title,
  bodyText,
  onChange,
  isChecked,
  imageURL,
  disabled
}) => {
  return (
    <div className={Style.wrapper} aria-disabled={disabled}>
      <label className={Style.label} htmlFor={id}>
        <input
          type="checkbox"
          name={name}
          id={id}
          className={Style.checkboxInput}
          onChange={onChange}
          checked={isChecked}
          disabled={disabled}
        />
        <div className={Style.imageWrapper}>
          <EFImage className={Style.image} src={imageURL} alt="Image" />
          {isChecked && (
            <div className={Style.checkboxWrapper}>
              <span className={Style.checkMark}>
                <FontAwesomeIcon icon={faCheck} className={Style.icon} />
              </span>
              <p className={Style.selectedText}>Selected</p>
            </div>
          )}
        </div>
        <div className={Style.body}>
          <p className={Style.title}>{title}</p>
          <p className={Style.teams}>{bodyText}</p>
        </div>
      </label>
    </div>
  );
};

EFSimpleDetailCheckboxCard.defaultProps = {
  disabled: false,
  isChecked: false
};

export default EFSimpleDetailCheckboxCard;
