import React from "react";
import { withCdn } from "../../../common/utils";
import EFDetailsCheckboxCard from "./EFDetailsCheckboxCard";

export default {
  title: "Cards/EFDetailsCheckboxCard",
  component: EFDetailsCheckboxCard,
  argsTypes: {
    checkboxId: { control: { type: "text" } },
    checkboxName: { control: { type: "text" } },
    title: { control: { type: "text" } },
    line1: { control: { type: "text" } },
    line2: { control: { type: "text" } },
    line3: { control: { type: "text" } },
    isChecked: { control: { type: "boolean" } },
    disabled: { control: { type: "boolean" } },
    disableHover: { control: { type: "boolean" } },
    image: { control: { type: "text" } }
  }
};

const Story = args => (
  <div style={{ width: "280px", margin: "3rem" }}>
    <EFDetailsCheckboxCard {...args} />
  </div>
);

export const Default = Story.bind({});

Default.args = {
  title: "Event title",
  image: withCdn("/static/images/league_of_legends.jpg"),
  line1: "Round Robin",
  line2: "Warzone • Round 2",
  line3: "Oct. 8 2021 | 3:00 PM",
  isChecked: true
};
