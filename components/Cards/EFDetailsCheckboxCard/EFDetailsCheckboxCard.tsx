import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck } from "@fortawesome/pro-solid-svg-icons";
import EFCard from "../EFCard/EFCard";

import Style from "./EFDetailsCheckboxCard.module.scss";

interface EFDetailsCheckboxCardProps {
  checkboxId: string;
  checkboxName: string;
  title?: string;
  line1?: string;
  line2?: string;
  line3?: string;
  isChecked: boolean;
  image: string;
  onChange?: (event: any) => void;
  disabled?: boolean;
  disableHover?: boolean;
}

const EFDetailsCheckboxCard: React.FC<EFDetailsCheckboxCardProps> = ({
  title,
  checkboxId,
  checkboxName,
  image,
  line1,
  line2,
  line3,
  isChecked = false,
  onChange,
  disabled = false,
  disableHover = false
}) => {
  return (
    <label htmlFor={checkboxId} className={Style.label}>
      <div className={`${Style.wrapper} ${disableHover ? "" : Style.hoverEffect}`} aria-hidden="true">
        <EFCard widthTheme="fullWidth" heightTheme="fullHeight" noBorder>
          <div className={Style.content} aria-disabled={disabled}>
            <input
              type="checkbox"
              name={checkboxName}
              id={checkboxId}
              className={Style.checkboxInput}
              onChange={onChange}
              checked={isChecked}
              disabled={disabled}
            />
            <div
              className={Style.header}
              style={{
                background: `${
                  isChecked
                    ? "linear-gradient(180deg, rgba(0, 0, 0, 0.75) 0%, rgba(0, 108, 250, 0.6) 100%), linear-gradient(0deg, rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3)),"
                    : ""
                } url(${image}), #FFFFFF`,
                backgroundBlendMode: `${isChecked ? "" : "hard-light, hard-light, color, normal, normal"}`,
                backgroundSize: "cover"
              }}
            >
              {isChecked && (
                <div className={Style.checkboxWrapper}>
                  <span className={Style.checkMark}>
                    <FontAwesomeIcon icon={faCheck} className={Style.icon} />
                  </span>
                  <p className={Style.selectedText}>Selected</p>
                </div>
              )}
              {title && <div className={Style.title}> {title}</div>}
            </div>
            <div className={Style.body}>
              {line1 && <div>{line1}</div>}
              {line2 && <div>{line2}</div>}
              {line3 && <div className={Style.lightGrey}>{line3}</div>}
            </div>
          </div>
        </EFCard>
      </div>
    </label>
  );
};

export default EFDetailsCheckboxCard;
