import React from "react";
import Style from "./EFListCard.module.scss";
import EFListCardMedia from "./EFListCardMedia/EFListCardMedia";
import EFListCardMainContent from "./EFListCardMainContent/EFListCardMainContent";

interface EFListCardProps {
  backgroundImage: string;
  liveChip: boolean;
  categoryChip: string;
  title: string;
  date: Date;
  owner: string;
  backgroundImageAltText: string;
  href: string;
  timeAgo?: boolean;
  size?: string;
  onClick: () => void;
}

const EFListCard: React.FC<EFListCardProps> = ({
  backgroundImage,
  liveChip,
  categoryChip,
  title,
  date,
  owner,
  backgroundImageAltText,
  href,
  timeAgo,
  size = "large",
  onClick = () => {}
}) => {
  return (
    <div className={`${Style.listCard} ${Style[size]}`}>
      <EFListCardMedia backgroundImage={backgroundImage} altText={backgroundImageAltText} size={size} />
      <EFListCardMainContent
        liveChip={liveChip}
        categoryChip={categoryChip}
        title={title}
        date={date}
        owner={owner}
        href={href}
        timeAgo={timeAgo}
        size={size}
        onClick={onClick}
      />
    </div>
  );
};

export default EFListCard;
