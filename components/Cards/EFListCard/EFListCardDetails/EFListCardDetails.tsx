import React from "react";
import TimeAgo from "react-timeago";
import Style from "./EFListCardDetails.module.scss";
import EFRenderConditionally from "../../../EFRenderConditionally/EFRenderConditionally";
import { formatMonth } from "../../../../helpers/GeneralHelper";

interface EFListCardDetailsProps {
  date: Date;
  owner: string;
  timeAgo?: boolean;
}

const EFListCardDetails: React.FC<EFListCardDetailsProps> = ({ date, owner, timeAgo = false }) => {
  return (
    <div className={Style.details}>
      <EFRenderConditionally condition={date}>
        {timeAgo && (
          <span className={Style.owner}>
            <TimeAgo date={date} minPeriod={30} />
          </span>
        )}
        {!timeAgo && <span className={Style.startDate}>{formatMonth(date)}</span>}
      </EFRenderConditionally>
      <EFRenderConditionally condition={owner}>
        <span className={Style.owner}>{owner}</span>
      </EFRenderConditionally>
    </div>
  );
};

export default EFListCardDetails;
