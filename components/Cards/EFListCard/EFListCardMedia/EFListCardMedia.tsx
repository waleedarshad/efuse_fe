import React from "react";
import EFImage from "../../../EFImage/EFImage";
import { withCdn } from "../../../../common/utils";
import Style from "./EFListCardMedia.module.scss";

const EFListCardMedia = ({
  backgroundImage,
  altText,
  size
}: {
  backgroundImage: string;
  altText: string;
  size: string;
}) => {
  return (
    <div className={`${Style.mediaSection} ${Style[size]}`}>
      <EFImage
        className={`${Style.listCardImage} ${Style[size]}`}
        src={backgroundImage || withCdn("/static/images/no_image.jpg")}
        alt={altText}
      />
    </div>
  );
};

export default EFListCardMedia;
