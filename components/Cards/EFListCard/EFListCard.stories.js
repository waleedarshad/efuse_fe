import React from "react";
import EFListCard from "./EFListCard";

export default {
  title: "Cards/EFListCard",
  component: EFListCard,
  argsTypes: {
    backgroundImage: {
      control: {
        type: "text"
      }
    },
    liveChip: {
      control: {
        type: "boolean"
      }
    },
    categoryChip: {
      control: {
        type: "select",
        options: ["job", "event", "teamOpening", "scholarship", "erena", "live", "news"]
      }
    },
    title: {
      control: {
        type: "text"
      }
    },
    date: {
      control: {
        type: "text"
      }
    },
    owner: {
      control: {
        type: "text"
      }
    },
    backgroundImageAltText: {
      control: {
        type: "text"
      }
    },
    href: {
      control: {
        type: "text"
      }
    }
  }
};

const Story = args => (
  <div style={{ width: "280px", margin: "3rem" }}>
    <EFListCard {...args} />
  </div>
);

export const General = Story.bind({});
General.args = {
  backgroundImage: "https://cdn.efuse.gg/uploads/static/global/mindblown_white.png",
  liveChip: true,
  categoryChip: "job",
  title: "This is the Job Title",
  date: "2020-12-07T11:38:05.565Z",
  owner: "Yasi",
  backgroundImageAltText: "mind blown",
  href: "/"
};
