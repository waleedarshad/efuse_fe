import React from "react";
import Link from "next/link";
import EFResponsiveChip from "../../../EFResponsiveChip/EFResponsiveChip";
import EFListCardDetails from "../EFListCardDetails/EFListCardDetails";
import Style from "./EFListCardMainContent.module.scss";
import EFRenderConditionally from "../../../EFRenderConditionally/EFRenderConditionally";

interface EFListCardMainContentProps {
  categoryChip: string;
  liveChip: boolean;
  title: string;
  date: Date;
  owner: string;
  href: string;
  timeAgo?: boolean;
  size: string;
  onClick: () => void;
}

const EFListCardMainContent = ({
  categoryChip,
  liveChip,
  title,
  date,
  owner,
  href,
  timeAgo,
  size,
  onClick
}: EFListCardMainContentProps) => {
  return (
    <div className={`${Style.mainContent} ${Style[size]}`}>
      <div className={Style.chipSection}>
        <EFRenderConditionally condition={liveChip}>
          <div className={Style.liveChip}>
            <EFResponsiveChip badgeType="LIVE" />
          </div>
        </EFRenderConditionally>
        <EFRenderConditionally condition={categoryChip}>
          <EFResponsiveChip badgeType={categoryChip} />
        </EFRenderConditionally>
      </div>
      <Link href={href}>
        <div role="button" tabIndex={0} className={Style.title} onClick={onClick} onKeyPress={onClick}>
          {title}
        </div>
      </Link>
      <EFListCardDetails date={date} owner={owner} timeAgo={timeAgo} />
    </div>
  );
};

export default EFListCardMainContent;
