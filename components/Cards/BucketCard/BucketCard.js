import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faInfoCircle } from "@fortawesome/pro-solid-svg-icons";
import { Tooltip, OverlayTrigger } from "react-bootstrap";
import Style from "./BucketCard.module.scss";

const BucketCard = props => {
  const { image } = props;
  return (
    <div className={Style.bucketCardContainer}>
      <img src={image} className={Style.bucketBackgroundImage} />
      <OverlayTrigger
        placement="top"
        overlay={
          <Tooltip id="tooltip-top" style={{ zIndex: 9999 }}>
            test text
          </Tooltip>
        }
      >
        <FontAwesomeIcon icon={faInfoCircle} className={Style.bucketInfoIcon} />
      </OverlayTrigger>
    </div>
  );
};

export default BucketCard;
