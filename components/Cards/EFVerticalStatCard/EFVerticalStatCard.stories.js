import React from "react";
import { faBuilding } from "@fortawesome/pro-light-svg-icons";
import EFVerticalStatCard from "./EFVerticalStatCard";

export default {
  title: "Cards/EFVerticalStatCard",
  component: EFVerticalStatCard,
  argTypes: {
    icon: {
      control: {
        type: "array"
      }
    },
    imageLink: {
      control: {
        type: "text"
      }
    },
    value: {
      control: {
        type: "text"
      }
    },
    text: {
      control: {
        type: "text"
      }
    },
    internalHref: {
      control: {
        type: "text"
      }
    },
    externalHref: {
      control: {
        type: "text"
      }
    },
    buttonText: {
      control: {
        type: "text"
      }
    }
  }
};

const Story = args => (
  <div style={{ width: "200px" }}>
    {/* eslint-disable-next-line react/jsx-props-no-spreading */}
    <EFVerticalStatCard {...args} />
  </div>
);

export const CardWithIcon = Story.bind({});
CardWithIcon.args = {
  icon: faBuilding,
  value: "2021",
  text: "EFUSE",
  externalHref: "https://www.efuse.gg",
  buttonText: "Visit"
};

export const CardWithImage = Story.bind({});
CardWithImage.args = {
  imageLink: "https://efuse.s3.amazonaws.com/uploads/static/erena/efuse-icon.png",
  value: "2021",
  text: "EFUSE",
  externalHref: "https://www.efuse.gg",
  buttonText: "Visit"
};
