import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Image } from "react-bootstrap";

import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";
import EFCard from "../EFCard/EFCard";
import Style from "./EFVerticalStatCard.module.scss";

const EFVerticalStatCard = ({
  icon,
  imageLink,
  value,
  text,
  internalHref,
  buttonText,
  onClick,
  externalHref,
  widthTheme,
  buttonColorTheme,
  disableButton
}) => {
  return (
    <div className={Style.cardContainer}>
      <EFCard widthTheme={widthTheme}>
        <div className={Style.cardBody}>
          {imageLink && <Image src={imageLink} className={Style.efuseImage} />}
          {icon && <FontAwesomeIcon className={Style.cardIcon} icon={icon} />}
          <p className={Style.cardAnalytics}>{value}</p>
          <small className={Style.cardText}>{text}</small>
          <div className={Style.cardBtn}>
            <EFRectangleButton
              colorTheme={buttonColorTheme}
              shadowTheme="small"
              width="fullWidth"
              text={buttonText}
              internalHref={internalHref}
              onClick={onClick}
              externalHref={externalHref}
              disabled={disableButton}
            />
          </div>
        </div>
      </EFCard>
    </div>
  );
};

EFVerticalStatCard.defaultProps = {
  widthTheme: "fullWidth",
  buttonColorTheme: "light",
  disableButton: false
};

export default EFVerticalStatCard;
