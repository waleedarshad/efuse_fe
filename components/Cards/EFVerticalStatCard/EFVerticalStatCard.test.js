import { mount } from "enzyme";
import React from "react";
import { faBuilding } from "@fortawesome/pro-light-svg-icons";
import EFVerticalStatCard from "./EFVerticalStatCard.js";

describe("LeaderboardCard", () => {
  it("displays icon when icon is passed", () => {
    const subject = mount(
      <EFVerticalStatCard
        icon={faBuilding}
        value={22}
        text="Organizations"
        internalHref={`/organizations`}
        buttonText="Manage"
      />
    );
    expect(subject.find("svg").length).toEqual(1);
  });
  it("displays image when image link is passed", () => {
    const subject = mount(
      <EFVerticalStatCard
        imageLink="https://efuse.s3.amazonaws.com/uploads/static/erena/efuse-icon.png"
        value={22}
        text="Organizations"
        internalHref={`/organizations`}
        buttonText="Manage"
      />
    );
    expect(subject.find("img").length).toEqual(1);
  });
});
