import React from "react";
import EFProfileCard from "./EFProfileCard";

export default {
  title: "Shared/EFProfileCard",
  component: EFProfileCard,
  argsTypes: {
    title: {
      control: {
        type: "text"
      }
    },
    buttonType: {
      type: "select",
      options: ["edit", "plus"]
    },
    onButtonClick: {
      control: {
        type: "text"
      }
    }
  }
};

const Story = args => <EFProfileCard {...args} />;

export const Edit = Story.bind({});
Edit.args = {
  title: "About",
  buttonType: "edit",
  onButtonClick: "click"
};

export const Plus = Story.bind({});
Plus.args = {
  title: "About",
  buttonType: "plus",
  onButtonClick: "click"
};

export const Children = Story.bind({});
Children.args = {
  title: "About",
  buttonType: "plus",
  onButtonClick: "click",
  children: <div>hello world!</div>
};
