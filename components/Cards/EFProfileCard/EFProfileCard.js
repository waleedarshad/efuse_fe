import React from "react";
import PropTypes from "prop-types";
import { faEdit } from "@fortawesome/pro-regular-svg-icons";
import { faPlus } from "@fortawesome/pro-solid-svg-icons";
import Style from "./EFProfileCard.module.scss";
import EFCard from "../EFCard/EFCard";
import EFCircleIconButton from "../../Buttons/EFCircleIconButton/EFCircleIconButton";

const iconTypes = {
  edit: faEdit,
  plus: faPlus
};

const EFProfileCard = ({ children, title, onButtonClick, buttonType, customTitle, showButton }) => {
  return (
    <div className={Style.cardContainer}>
      <EFCard widthTheme="fullWidth">
        <div className={Style.contentContainer}>
          <p className={Style.header}>
            {customTitle || title}
            {showButton && (
              <div className={Style.buttonWrapper}>
                <EFCircleIconButton icon={iconTypes[buttonType]} onClick={onButtonClick} />
              </div>
            )}
          </p>

          {children}
        </div>
      </EFCard>
    </div>
  );
};

EFProfileCard.propTypes = {
  buttonType: PropTypes.oneOf(["edit", "plus"]),
  title: PropTypes.string,
  showButton: PropTypes.bool
};

EFProfileCard.defaultProps = {
  title: "",
  buttonType: "edit",
  showButton: false
};

export default EFProfileCard;
