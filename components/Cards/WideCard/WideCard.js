import React from "react";
import Skeleton from "react-loading-skeleton";
import Link from "next/link";
import Style from "./WideCard.module.scss";

const WideCard = ({ displayImage, title, href, name, nameRedirect, summary, imageUrl, label, displayLabel }) => {
  if (!title) {
    return (
      <div className={Style.card}>
        <div className={Style.innerBox}>
          {!displayImage && (
            <div className={Style.videoPlayer}>
              <Skeleton height={140} width={140} />
            </div>
          )}
          {displayImage && (
            <div style={{ padding: "10px" }}>
              <Skeleton height={140} width={140} />
            </div>
          )}
          <div className={Style.textBox} style={{ flex: 1 }}>
            <h3 className={Style.title}>
              <Skeleton height={40} />
            </h3>
            <p className={Style.summary}>
              <Skeleton count={5} />
            </p>
            <p className={Style.name}>
              <Skeleton />
            </p>
          </div>
        </div>
      </div>
    );
  }
  return (
    <div className={Style.card}>
      <div className={Style.innerBox}>
        {displayImage && (
          <div className={Style.imageOutsideContainer}>
            {displayLabel && <div className={Style.statusLabel}>{label}</div>}
            <img className={Style.image} src={imageUrl} alt={label} />
          </div>
        )}
        <div className={Style.textBox}>
          <h3 className={Style.title}>
            <Link href={href} className={Style.titleLink} data-cy="widecard_link">
              {title}
            </Link>
          </h3>
          <p className={Style.summary}>{summary}</p>
          <a className={Style.name} href={nameRedirect}>
            {name}
          </a>
        </div>
      </div>
    </div>
  );
};

export default WideCard;
