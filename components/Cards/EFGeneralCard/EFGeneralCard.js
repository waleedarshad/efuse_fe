import React from "react";
import TimeAgo from "react-timeago";
import Link from "next/link";
import Style from "./EFGeneralCard.module.scss";
import EFResponsiveChip from "../../EFResponsiveChip/EFResponsiveChip";
import EFImage from "../../EFImage/EFImage";

const EFGeneralCard = props => {
  const {
    badgeTheme,
    badgeText,
    image,
    title,
    subTitle,
    description,
    onImageClickLink,
    onTitleClickLink,
    onSubTitleClickLink,
    date,
    externalLink
  } = props;

  const backgroundImageContainer = (
    <div className={Style.backgroundImageContainer}>
      <EFImage className={Style.backgroundImage} src={image} alt="Background image" />
      {badgeTheme && badgeText && (
        <div className={Style.badgeWrapper}>
          <EFResponsiveChip badgeType={badgeText} />
        </div>
      )}
    </div>
  );

  const cardTitle = <h3 className={Style.cardTitle}>{title}</h3>;
  const cardSubTitle = <p className={Style.cardSubTitle}>{subTitle}</p>;

  return (
    <div className={Style.cardContainer}>
      {externalLink ? (
        <a href={onImageClickLink} target="_blank" rel="noopener noreferrer">
          {backgroundImageContainer}
        </a>
      ) : (
        <Link href={onImageClickLink}>{backgroundImageContainer}</Link>
      )}
      <div className={Style.textWrapper}>
        {externalLink ? (
          <>
            <a href={onTitleClickLink} target="_blank" rel="noopener noreferrer">
              {cardTitle}
            </a>
            <a href={onSubTitleClickLink} target="_blank" rel="noopener noreferrer">
              {cardSubTitle}
            </a>
          </>
        ) : (
          <>
            <Link href={onTitleClickLink}>{cardTitle}</Link>
            <Link href={onSubTitleClickLink}>{cardSubTitle}</Link>
          </>
        )}
        {description && <p className={Style.description}>{description}</p>}
        {date && (
          <p className={Style.date}>
            <TimeAgo date={date} />
          </p>
        )}
      </div>
    </div>
  );
};

export default EFGeneralCard;
