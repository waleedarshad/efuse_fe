import React from "react";
import PropTypes from "prop-types";
import Style from "./CardBadge.module.scss";

const CardBadge = ({ badge, largeCard, placement }) => {
  return (
    <div
      className={`${Style.type} ${Style[placement]}
        ${largeCard && Style.largeCard}
        ${badge === "Event" && Style.event}
        ${(badge === "Employment Opening" || badge === "News") && Style.job}
        ${(badge === "Scholarship" || badge === "Organizations") && Style.scholarship}
        ${badge === "Team Opening" && Style.team}
        ${badge === "eRena" && `${Style.team} ${Style.erena}`}
        ${badge === "Live" && Style.liveStreams}`}
    >
      <span>{badge}</span>
    </div>
  );
};

CardBadge.propTypes = {
  placement: PropTypes.oneOf(["topLeft", "topRight"])
};

CardBadge.defaultProps = {
  placement: "topRight"
};

export default CardBadge;
