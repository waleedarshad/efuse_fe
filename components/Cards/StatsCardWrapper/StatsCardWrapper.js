import { faUnlink } from "@fortawesome/pro-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import { useSelector } from "react-redux";
import HorizontalScroll from "../../HorizontalScroll/HorizontalScroll";
import Style from "./StatsCardWrapper.module.scss";

const StatsCardWrapper = ({ children, onUnlinkClick, unlinkTooltipMessage }) => {
  const loggedInUserId = useSelector(state => state.auth.currentUser?._id);
  const portfolioUserId = useSelector(state => state.user.currentUser?._id);

  const isOwner = portfolioUserId && loggedInUserId === portfolioUserId;

  return (
    <>
      {isOwner && (
        <FontAwesomeIcon
          className={Style.unlink}
          icon={faUnlink}
          title={unlinkTooltipMessage}
          onClick={onUnlinkClick}
        />
      )}
      <div className={Style.rootContainer}>
        <HorizontalScroll>
          <div className={Style.statDetails}>{children}</div>
        </HorizontalScroll>
      </div>
    </>
  );
};

export default StatsCardWrapper;
