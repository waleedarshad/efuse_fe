import React from "react";
import { Col, Row } from "react-bootstrap";
import PropTypes from "prop-types";
import { faMapMarkerAlt } from "@fortawesome/pro-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Style from "./EFApplicantCard.module.scss";
import EFAvatar from "../../EFAvatar/EFAvatar";

const EFApplicantCard = ({
  titleImageUrl,
  titleImageHref,
  headerText,
  subHeaderText,
  headerTags,
  headerActionButtons,
  body,
  footerActionButtons
}) => {
  return (
    <div className={Style.cardContainer}>
      <div className={Style.contentContainer}>
        <Row className={Style.topSection}>
          <Col xs={2} md={1} className={Style.avatarWrapper}>
            <EFAvatar displayOnlineButton={false} profilePicture={titleImageUrl} size="small" href={titleImageHref} />
          </Col>
          <Col xs={10} md={11} className={Style.contentWrapper}>
            <div className={Style.content}>
              <div>
                <span className={Style.headerText}>{headerText}</span>
                <span className={Style.subHeaderText}>{subHeaderText}</span>
                {headerTags.length && (
                  <div className={Style.headerTags}>
                    <FontAwesomeIcon icon={faMapMarkerAlt} className={Style.tag} />
                    {headerTags.map((tag, index) => {
                      return (
                        <>
                          <span key={index} className={Style.tag}>
                            {tag}
                          </span>
                          {index < headerTags.length - 1 && (
                            <span className={`${Style.tag} ${Style.middleDot}`}>&#183;</span>
                          )}
                        </>
                      );
                    })}
                  </div>
                )}
              </div>
            </div>
            {headerActionButtons}
          </Col>
        </Row>
        <Row className={Style.footer}>
          <Col md={5} className={Style.body}>
            {body}
          </Col>
          <Col md={7} className={Style.footerActionButtons}>
            {footerActionButtons}
          </Col>
        </Row>
      </div>
    </div>
  );
};

EFApplicantCard.propTypes = {
  titleImageUrl: PropTypes.string,
  titleImageHref: PropTypes.string,
  headerText: PropTypes.string,
  subHeaderText: PropTypes.string,
  headerTags: PropTypes.array,
  headerActionButtons: PropTypes.element,
  body: PropTypes.element,
  footerActionButtons: PropTypes.element
};

export default EFApplicantCard;
