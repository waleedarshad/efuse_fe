import React from "react";
import { useQuery } from "@apollo/client";
import Link from "next/link";
import GET_ARTICLE_CATEGORIES from "../../../graphql/NewsArticlesCategoryQuery";
import EFCard from "../EFCard/EFCard";
import Style from "./EFQuickLinksWidget.module.scss";

const EFQuickLinksWidget = () => {
  const { data } = useQuery(GET_ARTICLE_CATEGORIES);
  const categories = data?.getCategories || [];

  return (
    <EFCard widthTheme="fullWidth">
      <div className={Style.widgetCard}>
        <div className={Style.heading}>Quick Links</div>
        <div>
          <Link href="/opportunities/browse?subType=Casual&subType=Amateur&subType=Club&subType=Collegiate&subType=Semi-Pro&subType=Pro">
            <div className={Style.row}>
              <div className={Style.categoryName}>Team Openings</div>
            </div>
          </Link>
          {categories.map(category => (
            <Link key={category._id} href={`/news/${category.slug}`}>
              <div className={Style.row}>
                <div className={Style.categoryName}>{category.name}</div>
              </div>
            </Link>
          ))}
        </div>
      </div>
    </EFCard>
  );
};

export default EFQuickLinksWidget;
