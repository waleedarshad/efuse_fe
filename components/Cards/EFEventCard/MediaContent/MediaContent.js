import React from "react";
import Link from "next/link";

import ReactPlayerComponent from "../../../ReactPlayerComponent/ReactPlayerComponent";
import Style from "./MediaContent.module.scss";
import EFImage from "../../../EFImage/EFImage";
import EFResponsiveChip from "../../../EFResponsiveChip/EFResponsiveChip";
import EFRenderConditionally from "../../../EFRenderConditionally/EFRenderConditionally";

const MediaContent = ({ videoUrl, imageUrl, badge, externalLink, mediaHref, onClick, isLive }) => {
  const content = (
    <div className={Style.mediaContainer}>
      {videoUrl && <ReactPlayerComponent url={videoUrl} controls autoplay muted playing />}
      {imageUrl && <EFImage className={Style.image} src={imageUrl} alt="eRena event image" />}
      <div className={Style.chipContainer}>
        <EFRenderConditionally condition={isLive}>
          <div>
            <EFResponsiveChip badgeType="LIVE" size="small" />
          </div>
        </EFRenderConditionally>
        <div>
          <EFResponsiveChip badgeType={badge} size="small" />
        </div>
      </div>
    </div>
  );

  return (
    <>
      {externalLink ? (
        <a href={mediaHref} target="_blank" rel="noopener noreferrer">
          {content}
        </a>
      ) : (
        <Link href={mediaHref}>
          <div role="button" tabIndex={0} onClick={onClick} onKeyPress={onClick}>
            {content}
          </div>
        </Link>
      )}
    </>
  );
};

export default MediaContent;
