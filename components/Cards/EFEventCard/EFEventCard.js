import React from "react";
import TimeAgo from "react-timeago";
import Link from "next/link";
import moment from "moment";
import { faEllipsisV } from "@fortawesome/pro-regular-svg-icons";

import EFCard from "../EFCard/EFCard";
import UserImageList from "../../Opportunities/OpportunityLanding/UserImageList/UserImageList";
import EFCircleIconButton from "../../Buttons/EFCircleIconButton/EFCircleIconButton";
import FEATURE_FLAGS from "../../../common/featureFlags";
import FeatureFlag from "../../General/FeatureFlag/FeatureFlag";
import FeatureFlagVariant from "../../General/FeatureFlag/FeatureFlagVariant/FeatureFlagVariant";
import Style from "./EFEventCard.module.scss";
import MediaContent from "./MediaContent/MediaContent";
import { EFHtmlParser } from "../../EFHtmlParser/EFHtmlParser";

const EFEventCard = ({
  badge,
  videoUrl,
  imageUrl,
  title,
  subTitle,
  date,
  userImages,
  isLive,
  titleHref,
  subTitleHref,
  mediaHref,
  externalLink,
  slug,
  description,
  game,
  bracketType
}) => {
  let bracketTypeDisplay;
  if (bracketType === "pointRace") {
    bracketTypeDisplay = "Leaderboard";
  } else {
    bracketTypeDisplay = bracketType;
  }

  const displayDate = date ? moment(date).format("MMM D") : null;

  const onClick = () => {
    analytics.track("ERENA_EVENT_CLICKED", {
      title,
      slug
    });
  };

  return (
    <EFCard>
      <div className={Style.eventCard}>
        <MediaContent
          videoUrl={videoUrl}
          imageUrl={imageUrl}
          isLive={isLive}
          badge={badge}
          mediaHref={mediaHref}
          externalLink={externalLink}
          slug={slug}
        />
        <div className={Style.contentContainer}>
          <Link href={titleHref}>
            <div
              role="button"
              tabIndex={0}
              className={`${Style.titleWrapper} ${Style.link}`}
              onClick={onClick}
              onKeyPress={onClick}
            >
              <p className={Style.title}>{title}</p>
              <FeatureFlag name={FEATURE_FLAGS.TEMP_EVENT_CARD_KEBAB_BUTTON}>
                <FeatureFlagVariant flagState>
                  <div className={Style.dropdown}>
                    <EFCircleIconButton icon={faEllipsisV} shadowTheme="none" colorTheme="transparent" />
                  </div>
                </FeatureFlagVariant>
              </FeatureFlag>
            </div>
          </Link>

          {subTitle && (
            <Link href={subTitleHref}>
              <p className={Style.subTitle}>{subTitle}</p>
            </Link>
          )}

          <div className={Style.subPointsOfInterest}>
            {displayDate && <span className={Style.date}>{displayDate}</span>}
            {date && game && <span className={Style.bullet}>•</span>}
            {game && <span>{game}</span>}
            {(date || game) && bracketType && <span className={Style.bullet}>•</span>}
            {bracketType && <span>{bracketTypeDisplay}</span>}
          </div>
          <div className={Style.description}>
            <EFHtmlParser>{description || ""}</EFHtmlParser>
          </div>

          {date && (
            <p className={Style.date}>
              <TimeAgo date={date} />
            </p>
          )}
          {userImages?.length && (
            <div className={Style.usersList}>
              <UserImageList
                imageList={userImages.map((user, index) => {
                  if (index < 4) {
                    return user?.profilePicture?.url;
                  }
                  return <></>;
                })}
              />
            </div>
          )}
        </div>
      </div>
    </EFCard>
  );
};

export default EFEventCard;
