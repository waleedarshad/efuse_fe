import React from "react";
import EFSelectCard from "./EFSelectCard";

export default {
  title: "Inputs/EFSelectCard",
  component: EFSelectCard,
  argTypes: {
    isChecked: {
      control: {
        type: "boolean"
      }
    }
  }
};

// eslint-disable-next-line react/jsx-props-no-spreading
const Story = args => <EFSelectCard {...args} />;

export const Checked = Story.bind({});

Checked.args = {
  isChecked: true,
  title: "Swiss",
  text:
    "A Swiss-system tournament is a non-eliminating tournament format that features a fixed number of rounds of competition, but considerably fewer than for a round-robin tournament; thus each competitor (team or individual) does not play all the other competitors."
};

export const Unchecked = Story.bind({});

Unchecked.args = {
  isChecked: false,
  title: "Swiss",
  text:
    "A Swiss-system tournament is a non-eliminating tournament format that features a fixed number of rounds of competition, but considerably fewer than for a round-robin tournament; thus each competitor (team or individual) does not play all the other competitors."
};
