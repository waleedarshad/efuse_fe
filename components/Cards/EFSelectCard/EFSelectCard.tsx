import React from "react";
import EFCard from "../EFCard/EFCard";
import Style from "./EFSelectCard.module.scss";

interface EFSelectCardProps {
  title: string;
  text: string;
  onCardSelectedChange: (isCardSelected: boolean) => void;
  isCardSelected: boolean;
  isDisabled?: boolean;
}

const EFSelectCard = ({
  title,
  text,
  onCardSelectedChange = () => {},
  isCardSelected,
  isDisabled = false
}: EFSelectCardProps) => {
  return (
    <EFCard
      widthTheme="fullWidth"
      isDisabled={isDisabled}
      isSelected={isCardSelected}
      onClick={() => onCardSelectedChange(!isCardSelected)}
    >
      <div className={`${Style.card} ${isCardSelected ? Style.checkedBackground : Style.unCheckedBackground}`}>
        <div className={`${Style.title} ${isCardSelected ? Style.checkedTextColor : Style.unCheckedTextColor}`}>
          {title}
        </div>
        <div className={`${Style.text} ${isCardSelected ? Style.checkedTextColor : Style.unCheckedTextColor}`}>
          {text}
        </div>
      </div>
    </EFCard>
  );
};

export default EFSelectCard;
