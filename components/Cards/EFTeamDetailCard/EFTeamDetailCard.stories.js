import React from "react";
import EFTeamDetailCard from "./EFTeamDetailCard";

export default {
  title: "Cards/EFTeamDetailCard",
  component: EFTeamDetailCard,
  argTypes: {
    imageUrl: {
      control: {
        type: "text"
      }
    },
    teamName: {
      control: {
        type: "text"
      }
    },
    nbTeamMembers: {
      control: {
        type: "number"
      }
    },
    teamMembers: {
      control: {
        type: "array"
      }
    },
    addMembers: {
      control: {
        type: "function"
      }
    },
    manage: {
      control: {
        type: "function"
      }
    },
    isAdmin: {
      control: {
        type: "boolean"
      }
    },
    isChecked: {
      control: {
        type: "boolean"
      }
    },
    showMemberViewButton: {
      control: {
        type: "boolean"
      }
    },
    disabled: {
      control: {
        type: "boolean"
      }
    },
    showRemoveButton: {
      control: {
        type: "boolean"
      }
    }
  }
};

const Story = args => (
  <div className="p-4">
    {/* eslint-disable-next-line react/jsx-props-no-spreading */}
    <EFTeamDetailCard {...args} />
  </div>
);

const teamData = {
  imageUrl: "https://i.ytimg.com/vi/0E44DClsX5Q/maxresdefault.jpg",
  teamName: "Logitech",
  nbTeamMembers: 4,
  teamMembers: [
    {
      user: {
        profilePicture: {
          url: "https://staging-cdn.efuse.gg/uploads/users/1625567259454-pipeline-background.jpg"
        },
        name: "Saud",
        username: "saud"
      }
    },
    {
      user: {
        profilePicture: {
          url:
            "https://scontent.efcdn.io/uploads/users/1554374592876-11392926_915755788485762_4748794089000750308_n.jpg"
        },
        name: "Fakhir",
        username: "vantsome"
      }
    },
    {
      user: {
        profilePicture: {
          url: "https://staging-cdn.efuse.gg/uploads/users/1617984471725-Overwatch-285x380.jpg"
        },
        name: "Pavel",
        username: "spavel"
      }
    },
    {
      user: {
        profilePicture: {
          url: "https://staging-cdn.efuse.gg/uploads/users/1627563634341-profileImage"
        },
        name: "Faizoo",
        username: "faisal"
      }
    }
  ]
};

export const AdminCard = Story.bind({});
AdminCard.args = {
  ...teamData,
  addMembers: () => {},
  manage: () => {},
  isAdmin: true,
  showMemberViewButton: true
};

export const NonAdminCard = Story.bind({});
NonAdminCard.args = {
  ...teamData,
  addMembers: () => {},
  manage: () => {},
  isAdmin: false,
  showMemberViewButton: true
};

export const CardWithRemoveButton = Story.bind({});
CardWithRemoveButton.args = {
  ...teamData,
  addMembers: () => {},
  manage: () => {},
  isAdmin: false,
  showMemberViewButton: false,
  showRemoveButton: true,
  remove: () => {}
};

export const CheckboxTeamCard = Story.bind({});
CheckboxTeamCard.args = {
  ...teamData,
  addMembers: () => {},
  manage: () => {},
  isAdmin: false,
  showMemberViewButton: false,
  isChecked: true
};

export const DisabledTeamCard = Story.bind({});
DisabledTeamCard.args = {
  ...teamData,
  addMembers: () => {},
  manage: () => {},
  isAdmin: false,
  showMemberViewButton: false,
  isChecked: false,
  disabled: true
};
