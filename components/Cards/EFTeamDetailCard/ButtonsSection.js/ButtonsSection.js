import React from "react";

import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";
import Style from "./ButtonsSection.module.scss";

const ButtonsSection = ({ addMembers, manage }) => {
  return (
    <div className={Style.buttonsSection}>
      <div className={Style.buttonWrapper}>
        <EFRectangleButton text="Add Members" width="fullWidth" shadowTheme="none" onClick={addMembers} />
      </div>

      <div className={Style.buttonWrapper}>
        <EFRectangleButton text="Manage" colorTheme="light" width="fullWidth" shadowTheme="none" onClick={manage} />
      </div>
    </div>
  );
};

export default ButtonsSection;
