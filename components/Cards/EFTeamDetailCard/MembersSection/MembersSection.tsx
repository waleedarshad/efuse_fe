import React from "react";

import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";
import EFUserBadge from "../../../EFUserBadge/EFUserBadge";
import Style from "./MembersSection.module.scss";

interface MembersSectionProps {
  teamMembers: [{ profilePictureUrl: string }];
}

const MembersSection = ({ teamMembers, nbTeamMembers, showMemberViewButton }: MembersSectionProps) => {
  return (
    <div className={Style.membersSection}>
      {teamMembers.map((member, index) => {
        return (
          <>
            <div className={Style.member}>
              <EFUserBadge
                profilePictureUrl={member.user.profilePicture.url}
                fullname={member.name}
                displayOnlineButton={false}
              />
              {showMemberViewButton && (
                <EFRectangleButton
                  text="View"
                  shadowTheme="none"
                  internalHref={`/u/${member.username}`}
                  colorTheme="transparent"
                  fontWeightTheme="normal"
                />
              )}
            </div>
            {nbTeamMembers - 1 !== index && <hr className={Style.breakLine} />}
          </>
        );
      })}
    </div>
  );
};

export default MembersSection;
