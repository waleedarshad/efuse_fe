import React from "react";
import { faTrashAlt } from "@fortawesome/pro-light-svg-icons";

import EFImage from "../../../EFImage/EFImage";
import EFCircleIconButton from "../../../Buttons/EFCircleIconButton/EFCircleIconButton";
import Style from "./TopSection.module.scss";

const TopSection = ({ imageUrl, teamName, nbTeamMembers, showRemoveButton, remove }) => {
  return (
    <div className={Style.topSection}>
      <EFImage width={50} height={50} src={imageUrl} className={Style.image} />
      <p className={Style.nameText}>{teamName}</p>
      {showRemoveButton ? (
        <div className={Style.removeButton}>
          <EFCircleIconButton shadowTheme="none" colorTheme="transparent" icon={faTrashAlt} onClick={remove} />
          <span className={Style.removeText}>Remove</span>
        </div>
      ) : (
        <p className={Style.membersText}>{nbTeamMembers} members</p>
      )}
    </div>
  );
};

export default TopSection;
