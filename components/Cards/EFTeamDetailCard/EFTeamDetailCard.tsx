import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck } from "@fortawesome/pro-light-svg-icons";

import EFCard from "../EFCard/EFCard";
import ButtonsSection from "./ButtonsSection.js/ButtonsSection";
import MembersSection from "./MembersSection/MembersSection";
import TopSection from "./TopSection/TopSection";
import Style from "./EFTeamDetailCard.module.scss";
import EFCheckboxWrapper from "../../EFCheckboxWrapper/EFCheckboxWrapper";

interface EFTeamDetailCardProps {
  id: string;
  imageUrl: string;
  teamName: string;
  nbTeamMembers: number;
  teamMembers: any;
  name: string;
  isChecked: boolean;
  onSelect: () => void;
  addMembers: () => void;
  manage: () => void;
  isAdmin: boolean;
  showMemberViewButton: boolean;
  disabled: boolean;
  showRemoveButton: boolean;
  remove: () => void;
}

const EFTeamDetailCard: React.FC<EFTeamDetailCardProps> = ({
  id,
  imageUrl,
  teamName,
  nbTeamMembers,
  teamMembers,
  addMembers,
  manage,
  isAdmin,
  name,
  onSelect,
  isChecked,
  showMemberViewButton,
  disabled,
  showRemoveButton,
  remove
}) => {
  return (
    <EFCheckboxWrapper id={id} onSelect={onSelect} isChecked={isChecked} disabled={disabled} name={name}>
      <div className={Style.cardWrapper}>
        <EFCard overflowTheme="visible" shadow="none">
          <div className={`${Style.cardContent}`}>
            <TopSection
              imageUrl={imageUrl}
              teamName={teamName}
              nbTeamMembers={nbTeamMembers}
              showRemoveButton={showRemoveButton}
              remove={remove}
            />
            <div className="p-3">
              {nbTeamMembers > 0 ? (
                <MembersSection
                  teamMembers={teamMembers}
                  nbTeamMembers={nbTeamMembers}
                  showMemberViewButton={showMemberViewButton}
                />
              ) : (
                <div className={Style.noMembersTextWrapper}>
                  <p className={Style.noMembersText}>
                    Currently no members are part of this team. Invite members to join
                  </p>
                </div>
              )}
              {isAdmin && <ButtonsSection addMembers={addMembers} manage={manage} />}
            </div>
          </div>
        </EFCard>
        {isChecked && (
          <div className={Style.selectedContentWrapper}>
            <div className={Style.selectedContent}>
              <span className={Style.iconWrapper}>
                <FontAwesomeIcon className={Style.icon} icon={faCheck} />
              </span>
              <div className={Style.selectedText}>Selected</div>
            </div>
          </div>
        )}
      </div>
    </EFCheckboxWrapper>
  );
};

EFTeamDetailCard.defaultProps = {
  name: "teamDetailCard",
  isAdmin: false,
  showMemberViewButton: true,
  isChecked: false,
  disabled: false
};

export default EFTeamDetailCard;
