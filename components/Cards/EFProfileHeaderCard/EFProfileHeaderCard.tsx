import React from "react";
import moment from "moment";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCalendarAlt } from "@fortawesome/pro-solid-svg-icons";
import Style from "./EFProfileHeaderCard.module.scss";
import EFCard from "../EFCard/EFCard";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";
import { withCdn } from "../../../common/utils";
import EFInfoPill from "../../EFInfoPill/EFInfoPill";

interface EFProfileHeaderCardProps {
  backgroundImage: string;
  title: string;
  date: string | Date;
  teams: string | number;
  leagues: string | number;
  organizations?: string | number;
  profileImage: string;
  description: string;
  buttonComponent?: React.ReactNode;
  primaryBtnText: string;
  secondaryBtnText: string;
  onPrimaryBtnClick: () => void;
  onSecondaryBtnClick: () => void;
  showButtons?: boolean;
}

const EFProfileHeaderCard: React.FC<EFProfileHeaderCardProps> = ({
  backgroundImage,
  title,
  date,
  teams,
  leagues,
  organizations,
  profileImage,
  description,
  buttonComponent,
  primaryBtnText,
  secondaryBtnText,
  onPrimaryBtnClick = () => {},
  onSecondaryBtnClick = () => {},
  showButtons = false
}) => {
  return (
    <EFCard heightTheme="fullHeight" widthTheme="fullWidth" shadow="none">
      <div className={Style.contentWrapper}>
        <div
          className={Style.backgroundImageContainer}
          style={{
            backgroundImage: `url(${backgroundImage})`
          }}
        />
        <div className={Style.detailsContainer}>
          <div className={Style.detailsList}>
            <div className={Style.organizerBar}>
              <img
                alt=""
                className={Style.organizerThumbnail}
                src={profileImage ?? withCdn("/static/images/mindblown.png")}
              />
            </div>
          </div>

          <div className={Style.detailsList}>
            <div className={Style.infoPills}>
              {<EFInfoPill>{leagues} Leagues</EFInfoPill>}
              {<EFInfoPill>{teams} Teams</EFInfoPill>}
              {organizations && <EFInfoPill>{organizations} Organizations</EFInfoPill>}
            </div>
            <div className={`${Style.cardHeading}`}>{title}</div>
            {date && (
              <div className={Style.flexBar}>
                <FontAwesomeIcon icon={faCalendarAlt} className={Style.icon} />
                <div className={Style.date}>Created {moment(date).format("MMMM D, YYYY")}</div>
              </div>
            )}
          </div>

          <div className={Style.buttonContainer}>
            {showButtons &&
              (buttonComponent || (
                <div className={Style.buttonWrapper}>
                  <EFRectangleButton
                    colorTheme="light"
                    className={`${Style.button} ${Style.secondaryBtn}`}
                    onClick={onSecondaryBtnClick}
                    shadowTheme="none"
                    text={<span>{secondaryBtnText}</span>}
                    fontWeightTheme="bold"
                  />

                  <EFRectangleButton
                    className={`${Style.button} ${Style.primaryBtn}`}
                    onClick={onPrimaryBtnClick}
                    shadowTheme="none"
                    text={<span>{primaryBtnText}</span>}
                  />
                </div>
              ))}
          </div>
        </div>
        <div className={Style.detailsContainer}>
          <div className={Style.description}>{description}</div>
        </div>
      </div>
    </EFCard>
  );
};

export default EFProfileHeaderCard;
