import React from "react";
import EFProfileHeaderCard from "./EFProfileHeaderCard";

export default {
  title: "Cards/EFProfileHeaderCard",
  component: EFProfileHeaderCard,
  argsTypes: {
    backgroundImage: {
      control: {
        type: "text"
      }
    },
    title: {
      control: {
        type: "text"
      }
    },
    date: {
      control: {
        type: "text"
      }
    },
    teams: {
      control: {
        type: "text"
      }
    },
    leagues: {
      control: {
        type: "text"
      }
    },
    organizations: {
      control: {
        type: "text"
      }
    },
    profileImage: {
      control: {
        type: "text"
      }
    },
    description: {
      control: {
        type: "text"
      }
    },
    primaryBtnText: {
      control: {
        type: "text"
      }
    },
    secondaryBtnText: {
      control: {
        type: "text"
      }
    }
  }
};

const Story = args => <EFProfileHeaderCard {...args} />;
export const General = Story.bind({});
General.args = {
  backgroundImage: "https://wallpapercave.com/wp/wp2928790.jpg",
  title: "TEXSEF",
  date: "2020-12-07T11:38:05.565Z",
  profileImage: "https://i.ytimg.com/vi/0E44DClsX5Q/maxresdefault.jpg",
  teams: "100",
  leagues: "10",
  organizations: "60",
  secondaryBtnText: "Invite",
  primaryBtnText: "Create League",
  description:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Tortor duis ridiculus duis ultricies amet, vulputate urna tellus nunc. Viverra mauris sit neque, congue phasellus. Convallis convallis nunc, venenatis, nulla ante mi. Tortor duis ridiculus duis ultricies amet, vulputate urna tellus nunc. Viverra mauris sit neque, congue phasellus. more"
};
