import React from "react";
import { withCdn } from "../../../common/utils";
import EFLeagueEventCard from "./EFLeagueEventCard";

export default {
  title: "Cards/EFLeagueEventCard",
  component: EFLeagueEventCard,
  argsTypes: {
    image: {
      control: {
        type: "text"
      }
    },
    title: {
      control: {
        type: "text"
      }
    },
    bracketType: {
      control: {
        type: "text"
      }
    },
    numberOfTeams: {
      control: {
        type: "number"
      }
    },
    date: {
      control: {
        type: "Date"
      }
    }
  }
};

const Story = args => <EFLeagueEventCard {...args} />;

export const Default = Story.bind({});

Default.args = {
  image: withCdn("/static/images/league_of_legends.jpg"),
  title: "Event title",
  bracketType: "roundRobin",
  numberOfTeams: "8",
  date: ""
};
