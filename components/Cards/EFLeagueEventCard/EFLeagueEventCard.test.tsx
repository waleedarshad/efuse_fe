import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom";
import EFLeagueEventCard from "./EFLeagueEventCard";

describe("EFLeagueEventCard", () => {
  it("render the component with correct title of card", () => {
    render(
      <EFLeagueEventCard
        image="img/url"
        title="Title Of Card"
        bracketType="roundRobin"
        numberOfTeams={8}
        date={new Date("1990-01-01")}
      />
    );
    expect(screen.getByText("Title Of Card"));
  });

  it("renders the game image on card when image url is passed to component", () => {
    render(
      <EFLeagueEventCard
        image="img/url"
        title="Title Of Card"
        bracketType="roundRobin"
        numberOfTeams={8}
        date={new Date("1990-01-01")}
      />
    );

    expect(screen.getByAltText("ef-square-img")).toBeInTheDocument();
  });

  it("renders the correct bracketTypes for league event card for all EVENT_BRACKET_TYPE_MAP values", () => {
    const { rerender } = render(
      <EFLeagueEventCard
        image="img/url"
        title="Title Of Card"
        bracketType="roundRobin"
        numberOfTeams={8}
        date={new Date("1990-01-01")}
      />
    );
    expect(screen.getByText("Round Robin • 8 Teams"));

    rerender(
      <EFLeagueEventCard
        image="img/url"
        title="Title Of Card"
        bracketType="singleElim"
        numberOfTeams={8}
        date={new Date("1990-01-01")}
      />
    );
    expect(screen.getByText("Single Elimination • 8 Teams"));

    rerender(
      <EFLeagueEventCard
        image="img/url"
        title="Title Of Card"
        bracketType="doubleElim"
        numberOfTeams={8}
        date={new Date("1990-01-01")}
      />
    );
    expect(screen.getByText("Double Elimination • 8 Teams"));

    rerender(
      <EFLeagueEventCard
        image="img/url"
        title="Title Of Card"
        bracketType="swiss"
        numberOfTeams={8}
        date={new Date("1990-01-01")}
      />
    );
    expect(screen.getByText("Swiss • 8 Teams"));

    rerender(
      <EFLeagueEventCard
        image="img/url"
        title="Title Of Card"
        bracketType="pointRace"
        numberOfTeams={8}
        date={new Date("1990-01-01")}
      />
    );
    expect(screen.getByText("Point Race • 8 Teams"));
  });

  it("renders the delete modal when user clicks delete icon", () => {
    render(
      <EFLeagueEventCard
        image="img/url"
        title="Title Of Card"
        bracketType="roundRobin"
        numberOfTeams={8}
        date={new Date("1990-01-01")}
      />
    );

    const deleteIcon = screen.getByTitle("delete-icon");

    fireEvent.click(deleteIcon);

    expect(screen.getByText("Delete this event?")).toBeInTheDocument();
    expect(screen.getByText("Yes")).toBeInTheDocument();
    expect(screen.getByText("No")).toBeInTheDocument();
  });
});
