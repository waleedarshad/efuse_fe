import React from "react";
import { faTrashAlt } from "@fortawesome/pro-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styled from "styled-components";
import EFCard from "../EFCard/EFCard";
import EFAlert from "../../EFAlert/EFAlert";
import colors from "../../../styles/sharedStyledComponents/colors";
import EFSquareImage from "../../EFSquareImage/EFSquareImage";
import { EVENT_BRACKET_TYPE_MAP } from "../../Leagues/constants";
import { TruncateWithEllipsis } from "../../../styles/sharedStyledComponents/sharedStyles";

interface EFLeagueEventCardProps {
  image: string;
  title: string;
  bracketType: string;
  numberOfTeams: number;
  date: Date;
  onClick?: (event: any) => void;
  onDelete?: (event: any) => void;
}

const EFLeagueEventCard: React.FC<EFLeagueEventCardProps> = ({
  image,
  title,
  bracketType,
  numberOfTeams,
  date,
  onClick,
  onDelete
}) => {
  const formattedDate = `${new Intl.DateTimeFormat("en-US", {
    month: "short",
    day: "2-digit",
    year: "numeric"
  }).format(new Date(date))} | ${new Intl.DateTimeFormat("en-US", {
    hour: "numeric",
    minute: "numeric"
  }).format(new Date(date))}`;

  const onDeleteClick = event => {
    event.stopPropagation();

    const modalTitle = "Delete this event?";
    const message = "Are you sure?";
    const confirmLabel = "Yes";
    const denyLabel = "No";

    EFAlert(modalTitle, message, confirmLabel, denyLabel, onDelete);
  };

  return (
    <CardWrapper>
      <EFCard widthTheme="fullWidth" heightTheme="fullHeight" onClick={onClick} hoverEffect="default" shadow="none">
        <EventCardBody>
          <EventCardHeader>
            <EFSquareImage alt="ef-square-img" src={image} size={40} />
            <Title>{title}</Title>
          </EventCardHeader>
          <DeleteIcon icon={faTrashAlt} title="delete-icon" onClick={onDeleteClick} />
          <DetailsContainer>
            <MainDetails>{`${EVENT_BRACKET_TYPE_MAP[bracketType]} • ${numberOfTeams} Teams`}</MainDetails>
            <SecondaryDetails>{formattedDate}</SecondaryDetails>
          </DetailsContainer>
        </EventCardBody>
      </EFCard>
    </CardWrapper>
  );
};

const CardWrapper = styled.div`
  width: 271px;
  height: 118px;
`;

const EventCardBody = styled.div`
  height: 100%;
  padding: 10px 20px;
  font-family: Poppins, sans-serif;
  font-weight: 500;
  font-size: 0.875rem;
  font-style: normal;
  line-height: 20px;
  color: ${colors.textStrong};
  position: relative;
`;

const EventCardHeader = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
`;

const Title = styled.span`
  ${TruncateWithEllipsis};
  width: 150px;
  margin-left: 10px;
  font-family: Poppins, sans-serif;
  font-size: 16px;
  font-style: normal;
  font-weight: 500;
  line-height: 22px;
  letter-spacing: 0;
  text-align: left;
`;

const DeleteIcon = styled(FontAwesomeIcon)`
  position: absolute;
  top: 12px;
  right: 14px;
  color: ${colors.textWeak};

  &:hover {
    cursor: pointer;
    color: ${colors.alertRed} !important;
  }
`;

const DetailsContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

const MainDetails = styled.span`
  margin-top: 10px;
  margin-bottom: 0;
`;

const SecondaryDetails = styled.span`
  margin-top: 5px;
  margin-bottom: 0;
  color: ${colors.textWeak};
`;

export default EFLeagueEventCard;
