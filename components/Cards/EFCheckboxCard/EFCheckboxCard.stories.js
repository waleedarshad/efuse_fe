import React from "react";
import EFCheckboxCard from "./EFCheckboxCard";

export default {
  title: "Cards/EFCheckboxCard",
  component: EFCheckboxCard,
  argTypes: {
    imageUrl: {
      control: {
        type: "text"
      }
    },
    disabled: {
      control: {
        type: "boolean"
      }
    },
    selected: {
      control: {
        type: "boolean"
      }
    },
    onchange: {
      control: {
        type: "function"
      }
    },
    size: {
      control: {
        type: "text"
      }
    }
  }
};

// eslint-disable-next-line react/jsx-props-no-spreading
const Story = args => (
  <div className="m-3">
    <EFCheckboxCard {...args} />
  </div>
);

export const DefaultSmallCard = Story.bind({});
DefaultSmallCard.args = {
  imageUrl: "https://efuse.s3.amazonaws.com/uploads/games/d9a5432c-5ae7-4d9e-b833-2dce3b1ac4ef.jpg",
  onchange: () => {},
  size: "small"
};
export const DefaultMediumCard = Story.bind({});
DefaultMediumCard.args = {
  imageUrl: "https://efuse.s3.amazonaws.com/uploads/games/d9a5432c-5ae7-4d9e-b833-2dce3b1ac4ef.jpg",
  onchange: () => {},
  size: "medium"
};

export const Selected = Story.bind({});
Selected.args = {
  imageUrl: "https://efuse.s3.amazonaws.com/uploads/games/d9a5432c-5ae7-4d9e-b833-2dce3b1ac4ef.jpg",
  selected: true,
  onchange: () => {},
  size: "small"
};

export const Disabled = Story.bind({});
Disabled.args = {
  imageUrl: "https://efuse.s3.amazonaws.com/uploads/games/d9a5432c-5ae7-4d9e-b833-2dce3b1ac4ef.jpg",
  disabled: true,
  onchange: () => {},
  size: "small"
};
