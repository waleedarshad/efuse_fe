import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck } from "@fortawesome/pro-light-svg-icons";

import Style from "./EFCheckboxCard.module.scss";

interface EFCheckboxCardProps {
  imageUrl: string;
  disabled: boolean;
  selected: boolean;
  onChange: () => void;
  id: string;
  size: string;
}

const EFCheckboxCard: React.FC<EFCheckboxCardProps> = ({ imageUrl, disabled, selected, onChange, id, size }) => {
  return (
    <label
      htmlFor={id}
      className={`${Style.checkboxCard} ${disabled && Style.disabled} ${selected && Style.selected} ${Style[size]} `}
      style={{
        background: selected
          ? ` linear-gradient(180deg, rgba(0, 0, 0, 0.75) 0%, rgba(0, 108, 250, 0.6) 100%), linear-gradient(0deg, rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3)), url(${imageUrl})`
          : `url(${imageUrl})`
      }}
    >
      <div className={`form-check ${Style.checkbox}`}>
        <input
          className="form-check-input d-none"
          type="checkbox"
          name="checkboxCard"
          checked={selected}
          id={id}
          disabled={disabled}
          onChange={onChange}
        />
        {selected && (
          <>
            <span className={Style.iconWrapper}>
              <FontAwesomeIcon className={Style.icon} icon={faCheck} />
            </span>
            <div className={Style.label}>Selected</div>
          </>
        )}
      </div>
    </label>
  );
};

EFCheckboxCard.defaultProps = {
  disabled: false,
  selected: false,
  size: "small"
};

export default EFCheckboxCard;
