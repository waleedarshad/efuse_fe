import React from "react";

import EFCheckboxCard from "../EFCheckboxCard/EFCheckboxCard";

interface EFCheckboxGameCardProps {
  gameImage: string;
  selected: boolean;
  disabled: boolean;
  onChange: () => void;
  id: string;
  size: string;
}

const EFCheckboxGameCard: React.FC<EFCheckboxGameCardProps> = ({
  gameImage,
  selected,
  disabled,
  onChange,
  id,
  size
}) => {
  return (
    <EFCheckboxCard
      imageUrl={gameImage}
      selected={selected}
      disabled={disabled}
      onChange={onChange}
      id={id}
      size={size}
    />
  );
};

export default EFCheckboxGameCard;
