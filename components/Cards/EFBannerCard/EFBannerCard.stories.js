import React from "react";
import EFBannerCard from "./EFBannerCard";

export default {
  title: "Cards/EFBannerCard",
  component: EFBannerCard,
  argsTypes: {
    backgroundImage: {
      control: {
        type: "text"
      }
    },
    title: {
      control: {
        type: "text"
      }
    },
    date: {
      control: {
        type: "text"
      }
    },
    teams: {
      control: {
        type: "text"
      }
    },
    game: {
      control: {
        type: "text"
      }
    },
    organizer: {
      control: {
        type: "text"
      }
    },
    organizerImage: {
      control: {
        type: "text"
      }
    }
  }
};

const Story = args => <EFBannerCard {...args} />;

export const Default = Story.bind({});
Default.args = {
  backgroundImage: "https://images.hdqwalls.com/download/tracer-from-overwatch-2-rm-1280x720.jpg",
  title: "Event Title",
  date: "2020-10-08T10:00:00.000Z",
  teams: "6/6",
  game: "Warzone",
  organizer: "MGG",
  organizerImage:
    "https://lh3.googleusercontent.com/proxy/PckVa1nMpsT9VAXZVxwDfGAh0H1Fwg1TqJJCkBLS3X-fQ0SFMMpuz1SRTJL1zMPNsadF0iQL7EY-1dSiJqmEjWSfW1MI63dDwmP-0EtdaDTNjg"
};
