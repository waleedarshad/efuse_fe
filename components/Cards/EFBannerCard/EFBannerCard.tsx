import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import EFCard from "../EFCard/EFCard";
import Style from "./EFBannerCard.module.scss";
import { withCdn } from "../../../common/utils";

interface EFBannerCardProps {
  backgroundImage: string;
  cardTitle: string;
  titleAvatar?: string;
  hasTitleAvatar?: boolean;
  cardSubtitle: string;
  subtitleIcon: any;
  cardDetails: any;
  cardButton?: any;
  cardShadow?: "none" | "small" | "medium" | "large";
}

const EFBannerCard: React.FC<EFBannerCardProps> = ({
  backgroundImage,
  cardTitle,
  titleAvatar,
  hasTitleAvatar,
  cardSubtitle,
  subtitleIcon,
  cardDetails,
  cardButton,
  cardShadow = "small"
}) => {
  return (
    <EFCard shadow={cardShadow} heightTheme="fullHeight" widthTheme="fullWidth">
      <div
        className={Style.backgroundImageContainer}
        style={{
          backgroundImage: `linear-gradient(0deg, #006CFA, #006CFA), linear-gradient(270deg, rgba(0, 0, 0, 0) 0%, rgba(0, 0, 0, 0.9) 100%), url(${backgroundImage})`
        }}
      >
        <div className={Style.cardTitleSection}>
          {hasTitleAvatar && (
            <img alt="" className={Style.titleAvatar} src={titleAvatar ?? withCdn("/static/images/mindblown.png")} />
          )}
          <div className={Style.title}>{cardTitle}</div>
        </div>
        <div className={Style.subtitleContainer}>
          <FontAwesomeIcon className={Style.subtitleIcon} icon={subtitleIcon} />
          <div className={Style.subtitle}>{cardSubtitle}</div>
        </div>
        <div className={Style.detailsContainer}>
          {cardDetails}
          {cardButton && <div className={Style.button}>{cardButton}</div>}
        </div>
      </div>
    </EFCard>
  );
};

export default EFBannerCard;
