import React from "react";
import Style from "./EFBannerSnippet.module.scss";

interface EFBannerSnippetProps {
  title: string;
  subtitle: string;
  imgSrc?: string;
  imgAltText?: string;
}

const EFBannerSnippet: React.FC<EFBannerSnippetProps> = ({ title, subtitle, imgSrc, imgAltText }) => {
  return (
    <div className={Style.detail}>
      <div className={Style.title}>{title}</div>
      <div className={Style.detailContent}>
        {imgSrc && <img alt={imgAltText} className={Style.avatar} src={imgSrc} />}
        <div className={`${Style.detailValue}`}>{subtitle}</div>
      </div>
    </div>
  );
};

export default EFBannerSnippet;
