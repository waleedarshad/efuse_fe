import React from "react";
import EFCard from "../EFCard/EFCard";
import CardBadge from "../CardBadge/CardBadge";
import Style from "./EFSidebarCard.module.scss";
import EFAvatar from "../../EFAvatar/EFAvatar";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";
import { EFHtmlParser } from "../../EFHtmlParser/EFHtmlParser";

const EFSidebarCard = ({
  backgroundImageUrl,
  logoImageUrl,
  title,
  subTitle,
  description,
  buttonInternalHref,
  buttonExternalHref,
  onButtonClick,
  buttonText,
  badge,
  cardShadow
}) => {
  return (
    <EFCard shadow={cardShadow} heightTheme="fullHeight" widthTheme="fullWidth">
      <div className={Style.contentWrapper}>
        <div
          className={Style.backgroundImageContainer}
          style={{
            backgroundImage: `linear-gradient(to bottom, rgba(255, 255, 255, 0), #ffffff),
      url(${backgroundImageUrl})`
          }}
        >
          <CardBadge badge={badge} />
          <div className={Style.logoContainer}>
            <EFAvatar
              profilePicture={logoImageUrl || "https://cdn.efuse.gg/static/images/Erena_Logo.png"}
              displayOnlineButton={false}
            />
          </div>
        </div>

        <div className={Style.cardContent}>
          <div className={Style.title}>{title}</div>
          <div className={Style.subTitle}>{subTitle}</div>
          <div className={Style.description}>
            <EFHtmlParser>{description}</EFHtmlParser>
          </div>
          <div className={Style.buttonWrapper}>
            <EFRectangleButton
              size="extraLarge"
              text={buttonText}
              width="fullWidth"
              internalHref={buttonInternalHref}
              externalHref={buttonExternalHref}
              onClick={onButtonClick}
            />
          </div>
        </div>
      </div>
    </EFCard>
  );
};

EFSidebarCard.defaultProps = {
  cardShadow: "small"
};

export default EFSidebarCard;
