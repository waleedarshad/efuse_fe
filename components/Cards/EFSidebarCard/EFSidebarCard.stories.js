import React from "react";
import EFSidebarCard from "./EFSidebarCard";

export default {
  title: "Cards/EFSidebarCard",
  component: EFSidebarCard,
  argsTypes: {
    backgroundImageUrl: {
      control: {
        type: "text"
      }
    },
    logoImageUrl: {
      control: {
        type: "text"
      }
    },
    titleText: {
      control: {
        type: "text"
      }
    },
    subTitle: {
      control: {
        type: "text"
      }
    },
    description: {
      control: {
        type: "text"
      }
    },
    buttonInternalHref: {
      control: {
        type: "text"
      }
    },
    buttonExternalHref: {
      control: {
        type: "text"
      }
    },
    buttonText: {
      control: {
        type: "text"
      }
    },
    badge: {
      control: {
        type: "text"
      }
    },
    onButtonClick: {
      control: {
        type: "text"
      }
    }
  }
};

const Story = args => <EFSidebarCard {...args} />;

export const Default = Story.bind({});
Default.args = {
  backgroundImageUrl: "https://i.ytimg.com/vi/0E44DClsX5Q/maxresdefault.jpg",
  logoImageUrl: "https://i.ytimg.com/vi/0E44DClsX5Q/maxresdefault.jpg",
  titleText: "this is an eRena event for testing",
  subTitle: "Saud Butt",
  buttonInternalHref: "/",
  buttonText: "View",
  badge: "erena",
  description:
    "<p>Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have scrambled parts of Cicero's De Finibus Bonorum et Malorum for use in a type specimen book.</p>"
};
