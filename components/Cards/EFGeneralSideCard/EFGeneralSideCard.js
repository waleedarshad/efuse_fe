import React from "react";
import TimeAgo from "react-timeago";
import Style from "./EFGeneralSideCard.module.scss";

import EFPillButton from "../../Buttons/EFPillButton/EFPillButton";
import EFImage from "../../EFImage/EFImage";
import OpportunityTypeBadge from "../../OpportunitiesV2/OpportunityCards/OpportunityTypeBadge/OpportunityTypeBadge";

const EFGeneralSideCard = ({
  imageUrl,
  title,
  alt,
  date,
  buttonUrl,
  buttonText,
  badgeTheme,
  badgeText,
  buttonOnClick
}) => {
  return (
    <div className={Style.cardContainer}>
      <div className={Style.imageContainer}>
        <EFImage src={imageUrl} className={Style.image} alt={alt} />
      </div>
      <div className={Style.textContainer}>
        {badgeTheme && badgeText && (
          <div className={Style.badgeWrapper}>
            <OpportunityTypeBadge opportunityType={badgeTheme} />
          </div>
        )}
        <p className={Style.title}>{title}</p>
        <div className={Style.buttonContainer}>
          <EFPillButton
            text={buttonText}
            colorTheme="light"
            shadowTheme="small"
            internalHref={buttonUrl}
            size="auto"
            onClick={buttonOnClick}
          />
          {date && (
            <p className={Style.subText}>
              <TimeAgo date={date} />
            </p>
          )}
        </div>
      </div>
    </div>
  );
};

EFGeneralSideCard.defaultProps = {
  buttonText: "VIEW"
};

export default EFGeneralSideCard;
