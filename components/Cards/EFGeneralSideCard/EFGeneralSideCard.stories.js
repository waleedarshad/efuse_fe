import React from "react";
import EFGeneralSideCard from "./EFGeneralSideCard";

export default {
  title: "Cards/EFGeneralSideCard",
  component: EFGeneralSideCard,
  argsTypes: {
    imageUrl: {
      control: {
        type: "text"
      }
    },
    title: {
      control: {
        type: "text"
      }
    },
    date: {
      control: {
        type: "text"
      }
    },
    buttonUrl: {
      control: {
        type: "text"
      }
    },
    buttonText: {
      control: {
        type: "text"
      }
    },
    badgeTheme: {
      control: {
        type: "select",
        options: ["job", "event", "scholarship", "teamOpening"]
      }
    },
    badgeText: {
      control: {
        type: "text"
      }
    }
  }
};

const Story = args => (
  <div style={{ width: "280px", margin: "3rem" }}>
    <EFGeneralSideCard {...args} />
  </div>
);

export const General = Story.bind({});
General.args = {
  imageUrl: "https://cdn.efuse.gg/uploads/static/global/mindblown_white.png",
  title: "Social Media Intern for WVU PSC Esports",
  date: "2020-12-07T11:38:05.565Z",
  buttonText: "VIEW",
  badgeTheme: "job",
  badgeText: "Job"
};

export const NoBadge = Story.bind({});
NoBadge.args = {
  imageUrl: "https://cdn.efuse.gg/uploads/static/global/mindblown_white.png",
  title: "Social Media Intern for WVU PSC Esports",
  date: "2020-12-07T11:38:05.565Z",
  buttonText: "VIEW"
};
