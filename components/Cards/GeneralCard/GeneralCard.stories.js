import React from "react";
import GeneralCard from "./GeneralCard";

export default {
  title: "Cards/GeneralCard",
  component: GeneralCard,
  argTypes: {
    badge: {
      control: {
        type: "select",
        options: ["Scholarship", "Team Opening", "Employment Opening", "Event", "News", "Learning", "Organizations"]
      }
    },
    userImages: {
      control: {
        type: "array"
      }
    },
    image: {
      control: {
        type: "text"
      }
    },
    title: {
      control: {
        type: "text"
      }
    },
    subTitle: {
      control: {
        type: "text"
      }
    },
    description: {
      control: {
        type: "text"
      }
    },
    date: {
      control: {
        type: "object"
      }
    },
    footerText: {
      control: {
        type: "text"
      }
    },
    location: {
      control: {
        type: "text"
      }
    },
    externalLink: {
      control: {
        type: "boolean"
      }
    },
    adjustToFullWidthOnMobile: {
      control: {
        type: "boolean"
      }
    },
    fullWidthHeight: {
      control: {
        type: "text"
      }
    },
    externalLink: false,
    adjustToFullWidthOnMobile: true
  }
};

const Story = args => <GeneralCard {...args} />;

export const Card = Story.bind({});
Card.args = {
  badge: "Scholarship",
  userImages: [
    {
      profilePicture: {
        url: "https://i1.sndcdn.com/avatars-000549979950-t3k7cq-t500x500.jpg"
      }
    },
    {
      profilePicture: {
        url: "https://i1.sndcdn.com/avatars-000613156446-c1wnr0-t500x500.jpg"
      }
    },
    {
      profilePicture: {
        url:
          "https://static-cdn.jtvnw.net/jtv_user_pictures/team-tatmanarmy-team_logo_image-97c907345dcc4b008d7a71ec38dedb1c-600x600.png"
      }
    }
  ],
  image: "https://i.ytimg.com/vi/0E44DClsX5Q/maxresdefault.jpg",
  title: "Call of Duty Warzone Scholarship",
  subTitle: "Shawnee State University",
  description: "Multi-platform entertainment organization and leader in professional branding and media.",
  date: new Date(),
  location: "Columbus, OH",
  externalLink: true,
  onImageClickLink: "https://www.efuse.gg",
  onTitleClickLink: "https://www.efuse.gg",
  adjustToFullWidthOnMobile: true
};

export const NoImages = Story.bind({});
NoImages.args = {
  badge: "Scholarship",
  image: "https://i.ytimg.com/vi/0E44DClsX5Q/maxresdefault.jpg",
  title: "Call of Duty Warzone Scholarship",
  subTitle: "Shawnee State University",
  description: "Multi-platform entertainment organization and leader in professional branding and media.",
  date: new Date(),
  location: "Columbus, OH",
  externalLink: true,
  onImageClickLink: "https://www.efuse.gg",
  onTitleClickLink: "https://www.efuse.gg",
  adjustToFullWidthOnMobile: true
};

export const FooterText = Story.bind({});
FooterText.args = {
  badge: "Scholarship",
  image: "https://i.ytimg.com/vi/0E44DClsX5Q/maxresdefault.jpg",
  title: "Call of Duty Warzone Scholarship",
  subTitle: "Shawnee State University",
  description: "Multi-platform entertainment organization and leader in professional branding and media.",
  location: "Columbus, OH",
  externalLink: true,
  onImageClickLink: "https://www.efuse.gg",
  onTitleClickLink: "https://www.efuse.gg",
  adjustToFullWidthOnMobile: true,
  footerText: "Custom Footer Text"
};
