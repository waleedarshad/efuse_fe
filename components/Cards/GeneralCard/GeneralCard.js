import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMapMarkerAlt } from "@fortawesome/pro-solid-svg-icons";
import TimeAgo from "react-timeago";
import Link from "next/link";
import UserImageList from "../../Opportunities/OpportunityLanding/UserImageList/UserImageList";
import Style from "./GeneralCard.module.scss";
import CardBadge from "../CardBadge/CardBadge";
import EFImage from "../../EFImage/EFImage";

const GeneralCard = props => {
  const {
    badge,
    userImages,
    image,
    title,
    subTitle,
    description,
    onImageClickLink,
    onTitleClickLink,
    date,
    footerText,
    location,
    marginTop,
    externalLink,
    adjustToFullWidthOnMobile,
    titleLinkAs,
    fullWidthHeight
  } = props;

  const backgroundImageContainer = (
    <div className={Style.backgroundImageContainer}>
      <EFImage className={Style.backgroundImage} src={image} />
      <CardBadge badge={badge} />
    </div>
  );

  const cardTitle = <h3 className={Style.cardTitle}>{title}</h3>;
  const cardSubTitle = <p className={Style.cardSubTitle}>{subTitle}</p>;

  return (
    <div
      className={`${Style.cardContainer} ${marginTop} ${adjustToFullWidthOnMobile &&
        Style.adjustToFullWidth} ${fullWidthHeight && Style.fullWidthHeight}`}
    >
      {externalLink ? (
        <a href={onImageClickLink} target="_blank" rel="noopener noreferrer">
          {backgroundImageContainer}
        </a>
      ) : (
        <Link href={onImageClickLink}>{backgroundImageContainer}</Link>
      )}
      <div className={Style.text}>
        {externalLink ? (
          <>
            <a href={onImageClickLink} target="_blank" rel="noopener noreferrer">
              {cardTitle}
            </a>
            <a href={onTitleClickLink} target="_blank" rel="noopener noreferrer">
              {cardSubTitle}
            </a>
          </>
        ) : (
          <>
            <Link href={onImageClickLink}>{cardTitle}</Link>
            <Link href={onTitleClickLink} as={titleLinkAs}>
              {cardSubTitle}
            </Link>
          </>
        )}
        {location && (
          <p className={Style.cardLocation}>
            <FontAwesomeIcon className={Style.locationIcon} icon={faMapMarkerAlt} />
            {location}
          </p>
        )}
        {description && <p className={Style.description}>{description}</p>}
      </div>
      <div className={Style.cardFooter}>
        {date && (
          <p className={Style.date}>
            <TimeAgo date={date} />
          </p>
        )}
        {footerText && <p className={Style.date}>{footerText}</p>}
        {userImages?.length > 0 && (
          <div className={Style.appliedIcons}>
            <UserImageList
              imageList={userImages.map((user, index) => {
                if (index < 4) {
                  return user?.profilePicture?.url;
                }
              })}
            />
          </div>
        )}
      </div>
    </div>
  );
};

export default GeneralCard;
