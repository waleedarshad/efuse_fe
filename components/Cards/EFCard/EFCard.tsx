import React from "react";
import Style from "./EFCard.module.scss";

interface EFCardProps {
  shadow?: "none" | "small" | "medium" | "large";
  widthTheme?: "fitContent" | "fullWidth";
  heightTheme?: "auto" | "fullHeight";
  overflowTheme?: "hidden" | "visible";
  hoverEffect?: "default" | "blue";
  noBorder?: boolean;
  isSelected?: boolean;
  children?: any;
  onClick?: (event: any) => void;
  isDisabled?: boolean;
  className?: string;
}

const EFCard: React.FC<EFCardProps> = ({
  shadow = "small",
  widthTheme = "fitContent",
  heightTheme = "auto",
  overflowTheme = "hidden",
  hoverEffect = "default",
  noBorder,
  isSelected = false,
  isDisabled = false,
  onClick = undefined,
  children,
  className
}) => {
  const isCardClickable = onClick && !isDisabled;
  const onCardClick = (event: any) => isCardClickable && onClick(event);

  const getClickableProps = () =>
    isCardClickable ? { role: "button", tabIndex: 0, onClick: onCardClick, onKeyPress: onCardClick } : {};

  return (
    <div
      {...getClickableProps()}
      className={`${Style.cardStyle} ${isSelected ? Style.selected : ""} ${Style[widthTheme]} ${Style[overflowTheme]} ${
        Style[heightTheme]
      } ${Style[`${shadow}Shadow`]} } ${isCardClickable ? Style[hoverEffect] : ""} ${noBorder ? Style.noBorder : ""} ${
        isDisabled ? Style.disabled : ""
      } ${className}`}
    >
      {children}
    </div>
  );
};

export default EFCard;
