import React from "react";
import EFCard from "./EFCard";

export default {
  title: "Cards/EFCard",
  component: EFCard,
  argTypes: {
    shadow: {
      control: {
        type: "select",
        options: ["none", "small", "medium", "large"]
      }
    },
    widthTheme: {
      control: {
        type: "select",
        options: ["fitContent", "fullWidth"]
      }
    },
    heightTheme: {
      control: {
        type: "select",
        options: ["auto", "fullHeight"]
      }
    },
    overflowTheme: {
      control: {
        type: "select",
        options: ["hidden", "visible"]
      }
    },
    hoverEffect: {
      control: {
        type: "select",
        options: ["default", "blue"]
      }
    },
    noBorder: {
      control: {
        type: "boolean"
      }
    }
  }
};

const Content = () => <div style={{ padding: "100px" }}>Content Here</div>;

// eslint-disable-next-line react/jsx-props-no-spreading
const Story = args => <EFCard {...args} />;

export const Default = Story.bind({});
Default.args = {
  children: <Content />
};

export const SmallShadow = Story.bind({});
SmallShadow.args = {
  children: <Content />,
  shadow: "small"
};

export const MediumShadow = Story.bind({});
MediumShadow.args = {
  children: <Content />,
  shadow: "medium"
};

export const LargeShadow = Story.bind({});
LargeShadow.args = {
  children: <Content />,
  shadow: "large"
};

export const NoShadowWithDefaultHover = Story.bind({});
NoShadowWithDefaultHover.args = {
  children: <Content />,
  shadow: "none",
  hoverEffect: "default"
};

export const NoBorderWithBlueHover = Story.bind({});
NoBorderWithBlueHover.args = {
  children: <Content />,
  noBorder: true,
  hoverEffect: "blue"
};
