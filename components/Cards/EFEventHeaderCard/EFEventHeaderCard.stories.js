import React from "react";
import EFEventHeaderCard from "./EFEventHeaderCard";

export default {
  title: "Cards/EFEventHeaderCard",
  component: EFEventHeaderCard,
  argsTypes: {
    backgroundImage: {
      control: {
        type: "text"
      }
    },
    title: {
      control: {
        type: "text"
      }
    },
    date: {
      control: {
        type: "text"
      }
    },
    teams: {
      control: {
        type: "text"
      }
    },
    game: {
      control: {
        type: "text"
      }
    },
    organizer: {
      control: {
        type: "text"
      }
    },
    organizerImage: {
      control: {
        type: "text"
      }
    },
    chip: {
      control: {
        type: "text"
      }
    }
  }
};

const Story = args => <EFEventHeaderCard {...args} />;
export const General = Story.bind({});
General.args = {
  backgroundImage: "https://wallpapercave.com/wp/wp2928790.jpg",
  title: "Championship Battle",
  subTitle: "Single Elimination",
  date: "2020-12-07T11:38:05.565Z",
  teams: "6/6",
  game: "Warzone",
  organizer: "MGG",
  organizerImage: "https://i.ytimg.com/vi/0E44DClsX5Q/maxresdefault.jpg",
  chip: "Under Construction"
};
