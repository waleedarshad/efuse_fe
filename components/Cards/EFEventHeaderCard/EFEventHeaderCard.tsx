import React, { ReactNode } from "react";
import moment from "moment";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChessRook } from "@fortawesome/pro-solid-svg-icons";
import Style from "./EFEventHeaderCard.module.scss";
import EFCard from "../EFCard/EFCard";
import EFResponsiveChip from "../../EFResponsiveChip/EFResponsiveChip";

interface EFEventHeaderCardProps {
  backgroundImage: string;
  title: string;
  date: string | Date;
  confirmedTeams: string | number;
  maxTeams: string | number;
  game: string;
  organizer: string;
  organizerImage: string;
  subTitle: string;
  leagueEventStateChip: string;
  actionButton?: ReactNode;
}

const EFCardEventHeader: React.FC<EFEventHeaderCardProps> = ({
  backgroundImage,
  title,
  subTitle,
  leagueEventStateChip,
  date,
  confirmedTeams,
  maxTeams,
  game,
  organizer,
  organizerImage,
  actionButton
}) => {
  return (
    <EFCard heightTheme="fullHeight" widthTheme="fullWidth" shadow="none" hoverEffect="default">
      <div className={Style.contentWrapper}>
        <div
          className={Style.backgroundImageContainer}
          style={{
            backgroundImage: `linear-gradient(0deg, rgba(17, 25, 37, 0.6), rgba(17, 25, 37, 0.6)), url(${backgroundImage})`
          }}
        >
          <div className={Style.eventChip}>
            <EFResponsiveChip badgeType={leagueEventStateChip} newVersion />
          </div>
          <div className={Style.contentContainer}>
            <div className={Style.cardHeaderBar}>
              <div className={Style.cardHeading}>{title}</div>
            </div>
            <div className={Style.flexBar}>
              <div className={Style.iconContainer}>
                <FontAwesomeIcon icon={faChessRook} className={Style.icon} />
              </div>
              <div className={Style.cardText}>{subTitle}</div>
            </div>
          </div>
        </div>
        <div className={Style.detailsContainer}>
          <div className={Style.detailsList}>
            <div className={Style.listHeading}>Date | Time</div>
            <div className={Style.listSubHeading}>{moment(date).format("MMM.DD | h:mm A")}</div>
          </div>
          <div className={Style.detailsList}>
            <div className={Style.listHeading}>Teams</div>
            <div className={Style.listSubHeading}>{`${confirmedTeams}/${maxTeams}`}</div>
          </div>
          <div className={Style.detailsList}>
            <div className={Style.listHeading}>Game</div>
            <div className={Style.listSubHeading}>{game}</div>
          </div>
          <div className={Style.detailsList}>
            <div className={Style.listHeading}>Organizer</div>
            <div className={Style.organizerBar}>
              <img alt="" className={Style.organizerThumbnail} src={organizerImage} />
              <div className={`${Style.listSubHeading} ${Style.organizerMargin}`}>{organizer}</div>
            </div>
          </div>
          {actionButton && <div className={Style.buttonContainer}>{actionButton}</div>}
        </div>
      </div>
    </EFCard>
  );
};

export default EFCardEventHeader;
