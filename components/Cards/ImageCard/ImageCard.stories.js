import React from "react";
import ImageCard from "./ImageCard";

export default {
  title: "Cards/ImageCard",
  component: ImageCard,
  argTypes: {
    type: {
      control: {
        type: "select",
        options: ["Scholarship", "Team Opening", "Employment Opening", "Event"]
      }
    },
    backgroundImage: {
      control: {
        type: "text"
      }
    },
    userImage: {
      control: {
        type: "text"
      }
    }
  }
};

const Story = args => <ImageCard {...args} />;

export const Card = Story.bind({});
Card.args = {
  type: "Scholarship",
  backgroundImage: "https://i.ytimg.com/vi/3KgmY5NrEzU/maxresdefault.jpg",
  userImage: "https://yt3.ggpht.com/a/AATXAJynkPnPVO1teU1292tS2f52lj5Tb0ZwM_ML4ounGg=s900-c-k-c0xffffffff-no-rj-mo"
};

export const NoType = Story.bind({});
NoType.args = {
  backgroundImage: "https://i.ytimg.com/vi/3KgmY5NrEzU/maxresdefault.jpg",
  userImage: "https://yt3.ggpht.com/a/AATXAJynkPnPVO1teU1292tS2f52lj5Tb0ZwM_ML4ounGg=s900-c-k-c0xffffffff-no-rj-mo"
};
