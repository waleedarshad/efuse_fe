import React from "react";
import Style from "./ImageCard.module.scss";
import CardBadge from "../CardBadge/CardBadge";

const ImageCard = ({ type, backgroundImage, onBackgroundClick, userImage, onUserClick }) => {
  return (
    <div className={Style.cardContainer}>
      {type && <CardBadge badge={type} />}
      <div className={Style.backgroundImageContainer}>
        <img className={Style.backgroundImage} src={backgroundImage} onClick={onBackgroundClick && onBackgroundClick} />
      </div>
      <div className={Style.userContainer}>
        <img className={Style.userImage} src={userImage} onClick={onUserClick && onUserClick} />
      </div>
    </div>
  );
};

export default ImageCard;
