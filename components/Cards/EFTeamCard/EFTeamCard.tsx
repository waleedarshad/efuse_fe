import React, { ReactNode } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faExclamationCircle } from "@fortawesome/pro-regular-svg-icons";
import Link from "next/link";
import uniqueId from "lodash/uniqueId";
import EFCard from "../EFCard/EFCard";
import Style from "./EFTeamCard.module.scss";
import EFUserBadge from "../../EFUserBadge/EFUserBadge";
import EFImage from "../../EFImage/EFImage";
import EFLightDropdown from "../../Dropdowns/EFLightDropdown/EFLightDropdown";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";
import EFTooltip from "../../tooltip/EFTooltip/EFTooltip";

export interface ITeamCardMenuItem {
  option: ReactNode | string;
  onClick: (event: any) => void;
}

export interface EFTeamCardProps {
  teamImageUrl: string;
  teamName: string;
  teamMembers: { imageUrl: string; name: string }[];
  menuItems: ITeamCardMenuItem[];
  inviteLink?: string;
  imageHref?: string;
  hideInvite: boolean;
  noMembersMessage: string;
  isAdmin: boolean;
}

const EFTeamCard = ({
  teamImageUrl,
  teamName,
  teamMembers,
  menuItems,
  inviteLink,
  hideInvite,
  imageHref,
  noMembersMessage = "Currently no members are part of this team. Invite members to join.",
  isAdmin = false
}: EFTeamCardProps) => {
  const teamMembersCount = teamMembers?.length;

  const displayImage = () => {
    const image = (
      <div className={`${Style.logoWrapper} ${imageHref ? Style.asLink : ""}`}>
        <EFImage width={50} height={50} src={teamImageUrl} className={Style.logo} />
      </div>
    );
    return imageHref ? <Link href={imageHref}>{image}</Link> : image;
  };

  return (
    <EFCard overflowTheme="visible" shadow="none">
      <div className={Style.efTeamCard}>
        {!hideInvite && teamMembersCount === 0 && (
          <span className={Style.alertIconWrapper}>
            <FontAwesomeIcon icon={faExclamationCircle} className={Style.icon} />
          </span>
        )}
        <div className={Style.header}>
          {displayImage()}
          <div className={Style.teamDetail}>
            <EFTooltip tooltipContent={teamName} tooltipPlacement="top">
              <h2 className={Style.teamName}>{teamName}</h2>
            </EFTooltip>
            <span className={Style.members}>{`${teamMembersCount} members`}</span>
          </div>
          {isAdmin && <EFLightDropdown menuItems={menuItems} />}
        </div>
        <ul className={Style.memberList}>
          {teamMembersCount > 0 ? (
            teamMembers.map((member, index) => (
              <React.Fragment key={uniqueId()}>
                <li>
                  <EFUserBadge
                    badgeSize="small"
                    profilePictureUrl={member.imageUrl}
                    fullname={member.name}
                    displayOnlineButton={false}
                  />
                </li>
                {index !== teamMembers.length - 1 && <hr className={Style.separator} />}
              </React.Fragment>
            ))
          ) : (
            <>
              <div className={Style.noMembersText}>{noMembersMessage}</div>
              {!hideInvite && (
                <EFRectangleButton text="Add Members" width="fullWidth" internalHref={inviteLink} shadowTheme="none" />
              )}
            </>
          )}
        </ul>
      </div>
    </EFCard>
  );
};

export default EFTeamCard;
