import React from "react";
import Style from "./EFDetailsListCard.module.scss";

const EFDetailsListCard = ({ title, data }) => {
  return (
    <div className={Style.cardContainer}>
      {title && <p className={Style.title}>{title}</p>}
      <div className={Style.additionalInformation}>
        {data.map((item, index) => {
          return (
            <div key={index} className={Style.field}>
              {item.title}: <b>{item.value || "N/A"}</b>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default EFDetailsListCard;
