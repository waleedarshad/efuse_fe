import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Style from "./PlatformCard.module.scss";

const PlatformCard = props => {
  const {
    displayImage,
    cardIcon,
    platform,
    firstLabel,
    secondLabel,
    firstValue,
    secondValue,
    buttonComponent,
    displayButton
  } = props;

  return (
    <div className={Style.platformCardContainer}>
      {displayImage ? (
        <>
          <img
            className={`${Style.platformImageIcon} ${platform === "battle.net" && Style.battlenetColor}`}
            src={cardIcon}
          />
          <img className={`${Style.backgroundImageIcon}`} src={cardIcon} />
        </>
      ) : (
        <>
          <FontAwesomeIcon icon={cardIcon} className={Style.backgroundIcon} />
          <FontAwesomeIcon
            icon={cardIcon}
            className={`${Style.platformIcon} ${platform === "steam" && Style.steamColor} ${platform === "xbox" &&
              Style.xboxColor} ${platform === "playstation" && Style.playstationColor}`}
          />
        </>
      )}
      <div className={Style.firstTextContainer}>
        <p className={Style.textLabel}>{firstLabel}</p>
        <p className={Style.textValue}>{firstValue}</p>
      </div>
      {secondValue && (
        <div className={Style.secondTextContainer}>
          <p className={Style.textLabel}>{secondLabel}</p>
          <p className={Style.textValue}>{secondValue}</p>
        </div>
      )}
      {buttonComponent && displayButton && buttonComponent}
    </div>
  );
};

export default PlatformCard;
