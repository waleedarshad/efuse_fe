import React from "react";
import dynamic from "next/dynamic";

import allowedTextFormats from "../../static/data/allowedTextFormats.json";
import Style from "./EFHtmlInput.module.scss";

const ReactQuill = dynamic(() => import("react-quill"), { ssr: false });

const EFHtmlInput = ({ disabled, value, onChange, displayError, errorMessage, id }) => {
  return (
    <>
      <ReactQuill
        id={id}
        disabled={disabled}
        value={value || ""}
        onChange={onChange}
        className={Style.reactQuill}
        formats={allowedTextFormats}
      />
      {displayError && <p className={Style.error}>{errorMessage}</p>}
    </>
  );
};

EFHtmlInput.defaultProps = {
  disabled: false
};

export default EFHtmlInput;
