import React from "react";
import EFHtmlInput from "./EFHtmlInput";
import InputRow from "../InputRow/InputRow";

export default {
  title: "Input/EFHtmlInput",
  component: EFHtmlInput,
  argTypes: {
    disabled: {
      control: {
        type: "boolean"
      }
    },
    value: {
      control: {
        type: "text"
      }
    },
    displayError: {
      control: {
        type: "boolean"
      }
    },
    errorMessage: {
      control: {
        type: "text"
      }
    }
  }
};

const Story = args => (
  <InputRow>
    <EFHtmlInput {...args} />
  </InputRow>
);

export const Default = Story.bind({});

Default.args = {
  disabled: false,
  value: "Something is in this input...",
  displayError: false,
  errorMessage: "Content is required"
};
