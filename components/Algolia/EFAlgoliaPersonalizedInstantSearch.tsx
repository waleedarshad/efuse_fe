import React, { memo } from "react";
import { useSelector } from "react-redux";
import { InstantSearch, Configure } from "react-instantsearch-dom";
import { returnAlgoliaSearchClient } from "../../helpers/AlgoliaHelper";

const EFAlgoliaPersonalizedInstantSearch = ({ indexName, children, hitsPerPage }) => {
  const currentUser = useSelector(state => state.auth.currentUser);

  return (
    <InstantSearch searchClient={returnAlgoliaSearchClient()} indexName={indexName}>
      <Configure
        enablePersonalization
        userToken={currentUser?._id}
        hitsPerPage={hitsPerPage}
        distinct
        analytics={false}
      />
      {children}
    </InstantSearch>
  );
};

export default memo(EFAlgoliaPersonalizedInstantSearch);
