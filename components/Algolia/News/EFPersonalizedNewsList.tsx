import { connectInfiniteHits } from "react-instantsearch-dom";
import React from "react";

import { triggerDebouncedSegmentEvent } from "../../../helpers/AlgoliaHelper";
import EFListCard from "../../Cards/EFListCard/EFListCard";
import Style from "./EFPersonalizedNewsList.module.scss";
import EFShowMoreButton from "../../Buttons/EFShowMoreButton/EFShowMoreButton";

const EFPersonalizedNewsList = connectInfiniteHits(({ hits, hasMore, refineNext, indexName, categoryList }) => {
  triggerDebouncedSegmentEvent(hits, "NEWS_CARD_LIST_VIEW", indexName);

  return (
    <>
      <ul className={Style.newsArticlesList}>
        {hits.map(hit => {
          const hitCategory = categoryList?.find(category => category._id === hit.category)?.slug;
          return (
            <li key={hit.objectID}>
              <div className={Style.cardWrapper}>
                <EFListCard
                  backgroundImage={hit?.image?.url}
                  liveChip={false}
                  categoryChip="news"
                  title={hit?.title}
                  date={hit?.publishDate}
                  owner={hit?.author?.name}
                  backgroundImageAltText={hit?.slug}
                  href={`/news/${hitCategory}/${hit?.slug}`}
                  timeAgo
                  size="large"
                  onClick={() => {
                    // mapping to Product Clicked in Algolia Insights destination
                    // @ts-ignore
                    analytics.track("NEWS_ARTICLE_CLICKED", {
                      opportunityId: hit?._id,
                      objectID: hit.objectID,
                      position: hit.__position,
                      index: indexName,
                      queryID: hit.__queryID
                    });
                  }}
                />
              </div>
            </li>
          );
        })}
      </ul>
      {hasMore && (
        <div className={Style.showMoreButton}>
          <EFShowMoreButton onClick={refineNext} text="Show more" />
        </div>
      )}
    </>
  );
});

export default EFPersonalizedNewsList;
