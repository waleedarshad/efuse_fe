import { connectPagination } from "react-instantsearch-dom";
import EFPaginationBar from "../../EFPaginationBar/EFPaginationBar";

const Pagination = ({ nbPages, refine }) => {
  return <EFPaginationBar totalPages={nbPages} onPageChange={refine} marginPages={1} pageRange={2} />;
};

export default connectPagination(Pagination);
