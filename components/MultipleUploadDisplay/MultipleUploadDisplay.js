import React from "react";

import Style from "./MultipleUploadDisplay.module.scss";

const MultipleUploadDisplay = ({ files }) => {
  // Temporary check to make sure we render an array, to be removed when media file stored in DB will be changed from object to string
  const mediaFiles = files instanceof Array ? files : [files];
  const content =
    files &&
    mediaFiles.map((file, index) => {
      const imageContainerclass = `imageContainer${index + 1}`;
      return (
        <div className={files.length === 3 && Style[imageContainerclass]} key={index}>
          <img src={file.url} alt="Post file" className={Style.image} />
        </div>
      );
    });

  return <div className={`${Style.mainContainer} ${mediaFiles.length > 1 && Style.gridGap}`}>{content || <></>}</div>;
};

export default MultipleUploadDisplay;
