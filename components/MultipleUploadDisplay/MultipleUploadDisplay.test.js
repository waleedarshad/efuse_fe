import { shallow } from "enzyme";
import MultipleUploadDisplay from "./MultipleUploadDisplay";

describe("MultipleUploadDisplay", () => {
  beforeEach(() => {
    const modalRoot = document.createElement("div");
    modalRoot.setAttribute("id", "modal-root");
    document.body.append(modalRoot);
  });

  it("Displays message when no image is provided", () => {
    const subject = shallow(<MultipleUploadDisplay />);
    expect(subject.find("div").text()).toEqual("");
  });

  it("Displays one image", () => {
    const files = [
      {
        filename: "3552c904-bc2f-4c44-b71a-af8d3e50fa00_Screenshot from 2020-12-01 15-48-30.png",
        contentType: "image/png",
        url:
          "https://staging-cdn.efuse.gg/uploads/media/3552c904-bc2f-4c44-b71a-af8d3e50fa00_Screenshot%20from%202020-12-01%2015-48-30.png"
      }
    ];
    const subject = shallow(<MultipleUploadDisplay files={files} />);
    expect(subject.find("img").length).toEqual(1);
  });

  it("Displays one image", () => {
    const files = [
      {
        filename: "3552c904-bc2f-4c44-b71a-af8d3e50fa00_Screenshot from 2020-12-01 15-48-30.png",
        contentType: "image/png",
        url:
          "https://staging-cdn.efuse.gg/uploads/media/3552c904-bc2f-4c44-b71a-af8d3e50fa00_Screenshot%20from%202020-12-01%2015-48-30.png"
      }
    ];
    const subject = shallow(<MultipleUploadDisplay files={files} />);
    expect(subject.find("img").length).toEqual(1);
  });

  it("Displays two images", () => {
    const files = [
      {
        filename: "3552c904-bc2f-4c44-b71a-af8d3e50fa00_Screenshot from 2020-12-01 15-48-30.png",
        contentType: "image/png",
        url:
          "https://staging-cdn.efuse.gg/uploads/media/3552c904-bc2f-4c44-b71a-af8d3e50fa00_Screenshot%20from%202020-12-01%2015-48-30.png"
      },
      {
        filename: "8ca03737-60fa-460c-b792-749c87bf6fcd_Screenshot from 2020-11-23 12-04-39.png",
        contentType: "image/png",
        url:
          "https://staging-cdn.efuse.gg/uploads/media/8ca03737-60fa-460c-b792-749c87bf6fcd_Screenshot%20from%202020-11-23%2012-04-39.png"
      }
    ];
    const subject = shallow(<MultipleUploadDisplay files={files} />);
    expect(subject.find("img").length).toEqual(2);
  });

  it("Displays three images", () => {
    const files = [
      {
        filename: "3552c904-bc2f-4c44-b71a-af8d3e50fa00_Screenshot from 2020-12-01 15-48-30.png",
        contentType: "image/png",
        url:
          "https://staging-cdn.efuse.gg/uploads/media/3552c904-bc2f-4c44-b71a-af8d3e50fa00_Screenshot%20from%202020-12-01%2015-48-30.png"
      },
      {
        filename: "8ca03737-60fa-460c-b792-749c87bf6fcd_Screenshot from 2020-11-23 12-04-39.png",
        contentType: "image/png",
        url:
          "https://staging-cdn.efuse.gg/uploads/media/8ca03737-60fa-460c-b792-749c87bf6fcd_Screenshot%20from%202020-11-23%2012-04-39.png"
      },
      {
        filename: "4d213094-f4c9-44a4-97e0-5dbb699333cf_Screenshot 2020-06-11 at 7.12.40 PM.png",
        contentType: "image/png",
        url:
          "https://staging-cdn.efuse.gg/uploads/media/dc061027-be92-4f95-aaad-1b45b1b9229f_Screenshot%202020-10-27%20at%202.56.59%20PM.png"
      }
    ];
    const subject = shallow(<MultipleUploadDisplay files={files} />);
    expect(subject.find("img").length).toEqual(3);
  });

  it("Displays four images", () => {
    const files = [
      {
        filename: "3552c904-bc2f-4c44-b71a-af8d3e50fa00_Screenshot from 2020-12-01 15-48-30.png",
        contentType: "image/png",
        url:
          "https://staging-cdn.efuse.gg/uploads/media/3552c904-bc2f-4c44-b71a-af8d3e50fa00_Screenshot%20from%202020-12-01%2015-48-30.png"
      },
      {
        filename: "8ca03737-60fa-460c-b792-749c87bf6fcd_Screenshot from 2020-11-23 12-04-39.png",
        contentType: "image/png",
        url:
          "https://staging-cdn.efuse.gg/uploads/media/8ca03737-60fa-460c-b792-749c87bf6fcd_Screenshot%20from%202020-11-23%2012-04-39.png"
      },
      {
        filename: "4d213094-f4c9-44a4-97e0-5dbb699333cf_Screenshot 2020-06-11 at 7.12.40 PM.png",
        contentType: "image/png",
        url:
          "https://staging-cdn.efuse.gg/uploads/media/dc061027-be92-4f95-aaad-1b45b1b9229f_Screenshot%202020-10-27%20at%202.56.59%20PM.png"
      },
      {
        filename: "3552c904-bc2f-4c44-b71a-af8d3e50fa00_Screenshot from 2020-12-01 15-48-30.png",
        contentType: "image/png",
        url:
          "https://staging-cdn.efuse.gg/uploads/media/3552c904-bc2f-4c44-b71a-af8d3e50fa00_Screenshot%20from%202020-12-01%2015-48-30.png"
      }
    ];
    const subject = shallow(<MultipleUploadDisplay files={files} />);
    expect(subject.find("img").length).toEqual(4);
  });
});
