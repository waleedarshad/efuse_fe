import React from "react";

import ProgressBar from "react-bootstrap/ProgressBar";
import Style from "./EFProgressBar.module.scss";
import EFRectangleButton from "../Buttons/EFRectangleButton/EFRectangleButton";

const EFProgressBar = ({ progressText, percentage, buttonRedirectLink, progressStatus }) => {
  return (
    <div className={Style.progress}>
      <div className={Style.progressBarInfo}>
        <div className={Style.barContent}>
          <span className={Style.progressBarTitle}>{progressText}</span>
          <span className={Style.progressBarValue}>{progressStatus}</span>
        </div>
      </div>

      <div className={Style.progressContainer}>
        <div className={Style.progressBar}>
          <ProgressBar now={percentage} />
        </div>
        <div className={Style.finishBtn}>
          <EFRectangleButton
            text="Finish"
            buttonType="button"
            colorTheme="secondary"
            size="small"
            internalHref={buttonRedirectLink}
            shadowTheme="small"
            onClick={() => analytics.track("COMPLETE_PORTFOLIO_COMPONENT_BUTTON_CLICKED")}
          />
        </div>
      </div>
    </div>
  );
};

export default EFProgressBar;
