import EFProgressBar from "./EFProgressBar";

export default {
  title: "Shared/EFProgressBar",
  component: EFProgressBar,
  argTypes: {
    progressText: {
      control: {
        type: "text"
      }
    },
    percentage: {
      control: {
        type: "number"
      }
    },
    buttonRedirectLink: {
      control: {
        type: "text"
      }
    },
    progressStatus: {
      contro: {
        type: "text"
      }
    }
  }
};

const Story = args => <EFProgressBar {...args} />;

export const ProgressBar = Story.bind({});

ProgressBar.args = {
  progressText: "Complete your Portfolio - ",
  percentage: "75",
  buttonRedirectLink: "/u/[u]",
  progressStatus: "Intermediate"
};
