import React, { useState } from "react";
import { Container } from "react-bootstrap";
import { useDispatch } from "react-redux";
import { useRouter } from "next/router";

import EFRectangleButton from "../Buttons/EFRectangleButton/EFRectangleButton";
import InputField from "../InputField/InputField";
import { subscribeToNewsletter } from "../../store/actions/authActions";
import Style from "./WeeklySpark.module.scss";
import EFImage from "../EFImage/EFImage";
import BackgroundImage from "../Join/Persona/BackgroundImage/BackgroundImage";

const WeeklySpark = () => {
  const dispatch = useDispatch();
  const router = useRouter();
  const [email, setEmail] = useState("");

  const onClick = e => {
    e.preventDefault();
    dispatch(subscribeToNewsletter(email));
    router.push("/");
  };

  return (
    <div className={Style.mainContainer}>
      <BackgroundImage />
      <Container>
        <div className={`row ${Style.contentContainer}`}>
          <div className="col-md-7">
            <div className={Style.textContainer} id="text">
              <h1 className={Style.heading}> The Weekly Spark </h1>
              <p className={Style.subHeading}>
                Get caught up on the latest financial, collegiate, and breaking news in the esports industry.
              </p>
              <div className={Style.emailInput}>
                <div className={Style.inputWrapper}>
                  <InputField placeholder="What's your email" type="email" onChange={e => setEmail(e.target.value)} />
                </div>
                <div className={Style.buttonWrapper}>
                  <EFRectangleButton colorTheme="secondary" text="Join the Spark" disabled={!email} onClick={onClick} />
                </div>
              </div>
              <p className={Style.description}>
                The weekly spark is a bi-weekly newsletter that delivers the most recent and important news you need to
                know about esports. From updates on competition, the newest partnerships in esports, and the companies
                raising funding to grow their operations. The spark has it all.
              </p>
            </div>
          </div>
          <div className={`col-md-5 ${Style.imageWrapper}`}>
            <EFImage src="https://cdn.efuse.gg/static/images/LetterAsset.png" width={350} height={300} />
          </div>
        </div>
      </Container>
    </div>
  );
};

export default WeeklySpark;
