import React, { useState } from "react";
import PhoneInput from "react-phone-number-input";
import "react-phone-number-input/style.css";
import { useSelector, useDispatch } from "react-redux";
import { Spinner } from "react-bootstrap";

import EFCard from "../Cards/EFCard/EFCard";
import Style from "./EFTwilioAppDownload.module.scss";
import EFRectangleButton from "../Buttons/EFRectangleButton/EFRectangleButton";
import { withCdn } from "../../common/utils";
import sendAppDownloadText from "../../store/actions/twilioActions";

const EFTwilioAppDownload = () => {
  const dispatch = useDispatch();
  const [value, setValue] = useState("");
  const isLoading = useSelector(state => state.loader.loading);

  return (
    <EFCard>
      <div className={Style.container}>
        <div className={Style.topRowText}>
          <h3>Download the App!</h3>
          <p>Enter your phone number below to recieve SMS with a link to download the eFuse app.</p>
        </div>
        <img
          src={withCdn("/uploads/landing-pages/mobile_app_screenshot.png")}
          alt="mobile app screenshot"
          className={Style.appScreenshot}
        />
        <div className={Style.phoneNumberRow}>
          <div className={Style.phoneInput}>
            <PhoneInput defaultCountry="US" placeholder="Enter phone number" value={value} onChange={setValue} />
          </div>
          <EFRectangleButton
            text={isLoading ? <Spinner animation="border" variant="dark" size="sm" /> : "Send"}
            size="large"
            disabled={isLoading || !value}
            colorTheme="white"
            onClick={() => {
              analytics.track("APP_DOWNLOAD_SMS_SENT");
              dispatch(sendAppDownloadText(value));
            }}
          />
        </div>
        <small>Standard message and data rates apply.</small>
      </div>
    </EFCard>
  );
};

export default EFTwilioAppDownload;
