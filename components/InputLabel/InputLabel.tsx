import PropTypes from "prop-types";
import Form from "react-bootstrap/Form";

import React from "react";
import Style from "./InputLabel.module.scss";

interface InputLabelProps {
  theme?: string;
  mobileMarginTop?: boolean;
  optional?: boolean;
  required?: boolean;
  children: React.ReactNode;
}

const InputLabel = ({ theme = "external", mobileMarginTop, optional, required, children }: InputLabelProps) => {
  return (
    <Form.Label className={`${Style.inputLabel} ${Style[theme]} ${mobileMarginTop && Style.mobileMarginTop}`}>
      {children}
      {optional && <span className={Style.optionalBadge}>Optional</span>}
      {required && <span className={Style.requiredAsterisk}>*</span>}
    </Form.Label>
  );
};

InputLabel.propTypes = {
  theme: PropTypes.string
};

InputLabel.defaultProps = {
  theme: "external"
};

export default InputLabel;
