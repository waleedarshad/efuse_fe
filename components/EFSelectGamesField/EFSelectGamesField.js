import React, { useState } from "react";
import { faGamepad } from "@fortawesome/pro-solid-svg-icons";

import EFPillButton from "../Buttons/EFPillButton/EFPillButton";
import SelectGamesModal from "../Modals/SelectGamesModal/SelectGamesModal";
import EFGameChipList from "../EFGameChipList/EFGameChipList";
import Style from "./EFSelectGamesField.module.scss";

// value accepts an array of game ids to be autoselected (for edit)
const EFSelectGamesField = ({ onChange, buttonText, value, selectMultiple, showGameChipList }) => {
  const [isOpen, toggleModal] = useState(false);

  const pillButtonText = buttonText || (selectMultiple ? "Select Games" : "Select Game");

  return (
    <>
      <EFPillButton
        buttonType="button"
        shadowTheme="small"
        colorTheme="light"
        fontWeightTheme="normal"
        icon={faGamepad}
        text={pillButtonText}
        width="fullWidth"
        size="large"
        alignContent="left"
        onClick={() => toggleModal(true)}
      />
      {showGameChipList && value && (
        <div className={Style.addMarginBottom}>
          <EFGameChipList games={value} />
        </div>
      )}
      <SelectGamesModal
        isOpen={isOpen}
        onClose={() => toggleModal(false)}
        selectMultiple={selectMultiple}
        value={value}
        onChange={onChange}
      />
    </>
  );
};

EFSelectGamesField.defaultProps = {
  showGameChipList: true,
  selectMultiple: true,
  buttonText: ""
};

export default EFSelectGamesField;
