import { useContext } from "react";
import { Spinner } from "react-bootstrap";
import ReactPlayer from "react-player";
import EFPillButton from "../../Buttons/EFPillButton/EFPillButton";
import { UploadContext } from "../UploadContext";
import Style from "./DisplayMedia.module.scss";

const DisplayMedia = ({
  file,
  gif,
  clearGif,
  twitchClip,
  clearTwitchClip,
  clearUploadedFile,
  youtubeClip,
  clearYoutubeClip,
  commentMedia
}) => {
  const { isUploading, cancelUpload } = useContext(UploadContext);

  if (isUploading) {
    return (
      <div className={Style.displayMediaContainer}>
        <div className={`${Style.wrapper} ${commentMedia && Style.commentWrapper}`}>
          <div className={Style.container}>
            <div className={Style.loader}>
              <Spinner animation="border" />
            </div>
          </div>
        </div>
      </div>
    );
  }

  const onRemove = () => {
    if (file.edit) {
      clearUploadedFile({});
    } else if (file.url) {
      cancelUpload();
    }

    if (clearGif) {
      clearGif();
    }

    if (clearTwitchClip) {
      clearTwitchClip();
    }

    if (clearYoutubeClip) {
      clearYoutubeClip({});
    }
  };

  const mediaUrl =
    file?.url ||
    gif?.images?.fixed_height_downsampled.url ||
    twitchClip?.thumbnail_url ||
    youtubeClip?.thumbnails?.medium?.url;

  if (mediaUrl) {
    return (
      <div className={Style.displayMediaContainer}>
        <div className={`${Style.wrapper} ${commentMedia && Style.commentWrapper}`}>
          <div className={Style.removeButton}>
            <EFPillButton
              onClick={onRemove}
              buttonType="button"
              shadowTheme="small"
              colorTheme="light"
              fontWeightTheme="normal"
              text="remove"
              fontCaseTheme="upperCase"
              alignContent="left"
            />
          </div>
          <div className={Style.container}>
            {file.contentType?.includes("video") ? (
              <>
                <div className={Style.playerOverlay} />
                <ReactPlayer width="100%" height="100%" url={mediaUrl} controls className={Style.player} />
              </>
            ) : (
              <img src={mediaUrl} className={Style.image} alt="uploaded media" />
            )}
          </div>
        </div>
      </div>
    );
  }

  return <></>;
};
export default DisplayMedia;
