import React, { useContext } from "react";
import DirectUpload, { UploadTypes } from "../DirectUpload";
import { UploadContext } from "../UploadContext";

type File = { url: string; filename: string; contentType: string };

interface UploadImageProps {
  onFileUpload: (file: File) => void;
  isMediaUploaded: boolean;
  directory: string;
  children: React.ReactNode;
  isDisabled?: boolean;
}

const UploadImage = ({ onFileUpload, isMediaUploaded, directory, children, isDisabled = false }: UploadImageProps) => {
  const { isUploading, uploadedFile, onUploadStart, onUploadSuccess, onUploadError, onUploadProgress } = useContext(
    UploadContext
  );
  const disabled = isMediaUploaded || isUploading || isDisabled;

  const onSuccess = (file, fileMeta) => {
    const newFile = onUploadSuccess(file, fileMeta);
    onFileUpload(newFile);
  };

  return (
    <DirectUpload
      onProgress={onUploadProgress}
      onError={onUploadError}
      preprocess={onUploadStart}
      onFinish={onSuccess}
      removeMargin
      noOpacityDisabled
      customDesign
      disabled={disabled}
      accept={UploadTypes.image}
      directory={directory}
    >
      {children}
    </DirectUpload>
  );
};

export default UploadImage;
