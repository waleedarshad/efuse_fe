import React, { createContext, forwardRef, useImperativeHandle, useState } from "react";
import { deleteFromS3 } from "../../store/actions/mediaActions";

export const UploadContext = createContext();

const WithUploadContext = ({ children }, ref) => {
  const DEFAULT_FILE_STATE = { url: "", filename: "", contentType: "" };

  const [uploadedFile, setUploadedFile] = useState(DEFAULT_FILE_STATE);
  const [fileKey, setFileKey] = useState("");
  const [uploadProgress, setUploadProgress] = useState(0);
  const [isUploading, setIsUploading] = useState(false);

  const onUploadStart = (fileToUpload, startCallback) => {
    setIsUploading(true);
    startCallback(fileToUpload);
  };

  const onUploadProgress = progress => {
    setUploadProgress(progress);
  };

  const onUploadSuccess = (file, fileMeta) => {
    const newFile = {
      url: `${file.signedUrl.split("/uploads")[0]}/${file.fileKey}`,
      filename: file.filename,
      contentType: fileMeta.type
    };

    setFileKey(file.fileKey);
    setIsUploading(false);
    setUploadProgress(0);
    setUploadedFile(newFile);
    return newFile;
  };

  const onUploadError = () => {
    clearState();
  };

  const cancelUpload = () => {
    if (fileKey) {
      deleteFromS3(fileKey);
    }

    clearState();
  };

  const clearState = () => {
    setUploadProgress(0);
    setIsUploading(false);
    setUploadedFile(DEFAULT_FILE_STATE);
    setFileKey("");
  };

  const contextProps = {
    uploadedFile,
    uploadProgress,
    isUploading,
    onUploadStart,
    onUploadSuccess,
    onUploadError,
    onUploadProgress,
    cancelUpload
  };

  useImperativeHandle(ref, () => contextProps);

  return <UploadContext.Provider value={contextProps}>{children}</UploadContext.Provider>;
};

export default forwardRef(WithUploadContext);
