import React, { Component } from "react";
import { faPhotoVideo } from "@fortawesome/pro-solid-svg-icons";
import PropTypes from "prop-types";
import ReactS3Uploader from "react-s3-uploader";
import axios from "axios";
import getConfig from "next/config";

import Style from "./DirectUpload.module.scss";
import { sendNotification } from "../../helpers/FlashHelper";
import EFPillButton from "../Buttons/EFPillButton/EFPillButton";

const { publicRuntimeConfig } = getConfig();
const { baseUrl } = publicRuntimeConfig;

export const UploadTypes = {
  applicationFiles: ".pdf, .gif, .png, .jpg, .jpeg, .mp4, .mov",
  image: ".gif, .png, .jpg, .jpeg",
  video: ".mp4, .mov",
  imageAndVideo: ".gif, .png, .jpg, .jpeg, .mp4, .mov"
};

class DirectUpload extends Component {
  inputRef = null;

  getSignedUrl = (file, callback) => {
    const { directory, onError } = this.props;
    axios
      .get(
        `/uploads/s3/sign?objectName=${encodeURIComponent(file.name)}&contentType=${encodeURIComponent(
          file.type
        )}&dir=${directory}`
      )
      .then(response => {
        callback(response.data);
      })
      .catch(error => {
        if (onError) {
          onError(error);
        } else {
          throw error;
        }
      });
  };

  initFileDialog = () => {
    this.inputRef.value = "";
    this.inputRef.click();
  };

  checkPreprocess = (fileToUpload, startCallback) => {
    const { preprocess, accept } = this.props;
    const fileName = fileToUpload?.name;
    // extract the extension from the file
    const ext = fileName?.substr(fileName?.lastIndexOf(".") + 1).toLowerCase();
    // check if extension exists inside the included list of extensions
    const hasExt = accept.includes(ext);
    // if extension exists, start the upload
    if (hasExt && preprocess) {
      this.props.preprocess(fileToUpload, startCallback);
    } else {
      // else, do not upload and dispatch an error
      sendNotification(
        `The file type was not accepted. The file must be one of the following: ${accept}`,
        "danger",
        "Error"
      );
    }
  };

  render() {
    const {
      accept,
      customDesign,
      dialogBtn,
      directory,
      disabled,
      noOpacityDisabled,
      onError,
      onFinish,
      onProgress,
      placeholder,
      removeMargin,
      theme
    } = this.props;
    return (
      <>
        {customDesign ? (
          <div
            className={`${Style.customDesign} ${disabled && !noOpacityDisabled && Style.disabled} ${disabled &&
              noOpacityDisabled &&
              Style.disabledNoOpacity}`}
            onClick={this.initFileDialog}
            onKeyPress={this.initFileDialog}
            role="button"
            tabIndex="-1"
          >
            {this.props.children}
          </div>
        ) : (
          <EFPillButton
            text={placeholder}
            icon={faPhotoVideo}
            onClick={this.initFileDialog}
            disabled={disabled}
            size="medium"
            className={`${Style.actionBtn} ${removeMargin && Style.removeMargin} ${Style[theme]} ${Style[dialogBtn]}`}
          />
        )}
        <ReactS3Uploader
          className={Style.directUploadInput}
          getSignedUrl={this.getSignedUrl}
          accept={accept}
          s3path={`/${directory}`}
          uploadRequestHeaders={{ "x-amz-acl": "public-read" }}
          contentDisposition="auto"
          server={baseUrl}
          scrubFilename={filename => filename.replace(/[^\w\d_\-.]+/gi, "")}
          inputRef={cmp => {
            this.inputRef = cmp;
          }}
          onProgress={onProgress}
          onError={onError}
          preprocess={this.checkPreprocess}
          onFinish={onFinish}
        />
      </>
    );
  }
}

DirectUpload.propTypes = {
  accept: PropTypes.string,
  customDesign: PropTypes.bool,
  directory: PropTypes.string,
  onError: PropTypes.func,
  onFinish: PropTypes.func,
  onProgress: PropTypes.func,
  placeholder: PropTypes.string,
  preprocess: PropTypes.func,
  removeMargin: PropTypes.bool
};

DirectUpload.defaultProps = {
  accept: UploadTypes.imageAndVideo,
  customDesign: false,
  directory: "uploads/media/",
  onError: null,
  onFinish: null,
  onProgress: null,
  placeholder: "Photo/Video",
  preprocess: null,
  removeMargin: false
};

export default DirectUpload;
