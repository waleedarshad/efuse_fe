import React, { useContext, useEffect, useState } from "react";
import ComfyJS, { OnMessageFlags } from "comfy.js";
import { useLazyQuery } from "@apollo/client";

import { GraphqlContext } from "../../GraphqlContext/GraphqlContext";
import { IClippyCommand, IClippyResponse, IClippyTwitchClip } from "../../../interfaces";
import { ClippyAllowedRolesEnum, ClippyCommandKindEnum } from "../../../enums";
import { GetShoutoutClip } from "../../../graphql/clippy/GetShoutoutClip";
import { GetSpecificClip } from "../../../graphql/clippy/GetSpecificClip";
import EFClippyShoutout from "../EFClippyShoutout/EFClippyShoutout";
import Style from "./Shoutout.module.scss";

interface ShoutoutProps {
  token: string | string[];
}

const Shoutout: React.FC<ShoutoutProps> = ({ token }) => {
  const clippyQueryData = useContext<{ data: { getClippyByToken: IClippyResponse } }>(GraphqlContext);

  const clippyObject = clippyQueryData.data?.getClippyByToken;

  const [getShoutoutClip, shoutoutQueryData] = useLazyQuery<{ getShoutoutClip: IClippyTwitchClip }>(GetShoutoutClip, {
    fetchPolicy: "no-cache"
  });

  const shoutoutClipObject = shoutoutQueryData.data?.getShoutoutClip;

  const [getSpecificClip, specificClipQueryData] = useLazyQuery<{ getSpecificClip: IClippyTwitchClip }>(
    GetSpecificClip,
    { fetchPolicy: "no-cache" }
  );

  const specificClipObject = specificClipQueryData.data?.getSpecificClip;

  const [shoutoutConfig, setShoutoutConfig] = useState({
    activeClip: null,
    lastActiveClip: null,
    muted: false,
    queue: [],
    shoutoutCommand: null,
    lastUrlPosted: null
  });

  const validateFlags = (flags: OnMessageFlags, commandConfig: IClippyCommand) => {
    if (flags.broadcaster) {
      return true;
    }

    const { allowedRoles } = commandConfig;

    if (flags.mod && allowedRoles.includes(ClippyAllowedRolesEnum.MODERATOR)) {
      return true;
    }

    if (allowedRoles.includes(ClippyAllowedRolesEnum.EVERYONE)) {
      return true;
    }

    return Object.keys(flags).some(flag => flags[flag] === true && allowedRoles.includes(flag.toUpperCase()));
  };

  const isValidClipUrl = (url: string) =>
    url.includes("clips.twitch.tv") || (url.includes("twitch.tv") && url.includes("/clip"));

  const handleShoutoutCommand = (commandConfig: IClippyCommand, message: string, flags: OnMessageFlags) => {
    if (validateFlags(flags, commandConfig)) {
      const channelName = message[0] === "@" ? message.substring(1) : message;

      getShoutoutClip({ variables: { token, commandId: commandConfig._id, channelName } });
    }
  };

  const handleWatchCommand = (commandConfig: IClippyCommand, message: string, flags: OnMessageFlags) => {
    if (validateFlags(flags, commandConfig)) {
      if (!message && shoutoutConfig.lastUrlPosted) {
        getSpecificClip({ variables: { token, url: shoutoutConfig.lastUrlPosted } });
      } else if (isValidClipUrl(message)) {
        getSpecificClip({ variables: { token, url: message } });
      }
    }
  };

  const handleMuteCommand = (commandConfig: IClippyCommand, message: string, flags: OnMessageFlags) => {
    if (validateFlags(flags, commandConfig)) {
      setShoutoutConfig({ ...shoutoutConfig, muted: true });
    }
  };

  const handleUnmuteCommand = (commandConfig: IClippyCommand, message: string, flags: OnMessageFlags) => {
    if (validateFlags(flags, commandConfig)) {
      setShoutoutConfig({ ...shoutoutConfig, muted: false });
    }
  };

  const handleStopCommand = (commandConfig: IClippyCommand, message: string, flags: OnMessageFlags) => {
    if (validateFlags(flags, commandConfig)) {
      setShoutoutConfig({ ...shoutoutConfig, muted: false, activeClip: null });
    }
  };

  const handleStopAllCommand = (commandConfig: IClippyCommand, message: string, flags: OnMessageFlags) => {
    if (validateFlags(flags, commandConfig)) {
      setShoutoutConfig({ ...shoutoutConfig, muted: false, activeClip: null, queue: [] });
    }
  };

  const handleReplayCommand = (commandConfig: IClippyCommand, message: string, flags: OnMessageFlags) => {
    if (validateFlags(flags, commandConfig)) {
      setShoutoutConfig({ ...shoutoutConfig, activeClip: shoutoutConfig.lastActiveClip, muted: false });

      const video = document.querySelector("video");

      if (video) {
        video.currentTime = 0;
      }
    }
  };

  const handleResetCommand = (commandConfig: IClippyCommand, message: string, flags: OnMessageFlags) => {
    if (validateFlags(flags, commandConfig)) {
      if (window) {
        window.location.reload();
      }
    }
  };

  const handleVideoEnded = () => {
    const queue = [...shoutoutConfig.queue];

    const activeClip = queue.length > 0 ? queue.shift() : null;

    setShoutoutConfig({
      ...shoutoutConfig,
      activeClip,
      muted: false,
      queue,
      lastActiveClip: shoutoutConfig.activeClip
    });
  };

  const addClipToState = (clip: IClippyTwitchClip) => {
    // Check if queuing is enabled
    if (shoutoutConfig.shoutoutCommand?.queueClips && shoutoutConfig.activeClip) {
      setShoutoutConfig({
        ...shoutoutConfig,
        queue: [...shoutoutConfig.queue, clip]
      });
    } else {
      setShoutoutConfig({
        ...shoutoutConfig,
        activeClip: clip,
        lastActiveClip: shoutoutConfig.activeClip || clip,
        muted: false
      });
    }
  };

  // CDM => Init Comfy.JS & segments analytics
  useEffect(() => {
    document.body.style.backgroundColor = "transparent";
    if (clippyObject) {
      const { user, commands } = clippyObject;

      // Identify user for tracking
      analytics.identify(user._id);

      // Track page
      analytics.page("Clippy Shoutout Page");

      // Find the shoutout command
      const shoutoutCommand = commands.find(c => c.kind === ClippyCommandKindEnum.SHOUTOUT);

      // Set the shoutout command in state
      if (shoutoutCommand) {
        setShoutoutConfig({ ...shoutoutConfig, shoutoutCommand });
      }

      // Init ComfyJS
      ComfyJS.Init(user.twitch.username);
    }

    return () => {
      // Make sure to Disconnect when we have some data, because client is configured after we get the data
      if (clippyObject) {
        ComfyJS.Disconnect();
      }
      document.body.style.backgroundColor = "#fff";
    };
  }, []);

  // Setting up commands outside useEffect, otherwise onCommand loses access to react-states
  if (clippyObject) {
    // eslint-disable-next-line consistent-return
    ComfyJS.onCommand = (_, incomingCommand, message, flags) => {
      const { commands } = clippyObject;

      const commandConfig = commands.find((c: IClippyCommand) => c.command === incomingCommand);

      if (commandConfig) {
        analytics.track(`CLIPPY_COMMAND_${commandConfig.kind}`, { flags, message });
      }

      switch (commandConfig?.kind) {
        case ClippyCommandKindEnum.MUTE:
          return handleMuteCommand(commandConfig, message, flags);
        case ClippyCommandKindEnum.RESET:
          return handleResetCommand(commandConfig, message, flags);
        case ClippyCommandKindEnum.REPLAY:
          return handleReplayCommand(commandConfig, message, flags);
        case ClippyCommandKindEnum.SHOUTOUT:
          return handleShoutoutCommand(commandConfig, message, flags);
        case ClippyCommandKindEnum.STOP:
          return handleStopCommand(commandConfig, message, flags);
        case ClippyCommandKindEnum.STOP_ALL:
          return handleStopAllCommand(commandConfig, message, flags);
        case ClippyCommandKindEnum.UNMUTE:
          return handleUnmuteCommand(commandConfig, message, flags);
        case ClippyCommandKindEnum.WATCH:
          return handleWatchCommand(commandConfig, message, flags);
        default:
          console.error("Command not found");
          break;
      }
    };

    // Setup chat listener
    ComfyJS.onChat = (_, message) => {
      if (isValidClipUrl(message)) {
        setShoutoutConfig({ ...shoutoutConfig, lastUrlPosted: message });
      }
    };
  }

  // Set Shoutout Clip
  useEffect(() => {
    // TODO: Error Handling
    if (shoutoutClipObject) {
      addClipToState(shoutoutClipObject);
    }
  }, [shoutoutClipObject]);

  // Set Specific Clip
  useEffect(() => {
    // TODO: Error Handling
    if (specificClipObject) {
      addClipToState(specificClipObject);
    }
  }, [specificClipObject]);

  const { activeClip, muted } = shoutoutConfig;

  return (
    <div className={Style.mainContainer}>
      {activeClip && (
        <div className={Style.videoContainer}>
          <EFClippyShoutout
            topText={`Go check out ${activeClip?.broadcaster_name}!`}
            muted={muted}
            handleVideoEnded={handleVideoEnded}
            videoUrl={activeClip?.mp4_url}
            clipTitle={activeClip?.title}
            clipGameTile={activeClip?.game?.name}
          />
        </div>
      )}
    </div>
  );
};

export default Shoutout;
