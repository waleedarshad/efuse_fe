import React from "react";
import ReactPlayer from "react-player";
import styled from "styled-components";

const EFClippyShoutout = ({ topText, muted, handleVideoEnded, videoUrl, clipTitle, clipGameTile }) => {
  return (
    <ShoutoutContainer>
      <TopTextContainer>{topText}</TopTextContainer>
      <VideoContainer>
        <ReactPlayer
          url={videoUrl}
          playing
          muted={muted}
          config={{ file: { forceVideo: true } }}
          width={760}
          height={428}
          onEnded={handleVideoEnded}
        />
        <VideoOverlay>
          <img src="https://cdn.efuse.gg/static/images/clippy_landing_pages/logo.svg" alt="clippy logo" />
          <span>Powered by eFuse</span>
        </VideoOverlay>
      </VideoContainer>
      <FooterContainer>
        {clipTitle && <TitleTag>{clipTitle}</TitleTag>}
        {clipGameTile && <GameTag>{clipGameTile}</GameTag>}
      </FooterContainer>
    </ShoutoutContainer>
  );
};

const ShoutoutContainer = styled.div`
  background-color: #000000;
  border-radius: 8px;
  box-shadow: 0 1px 1px rgb(0 0 0 / 25%), 0 2px 2px rgb(0 0 0 / 25%), 0 4px 4px rgb(0 0 0 / 25%),
    0 8px 8px rgb(0 0 0 / 25%), 0 16px 16px rgb(0 0 0 / 25%);
  width: fit-content;
`;

const TopTextContainer = styled.div`
  color: #ffffff;
  text-align: center;
  font-weight: 600;
  padding: 8px;
  margin: 0px;
  font-family: Poppins;
  font-style: normal;
  font-size: 18px;
  line-height: 24px;
  text-align: center;
`;

const VideoContainer = styled.div`
  position: relative;
`;

const VideoOverlay = styled.div`
  position: absolute;
  bottom: 0;
  width: 100%;
  background: linear-gradient(180deg, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.3), #000000);
  color: #ffffff;
  text-align: left;
  padding: 10px;
  padding-top: 15px;
  display: flex;
  align-items: center;

  & > img {
    height: 24px;
    margin-right: 10px;
  }

  & > span {
    font-family: Poppins;
    font-style: normal;
    font-weight: normal;
    font-size: 12px;
    line-height: 20px;
    text-shadow: 0px 1px 4px rgba(0, 0, 0, 0.25);
  }
`;

const FooterContainer = styled.div`
  display: flex;
  margin: 10px 9px;
`;

const TitleTag = styled.div`
  font-family: Poppins;
  font-style: normal;
  font-weight: 600;
  font-size: 12px;
  line-height: 20px;
  color: #ffffff;
  text-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
  margin-right: 10px;
`;

const GameTag = styled.div`
  font-family: Poppins;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 20px;
  color: #ffffff;
  text-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
`;

export default EFClippyShoutout;
