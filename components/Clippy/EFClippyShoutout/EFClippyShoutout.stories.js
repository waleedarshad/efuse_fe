import EFClippyShoutout from "./EFClippyShoutout";

export default {
  title: "Clippy/EFClippyShoutout",
  component: EFClippyShoutout,
  argTypes: {
    topText: {
      type: "text"
    },
    videoUrl: {
      type: "text"
    },
    clipTitle: {
      type: "text"
    },
    clipGameTile: {
      type: "text"
    }
  }
};

const Story = args => <EFClippyShoutout {...args} />;

export const BasicShoutout = Story.bind({});

BasicShoutout.args = {
  topText: "Go check out Blevishkin!",
  videoUrl: "https://scontent.efcdn.io/media/video/efuse-home-page-video.m4v",
  clipTitle: "DMR meta?",
  clipGameTile: "Call of Duty: Warzone",
  handleVideoEnded: ()=>{},
};
