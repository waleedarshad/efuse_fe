import { FormControl, InputGroup } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/pro-regular-svg-icons";
import Style from "./SearchField.module.scss";

const SearchField = props => (
  <div className={Style.searchFieldWrapper}>
    <InputGroup>
      <InputGroup.Prepend>
        <span className={Style.searchIcon}>
          <FontAwesomeIcon icon={faSearch} />
        </span>
      </InputGroup.Prepend>
      <FormControl {...props} className={Style.searchField} />
    </InputGroup>
  </div>
);

export default SearchField;
