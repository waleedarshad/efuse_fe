/* eslint-disable react/no-array-index-key */
import React from "react";
import Style from "./TraitsMotivationsSelection.module.scss";

const TraitsMotivationsSelection = ({ optionClicked, questions, questionArrayName }) => (
  <div className={Style.questionList}>
    {questions.map((question, index) => (
      <>
        {index === 0 && <div className={Style.instructions}>Answer the required questions below</div>}
        <div key={index} className={Style.singleQuestionContainer}>
          <div className={Style.question}>{question.question}</div>
          <div className={Style.multipleAnswerContainer}>
            <div className={Style.answerInstructions}>
              {question.type === "single-answer" && <>Select one</>}
              {question.type === "multi-answer" && <>Select all that apply</>}
            </div>
            {question.options.map((option, optionIndex) => (
              <button
                type="button"
                onClick={e => {
                  optionClicked(questionArrayName, option.value, question.id, e);
                }}
                className={option.selected ? Style.activeSpan : ""}
                key={optionIndex}
              >
                {option.label}
              </button>
            ))}
          </div>
          {question.validationError && (
            <div className={Style.validationErrorMessage}>
              {question.type === "single-answer" && <>Please select one option.</>}
              {question.type === "multi-answer" && <>Please select at least one option.</>}
            </div>
          )}
        </div>
      </>
    ))}
  </div>
);

export default TraitsMotivationsSelection;
