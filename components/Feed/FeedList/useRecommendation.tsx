import { useSelector } from "react-redux";
import { useEffect, useRef } from "react";

const useRecommendation = (): [[string], number] => {
  const recommendationUsersEnabled = useSelector(state => state.features.recommendations_users);
  const recommendationLearningEnabled = useSelector(state => state.features.recommendations_learning);
  const recommendationOpportunitiesEnabled = useSelector(state => state.features.recommendations_opportunities);
  const recommendationOrganizationsEnabled = useSelector(state => state.features.recommendations_organization);

  const recommendations = useRef([]);
  const startingRecommendationIndex = useRef(0);

  useEffect(() => {
    if (recommendationUsersEnabled) {
      recommendations.current.push("users");
    }
    if (recommendationLearningEnabled) {
      recommendations.current.push("learning");
    }
    if (recommendationOrganizationsEnabled) {
      recommendations.current.push("organizations");
    }
    if (recommendationOpportunitiesEnabled) {
    }
    recommendations.current.push("opportunities");

    startingRecommendationIndex.current = Math.floor(Math.random() * recommendations.current.length);
  }, []);

  return [recommendations.current, startingRecommendationIndex.current];
};

export default useRecommendation;
