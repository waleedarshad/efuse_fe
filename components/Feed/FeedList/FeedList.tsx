import { useSelector } from "react-redux";
import React, { Fragment, useContext } from "react";
import useRecommendation from "./useRecommendation";
import SkeletonLoader from "../../SkeletonLoader/SkeletonLoader";
import EFLazyLoad from "../../EFLazyLoad/EFLazyLoad";
import Post from "../Post/Post";
import Recommendation from "../../Recommendation/Recommendation";
import style from "./FeedList.module.scss";
import { FeedContext } from "../FeedProvider";
import ErrorBoundary from "../../ErrorBoundary/ErrorBoundary";
import EmptyComponent from "../../EmptyComponent/EmptyComponent";

const RECOMMENDATION_FREQUENCY = 9;

const FeedList = () => {
  const { feedType, setPostToExpand } = useContext(FeedContext);

  const [recommendations, startingRecommendationIndex] = useRecommendation();

  const posts = useSelector(state => state.feed.posts);
  const isGettingFeed = useSelector(state => state.feed.isGettingFeed);

  const renderRecommendations = index => {
    if (index % RECOMMENDATION_FREQUENCY === RECOMMENDATION_FREQUENCY - 1) {
      const recommendationType =
        recommendations[
          (startingRecommendationIndex + Math.floor(index / RECOMMENDATION_FREQUENCY)) % recommendations.length
        ];

      return <Recommendation type={recommendationType} key={index} />;
    }
    return <></>;
  };

  if (posts.length === 0 && isGettingFeed === false) {
    return <EmptyComponent text="No Post Yet" />;
  }

  if (posts.length === 0) {
    return <SkeletonLoader skeletons={5} postHeight={160} engagementHeight={40} />;
  }

  return (
    <div className={style.feedList}>
      {posts.map((post, index) => (
        <Fragment key={post.parentFeedId}>
          <EFLazyLoad offset={200} style={{ marginBottom: "1em" }}>
            <ErrorBoundary>
              <Post post={post} postType={feedType} onViewPostClicked={setPostToExpand} />
            </ErrorBoundary>
          </EFLazyLoad>
          {renderRecommendations(index)}
        </Fragment>
      ))}
    </div>
  );
};

export default FeedList;
