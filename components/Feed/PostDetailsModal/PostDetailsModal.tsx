import { useRouter } from "next/router";
import React, { useContext, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useLazyQuery } from "@apollo/client";
import cloneDeep from "lodash/cloneDeep";
import { FeedContext } from "../FeedProvider";
import Post from "../Post/Post";
import EFModal from "../../Modals/EFModal/EFModal";
import {
  GET_LOUNGE_FEED_POST,
  GetLoungeFeedPostData,
  GetLoungeFeedPostVars
} from "../../../graphql/feeds/LoungeFeedPostQuery";
import { GET_HOME_FEED_POST, GetHomeFeedPostData, GetHomeFeedPostVars } from "../../../graphql/feeds/HomeFeedPostQuery";
import Style from "./PostDetailsModal.module.scss";
import { setFeedpostToExpand } from "../../../store/actions/feedActions";
import { toggleCommentCreation } from "../../../store/actions/comment/commentActions";
import { Post as PostType } from "../../../graphql/feeds/Models";

const PostDetailsModal = () => {
  const router = useRouter();
  const dispatch = useDispatch();
  const { postToExpand, setPostToExpand, feedType } = useContext(FeedContext);
  const [isPostNotFound, setIsPostNotFound] = useState<boolean>(false);
  const expanededPost = useSelector(state => state.feed.postToExpand);

  const displayPost: PostType = postToExpand || expanededPost;
  const [getLoungePost, { data: loungePostData }] = useLazyQuery<GetLoungeFeedPostData, GetLoungeFeedPostVars>(
    GET_LOUNGE_FEED_POST,
    {
      fetchPolicy: "no-cache",
      onCompleted: () => {
        analytics.track("LOUNGE_VIEW", {
          loungeFeedId: loungePostData?.getLoungeFeedPost.feedId
        });
        const post = cloneDeep(loungePostData.getLoungeFeedPost);
        post.isNewPost = true;
        post.enableCommentCreation = true;
        dispatch(setFeedpostToExpand(post));
        dispatch(toggleCommentCreation(post.feedId, "enable"));
      },
      onError: () => setIsPostNotFound(true)
    }
  );

  const [getHomePost, { data: homePostData }] = useLazyQuery<GetHomeFeedPostData, GetHomeFeedPostVars>(
    GET_HOME_FEED_POST,
    {
      fetchPolicy: "no-cache",
      onCompleted: () => {
        analytics.track("POST_VIEW", {
          postId: homePostData?.gethomeFeedPost.feedId
        });
        const post = cloneDeep(homePostData.gethomeFeedPost);
        post.isNewPost = true;
        post.enableCommentCreation = true;
        dispatch(setFeedpostToExpand(post));
        dispatch(toggleCommentCreation(post.feedId, "enable"));
      },
      onError: () => setIsPostNotFound(true)
    }
  );

  useEffect(() => {
    if (postToExpand) {
      dispatch(setFeedpostToExpand(postToExpand));
      dispatch(toggleCommentCreation(postToExpand.feedId, "enable"));
    }
  }, [JSON.stringify(postToExpand)]);

  const feedId = Array.isArray(router.query.feedId) ? router.query.feedId[0] : router.query.feedId;

  useEffect(() => {
    if (feedId && !displayPost) {
      if (feedType === "following") {
        getHomePost({ variables: { parentFeedId: feedId } });
      } else {
        getLoungePost({ variables: { parentFeedId: feedId } });
      }
    }
  }, [feedId]);

  const onModalOpen = () => {
    if (displayPost && !router.asPath.includes(displayPost.parentFeedId) && feedType !== "profile") {
      router.replace({ pathname: router.asPath, query: { feedId: displayPost.parentFeedId } }, undefined, {
        shallow: true
      });
    }
  };

  const onModalClose = () => {
    setPostToExpand(null);
    dispatch(setFeedpostToExpand(null));
    setIsPostNotFound(false);
    if (feedType !== "profile") {
      router.replace(window.location.pathname, undefined, { shallow: true });
    }
  };

  return (
    <EFModal
      isOpen={!!displayPost || isPostNotFound}
      onOpen={onModalOpen}
      onClose={onModalClose}
      widthTheme={isPostNotFound ? "small" : "medium"}
      displayCloseButton={false}
      allowBackgroundClickClose
      allowCloseOnEscape
    >
      {displayPost && (
        <Post
          post={expanededPost}
          postType={feedType}
          onViewPostClicked={() => {
            if (expanededPost) {
              analytics.track("POST_VIEW", {
                postId: expanededPost.feedId
              });
            }
          }}
        />
      )}
      {isPostNotFound && (
        <div className={Style.noRecordFound}>
          <p>This post is no longer available</p>
        </div>
      )}
    </EFModal>
  );
};

export default PostDetailsModal;
