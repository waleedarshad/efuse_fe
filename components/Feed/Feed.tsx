import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import InfiniteScroll from "react-infinite-scroller";
import uniqueId from "lodash/uniqueId";
import FeedList from "./FeedList/FeedList";
import AnimatedLogo from "../AnimatedLogo";
import { getMediaSettings } from "../../store/actions/userSettingActions";
import { clearFeed } from "../../store/actions/feedActions";
import FeedProvider from "./FeedProvider";
import PostDetailsModal from "./PostDetailsModal/PostDetailsModal";
import EditPostModal from "../Modals/EditPostModal/EditPostModal";
import Style from "./Feed.module.scss";
import { FeedType } from "./types";

interface FeedProps {
  feedType: FeedType;
  getFeed: (page: number) => void;
}

const Feed = ({ feedType, getFeed }: FeedProps) => {
  const dispatch = useDispatch();
  const pagination = useSelector(state => state.feed.pagination);

  useEffect(() => {
    dispatch(clearFeed());
    getFeed(1);
    dispatch(getMediaSettings());
    return () => {
      dispatch(clearFeed());
    };
  }, []);

  const getMoreFeed = () => {
    if (pagination?.hasNextPage) {
      getFeed(pagination.page + 1);
    }
  };

  return (
    <FeedProvider feedType={feedType}>
      <InfiniteScroll
        pageStart={1}
        loadMore={getMoreFeed}
        hasMore={pagination?.hasNextPage}
        loader={
          // using uniqueId to remove warning i.e each child in a list should have a unique "key" prop.
          <span className={Style.center} key={uniqueId()}>
            <AnimatedLogo key={0} theme="content" />
          </span>
        }
      >
        <FeedList />
      </InfiniteScroll>
      <PostDetailsModal />
      <EditPostModal />
    </FeedProvider>
  );
};

export default Feed;
