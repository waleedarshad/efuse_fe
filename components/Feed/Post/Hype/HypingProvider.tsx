import { createContext, useState, useEffect, ReactNode, SetStateAction, Dispatch } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useMutation } from "@apollo/client";
import times from "lodash/times";
import HYPE_MUTATION from "./HypeMutation";
import FEATURE_FLAGS from "../../../../common/featureFlags";
import { hypePost } from "../../../../store/actions/feedActions";
import { Post } from "../../../../graphql/feeds/Models";

interface HypingContextProps {
  isTimerDisplayed: boolean;
  setIsTimerDisplayed: Dispatch<SetStateAction<boolean>>;
  isTimerRunning: boolean;
  setIsTimerRunning: Dispatch<SetStateAction<boolean>>;
  newUserHypes: number;
  setNewUserHypes: Dispatch<SetStateAction<number>>;
  onTimerEnd: () => void;
}
export const HypingContext = createContext<HypingContextProps>(null);

interface HypingProviderProps {
  post: Post;
  children: ReactNode;
}

const HypingProvider = ({ post, children }: HypingProviderProps) => {
  const dispatch = useDispatch();
  const enableHypeMutation = useSelector(state => state.features[FEATURE_FLAGS.ENABLE_HYPE_MUTATION]);

  const [isTimerDisplayed, setIsTimerDisplayed] = useState<boolean>(false);
  const [isTimerRunning, setIsTimerRunning] = useState<boolean>(false);
  const [newUserHypes, setNewUserHypes] = useState<number>(0);

  const trackHype = () => {
    analytics.track("POST_HYPE_SESSION");
    times(newUserHypes, () => analytics.track("POST_HYPE"));
  };

  const [hypeFeed] = useMutation(HYPE_MUTATION, { onCompleted: trackHype, onError: trackHype });

  useEffect(() => {
    setNewUserHypes(0);
  }, []);

  useEffect(() => {
    setNewUserHypes(0);
  }, [post.currentUserHypes]);

  useEffect(() => {
    if (!isTimerRunning) {
      setTimeout(() => setIsTimerDisplayed(false), 1000);
    }
  }, [isTimerRunning]);

  const onTimerEnd = () => {
    setIsTimerRunning(false);

    enableHypeMutation
      ? hypeFeed({ variables: { hypeFeedFeedId: post.feedId, hypeFeedHypeCount: newUserHypes } })
      : dispatch(hypePost(post.feedId, newUserHypes));
  };

  const contextProps = {
    isTimerDisplayed,
    setIsTimerDisplayed,
    isTimerRunning,
    setIsTimerRunning,
    newUserHypes,
    setNewUserHypes,
    onTimerEnd
  };

  return <HypingContext.Provider value={contextProps}>{children}</HypingContext.Provider>;
};

export default HypingProvider;
