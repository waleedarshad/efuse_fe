import { gql } from "@apollo/client";

const HYPE_MUTATION = gql`
  mutation HypeFeedMutation($hypeFeedFeedId: String!, $hypeFeedHypeCount: Int!) {
    hypeFeed(feedId: $hypeFeedFeedId, hypeCount: $hypeFeedHypeCount) {
      newHype
      hype {
        objId
        hypeCount
        objType
        platform
      }
    }
  }
`;

export default HYPE_MUTATION;
