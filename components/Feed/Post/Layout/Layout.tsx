import { ReactNode } from "react";
import EFCard from "../../../Cards/EFCard/EFCard";

interface LayoutProps {
  children: ReactNode;
}
const Layout = ({ children }: LayoutProps) => {
  return (
    <EFCard widthTheme="fullWidth" overflowTheme="visible">
      {children}
    </EFCard>
  );
};

export default Layout;
