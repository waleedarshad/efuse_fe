import Header from "./Header/Header";
import Content from "./Content/Content";
import ActionButtons from "./ActionButtons/ActionButtons";
import PostProvider from "./PostProvider";
import Layout from "./Layout/Layout";
import EngagementCounters from "./EngagementCounters/EngagementCounters";
import { Post as IPost } from "../../../graphql/feeds/Models";
import { FeedType } from "../types";
import Comments from "./Comments/Comments";
import HypePostModal from "./HypeModal/HypePostModal";
import ReportPostModal from "./ReportPostModal/ReportPostModal";
import TopComment from "./TopComment/TopComment";
import FeatureFlag from "../../General/FeatureFlag/FeatureFlag";
import FEATURE_FLAGS from "../../../common/featureFlags";
import FeatureFlagVariant from "../../General/FeatureFlag/FeatureFlagVariant/FeatureFlagVariant";

interface PostProps {
  post: IPost;
  postType: FeedType;
  onViewPostClicked: (Post) => void;
  onReportPostClicked: (Post) => void;
}

const Post = ({ post, postType, onViewPostClicked }: PostProps) => {
  return (
    <PostProvider postType={postType} post={post} onViewPostClicked={onViewPostClicked}>
      <Layout>
        <Header />
        <Content />
        <EngagementCounters />
        <ActionButtons />
        {post.loadHypeUser && <HypePostModal />}
        <FeatureFlag name={FEATURE_FLAGS.TOP_COMMENT_WEB}>
          <FeatureFlagVariant flagState>
            {post.topComment && !post.enableCommentCreation && <TopComment comment={post.topComment} />}
          </FeatureFlagVariant>
        </FeatureFlag>
        {post.enableCommentCreation && <Comments />}
        <ReportPostModal />
      </Layout>
    </PostProvider>
  );
};

export default Post;
