import { Image } from "react-bootstrap";
import TimeAgo from "react-timeago";
import { useContext } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircle } from "@fortawesome/pro-solid-svg-icons";
import { useDispatch } from "react-redux";
import Style from "./TopComment.module.scss";
import { CommentModel } from "../../../../graphql/comments/Models";
import UserLink from "../../../UserLink/UserLink";
import VerifiedIcon from "../../../VerifiedIcon/VerifiedIcon";
import EFStreakBadge from "../../../EFStreakBadge/EFStreakBadge";
import CommentContent from "../Comments/CommentList/Comment/CommentBody/CommentContent/CommentContent";
import DisplayMedia from "../Comments/CommentMedia/DisplayMedia/DisplayMedia";
import EFAvatar from "../../../EFAvatar/EFAvatar";
import { rescueNil } from "../../../../helpers/GeneralHelper";
import CommentHypeCounter from "../Comments/CommentList/Comment/CommentActions/CommentHypeCounter/CommentHypeCounter";
import { PostContext } from "../PostProvider";
import PostButton from "../PostButton/PostButton";
import { toggleCommentCreation } from "../../../../store/actions/comment/commentActions";
import WithUploadContext from "../../../DirectUpload/UploadContext";

const TopComment = ({ comment }: { comment: CommentModel }) => {
  const dispatch = useDispatch();

  const { user } = comment;
  const dailyStreak = user?.streak;

  const { post } = useContext(PostContext);

  const { media, giphy } = comment;

  const giphyUrl = giphy?.images?.fixed_height_downsampled?.url;

  const profilePicture = rescueNil(user, "profileImage");

  return (
    <div className={Style.wrapper}>
      <div className={Style.line}>
        <div className={Style.heading}>
          <b>Top Comment</b>
        </div>
      </div>

      <div
        className={Style.commentWrapper}
        onClick={() => {
          analytics.track("TOP_COMMENT_PRESSED");
          dispatch(toggleCommentCreation(post.feedId));
        }}
        aria-hidden="true"
      >
        <div className={`mx-4 ${Style.topComment} `}>
          <div className={Style.imageWidth}>
            <EFAvatar
              displayOnlineButton={false}
              profilePicture={profilePicture}
              size="extraSmall"
              href={`/u/${user?.username}`}
            />
          </div>
          <div className={`${Style.comment}`}>
            <h6 className={Style.name}>
              <div className={Style.usernameLink}>
                <UserLink user={user} />
              </div>
              <div className={Style.verifiedIcon}>{user.verified && <VerifiedIcon />}</div>
              <div className={`${!user.verified && Style.streak}`}>
                <EFStreakBadge streak={dailyStreak} />
              </div>
            </h6>
            <p className={Style.username}>
              @{user.username}
              <span className={Style.options}>
                <TimeAgo date={comment.createdAt} minPeriod={30} />
              </span>
            </p>
          </div>
        </div>

        <div className={Style.commentContent}>
          {media && (
            <WithUploadContext>
              <DisplayMedia media={media} />
            </WithUploadContext>
          )}
          {giphy && giphyUrl && <Image src={giphyUrl} alt="Giphy" className="pt-2 pb-2" />}
          <CommentContent comment={comment} />
        </div>

        <div className={Style.engagementCounters}>
          <div className={Style.countersContainer}>
            <CommentHypeCounter numberOfHypes={comment.likes} comment={comment} />
            <FontAwesomeIcon className={Style.dot} icon={faCircle} />

            <PostButton onClick={() => {}}>{`${comment?.thread?.totalDocs || 0} Comment${
              comment.thread.totalDocs >= 1 ? "" : "s"
            }`}</PostButton>
          </div>
        </div>
        <div className={Style.separator} />
      </div>
    </div>
  );
};

export default TopComment;
