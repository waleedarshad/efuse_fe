import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { IconProp } from "@fortawesome/fontawesome-svg-core";
import PostButton, { PostButtonProps } from "./PostButton";
import Style from "./PostButtonWithIcon.module.scss";

interface PostButtonWithIconProps extends Pick<PostButtonProps, "onClick" | "isDisabled" | "highlightColor"> {
  text: string | any;
  icon: IconProp;
}

const PostButtonWithIcon = ({ icon, text, onClick, isDisabled, highlightColor }: PostButtonWithIconProps) => {
  return (
    <PostButton onClick={onClick} isDisabled={isDisabled} highlightColor={highlightColor}>
      <FontAwesomeIcon icon={icon} className={Style.icon} />
      {text}
    </PostButton>
  );
};

export default PostButtonWithIcon;
