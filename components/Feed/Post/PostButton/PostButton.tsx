import React, { ReactNode } from "react";
import Style from "./PostButton.module.scss";

export interface PostButtonProps {
  children: ReactNode;
  onClick: () => void;
  isDisabled?: boolean;
  highlightColor?: "blue" | "orange";
}
const PostButton = ({ children, onClick, isDisabled = false, highlightColor = "blue" }: PostButtonProps) => {
  return (
    <button
      type="button"
      className={`${Style.postButton} ${Style[highlightColor]}`}
      onClick={onClick}
      disabled={isDisabled}
    >
      <div className={Style.content}>{children}</div>
    </button>
  );
};

export default PostButton;
