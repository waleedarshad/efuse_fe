import React, { useContext, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Mention, MentionsInput } from "react-mentions";
import Form from "react-bootstrap/Form";
import { FormGroup } from "react-bootstrap";
import isEmpty from "lodash/isEmpty";
import isEqual from "lodash/isEqual";
import Style from "./CommentForm.module.scss";
import EFAvatar from "../../../../EFAvatar/EFAvatar";
import { getImage, rescueNil, setMentionInfo } from "../../../../../helpers/GeneralHelper";
import { globalSearchFollowersToMention } from "../../../../../store/actions/followerActions";
import { renderSuggestion } from "../../../../../helpers/PostHelper";
import CommentMedia from "../CommentMedia/CommentMedia";
import { CommentModel } from "../../../../../graphql/comments/Models";
import { addComment, updateComment } from "../../../../../store/actions/comment/commentActions";
import { PostContext } from "../../PostProvider";
import { PostCommentBody } from "../../../../../graphql/comments/CommentQuery";
import { Mention as IMention } from "../../../../../graphql/interface/Mention";
import { File } from "../../../../../graphql/interface/File";
import { Giphy } from "../../../../../graphql/interface/Giphy";
import WithUploadContext from "../../../../DirectUpload/UploadContext";

const CommentForm = ({ comment, parent }: { comment?: CommentModel; parent?: string }) => {
  const { post } = useContext(PostContext);
  const dispatch = useDispatch();
  const platform = "web";

  const [content, setContent] = useState("");
  const [mentions, setMentions] = useState<IMention[]>([]);

  const [uploadedFile, setUploadedFile] = useState<File>({});
  const [selectedGif, setSelectedGif] = useState<Giphy>({});

  const followers = useSelector(state => state.followers.mentionableFollowers);
  const { currentUser } = useSelector(state => state.auth);

  useEffect(() => {
    if (comment) {
      setContent(comment.content || "");
      setMentions(comment.mentions || []);

      if (!isEmpty(comment.giphy)) {
        setSelectedGif(comment.giphy);
      }

      if (!isEmpty(comment.media)) {
        setUploadedFile({ ...comment.media?.file, edit: true });
      }
    }
  }, []);

  const onChange = (event, newValue, newPlainTextValue, newMentions) => {
    if (newMentions && !isEmpty(newMentions)) {
      setMentions(setMentionInfo(newMentions, "id"));
    }
    setContent(newValue);
  };

  const searchFollowers = (query, callback) => {
    dispatch(globalSearchFollowersToMention(query));
    callback(followers);
  };

  const validatMedia = commentBody => {
    // media is new, add new file
    if (!isEqual(comment?.media?.file, uploadedFile)) {
      // eslint-disable-next-line no-param-reassign
      commentBody.file = uploadedFile;
    } else {
      // don't pass media to api as nothing changes
      // eslint-disable-next-line no-param-reassign
      delete commentBody.file;
    }

    if (!isEqual(comment.giphy, selectedGif)) {
      // eslint-disable-next-line no-param-reassign
      commentBody.giphy = selectedGif;
    } else {
      // eslint-disable-next-line no-param-reassign
      delete commentBody.giphy;
    }
  };

  const updatePostComment = postCommentBody => {
    validatMedia(postCommentBody);

    // eslint-disable-next-line no-param-reassign
    delete postCommentBody.file?.__typename;

    const updatepostCommentBody = {
      commentId: comment._id,
      content: postCommentBody.content,
      file: postCommentBody.file,
      giphy: postCommentBody.giphy
    };

    dispatch(updateComment(post.feedId, updatepostCommentBody));
  };

  const onKeyPress = event => {
    if (event.key === "Enter" && !event.shiftKey) {
      if (!isEmpty(selectedGif)) {
        delete selectedGif.images["480w_still"];
      }

      if (content.length > 0 || !isEmpty(uploadedFile) || !isEmpty(selectedGif)) {
        const commentBody: PostCommentBody = {
          feedId: post.feedId,
          content: content.trim(),
          mentions,
          platform,
          file: uploadedFile,
          giphy: selectedGif
        };

        if (comment) {
          delete uploadedFile.edit;
          updatePostComment(commentBody);
        } else {
          dispatch(addComment({ ...commentBody, parent }));
        }
        clearInputs();
      }
    }
    return {};
  };

  const clearInputs = () => {
    setTimeout(() => {
      setContent("");
      setMentions([]);
      setUploadedFile({});
      setSelectedGif({});
    }, 50);
  };

  const onGifSelected = gif => {
    analytics.track("COMMENT_GIPHY_BUTTON_CLICKED");
    setSelectedGif(gif);
  };

  const isMediaUploaded = !!uploadedFile.url || !!selectedGif.url;

  const profilePicture = rescueNil(currentUser, "profilePicture");

  return (
    <div className={`mt-0 mb-4 ${!comment && "mx-4"}`}>
      <Form className={`${Style.wrapper} ${Style.thread}`}>
        <div className={Style.formAvatar}>
          <EFAvatar displayOnlineButton={false} profilePicture={getImage(profilePicture, "avatar")} size="small" />
        </div>
        <FormGroup className={Style.fieldWrapper}>
          <MentionsInput
            value={content}
            placeholder="Add a comment"
            onChange={onChange}
            className={`mentionWrapper ${parent && "threadMentionWrapper"}`}
            onKeyPress={onKeyPress}
            autoFocus
            allowSpaceInQuery
          >
            <Mention
              trigger="@"
              data={searchFollowers}
              className="mentionedFriend"
              displayTransform={(_, display) => `@${display}`}
              renderSuggestion={renderSuggestion}
            />
          </MentionsInput>
          <WithUploadContext>
            <CommentMedia
              onFileUpload={setUploadedFile}
              onGifSelected={onGifSelected}
              selectedGif={selectedGif}
              isMediaUploaded={isMediaUploaded}
              selectedMedia={uploadedFile}
            />
          </WithUploadContext>
        </FormGroup>
      </Form>
    </div>
  );
};

CommentForm.defaultProps = {
  comment: null,
  parent: null
};

export default CommentForm;
