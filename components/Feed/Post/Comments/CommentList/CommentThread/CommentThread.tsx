import { Fragment } from "react";
import { useDispatch } from "react-redux";
import { faReply } from "@fortawesome/pro-solid-svg-icons";
import uniqueId from "lodash/uniqueId";

import EFRectangleButton from "../../../../../Buttons/EFRectangleButton/EFRectangleButton";
import { getPaginatedThreadComment } from "../../../../../../store/actions/comment/commentActions";
import { CommentModel } from "../../../../../../graphql/comments/Models";
import Comment from "../Comment/Comment";
import HypeCommentModal from "../../../HypeModal/HypeCommentModal";

const CommentThread = ({ comment, replyRef }: { comment: CommentModel; replyRef: any }) => {
  const dispatch = useDispatch();

  const { docs, hasNextPage, totalDocs, page } = comment.thread;

  const remainingComments = totalDocs - docs.length;

  const { commentable } = comment;

  const viewMore = () => {
    dispatch(getPaginatedThreadComment(commentable.feedId, comment._id, page + 1, 5));
  };

  const threadContent = docs.map((doc, index) => {
    return (
      <Fragment key={uniqueId()}>
        <Comment isLastComment={docs?.length - 1 === index} comment={doc} parent={comment._id} replyRef={replyRef} />
        {doc.loadHypeUser && <HypeCommentModal comment={doc} />}
      </Fragment>
    );
  });

  return (
    <div>
      {threadContent}
      {hasNextPage && remainingComments > 0 && (
        <div>
          <EFRectangleButton
            text={`View ${remainingComments} More
            ${remainingComments > 1 ? " Replies" : " Reply"}`}
            icon={faReply}
            disabled={comment.threadLoading}
            onClick={viewMore}
            colorTheme="transparent"
            shadowTheme="none"
            fontWeightTheme="light"
          />
        </div>
      )}
    </div>
  );
};

export default CommentThread;
