import CommentForm from "../../CommentForm/CommentForm";

const Reply = ({ comment, replyRef }) => {
  return (
    <div className="mb-3" ref={replyRef}>
      <CommentForm parent={comment._id} />
    </div>
  );
};

export default Reply;
