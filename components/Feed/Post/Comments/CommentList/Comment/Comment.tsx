import Style from "./Comment.module.scss";
import { CommentModel } from "../../../../../../graphql/comments/Models";
import CommentHeader from "./CommentHeader/CommentHeader";
import CommenterAvatar from "./CommenterAvatar/CommenterAvatar";
import CommentBody from "./CommentBody/CommentBody";

const Comment = ({
  comment,
  isLastComment,
  parent,
  replyRef
}: {
  comment: CommentModel;
  isLastComment: Boolean;
  parent?: string;
  replyRef: any;
}) => {
  return (
    <div>
      <div className={`${Style.commentWrapper} ${parent ? Style.thread : ""}`}>
        <CommenterAvatar comment={comment} parent={parent} />
        <div className={`${Style.comment} ${parent ? Style.thread : ""}`}>
          <CommentHeader comment={comment} />
          <CommentBody comment={comment} replyRef={replyRef} />
        </div>
      </div>
      {!(isLastComment && !comment?.thread?.totalDocs) && <div className={Style.separator} />}
    </div>
  );
};

Comment.defaultProps = {
  parent: ""
};

export default Comment;
