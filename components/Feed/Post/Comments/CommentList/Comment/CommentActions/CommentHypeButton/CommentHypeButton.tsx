import React from "react";
import { useDispatch } from "react-redux";
import { faFlame } from "@fortawesome/pro-solid-svg-icons";
import Style from "./CommentHypeButton.module.scss";
import { CommentModel } from "../../../../../../../../graphql/comments/Models";
import PostButtonWithIcon from "../../../../../PostButton/PostButtonWithIcon";
import { hypeComment, unhypeComment } from "../../../../../../../../store/actions/comment/commentActions";

const CommentHypeButton = ({ comment }: { comment: CommentModel }) => {
  const dispatch = useDispatch();

  const { hype, commentable } = comment;

  if (hype) {
    return (
      <span className={Style.hypedButton}>
        <PostButtonWithIcon
          icon={faFlame}
          text={<span className={Style.responsiveText}> Hyped </span>}
          onClick={() => dispatch(unhypeComment(commentable.feedId, comment._id, comment.parent))}
        />
      </span>
    );
  }

  return (
    <PostButtonWithIcon
      icon={faFlame}
      text={<span className={Style.responsiveText}> Hype </span>}
      onClick={() => dispatch(hypeComment(commentable.feedId, comment._id, 1, comment.parent))}
      highlightColor="orange"
    />
  );
};

export default CommentHypeButton;
