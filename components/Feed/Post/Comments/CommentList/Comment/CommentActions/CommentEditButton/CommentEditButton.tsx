import React from "react";
import { useDispatch } from "react-redux";
import { faPencilAlt } from "@fortawesome/pro-regular-svg-icons";
import PostButtonWithIcon from "../../../../../PostButton/PostButtonWithIcon";
import { CommentModel } from "../../../../../../../../graphql/comments/Models";
import { toggleCommentEditBtn } from "../../../../../../../../store/actions/comment/commentActions";
import Style from "../CommentActions.module.scss";

const CommentActions = ({ comment }: { comment: CommentModel }) => {
  const dispatch = useDispatch();

  return (
    <PostButtonWithIcon
      icon={faPencilAlt}
      text={<span className={Style.responsiveText}>Cancel</span>}
      onClick={() => dispatch(toggleCommentEditBtn(comment.commentable.feedId, comment._id, comment.parent))}
    />
  );
};

export default CommentActions;
