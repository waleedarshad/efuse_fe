import { useDispatch, useSelector } from "react-redux";
import { faReply } from "@fortawesome/pro-regular-svg-icons";
import Style from "./CommentActions.module.scss";
import PostButtonWithIcon from "../../../../PostButton/PostButtonWithIcon";
import CommentHypeCounter from "./CommentHypeCounter/CommentHypeCounter";
import { CommentModel } from "../../../../../../../graphql/comments/Models";
import CommentHypedButton from "./CommentHypeButton/CommentHypeButton";
import CommentEditButton from "./CommentEditButton/CommentEditButton";
import { toggleCommentReply } from "../../../../../../../store/actions/comment/commentActions";

const CommentActions = ({ comment, replyRef }: { comment: CommentModel; replyRef: any }) => {
  const dispatch = useDispatch();
  const currentUser = useSelector(state => state.auth.currentUser);

  const scrollToReply = () => {
    if (replyRef.current) {
      replyRef.current.scrollIntoView({
        behavior: "smooth",
        block: "center",
        inline: "center"
      });
    }
  };

  return (
    <div className={Style.actionButtons}>
      <ul className={Style.actionButtonsList}>
        <li>
          <CommentHypeCounter numberOfHypes={comment.likes} comment={comment} />
        </li>
        <span className={Style.line}>|</span>
        <li>
          <CommentHypedButton comment={comment} />
        </li>
        {currentUser.id === comment.user._id && comment.enableEdit && (
          <>
            <span className={Style.line}>|</span>
            <li>
              <CommentEditButton comment={comment} />
            </li>
          </>
        )}

        <span className={Style.line}>|</span>
        <li>
          <PostButtonWithIcon
            icon={faReply}
            text={<span className={Style.responsiveText}>Reply</span>}
            onClick={() => {
              if (comment.parent) {
                scrollToReply();
              } else {
                dispatch(toggleCommentReply(comment.commentable.feedId, comment._id, scrollToReply));
              }
            }}
          />
        </li>
      </ul>
    </div>
  );
};

export default CommentActions;
