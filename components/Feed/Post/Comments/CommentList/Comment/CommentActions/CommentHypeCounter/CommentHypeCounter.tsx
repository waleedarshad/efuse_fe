import React, { useContext } from "react";
import { useDispatch } from "react-redux";
import { faFlame } from "@fortawesome/pro-solid-svg-icons";
import Style from "../CommentActions.module.scss";
import { toggleCommentHypeUserRequest } from "../../../../../../../../store/actions/comment/commentActions";
import { PostContext } from "../../../../../PostProvider";
import { CommentModel } from "../../../../../../../../graphql/comments/Models";
import PostButtonWithIcon from "../../../../../PostButton/PostButtonWithIcon";

interface CommentHypeCounterProps {
  numberOfHypes: number;
  comment: CommentModel;
}

const CommentHypeCounter = ({ numberOfHypes = 0, comment }: CommentHypeCounterProps) => {
  const dispatch = useDispatch();
  const { post } = useContext(PostContext);
  const getHypeUser = () => {
    return numberOfHypes > 0 ? dispatch(toggleCommentHypeUserRequest(post.feedId, comment)) : {};
  };
  return (
    <span className={`${Style.hypeCounter} ${numberOfHypes > 0 ? Style.hyped : ""}`}>
      <PostButtonWithIcon
        icon={faFlame}
        text={numberOfHypes.toString()}
        onClick={getHypeUser}
        isDisabled={false}
        highlightColor="orange"
      />
    </span>
  );
};

export default CommentHypeCounter;
