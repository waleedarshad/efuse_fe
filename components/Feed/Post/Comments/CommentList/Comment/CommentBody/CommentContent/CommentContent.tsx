import parse, { domToReact } from "html-react-parser";
import Linkify from "react-linkify";
import { parseMention } from "../../../../../../../../helpers/PostHelper";
import Style from "../../Comment.module.scss";

const CommentContent = ({ comment }) => {
  const htmlComment = parseMention(comment);

  const componentDecorator = (href, text, key) => (
    <a href={href} key={key} target="_blank" rel="noreferrer">
      {text}
    </a>
  );

  const options = {
    // eslint-disable-next-line react/display-name
    replace: ({ attribs, children }) => {
      if (!attribs) return;
      if (attribs.class === "mentionTagDisplay") {
        // eslint-disable-next-line consistent-return
        return domToReact(children, options);
      }
      // eslint-disable-next-line consistent-return
      return <></>;
    }
  };
  const { media, content } = comment;

  return (
    <p className={`${media && "pt-2 pb-2"} ${Style.text}`}>
      {content && <Linkify componentDecorator={componentDecorator}>{parse(htmlComment, options)}</Linkify>}
    </p>
  );
};

export default CommentContent;
