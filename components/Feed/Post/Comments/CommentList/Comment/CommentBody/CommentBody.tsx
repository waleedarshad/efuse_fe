import { Image } from "react-bootstrap";
import CommentActions from "../CommentActions/CommentActions";

import CommentContent from "./CommentContent/CommentContent";
import { CommentModel } from "../../../../../../../graphql/comments/Models";
import DisplayMedia from "../../../CommentMedia/DisplayMedia/DisplayMedia";
import CommentForm from "../../../CommentForm/CommentForm";

const CommentBody = ({ comment, replyRef }: { comment: CommentModel; replyRef: any }) => {
  const { media, giphy, enableEdit } = comment;

  const giphyUrl = giphy?.images?.fixed_height_downsampled?.url;

  if (enableEdit) {
    return (
      <>
        <CommentForm comment={comment} />
        <CommentActions comment={comment} replyRef={replyRef} />
      </>
    );
  }

  return (
    <>
      {media && <DisplayMedia media={media} />}
      {giphy && giphyUrl && <Image src={giphyUrl} alt="Giphy" className="pt-2 pb-2" />}
      <CommentContent comment={comment} />
      <CommentActions comment={comment} replyRef={replyRef} />
    </>
  );
};

export default CommentBody;
