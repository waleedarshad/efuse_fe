import { rescueNil } from "../../../../../../../helpers/GeneralHelper";

import EFAvatar from "../../../../../../EFAvatar/EFAvatar";
import Style from "../Comment.module.scss";

const CommenterAvatar = ({ comment, parent }) => {
  const { user } = comment;
  const profilePicture = rescueNil(user, "profileImage");

  return (
    <div className={`${Style.avatar} `}>
      <EFAvatar
        displayOnlineButton={false}
        profilePicture={profilePicture}
        size={parent ? "extraSmall" : "small"}
        href={`/u/${user?.username}`}
      />
    </div>
  );
};

export default CommenterAvatar;
