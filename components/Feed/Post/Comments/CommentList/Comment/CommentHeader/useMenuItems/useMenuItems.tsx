import { faUserSlash, faBan, faPencilAlt, faTrash } from "@fortawesome/pro-solid-svg-icons";
import { useDispatch, useSelector } from "react-redux";
import { useContext } from "react";
import { toggleCommentEditBtn, deleteComment } from "../../../../../../../../store/actions/comment/commentActions";
import { PostContext } from "../../../../../PostProvider";
import { RolesEnum } from "../../../../../../../../enums";
import { banUser } from "../../../../../../../../store/actions/user/userActions";
import EFAlert from "../../../../../../../EFAlert/EFAlert";

const useMenuItems = (comment, setOpen) => {
  const currentUser = useSelector(state => state.auth.currentUser);
  const isCurrentUserTheAuthor = currentUser._id === comment.user._id;
  const isCurrentUserTheAdmin = currentUser.roles.includes(RolesEnum.ADMIN);

  const { post } = useContext(PostContext);

  const dispatch = useDispatch();

  const options = {
    reportPost: {
      title: "Report Comment",
      as: "button",
      type: "button",
      icon: faBan,
      onClick: () => setOpen(true)
    },
    edit: {
      title: comment.enableEdit ? "Cancel" : "Edit",
      as: "button",
      type: "button",
      icon: faPencilAlt,
      onClick: () => dispatch(toggleCommentEditBtn(comment.commentable.feedId, comment._id, comment.parent))
    },
    delete: {
      title: "Delete",
      as: "button",
      type: "button",
      icon: faTrash,
      onClick: () => dispatch(deleteComment(comment.commentable.feedId, comment._id))
    },
    banUser: {
      title: "Ban User",
      as: "button",
      type: "button",
      icon: faUserSlash,
      onClick: () => EFAlert("Ban this user?", "Are you sure?", "Yes", "No", () => dispatch(banUser(comment.user._id)))
    }
  };

  let menuItems = [];
  if (isCurrentUserTheAuthor) {
    menuItems = [options.edit];
  } else {
    menuItems = [options.reportPost];
  }

  if (isCurrentUserTheAuthor || currentUser.id === post.author._id || isCurrentUserTheAdmin) {
    menuItems = [...menuItems, options.delete];
  }

  if (isCurrentUserTheAdmin) {
    menuItems.push(options.banUser);
  }

  return menuItems;
};

export default useMenuItems;
