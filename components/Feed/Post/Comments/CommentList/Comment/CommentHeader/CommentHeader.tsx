import TimeAgo from "react-timeago";
import { Form, Col } from "react-bootstrap";
import { useState } from "react";
import { useDispatch } from "react-redux";
import UserLink from "../../../../../../UserLink/UserLink";
import VerifiedIcon from "../../../../../../VerifiedIcon/VerifiedIcon";

import EFStreakBadge from "../../../../../../EFStreakBadge/EFStreakBadge";
import CustomDropdown from "../../../../../../CustomDropdown/CustomDropdown";

import Style from "../Comment.module.scss";
import menuItems from "./useMenuItems/useMenuItems";
import EFPrimaryModal from "../../../../../../Modals/EFPrimaryModal/EFPrimaryModal";
import InputRow from "../../../../../../InputRow/InputRow";
import InputLabel from "../../../../../../InputLabel/InputLabel";
import InputField from "../../../../../../InputField/InputField";
import ActionButtonGroup from "../../../../../../Layouts/Internal/ActionButtonGroup/ActionButtonGroup";
import { reportComment } from "../../../../../../../store/actions/comment/commentActions";

const CommentHeader = ({ comment }) => {
  const { user } = comment;

  const dailyStreak = user?.streak;

  const [isOpen, setOpen] = useState(false);
  const [reason, setReason] = useState("");
  const dispatch = useDispatch();

  const commentMenuItems = menuItems(comment, setOpen);
  return (
    <>
      <div className={Style.nameWrapper}>
        <h6 className={Style.name}>
          <UserLink user={user} />
          {user.verified && <VerifiedIcon />}

          <span className={Style.streak}>
            <EFStreakBadge streak={dailyStreak} />
          </span>
        </h6>
        <CustomDropdown icon="darkDots" items={commentMenuItems} customClass={Style.dropdown} />
      </div>
      <p className={Style.username}>
        @{user.username}
        <small className={Style.options}>
          <TimeAgo date={comment.createdAt} minPeriod={30} />
        </small>
      </p>
      <EFPrimaryModal
        title="Report Comment"
        widthTheme="small"
        isOpen={isOpen}
        allowBackgroundClickClose
        onClose={() => {
          setReason("");
        }}
      >
        <Form
          noValidate
          validated={false}
          onSubmit={event => {
            event.preventDefault();
            dispatch(reportComment(comment._id, reason));
            setReason("");
            setOpen(false);
          }}
        >
          <InputRow>
            <Col sm={12}>
              <InputLabel theme="darkColor">Reason</InputLabel>
              <InputField
                name="report"
                placeholder="Reason"
                theme="whiteShadow"
                size="lg"
                onChange={e => setReason(e.target.value)}
                value={reason}
              />
            </Col>
          </InputRow>
          <ActionButtonGroup
            onCancel={() => {
              setReason("");
              setOpen(false);
            }}
            disabledCancel
            disabled={!reason}
          />
        </Form>
      </EFPrimaryModal>
    </>
  );
};

export default CommentHeader;
