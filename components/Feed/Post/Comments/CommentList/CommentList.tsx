import React, { useContext, createRef } from "react";

import Comment from "./Comment/Comment";

import AnimatedLogo from "../../../../AnimatedLogo";
import { PostContext } from "../../PostProvider";
import { CommentModel } from "../../../../../graphql/comments/Models";
import CommentThread from "./CommentThread/CommentThread";
import CommentReply from "./CommentReply/CommentReply";
import HypeCommentModal from "../../HypeModal/HypeCommentModal";

const CommentList = () => {
  const { post } = useContext(PostContext);

  const comments = post.postComments?.docs;

  const postComments = comments?.map((comment: CommentModel, index) => {
    const replyRef = createRef();

    return (
      <div key={comment._id}>
        <Comment isLastComment={comments?.length - 1 === index} comment={comment} replyRef={replyRef} />
        {comment.thread && <CommentThread comment={comment} replyRef={replyRef} />}
        {comment.enableThreadReply && <CommentReply comment={comment} replyRef={replyRef} />}
        {comment.loadHypeUser && <HypeCommentModal comment={comment} />}
      </div>
    );
  });

  return (
    <div className="mx-4">
      {postComments}
      {post.isFetchingPostComments && <AnimatedLogo theme="content" />}
    </div>
  );
};

export default CommentList;
