import EFRectangleButton from "../../../../Buttons/EFRectangleButton/EFRectangleButton";

import Style from "./LoadMore.module.scss";

const LoadMore = ({ loadMore }) => (
  <div className={Style.textCenter}>
    <EFRectangleButton
      text="Load More Comments"
      onClick={loadMore}
      colorTheme="transparent"
      shadowTheme="none"
      className={Style.loadMoreBtn}
      fontWeightTheme="normal"
    />
  </div>
);

export default LoadMore;
