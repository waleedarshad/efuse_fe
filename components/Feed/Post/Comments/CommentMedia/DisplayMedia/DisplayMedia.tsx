import VideoPlayer from "../../../Content/PostMedia/VideoPlayer";
import Style from "./DisplayMedia.module.scss";

const DisplayMedia = ({ media }) => {
  const { file } = media;

  if (file.contentType?.includes("video")) {
    return <VideoPlayer videoUrl={file.url} />;
  }

  const image = (
    <div className={Style.imageWrapper}>
      <img src={file.url || file[0].url} alt="User posted media" className={Style.postImage} />
    </div>
  );

  return image;
};

export default DisplayMedia;
