import { useContext } from "react";
import { OverlayTrigger, Tooltip } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCamera } from "@fortawesome/pro-solid-svg-icons";

import DirectUpload from "../../../../../DirectUpload/DirectUpload";
import { UploadContext } from "../../../../../DirectUpload/UploadContext";

const AddCommentMedia = ({ disabled }) => {
  const { onUploadStart, onUploadSuccess, onUploadError, onUploadProgress } = useContext(UploadContext);

  const onFileSelected = (fileToUpload, startCallback) => {
    onUploadStart(fileToUpload, startCallback);
  };

  return (
    <DirectUpload
      onProgress={onUploadProgress}
      onError={onUploadError}
      preprocess={onFileSelected}
      onFinish={onUploadSuccess}
      removeMargin
      noOpacityDisabled
      customDesign
      disabled={disabled}
    >
      <OverlayTrigger
        key="top"
        placement="top"
        overlay={<Tooltip id="tooltip-direct-upload-top">Attach Photo/Video</Tooltip>}
      >
        <FontAwesomeIcon icon={faCamera} />
      </OverlayTrigger>
    </DirectUpload>
  );
};

export default AddCommentMedia;
