import { useContext, useEffect } from "react";
import Style from "./CommentMedia.module.scss";

import { UploadContext } from "../../../../DirectUpload/UploadContext";
import directUpload from "../../../../hoc/directUpload";

import EFGiphySearch from "../../../../EFGiphySearch/EFGiphySearch";
import AddCommentMedia from "./AddCommentMedia/AddCommentMedia";
import DisplayMedia from "../../../../DirectUpload/DisplayMedia/DisplayMedia";
import EFPillButton from "../../../../Buttons/EFPillButton/EFPillButton";
import { faGif } from "../../../../../config/customIcons";

const CommentMedia = ({ onFileUpload, selectedGif, selectedMedia, onGifSelected, isMediaUploaded }) => {
  const { isUploading, uploadedFile } = useContext(UploadContext);

  const areMediaActionsDisabled = isMediaUploaded || isUploading;

  useEffect(() => {
    onFileUpload(uploadedFile);
  }, [uploadedFile.url]);

  return (
    <div className={`${Style.mediaWrapper} `}>
      <div className={Style.item}>
        <div className={Style.displayMedia}>
          <DisplayMedia
            file={selectedMedia}
            gif={selectedGif}
            clearGif={() => onGifSelected({})}
            clearUploadedFile={onFileUpload}
            commentMedia
          />
        </div>
        <AddCommentMedia disabled={areMediaActionsDisabled} />
        <EFGiphySearch
          renderCustomTrigger={onTriggerClick => (
            <div className={Style.displayGiphy}>
              <EFPillButton
                buttonType="button"
                shadowTheme="none"
                colorTheme="transparent"
                fontWeightTheme="bold"
                icon={faGif}
                text="  "
                width="fullWidth"
                disabled={areMediaActionsDisabled}
                alignContent="left"
                size="medium"
                onClick={event => {
                  onTriggerClick(event);
                }}
              />
            </div>
          )}
          disabled={areMediaActionsDisabled}
          onGifSelected={onGifSelected}
          placement="top"
        />
      </div>
    </div>
  );
};

export default directUpload(CommentMedia);
