import React, { useContext, useEffect } from "react";
import { useDispatch } from "react-redux";
import CommentForm from "./CommentForm/CommentForm";
import { PostContext } from "../PostProvider";
import CommentList from "./CommentList/CommentList";

import { getPaginatedComments } from "../../../../store/actions/comment/commentActions";
import LoadMore from "./LoadMore/LoadMore";

const Comments = () => {
  const { post } = useContext(PostContext);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getPaginatedComments(post.feedId, 1, 5));
  }, []);

  const loadMore = () => {
    dispatch(getPaginatedComments(post.feedId, post.postComments.pagination.page + 1, 5));
  };

  return (
    <div>
      <CommentForm />
      <CommentList />
      {post.postComments?.pagination?.hasNextPage && <LoadMore loadMore={loadMore} />}
    </div>
  );
};

export default Comments;
