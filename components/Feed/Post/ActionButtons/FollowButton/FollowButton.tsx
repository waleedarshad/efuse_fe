import { useContext } from "react";
import { faUserPlus } from "@fortawesome/pro-regular-svg-icons";
import { faUserCheck } from "@fortawesome/pro-solid-svg-icons";
import { useDispatch } from "react-redux";
import { PostContext } from "../../PostProvider";
import PostButtonWithIcon from "../../PostButton/PostButtonWithIcon";
import {
  unFollowOrganization,
  unFollowUser,
  followOrganization,
  followUser
} from "../../../../../store/actions/feedActions";
import Style from "./FollowButton.module.scss";

const FollowButton = () => {
  const dispatch = useDispatch();
  const { post, isCurrentUserTheAuthor, isPostedByOrg, postType } = useContext(PostContext);

  if (isCurrentUserTheAuthor || postType === "following") {
    return <></>;
  }

  const onUnfollow = () => {
    if (isPostedByOrg) {
      dispatch(unFollowOrganization(post.organization.id, post.organization.name));
    } else {
      dispatch(unFollowUser(post.author._id, post.author.name));
    }
  };

  const onFollow = () => {
    if (isPostedByOrg) {
      dispatch(followOrganization(post.organization.id, post.organization.name));
    } else {
      dispatch(followUser(post.author._id, post.author.name));
    }
  };

  if (post.currentUserFollows) {
    return (
      <div className={Style.followingButton}>
        <PostButtonWithIcon onClick={onUnfollow} icon={faUserCheck} text="Following" />
      </div>
    );
  }

  return <PostButtonWithIcon onClick={onFollow} icon={faUserPlus} text="Follow" />;
};

export default FollowButton;
