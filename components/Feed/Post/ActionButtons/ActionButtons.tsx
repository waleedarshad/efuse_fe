import { faCommentAlt, faShare } from "@fortawesome/pro-regular-svg-icons";
import React, { useContext } from "react";
import { useDispatch } from "react-redux";
import getConfig from "next/config";
import Style from "./ActionButtons.module.scss";
import HypeButton from "./HypeButton/HypeButton";
import { toggleCommentCreation } from "../../../../store/actions/comment/commentActions";
import { PostContext } from "../PostProvider";
import PostButtonWithIcon from "../PostButton/PostButtonWithIcon";
import { constructPostUrl } from "../../../../helpers/PostHelper";
import { shareLinkModal } from "../../../../store/actions/shareAction";
import FollowButton from "./FollowButton/FollowButton";
import { SHARE_LINK_EVENTS } from "../../../../common/analyticEvents";

const { publicRuntimeConfig } = getConfig();
const { feBaseUrl } = publicRuntimeConfig;

const ActionButtons = () => {
  const dispatch = useDispatch();

  const { post, isCurrentUserTheAuthor } = useContext(PostContext);

  return (
    <div className={Style.actionButtons}>
      <ul className={Style.actionButtonsList}>
        <li>
          <HypeButton />
        </li>
        <li>
          <PostButtonWithIcon
            icon={faCommentAlt}
            text="Comment"
            onClick={() => dispatch(toggleCommentCreation(post.feedId))}
          />
        </li>
        <li>
          <PostButtonWithIcon
            icon={faShare}
            text="Share"
            onClick={() => {
              const shareModalTitle = `Share ${isCurrentUserTheAuthor ? "your" : `${post.author.name}'s`} post`;
              const shareModalLink = `${feBaseUrl}${constructPostUrl(post.parentFeedId, post.isNewPost)}`;

              analytics.track(SHARE_LINK_EVENTS.SHARE_BUTTON_CLICKED, {
                url: shareModalLink
              });

              dispatch(shareLinkModal(true, shareModalTitle, shareModalLink));
            }}
          />
        </li>
      </ul>
      <FollowButton />
    </div>
  );
};
export default ActionButtons;
