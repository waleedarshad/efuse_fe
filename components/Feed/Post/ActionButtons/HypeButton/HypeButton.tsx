import React, { useContext } from "react";
import { faFlame } from "@fortawesome/pro-solid-svg-icons";
import { HypingContext } from "../../Hype/HypingProvider";
import { PostContext } from "../../PostProvider";
import PostButtonWithIcon from "../../PostButton/PostButtonWithIcon";
import Style from "./HypeButton.module.scss";

const HypeButton = () => {
  const { setIsTimerDisplayed, isTimerRunning, setIsTimerRunning, newUserHypes, setNewUserHypes } = useContext(
    HypingContext
  );

  const { post } = useContext(PostContext);

  const hypingSessionShouldStart = newUserHypes === 0 && !isTimerRunning;
  const hypingSessionRunning = newUserHypes > 0 && isTimerRunning;
  const userIsAllowedToHype = post.currentUserHypes === 0;

  const onHypeClick = () => {
    if (userIsAllowedToHype) {
      if (hypingSessionShouldStart) {
        setNewUserHypes(newUserHypes + 1);
        setIsTimerDisplayed(true);
        setIsTimerRunning(true);
        return;
      }

      if (hypingSessionRunning) {
        setNewUserHypes(newUserHypes + 1);
      }
    }
  };

  const userHasAlreadyHyped = post.currentUserHypes > 0 || newUserHypes > 0;

  if (userHasAlreadyHyped && !isTimerRunning) {
    return (
      <span className={Style.hypedButton}>
        <PostButtonWithIcon icon={faFlame} text="Hyped" onClick={() => {}} isDisabled />
      </span>
    );
  }

  return <PostButtonWithIcon icon={faFlame} text="Hype" onClick={onHypeClick} highlightColor="orange" />;
};

export default HypeButton;
