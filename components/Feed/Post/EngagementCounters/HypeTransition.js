import PropTypes from "prop-types";
import Transition from "../../../General/Transition/Transition";
import HypingTimer from "./HypingTimer/HypingTimer";

const HypeTransition = ({ isTimerDisplayed, onTimerEnd, newUserHypes }) => {
  return (
    <Transition
      isMounted={isTimerDisplayed}
      containerStyles={{
        width: "100%",
        height: "40px",
        position: "absolute"
      }}
    >
      <HypingTimer onTimerEnd={onTimerEnd} newUserHypes={newUserHypes} />
    </Transition>
  );
};

HypeTransition.propTypes = {
  isTimerDisplayed: PropTypes.bool.isRequired,
  onTimerEnd: PropTypes.func.isRequired,
  newUserHypes: PropTypes.number.isRequired
};

export default HypeTransition;
