import React, { useContext } from "react";
import { useDispatch, useSelector } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircle } from "@fortawesome/pro-solid-svg-icons";
import Style from "./EngagementCounters.module.scss";
import HypeTransition from "./HypeTransition";
import { HypingContext } from "../Hype/HypingProvider";
import { PostContext } from "../PostProvider";
import HypeCounter from "./HypeCounter/HypeCounter";
import { toggleCommentCreation } from "../../../../store/actions/comment/commentActions";

import PostButton from "../PostButton/PostButton";

const EngagementCounters = () => {
  const { isTimerDisplayed, newUserHypes, onTimerEnd } = useContext(HypingContext);
  const { post } = useContext(PostContext);
  const isAuthor = useSelector(state => state.auth.currentUser?._id === post.author._id);
  const isAdmin = useSelector(state => state.auth.currentUser?.roles?.includes("admin"));
  const displayViews = isAdmin || isAuthor;

  const dispatch = useDispatch();

  return (
    <div className={Style.engagementCounters}>
      <HypeTransition isTimerDisplayed={isTimerDisplayed} onTimerEnd={onTimerEnd} newUserHypes={newUserHypes} />
      {!isTimerDisplayed && (
        <div className={Style.countersContainer}>
          <HypeCounter numberOfHypes={post.hypes + newUserHypes} />
          <FontAwesomeIcon className={Style.dot} icon={faCircle} />
          <PostButton onClick={() => dispatch(toggleCommentCreation(post.feedId))}>
            {`${post.comments} Comment${post.comments === 1 ? "" : "s"}`}
          </PostButton>
          {displayViews && <FontAwesomeIcon className={Style.dot} icon={faCircle} />}
          {displayViews && <PostButton onClick={() => {}}>{post.views} Views</PostButton>}
        </div>
      )}
    </div>
  );
};

export default EngagementCounters;
