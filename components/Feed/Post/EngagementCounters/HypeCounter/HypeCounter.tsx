import { useContext } from "react";
import { useDispatch } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFlame } from "@fortawesome/pro-solid-svg-icons";
import Style from "./HypeCounter.module.scss";
import { toggleHypeUserRequest } from "../../../../../store/actions/feedActions";
import { PostContext } from "../../PostProvider";

interface HypeCounterProps {
  numberOfHypes: number;
}

const HypeCounter = ({ numberOfHypes = 0 }: HypeCounterProps) => {
  const { post } = useContext(PostContext);

  const dispatch = useDispatch();
  return (
    <span
      className={Style.hypeCounter}
      onClick={() => (numberOfHypes > 0 ? dispatch(toggleHypeUserRequest(post.feedId)) : {})}
      aria-hidden="true"
    >
      <FontAwesomeIcon className={`${Style.icon} ${numberOfHypes > 0 ? Style.hyped : ""}`} icon={faFlame} />
      <span>{numberOfHypes}</span>
    </span>
  );
};

export default HypeCounter;
