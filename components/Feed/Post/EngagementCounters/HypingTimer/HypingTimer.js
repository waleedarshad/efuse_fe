import React, { useEffect, useState } from "react";
import { animated, useSpring } from "react-spring";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFlame } from "@fortawesome/pro-solid-svg-icons";
import { withCdn } from "../../../../../common/utils";
import useMeasure from "../../../../../hooks/useMeasure";
import Styles from "./HypingTimer.module.scss";

const TIMER_DURATION = 3000;

export const getBarMessage = (completed, newUserHypes) => {
  if (completed < 0.5) {
    return `keep hyping +${newUserHypes}`;
  }
  if (completed < 0.75) {
    return `almost there +${newUserHypes}`;
  }
  if (completed < 1) {
    return `keep going +${newUserHypes}`;
  }
  if (completed === 1) {
    return `hyped max +${newUserHypes}`;
  }
  return "";
};

const HypingTimer = ({ newUserHypes, onTimerEnd }) => {
  const [containerBind, containerProps] = useMeasure();
  const [barBind, barProps] = useMeasure();
  const [displayFlame, setDisplayFlame] = useState(false);
  const [message, setMessage] = useState(" ");

  useEffect(() => {
    const ratioTimerCompleted = barProps.width / containerProps.width;

    if (ratioTimerCompleted === 1) {
      setDisplayFlame(true);
      onTimerEnd();
    }

    setMessage(getBarMessage(ratioTimerCompleted, newUserHypes));
  }, [barProps.width]);

  const barSpringProps = useSpring({
    from: { width: "0%" },
    to: { width: "100%" },
    config: { duration: TIMER_DURATION }
  });

  const messageSpringProps = useSpring({
    from: { right: "0px" },
    to: { right: "120px" },
    config: { duration: TIMER_DURATION }
  });

  const imgSpringProps = useSpring({
    opacity: displayFlame ? 1 : 0,
    config: { duration: 200 }
  });

  const faSpringProps = useSpring({
    opacity: displayFlame ? 0 : 1,
    config: { duration: 200 }
  });

  const AnimatedFontAwesomeIcon = animated(FontAwesomeIcon);

  return (
    <>
      <span className={Styles.icon}>
        <div className={Styles.flameContainer}>
          <animated.div style={imgSpringProps} className={Styles.imgContainer}>
            <img className={Styles.flameImg} src={withCdn("/static/images/flame.png")} alt="flame" />
          </animated.div>
          <AnimatedFontAwesomeIcon style={faSpringProps} icon={faFlame} />
        </div>
      </span>
      <div className={Styles.hypingTimer}>
        <div {...containerBind} className={Styles.barContainer}>
          <animated.div {...barBind} className={Styles.bar} style={barSpringProps}>
            <animated.div style={messageSpringProps} className={Styles.messageContainer}>
              {message}
            </animated.div>
          </animated.div>
        </div>
      </div>
    </>
  );
};

export default HypingTimer;
