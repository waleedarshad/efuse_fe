import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFlame } from "@fortawesome/pro-solid-svg-icons";
import { mount } from "enzyme";

import useMeasure from "../../../../../hooks/useMeasure";
import HypingTimer, { getBarMessage } from "./HypingTimer";

jest.mock("../../../../../hooks/useMeasure", () => jest.fn(() => [{}, {}]));

describe("HypingTimer", () => {
  let subject;
  let onTimerEnd;

  beforeEach(() => {
    onTimerEnd = jest.fn();
    subject = mount(<HypingTimer onTimerEnd={onTimerEnd} newUserHypes={4} />);
  });

  it("prints correct message based on progress with number of new user hypes", () => {
    expect(getBarMessage(0.49, 5)).toEqual("keep hyping +5");
    expect(getBarMessage(0.74, 5)).toEqual("almost there +5");
    expect(getBarMessage(0.99, 5)).toEqual("keep going +5");
    expect(getBarMessage(1, 5)).toEqual("hyped max +5");
  });

  it("calls onTimerEnd when progress bar has finished", () => {
    useMeasure.mockReturnValue([{}, { width: 1 }]);

    mount(<HypingTimer onTimerEnd={onTimerEnd} newUserHypes={4} />);

    expect(onTimerEnd).toHaveBeenCalled();
  });

  it("renders flame image and icon", () => {
    expect(subject.find("img").prop("src")).toEqual("/static/images/flame.png");
    expect(subject.find(FontAwesomeIcon).prop("icon")).toEqual(faFlame);
  });
});
