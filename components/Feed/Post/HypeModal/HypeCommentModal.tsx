import { useContext, useEffect } from "react";
import InfiniteScroll from "react-infinite-scroller";
import { useDispatch } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFlame } from "@fortawesome/pro-solid-svg-icons";
import AnimatedLogo from "../../../AnimatedLogo";
import HypeModalContent from "./HypeModalContent/HypeModalContent";
import EFPrimaryModal from "../../../Modals/EFPrimaryModal/EFPrimaryModal";
import { PostContext } from "../PostProvider";
import Style from "./HypeModal.module.scss";
import { clearHypeUser } from "../../../../store/actions/feedActions";
import { getCommentHypedUsers, toggleCommentHypeUserRequest } from "../../../../store/actions/comment/commentActions";

const HypeCommentModal = ({ comment }) => {
  const { post } = useContext(PostContext);
  const { hypedUser } = comment;

  const dispatch = useDispatch();
  const getHypedUser = () => {
    if (hypedUser?.pagination?.hasNextPage) {
      dispatch(getCommentHypedUsers(post.feedId, comment, hypedUser?.pagination?.page + 1, 5));
    }
  };

  useEffect(() => {
    dispatch(getCommentHypedUsers(post.feedId, comment, 1, 5));
  }, []);

  return (
    <EFPrimaryModal
      isOpen={comment.loadHypeUser}
      displayCloseButton
      onClose={() => {
        dispatch(clearHypeUser(post.feedId, comment._id));
        dispatch(toggleCommentHypeUserRequest(post.feedId, comment));
      }}
      allowBackgroundClickClose={false}
      title={
        <>
          <div className={Style.hypes}>
            <FontAwesomeIcon className={`${Style.icon} ${Style.hyped}`} icon={faFlame} />
            <span>Hypes</span> <small className={Style.hypeCount}>{comment.likes}</small>
          </div>
        </>
      }
    >
      <InfiniteScroll
        pageStart={1}
        loadMore={getHypedUser}
        hasMore={hypedUser?.pagination?.hasNextPage}
        loader={<AnimatedLogo key={0} theme="inline" />}
      >
        {!hypedUser?.pagination && <AnimatedLogo key={0} theme="inline" />}
        <HypeModalContent
          hypedUser={hypedUser}
          type="comments"
          postId={post.feedId}
          commentId={comment._id}
          parentId={comment.parent}
        />
      </InfiniteScroll>
    </EFPrimaryModal>
  );
};

export default HypeCommentModal;
