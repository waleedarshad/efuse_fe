import { useContext, useEffect } from "react";
import InfiniteScroll from "react-infinite-scroller";
import { useDispatch } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFlame } from "@fortawesome/pro-solid-svg-icons";
import AnimatedLogo from "../../../AnimatedLogo";
import HypeModalContent from "./HypeModalContent/HypeModalContent";
import EFPrimaryModal from "../../../Modals/EFPrimaryModal/EFPrimaryModal";
import { PostContext } from "../PostProvider";
import Style from "./HypeModal.module.scss";
import { clearHypeUser, getPostHypedUsers, toggleHypeUserRequest } from "../../../../store/actions/feedActions";

const HypePostModal = () => {
  const { post } = useContext(PostContext);
  const { hypedUser } = post;

  const dispatch = useDispatch();
  const getHypedUser = () => {
    if (hypedUser?.pagination?.hasNextPage) {
      dispatch(getPostHypedUsers(post.feedId, hypedUser?.pagination?.page + 1, 5));
    }
  };

  useEffect(() => {
    dispatch(getPostHypedUsers(post.feedId, 1, 5));
  }, []);

  return (
    <EFPrimaryModal
      isOpen={post.loadHypeUser}
      displayCloseButton
      onClose={() => {
        dispatch(clearHypeUser(post.feedId));
        dispatch(toggleHypeUserRequest(post.feedId));
      }}
      allowBackgroundClickClose={false}
      title={
        <>
          <div className={Style.hypes}>
            <FontAwesomeIcon className={`${Style.icon} ${Style.hyped}`} icon={faFlame} />
            <span>Hypes</span> <small className={Style.hypeCount}>{post.hypes}</small>
          </div>
        </>
      }
    >
      <InfiniteScroll
        pageStart={1}
        loadMore={getHypedUser}
        hasMore={hypedUser?.pagination?.hasNextPage}
        loader={<AnimatedLogo key={0} theme="inline" />}
      >
        {!hypedUser?.pagination && <AnimatedLogo key={0} theme="inline" />}
        <HypeModalContent hypedUser={hypedUser} type="posts" postId={post.feedId} />
      </InfiniteScroll>
    </EFPrimaryModal>
  );
};

export default HypePostModal;
