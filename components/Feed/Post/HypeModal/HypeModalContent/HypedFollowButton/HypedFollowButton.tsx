import React from "react";
import Router from "next/router";
import { useSelector, useDispatch } from "react-redux";
import { faUserMinus, faUserPlus, faComment } from "@fortawesome/pro-regular-svg-icons";
import EFRectangleButton from "../../../../../Buttons/EFRectangleButton/EFRectangleButton";
import { startChatWith } from "../../../../../../store/actions/messageActions";
import { followHypedUser, unFollowHypedUser } from "../../../../../../store/actions/follow/followerAction";
import Styles from "../HypeModalContent.module.scss";

const HypedFollowButton = ({ user, type, postId, commentId, parentId }) => {
  const dispatch = useDispatch();
  const disable = useSelector(state => state.followers.disable);
  if (user.isFollowedByUser && user.currentUserFollows) {
    return (
      <div className={Styles.button}>
        <EFRectangleButton
          icon={faComment}
          colorTheme="light"
          fontWeightTheme="normalCase"
          onClick={() => {
            startChatWith(user?._id, user?.name);
            Router.push("/messages");
          }}
          disabled={disable}
          text="Message"
          shadowTheme="none"
        />
      </div>
    );
  }
  if (user.currentUserFollows) {
    return (
      <div className={Styles.button}>
        <EFRectangleButton
          icon={faUserMinus}
          colorTheme="light"
          fontWeightTheme="normalCase"
          onClick={() => dispatch(unFollowHypedUser(user.id, type, postId, commentId, parentId))}
          disabled={disable}
          text="Following"
          shadowTheme="none"
        />
      </div>
    );
  }

  return (
    <div className={Styles.button}>
      <EFRectangleButton
        icon={faUserPlus}
        colorTheme="light"
        fontWeightTheme="normalCase"
        onClick={() => dispatch(followHypedUser(user.id, type, postId, commentId, parentId))}
        disabled={disable}
        text="Follow"
        shadowTheme="none"
      />
    </div>
  );
};

export default HypedFollowButton;
