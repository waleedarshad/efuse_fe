import { useSelector } from "react-redux";
import isEmpty from "lodash/isEmpty";
import uniqueId from "lodash/uniqueId";
import Link from "next/link";
import HypedFollowButton from "./HypedFollowButton/HypedFollowButton";
import { PaginatedHypeUser } from "../../../../../graphql/feeds/Models";
import { getImage } from "../../../../../helpers/GeneralHelper";
import EFAvatar from "../../../../EFAvatar/EFAvatar";
import Style from "./HypeModalContent.module.scss";

const HypeModalContent = ({
  hypedUser,
  type,
  postId,
  commentId,
  parentId
}: {
  hypedUser: PaginatedHypeUser;
  type: string;
  postId: string;
  commentId?: string;
  parentId: string;
}) => {
  const currentUser = useSelector(state => state.auth.currentUser);

  const content = hypedUser?.docs.map(user => {
    const isOwner = !isEmpty(currentUser) && currentUser.id === user.id;
    return (
      <div key={uniqueId()} className={Style.hypeUser}>
        <div className={Style.userDetail}>
          <div className={Style.avatarIcon}>
            <EFAvatar
              profilePicture={getImage(user.profilePicture, "url")}
              size="small"
              href={`/u/${user?.username}`}
              online={false}
              displayOnlineButton={false}
            />
          </div>
          <div className={Style.username}>
            <span>
              <Link href={`/u/${user.username}`}>{user.name}</Link>
            </span>
            <small className={Style.name}>
              <Link href={`/u/${user.username}`}>{`@${user.username}`}</Link>
            </small>
          </div>
        </div>
        {!isOwner && (
          <div className={Style.btn}>
            <HypedFollowButton user={user} type={type} postId={postId} commentId={commentId} parentId={parentId} />
          </div>
        )}
      </div>
    );
  });
  return <div>{content} </div>;
};

HypeModalContent.defaultProps = {
  commentId: null
};
export default HypeModalContent;
