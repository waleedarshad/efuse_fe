import { useContext, useState } from "react";

import { Form, Col } from "react-bootstrap";
import { useDispatch } from "react-redux";

import { PostContext } from "../PostProvider";

import { reportPost } from "../../../../store/actions/feedActions";
import EFPrimaryModal from "../../../Modals/EFPrimaryModal/EFPrimaryModal";

import InputRow from "../../../InputRow/InputRow";
import InputLabel from "../../../InputLabel/InputLabel";
import InputField from "../../../InputField/InputField";
import ActionButtonGroup from "../../../Layouts/Internal/ActionButtonGroup/ActionButtonGroup";

const ReportPostModal = () => {
  const dispatch = useDispatch();

  const { post, isPostReported, setReportPost } = useContext(PostContext);

  const [reason, setReason] = useState("");

  return (
    <EFPrimaryModal
      title="Report Post"
      widthTheme="small"
      isOpen={isPostReported}
      allowBackgroundClickClose
      onClose={() => {
        setReason("");
      }}
    >
      <Form
        noValidate
        validated={false}
        onSubmit={event => {
          event.preventDefault();
          dispatch(reportPost(post.parentFeedId, post.feedId, reason));
          setReason("");
          setReportPost(false);
        }}
      >
        <InputRow>
          <Col sm={12}>
            <InputLabel theme="darkColor">Reason</InputLabel>
            <InputField
              name="report"
              placeholder="Reason"
              theme="whiteShadow"
              size="lg"
              onChange={e => setReason(e.target.value)}
              value={reason}
            />
          </Col>
        </InputRow>
        <ActionButtonGroup
          onCancel={() => {
            setReason("");
            setReportPost(false);
          }}
          disabledCancel
          disabled={!reason}
        />
      </Form>
    </EFPrimaryModal>
  );
};

export default ReportPostModal;
