import {
  faUserSlash,
  faBan,
  faChartLine,
  faEye,
  faEyeSlash,
  faPencilAlt,
  faTrash
} from "@fortawesome/pro-solid-svg-icons";
import { useDispatch, useSelector } from "react-redux";
import { useContext, useEffect, useState } from "react";
import {
  deletePost,
  hidePostFromLounge,
  removePostFromHome,
  updatePostBoost,
  getPostToEdit
} from "../../../../store/actions/feedActions";
import { Post } from "../../../../graphql/feeds/Models";
import { FeedType } from "../../types";
import { PostContext } from "../PostProvider";
import { banUser } from "../../../../store/actions/user/userActions";
import EFAlert from "../../../EFAlert/EFAlert";

const useMenuItems = (
  post: Post,
  postType: FeedType,
  isCurrentUserTheAuthor: boolean,
  isCurrentUserTheAdmin: boolean
) => {
  const dispatch = useDispatch();
  const { onViewPostClicked, setReportPost } = useContext(PostContext);
  const currentUser = useSelector(state => state.auth.currentUser);

  const [menuItems, setMenuItems] = useState([]);

  const buildOptions = () => ({
    view: {
      title: "View",
      as: "button",
      type: "button",
      onClick: () => {
        onViewPostClicked(post);
      },
      icon: faEye
    },
    reportPost: {
      title: "Report Post",
      as: "button",
      type: "button",
      icon: faBan,
      onClick: () => setReportPost(true)
    },
    edit: {
      title: "Edit",
      as: "button",
      type: "button",
      icon: faPencilAlt,
      onClick: () => dispatch(getPostToEdit(post.feedId))
    },
    delete: {
      title: "Delete",
      as: "button",
      type: "button",
      icon: faTrash,
      onClick: () => dispatch(deletePost(post.feedId))
    },
    removeFromTimeline: {
      title: "Remove From Timeline",
      as: "button",
      type: "button",
      icon: faTrash,
      onClick: () => removePostFromHome(post.parentFeedId, post.feedId)
    },
    hide: {
      title: "Hide",
      as: "button",
      type: "button",
      icon: faEyeSlash,
      onClick: () => dispatch(hidePostFromLounge(post.parentFeedId, post.feedId))
    },
    boost: {
      title: post.boosted ? "Un-Boost" : "Boost",
      as: "button",
      type: "button",
      icon: faChartLine,
      onClick: () => dispatch(updatePostBoost(post.feedId, !post.boosted))
    },
    banUser: {
      title: "Ban User",
      as: "button",
      type: "button",
      icon: faUserSlash,
      onClick: () => EFAlert("Ban this user?", "Are you sure?", "Yes", "No", () => dispatch(banUser(post.author._id)))
    }
  });

  useEffect(() => {
    const options = buildOptions();

    let localMenuItems = [];

    if (isCurrentUserTheAuthor) {
      localMenuItems = [options.view, options.edit, options.delete];
    } else {
      if (postType === "following") {
        localMenuItems = [options.view, options.removeFromTimeline, options.reportPost];
      }

      if (postType === "profile") {
        localMenuItems = [options.view, options.reportPost];
      }

      if (postType === "verified" || postType === "featured") {
        localMenuItems = [options.view, options.reportPost];

        if (currentUser?.roles?.includes("admin")) {
          localMenuItems.push(options.hide, options.boost);
        }
      }
      if (postType === "profile") {
        localMenuItems = [options.view, options.reportPost];
      }
    }

    if (isCurrentUserTheAdmin && !isCurrentUserTheAuthor) {
      localMenuItems.push(options.delete, options.banUser);
    }

    setMenuItems(localMenuItems);
  }, [post.boosted]);

  return menuItems;
};

export default useMenuItems;
