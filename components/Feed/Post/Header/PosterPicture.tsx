import { useContext } from "react";
import { PostContext } from "../PostProvider";
import EFAvatar from "../../../EFAvatar/EFAvatar";
import AvatarTalentCard from "./AvatarTalentCard";

const PosterPicture = () => {
  const { isPostedByOrg, post } = useContext(PostContext);

  const { author } = post;

  if (isPostedByOrg) {
    return (
      <EFAvatar
        displayOnlineButton={false}
        profilePicture={post.organization.profileImage}
        size="small"
        href={`/u/${author.username}`}
      />
    );
  }

  return <AvatarTalentCard user={author} />;
};

export default PosterPicture;
