import { useContext } from "react";
import TimeAgo from "react-timeago";
import CustomDropdown from "../../../CustomDropdown/CustomDropdown";
import { PostContext } from "../PostProvider";
import Style from "./Header.module.scss";
import useMenuItems from "./useMenuItems";
import PosterPicture from "./PosterPicture";
import UserLink from "../../../UserLink/UserLink";
import { truncateString } from "../../../../helpers/GeneralHelper";
import EFUsernameBadges from "../../../EFUsernameBadges/EFUsernameBadges";

import VerifiedIcon from "../../../VerifiedIcon/VerifiedIcon";

const Header = () => {
  const {
    post,
    postType,
    postUrl,
    isCurrentUserTheAuthor,
    isCurrentUserTheAdmin,
    isPostedByOrg,
    onViewPostClicked
  } = useContext(PostContext);

  const { author } = post;

  const menuItems = useMenuItems(post, postType, isCurrentUserTheAuthor, isCurrentUserTheAdmin);

  const displayName = isPostedByOrg ? post.organization.name : author.name;
  const profilePath = isPostedByOrg
    ? `/organizations/timeline?id=${post.organization.id}`
    : `/users/posts?id=${author._id}`;

  const renderVerifiedIcon = () => {
    if (isPostedByOrg) {
      if (post.organization.verified) {
        return <VerifiedIcon organization />;
      }
    } else if (author.verified) {
      return <VerifiedIcon organization={false} />;
    }
    return <></>;
  };

  const affiliateBadge = author.badges.find(badge => badge?.badge?.slug === "AFFILIATE");
  const partnerBadge = author.badges.find(badge => badge?.badge?.slug === "PARTNER");

  return (
    <div className={Style.header}>
      <div className={Style.menu}>
        <CustomDropdown icon="darkDots" items={menuItems} customClass={Style.dropdown} />
      </div>

      <div className={Style.avatarWrapper}>
        <PosterPicture />
        <div className={Style.userInfo}>
          <h4 className={Style.name}>
            <UserLink user={author} displayName={truncateString(displayName, 40)} profilePath={profilePath} />
            {renderVerifiedIcon()}
          </h4>
          {!isPostedByOrg && (
            <EFUsernameBadges
              username={`@${author.username}`}
              showStreakBadge
              streakValue={author.streak}
              showAffiliateBadge={affiliateBadge?.badge?.active}
              affiliateBadgeText={affiliateBadge?.badge?.name}
              showPartnerBadge={partnerBadge?.badge?.active}
              partnerBadgeText={partnerBadge?.badge?.name}
            />
          )}
          <span className={Style.time}>
            <a
              href={postUrl}
              onClick={e => {
                e.preventDefault();
                onViewPostClicked(post);
              }}
            >
              <TimeAgo date={post.createdAt} minPeriod={30} />
            </a>
          </span>
        </div>
      </div>
    </div>
  );
};

export default Header;
