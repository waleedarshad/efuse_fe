import { getImage } from "../../../../helpers/GeneralHelper";
import UserTalentCard from "./UserTalentCard/UserTalentCard";
import EFAvatar from "../../../EFAvatar/EFAvatar";
import EFTippy from "../../../EFTippy/EFTippy";

interface AvatarTalentCardProps {
  user: {
    _id: string;
    name: string;
    streak: number;
    username: string;
    profileImage: string;
    verified: boolean;
    online: boolean;
    bio: string;
    twitch: {
      username: string;
    };
  };
}

const AvatarTalentCard = ({ user }: AvatarTalentCardProps) => (
  <EFTippy
    content={<UserTalentCard user={user} />}
    interactive
    delay={[550, 100]}
    animation="scale"
    onMount={() => analytics.track("TALENT_CARD_OPENED")}
  >
    <EFAvatar
      profilePicture={getImage({ url: user.profileImage }, "avatar")}
      size="small"
      href={`/u/${user?.username}`}
      online={user?.online}
      displayOnlineButton
    />
  </EFTippy>
);

export default AvatarTalentCard;
