import { faShare } from "@fortawesome/pro-solid-svg-icons";
import React from "react";
import { useSelector } from "react-redux";
import { getImage } from "../../../../../helpers/GeneralHelper";
import EFAvatar from "../../../../EFAvatar/EFAvatar";
import EFFollow from "../../../../EFFollow/EFFollow";
import EFBomb from "../../../../Icons/EFBomb/EFBomb";
import MessageButton from "../../../../MessageButton/MessageButton";
import NameAndCheckmark from "../../../../NameAndCheckmark/NameAndCheckmark";
import ShareLinkButton from "../../../../ShareLinkButton";
import { SHARE_LINK_KINDS } from "../../../../ShareLinkModal/ShareLinkModal";
import UsernameAndStreak from "../../../../UsernameAndStreak/UsernameAndStreak";
import Style from "./TalentCard.module.scss";
import EFCard from "../../../../Cards/EFCard/EFCard";
import TwitchButton from "../../../../Buttons/TwitchButton/TwitchButton";

interface UserTalentCardProps {
  user: {
    _id: string;
    name: string;
    streak: number;
    username: string;
    profileImage: string;
    verified: boolean;
    online: boolean;
    bio: string;
    twitch: {
      username: string;
    };
  };
}

const UserTalentCard = ({ user }: UserTalentCardProps) => {
  const currentUser = useSelector(state => state.auth.currentUser);
  const isOwner = currentUser?._id === user?._id;
  const isFollowed = useSelector(state => state.followers.isFollowed);

  return (
    <EFCard shadow="large">
      <div className={Style.talentCardWrapper}>
        <div className={Style.bomb}>
          <EFBomb darkBomb size="small" />
        </div>
        <div className={Style.topButtonWrapper}>
          <MessageButton userName={user?.name} userId={user?._id} />
          <ShareLinkButton
            userId={user?._id}
            shareLinkKind={SHARE_LINK_KINDS.PORTFOLIO}
            theme="roundButton"
            title={`Share ${isOwner ? "Your" : `${user?.name}'s`} Portfolio`}
            url={`https://efuse.gg/u/${user?.username}`}
            icon={faShare}
          />
        </div>

        <EFAvatar
          displayOnlineButton={false}
          profilePicture={getImage({ url: user?.profileImage }, "avatar")}
          size="medium"
          href={`/u/${user?.username}`}
        />

        <NameAndCheckmark name={user?.name} verified={user?.verified} username={user?.username} />
        <UsernameAndStreak online={user?.online} username={`@${user?.username}`} streak={user?.streak} />
        <div className={Style.buttonWrapper}>
          <EFFollow id={user?._id} isFollowed={isFollowed} />
          {user?.twitch?.username && (
            <TwitchButton href={`https://www.twitch.tv/${user.twitch.username}`} isLive={false} />
          )}
        </div>
        <div className={Style.bio}>{user?.bio}</div>
      </div>
    </EFCard>
  );
};
export default UserTalentCard;
