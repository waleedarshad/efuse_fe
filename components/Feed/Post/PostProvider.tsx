import { createContext, ReactNode, useState } from "react";
import { useSelector } from "react-redux";
import { constructPostUrl } from "../../../helpers/PostHelper";
import HypingProvider from "./Hype/HypingProvider";
import { Post } from "../../../graphql/feeds/Models";
import { FeedType } from "../types";
import { RolesEnum } from "../../../enums";

interface PostContextProps {
  postType: FeedType;
  post: Post;
  isCurrentUserTheAuthor: boolean;
  isCurrentUserTheAdmin: boolean;
  isPostedByOrg: boolean;
  postUrl: string;
  onViewPostClicked: (Post) => void;
  isPostReported: Post;
  setReportPost: (boolean) => void;
}

export const PostContext = createContext<PostContextProps>({
  postType: null,
  post: null,
  isCurrentUserTheAuthor: false,
  isCurrentUserTheAdmin: false,
  isPostedByOrg: false,
  postUrl: "",
  onViewPostClicked: () => {},
  isPostReported: null,
  setReportPost: () => {}
});

interface PostProviderProps {
  postType: FeedType;
  post: Post;
  children: ReactNode;
  onViewPostClicked: (Post) => void;
}
const PostProvider = ({ postType, post, onViewPostClicked, children }: PostProviderProps) => {
  const currentUser = useSelector(state => state.auth.currentUser);
  const currentLoggedInUserId = useSelector(state => state.auth.currentUser?.id);

  const [isPostReported, setReportPost] = useState(null);

  const contextProps = {
    postType,
    post,
    isCurrentUserTheAuthor: currentLoggedInUserId === post?.author._id,
    isCurrentUserTheAdmin: currentUser?.roles?.includes(RolesEnum.ADMIN),
    isPostedByOrg: post.kind === "orgPost",
    postUrl: constructPostUrl(post.parentFeedId),
    onViewPostClicked,
    isPostReported,
    setReportPost
  };
  return (
    <PostContext.Provider value={contextProps}>
      <HypingProvider post={post}>{children}</HypingProvider>
    </PostContext.Provider>
  );
};

export default PostProvider;
