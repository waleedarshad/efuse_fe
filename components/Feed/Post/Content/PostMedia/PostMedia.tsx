import { useContext } from "react";

import { PostContext } from "../../PostProvider";
import VideoPlayer from "./VideoPlayer";
import Style from "./PostMedia.module.scss";

const PostMedia = () => {
  const { post, postUrl, onViewPostClicked } = useContext(PostContext);

  if (post.media.length === 0 && !post.youtubeVideo) {
    return <></>;
  }

  if (post.youtubeVideo) {
    return (
      <div className={Style.postMedia}>
        <VideoPlayer videoUrl={post.youtubeVideo.videoUrl} />
      </div>
    );
  }

  const mediaFile = post.media[0].file;

  if (mediaFile.contentType.includes("video")) {
    return (
      <div className={Style.postMedia}>
        <VideoPlayer videoUrl={mediaFile.url} />
      </div>
    );
  }

  return (
    <div className={Style.postMedia}>
      <a
        href={postUrl}
        onClick={e => {
          e.preventDefault();
          onViewPostClicked(post);
        }}
      >
        <div className={Style.imageWrapper}>
          <img src={mediaFile.url} alt="user posted media" className={Style.postImage} />
        </div>
      </a>
    </div>
  );
};

export default PostMedia;
