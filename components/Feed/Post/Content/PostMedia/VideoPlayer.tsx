import { useSelector } from "react-redux";
import ReactPlayerComponent from "../../../../ReactPlayerComponent/ReactPlayerComponent";

interface VideoPlayerProps {
  videoUrl: string;
}

const VideoPlayer = ({ videoUrl }: VideoPlayerProps) => {
  const mediaSettings = useSelector(state => state.user.mediaSettings);
  const autoplay = mediaSettings ? mediaSettings.autoplay : true;
  const muted = mediaSettings ? mediaSettings.mute : true;

  return <ReactPlayerComponent url={videoUrl} controls autoplay={autoplay} muted={muted} playing={false} />;
};

export default VideoPlayer;
