import parse, { domToReact } from "html-react-parser";
import { Textfit } from "react-textfit";
import Linkify from "react-linkify";
import { useContext } from "react";
import ShowMore from "../../../../ShowMore/ShowMore";
import { parsePostText } from "../../../../../helpers/PostHelper";
import Style from "./PostText.module.scss";
import { PostContext } from "../../PostProvider";

const PostText = () => {
  const { post } = useContext(PostContext);
  const { text, mentions } = post;

  const options = {
    // eslint-disable-next-line react/display-name
    replace: ({ attribs, children }) => {
      if (!attribs) {
        return;
      }
      if (attribs.class === "mentionTagDisplay") {
        // eslint-disable-next-line consistent-return
        return domToReact(children, options);
      }

      // eslint-disable-next-line consistent-return
      return <></>;
    }
  };

  if (!text) {
    return <></>;
  }

  return (
    <div className={Style.postText}>
      <Textfit mode="multi" min={16} className={Style.textToFit}>
        <ShowMore>
          <Linkify
            componentDecorator={(decoratedHref, decoratedText, key) => (
              <a target="blank" href={decoratedHref} key={key}>
                {decoratedText}
              </a>
            )}
          >
            {parse(parsePostText(text, mentions), options)}
          </Linkify>
        </ShowMore>
      </Textfit>
    </div>
  );
};

export default PostText;
