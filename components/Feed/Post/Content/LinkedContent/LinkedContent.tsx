import isEmpty from "lodash/isEmpty";
import { useContext } from "react";
import EFMicrolink from "../../../../EFMicrolink/EFMicrolink";
import { PostContext } from "../../PostProvider";
import Style from "./LinkedContent.module.scss";

const LinkedContent = () => {
  const { post } = useContext(PostContext);

  if (!isEmpty(post.media) || isEmpty(post.openGraph)) {
    return <></>;
  }

  const { url, opengraph } = post.openGraph[0];

  if (isEmpty(opengraph)) {
    return <></>;
  }

  const { title, description, img } = opengraph;

  return (
    <div className={Style.linkedContent}>
      <EFMicrolink href={url} title={title} description={description} imageUrl={img} />
    </div>
  );
};

export default LinkedContent;
