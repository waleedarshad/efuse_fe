import PostText from "./PostText/PostText";
import PostMedia from "./PostMedia/PostMedia";
import LinkedContent from "./LinkedContent/LinkedContent";

const Content = () => (
  <div>
    <PostText />
    <PostMedia />
    <LinkedContent />
  </div>
);

export default Content;
