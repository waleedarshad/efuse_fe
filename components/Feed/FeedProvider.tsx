import { createContext, ReactNode, useState } from "react";
import { useRouter } from "next/router";
import { FeedType } from "./types";
import { Post } from "../../graphql/feeds/Models";

interface FeedContextProps {
  feedType: FeedType;
  postToExpand: Post;
  setPostToExpand: (Post) => void;
}

export const FeedContext = createContext<FeedContextProps>({
  feedType: null,
  postToExpand: null,
  setPostToExpand: () => {}
});

interface FeedProviderProps {
  feedType: FeedType;
  children: ReactNode;
}
const FeedProvider = ({ feedType, children }: FeedProviderProps) => {
  const router = useRouter();
  const [postToExpand, setPostToExpand] = useState(null);

  const expandPost = post => {
    if (post) {
      if (post?.isNewPost) {
        router.replace({ pathname: "/home", query: { feedId: post.parentFeedId } }, undefined, {
          shallow: true
        });
      } else {
        setPostToExpand(post);
      }
    } else {
      setPostToExpand(post);
    }
  };
  const contextProps = {
    feedType,
    postToExpand,
    setPostToExpand: expandPost
  };
  return <FeedContext.Provider value={contextProps}>{children}</FeedContext.Provider>;
};

export default FeedProvider;
