import Link from "next/link";
import EFRectangleButton from "../Buttons/EFRectangleButton/EFRectangleButton";

const BackButton = props => {
  const { path, ...other } = props;
  return (
    <Link href={path}>
      <EFRectangleButton {...other} as="a" href={path} />
    </Link>
  );
};

export default BackButton;
