import React, { Component } from "react";

export default class DiscordIframe extends Component {
  render() {
    const { discordServer } = this.props;
    return (
      <iframe
        src={`https://discordapp.com/widget?id=${discordServer}&theme=light`}
        style={{
          boxShadow: "0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24)",
          borderRadius: "10px",
          marginBottom: "20px"
        }}
        width="100%"
        height="500"
        allowtransparency="true"
        frameBorder="0"
      />
    );
  }
}
