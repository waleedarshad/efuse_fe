import React from "react";
import EFAvatarList from "./EFAvatarList";

export default {
  title: "Users/EFAvatarList",
  component: EFAvatarList
};

// eslint-disable-next-line react/jsx-props-no-spreading
const Story = args => <EFAvatarList {...args} />;

export const Primary = Story.bind({});
Primary.args = {
  imageSrcList: [
    "https://yt3.ggpht.com/a/AATXAJynkPnPVO1teU1292tS2f52lj5Tb0ZwM_ML4ounGg=s900-c-k-c0xffffffff-no-rj-mo",
    "https://yt3.ggpht.com/a/AATXAJynkPnPVO1teU1292tS2f52lj5Tb0ZwM_ML4ounGg=s900-c-k-c0xffffffff-no-rj-mo",
    "https://yt3.ggpht.com/a/AATXAJynkPnPVO1teU1292tS2f52lj5Tb0ZwM_ML4ounGg=s900-c-k-c0xffffffff-no-rj-mo",
    "https://yt3.ggpht.com/a/AATXAJynkPnPVO1teU1292tS2f52lj5Tb0ZwM_ML4ounGg=s900-c-k-c0xffffffff-no-rj-mo",
    "https://yt3.ggpht.com/a/AATXAJynkPnPVO1teU1292tS2f52lj5Tb0ZwM_ML4ounGg=s900-c-k-c0xffffffff-no-rj-mo",
    "https://yt3.ggpht.com/a/AATXAJynkPnPVO1teU1292tS2f52lj5Tb0ZwM_ML4ounGg=s900-c-k-c0xffffffff-no-rj-mo"
  ]
};
