import uniqueId from "lodash/uniqueId";
import EFAvatar from "../EFAvatar/EFAvatar";
import Style from "./EFAvatarList.module.scss";

const MAX_NUMBER_OF_AVATARS = 6;

const EFAvatarList = ({ imageSrcList }) => {
  const clampedImageSrcList =
    imageSrcList.length > MAX_NUMBER_OF_AVATARS ? imageSrcList.slice(0, MAX_NUMBER_OF_AVATARS) : imageSrcList;

  return (
    <div className={Style.container}>
      {clampedImageSrcList.map(imageSrc => (
        <div key={uniqueId()}>
          <EFAvatar
            size="extraSmall"
            borderTheme="white"
            profilePicture={imageSrc}
            withBorderShadow
            displayOnlineButton={false}
          />
        </div>
      ))}
    </div>
  );
};

EFAvatarList.defaultProps = {
  imageSrcList: []
};

export default EFAvatarList;
