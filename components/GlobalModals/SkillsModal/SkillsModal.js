import React, { Component } from "react";
import Modal from "../../Modal/Modal";
import AddSkills from "../../User/Portfolio/SkillsComponent/AddSkills/AddSkills";

class SkillsModal extends Component {
  state = {
    forceCloseModal: false
  };

  closeModal() {
    this.setState({
      forceCloseModal: true
    });
  }

  render() {
    const { trackAnalytics, type } = this.props;
    const { forceCloseModal } = this.state;
    const component = <AddSkills type={type} closeModal={() => this.closeModal()} />;
    return (
      <Modal
        displayCloseButton
        allowBackgroundClickClose
        openOnLoad={false}
        component={component}
        textAlignCenter={false}
        customMaxWidth="800px"
        title={`Add a ${type} Skill`}
        forceClose={forceCloseModal}
        onOpenModal={() => {
          this.setState({
            ...this.state,
            forceCloseModal: false
          });
          trackAnalytics();
        }}
      >
        {this.props.children}
      </Modal>
    );
  }
}

SkillsModal.defaultProps = {
  trackAnalytics: () => analytics.track("PORTFOLIO_SKILLS_MODAL_OPEN")
};

export default SkillsModal;
