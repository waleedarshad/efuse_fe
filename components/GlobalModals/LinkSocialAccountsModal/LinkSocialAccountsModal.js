import React from "react";
import Modal from "../../Modal/Modal";
import SocialCardList from "../../User/Portfolio/SocialCardList/SocialCardList";

const LinkSocialAccountModal = ({
  children,
  trackAnalytics = () => analytics.track("PORTFOLIO_GAME_PLATFORM_MODAL_OPEN")
}) => {
  const component = <SocialCardList />;
  return (
    <Modal
      displayCloseButton
      allowBackgroundClickClose
      openOnLoad={false}
      component={component}
      textAlignCenter={false}
      title="Add a Social Account"
      onOpenModal={() => trackAnalytics()}
    >
      {children}
    </Modal>
  );
};

export default LinkSocialAccountModal;
