import React from "react";
import Modal from "../../Modal/Modal";
import Platforms from "../../User/Portfolio/Platforms/Platforms";

const LinkSocialAccountModal = ({
  children,
  trackAnalytics = () => analytics.track("PORTFOLIO_GAME_PLATFORM_MODAL_OPEN")
}) => {
  const component = <Platforms />;
  return (
    <Modal
      displayCloseButton
      allowBackgroundClickClose
      openOnLoad={false}
      component={component}
      textAlignCenter={false}
      title="Add a Game Platform"
      onOpenModal={() => trackAnalytics()}
    >
      {children}
    </Modal>
  );
};

export default LinkSocialAccountModal;
