import React, { Component } from "react";
import Style from "./BuyGoldModal.module.scss";
import Modal from "../../Modal/Modal";

class BuyGoldModal extends Component {
  render() {
    const component = (
      <div className={Style.goldModalContainer}>
        <div className={Style.headerContent}>
          <div className={Style.textContent}>
            <h3 className={Style.title}>This is a Gold Feature</h3>
            <p className={Style.subTitle}>
              The answer will go here etiam porta sem malesuada magna mollis euismod. Cras justo odio, dapibus ac
              facilisis in, egestas eget quam.
            </p>
          </div>
          <div className={Style.buttonContent}>
            <button className={Style.darkButton}>Learn More</button>
          </div>
        </div>
        <div className={Style.goldPromotionContent}>
          <div className={Style.promotionCard}>
            <p className={Style.promotionText}>
              Become elite for only <span className={Style.dollarText}>$1.00</span> a month
            </p>
            <button className={Style.joinNowButton}>Join Now</button>
          </div>
        </div>
        <div className={Style.additionalTextContainer}>
          <p className={Style.additionalText}>
            Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh justo sit amet risus.
          </p>
        </div>
        <div className={Style.goldFooter}>
          <p className={Style.goldFooterText}>
            Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh justo sit amet risus.
          </p>
          <div className={Style.buttonContent}>
            <button className={Style.darkButton}>Invite Friends</button>
          </div>
        </div>
      </div>
    );
    return (
      <Modal
        displayCloseButton
        allowBackgroundClickClose
        leftAlignCloseButton
        customCloseButtonStyle={Style.goldButtonStyle}
        openOnLoad={false}
        component={component}
        textAlignCenter={false}
        customBackgroundColor={Style.darkCardBackground}
        customMaxWidth="800px"
        backgroundImage="https://cdn.efuse.gg/uploads/static/global/opportunity_background_image.jpg"
        onOpenModal={() => {
          analytics.track("BUY_GOLD_MODAL_OPEN");
        }}
      >
        {this.props.children}
      </Modal>
    );
  }
}

export default BuyGoldModal;
