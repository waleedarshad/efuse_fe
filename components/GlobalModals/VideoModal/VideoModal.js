import React from "react";
import Modal from "../../Modal/Modal";

import ReactPlayerComponent from "../../ReactPlayerComponent/ReactPlayerComponent";

const VideoModal = props => {
  const { url, children } = props;
  const component = <ReactPlayerComponent url={url} autoplay muted={false} controls playing />;
  return (
    <Modal
      displayCloseButton
      allowBackgroundClickClose
      openOnLoad={false}
      component={component}
      textAlignCenter={false}
      customMaxWidth="1000px"
      noPadding
    >
      {children}
    </Modal>
  );
};

export default VideoModal;
