import React, { useState } from "react";
import { connect } from "react-redux";
import { Form, Col } from "react-bootstrap";

import InputRow from "../../InputRow/InputRow";
import InputLabel from "../../InputLabel/InputLabel";
import InputField from "../../InputField/InputField";
import ActionButtonGroup from "../../Layouts/Internal/ActionButtonGroup/ActionButtonGroup";
import { setDiscordWebhook } from "../../../store/actions/userActions";
import Style from "./AddDiscordWebhook.module.scss";
import Modal from "../../Modal/Modal";

const AddDiscordWebhook = props => {
  const [validated, setValidated] = useState(false);
  const [discordWebHook, setDiscordWebHookState] = useState({
    name: "",
    url: ""
  });
  const [forceCloseModal, setForceClose] = useState(false);
  const { inProgress } = props;
  const { name, url } = discordWebHook;

  const onChange = event => {
    setDiscordWebHookState({
      ...discordWebHook,
      [event.target.name]: event.target.value
    });
  };

  const onSubmit = formEvent => {
    formEvent.preventDefault();
    if (!validated) {
      setValidated(true);
    }
    if (formEvent.currentTarget.checkValidity() && discordWebHook) {
      props.setDiscordWebhook({ discordWebHook });
      setForceClose(true);
    }
  };

  const discordInput = (
    <>
      <div className={Style.imageContainer}>
        <div className={Style.imageBody}>
          <p className={Style.title}>Step 1</p>
          <p className={Style.desc}>Click the down arrow on your server to open more options.</p>
          <img
            className={Style.step1}
            src="https://cdn.efuse.gg/uploads/static/discord/discord_step1.png"
            alt="discord step 1"
          />
        </div>
        <div className={Style.imageBody}>
          <p className={Style.title}>Step 2</p>
          <p className={Style.desc}>
            Select the <b>Server Settings</b> option.
          </p>
          <img
            className={Style.step2}
            src="https://cdn.efuse.gg/uploads/static/discord/discord_step2.png"
            alt="discord step 2"
          />
        </div>
        <div className={Style.imageBody}>
          <p className={Style.title}>Step 3</p>
          <p className={Style.desc}>
            Click the <b>Integrations</b> option and then select <b>Create Webhook</b> or <b>View Webhooks</b>.
          </p>
          <img
            className={Style.step1}
            src="https://cdn.efuse.gg/uploads/static/discord/hook_step3.png"
            alt="discord step 3"
          />
        </div>
        <div className={Style.imageBody}>
          <p className={Style.title}>Step 4</p>
          <p className={Style.desc}>
            When creating your new webhook select the channel you would like to link (
            <i>when creating a post, this will be the channel the post is sent to</i>
            ). Lastly, copy the webhook URL and paste it below.
          </p>
          <img
            className={Style.step2}
            src="https://cdn.efuse.gg/uploads/static/discord/hook_step4.png"
            alt="discord step 4"
          />
        </div>
      </div>
      <br />
      <Form noValidate validated={validated} onSubmit={onSubmit}>
        <InputRow>
          <Col sm={6}>
            <InputLabel theme="internal">Webhook Name</InputLabel>
            <InputField
              name="name"
              placeholder="ex: eFuse General Channel"
              theme="internal"
              size="lg"
              onChange={onChange}
              value={name}
              required
              errorMessage="A webhook name is required"
              maxLength={80}
            />
          </Col>
          <Col sm={6}>
            <InputLabel theme="internal">Discord Webhook URL</InputLabel>
            <InputField
              name="url"
              placeholder="ex: https://discordapp.com/api/webhooks/684854544878010369/RmO5Jto2qbBFtcxes2YI7CVehKQ-BMEnOgTLHavgG15_OHmEZHm8xOOJhXdkQlOUyTW8"
              theme="internal"
              size="lg"
              onChange={onChange}
              value={url}
              required
              errorMessage="Discord Webhook is required"
            />
          </Col>
        </InputRow>
        <ActionButtonGroup onCancel={() => setForceClose(true)} disabled={inProgress} />
      </Form>
    </>
  );
  return (
    <Modal
      displayCloseButton
      allowBackgroundClickClose={false}
      openOnLoad={false}
      component={discordInput}
      textAlignCenter={false}
      customMaxWidth="800px"
      title=""
      forceClose={forceCloseModal}
      onOpenModal={() => {
        setForceClose(false);
      }}
    >
      {props.children}
    </Modal>
  );
};

const mapStateToProps = state => ({
  inProgress: state.portfolio.inProgress
});

export default connect(mapStateToProps, {
  setDiscordWebhook
})(AddDiscordWebhook);
