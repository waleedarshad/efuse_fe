import React, { useState } from "react";
import { connect } from "react-redux";
import { Form } from "react-bootstrap";
import Modal from "../../Modal/Modal";
import ActionButtonGroup from "../../Layouts/Internal/ActionButtonGroup/ActionButtonGroup";
import { dispatchNotification } from "../../../store/actions/notificationActions";
import { addFacebookPage, getFacebookPages } from "../../../store/actions/userActions";
import Style from "./AddFacebookPage.module.scss";

const AddFacebookPage = props => {
  const [validated, setValidated] = useState(false);
  const [facebookPage, setfacebookPageState] = useState({
    pageName: "",
    pageId: ""
  });
  const [forceCloseModal, setForceClose] = useState(false);

  const updatePage = page => {
    setfacebookPageState({
      facebookPage: {
        pageId: page.id,
        pageName: page.name
      }
    });
  };
  const onSubmit = formEvent => {
    formEvent.preventDefault();
    if (!validated) {
      setValidated({ validated: true });
    }
    if (facebookPage.pageName && facebookPage.pageId) {
      props.addFacebookPage({ data: facebookPage });
    }
  };

  const { inProgress, facebookPages } = props;

  const facebookInput = (
    <Form noValidate validated={validated} onSubmit={onSubmit}>
      <div className={Style.title}>
        {facebookPages?.length > 0 ? (
          "Select a Facebook Page"
        ) : (
          <>
            <p className={Style.important}>eFuse does not have access to any of your Facebook pages.</p>
            <p>
              To allow eFuse access to your Facebook pages, you can unlink and relink your facebook account. Please
              ensure that you select at least one Facebook page when linking your account.
            </p>
          </>
        )}
      </div>
      {facebookPages?.length > 0 && (
        <>
          <div className={Style.flex}>
            {facebookPages &&
              facebookPages.map((page, index) => {
                return (
                  <div
                    key={index}
                    className={`${Style.page} ${facebookPage.pageId == page.id && Style.active}`}
                    onClick={() => updatePage(page)}
                  >
                    <p className={Style.name}>{page.name}</p>
                    <p className={Style.category}>{page.category}</p>
                  </div>
                );
              })}
          </div>
          <ActionButtonGroup onCancel={() => setForceClose(true)} disabled={inProgress} />
        </>
      )}
    </Form>
  );

  return (
    <Modal
      displayCloseButton
      allowBackgroundClickClose={false}
      openOnLoad={false}
      component={facebookInput}
      textAlignCenter={false}
      customMaxWidth="800px"
      title=""
      forceClose={forceCloseModal}
      onOpenModal={() => {
        setForceClose(false);
        props.getFacebookPages();
      }}
    >
      {props.children}
    </Modal>
  );
};
const mapStateToProps = state => ({
  inProgress: state.portfolio.inProgress,
  facebookPages: state.user.facebookPages
});

export default connect(mapStateToProps, {
  getFacebookPages,
  addFacebookPage
})(AddFacebookPage, dispatchNotification);
