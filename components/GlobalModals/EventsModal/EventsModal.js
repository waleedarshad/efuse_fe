import React, { Component } from "react";
import Modal from "../../Modal/Modal";
import AddEvent from "../../User/Portfolio/Accolades/Events/AddEvent";

class EventsModal extends Component {
  state = {
    forceCloseModal: false
  };

  closeModal() {
    this.setState({
      forceCloseModal: true
    });
  }

  render() {
    const { edit, portfolioEvent, trackAnalytics, organizationId } = this.props;
    const { forceCloseModal } = this.state;
    const component = (
      <AddEvent
        edit={edit}
        portfolioEvent={portfolioEvent}
        closeModal={() => this.closeModal()}
        organizationId={organizationId}
      />
    );
    return (
      <Modal
        displayCloseButton
        allowBackgroundClickClose
        openOnLoad={false}
        component={component}
        textAlignCenter={false}
        customMaxWidth="800px"
        title={`${edit ? "Edit" : "Add"} Events Experience`}
        forceClose={forceCloseModal}
        onOpenModal={() => {
          this.setState({
            ...this.state,
            forceCloseModal: false
          });
          edit ? analytics.track("PORTFOLIO_EVENTS_MODAL_OPEN_FROM_EDIT") : trackAnalytics();
        }}
      >
        {this.props.children}
      </Modal>
    );
  }
}

EventsModal.defaultProps = {
  trackAnalytics: () => analytics.track("PORTFOLIO_EVENTS_MODAL_OPEN")
};

export default EventsModal;
