import React, { Component } from "react";
import Modal from "../../Modal/Modal";
import TwitchClips from "../../TwitchClips/TwitchClips";

class TwitchClipsModal extends Component {
  state = {
    forceCloseModal: false
  };

  closeModal() {
    this.setState({
      forceCloseModal: true
    });
  }

  clickClip = clip => {
    this.props.onClick(clip);
    this.closeModal();
  };

  render() {
    const { forceCloseModal } = this.state;
    const component = <TwitchClips onClick={this.clickClip} />;

    return (
      <Modal
        displayCloseButton
        allowBackgroundClickClose
        openOnLoad={false}
        component={component}
        textAlignCenter={false}
        customMaxWidth="1000px"
        title="Choose a Twitch Clip"
        forceClose={forceCloseModal}
        onOpenModal={() => {
          this.setState({
            ...this.state,
            forceCloseModal: false
          });
          analytics.track("TWITCH_ADD_CLIP_MODAL_OPENED");
        }}
        highStackOrder={this.props.highStackOrder}
      >
        {this.props.children}
      </Modal>
    );
  }
}

export default TwitchClipsModal;
