import React, { Component } from "react";
import Modal from "../../Modal/Modal";
import AddExperience from "../../User/Portfolio/EducationExperience/AddExperience/AddExperience";

class EducationExperienceModal extends Component {
  state = {
    forceCloseModal: false
  };

  closeModal() {
    this.setState({
      forceCloseModal: true
    });
  }

  render() {
    const { edit, experience, trackAnalytics } = this.props;
    const { forceCloseModal } = this.state;
    const component = <AddExperience edit={edit} experience={experience} closeModal={() => this.closeModal()} />;
    return (
      <Modal
        displayCloseButton
        allowBackgroundClickClose
        openOnLoad={false}
        component={component}
        textAlignCenter={false}
        customMaxWidth="800px"
        title={`${edit ? "Edit" : "Add"} Education Experience`}
        forceClose={forceCloseModal}
        onOpenModal={() => {
          this.setState({
            ...this.state,
            forceCloseModal: false
          });
          edit ? analytics.track("PORTFOLIO_EDUCATION_EXPERIENCE_MODAL_OPEN_FROM_EDIT") : trackAnalytics();
        }}
      >
        {this.props.children}
      </Modal>
    );
  }
}

EducationExperienceModal.defaultProps = {
  trackAnalytics: () => analytics.track("PORTFOLIO_EDUCATION_EXPERIENCE_MODAL_OPEN")
};

export default EducationExperienceModal;
