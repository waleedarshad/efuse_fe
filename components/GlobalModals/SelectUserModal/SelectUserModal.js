import React from "react";
import Modal from "../../Modal/Modal";
import SelectUserForm from "../../SelectUserForm/SelectUserForm";

const SelectUserModal = ({ title, children, submitFunction, placeholder, excludedUsers }) => {
  const component = (
    <SelectUserForm submitFunction={submitFunction} placeholder={placeholder} excludedUsers={excludedUsers} />
  );
  return (
    <Modal
      displayCloseButton
      allowBackgroundClickClose
      openOnLoad={false}
      component={component}
      textAlignCenter={false}
      title={title}
    >
      {children}
    </Modal>
  );
};

export default SelectUserModal;
