import React, { useState } from "react";
import { connect } from "react-redux";
import { Form } from "react-bootstrap";
import capitalize from "lodash/capitalize";
import upperCase from "lodash/upperCase";
import Style from "./AddPlatformModal.module.scss";
import { addPlatform } from "../../../store/actions/userActions";
import InputField from "../../InputField/InputField";
import InputLabel from "../../InputLabel/InputLabel";
import Modal from "../../Modal/Modal";

const AddPlatformModal = props => {
  const { platform, addPlatform } = props;

  const [validated, setValidated] = useState(false);
  const [username, setUsername] = useState("");
  const [forceCloseModal, setForceClose] = useState(false);

  const onSubmit = event => {
    event.preventDefault();
    if (!validated) {
      setValidated(true);
    }
    if (username == "") {
      return;
    }
    if (event.currentTarget.checkValidity()) {
      if (platform == "battlenet") {
        const data = {
          displayName: username,
          platform: "battlenet"
        };
        addPlatform(data);
      }
      if (platform == "xbox") {
        const data = {
          displayName: username,
          platform: "xbl"
        };
        addPlatform(data);
      }
      if (platform == "playstation") {
        const data = {
          displayName: username,
          platform: "psn"
        };
        addPlatform(data);
      }
      // track when platform is added
      analytics.track(`PLATFORM_LINKED_${upperCase(platform)}`, {
        [platform]: username
      });

      closeModal();
    }
  };

  const closeModal = () => {
    setForceClose(true);
  };

  const onChange = event => {
    setUsername(event.target.value);
  };

  const formInput = (
    <Form validated={validated} onSubmit={onSubmit}>
      <InputLabel theme="darkColor">Username</InputLabel>
      <InputField
        name="username"
        placeholder={`Add ${capitalize(platform)} Username`}
        theme="whiteShadow"
        size="lg"
        onChange={onChange}
        value={username}
        required
        errorMessage={`${capitalize(platform)} username is required`}
        maxLength={50}
      />

      <button className={Style.blueButton}>Submit</button>
    </Form>
  );

  return (
    <Modal
      displayCloseButton
      allowBackgroundClickClose={false}
      openOnLoad={false}
      component={formInput}
      textAlignCenter={false}
      customMaxWidth="800px"
      title={`Link ${capitalize(platform)}`}
      forceClose={forceCloseModal}
      onOpenModal={() => {
        setForceClose(false);
      }}
    >
      {props.children}
    </Modal>
  );
};

export default connect(null, { addPlatform })(AddPlatformModal);
