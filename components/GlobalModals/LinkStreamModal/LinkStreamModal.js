import React, { Component } from "react";
import { connect } from "react-redux";
import { Form } from "react-bootstrap";
import { faLink } from "@fortawesome/pro-solid-svg-icons";

import Style from "./LinkStreamModal.module.scss";
import Auth from "../../User/Portfolio/VideoComponent/Auth/Auth";
import { updateVideoInfo } from "../../../store/actions/portfolioActions";
import InputField from "../../InputField/InputField";
import InputLabel from "../../InputLabel/InputLabel";
import ActionButtonGroup from "../../Layouts/Internal/ActionButtonGroup/ActionButtonGroup";
import AddButton from "../../User/Portfolio/AddButton/AddButton";
import Modal from "../../Modal/Modal";
import { withCdn } from "../../../common/utils";

class LinkStreamModal extends Component {
  state = {
    forceCloseModal: false,
    validated: false,
    valuesSet: false,
    youtubeSelected: false,
    userYoutube: {
      youtubeChannelId: null
    }
  };

  onChange = event => {
    this.setState({
      userYoutube: {
        ...this.state.userYoutube,
        [event.target.name]: event.target.value
      }
    });
  };

  onSubmit = event => {
    event.preventDefault();
    const { validated } = this.state;
    if (!validated) {
      this.setState({ validated: true });
    }
    if (event.currentTarget.checkValidity()) {
      this.props.updateVideoInfo({ ...this.state.userYoutube });
      this.closeModal();
    }
  };

  showYoutube = () => {
    this.setState({
      ...this.state,
      youtubeSelected: !this.state.youtubeSelected
    });
  };

  closeModal() {
    this.setState({
      forceCloseModal: true
    });
  }

  render() {
    const { user, trackAnalytics } = this.props;
    const { validated, userYoutube, youtubeSelected, forceCloseModal } = this.state;
    const { youtubeChannelId } = userYoutube;
    const component = (
      <>
        <p className={Style.subText}>
          *To apply to this opportunity, you must have at least one of the following stream accounts linked.
        </p>
        <div className={Style.container}>
          <Auth user={user} button="twitch" isVerified={user?.twitchVerified} />
        </div>
        <div className={Style.container}>
          <AddButton
            desc="Link YouTube Channel ID"
            icon={faLink}
            image={withCdn("/static/images/youtube_icon.png")}
            onClick={this.showYoutube}
          />
        </div>
        {youtubeSelected && (
          <div className={Style.container}>
            <Form noValidate validated={validated} onSubmit={this.onSubmit}>
              <InputLabel theme="internal">Add Channel ID</InputLabel>
              <InputField
                name="youtubeChannelId"
                placeholder="Add Youtube Channel ID"
                theme="internal"
                size="lg"
                onChange={this.onChange}
                value={youtubeChannelId}
              />
              <ActionButtonGroup />
            </Form>
          </div>
        )}
      </>
    );
    return (
      <Modal
        displayCloseButton
        allowBackgroundClickClose
        openOnLoad={false}
        component={component}
        textAlignCenter={false}
        // customMaxWidth="800px"
        title="Link a Stream"
        forceClose={forceCloseModal}
        onOpenModal={() => {
          this.setState({
            ...this.state,
            forceCloseModal: false
          });
          trackAnalytics();
        }}
      >
        {this.props.children}
      </Modal>
    );
  }
}

LinkStreamModal.defaultProps = {
  trackAnalytics: () => analytics.track("PORTFOLIO_STREAM_MODAL_OPEN")
};

const mapStateToProps = state => ({});

export default connect(mapStateToProps, { updateVideoInfo })(LinkStreamModal);
