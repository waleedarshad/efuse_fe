import React, { Component } from "react";
import Modal from "../../Modal/Modal";
import AddHonor from "../../User/Portfolio/Accolades/Honors/AddHonor";

class HonorsModal extends Component {
  state = {
    forceCloseModal: false
  };

  closeModal() {
    this.setState({
      forceCloseModal: true
    });
  }

  render() {
    const { edit, portfolioHonor, trackAnalytics, organizationId } = this.props;
    const { forceCloseModal } = this.state;
    const component = (
      <AddHonor
        edit={edit}
        portfolioHonor={portfolioHonor}
        organizationId={organizationId}
        closeModal={() => this.closeModal()}
      />
    );
    return (
      <Modal
        displayCloseButton
        allowBackgroundClickClose
        openOnLoad={false}
        component={component}
        textAlignCenter={false}
        customMaxWidth="800px"
        title={`${edit ? "Edit" : "Add"} Honors Experience`}
        forceClose={forceCloseModal}
        onOpenModal={() => {
          this.setState({
            ...this.state,
            forceCloseModal: false
          });
          edit ? analytics.track("PORTFOLIO_HONORS_MODAL_OPEN_FROM_EDIT") : trackAnalytics();
        }}
      >
        {this.props.children}
      </Modal>
    );
  }
}

HonorsModal.defaultProps = {
  trackAnalytics: () => analytics.track("PORTFOLIO_HONORS_MODAL_OPEN")
};

export default HonorsModal;
