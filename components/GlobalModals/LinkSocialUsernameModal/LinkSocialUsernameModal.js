import React, { Component } from "react";
import { connect } from "react-redux";
import { Form } from "react-bootstrap";
import capitalize from "lodash/capitalize";
import Modal from "../../Modal/Modal";
import { addSnapchatUsername, addYoutubeUsername, addTiktokUsername } from "../../../store/actions/portfolioActions";
import InputField from "../../InputField/InputField";
import InputLabel from "../../InputLabel/InputLabel";
import Style from "./LinkSocialUsernameModal.module.scss";

class LinkSocial extends Component {
  state = {
    validated: false,
    valuesSet: false,
    username: "",
    forceCloseModal: false
  };

  onSubmit = event => {
    const { social } = this.props;
    const { validated, username } = this.state;
    if (!validated) {
      this.setState({ validated: true });
    }
    if (username == "") {
      return;
    }
    if (event.currentTarget.checkValidity()) {
      analytics.track(`EXTERNAL_ACCOUNT_LINKED_${social.toUpperCase()}`, {
        [social]: username
      });

      if (social == "snapchat") {
        const data = { displayName: username };
        this.props.addSnapchatUsername(data);
      }
      if (social == "youtube") {
        const data = { youtubeUsername: username };
        this.props.addYoutubeUsername(data);
      }
      if (social == "tiktok") {
        const data = { tiktokUsername: username };
        this.props.addTiktokUsername(data);
      }
      this.closeModal();
    }
  };

  closeModal = () => {
    this.setState({
      forceCloseModal: true
    });
  };

  onChange = event => {
    this.setState({
      username: event.target.value
    });
  };

  render() {
    const { social } = this.props;
    const { validated, username, forceCloseModal } = this.state;
    const component = (
      <Form validated={validated} onSubmit={this.onSubmit}>
        <InputLabel theme="internal">Username</InputLabel>
        <InputField
          name="username"
          placeholder={`Add ${capitalize(social)} Username`}
          theme="internal"
          size="lg"
          onChange={this.onChange}
          value={username}
          required
          errorMessage={`${capitalize(social)} username is required`}
          maxLength={50}
        />
        <div className={Style.buttonWrapper}>
          <button className={Style.blueButton}>Submit</button>
        </div>
      </Form>
    );
    return (
      <Modal
        displayCloseButton
        allowBackgroundClickClose={false}
        openOnLoad={false}
        component={component}
        textAlignCenter={false}
        customMaxWidth="800px"
        title={`Link ${capitalize(social)}`}
        forceClose={forceCloseModal}
        onOpenModal={() => {
          this.setState({
            ...this.state,
            forceCloseModal: false
          });
        }}
      >
        {this.props.children}
      </Modal>
    );
  }
}

const mapStateToProps = state => ({});

export default connect(mapStateToProps, {
  addSnapchatUsername,
  addYoutubeUsername,
  addTiktokUsername
})(LinkSocial);
