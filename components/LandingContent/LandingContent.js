import React from "react";

import Style from "./LandingContent.module.scss";

const LandingContent = ({ title, description, imageUrl, alt = "", bottomComponent, id, customClass }) => {
  return (
    <div className={`${Style.containerTwo} ${customClass}`} id={id}>
      <div className={Style.cardContainer}>
        <div className={Style.title}>
          <b>{title}</b>
        </div>
        <div className={Style.content}>
          <p className={Style.description}>{description}</p>
          {imageUrl && <div className={`${Style.img}`} style={{ backgroundImage: `url(${imageUrl})` }} />}
          {bottomComponent && <div className={Style.bottomInfo}>{bottomComponent}</div>}
        </div>
      </div>
    </div>
  );
};
export default LandingContent;
