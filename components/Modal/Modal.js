/* eslint-disable jsx-a11y/click-events-have-key-events, jsx-a11y/no-static-element-interactions */
import React from "react";
import ReactDOM from "react-dom";
import { withRouter } from "next/router";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/pro-solid-svg-icons";
import { disableBodyScroll, enableBodyScroll, clearAllBodyScrollLocks } from "body-scroll-lock";
import Style from "./Modal.module.scss";

class Modal extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isOpen: false
    };

    this.targetRef = React.createRef();
    this.targetElement = undefined;
    this.clearState = this.clearState.bind(this);
  }

  componentDidUpdate() {
    const { openOnLoad } = this.props;
    const { isOpen } = this.state;

    if (!this.targetElement && this.targetRef.current) {
      this.targetElement = this.targetRef.current;
      this.lockBodyScroll();
    }

    if (openOnLoad && !isOpen) {
      this.lockBodyScroll();

      // This is only used to update the state one based on isOpen, should be safe
      // eslint-disable-next-line react/no-did-update-set-state
      this.setState({
        isOpen: true
      });
    }
  }

  componentWillUnmount() {
    if (this.TargetElement) {
      enableBodyScroll(this.TargetElement);
    }
  }

  get TargetElement() {
    if (!this.targetElement) {
      this.targetElement = this.targetRef.current;
    }
    return this.targetElement;
  }

  unlockBodyScroll = () => {
    if (this.TargetElement) {
      enableBodyScroll(this.TargetElement);
    }
  };

  lockBodyScroll = () => {
    if (this.TargetElement) {
      disableBodyScroll(this.TargetElement);
    }
  };

  openModal(e) {
    if (e) e.stopPropagation();

    const { onOpenModal } = this.props;
    if (onOpenModal) onOpenModal();

    this.lockBodyScroll();
    this.setState({ isOpen: true });
  }

  closeModal(e) {
    if (e) e.stopPropagation();

    const { onCloseModal } = this.props;
    if (onCloseModal) onCloseModal();

    this.unlockBodyScroll();
    this.setState({ isOpen: false });
  }

  clearState() {
    this.setState({
      isOpen: false
    });
  }

  render() {
    const {
      children,
      displayCloseButton,
      component,
      title,
      allowBackgroundClickClose,
      leftAlignCloseButton,
      customCloseButtonStyle,
      forceClose,
      textAlignCenter,
      customMaxWidth,
      hideOverflow,
      childSpanStyle,
      customBackgroundColor,
      backgroundImage,
      noPadding,
      highStackOrder
    } = this.props;

    const { isOpen } = this.state;
    if (forceClose) this.unlockBodyScroll();

    const modal = (
      <div
        className={`${Style.modalBackground} ${Style[highStackOrder]}`}
        onClick={e => {
          if (allowBackgroundClickClose) this.closeModal(e);
        }}
        ref={this.targetRef}
      >
        <div
          className={`${Style.modal} ${textAlignCenter && Style.textAlignCenter} ${hideOverflow &&
            Style.hideOverflow} ${noPadding && Style.noPadding}`}
          style={{
            maxWidth: customMaxWidth,
            backgroundColor: customBackgroundColor
          }}
          onClick={e => {
            e.stopPropagation();
          }}
        >
          {backgroundImage && <img className={Style.fullBackgroundImage} src={backgroundImage} alt="background" />}
          {displayCloseButton && (
            <div
              className={`${Style.modalCloseButton} ${leftAlignCloseButton &&
                Style.leftAlignCloseButton} ${customCloseButtonStyle && customCloseButtonStyle}`}
              onClick={() => {
                this.closeModal();
              }}
            >
              <FontAwesomeIcon icon={faTimes} />
            </div>
          )}
          {title && <h3 className={Style.title}>{title}</h3>}
          {React.cloneElement(component, {
            closeModal: () => {
              this.closeModal();
            }
          })}
        </div>
      </div>
    );

    if (typeof window && isOpen && !forceClose) {
      return (
        <>
          {children}
          {ReactDOM.createPortal(modal, document.getElementById("modal-root"))}
        </>
      );
    }

    return (
      <span
        className={childSpanStyle}
        onClick={e => {
          this.openModal(e);
        }}
      >
        {children}
      </span>
    );
  }
}

Modal.defaultProps = {
  allowBackgroundClickClose: true,
  backgroundImage: null,
  children: null,
  childSpanStyle: null,
  component: null,
  customBackgroundColor: null,
  customCloseButtonStyle: null,
  customMaxWidth: null,
  displayCloseButton: true,
  forceClose: false,
  hideOverflow: false,
  leftAlignCloseButton: false,
  noPadding: null,
  onCloseModal: null,
  onOpenModal: null,
  openOnLoad: false,
  textAlignCenter: true,
  title: null
};

export default withRouter(Modal);
