import React from "react";
import SelectBox from "../../../SelectBox/SelectBox";
import { formattedCountries } from "../../../Settings/Recruiting/dropdownData";

const SelectCountry = ({ onChange, value }) => {
  return (
    <SelectBox
      disabled={false}
      name="country"
      options={formattedCountries}
      validated={false}
      theme="whiteShadow"
      onChange={onChange}
      value={value}
    />
  );
};

export default SelectCountry;
