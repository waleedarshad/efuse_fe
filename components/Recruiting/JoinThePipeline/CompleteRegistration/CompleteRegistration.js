import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faGamepadAlt, faTrophy, faStar } from "@fortawesome/pro-regular-svg-icons";

import PipelineBackground from "../PipelineBackground/PipelineBackground";
import Style from "./CompleteRegistration.module.scss";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";

const CompleteRegistration = ({ closeModal, back }) => {
  return (
    <>
      <PipelineBackground title="YOU HAVE OFFICIALLY JOINED THE PIPELINE!" />
      <div className={Style.contentWrapper}>
        <p className={Style.subTitle}>
          Welcome and congratulations - this is the first step in your journey being discovered as an ELITE esports
          competitor! Your portfolio is now available to collegiate coaches all over the world. With the Pipeline, you
          now have access to exclusive leaderboards showing where you rank amongst your peers in your favorite game
          titles.
          <br />
          <br />
          Along with the leaderboards, you now have access to organizations, opportunities, and the lounge on eFuse for
          more esports related content. You can return to this page any time to update your Pipeline portfolio.
        </p>
        <div className={Style.iconsWrapper}>
          <FontAwesomeIcon icon={faGamepadAlt} className={Style.icon} />
          <FontAwesomeIcon icon={faTrophy} className={Style.icon} />
          <FontAwesomeIcon icon={faStar} className={Style.icon} />
        </div>
        <div className={Style.buttonWrapper}>
          <EFRectangleButton width="fullWidth" text="Complete Registration" onClick={() => closeModal()} />
        </div>
        <div className={Style.buttonWrapper}>
          <EFRectangleButton text="Back" colorTheme="transparent" shadowTheme="none" onClick={back} />
        </div>
      </div>
    </>
  );
};

export default CompleteRegistration;
