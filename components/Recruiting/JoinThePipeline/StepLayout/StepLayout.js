import React from "react";

import Style from "./StepLayout.module.scss";
import ActionButtons from "../ActionButtons/ActionButtons";

const StepLayout = ({ children, next, back, title, subTitle, disableNextButton }) => {
  return (
    <div className={Style.layout}>
      <h4 className={Style.title}>{title}</h4>
      <p className={Style.subTitle}>{subTitle}</p>
      <div className={Style.row}>
        <div className={Style.content}>{children}</div>
        <div className={Style.buttons}>
          <ActionButtons disableNextButton={disableNextButton} onNext={next} onBack={back} />
        </div>
      </div>
    </div>
  );
};

StepLayout.defaultProps = {
  aligned: true
};

export default StepLayout;
