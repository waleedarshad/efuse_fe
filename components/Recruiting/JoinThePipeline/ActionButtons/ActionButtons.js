import React from "react";

import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";
import Style from "./ActionButtons.module.scss";

const ActionButtons = ({ onBack, onNext, disableNextButton, alignRight }) => {
  return (
    <div className={`${alignRight ? Style.alignRight : Style.buttonsWrapper}`}>
      {onBack && (
        <div className={Style.actionButton}>
          <EFRectangleButton colorTheme="transparent" text="Back" onClick={onBack} />
        </div>
      )}
      <div className={Style.actionButton}>
        <EFRectangleButton text="Next" onClick={onNext} disabled={disableNextButton} />
      </div>
    </div>
  );
};

export default ActionButtons;
