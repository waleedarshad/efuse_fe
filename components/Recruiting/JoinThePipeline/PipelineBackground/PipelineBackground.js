import React from "react";

import EFImage from "../../../EFImage/EFImage";
import Style from "./PipelineBackground.module.scss";

const PipelineBackground = ({ title, subtitle }) => {
  return (
    <div className={Style.backgroundImage}>
      <div className={Style.backgroundFade}>
        <div className={Style.logo}>
          <EFImage src="https://cdn.efuse.gg/uploads/pipeline/eFusePipelineLogo.png" width={220} height={198} />
        </div>
        <h3 className={Style.title}>{title}</h3>
        <p className={Style.subTitle}>{subtitle}</p>
      </div>
    </div>
  );
};

export default PipelineBackground;
