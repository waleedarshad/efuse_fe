import React from "react";
import { useDispatch } from "react-redux";
import { faPlus } from "@fortawesome/pro-solid-svg-icons";

import EFCircleIconButton from "../../../../Buttons/EFCircleIconButton/EFCircleIconButton";
import HonorsModal from "../../../../GlobalModals/HonorsModal/HonorsModal";
import InputField from "../../../../InputField/InputField";
import InputLabelWithHelpTooltip from "../../../../InputLabelWithHelpTooltip/InputLabelWithHelpTooltip";
import DisplayHonorAndEvent from "../DisplayHonorAndEvent/DisplayHonorAndEvent";
import { removeData } from "../../../../../store/actions/portfolioActions";
import Style from "../HonorsAndEvents.module.scss";

const Honors = ({ honors }) => {
  const dispatch = useDispatch();

  const displayHonors = honors?.map((honor, index) => {
    return (
      <DisplayHonorAndEvent
        data={honor}
        key={index}
        onRemove={() => dispatch(removeData("/portfolio/portfolio_honor", honor._id))}
      />
    );
  });

  return (
    <div className={Style.honorsContainer}>
      <InputLabelWithHelpTooltip
        labelText="Add an event"
        tooltipText="Enter an event you have participated in if applicable"
        labelTheme="smallText"
      />
      <HonorsModal>
        <div className={Style.addHonorAndEvent}>
          <InputField placeholder="Enter an event " />
          <div className={Style.addButton}>
            <EFCircleIconButton icon={faPlus} />
          </div>
        </div>
      </HonorsModal>
      {displayHonors}
    </div>
  );
};

export default Honors;
