import React from "react";
import { faTrash } from "@fortawesome/pro-solid-svg-icons";

import EFCircleIconButton from "../../../../Buttons/EFCircleIconButton/EFCircleIconButton";
import ConfirmAlert from "../../../../ConfirmAlert/ConfirmAlert";
import InputField from "../../../../InputField/InputField";
import Style from "../HonorsAndEvents.module.scss";

const DisplayHonorAndEvent = ({ data, onRemove }) => {
  return (
    <div className={Style.addHonorAndEvent}>
      <InputField value={data.title} disabled />
      <div className={Style.addButton}>
        <ConfirmAlert title="This action can't be undone" message="Are you sure you want to remove?" onYes={onRemove}>
          <EFCircleIconButton icon={faTrash} />
        </ConfirmAlert>
      </div>
    </div>
  );
};

export default DisplayHonorAndEvent;
