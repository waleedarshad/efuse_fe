import React from "react";
import { useDispatch } from "react-redux";
import { faPlus } from "@fortawesome/pro-solid-svg-icons";

import EventsModal from "../../../../GlobalModals/EventsModal/EventsModal";
import EFCircleIconButton from "../../../../Buttons/EFCircleIconButton/EFCircleIconButton";
import InputField from "../../../../InputField/InputField";
import InputLabelWithHelpTooltip from "../../../../InputLabelWithHelpTooltip/InputLabelWithHelpTooltip";
import { removeData } from "../../../../../store/actions/portfolioActions";
import DisplayHonorAndEvent from "../DisplayHonorAndEvent/DisplayHonorAndEvent";
import Style from "../HonorsAndEvents.module.scss";

const Events = ({ events }) => {
  const dispatch = useDispatch();

  const displayEvents = events?.map((event, index) => {
    return (
      <DisplayHonorAndEvent
        data={event}
        key={index}
        onRemove={() => dispatch(removeData("/portfolio/portfolio_event", event._id))}
      />
    );
  });

  return (
    <div>
      <InputLabelWithHelpTooltip
        labelText="Add a honor"
        tooltipText="Enter a honor you have received if applicable"
        labelTheme="smallText"
      />
      <EventsModal>
        <div className={Style.addHonorAndEvent}>
          <InputField placeholder="Enter a honor " />
          <div className={Style.addButton}>
            <EFCircleIconButton icon={faPlus} />
          </div>
        </div>
      </EventsModal>
      {displayEvents}
    </div>
  );
};

export default Events;
