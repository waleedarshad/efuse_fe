import React from "react";
import { useSelector } from "react-redux";

import Events from "./Events/Events";
import Honors from "./Honors/Honors";

const HonorsAndEvents = () => {
  const honors = useSelector(state => state.user.currentUser.portfolioHonors);
  const events = useSelector(state => state.user.currentUser.portfolioEvents);

  return (
    <div>
      <Honors honors={honors} />
      <Events events={events} />
    </div>
  );
};

export default HonorsAndEvents;
