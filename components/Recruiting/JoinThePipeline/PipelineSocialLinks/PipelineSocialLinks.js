import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { faSnapchat, faTwitch, faDiscord, faYoutube, faTwitter, faTiktok } from "@fortawesome/free-brands-svg-icons";

import { SocialCard } from "../../../User/Portfolio/SocialCardList/SocialCard/SocialCard";
import Style from "./PipelineSocialLinks.module.scss";
import { getCurrentUser } from "../../../../store/actions/common/userAuthActions";

const PipelineSocialLinks = () => {
  const user = useSelector(state => state.user.currentUser);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getCurrentUser());
  }, []);

  return (
    <div>
      <div className={Style.flexContainer}>
        <SocialCard
          social="twitch"
          icon={faTwitch}
          noStats={!user?.twitchVerified}
          username={user && user?.twitchVerified && user?.twitchUserName}
          count={user?.twitchFollowersCount}
          countLabel="Followers"
          link={user?.twitchUserName && `https://www.twitch.tv/${user?.twitchUserName}`}
          verified={user?.twitchVerified}
        />
        <SocialCard
          social="twitter"
          icon={faTwitter}
          noStats={!user?.twitterVerified}
          username={user && user?.twitterVerified && user?.twitterUsername}
          count={user?.twitterfollowersCount}
          countLabel="Followers"
          link={user?.twitterUsername && `https://twitter.com/${user?.twitterUsername}`}
          verified={user?.twitterVerified}
        />
        <SocialCard
          social="discord"
          icon={faDiscord}
          noStats={!user?.discordVerified}
          username={user && user?.discordVerified && user?.discordUserName}
          verified={user?.discordVerified}
        />
        <SocialCard
          social="snapchat"
          icon={faSnapchat}
          verified={user?.snapchat?.displayName}
          username={user && user?.snapchat?.displayName}
          linkModal
          isOwner
        />
        <SocialCard
          social="youtube"
          icon={faYoutube}
          verified={user?.googleVerified}
          username={user?.youtubeChannel?.title || user?.google?.name}
          isOwner
          count={user?.youtubeChannel?.statistics?.subscriberCount}
          countLabel="Subscribers"
          link={user?.youtubeChannel?.id && `https://www.youtube.com/channel/${user?.youtubeChannel?.id}`}
        />
        <SocialCard
          social="tiktok"
          icon={faTiktok}
          verified={user?.tikTok?.tiktokUsername}
          username={user && user?.tikTok?.tiktokUsername}
          linkModal
          link={`https://www.tiktok.com/@${user?.tikTok?.tiktokUsername}`}
          isOwner
        />
      </div>
    </div>
  );
};

export default PipelineSocialLinks;
