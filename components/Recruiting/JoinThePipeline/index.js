import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import ProgressBar from "react-bootstrap/ProgressBar";

import intersection from "lodash/intersection";
import IntroStep from "./IntroStep/IntroStep";
import RecruitInfo from "./RecruitInfo/RecruitInfo";
import Style from "./JoinThePipeline.module.scss";
import AcademicInfo from "./AcademicInfo/AcademicInfo";
import GamingInfo from "./GamingInfo/GamingInfo";
import { getRecruitmentProfile, updateRecruitmentProfile } from "../../../store/actions/recruitingActions";
import GameStats from "./GameStats/GameStats";
import HonorsAndEvents from "./HonorsAndEvents/HonorsAndEvents";
import { getCurrentUser } from "../../../store/actions/common/userAuthActions";
import ClipsAndVideos from "./ClipsAndVideos/ClipsAndVideos";
import CompleteRegistration from "./CompleteRegistration/CompleteRegistration";
import joinPipelineSteps from "./joinPipelineSteps";
import PipelineSocialLinks from "./PipelineSocialLinks/PipelineSocialLinks";
import StepLayout from "./StepLayout/StepLayout";
import {
  getAimlabAuthStatusForLoggedUser,
  getValorantAuthStatusForLoggedUser
} from "../../../store/actions/externalAuth/externalAuthActions";
import { sendNotification } from "../../../helpers/FlashHelper";

const JoinThePipeline = ({ startStep, closeModal }) => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getRecruitmentProfile());
    dispatch(getCurrentUser());
  }, []);

  const recruitProfile = useSelector(state => state.recruiting.profile);

  const [currentStep, setCurrentStep] = useState(startStep);
  const [recruitingProfile, setRecruitingProfile] = useState(recruitProfile);

  const {
    name,
    graduationClass,
    addressLine1,
    city,
    state,
    zipCode,
    country,
    gradingScale,
    highSchoolGPA,
    highSchool,
    primaryGame,
    highSchoolClubTeam,
    primaryGameHoursPlayed
  } = recruitingProfile || {};

  const onChange = e => {
    setRecruitingProfile({
      ...recruitingProfile,
      [e.target.name]: e.target.value
    });
  };

  const changeStep = (value, saveProfile) => () => {
    setCurrentStep(value);
    analytics.track("PIPELINE_APPLICATION_STEP_VIEW", { step: value });

    if (saveProfile) {
      dispatch(updateRecruitmentProfile(recruitingProfile));
    }
  };

  const loadData = async () => {
    dispatch(getValorantAuthStatusForLoggedUser());
    dispatch(getAimlabAuthStatusForLoggedUser());
    dispatch(getCurrentUser());
  };

  // TODO: Eventually move auth callback logic to a common place
  const authCallback = () => {
    window.onmessage = async e => {
      const common = intersection(Object.keys(e.data), ["riot", "statespace"]);
      if (common.length === 1) {
        const title = common[0];
        if (e.data.status === "success") {
          sendNotification("Successfully verified!", "success", title);
          loadData();
        } else {
          sendNotification("Can not be verified, please try again.", "danger", title);
        }
      }
    };
  };

  useEffect(() => {
    authCallback();
    loadData();
  }, [currentStep]);

  const progressPercent = {
    [joinPipelineSteps.intro]: 0,
    [joinPipelineSteps.recruitInfo]: 15,
    [joinPipelineSteps.academicInfo]: 30,
    [joinPipelineSteps.gamingInfo]: 45,
    [joinPipelineSteps.gameStats]: 60,
    [joinPipelineSteps.honorsAndEvents]: 75,
    [joinPipelineSteps.videosAndClips]: 90,
    [joinPipelineSteps.videosAndClips]: 90,
    [joinPipelineSteps.socialLink]: 100,
    [joinPipelineSteps.complete]: 100
  };

  return (
    <>
      <div className={Style.contentContainer}>
        {currentStep === joinPipelineSteps.intro && (
          <IntroStep
            country={recruitingProfile?.country}
            onChange={onChange}
            graduationClass={recruitingProfile?.graduationClass}
            next={changeStep(joinPipelineSteps.recruitInfo)}
          />
        )}
        {currentStep === joinPipelineSteps.recruitInfo && (
          <StepLayout
            title="Your Recruit Information"
            subTitle="College coaches will use this information to learn more about and contact you."
            next={changeStep(joinPipelineSteps.academicInfo, true)}
            disableNextButton={!(name && graduationClass && addressLine1 && city && state && zipCode && country)}
          >
            <RecruitInfo recruitingProfile={recruitingProfile} onChange={onChange} />
          </StepLayout>
        )}
        {currentStep === joinPipelineSteps.academicInfo && (
          <StepLayout
            title="Academic Information"
            subTitle="Completing this section will give college coaches an understanding of where you stand academically."
            next={changeStep(joinPipelineSteps.gamingInfo, true)}
            back={changeStep(joinPipelineSteps.recruitInfo)}
            disableNextButton={!(gradingScale && highSchoolGPA && highSchool)}
          >
            <AcademicInfo recruitingProfile={recruitingProfile} onChange={onChange} />
          </StepLayout>
        )}
        {currentStep === joinPipelineSteps.gamingInfo && (
          <StepLayout
            title="Add your gaming focus"
            subTitle="Complete this section to give college coaches an idea of your skill level in one or two games."
            disableNextButton={!(primaryGame && highSchoolClubTeam)}
            next={changeStep(joinPipelineSteps.gameStats, true)}
            back={changeStep(joinPipelineSteps.academicInfo)}
          >
            <GamingInfo recruitingProfile={recruitingProfile} setRecruitingProfile={setRecruitingProfile} />
          </StepLayout>
        )}
        {currentStep === joinPipelineSteps.gameStats && (
          <StepLayout
            title="Add your gaming stats"
            subTitle="Complete this section to give college coaches an idea of your skill level in one or two games."
            next={changeStep(joinPipelineSteps.honorsAndEvents, true)}
            back={changeStep(joinPipelineSteps.gamingInfo)}
            disableNextButton={!primaryGameHoursPlayed}
          >
            <GameStats recruitingProfile={recruitingProfile} onChange={onChange} />
          </StepLayout>
        )}
        {currentStep === joinPipelineSteps.honorsAndEvents && (
          <StepLayout
            title="Honors and Events"
            subTitle="Completing this section will give college coaches information about your achievements"
            next={changeStep(joinPipelineSteps.videosAndClips)}
            back={changeStep(joinPipelineSteps.gameStats)}
          >
            <HonorsAndEvents />
          </StepLayout>
        )}
        {currentStep === joinPipelineSteps.videosAndClips && (
          <StepLayout
            title="Clips and Videos"
            subTitle="Completing this section will give college coaches a better chance to observe your skills"
            next={changeStep(joinPipelineSteps.socialLink)}
            back={changeStep(joinPipelineSteps.honorsAndEvents)}
          >
            <ClipsAndVideos
              next={changeStep(joinPipelineSteps.socialLink)}
              back={changeStep(joinPipelineSteps.honorsAndEvents)}
            />
          </StepLayout>
        )}
        {currentStep === joinPipelineSteps.socialLink && (
          <StepLayout
            title="Link social and gaming platforms"
            next={changeStep(joinPipelineSteps.complete)}
            back={changeStep(joinPipelineSteps.videosAndClips)}
          >
            <PipelineSocialLinks />
          </StepLayout>
        )}
        {currentStep === joinPipelineSteps.complete && (
          <CompleteRegistration
            closeModal={() => {
              analytics.track("PIPELINE_APPLICATION_SUBMIT");
              closeModal();
            }}
            back={changeStep(joinPipelineSteps.socialLink)}
          />
        )}
      </div>

      <ProgressBar className={Style.progressBar} now={progressPercent[currentStep]} variant="blue" />
    </>
  );
};

export default JoinThePipeline;
