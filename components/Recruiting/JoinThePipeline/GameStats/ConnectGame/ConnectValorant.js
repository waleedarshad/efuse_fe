import React from "react";
import { useSelector } from "react-redux";
import { useQuery } from "@apollo/client";
import GameCard from "../GameCard/GameCard";
import Style from "../GameStats.module.scss";
import { EXTERNAL_AUTH_SERVICES } from "../../../../../common/externalAuthServices";
import { GET_VALORANT_PROFILE } from "../../../../../graphql/ValorantStatsQuery";

const SLUG = "valorant";

const ConnectValorant = ({ startOAuth, gamesList, username, onUsernameChange }) => {
  const showValorant = useSelector(state => state.features.valorant_section);
  const isLinked = useSelector(state => state.auth?.external?.riot);

  const onCompleted = data => {
    onUsernameChange(data?.getUserById?.valorantAccountProfile?.gameName || "");
  };

  const userId = useSelector(state => state.auth?.currentUser?._id);
  const { refetch } = useQuery(GET_VALORANT_PROFILE, { variables: { id: userId }, onCompleted });

  const refresh = () => {
    refetch().then(response => onCompleted(response.data));
  };

  const game = gamesList.find(it => it.slug === SLUG);

  const auth = () => startOAuth(EXTERNAL_AUTH_SERVICES.RIOT);

  return showValorant ? (
    <div className={Style.gameCardContainer}>
      <GameCard game={game} connect={auth} isLinked={isLinked} username={username} refresh={refresh} />
    </div>
  ) : (
    <div />
  );
};

export default ConnectValorant;
