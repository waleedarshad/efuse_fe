import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import GameCard from "../GameCard/GameCard";
import Style from "../GameStats.module.scss";
import AddLeagueOfLegendsContainer from "../../../../User/Portfolio/GameStatsAccordion/LeagueOfLegends/AddLeagueOfLegends/AddLeagueOfLegendsContainer";
import { togglePortfolioModal } from "../../../../../store/actions/portfolioActions";
import { getCurrentUser } from "../../../../../store/actions/common/userAuthActions";

const SLUG = "league-of-legends";

const LOLGameStats = ({ onUsernameChange, gamesList, username }) => {
  const showLeagueOfLegends = useSelector(state => state.features.leagueoflegends_section);
  const dispatch = useDispatch();

  const lolModal = useSelector(state => state.portfolio.modal);
  const lolModalSection = useSelector(state => state.portfolio.modalSection);
  const isLinked = useSelector(state => state.user?.currentUser?.leagueOfLegends?.verified);
  const lolGameName = useSelector(state => state.user?.currentUser?.leagueOfLegends?.riotNickname);

  useEffect(() => {
    if (lolGameName !== username) {
      onUsernameChange(lolGameName);
    }
  }, [lolGameName]);

  const refresh = () => {
    dispatch(getCurrentUser());
    onUsernameChange(lolGameName);
  };

  const auth = () => dispatch(togglePortfolioModal(true, "league of legends"));
  const game = gamesList.find(it => it.slug === SLUG);
  const showModal = lolModal && lolModalSection === "league of legends";

  return showLeagueOfLegends ? (
    <div>
      <AddLeagueOfLegendsContainer show={showModal} onHide={() => dispatch(togglePortfolioModal(false))} />
      <div className={Style.gameCardContainer}>
        <GameCard game={game} connect={auth} isLinked={isLinked} username={username} refresh={refresh} />
      </div>
    </div>
  ) : (
    <div />
  );
};

export default LOLGameStats;
