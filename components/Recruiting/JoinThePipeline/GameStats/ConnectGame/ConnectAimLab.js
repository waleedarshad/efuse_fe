import React from "react";
import { useSelector } from "react-redux";
import { useQuery } from "@apollo/client";
import GameCard from "../GameCard/GameCard";
import Style from "../GameStats.module.scss";
import { EXTERNAL_AUTH_SERVICES } from "../../../../../common/externalAuthServices";
import { GET_AIMLAB_STATS_FOR_USER } from "../../../../../graphql/UserQuery";

const SLUG = "aim-lab";

const ConnectAimLab = ({ onUsernameChange, startOAuth, gamesList, username }) => {
  const showAimLab = useSelector(state => state.features.aimlab_section);
  const isLinked = useSelector(state => state.auth?.external?.statespace);

  const onCompleted = data => {
    onUsernameChange(data?.getUserById?.aimlabStats?.username || "");
  };

  const game = gamesList.find(it => it.slug === SLUG);

  const userId = useSelector(state => state.auth?.currentUser?._id);
  const { refetch } = useQuery(GET_AIMLAB_STATS_FOR_USER, { variables: { id: userId }, onCompleted });

  const refresh = () => {
    refetch().then(response => onCompleted(response.data));
  };

  const auth = () => startOAuth(EXTERNAL_AUTH_SERVICES.STATESPACE);

  return showAimLab ? (
    <div className={Style.gameCardContainer}>
      <GameCard game={game} connect={auth} isLinked={isLinked} username={username} refresh={refresh} />
    </div>
  ) : (
    <div />
  );
};

export default ConnectAimLab;
