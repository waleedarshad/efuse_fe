import React from "react";
import { Col } from "react-bootstrap";
import Style from "../GameStats.module.scss";
import InputRow from "../../../../InputRow/InputRow";
import InputLabelWithHelpTooltip from "../../../../InputLabelWithHelpTooltip/InputLabelWithHelpTooltip";
import SelectHoursPlayed from "../../SelectHoursPlayed/SelectHoursPlayed";
import SelectGameRole from "../../SelectGameRole/SelectGameRole";

const GameStatsDropdowns = ({
  gameInfo,
  hoursPlayedField,
  hoursPlayedValue,
  roleField,
  roleValue,
  onChange,
  required
}) => (
  <div className={Style.gameStatsDropdowns}>
    <InputRow>
      <Col>
        <InputLabelWithHelpTooltip
          labelText="Hours Played"
          required={required}
          tooltipText={`Select estimated hours played in ${gameInfo?.title}`}
          labelTheme="smallText"
        />
        <SelectHoursPlayed name={hoursPlayedField} value={hoursPlayedValue} onChange={onChange} />
      </Col>
      {gameInfo?.pipelineInfo?.roles?.length > 0 && (
        <Col>
          <InputLabelWithHelpTooltip
            labelText="Role"
            optional
            tooltipText={`Select the role you typically play in ${gameInfo?.title}`}
            labelTheme="smallText"
          />
          <SelectGameRole name={roleField} game={gameInfo} value={roleValue} onChange={onChange} />
        </Col>
      )}
    </InputRow>
  </div>
);

export default GameStatsDropdowns;
