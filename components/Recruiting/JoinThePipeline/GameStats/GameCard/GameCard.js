import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheckCircle, faExclamationCircle, faCircle } from "@fortawesome/pro-solid-svg-icons";

import EFCard from "../../../../Cards/EFCard/EFCard";
import Style from "./GameCard.module.scss";

const GameCard = ({ game, isLinked, username, connect, refresh }) => {
  const onClick = () => {
    if (!isLinked) {
      connect();
    } else if (!username) {
      refresh();
    }
  };

  const containerStyle = isLinked ? {} : { cursor: "pointer" };

  const renderIcon = () => {
    if (isLinked && username) {
      return <FontAwesomeIcon icon={faCheckCircle} className={`${Style.icon} ${Style.iconVerified}`} />;
    }

    if (isLinked) {
      return <FontAwesomeIcon icon={faExclamationCircle} className={`${Style.icon} ${Style.iconWarn}`} />;
    }

    return <FontAwesomeIcon icon={faCircle} className={`${Style.icon} ${Style.iconUnlinked}`} />;
  };

  return (
    <div onClick={onClick} style={containerStyle}>
      <EFCard widthTheme="fullWidth">
        <div className={Style.cardContent}>
          <div>
            {game?.logoImage?.url && (
              <img src={game?.logoImage.url} width={30} height={30} className={Style.image} alt={game?.title} />
            )}
            <img src={game?.gameImage.url} width={46} height={62} className={Style.image} alt={game?.title} />
            <div className={Style.textContainer}>
              <p className={Style.title}>{game?.title}</p>
              <div className={Style.text}>
                {isLinked ? (
                  <>
                    <p>Connected</p>
                    {username ? (
                      <>
                        {game?.title} account: {username}
                      </>
                    ) : (
                      <>
                        <p>Unable to find username for {game?.title}</p>
                        <p className={Style.tryAgain}>Click to refresh</p>
                      </>
                    )}
                  </>
                ) : (
                  <>Click to connect your stats</>
                )}
              </div>
            </div>
          </div>
          {renderIcon()}
        </div>
      </EFCard>
    </div>
  );
};

export default GameCard;
