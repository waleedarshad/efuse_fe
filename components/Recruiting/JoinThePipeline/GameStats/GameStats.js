import React from "react";

import { useQuery } from "@apollo/client";
import InputLabel from "../../../InputLabel/InputLabel";
import authenticateAccount from "../../../hoc/authenticateAccount";

import ConnectLoL from "./ConnectGame/ConnectLoL";
import ConnectValorant from "./ConnectGame/ConnectValorant";
import ConnectAimLab from "./ConnectGame/ConnectAimLab";
import { GET_PIPELINE_GAMES } from "../../../../graphql/GameQuery";

import GameStatsDropdowns from "./GameStatsDropdowns/GameStatsDropdowns";

const GameStats = ({ recruitingProfile, startOAuth, onChange }) => {
  const {
    primaryGame,
    secondaryGame,
    primaryGameHoursPlayed,
    secondaryGameHoursPlayed,
    primaryGameUsername,
    secondaryGameUsername,
    primaryGameRole,
    secondaryGameRole
  } = recruitingProfile;
  const { data } = useQuery(GET_PIPELINE_GAMES);
  const gamesList = data?.getPipelineGames || [];

  const primaryGameInfo = gamesList.find(it => primaryGame === it.slug);
  const secondaryGameInfo = gamesList.find(it => secondaryGame === it.slug);

  const setPrimaryGameUsername = username => {
    onChange({ target: { name: "primaryGameUsername", value: username || "" } });
  };

  const setSecondaryGameUsername = username => {
    onChange({ target: { name: "secondaryGameUsername", value: username || "" } });
  };

  const renderConnectCard = (gameSlug, setUsername, username) => {
    switch (gameSlug) {
      case "league-of-legends":
        return <ConnectLoL onUsernameChange={setUsername} gamesList={gamesList} username={username} />;
      case "valorant":
        return (
          <ConnectValorant
            startOAuth={startOAuth}
            onUsernameChange={setUsername}
            gamesList={gamesList}
            username={username}
          />
        );
      case "aim-lab":
        return (
          <ConnectAimLab
            startOAuth={startOAuth}
            onUsernameChange={setUsername}
            gamesList={gamesList}
            username={username}
          />
        );
      default:
        return <div />;
    }
  };

  return (
    <div>
      <div>
        {primaryGame && (
          <>
            <InputLabel required theme="smallText">
              Primary Game
            </InputLabel>
            {renderConnectCard(primaryGame, setPrimaryGameUsername, primaryGameUsername)}
            <GameStatsDropdowns
              onChange={onChange}
              gameInfo={primaryGameInfo}
              hoursPlayedField="primaryGameHoursPlayed"
              hoursPlayedValue={primaryGameHoursPlayed}
              roleField="primaryGameRole"
              roleValue={primaryGameRole}
              required
            />
          </>
        )}

        {secondaryGame && (
          <>
            <InputLabel theme="smallText"> Secondary Game </InputLabel>
            {renderConnectCard(secondaryGame, setSecondaryGameUsername, secondaryGameUsername)}
            <GameStatsDropdowns
              onChange={onChange}
              gameInfo={secondaryGameInfo}
              hoursPlayedField="secondaryGameHoursPlayed"
              hoursPlayedValue={secondaryGameHoursPlayed}
              roleField="secondaryGameRole"
              roleValue={secondaryGameRole}
            />
          </>
        )}
      </div>
    </div>
  );
};

export default authenticateAccount(GameStats);
