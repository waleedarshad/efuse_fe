import React from "react";
import { Col } from "react-bootstrap";

import InputField from "../../../../InputField/InputField";
import InputLabelWithHelpTooltip from "../../../../InputLabelWithHelpTooltip/InputLabelWithHelpTooltip";
import InputRow from "../../../../InputRow/InputRow";
import SelectBox from "../../../../SelectBox/SelectBox";
import InputLabel from "../../../../InputLabel/InputLabel";
import SelectGraduationYear from "../../SelectGraduationYear/SelectGraduationYear";
import SelectCountry from "../../SelectCountry/SelectCountry";
import stateData from "../../../../../static/data/states.json";
import { formattedStates, UNITED_STATES } from "../../../../Settings/Recruiting/dropdownData";

const RequiredFields = ({ recruitingProfile, onChange }) => {
  const { name, graduationClass, addressLine1, city, state, zipCode, country } = recruitingProfile;

  return (
    <>
      <InputRow>
        <Col>
          <InputLabelWithHelpTooltip
            labelText="Full Name"
            required
            tooltipText="Enter your full name."
            labelTheme="smallText"
          />
          <InputField name="name" placeholder="Enter your name" value={name} onChange={onChange} theme="whiteShadow" />
        </Col>
      </InputRow>
      <InputRow>
        <Col>
          <InputLabelWithHelpTooltip
            labelText="Graduating Class"
            tooltipText="Select the year that you plan to graduate from high school."
            labelTheme="smallText"
            required
          />
          <SelectGraduationYear value={graduationClass} onChange={onChange} />
        </Col>
      </InputRow>
      <InputRow>
        <Col>
          <InputLabelWithHelpTooltip
            labelText="Mailing Address"
            tooltipText="Type your mailing address including street name, house number, city, state, zip code, and all other applicable information."
            labelTheme="smallText"
            required
          />
          <InputField
            name="addressLine1"
            placeholder="Enter your mailing address"
            value={addressLine1}
            onChange={onChange}
            theme="whiteShadow"
          />
        </Col>
      </InputRow>
      <InputRow>
        <Col>
          <InputLabel required theme="smallText">
            Town/City
          </InputLabel>
          <InputField
            name="city"
            placeholder="Enter the town or city you are from"
            value={city}
            onChange={onChange}
            theme="whiteShadow"
          />
        </Col>
      </InputRow>
      <InputRow>
        <Col>
          {country === UNITED_STATES ? (
            <>
              <InputLabel required theme="smallText">
                State
              </InputLabel>
              <SelectBox
                name="state"
                options={formattedStates}
                validated={false}
                theme="whiteShadow"
                value={state}
                onChange={onChange}
              />
            </>
          ) : (
            <>
              <InputLabel required theme="smallText">
                State / Province / Region
              </InputLabel>
              <InputField
                name="state"
                placeholder=""
                size="lg"
                theme="whiteShadow"
                onChange={onChange}
                value={state}
                maxLength={70}
              />
            </>
          )}
        </Col>
        <Col>
          <InputLabel required theme="smallText">
            Zip Code
          </InputLabel>
          <InputField name="zipCode" placeholder="Enter Zip" value={zipCode} onChange={onChange} theme="whiteShadow" />
        </Col>
      </InputRow>
      <InputRow>
        <Col>
          <InputLabel theme="smallText" required>
            Country
          </InputLabel>
          <SelectCountry value={country} onChange={onChange} />
        </Col>
      </InputRow>
    </>
  );
};

export default RequiredFields;
