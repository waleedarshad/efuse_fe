import React from "react";
import { Col } from "react-bootstrap";

import InputField from "../../../../InputField/InputField";
import InputLabelWithHelpTooltip from "../../../../InputLabelWithHelpTooltip/InputLabelWithHelpTooltip";
import InputRow from "../../../../InputRow/InputRow";
import SelectBox from "../../../../SelectBox/SelectBox";
import EFGenderInput from "../../../../EFGenderInput/EFGenderInput";
import { formattedCollegeSizes, formattedCollegeRegions } from "../../../../Settings/Recruiting/dropdownData";

const OptionalFields = ({ recruitingProfile, onChange }) => {
  const {
    desiredCollegeMajor,
    desiredCollegeSize,
    desiredCollegeRegion,
    gender,
    desiredCollegeAttributes,
    esportsCareerField
  } = recruitingProfile;

  return (
    <>
      <InputRow>
        <Col>
          <InputLabelWithHelpTooltip
            labelText="Desired Major"
            tooltipText="Type the major you intend to study in college. If you are undecided, please type 'undecided.'"
            labelTheme="smallText"
          />
          <InputField
            name="desiredCollegeMajor"
            placeholder="Enter the things you would like from a College or Program"
            value={desiredCollegeMajor}
            onChange={onChange}
            theme="whiteShadow"
          />
        </Col>
      </InputRow>
      <InputRow>
        <Col>
          <InputLabelWithHelpTooltip
            labelText="Desired Size of College"
            tooltipText="Select the size of institution you would like to attend for college. This helps recruiters understand your preferences."
            labelTheme="smallText"
          />
          <SelectBox
            name="desiredCollegeSize"
            options={formattedCollegeSizes}
            validated={false}
            theme="whiteShadow"
            value={desiredCollegeSize}
            onChange={onChange}
          />
        </Col>
        <Col>
          <InputLabelWithHelpTooltip
            labelText="Desired College Region"
            tooltipText="Select the region (in the United States) in which you would prefer to attend college."
            labelTheme="smallText"
          />
          <SelectBox
            name="desiredCollegeRegion"
            options={formattedCollegeRegions}
            validated={false}
            theme="whiteShadow"
            value={desiredCollegeRegion}
            onChange={onChange}
          />
        </Col>
      </InputRow>
      <InputRow>
        <Col>
          <InputLabelWithHelpTooltip labelText="Gender" tooltipText="Select your gender" labelTheme="smallText" />
          <EFGenderInput value={gender} onChange={onChange} />
        </Col>
      </InputRow>
      <InputRow>
        <Col>
          <InputLabelWithHelpTooltip
            labelText="Desired Attributes of a College/Program"
            tooltipText="Enter any special preferences you would like to see in an institution or program you attend for college."
            labelTheme="smallText"
          />
          <InputField
            as="textarea"
            rows={3}
            placeholder="Enter the things you would like from a College or Program"
            name="desiredCollegeAttributes"
            value={desiredCollegeAttributes}
            onChange={onChange}
            theme="whiteShadow"
          />
        </Col>
      </InputRow>
      <InputRow>
        <Col>
          <InputLabelWithHelpTooltip
            labelText="Interest Esports Career or Field"
            tooltipText="Enter any relevant career aspirations."
            labelTheme="smallText"
          />
          <InputField
            as="textarea"
            rows={3}
            placeholder="Enter the things you would like from a College or Program"
            name="esportsCareerField"
            value={esportsCareerField}
            onChange={onChange}
            theme="whiteShadow"
          />
        </Col>
      </InputRow>
    </>
  );
};

export default OptionalFields;
