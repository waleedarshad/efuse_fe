import React from "react";

import Style from "../JoinThePipeline.module.scss";
import RequiredFields from "./RequiredFields/RequiredFields";
import OptionalFields from "./OptionalFields/OptionalFields";

const RecruitInfo = ({ recruitingProfile, onChange }) => {
  return (
    <div>
      <RequiredFields recruitingProfile={recruitingProfile} onChange={onChange} />
      <p className={Style.optionalLabel}>OPTIONAL</p>
      <OptionalFields recruitingProfile={recruitingProfile} onChange={onChange} />
    </div>
  );
};

export default RecruitInfo;
