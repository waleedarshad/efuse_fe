import React from "react";
import { Col } from "react-bootstrap";

import InputField from "../../../InputField/InputField";
import InputLabelWithHelpTooltip from "../../../InputLabelWithHelpTooltip/InputLabelWithHelpTooltip";
import InputRow from "../../../InputRow/InputRow";
import Style from "../JoinThePipeline.module.scss";

const AcademicInfo = ({ onChange, recruitingProfile }) => {
  const {
    gradingScale,
    highSchoolGPA,
    actTestScore,
    satTestScore,
    toeflTestScores,
    extracurricularActivities,
    country,
    highSchool
  } = recruitingProfile;
  return (
    <div>
      <InputRow>
        <Col>
          <InputLabelWithHelpTooltip
            labelText="High School / Secondary School"
            tooltipText="Enter the name of your high school or secondary school."
            labelTheme="smallText"
            required
          />
          <InputField
            name="highSchool"
            placeholder="Ex. eFuse High School."
            value={highSchool}
            onChange={onChange}
            theme="whiteShadow"
          />
        </Col>
      </InputRow>
      <InputRow>
        <Col>
          <InputLabelWithHelpTooltip
            labelText="Grading Scale and Average"
            tooltipText="Enter the name of your grading system. Ex. 4 point scale"
            labelTheme="smallText"
            required
          />
          <InputField
            name="gradingScale"
            placeholder="Enter your grading scale"
            value={gradingScale}
            onChange={onChange}
            theme="whiteShadow"
          />
        </Col>
      </InputRow>
      <InputRow>
        <Col>
          <InputLabelWithHelpTooltip
            labelText="GPA"
            tooltipText="Enter your high school grade point average."
            labelTheme="smallText"
            required
          />
          <InputField
            name="highSchoolGPA"
            placeholder="Enter your grade point average."
            value={highSchoolGPA}
            onChange={onChange}
            theme="whiteShadow"
          />
        </Col>
      </InputRow>
      <p className={Style.optionalLabel}>OPTIONAL</p>
      <InputRow>
        <Col>
          <InputLabelWithHelpTooltip
            labelText="SAT Score"
            tooltipText="Type your official SAT score. If you have yet to take these tests, leave this section blank and update later."
            labelTheme="smallText"
          />
          <InputField
            name="satTestScore"
            placeholder="Enter your SAT score: 1200"
            value={satTestScore}
            onChange={onChange}
            theme="whiteShadow"
          />
        </Col>
      </InputRow>
      <InputRow>
        <Col>
          <InputLabelWithHelpTooltip
            labelText="ACT Score"
            tooltipText="Type your official ACT score. If you have yet to take these tests, leave this section blank and update later."
            labelTheme="smallText"
          />
          <InputField
            name="actTestScore"
            placeholder="Enter your ACT score: 25"
            value={actTestScore}
            onChange={onChange}
            theme="whiteShadow"
          />
        </Col>
      </InputRow>
      {country !== "United States" && (
        <InputRow>
          <Col>
            <InputLabelWithHelpTooltip
              labelText="TOEFL Score"
              tooltipText="Enter your TOEFL score out of 1200"
              labelTheme="smallText"
            />
            <InputField
              name="toeflTestScores"
              placeholder="Ex. 1000"
              value={toeflTestScores}
              onChange={onChange}
              theme="whiteShadow"
            />
          </Col>
        </InputRow>
      )}
      <InputRow>
        <Col>
          <InputLabelWithHelpTooltip
            labelText="Extracurricular Activities"
            tooltipText="Type the name of all relevant extracurricular activities along with all significant honors received in respective activities."
            labelTheme="smallText"
          />
          <InputField
            name="extracurricularActivities"
            placeholder="Ex. Baseball, Gaming Club, Robotics Club, etc."
            value={extracurricularActivities}
            onChange={onChange}
            as="textarea"
            rows={3}
            theme="whiteShadow"
          />
        </Col>
      </InputRow>
    </div>
  );
};

export default AcademicInfo;
