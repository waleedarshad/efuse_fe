const joinPipelineSteps = {
  intro: "intro",
  recruitInfo: "recruitInfo",
  academicInfo: "academicInfo",
  gamingInfo: "gamingInfo",
  gameStats: "gameStats",
  honorsAndEvents: "honorsAndEvents",
  videosAndClips: "videosAndClips",
  socialLink: "socialLink",
  complete: "complete"
};

export default joinPipelineSteps;
