import React from "react";
import { Col } from "react-bootstrap";

import { useQuery } from "@apollo/client";
import InputLabelWithHelpTooltip from "../../../../InputLabelWithHelpTooltip/InputLabelWithHelpTooltip";
import InputRow from "../../../../InputRow/InputRow";
import EFGameCardList from "../../../../EFGameCardList/EFGameCardList";
import { GET_PIPELINE_GAMES } from "../../../../../graphql/GameQuery";

const GameInput = ({ labelText, tooltipText, onSelect, fieldName, selected, required, disabledGames }) => {
  const { data } = useQuery(GET_PIPELINE_GAMES);
  const games = (data?.getPipelineGames || [])
    .filter(it => !disabledGames?.includes(it.slug))
    .map(game => ({ ...game, selected: game.slug === selected }));

  return (
    <InputRow>
      <Col>
        <InputLabelWithHelpTooltip
          required={required}
          labelText={labelText}
          tooltipText={tooltipText}
          labelTheme="smallText"
        />
        <EFGameCardList games={games} horizontalScroll onGameClick={game => onSelect(fieldName, game.slug)} />
      </Col>
    </InputRow>
  );
};

export default GameInput;
