import React from "react";

import Style from "./SelectButton.module.scss";

const SelectButton = ({ text, active, onClick }) => {
  return (
    <button
      className={`${Style.selectButton} ${active && Style.activeButton}`}
      onClick={() => onClick("highSchoolClubTeam", text)}
    >
      <span className={Style.text}>{text}</span>
      <span className={`${Style.circle} ${active && Style.activeCircle}`} />
    </button>
  );
};

export default SelectButton;
