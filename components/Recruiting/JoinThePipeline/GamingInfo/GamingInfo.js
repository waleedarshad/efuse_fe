import React from "react";
import { Col } from "react-bootstrap";
import InputRow from "../../../InputRow/InputRow";
import InputLabel from "../../../InputLabel/InputLabel";
import SelectButton from "./SelectButton/SelectButton";
import GameInput from "./GameInput/GameInput";
import Style from "../JoinThePipeline.module.scss";

const GamingInfo = ({ recruitingProfile, setRecruitingProfile }) => {
  const { primaryGame, secondaryGame, highSchoolClubTeam } = recruitingProfile;

  const setPrimaryGame = (fieldName, value) => {
    setRecruitingProfile({
      ...recruitingProfile,
      [fieldName]: value,
      secondaryGame: value === secondaryGame ? "" : secondaryGame
    });
  };

  const setField = (fieldName, value) => {
    setRecruitingProfile({
      ...recruitingProfile,
      [fieldName]: value
    });
  };

  return (
    <div>
      <GameInput
        labelText="Select your Primary Game"
        tooltipText="Select the game in which you have either played the most hours,achieved the highest level in, and/or would like to be recruited in."
        fieldName="primaryGame"
        onSelect={setPrimaryGame}
        selected={primaryGame}
        required
      />
      <p className={Style.optionalLabel}>OPTIONAL. Must be different than your primary game.</p>
      <GameInput
        labelText="Select your Secondary Game"
        tooltipText="Select an optional secondary game in which you wish to be considered for recruitment."
        fieldName="secondaryGame"
        onSelect={setField}
        selected={secondaryGame}
        disabledGames={[primaryGame]}
      />
      <InputRow>
        <Col>
          <InputLabel required theme="smallText">
            Do you play in a high school or club Esports team?
          </InputLabel>
          <div>
            <SelectButton text="No" active={highSchoolClubTeam === "No"} onClick={setField} />
            <SelectButton text="Yes" active={highSchoolClubTeam === "Yes"} onClick={setField} />
          </div>
        </Col>
      </InputRow>
    </div>
  );
};

export default GamingInfo;
