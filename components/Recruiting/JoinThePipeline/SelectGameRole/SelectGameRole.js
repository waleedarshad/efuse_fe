import React from "react";
import SelectBox from "../../../SelectBox/SelectBox";

const SelectGameRole = ({ value, onChange, name, game }) => {
  const roles = game?.pipelineInfo?.roles || [];
  const formattedRoles = roles.map(it => ({ value: it, label: it }));
  formattedRoles.unshift({ label: "Select one", value: "" });
  return (
    <SelectBox
      name={name}
      options={formattedRoles}
      validated={false}
      theme="whiteShadow"
      value={value}
      onChange={onChange}
    />
  );
};

export default SelectGameRole;
