import React from "react";
import SelectBox from "../../../SelectBox/SelectBox";
import { hoursPlayed } from "../../../Settings/Recruiting/dropdownData";

const SelectHoursPlayed = ({ value, onChange, name }) => {
  const formattedHoursPlayed = hoursPlayed.map(it => ({ value: it, label: it }));
  formattedHoursPlayed.unshift({ label: "Select one", value: "" });
  return (
    <SelectBox
      name={name}
      options={formattedHoursPlayed}
      validated={false}
      theme="whiteShadow"
      value={value}
      onChange={onChange}
    />
  );
};

export default SelectHoursPlayed;
