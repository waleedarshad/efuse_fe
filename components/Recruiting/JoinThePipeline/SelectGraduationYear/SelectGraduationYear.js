import React from "react";

import SelectBox from "../../../SelectBox/SelectBox";
import { formattedGraduationYears } from "../../../Settings/Recruiting/dropdownData";

const SelectGraduationYear = ({ value, onChange }) => {
  return (
    <SelectBox
      name="graduationClass"
      options={formattedGraduationYears}
      validated={false}
      theme="whiteShadow"
      value={value}
      onChange={onChange}
    />
  );
};

export default SelectGraduationYear;
