import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { faTwitch, faYoutube } from "@fortawesome/free-brands-svg-icons";

import EFRectangleButton from "../../../../Buttons/EFRectangleButton/EFRectangleButton";
import EmbedVideo from "../../../../User/Portfolio/VideoComponent/Youtube/EmbedVideo";
import { togglePortfolioModal } from "../../../../../store/actions/portfolioActions";
import Style from "./UploadButtons.module.scss";
import ExternalAccounts from "../../../../Settings/ExternalAccounts/index";

const UploadButtons = ({ user }) => {
  const dispatch = useDispatch();

  const modal = useSelector(state => state.portfolio.modal);
  const modalSection = useSelector(state => state.portfolio.modalSection);

  const loadVideoModal = () => {
    dispatch(togglePortfolioModal(true, "stream"));
  };

  const onHide = () => {
    dispatch(togglePortfolioModal(false));
  };

  return (
    <>
      <div className={Style.uploadButtonsWrapper}>
        <ExternalAccounts
          onlyShowLinks
          displaySteam={false}
          displayBattlenet={false}
          displayLinkedin={false}
          displayTwitter={false}
          displayDiscord={false}
          displayFacebook={false}
          displayTwitch
          displayGoogle={false}
          displaySnapchat={false}
          displayAimLab={false}
          displayValorant={false}
          avoidGrid
        >
          <div className={Style.uploadButton}>
            <EFRectangleButton
              shadowTheme="small"
              colorTheme="light"
              fontWeightTheme="normal"
              icon={faTwitch}
              text="Link Twitch Stream"
              width="fullWidth"
              size="large"
              alignContent="left"
              disabled={user.twitchVerified}
            />
          </div>
        </ExternalAccounts>
        <div className={Style.uploadButton}>
          <EFRectangleButton
            shadowTheme="small"
            colorTheme="light"
            fontWeightTheme="normal"
            icon={faYoutube}
            text="Embed YouTube Video"
            width="fullWidth"
            size="large"
            alignContent="left"
            onClick={() => loadVideoModal()}
            disabled={user.embeddedLink}
          />
        </div>
        {modal && modalSection === "stream" && <EmbedVideo show={modal} onHide={onHide} user={user} />}
      </div>
    </>
  );
};

export default UploadButtons;
