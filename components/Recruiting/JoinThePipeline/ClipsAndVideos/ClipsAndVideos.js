import React from "react";
import { useSelector } from "react-redux";

import InputLabelWithHelpTooltip from "../../../InputLabelWithHelpTooltip/InputLabelWithHelpTooltip";
import DisplayAddedVideos from "./DisplayAddedVideos/DisplayAddedVideos";
import UploadButtons from "./UploadButtons/UploadButtons";

const ClipsAndVideos = () => {
  const user = useSelector(state => state.user.currentUser);

  return (
    <div>
      <InputLabelWithHelpTooltip
        labelText="Add a clip or video"
        tooltipText="Upload or link a video to show your skills "
        labelTheme="smallText"
      />
      <UploadButtons user={user} />
      {(user.embeddedLink || user.twitchVerified) && (
        <DisplayAddedVideos embeddedLink={user.embeddedLink} twitchUserName={user.twitchUserName} />
      )}
    </div>
  );
};

export default ClipsAndVideos;
