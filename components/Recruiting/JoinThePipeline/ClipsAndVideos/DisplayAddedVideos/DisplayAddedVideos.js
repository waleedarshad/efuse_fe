import React from "react";
import { useDispatch } from "react-redux";
import { faTrash } from "@fortawesome/pro-solid-svg-icons";

import ConfirmAlert from "../../../../ConfirmAlert/ConfirmAlert";
import EFCircleIconButton from "../../../../Buttons/EFCircleIconButton/EFCircleIconButton";
import InputLabel from "../../../../InputLabel/InputLabel";
import VideoPlayer from "../../../../VideoPlayer/VideoPlayer";
import { updateVideoInfo } from "../../../../../store/actions/portfolioActions";
import Style from "./DisplayAddedVideos.module.scss";
import AuthenticateTwitch from "../../../../AuthenticateAccounts/AuthenticateTwitch/AuthenticateTwitch";

const DisplayAddedVideos = ({ embeddedLink, twitchUserName }) => {
  const dispatch = useDispatch();

  const removeYoutubeVideo = () => {
    dispatch(updateVideoInfo({ embeddedLink: null, youtubeChannelId: null }));
  };

  const unlinkButton = (
    <div className={Style.trash}>
      <EFCircleIconButton icon={faTrash} shadowTheme="none" size="small" />
    </div>
  );

  const displayVideo = (videoUrl, type) => (
    <div className={Style.videoItem}>
      <VideoPlayer url={videoUrl} height="250px" />
      {type === "youtube" ? (
        <ConfirmAlert
          title="Remove this video/clip?"
          message="Are you sure you want to remove this YouTube link?"
          onYes={() => removeYoutubeVideo()}
        >
          {unlinkButton}
        </ConfirmAlert>
      ) : (
        <AuthenticateTwitch>{unlinkButton}</AuthenticateTwitch>
      )}
    </div>
  );

  return (
    <div className={Style.displayContainer}>
      <InputLabel theme="smallText">Added clips and videos</InputLabel>
      <div className={Style.videoContainer}>
        {embeddedLink && displayVideo(embeddedLink, "youtube")}
        {twitchUserName && displayVideo(`https://www.twitch.tv/${twitchUserName}`)}
      </div>
    </div>
  );
};

export default DisplayAddedVideos;
