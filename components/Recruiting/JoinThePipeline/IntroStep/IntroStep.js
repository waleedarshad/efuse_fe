import React, { useEffect } from "react";
import { Col } from "react-bootstrap";

import Style from "./IntroStep.module.scss";
import InputRow from "../../../InputRow/InputRow";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";
import InputLabel from "../../../InputLabel/InputLabel";
import SelectCountry from "../SelectCountry/SelectCountry";
import SelectGraduationYear from "../SelectGraduationYear/SelectGraduationYear";
import PipelineBackground from "../PipelineBackground/PipelineBackground";

const IntroStep = ({ graduationClass, country, onChange, next }) => {
  const isButtonEnabled = graduationClass && country;

  return (
    <>
      <PipelineBackground
        title="Join the Pipeline"
        subtitle="Where do you stack up? Earn your way to the top of the rankings by increasing your skills, impact, & experience."
      />
      <div className={Style.contentWrapper}>
        <div className={Style.inputsWrapper}>
          <InputRow>
            <Col sm={12}>
              <InputLabel theme="smallText">Graduation Year</InputLabel>
              <SelectGraduationYear value={graduationClass} onChange={onChange} />
            </Col>
          </InputRow>
          <InputRow>
            <Col sm={12}>
              <InputLabel theme="smallText">Country</InputLabel>
              <SelectCountry value={country} onChange={onChange} />
            </Col>
          </InputRow>
          <EFRectangleButton
            text="Join the Pipeline"
            width="fullWidth"
            colorTheme="secondary"
            onClick={next}
            disabled={!isButtonEnabled}
          />
        </div>
      </div>
    </>
  );
};

export default IntroStep;
