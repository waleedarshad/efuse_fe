import { Component } from "react";
import { Form, Col, Spinner } from "react-bootstrap";
import { connect } from "react-redux";
import { withRouter } from "next/router";
import InputRow from "../../InputRow/InputRow";
import Style from "./Questionnaire.module.scss";
import SelectBox from "../../SelectBox/SelectBox";
import Checkbox from "../../Checkbox/Checkbox";
import HelpToolTip from "../../HelpToolTip/HelpToolTip";
import { getRecruitmentProfile, updateRecruitmentProfile } from "../../../store/actions/recruitingActions";
import countriesData from "../../../static/data/countries.json";
import { yearsOptions } from "../../../helpers/GeneralHelper";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";

class Questionnaire extends Component {
  state = {
    country: "",
    graduationYear: "",
    view: "questions", // "questions", "accepted", "rejected"
    agreedToDisclaimer: false,
    agreedToTosAndPP: false,
    validated: false
  };

  componentDidMount() {
    const { getRecruitmentProfile } = this.props;
    getRecruitmentProfile();
  }

  componentDidUpdate() {
    const { recruitingProfile } = this.props;
    const { view } = this.state;
    if (
      recruitingProfile &&
      recruitingProfile.country.length > 0 &&
      recruitingProfile.graduationClass.length > 0 &&
      view == "questions"
    ) {
      this.setState({ ...this.state, view: "accepted" });
    }
  }

  onChange = event => {
    this.setState({
      ...this.state,
      [event.target.name]: event.target.value
    });
  };

  onChangeCheckbox = event => {
    this.setState({
      [event.target.name]: event.target.checked
    });
  };

  onSubmit = event => {
    const { graduationYear, country } = this.state;
    const { updateRecruitmentProfile } = this.props;
    event.preventDefault();
    this.setState({ validated: true });
    const formValid = event.currentTarget.checkValidity();

    if (formValid) {
      analytics.track("PIPELINE_QUESTIONNAIRE_SUBMIT");
      updateRecruitmentProfile({ graduationClass: graduationYear, country }, "/settings/recruiting?welcome=true");
    }
  };

  render() {
    const { graduationYear, country, validated, view, agreedToDisclaimer, agreedToTosAndPP } = this.state;
    const { submittingPipelineQuestionniare } = this.props;
    let formatedCountries = [];
    countriesData.map(country => {
      formatedCountries.push({ value: country, label: country });
    });
    formatedCountries = [{ label: "Select one", value: "" }, ...formatedCountries];

    const formatedGraduationYears = yearsOptions(100, 8, "Select one", true);

    if (view == "questions") {
      return (
        <Form noValidate validated={validated} role="form" onSubmit={this.onSubmit}>
          <div className={Style.formBody}>
            <div className={Style.instructions}>Answer the required questions below</div>
            <InputRow>
              <Col sm={12}>
                <div className={Style.labelContainer}>
                  <label className={Style.label}>Country</label>
                  <HelpToolTip text="Select the country where you live now." />
                </div>
                <SelectBox
                  disabled={submittingPipelineQuestionniare}
                  name="country"
                  options={formatedCountries}
                  validated={validated}
                  theme="whiteShadow"
                  value={country}
                  required
                  onChange={this.onChange}
                  errorMessage="Country is required."
                />
              </Col>
            </InputRow>
            <InputRow>
              <Col sm={12}>
                <div className={Style.labelContainer}>
                  <label className={Style.label}>High School Graduating Class</label>
                  <HelpToolTip text="Select the year that you plan to graduate from high school." />
                </div>
                <SelectBox
                  disabled={submittingPipelineQuestionniare}
                  name="graduationYear"
                  options={formatedGraduationYears}
                  validated={validated}
                  theme="whiteShadow"
                  value={graduationYear}
                  required
                  onChange={this.onChange}
                  errorMessage="High School Graduating Class is required."
                />
              </Col>
            </InputRow>
            <div className={Style.checkboxSection}>
              <Checkbox
                type="checkbox"
                name="agreedToDisclaimer"
                label="By checking this box, you are authorizing eFuse to share your data with college administrators and other esports professionals for recruiting purposes."
                id="agreedToDisclaimer"
                theme="internal"
                checked={agreedToDisclaimer}
                onChange={this.onChangeCheckbox}
                required
                errorMessage="You must agree to this policy before joing Pipeline."
              />
              <Checkbox
                type="checkbox"
                name="agreedToTosAndPP"
                label={
                  <>
                    I agree to the{" "}
                    <a target="pp" href="https://efuse.gg/privacy" target="_blank" className={Style.marginRight}>
                      Privacy Policy
                    </a>{" "}
                    and{" "}
                    <a target="terms" href="https://efuse.gg/terms" target="_blank" className={Style.marginLeft}>
                      {" "}
                      Terms of Service
                    </a>
                  </>
                }
                id="agreedToTosAndPP"
                theme="internal"
                checked={agreedToTosAndPP}
                onChange={this.onChangeCheckbox}
                required
                errorMessage="You must agree to this policy before joing Pipeline."
              />
            </div>
          </div>

          <EFRectangleButton
            disabled={submittingPipelineQuestionniare}
            buttonType="submit"
            text={
              <>
                {submittingPipelineQuestionniare && <Spinner animation="border" variant="light" size="sm" />}
                {submittingPipelineQuestionniare ? " Submitting..." : "Complete"}
              </>
            }
          />
        </Form>
      );
    }
    if (view == "accepted") {
    } else if (view == "rejected") {
    }
  }
}

const mapStateToProps = state => ({
  submittingPipelineQuestionniare: state.recruiting.submittingPipelineQuestionniare,
  recruitingProfile: state.recruiting.profile
});

export default connect(mapStateToProps, {
  getRecruitmentProfile,
  updateRecruitmentProfile
})(withRouter(Questionnaire));
