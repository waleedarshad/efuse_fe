import React from "react";
import { Image, Container, Row, Col } from "react-bootstrap";

import LandingNav from "../../Layouts/LandingNav/LandingNav";
import Style from "./WelcomeContent.module.scss";
import HeaderWrapper from "../../Layouts/Internal/HeaderWrapper/HeaderWrapper";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";
import JoinThePipelineModalButton from "../JoinThePipelineModalButton/JoinThePipelineModalButton";

const WelcomeContent = ({
  initialQuestionnaireComplete,
  openModalOnLoad,
  showCollegeLogin,
  toggleModal,
  isModalOpen,
  newPipelineOnboarding,
  currentUser
}) => {
  let mainButton = (
    <JoinThePipelineModalButton
      toggleModal={toggleModal}
      newPipelineOnboarding={newPipelineOnboarding}
      openModalOnLoad={openModalOnLoad}
      buttonColorTheme="secondary"
      isModalOpen={isModalOpen}
    />
  );

  if (!isModalOpen && initialQuestionnaireComplete) {
    mainButton = (
      <EFRectangleButton
        colorTheme="secondary"
        text="VIEW RECRUIT PROFILE"
        internalHref="/settings/recruiting"
        onClick={() => {
          analytics.track("PIPELINE_VIEW_RECRUIT_PROFILE_CLICK");
        }}
      />
    );
  }

  return (
    <>
      {currentUser?.id ? <HeaderWrapper /> : <LandingNav preventScrollFade />}
      <div className={Style.mainContent}>
        <Container>
          <Row>
            <Col className={Style.textContainer}>
              <p className={Style.text}>
                <span className={Style.textTitle}>Get Recruited and Join a Gaming Team</span>
                <br />
                <span className={Style.textDesc}>
                  Earn your way to the top of the rankings by increasing your skills, impact, & experience.
                </span>
              </p>
              <div className={Style.buttonContainer}>{mainButton}</div>

              {showCollegeLogin && (
                <div className={Style.buttonContainer}>
                  <EFRectangleButton
                    colorTheme="primary"
                    text="COLLEGE LOGIN"
                    externalHref="https://forms.gle/5RH3bsjZjJ7pL79V7"
                    onClick={() => {
                      analytics.track("PIPELINE_COLLEGE_LOGIN_CLICK");
                    }}
                  />
                </div>
              )}
            </Col>
            <Col className={Style.imageContainer}>
              <Image className={Style.logo} src="https://cdn.efuse.gg/uploads/pipeline/homepage/pipeline_logo.png" />
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
};

export default WelcomeContent;
