import React, { useState } from "react";
import { useRouter } from "next/router";

import Modal from "../../Modal/Modal";
import Questionnaire from "../Questionnaire/Questionnaire";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";
import JoinThePipeline from "../JoinThePipeline";
import joinPipelineSteps from "../JoinThePipeline/joinPipelineSteps";
import { isUserLoggedIn } from "../../../helpers/AuthHelper";
import SignupModal from "../../SignupModal/SignupModal";
import Style from "./JoinThePipelineModalButton.module.scss";

const JoinThePipelineModalButton = ({ toggleModal, newPipelineOnboarding, buttonColorTheme, openModalOnLoad }) => {
  const [localOpenModalOnLoad, setLocalOpenModalOnLoad] = useState(openModalOnLoad);

  const router = useRouter();

  return isUserLoggedIn() ? (
    <Modal
      component={
        newPipelineOnboarding ? (
          <JoinThePipeline startStep={joinPipelineSteps.intro} closeModal={() => toggleModal(false)} />
        ) : (
          <Questionnaire />
        )
      }
      allowBackgroundClickClose={false}
      displayCloseButton
      openOnLoad={localOpenModalOnLoad}
      onCloseModal={() => {
        toggleModal(false);
        setLocalOpenModalOnLoad(false);
        router.push("/pipeline");
        analytics.track("PIPELINE_APPLICATION_CLOSE");
      }}
      onOpenModal={() => {
        analytics.track("PIPELINE_APPLICATION_OPEN");
      }}
      customCloseButtonStyle={Style.modalCloseButton}
      customMaxWidth="800px"
    >
      <EFRectangleButton
        colorTheme={buttonColorTheme}
        text="JOIN THE PIPELINE"
        onClick={() => {
          analytics.track("PIPELINE_APPLY_CLICK");
          toggleModal(true);
        }}
      />
    </Modal>
  ) : (
    <SignupModal
      title="Create an account to join The Pipeline."
      message="See where you stack up with other high school recruits. Sign up today!"
      redirectURL="/pipeline?register=true"
    >
      <EFRectangleButton
        colorTheme={buttonColorTheme}
        text="JOIN THE PIPELINE"
        onClick={() => {
          analytics.track("PIPELINE_APPLY_CLICK");
        }}
      />
    </SignupModal>
  );
};

export default JoinThePipelineModalButton;
