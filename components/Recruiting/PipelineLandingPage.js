import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useRouter } from "next/router";
import { NextSeo } from "next-seo";
import { Container } from "react-bootstrap";

import Style from "./PipelineLandingPage.module.scss";
import WelcomeContent from "./WelcomeContent/WelcomeContent";
import PipelineLeaderboardWrapper from "./PipelineLeaderboardWrapper/PipelineLeaderboardWrapper";
import InfoCards from "./InfoCards/InfoCards";
import ErrorPage from "../../pages/_error";
import pipelineLeaderboardGames from "../../static/data/pipelineLeaderboardGames.json";
import { getRecruitmentProfile } from "../../store/actions/recruitingActions";
import { isUserLoggedIn } from "../../helpers/AuthHelper";
import { setLeaderboardData } from "../../store/actions/leaderboardActions";
import CTASection from "./CTASection/CTASection";

const PipelineLandingPage = ({ pageProps }) => {
  const dispatch = useDispatch();
  const router = useRouter();

  const [isModalOpen, toggleModal] = useState(router.query.register ? true : false);

  const authUser = useSelector(state => state.auth.currentUser);
  const recruitingProfile = useSelector(state => state.recruiting.profile);
  const newPipelineOnboarding = useSelector(state => state.features.temp_pipeline_onboarding);

  useEffect(() => {
    analytics.page("Pipeline Landing Page");
    if (isUserLoggedIn()) {
      dispatch(getRecruitmentProfile());
    }
  }, []);

  useEffect(() => {
    dispatch(setLeaderboardData(pageProps));
  }, [pageProps]);

  const validateSlug = slug => {
    return pipelineLeaderboardGames[slug];
  };

  const { game } = router.query;
  const initialQuestionnaireComplete =
    recruitingProfile && recruitingProfile.graduationClass.length !== 0 && recruitingProfile.country.length !== 0;

  if (game && !validateSlug(game)) {
    return <ErrorPage statusCode={404} />;
  }

  return (
    <div className={Style.mainContainer}>
      <NextSeo
        title="The Pipeline | Powered by eFuse"
        description="A place where aspiring gamers are discovered. See where you stack up & be recruited by tons of collegiate organizations by registering for The Pipeline today!"
        openGraph={{
          type: "website",
          url: "https://efuse.gg/pipeline",
          title: "The Pipeline | Powered by eFuse",
          site_name: "eFuse.gg",
          description:
            "A place where aspiring gamers are discovered. See where you stack up & be recruited by tons of collegiate organizations by registering for The Pipeline today!",
          images: [
            {
              url: "https://cdn.efuse.gg/uploads/pipeline/homepage/og-card.png"
            }
          ]
        }}
        twitter={{
          handle: "@eFuseOfficial",
          site: "https://efuse.gg/pipeline",
          cardType: "summary_large_image"
        }}
      />
      <WelcomeContent
        initialQuestionnaireComplete={initialQuestionnaireComplete}
        openModalOnLoad={router.query.register}
        showCollegeLogin
        toggleModal={toggleModal}
        isModalOpen={isModalOpen}
        newPipelineOnboarding={newPipelineOnboarding}
        currentUser={authUser}
      />
      <Container>
        <PipelineLeaderboardWrapper />
        {!initialQuestionnaireComplete && (
          <CTASection toggleModal={toggleModal} newPipelineOnboarding={newPipelineOnboarding} />
        )}
        <InfoCards />
      </Container>
    </div>
  );
};

export default PipelineLandingPage;
