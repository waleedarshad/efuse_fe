import React from "react";

import HelpToolTip from "../../../HelpToolTip/HelpToolTip";
import Style from "./LeaderBoardHeaderRow.module.scss";

const LeaderBoardHeaderRow = ({ title, customStatColumnName, tooltipText }) => {
  return (
    <div className={`row ${Style.headerList}`}>
      <div className={`col-md-4 ${Style.headerItem}`}>
        <span>eFuse Top {title} </span>
        {tooltipText && <HelpToolTip text={tooltipText} placement="bottom" />}
      </div>
      <div className={`col-md-3 ${Style.headerItem} ${Style.rankHead} ${Style.responsive}`}>
        <span>Hometown</span>
      </div>
      <div className={`col-md-2 ${Style.headerItem} ${Style.rankHead} ${Style.responsive}`}>
        <span>Graduation Class</span>
      </div>
      <div className={`col-md-2 ${Style.headerItem} ${Style.rankHead} ${Style.responsive}`}>
        <span>{customStatColumnName}</span>
      </div>
    </div>
  );
};

export default LeaderBoardHeaderRow;
