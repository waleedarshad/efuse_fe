import React from "react";
import TableItemCard from "../../../Organizations/Table/TableItem/TableItemCard/TableItemCard";
import Style from "./LeaderboardBodyRows.module.scss";
import ResponsiveRow from "./ResponsiveRow/ResponsiveRow";
import LeaderboardRow from "./LeaderboardRow/LeaderboardRow";

const LeaderboardBodyRows = ({ stats, customStat }) => {
  const leaderboardStats = stats;
  return (
    <>
      {leaderboardStats?.map(stat => {
        return (
          <>
            <div className={Style.responsiveRow}>
              <TableItemCard key={stat._id}>
                <ResponsiveRow stat={stat} customStat={customStat} />
              </TableItemCard>
            </div>

            <div className={Style.leaderboardRow}>
              <TableItemCard key={stat._id}>
                <LeaderboardRow stat={stat} customStat={customStat} />
              </TableItemCard>
            </div>
          </>
        );
      })}
    </>
  );
};

export default LeaderboardBodyRows;
