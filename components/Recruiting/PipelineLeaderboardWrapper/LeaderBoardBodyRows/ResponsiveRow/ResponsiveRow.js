import React from "react";
import { Col } from "react-bootstrap";
import Link from "next/link";

import EFAvatar from "../../../../EFAvatar/EFAvatar";
import Style from "../LeaderboardBodyRows.module.scss";

const ResponsiveRow = ({ stat, customStat }) => {
  const leagueOfLegendsSummonerName = () => {
    // league info can be array so find appropriate info
    const foundLeagueInfo = stat?.stats?.leagueInfo?.find(info => info?.queueType === "RANKED_SOLO_5x5");
    return foundLeagueInfo?.summonerName;
  };

  const inGameName = leagueOfLegendsSummonerName() || stat.stats?.username || stat.stats?.gameName;
  return (
    <>
      <Col xs={4}>
        <div className={Style.avatar}>
          <div className={Style.position}>{stat.rank}</div>
          <EFAvatar
            href={`/u/${stat.user.username}`}
            displayOnlineButton={false}
            profilePicture={stat.user.profilePicture.url}
            size="medium"
          />
        </div>
      </Col>
      <Col xs={8} className={`${Style.leftAlign} pr-0`}>
        <Link href={`/u/${stat.user.username}`}>
          <span className={Style.username}>{stat.user.name}</span>
        </Link>
        <div className={Style.gameName}>{inGameName}</div>
        <div className={Style.smallText}>
          <div>
            <span>Graduation Class:&nbsp;</span>
            {stat.recruitmentProfile.graduationClass && (
              <span className={Style.lightBold}>{stat.recruitmentProfile.graduationClass}</span>
            )}
          </div>
          <div>
            <span>In-Game Rank:&nbsp;</span>
            <span className={`${Style.gameRank} ${Style.lightBold}`}>{customStat(stat)}</span>
          </div>
          {stat.recruitmentProfile.city && stat.recruitmentProfile.state && (
            <div
              className={`${Style.city} ${Style.upperCase}`}
            >{`${stat.recruitmentProfile.city}, ${stat.recruitmentProfile.state}`}</div>
          )}
          <div className={`${Style.oneLineClamp}`}>
            {stat?.recruitmentProfile?.highSchool && (
              <span className={Style.upperCase}>{`${stat.recruitmentProfile.highSchool}`}</span>
            )}
          </div>
        </div>
      </Col>
    </>
  );
};

export default ResponsiveRow;
