import React from "react";
import { Col } from "react-bootstrap";
import Link from "next/link";

import EFAvatar from "../../../../EFAvatar/EFAvatar";
import EfuseLink from "../../../../Organizations/Table/TableItem/EfuseLink/EfuseLink";
import Style from "../LeaderboardBodyRows.module.scss";

const LeaderboardRow = ({ stat, customStat }) => {
  const leagueOfLegendsSummonerName = () => {
    // league info can be array so find appropriate info
    const foundLeagueInfo = stat?.stats?.leagueInfo?.find(info => info?.queueType === "RANKED_SOLO_5x5");
    return foundLeagueInfo?.summonerName;
  };

  const inGameName = leagueOfLegendsSummonerName() || stat.stats?.username || stat.stats?.gameName;

  return (
    <>
      <Col xs={4} sm={1}>
        <div className={Style.avatar}>
          <div className={Style.position}>{stat?.rank}</div>
          <EFAvatar
            displayOnlineButton={false}
            href={`/u/${stat.user.username}`}
            profilePicture={stat?.user?.profilePicture?.url}
            size="small"
          />
        </div>
      </Col>
      <Col xs={8} sm={3} className={Style.leftAlignMobileOnly}>
        <Link href={`/u/${stat?.user?.username}`}>
          <span className={Style.username}>{stat?.user?.name}</span>
        </Link>
        <br />
        <span className={Style.gameName}>{inGameName}</span>
      </Col>
      <Col xs={4} sm={3} className={`${Style.textContainer} ${Style.upperCase} ${Style.smallText}`}>
        {stat.recruitmentProfile.city && stat.recruitmentProfile.state && (
          <div
            className={Style.oneLineClamp}
          >{`${stat.recruitmentProfile.city}, ${stat.recruitmentProfile.state}`}</div>
        )}
        {stat?.recruitmentProfile?.highSchool && (
          <div className={`${Style.oneLineClamp} ${Style.upperCase}`}>{`${stat.recruitmentProfile.highSchool}`}</div>
        )}
      </Col>
      <Col xs={4} sm={2} className={Style.textContainer}>
        <span className={Style.mobileOnlyLabel}>Class of </span>
        {stat?.recruitmentProfile?.graduationClass && <span>{stat.recruitmentProfile.graduationClass}</span>}
      </Col>
      <Col xs={4} sm={2} className={Style.textContainer}>
        <span>{customStat(stat)}</span>
      </Col>
      <Col sm={1} className={`${Style.rankContainer} d-none d-sm-block`}>
        <EfuseLink href={`/u/${stat?.user?.username}`} tooltipText="View Portfolio" tooltipPlacement="top" />
      </Col>
    </>
  );
};

export default LeaderboardRow;
