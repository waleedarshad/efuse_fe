import React, { useEffect, useRef, useState } from "react";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
import capitalize from "lodash/capitalize";

import Style from "./PipelineLeaderboardWrapper.module.scss";
import EFSubHeader from "../../Layouts/Internal/EFSubHeader/EFSubHeader";
import { getPipelineNavigationList } from "../../../navigation/pipeline";
import LeaderboardBodyRows from "./LeaderBoardBodyRows/LeaderboardBodyRows";
import { getCurrentUserLeaderboardData, getLeaderboardData } from "../../../store/actions/leaderboardActions";
import pipelineLeaderboardGames from "../../../static/data/pipelineLeaderboardGames.json";
import { getFlagForFeature } from "../../../store/selectors/featureFlagSelectors";
import LeaderBoardHeaderRow from "./LeaderboardHeaderRow/LeaderBoardHeaderRow";
import EFTooltip from "../../tooltip/EFTooltip/EFTooltip";
import EFPaginationBar from "../../EFPaginationBar/EFPaginationBar";
import SkeletonLoader from "../../SkeletonLoader/SkeletonLoader";

const PipelineLeaderboardWrapper = () => {
  const router = useRouter();
  const dispatch = useDispatch();
  const { query } = router;
  const gameSlug = query.game || "league-of-legends";
  const selectedGameData = pipelineLeaderboardGames[gameSlug];
  const showLeaderboard = useSelector(state => getFlagForFeature(state, selectedGameData.flag));
  const leaderboardData = useSelector(state => state.leaderboard.leaderboardData);
  const currentUserId = useSelector(state => state?.auth?.currentUser?.id);
  const currentUserLeaderboardData = useSelector(state => state?.leaderboard?.currentUserLeaderboardData);

  const [isBusy, setIsBusy] = useState(true);

  // Make sure pageSize can cleanly fit in leaderboardLimit. Example defaultPageSize = 25 and limitNumberOnLeaderboard = 100
  const defaultPageSize = 10;
  const limitNumberOnLeaderboard = 2000;

  const paginationRef = useRef();

  const getLeaderboardNumberOfPages = () => {
    const maxNumberOfPages = Math.ceil(limitNumberOnLeaderboard / defaultPageSize);

    return leaderboardData?.data?.totalPages < maxNumberOfPages ? leaderboardData?.data?.totalPages : maxNumberOfPages;
  };

  useEffect(() => {
    // on games like overwatch where they aren't setup yet we need to stop indicator
    setIsBusy(
      selectedGameData.game !== "fortnite" &&
        selectedGameData.game !== "rocketleague" &&
        selectedGameData.game !== "overwatch"
    );
    // eslint-disable-next-line no-unused-expressions
    paginationRef?.current?.goToPageNumber(1); // resets pagination to page 1 if game changes
  }, [query.game]);

  useEffect(() => {
    if (currentUserId) {
      dispatch(getCurrentUserLeaderboardData(selectedGameData.game, currentUserId));
    }
  }, [currentUserId, query.game]);

  useEffect(() => {
    if (leaderboardData && !Array.isArray(leaderboardData)) {
      setIsBusy(false);
    }
  }, [leaderboardData]);

  const getNextLeaderboardSet = page => {
    if (page >= 1) {
      setIsBusy(true);
      dispatch(getLeaderboardData(selectedGameData.game, page, defaultPageSize));
    }
  };

  const leagueoflegends = stat => {
    // league info can be array so find appropriate info
    const foundLeagueInfo = stat?.stats?.leagueInfo?.find(info => info?.queueType === "RANKED_SOLO_5x5");
    return `${capitalize(foundLeagueInfo?.tier)} ${foundLeagueInfo?.rank}`;
  };

  const overwatch = stat => {
    return stat?.overwatchStats?.segments.stats.wins.displayValue;
  };

  const rocketleague = () => {
    return "";
  };

  const fornite = () => {
    return "";
  };

  const valorant = stat => {
    return (
      <EFTooltip tooltipContent={`Ranked Rating: ${stat?.stats?.rankedRating || "Unranked"}`} tooltipPlacement="top">
        <p className={Style.role}>{stat?.stats?.competitiveTierName || "N/A"}</p>
      </EFTooltip>
    );
  };

  const aimlab = stat => {
    return stat?.stats?.ranking?.rank?.displayName;
  };

  const getRank = {
    leagueoflegends,
    overwatch,
    rocketleague,
    fornite,
    valorant,
    aimlab
  };

  return (
    <>
      <div className={Style.subContent}>
        {!showLeaderboard && (
          <div className={Style.comingSoonContainer}>
            <img
              src={`https://cdn.efuse.gg/uploads/pipeline/top100/${gameSlug}+Badge.png`}
              className={Style.comingSoonImage}
              alt={selectedGameData.title}
            />
            <h2>COMING SOON</h2>
          </div>
        )}
        <div className={Style.subheaderWrapper}>
          <EFSubHeader
            positionUnderNav={false}
            navigationList={getPipelineNavigationList(query)}
            includeButtonsInScroll
          />
        </div>

        <div className={`mt-3 ${Style.data} ${!showLeaderboard && Style.blur}`}>
          <div>
            {isBusy && <SkeletonLoader skeletons={1} postHeight={140} engagementHeight={0} />}
            {currentUserLeaderboardData?.data && (
              <div>
                <div className={`row ${Style.headerList}`}>
                  <div className={`col-md-4 ${Style.headerItem}`}>
                    <span>Where do you rank?</span>
                  </div>
                </div>
                {/* the current users personal place on the leaderboard */}
                <LeaderboardBodyRows
                  stats={[currentUserLeaderboardData?.data]}
                  customStat={stat => getRank[selectedGameData.game](stat)}
                />
              </div>
            )}
            <div>
              <div className={Style.responsive}>
                <LeaderBoardHeaderRow
                  // This is happening because total docs gets all docs in collection
                  title={`${
                    leaderboardData?.data?.totalDocs > limitNumberOnLeaderboard
                      ? limitNumberOnLeaderboard
                      : leaderboardData?.data?.totalDocs
                  } - ${selectedGameData.title}`}
                  tooltipText={selectedGameData.tooltip}
                  customStatColumnName={selectedGameData.customStatColumnName}
                />
              </div>
              {isBusy && <SkeletonLoader skeletons={10} postHeight={84} engagementHeight={0} />}
              {!isBusy && (
                <LeaderboardBodyRows
                  stats={leaderboardData?.data?.docs}
                  customStat={stat => getRank[selectedGameData.game](stat)}
                />
              )}
            </div>
          </div>
        </div>
        {getLeaderboardNumberOfPages() > 1 && (
          <div className="text-center pt-2">
            <EFPaginationBar
              ref={paginationRef}
              totalPages={getLeaderboardNumberOfPages()}
              onPageChange={currentPage => getNextLeaderboardSet(currentPage)}
              marginPages={1}
              pageRange={2}
            />
          </div>
        )}
      </div>
    </>
  );
};

export default PipelineLeaderboardWrapper;
