import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

import EFAvatar from "../../../../EFAvatar/EFAvatar";
import { formatDate } from "../../../../../helpers/GeneralHelper";

import CreateNote from "./CreateNote/CreateNote";
import { getProfileNotes } from "../../../../../store/actions/recruitingActions";
import Style from "./Notes.module.scss";

const Notes = ({ recruitId, recruiterId, recruitType }) => {
  const dispatch = useDispatch();
  const profileNotes = useSelector(state => state.recruiting.notes);

  useEffect(() => {
    dispatch(getProfileNotes(recruitId, recruiterId));
  }, [recruitId]);

  const content = profileNotes.length ? (
    profileNotes.map((profileNote, index) => {
      const noteOwner = profileNote.ownerType === "organizations" ? profileNote.owner : profileNote.author;

      const profilePicture =
        profileNote.ownerType === "organizations"
          ? profileNote.owner.profileImage.url
          : profileNote.author.profilePicture.url;
      return (
        <div className={Style.noteContainer} key={index}>
          <div className={`row ${Style.note}`}>
            <div className="col-md-1">
              <EFAvatar displayOnlineButton={false} profilePicture={profilePicture} size="small" />
            </div>
            <div className={`col-md-11 ${Style.noteDetails}`}>
              <p className={Style.username}>{noteOwner.name}</p>
              <p className={Style.noteText}>{profileNote.content}</p>
              <span className={Style.noteDate}>{formatDate(profileNote.createdAt)}</span>
            </div>
          </div>
        </div>
      );
    })
  ) : (
    <div className={Style.noNotes}>
      <p className={Style.message}>No Notes Yet</p>
      <p className={Style.subMessage}>Start type to enter your note</p>
    </div>
  );

  return (
    <div className={Style.notesWrapper}>
      <div className={Style.notes}>{content}</div>
      <CreateNote recruitId={recruitId} recruiterId={recruiterId} recruitType={recruitType} />
    </div>
  );
};

export default Notes;
