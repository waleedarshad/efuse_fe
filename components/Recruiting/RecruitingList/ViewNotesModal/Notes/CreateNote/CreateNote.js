import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";

import EFAvatar from "../../../../../EFAvatar/EFAvatar";
import InputField from "../../../../../InputField/InputField";
import { rescueNil, getImage } from "../../../../../../helpers/GeneralHelper";
import { createProfileNote } from "../../../../../../store/actions/recruitingActions";
import Style from "./CreateNote.module.scss";

const CreateNote = ({ recruitId, recruiterId, recruitType }) => {
  const dispatch = useDispatch();
  const user = useSelector(state => state.auth.currentUser);
  const [note, setNote] = useState("");
  const profilePicture = rescueNil(user, "profilePicture");
  const enterPressed = e => {
    if (e.which === 13) {
      if (note) {
        const createNoteData = {
          author: recruiterId,
          content: note,
          relatedModel: "recruitmentprofiles",
          relatedDoc: recruitId,
          owner: recruiterId,
          ownerType: recruitType === "ORGANIZATION" ? "organizations" : "users"
        };

        dispatch(createProfileNote(createNoteData));
        analytics.track("PIPELINE_RECRUITMENT_ADDED_NOTE", {
          prospect: recruitId
        });
        setNote("");
      }
    }
  };
  return (
    <div className={Style.createNote}>
      <div className={Style.avatar}>
        <EFAvatar
          displayOnlineButton={false}
          profilePicture={getImage(profilePicture, "avatar")}
          size="small"
          href={`/u/${user?.username}`}
        />
      </div>

      <div className={Style.input}>
        <InputField
          theme="whiteShadow"
          placeholder="Start typing to add a note"
          name="note"
          value={note}
          onChange={e => setNote(e.target.value)}
          onKeyPress={e => enterPressed(e)}
        />
        {note !== "" && (
          <div className={Style.infoContainer}>
            <small className={Style.infoText}>Press enter to post</small>
          </div>
        )}
      </div>
    </div>
  );
};

export default CreateNote;
