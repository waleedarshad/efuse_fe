import React from "react";

import Modal from "../../../Modal/Modal";
import Notes from "./Notes/Notes";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";
import Style from "../ViewMoreModal/ViewMoreModal.module.scss";

const ViewNotesModal = ({ recruitId, recruiterId, recruitType }) => {
  return (
    <Modal
      displayCloseButton
      allowBackgroundClickClose
      openOnLoad={false}
      component={<Notes recruitId={recruitId} recruiterId={recruiterId} recruitType={recruitType} />}
      onOpenModal={() =>
        analytics.track("PIPELINE_RECRUITMENT_OPENED_NOTES", {
          prospect: recruitId
        })
      }
    >
      <EFRectangleButton shadowTheme="none" text="Notes" colorTheme="light" className={Style.profileButton} />
    </Modal>
  );
};

export default ViewNotesModal;
