import React from "react";
import { useDispatch } from "react-redux";

import Modal from "../../../Modal/Modal";
import GameStatsAccordion from "../../../User/Portfolio/GameStatsAccordion/GameStatsAccordion";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";
import { updateUserSimple } from "../../../../store/actions/userActions";

const ViewStatsModal = ({ user }) => {
  const dispatch = useDispatch();

  return (
    <Modal
      displayCloseButton
      allowBackgroundClickClose
      openOnLoad={false}
      component={
        <GameStatsAccordion
          gamesToDisplay={["leagueOfLegends", "riot", "statespace"]}
          openAllOnMount
          showNoStatsMessage
        />
      }
      textAlignCenter={false}
      customMaxWidth="800px"
      title="Game Stats"
      onOpenModal={() => {
        analytics.track("PIPELINE_RECRUITMENT_VIEW_STATS", {
          prospect: user._id
        });
        dispatch(updateUserSimple(user));
      }}
    >
      <EFRectangleButton shadowTheme="small" text="Stats" colorTheme="light" />
    </Modal>
  );
};

export default ViewStatsModal;
