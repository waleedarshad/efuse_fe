import React from "react";
import { Row, Col } from "react-bootstrap";
import InfiniteScroll from "react-infinite-scroller";

import AnimatedLogo from "../../../AnimatedLogo";
import NoRecordFound from "../../../NoRecordFound/NoRecordFound";
import RecruitingCard from "../RecruitingCard/RecruitingCard";

const RecruitingListview = ({ recruiting, hasNextPage, isGettingFirstRecruit, loadMoreProfiles }) => {
  const recruitingProfiles = recruiting.map((recruit, index) => {
    return (
      <Col lg={12} md={12} key={index}>
        <RecruitingCard recruit={recruit} />
      </Col>
    );
  });

  return (
    <>
      <InfiniteScroll
        pageStart={1}
        loadMore={loadMoreProfiles}
        hasMore={hasNextPage}
        loader={<AnimatedLogo key={0} theme="inline" />}
      >
        <Row>
          {isGettingFirstRecruit && (
            <div className="text-center" style={{ width: "100%" }}>
              <AnimatedLogo key={0} theme="" />
            </div>
          )}
          {recruitingProfiles.length === 0 ? <NoRecordFound /> : recruitingProfiles}
        </Row>
      </InfiniteScroll>
    </>
  );
};

export default RecruitingListview;
