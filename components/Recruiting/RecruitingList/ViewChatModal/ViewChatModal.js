import React from "react";
import dynamic from "next/dynamic";
import { useDispatch } from "react-redux";
import { startChatWith } from "../../../../store/actions/messageActions";
import EFPrimaryModal from "../../../Modals/EFPrimaryModal/EFPrimaryModal";

const SingleChat = dynamic(import("../../../Messages/Inbox/SingleChat"), {
  ssr: false
});

const ViewChatModal = ({ userName, userId, isOpen, onClose }) => {
  const dispatch = useDispatch();
  return (
    <EFPrimaryModal
      title={`Send message to ${userName}`}
      isOpen={isOpen}
      onClose={onClose}
      allowBackgroundClickClose
      displayCloseButton
      widthTheme="medium"
      onOpen={() => {
        dispatch(startChatWith(userId, userName));
      }}
    >
      <SingleChat />
    </EFPrimaryModal>
  );
};

export default ViewChatModal;
