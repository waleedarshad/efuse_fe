import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faGamepadAlt } from "@fortawesome/pro-light-svg-icons";
import { Col, Tabs, Tab } from "react-bootstrap";
import Cookies from "js-cookie";

import Internal from "../../Layouts/Internal/Internal";
import Style from "./RecruitingList.module.scss";
import {
  getStarredRecruits,
  clearProfiles,
  toggleRecruitingModal,
  recruitingAs,
  recrutingSelection,
  getRecruits
} from "../../../store/actions/recruitingActions";

import { getPostableOrganizations } from "../../../store/actions/userActions";
import { getCurrentUser } from "../../../store/actions/common/userAuthActions";
import RecruitingFilter from "../RecruitingWrapper/RecruitingFilter/RecruitingFilter";
import RecruitingListview from "./RecruitingListview/RecruitingListview";
import FiltersApplied from "./FiltersApplied/FiltersApplied";
import StarredFiltersApplied from "./StarredFiltersApplied/StarredFiltersApplied";

import FEATURE_FLAGS from "../../../common/featureFlags";
import FeatureFlag from "../../General/FeatureFlag/FeatureFlag";
import FeatureFlagVariant from "../../General/FeatureFlag/FeatureFlagVariant/FeatureFlagVariant";
import SelectOrganization from "./SelectOrganization/SelectOrganization";
import COOKIES from "../../../common/cookies";

export const RECRUITING_TABS = {
  all: "all",
  starred: "starred"
};

let page = 1;
const RecruitingList = props => {
  const dispatch = useDispatch();
  const [tab, setTab] = useState(RECRUITING_TABS.all);

  const totalRecruitingProfiles = useSelector(state => state.recruiting.pagination.totalDocs);
  const loading = useSelector(state => state.loader.loading);
  const postableOrganizations = useSelector(state => state.user.postableOrganizations);
  const isSelectingRecruit = useSelector(state => state.recruiting.isSelectingRecruit);
  const onLoadSelectingRecruit = useSelector(state => state.recruiting.onLoadSelectingRecruit);
  const recruitId = useSelector(state => state.recruiting.recruitId);
  const recruitType = useSelector(state => state.recruiting.recruitType);
  const recruitingProfiles = useSelector(state => state.recruiting.recruiting);
  const hasNextPage = useSelector(state => state.recruiting.pagination.hasNextPage);
  const isGettingFirstRecruit = useSelector(state => state.recruiting.isGettingFirstRecruit);
  const user = useSelector(state => state.auth.currentUser);
  const filters = useSelector(state => state.recruiting.searchFilters);

  const displayName = user?.name;
  let recruiting = displayName;

  const { t } = props;
  useEffect(() => {
    analytics.page("Recruit Landing Page");
    dispatch(getCurrentUser());
    dispatch(getPostableOrganizations());

    const _piplineRecruitingAs = Cookies.get(COOKIES.PIPELINE_RECRUITING_AS);
    const _piplineRecruitingId = Cookies.get(COOKIES.PIPELINE_RECRUITING_ID);

    if (_piplineRecruitingAs) {
      dispatch(recruitingAs(_piplineRecruitingAs, _piplineRecruitingId));
      dispatch(recrutingSelection(_piplineRecruitingId));
    }

    if (tab === RECRUITING_TABS.all) {
      dispatch(getRecruits(1, recruitType, recruitId, filters));
    }
  }, [recruitType]);

  const setRecruitingTab = key => {
    setTab(key);
    dispatch(clearProfiles());
    page = 1;
    getRecruitProfiles(1, recruitType, recruitId, key);
  };

  const toggleModal = isOpen => {
    dispatch(toggleRecruitingModal(isOpen));
  };

  if (recruitType === "ORGANIZATION") {
    recruiting = postableOrganizations.length > 0 && postableOrganizations.find(org => org._id === recruitId).name;
  }

  const updateRecruitingProfiles = (type, recruiterId) => {
    page = 1;
    getRecruitProfiles(1, type, recruiterId, tab);
  };

  const getRecruitProfiles = (nextPage, type, recruiterId, key) => {
    if (key === "all") {
      dispatch(getRecruits(nextPage, type, recruiterId, filters));
    } else {
      dispatch(getStarredRecruits(nextPage, type, recruiterId, filters));
    }
  };

  const loadMoreProfiles = () => {
    page += 1;
    getRecruitProfiles(page, recruitType, recruitId, tab);
  };

  return (
    <Internal metaTitle="eFuse | Recruiting">
      <FeatureFlag name={FEATURE_FLAGS.USER_RECRUITING}>
        <FeatureFlagVariant flagState={true}>
          <>
            <div className={Style.recruitSection}>
              <div className={Style.applicants}>
                <FontAwesomeIcon className={Style.sectionIcon} icon={faGamepadAlt} />
                <h1 className={Style.applicantTitle}>Applicants and Prospects</h1>
              </div>
              {postableOrganizations.length > 0 && (
                <div className={Style.recruitingAs}>
                  <div>
                    <b>RECRUITING AS</b>
                  </div>
                  <a className={Style.recruitingName} onClick={() => toggleModal(true)}>
                    {recruiting}
                  </a>
                </div>
              )}
            </div>
            <div className={Style.bottomContainer}>
              <Col sm={3} className="d-none d-lg-block">
                <RecruitingFilter tab={tab} />
              </Col>
              <Col fluid="md" lg={9}>
                <div className={Style.filters}>
                  {tab === "all" ? <FiltersApplied t={t} /> : <StarredFiltersApplied t={t} />}
                </div>
                <div className={Style.contentHeader}>
                  <div className={Style.header}>APPLICANTS AND PROSPECTS</div>
                  <div className={Style.total}>
                    Total Applicants and Prospects: <b>{totalRecruitingProfiles || "0"}</b>
                  </div>
                </div>
                <Tabs
                  variant="pills"
                  defaultActiveKey="all"
                  id="recruiting"
                  className={Style.tipsContainer}
                  activeKey={tab}
                  onSelect={k => setRecruitingTab(k)}
                  unmountOnExit
                >
                  <Tab
                    eventKey={RECRUITING_TABS.all}
                    disabled={loading}
                    title={<div className={Style.tipHead}>All</div>}
                  >
                    <RecruitingListview
                      recruiting={recruitingProfiles}
                      hasNextPage={hasNextPage}
                      isGettingFirstRecruit={isGettingFirstRecruit}
                      loadMoreProfiles={loadMoreProfiles}
                    />
                  </Tab>
                  <Tab
                    eventKey={RECRUITING_TABS.starred}
                    disabled={loading}
                    title={<div className={Style.tipHead}>Starred</div>}
                  >
                    <RecruitingListview
                      recruiting={recruitingProfiles}
                      hasNextPage={hasNextPage}
                      isGettingFirstRecruit={isGettingFirstRecruit}
                      loadMoreProfiles={loadMoreProfiles}
                    />
                  </Tab>
                </Tabs>
              </Col>
            </div>
            {postableOrganizations.length > 0 && !recruitType && (
              <SelectOrganization
                allowBackgroundClickClose={false}
                displayCloseButton={false}
                onClose={() => toggleModal(false)}
                isOpen={onLoadSelectingRecruit}
                updateRecruitingProfiles={updateRecruitingProfiles}
              />
            )}
            <SelectOrganization
              onClose={() => toggleModal(false)}
              isOpen={isSelectingRecruit}
              allowBackgroundClickClose={false}
              updateRecruitingProfiles={updateRecruitingProfiles}
            />
          </>
        </FeatureFlagVariant>
        <FeatureFlagVariant flagState={false}>
          <div className={Style.permission}>User does not have permission to recruit other users.</div>
        </FeatureFlagVariant>
      </FeatureFlag>
    </Internal>
  );
};

export default RecruitingList;
