import EFDetailsListCard from "../../../../Cards/EFDetailsListCard/EFDetailsListCard";

const DemographicInformation = ({
  recruit: {
    desiredCollegeMajor,
    desiredCollegeRegion,
    desiredCollegeAttributes,
    esportsCareerField,
    desiredCollegeSize
  }
}) => {
  const academicData = () => [
    { title: "Desired Major", value: desiredCollegeMajor },
    { title: "Desired Size of College", value: desiredCollegeSize },
    { title: "Desired College Region", value: desiredCollegeRegion },
    { title: "Desired Attributes of a College/Program", value: desiredCollegeAttributes },
    { title: "Desired Career in Esports", value: esportsCareerField }
  ];

  return <EFDetailsListCard data={academicData()} />;
};

export default DemographicInformation;
