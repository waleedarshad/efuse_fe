import EFDetailsListCard from "../../../../Cards/EFDetailsListCard/EFDetailsListCard";

const DemographicInformation = ({
  recruit: { graduationClass, addressLine1, city, state, zipCode, country, gender }
}) => {
  const demographicData = () => [
    { title: "High School Graduating Class", value: graduationClass },
    { title: "Mailing Address", value: addressLine1 },
    { title: "City", value: city },
    { title: "State", value: state },
    { title: "Zipcode / Postal Code", value: zipCode },
    { title: "Country", value: country },
    { title: "Gender", value: gender }
  ];

  return <EFDetailsListCard data={demographicData()} />;
};

export default DemographicInformation;
