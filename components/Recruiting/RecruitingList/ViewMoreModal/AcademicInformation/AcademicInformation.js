import React from "react";
import EFDetailsListCard from "../../../../Cards/EFDetailsListCard/EFDetailsListCard";

const AcademicInformation = ({
  recruit: {
    gradingScale,
    highSchoolGPA,
    highSchool,
    satTestScore,
    actTestScore,
    toeflTestScores,
    extracurricularActivities,
    country
  }
}) => {
  const academicData = () => {
    const data = [
      { title: "High School / Secondary School", value: highSchool },
      { title: "Grading Scale and Average", value: gradingScale },
      { title: "GPA", value: highSchoolGPA }
    ];
    if (country === "United States") {
      data.push({ title: "ACT Score", value: actTestScore }, { title: "SAT Score", value: satTestScore });
    } else {
      data.push({ title: "TOEFL Score", value: toeflTestScores });
    }
    data.push({ title: "Extracurricular Activities", value: extracurricularActivities });
    return data;
  };

  return <EFDetailsListCard data={academicData()} />;
};
export default AcademicInformation;
