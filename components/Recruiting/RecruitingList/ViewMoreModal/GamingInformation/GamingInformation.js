import Style from "../ViewMoreModal.module.scss";
import getGamesHook from "../../../../../hooks/getHooks/getGamesHook";
import EFDetailsListCard from "../../../../Cards/EFDetailsListCard/EFDetailsListCard";

const GamingInformation = ({
  recruit: {
    primaryGame,
    primaryGameUsername,
    primaryGameHoursPlayed,
    primaryGameRole,
    secondaryGame,
    secondaryGameUsername,
    secondaryGameHoursPlayed,
    secondaryGameRole,
    highSchoolClubTeam,
    user
  }
}) => {
  const games = getGamesHook();
  const primaryGameName = games?.find(it => it.slug === primaryGame)?.title || primaryGame;
  const secondaryGameName = games?.find(it => it.slug === secondaryGame)?.title || secondaryGame;

  const primaryGameData = () => [
    { title: "In-Game Username", value: primaryGameUsername },
    { title: "Hours Played", value: primaryGameHoursPlayed },
    { title: "Game Role", value: primaryGameRole }
  ];

  const secondaryGameData = () => [
    { title: "In-Game Username", value: secondaryGameUsername },
    { title: "Hours Played", value: secondaryGameHoursPlayed },
    { title: "Game Role", value: secondaryGameRole }
  ];

  const miscellaneousData = () => [
    { title: "Do you play in a high school or club esports team?", value: highSchoolClubTeam },
    { title: "Discord Id", value: user.discordUserUniqueId }
  ];

  return (
    <div>
      <div>
        <div>Primary Game</div>
        {primaryGame && <EFDetailsListCard title={primaryGameName} data={primaryGameData()} />}
      </div>
      <div>
        <div className={Style.cardTitle}>Secondary Game</div>
        {secondaryGame && <EFDetailsListCard title={secondaryGameName} data={secondaryGameData()} />}
      </div>
      <div>
        <div className={Style.cardTitle}>Miscellaneous</div>
        {secondaryGame && <EFDetailsListCard data={miscellaneousData()} />}
      </div>
    </div>
  );
};

export default GamingInformation;
