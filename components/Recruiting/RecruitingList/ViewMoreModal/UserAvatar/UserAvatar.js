import Style from "../ViewMoreModal.module.scss";
import { rescueNil, getImage } from "../../../../../helpers/GeneralHelper";
import EFAvatar from "../../../../EFAvatar/EFAvatar";

const UserAvatar = ({ recruit: { user } }) => {
  const profilePicture = rescueNil(user, "profilePicture");
  const displayName = user?.name;
  const displayUsername = `@${user?.username}`;

  return (
    <div className={Style.avatarWrapper}>
      <EFAvatar
        displayOnlineButton={false}
        profilePicture={getImage(profilePicture, "avatar")}
        size="small"
        href={`/u/${user?.username}`}
      />
      <div className={Style.userDetail}>
        <div className={Style.firstname}>{displayName}</div>{" "}
        <div className={Style.username}>{user?.username ? displayUsername : ""}</div>
      </div>
    </div>
  );
};
export default UserAvatar;
