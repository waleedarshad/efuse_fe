import React from "react";
import { Tabs, Tab } from "react-bootstrap";
import { useDispatch } from "react-redux";
import Modal from "../../../Modal/Modal";
import Style from "./ViewMoreModal.module.scss";
import UserAvatar from "./UserAvatar/UserAvatar";
import DemographicInformation from "./DemographicInformation/DemographicInformation";
import AcademicInformation from "./AcademicInformation/AcademicInformation";
import GamingInformation from "./GamingInformation/GamingInformation";
import AcademicPreferences from "./AcademicPreferences/AcademicPreferences";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";
import GameStatsAccordion from "../../../User/Portfolio/GameStatsAccordion/GameStatsAccordion";
import { updateUserSimple } from "../../../../store/actions/userActions";

const content = recruit => {
  return (
    <div className={Style.content}>
      <UserAvatar recruit={recruit} />

      <Tabs variant="pills" defaultActiveKey="demographic" id="recruit-profile-tabs" className={Style.tabsContainer}>
        <Tab eventKey="demographic" tabClassName={Style.profileTab} title={<div>Demographic</div>}>
          <DemographicInformation recruit={recruit} />
        </Tab>
        <Tab eventKey="gaming" tabClassName={Style.profileTab} title={<div>Gaming</div>}>
          <GamingInformation recruit={recruit} />
        </Tab>
        <Tab eventKey="stats" tabClassName={Style.profileTab} title={<div>Stats</div>}>
          <GameStatsAccordion
            gamesToDisplay={["leagueOfLegends", "riot", "statespace"]}
            openAllOnMount
            showNoStatsMessage
          />
        </Tab>
        <Tab eventKey="academic" tabClassName={Style.profileTab} title={<div>Academic Info</div>}>
          <AcademicInformation recruit={recruit} />
        </Tab>
        <Tab eventKey="academicPrefernces" tabClassName={Style.profileTab} title={<div>Academic Preferences</div>}>
          <AcademicPreferences recruit={recruit} />
        </Tab>
      </Tabs>
    </div>
  );
};

const ViewMoreModal = ({ recruit }) => {
  const dispatch = useDispatch();

  return (
    <Modal
      displayCloseButton
      allowBackgroundClickClose
      openOnLoad={false}
      component={content(recruit)}
      textAlignCenter={false}
      customMaxWidth="800px"
      customCloseButtonStyle={Style.closeButton}
      title="View Profile"
      onOpenModal={() => {
        analytics.track("PIPELINE_RECRUITMENT_VIEW_RECRUIT_PROFILE", {
          prospect: recruit.user._id
        });
        analytics.track("PIPELINE_RECRUITMENT_VIEW_STATS", {
          prospect: recruit.user._id
        });
        dispatch(updateUserSimple(recruit.user));
      }}
    >
      <EFRectangleButton shadowTheme="none" text="Profile" colorTheme="light" className={Style.profileButton} />
    </Modal>
  );
};

export default ViewMoreModal;
