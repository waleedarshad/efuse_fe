import React from "react";
import EFPrimaryModal from "../../../Modals/EFPrimaryModal/EFPrimaryModal";
import TwitchPlayerIframe from "../../../TwitchPlayerIframe/TwitchPlayerIframe";

const ViewStreamModal = ({ twitchUserName, isOpen, onClose, onOpen }) => {
  return (
    <EFPrimaryModal
      title="Twitch Stream"
      isOpen={isOpen}
      onClose={onClose}
      allowBackgroundClickClose
      displayCloseButton
      widthTheme="medium"
      onOpen={onOpen}
    >
      <TwitchPlayerIframe twitchChannel={twitchUserName} roundedCorners />
    </EFPrimaryModal>
  );
};

export default ViewStreamModal;
