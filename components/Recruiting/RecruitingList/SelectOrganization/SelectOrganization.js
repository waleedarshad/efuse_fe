import React from "react";

import OrganizationOptions from "./OrganizationOptions/OrganizationOptions";

import EFPrimaryModal from "../../../Modals/EFPrimaryModal/EFPrimaryModal";

const SelectOrganization = ({
  isOpen = true,
  onClose,
  allowBackgroundClickClose,
  displayCloseButton,
  updateRecruitingProfiles
}) => {
  return (
    <EFPrimaryModal
      children={<OrganizationOptions updateRecruitingProfiles={updateRecruitingProfiles} />}
      title="Select your Recruiting Team"
      isOpen={isOpen}
      allowBackgroundClickClose={allowBackgroundClickClose}
      displayCloseButton={displayCloseButton}
      onClose={onClose}
    />
  );
};

export default SelectOrganization;
