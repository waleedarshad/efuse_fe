import { useSelector, useDispatch } from "react-redux";
import { Container } from "react-bootstrap";

import Style from "./OrganizationOptions.module.scss";
import EFAvatar from "../../../../EFAvatar/EFAvatar";
import { rescueNil, getImage } from "../../../../../helpers/GeneralHelper";
import { recrutingSelection } from "../../../../../store/actions/recruitingActions";

const ListOrganizations = props => {
  const dispatch = useDispatch();
  const postableOrganizations = useSelector(state => state.user.postableOrganizations);
  const recruitingSelectionId = useSelector(state => state.recruiting.recruitingSelectionId);

  const updateRecruit = (type, id) => {
    props.setRecruitingType(type);
    props.setRecruitingId(id);
    dispatch(recrutingSelection(id));
  };

  return postableOrganizations.map(org => {
    const profilePicture = rescueNil(org, "profileImage");
    return (
      <Container
        className={`${Style.container} ${org._id === recruitingSelectionId && Style.selected}`}
        key={org._id}
        onClick={() => updateRecruit("ORGANIZATION", org._id)}
      >
        <div className={Style.organization}>
          <div className={Style.avatarWrapper}>
            <EFAvatar displayOnlineButton={false} profilePicture={getImage(profilePicture, "avatar")} size="medium" />
          </div>

          <div className={Style.content}>
            <div>
              <div className={Style.firstname}>{org?.name}</div>{" "}
              <div className={`${Style.username} ${org._id === recruitingSelectionId && Style.selected}`}>
                Your notes and starred recruits will be shared with captains/owner of {org?.name}
              </div>
            </div>
          </div>
        </div>
      </Container>
    );
  });
};

export default ListOrganizations;
