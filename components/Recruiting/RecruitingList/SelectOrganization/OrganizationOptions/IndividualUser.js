import { useSelector, useDispatch } from "react-redux";
import { Container } from "react-bootstrap";

import Style from "./OrganizationOptions.module.scss";
import EFAvatar from "../../../../EFAvatar/EFAvatar";
import { getImage, rescueNil } from "../../../../../helpers/GeneralHelper";
import { recrutingSelection } from "../../../../../store/actions/recruitingActions";

const IndividualUser = props => {
  const dispatch = useDispatch();
  const user = useSelector(state => state.auth.currentUser);
  const profilePicture = rescueNil(user, "profilePicture");
  const displayName = user?.name;
  const recruitingSelectionId = useSelector(state => state.recruiting.recruitingSelectionId);

  const updateRecruit = (type, id) => {
    props.setRecruitingType(type);
    props.setRecruitingId(id);
    dispatch(recrutingSelection(id));
  };

  return (
    <Container
      className={`${Style.container} ${user._id === recruitingSelectionId && Style.selected}`}
      onClick={() => updateRecruit("User", user._id)}
    >
      <div className={Style.organization}>
        <div className={Style.avatarWrapper}>
          <EFAvatar
            displayOnlineButton={false}
            profilePicture={getImage(profilePicture, "avatar")}
            size="medium"
            href={`/u/${user?.username}`}
          />
        </div>

        <div className={Style.content}>
          <div>
            <div className={Style.firstname}>{displayName} (Individual)</div>{" "}
            <div className={`${Style.username} ${user._id === recruitingSelectionId && Style.selected}`}>
              Your notes and starred recruits will not be shared
            </div>
          </div>
        </div>
      </div>
    </Container>
  );
};

export default IndividualUser;
