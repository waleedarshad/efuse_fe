import { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import Cookies from "js-cookie";
import ListOrganizations from "./ListOrganizations";
import IndividualUser from "./IndividualUser";
import EFRectangleButton from "../../../../Buttons/EFRectangleButton/EFRectangleButton";
import Style from "./OrganizationOptions.module.scss";
import { recruitingAs, toggleRecruitingModal } from "../../../../../store/actions/recruitingActions";
import COOKIES from "../../../../../common/cookies";

const OrganizationOptions = ({ updateRecruitingProfiles }) => {
  const postableOrganizations = useSelector(state => state.user.postableOrganizations);
  const dispatch = useDispatch();
  const [recruitingType, setRecruitingType] = useState(null);
  const [recruitingId, setRecruitingId] = useState(null);

  const updateRecruit = () => {
    Cookies.set(COOKIES.PIPELINE_RECRUITING_AS, recruitingType);
    Cookies.set(COOKIES.PIPELINE_RECRUITING_ID, recruitingId);

    dispatch(recruitingAs(recruitingType, recruitingId));
    dispatch(toggleRecruitingModal(false));
    updateRecruitingProfiles(recruitingType, recruitingId);
  };

  return (
    <div>
      <IndividualUser setRecruitingType={setRecruitingType} setRecruitingId={setRecruitingId} />
      <ListOrganizations
        postableOrganizations={postableOrganizations}
        setRecruitingType={setRecruitingType}
        setRecruitingId={setRecruitingId}
      />
      <div className={Style.buttonContainer}>
        <EFRectangleButton colorTheme="primary" text="Continue" onClick={updateRecruit} disabled={!recruitingType} />
      </div>
    </div>
  );
};

export default OrganizationOptions;
