import React, { useState } from "react";
import { useDispatch } from "react-redux";

import ViewNotesModal from "../../ViewNotesModal/ViewNotesModal";
import ViewMoreModal from "../../ViewMoreModal/ViewMoreModal";

import Style from "./ButtonsSection.module.scss";

import {
  performRecruitAction,
  performRecruitActionAsOrganization
} from "../../../../../store/actions/recruitingActions";
import EFSelectDropdown from "../../../../Dropdowns/EFSelectDropdown/EFSelectDropdown";

const recruitActions = [
  { label: "No Action", value: "No Action" },
  { label: "Contacted", value: "Contacted" },
  { label: "Recruiting", value: "Recruiting" },
  { label: "Try-Out", value: "Try Out" },
  { label: "Offered", value: "Offered" },
  { label: "Committed", value: "Committed" }
];

const ButtonsSection = ({ recruit, recruiterId, recruitType }) => {
  const dispatch = useDispatch();
  const [isInitialOnChange, setIsInitialOnChange] = useState(true);

  const onChange = action => {
    // the SelectDropDown fires an onChange when we first set the default value, but we don't want to record that as an event.
    if (!isInitialOnChange) {
      analytics.track("PIPELINE_RECRUITMENT_CHANGED_ACTION", {
        prospect: recruit.user._id,
        action
      });
    } else {
      setIsInitialOnChange(true);
    }

    if (recruitType === "ORGANIZATION") {
      dispatch(performRecruitActionAsOrganization(recruit._id, recruiterId, action));
    } else {
      dispatch(performRecruitAction(recruit._id, action));
    }
  };

  return (
    <>
      <div className={Style.buttonContainer}>
        <ViewMoreModal recruit={recruit} />
      </div>

      <div className={Style.buttonContainer}>
        <ViewNotesModal recruitId={recruit._id} recruiterId={recruiterId} recruitType={recruitType} />
      </div>
      <div className={Style.dropdownBox}>
        <EFSelectDropdown
          options={recruitActions}
          defaultValue={recruit.recruitAction || "No Action"}
          onSelect={value => onChange(value)}
        />
      </div>
    </>
  );
};

export default ButtonsSection;
