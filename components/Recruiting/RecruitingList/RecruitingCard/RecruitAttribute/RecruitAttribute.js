import React from "react";

import Style from "./RecruitAttribute.module.scss";

const RecruitAttribute = ({ game, inGameRank }) => {
  return (
    <div className={Style.attribute}>
      <div>
        Primary game: <b>{game || ""}</b>
      </div>
      <div className={Style.attribute}>
        Rank: <b>{inGameRank || ""}</b>
      </div>
    </div>
  );
};

export default RecruitAttribute;
