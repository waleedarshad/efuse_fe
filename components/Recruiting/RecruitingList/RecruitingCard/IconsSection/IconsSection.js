import React, { useState } from "react";
import Link from "next/link";
import { useDispatch } from "react-redux";
import { faStar } from "@fortawesome/pro-solid-svg-icons";
import { faShare, faEnvelope } from "@fortawesome/pro-regular-svg-icons";
import { faStar as falStar } from "@fortawesome/pro-light-svg-icons";
import { faTwitch } from "@fortawesome/free-brands-svg-icons";
import { useRouter } from "next/router";
import EFCircleIconButton from "../../../../Buttons/EFCircleIconButton/EFCircleIconButton";
import ViewStreamModal from "../../ViewStreamModal/ViewStreamModal";
import ViewChatModal from "../../ViewChatModal/ViewChatModal";
import { withCdn } from "../../../../../common/utils";
import { SHARE_LINK_KINDS } from "../../../../ShareLinkModal/ShareLinkModal";
import { markRecruitStarred, deleteStarredRecruit } from "../../../../../store/actions/recruitingActions";
import EFTooltip from "../../../../tooltip/EFTooltip/EFTooltip";
import { shareLinkModal } from "../../../../../store/actions/shareAction";
import Style from "./IconsSection.module.scss";
import EFLightDropdown from "../../../../Dropdowns/EFLightDropdown/EFLightDropdown";
import EFMenuItem from "../../../../Dropdowns/EFMenuItem/EFMenuItem";
import EFMenuItemStyle from "../../../../Dropdowns/EFMenuItem/EFMenuItem.module.scss";

const IconsSection = ({ recruit, currentUser, recruiterId, recruitType }) => {
  const dispatch = useDispatch();
  const router = useRouter();

  const [isChatModalOpen, toggleChatModal] = useState(false);
  const [isStreamModalOpen, toggleStreamModal] = useState(false);

  const markStarred = recruitId => {
    analytics.track("PIPELINE_RECRUITMENT_STARRED_PROSPECT", {
      prospect: recruit.user._id
    });

    const starData = {
      creator: recruiterId,
      relatedDoc: recruitId,
      relatedModel: "recruitmentprofiles",
      owner: recruiterId,
      ownerType: recruitType === "ORGANIZATION" ? "organizations" : "users"
    };

    dispatch(markRecruitStarred(starData));
  };

  const deleteStarred = recruitId => {
    analytics.track("PIPELINE_RECRUITMENT_UNSTARRED_PROSPECT", {
      prospect: recruit.user._id
    });

    dispatch(deleteStarredRecruit(recruiterId, recruitId));
  };

  const userClicked = id => {
    analytics.track("PIPELINE_RECRUITMENT_VIEW_USER_PROFILE", {
      prospect: id
    });
  };

  const twitchClicked = id => {
    analytics.track("PIPELINE_RECRUITMENT_VIEW_TWITCH", {
      prospect: id
    });
  };

  const portfolioShareClicked = user => {
    analytics.track("PIPELINE_RECRUITMENT_SHARED_PORTFOLIO", {
      prospect: user?._id
    });

    dispatch(
      shareLinkModal(
        true,
        `Share ${user?.name}'s Portfolio`,
        `https://efuse.gg/u/${user?.username}`,
        SHARE_LINK_KINDS.PORTFOLIO
      )
    );
  };

  const portfolioMessageClicked = id => {
    analytics.track("PIPELINE_RECRUITMENT_MESSAGE_PROSPECT", {
      prospect: id
    });
  };

  const menuItems = [
    {
      option: (
        <p className={EFMenuItemStyle.menuItem}>
          <img src={withCdn("/static/images/mark.svg")} alt="Portfolio" className={EFMenuItemStyle.icon} />
          eFuse Portfolio
        </p>
      ),

      onClick: () => {
        router.push(`/u/${recruit.user.username}`);
        userClicked(recruit.user._id);
      }
    },
    {
      option: <EFMenuItem icon={faShare} text="Share" iconColorTheme="black" textColorTheme="black" />,
      onClick: () => portfolioShareClicked(recruit?.user)
    },
    {
      option: <EFMenuItem icon={faEnvelope} text="Message" iconColorTheme="black" textColorTheme="black" />,
      onClick: () => toggleChatModal(true)
    },
    {
      option: (
        <EFMenuItem
          icon={recruit.starred ? faStar : falStar}
          text="Star"
          iconColorTheme="black"
          textColorTheme="black"
        />
      ),
      onClick: () => {
        if (recruit.starred) {
          deleteStarred(recruit._id);
        } else {
          markStarred(recruit._id);
        }
      }
    }
  ];

  if (recruit?.user?.twitchVerified && recruit?.user?.twitchUserName) {
    menuItems.unshift({
      option: <EFMenuItem icon={faTwitch} text="Twitch" iconColorTheme="black" textColorTheme="black" />,
      onClick: () => toggleStreamModal(true)
    });
  }

  return (
    <>
      <div className={Style.dropdown}>
        <EFLightDropdown menuItems={menuItems} />
      </div>
      <div className={Style.buttonContent}>
        {recruit?.user?.twitchVerified && recruit?.user?.twitchUserName && (
          <EFTooltip tooltipContent="View Twitch Profile" tooltipPlacement="top">
            <div className={Style.icon}>
              <ViewStreamModal
                twitchUserName={recruit?.user?.twitchUserName}
                isOpen={isStreamModalOpen}
                onOpen={() => twitchClicked(recruit?.user?._id)}
                onClose={() => toggleStreamModal(false)}
              />
              <EFCircleIconButton shadowTheme="none" icon={faTwitch} onClick={() => toggleStreamModal(true)} />
            </div>
          </EFTooltip>
        )}
        <EFTooltip tooltipContent="View eFuse Profile" tooltipPlacement="top">
          <div className={`${Style.icon} ${Style.efuseBtn}`}>
            <Link
              href={`/u/${recruit.user.username}`}
              as={`/u/${recruit.user.username}`}
              onKeyPress={() => {
                userClicked(recruit.user._id);
              }}
              onClick={() => {
                userClicked(recruit.user._id);
              }}
            >
              <img src={withCdn("/static/images/mark.svg")} alt="Loading" className={Style.mark} />
            </Link>
          </div>
        </EFTooltip>
        <EFTooltip tooltipContent="Share" tooltipPlacement="top">
          <div className={Style.icon}>
            <EFCircleIconButton
              icon={faShare}
              shadowTheme="none"
              onClick={() => portfolioShareClicked(recruit?.user)}
            />
          </div>
        </EFTooltip>
        <EFTooltip tooltipContent="Message" tooltipPlacement="top">
          <div className={Style.icon}>
            <ViewChatModal
              userName={recruit.user.name}
              userId={recruit.user._id}
              currentUser={currentUser}
              isOpen={isChatModalOpen}
              onOpen={() => portfolioMessageClicked(recruit?.user?._id)}
              onClose={() => toggleChatModal(false)}
            />
            <EFCircleIconButton shadowTheme="none" icon={faEnvelope} onClick={() => toggleChatModal(true)} />
          </div>
        </EFTooltip>
        <EFTooltip tooltipContent="Star for Later" tooltipPlacement="top">
          <div className={Style.icon}>
            <EFCircleIconButton
              icon={recruit.starred ? faStar : falStar}
              shadowTheme="none"
              onClick={() => {
                if (recruit.starred) {
                  deleteStarred(recruit._id);
                } else {
                  markStarred(recruit._id);
                }
              }}
            />
          </div>
        </EFTooltip>
      </div>
    </>
  );
};

export default IconsSection;
