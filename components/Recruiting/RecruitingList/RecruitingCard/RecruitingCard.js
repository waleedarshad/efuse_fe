import React from "react";
import PropTypes from "prop-types";
import { useSelector } from "react-redux";

import VisibilitySensor from "react-visibility-sensor";
import Style from "./RecruitingCard.module.scss";
import { rescueNil, getImage } from "../../../../helpers/GeneralHelper";
import IconsSection from "./IconsSection/IconsSection";
import RecruiteAttribute from "./RecruitAttribute/RecruitAttribute";
import ButtonsSection from "./ButtonsSection/ButtonsSection";
import getGamesHook from "../../../../hooks/getHooks/getGamesHook";
import getUserLeaderBoardDataHook from "../../../../hooks/getHooks/getUserLeaderBoardDataHook";
import EFApplicantCard from "../../../Cards/EFApplicantCard/EFApplicantCard";

const RecruitingCard = ({ recruit }) => {
  const currentUser = useSelector(state => state.auth.currentUser);
  const recruitType = useSelector(state => state.recruiting.recruitType);
  const recruiterId = useSelector(state => state.recruiting.recruitId);

  const games = getGamesHook();

  const user = recruit?.user;
  const profilePicture = rescueNil(user, "profilePicture");
  const displayName = user?.name;
  const displayUsername = `@${user?.username}`;
  const displayGame = games?.find(it => it.slug === recruit.primaryGame)?.title || recruit.primaryGame;

  const userLeaderBoard = getUserLeaderBoardDataHook(recruit.primaryGame, user._id);

  const leagueOfLegendsStats = () => {
    // league info can be array so find appropriate info
    const foundLeagueInfo = userLeaderBoard?.stats?.leagueInfo?.find(info => info?.queueType === "RANKED_SOLO_5x5");
    return foundLeagueInfo;
  };

  const inGameName =
    leagueOfLegendsStats()?.summonerName || userLeaderBoard?.stats?.username || userLeaderBoard?.stats?.gameName;

  const getInGameRank = () => {
    return leagueOfLegendsStats()
      ? `${leagueOfLegendsStats()?.tier} ${leagueOfLegendsStats()?.rank}`
      : userLeaderBoard?.stats?.competitiveTierName || userLeaderBoard?.stats?.ranking?.rank?.displayName;
  };

  return (
    <VisibilitySensor
      onChange={isVisible => {
        if (isVisible) {
          analytics.track("RECRUITING_BROWSE_LIST_VIEW");
        }
      }}
    >
      <div className={Style.cardContainer}>
        <EFApplicantCard
          titleImageUrl={getImage(profilePicture, "avatar")}
          titleImageHref={`u/${user.username}`}
          headerText={displayName}
          subHeaderText={displayUsername}
          headerTags={[recruit.state, recruit.country, `Class of ${recruit.graduationClass}`]}
          headerActionButtons={
            <IconsSection
              recruit={recruit}
              currentUser={currentUser}
              recruiterId={recruiterId}
              recruitType={recruitType}
            />
          }
          body={
            <RecruiteAttribute
              game={displayGame}
              inGameName={inGameName}
              inGameRank={getInGameRank()}
              leaderBoardRank={userLeaderBoard?.rank}
              hoursPlayed={recruit.primaryGameHoursPlayed}
              role={recruit.primaryGameRole}
            />
          }
          footerActionButtons={<ButtonsSection recruit={recruit} recruiterId={recruiterId} recruitType={recruitType} />}
        />
      </div>
    </VisibilitySensor>
  );
};

RecruitingCard.propTypes = {
  recruit: PropTypes.object.isRequired
};

export default RecruitingCard;
