import React from "react";
import { FormGroup } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";

import { useQuery } from "@apollo/client";
import Style from "./RecruitingFilter.module.scss";
import InputLabel from "../../../InputLabel/InputLabel";
import SelectBox from "../../../SelectBox/SelectBox";

import {
  updateFilterValue,
  searchRecruits,
  updateSearchObject,
  searchStarredRecruits
} from "../../../../store/actions/recruitingFilterActions";
import { applyFilters } from "../../../../helpers/FilterHelper";
import {
  formattedCountries,
  formattedGender,
  formattedGraduationYears,
  formattedStates,
  UNITED_STATES
} from "../../../Settings/Recruiting/dropdownData";
import { GET_PIPELINE_GAMES } from "../../../../graphql/GameQuery";

// eslint-disable-next-line import/no-cycle
import { RECRUITING_TABS } from "../../RecruitingList/RecruitingList";

const RecruitingFilter = ({ avoidStickySidebar, tab }) => {
  const searchFilters = useSelector(state => state.recruiting.searchFilters);
  const filtersApplied = useSelector(state => state.recruiting.filtersApplied);

  const { data } = useQuery(GET_PIPELINE_GAMES);
  const formattedGames = [
    { label: "Select Game", value: "" },
    ...(data?.getPipelineGames?.map(it => ({ value: it.slug, label: it.title })) || [])
  ];

  const dispatch = useDispatch();

  const onChange = event => {
    const { name, value } = event.target;
    setFilterAndSearch(name, value);
  };

  const search = (searchObject, filters, limit) => {
    if (tab === RECRUITING_TABS.all) {
      dispatch(searchRecruits(searchObject, filters, limit));
    } else {
      dispatch(searchStarredRecruits(searchObject, filters, limit));
    }
  };

  const setFilterAndSearch = (name, value) => {
    dispatch(updateFilterValue(name, value));
    dispatch(updateSearchObject({ [name]: value }));
    applyFilters(name, value, value, filtersApplied, search);
  };

  return (
    <div className={`${!avoidStickySidebar && "stickySidebar"} ${Style.filterWrapper}`}>
      <div className={Style.card}>
        <div className={Style.header}>
          <h5 className={Style.headerTitle}>Filter</h5>
        </div>
        <div className={Style.body}>
          <InputLabel theme="darkColor">Graduation Class</InputLabel>
          <FormGroup>
            <SelectBox
              disabled={false}
              name="graduationClass"
              options={formattedGraduationYears}
              validated={false}
              theme="whiteShadow"
              value={searchFilters.graduationClass}
              required
              onChange={onChange}
              size="lg"
              block="true"
            />
          </FormGroup>

          <InputLabel theme="darkColor">Country</InputLabel>
          <FormGroup>
            <SelectBox
              disabled={false}
              name="country"
              options={formattedCountries}
              validated={false}
              theme="whiteShadow"
              value={searchFilters.country}
              required
              onChange={onChange}
              size="lg"
              block="true"
            />
          </FormGroup>

          {searchFilters.country === UNITED_STATES && (
            <>
              <InputLabel theme="darkColor">State</InputLabel>

              <FormGroup>
                <SelectBox
                  disabled={false}
                  name="state"
                  options={formattedStates}
                  validated={false}
                  theme="whiteShadow"
                  value={searchFilters.state}
                  required
                  onChange={onChange}
                  size="lg"
                  block="true"
                />
              </FormGroup>
            </>
          )}

          <>
            <InputLabel theme="darkColor">Gender</InputLabel>
            <FormGroup>
              <SelectBox
                disabled={false}
                name="gender"
                options={formattedGender}
                validated={false}
                theme="whiteShadow"
                value={searchFilters.gender}
                required
                onChange={onChange}
                size="lg"
                block="true"
              />
            </FormGroup>
          </>

          <>
            <InputLabel theme="darkColor">Primary Game</InputLabel>

            <FormGroup>
              <SelectBox
                disabled={false}
                name="primaryGame"
                options={formattedGames}
                validated={false}
                theme="whiteShadow"
                value={searchFilters.primaryGame}
                required
                onChange={onChange}
                size="lg"
                block="true"
              />
            </FormGroup>
          </>
        </div>
      </div>
    </div>
  );
};

export default RecruitingFilter;
