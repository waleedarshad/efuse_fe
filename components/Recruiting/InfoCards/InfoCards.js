import React from "react";
import { Card, CardDeck } from "react-bootstrap";

import Style from "./InfoCards.module.scss";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";

export default function InfoCards() {
  return (
    <div className={Style.infoCards}>
      <CardDeck>
        <Card className={Style.infoCard}>
          <Card.Img
            variant="top"
            src="https://cdn.efuse.gg/uploads/pipeline/homepage-info-cards/scholarships.png"
            className={Style.cardImg}
          />
          <Card.Body className={Style.cardBody}>
            <Card.Text className={Style.cardTextTitle}>Scholarships</Card.Text>
            <Card.Text className={Style.cardText}>
              eFuse connects young gamers to scholarships throughout the collegiate esports community. Take your shot at
              earning an esports scholarship today!
            </Card.Text>
          </Card.Body>
          <Card.Footer className={Style.cardFooter}>
            <EFRectangleButton
              text="CURRENT SCHOLARSHIPS"
              colorTheme="secondary"
              internalHref="/opportunities"
              width="fullWidth"
            />
          </Card.Footer>
        </Card>
        <Card className={Style.infoCard}>
          <Card.Img
            variant="top"
            src="https://cdn.efuse.gg/uploads/pipeline/homepage-info-cards/learning.png"
            className={Style.cardImg}
          />
          <Card.Body className={Style.cardBody}>
            <Card.Text className={Style.cardTextTitle}>News Articles</Card.Text>
            <Card.Text className={Style.cardText}>
              Navigating through high school esports can be challenging. Take advantage of eFuse's professionally-made
              news articles and put yourself in a position for esports success!
            </Card.Text>
          </Card.Body>
          <Card.Footer className={Style.cardFooter}>
            <EFRectangleButton text="NEWS ARTICLES" colorTheme="secondary" internalHref="/news" width="fullWidth" />
          </Card.Footer>
        </Card>
        <Card className={Style.infoCard}>
          <Card.Img
            variant="top"
            src="https://cdn.efuse.gg/uploads/pipeline/homepage-info-cards/organizations.png"
            className={Style.cardImg}
          />

          <Card.Body className={Style.cardBody}>
            <Card.Text className={Style.cardTextTitle}>Organizations</Card.Text>
            <Card.Text className={Style.cardText}>
              Colleges and universities around the world offer tremendous opportunities to gamers of all levels. Learn
              about and take advantage of collegiate esports programs and other organizations today!
            </Card.Text>
          </Card.Body>
          <Card.Footer className={Style.cardFooter}>
            <EFRectangleButton
              text="ORGANIZATION LISTINGS"
              colorTheme="secondary"
              internalHref="/organizations"
              width="fullWidth"
            />
          </Card.Footer>
        </Card>
      </CardDeck>
    </div>
  );
}
