import React from "react";
import { Row, Col } from "react-bootstrap";
import EFImage from "../../EFImage/EFImage";

import Style from "./CTASection.module.scss";
import EFCard from "../../Cards/EFCard/EFCard";
import JoinThePipelineModalButton from "../JoinThePipelineModalButton/JoinThePipelineModalButton";

const CTASection = ({ toggleModal, newPipelineOnboarding }) => {
  return (
    <div className={Style.mainWrapper}>
      <EFCard widthTheme="fullWidth">
        <div className={Style.content}>
          <Row className="align-items-center">
            <Col md={6} className={Style.textContainer}>
              <p className={Style.text}>
                <span className={Style.textTitle}>Ready to get Recruited?</span>
                <br />
                <span className={Style.textTitle}> Join the leaderboard today.</span>
                <br />
                <span className={Style.textDesc}>
                  Earn your way to the top of the rankings by increasing your skills, impact, & experience.
                </span>
              </p>

              <div className={Style.buttonContainer}>
                <JoinThePipelineModalButton
                  toggleModal={toggleModal}
                  newPipelineOnboarding={newPipelineOnboarding}
                  openModalOnLoad={false}
                  buttonColorTheme="light"
                />
              </div>
            </Col>
            <Col md={6} className={Style.imageContainer}>
              <EFImage className={Style.logo} src="https://cdn.efuse.gg/uploads/pipeline/recruitingCTA.png" />
            </Col>
          </Row>
        </div>
      </EFCard>
    </div>
  );
};
export default CTASection;
