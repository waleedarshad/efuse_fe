import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { faEdit } from "@fortawesome/pro-regular-svg-icons";
import { faPlus } from "@fortawesome/pro-solid-svg-icons";

import EventsModal from "../../../GlobalModals/EventsModal/EventsModal";
import EFCircleIconButton from "../../../Buttons/EFCircleIconButton/EFCircleIconButton";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";
import DisplayHonorsAndEvents from "../DisplayHonorsAndEvents/DisplayHonorsAndEvents";
import { removeData } from "../../../../store/actions/portfolioActions";
import Style from "../HonorsAndEvents.module.scss";

const PortfolioEvents = ({ events, isOwner }) => {
  const dispatch = useDispatch();
  const [showMore, setShowMore] = useState(false);

  let portfolioEvents = events?.slice(0, 2);
  if (showMore) {
    portfolioEvents = events;
  }

  const displayEvents = portfolioEvents?.map((data, index) => {
    return (
      <DisplayHonorsAndEvents
        data={data}
        isOwner={isOwner}
        key={index}
        onRemove={() => {
          analytics.track("PORTFOLIO_EVENTS_DELETE", {
            eventId: data._id
          });
          dispatch(removeData("/portfolio/portfolio_event", data._id));
        }}
        editModal={
          <EventsModal portfolioEvent={data} edit>
            <EFCircleIconButton colorTheme="transparent" shadowTheme="none" icon={faEdit} />
          </EventsModal>
        }
      />
    );
  });
  return (
    <div>
      {isOwner && (
        <div className={Style.add}>
          <EventsModal>
            <EFCircleIconButton icon={faPlus} />
          </EventsModal>
        </div>
      )}
      {displayEvents}
      {events?.length > 2 && (
        <div className={Style.showMoreButton}>
          <EFRectangleButton
            text={showMore ? "Show Less" : "Show More"}
            onClick={() => {
              setShowMore(!showMore);
            }}
            colorTheme="light"
          />
        </div>
      )}
    </div>
  );
};

export default PortfolioEvents;
