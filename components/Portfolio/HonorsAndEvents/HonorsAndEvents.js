import React from "react";
import { useSelector } from "react-redux";
import Tab from "react-bootstrap/Tab";
import Tabs from "react-bootstrap/Tabs";

import Style from "./HonorsAndEvents.module.scss";
import CardLayout from "../CardLayout/CardLayout";
import PortfolioEvents from "./PortfolioEvents/PortfolioEvents";
import PortfolioHonors from "./PortfolioHonors/PortfolioHonors";
import { sortArrayByDate } from "../../../helpers/UserPortfolioHelper";

const HonorsAndEvents = () => {
  const user = useSelector(state => state.user.currentUser);
  const currentUser = useSelector(state => state.auth.currentUser);
  const mockExternalUser = useSelector(state => state.portfolio.mockExternalUser);
  let isOwner = currentUser && user._id === currentUser._id;
  if (mockExternalUser) {
    isOwner = false;
  }
  const honors = sortArrayByDate(user.portfolioHonors, "eventDate");
  const events = sortArrayByDate(user.portfolioEvents, "eventDate");
  let displayEvents = <></>;
  let displayHonors = <></>;

  const eventsTab = (
    <Tab eventKey="events" tabClassName={Style.tab} title="Events">
      <PortfolioEvents events={events} isOwner={isOwner} />
    </Tab>
  );
  const honorsTab = (
    <Tab tabClassName={Style.tab} eventKey="honors" title="Honors">
      <PortfolioHonors honors={honors} isOwner={isOwner} />
    </Tab>
  );

  let activeTab = "events";
  if (!isOwner) {
    if (events?.length) {
      displayEvents = eventsTab;
    }
    if (honors?.length) {
      displayHonors = honorsTab;
      activeTab = "honors";
    }
    if (events?.length && honors?.length) {
      displayEvents = eventsTab;
      displayHonors = honorsTab;
      activeTab = "events";
    }
  } else {
    displayEvents = eventsTab;
    displayHonors = honorsTab;
  }

  return (
    <CardLayout>
      <Tabs className={Style.tabsWrapper} defaultActiveKey={activeTab}>
        {displayEvents}
        {displayHonors}
      </Tabs>
    </CardLayout>
  );
};

export default HonorsAndEvents;
