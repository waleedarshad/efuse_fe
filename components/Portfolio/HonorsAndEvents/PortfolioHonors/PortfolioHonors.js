import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { faEdit } from "@fortawesome/pro-regular-svg-icons";
import { faPlus } from "@fortawesome/pro-solid-svg-icons";

import HonorsModal from "../../../GlobalModals/HonorsModal/HonorsModal";
import EFCircleIconButton from "../../../Buttons/EFCircleIconButton/EFCircleIconButton";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";
import DisplayHonorsAndEvents from "../DisplayHonorsAndEvents/DisplayHonorsAndEvents";
import { removeData } from "../../../../store/actions/portfolioActions";
import Style from "../HonorsAndEvents.module.scss";

const PortfolioHonor = ({ honors, isOwner }) => {
  const dispatch = useDispatch();
  const [showMore, setShowMore] = useState(false);
  let portfolioHonors = honors?.slice(0, 2);
  if (showMore) {
    portfolioHonors = honors;
  }

  const displayHonor = portfolioHonors?.map((data, index) => {
    return (
      <DisplayHonorsAndEvents
        data={data}
        isOwner={isOwner}
        key={index}
        onRemove={() => {
          analytics.track("PORTFOLIO_HONORS_DELETE", {
            eventId: data._id
          });
          dispatch(removeData("/portfolio/portfolio_honor", data._id));
        }}
        editModal={
          <HonorsModal portfolioHonor={data} edit>
            <EFCircleIconButton colorTheme="transparent" shadowTheme="none" icon={faEdit} />
          </HonorsModal>
        }
      />
    );
  });
  return (
    <div>
      {isOwner && (
        <div className={Style.add}>
          <HonorsModal>
            <EFCircleIconButton icon={faPlus} />
          </HonorsModal>
        </div>
      )}
      {displayHonor}
      {honors?.length > 2 && (
        <div className={Style.showMoreButton}>
          <EFRectangleButton
            text={showMore ? "Show Less" : "Show More"}
            onClick={() => {
              setShowMore(!showMore);
            }}
            colorTheme="light"
          />
        </div>
      )}
    </div>
  );
};

export default PortfolioHonor;
