import React from "react";
import { faTrash } from "@fortawesome/pro-solid-svg-icons";

import ConfirmAlert from "../../../ConfirmAlert/ConfirmAlert";
import EFCircleIconButton from "../../../Buttons/EFCircleIconButton/EFCircleIconButton";
import { EFHtmlParser } from "../../../EFHtmlParser/EFHtmlParser";
import { formatDate } from "../../../../helpers/GeneralHelper";
import Style from "./DisplayHonorsAndEvents.module.scss";

const DisplayHonorsAndEvents = ({ data, onRemove, editModal, isOwner }) => {
  return (
    <div className={Style.contentWrapper}>
      <h6>
        {data.title}
        {isOwner && (
          <>
            {editModal}
            <ConfirmAlert
              title="This action can't be undone"
              message="Are you sure you want to remove?"
              onYes={onRemove}
            >
              <EFCircleIconButton icon={faTrash} colorTheme="transparent" shadowTheme="none" />
            </ConfirmAlert>
          </>
        )}
      </h6>
      <p className={Style.subTitle}>{data.subTitle}</p>
      {data.eventDate && <small>{formatDate(data.eventDate)}</small>}
      <p className={Style.description}>
        <EFHtmlParser>{data.description ? data.description : "No description"}</EFHtmlParser>
      </p>
    </div>
  );
};

export default DisplayHonorsAndEvents;
