import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { faEdit, faTrashAlt } from "@fortawesome/pro-regular-svg-icons";
import { faPlus } from "@fortawesome/pro-solid-svg-icons";

import Style from "../Experiences/Experiences.module.scss";
import { formatDate } from "../../../helpers/GeneralHelper";
import { sortArrayByDate } from "../../../helpers/UserPortfolioHelper";
import EducationExperienceModal from "../../GlobalModals/EducationExperienceModal/EducationExperienceModal";
import ConfirmAlert from "../../ConfirmAlert/ConfirmAlert";
import { removeData } from "../../../store/actions/portfolioActions";
import CardLayout from "../CardLayout/CardLayout";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";
import EFCircleIconButton from "../../Buttons/EFCircleIconButton/EFCircleIconButton";
import { EFHtmlParser } from "../../EFHtmlParser/EFHtmlParser";
import EFImage from "../../EFImage/EFImage";

const Education = () => {
  const dispatch = useDispatch();
  const [showMore, setShowMore] = useState(false);
  const user = useSelector(state => state.user.currentUser);
  const currentUser = useSelector(state => state.auth.currentUser);
  const mockExternalUser = useSelector(state => state.portfolio.mockExternalUser);
  let isCurrentUser = currentUser && user?._id === currentUser?._id;
  if (mockExternalUser) {
    isCurrentUser = false;
  }

  const sortedEducation = sortArrayByDate(user?.educationExperience, "startDate");

  let educationExperiences = sortedEducation?.slice(0, 2);
  if (showMore) {
    educationExperiences = sortedEducation;
  }

  const educations = educationExperiences?.map((education, index) => (
    <div key={index} className={Style.experience}>
      <div className={Style.experienceImage}>
        <EFImage src={education?.image?.url} width={50} height={50} />
      </div>
      <div className={Style.experienceDetail}>
        <h6 className={Style.title}>
          {education.school}
          {isCurrentUser && (
            <>
              <EducationExperienceModal experience={education} edit>
                <div className={Style.buttonWrapper}>
                  <EFCircleIconButton icon={faEdit} colorTheme="transparent" shadowTheme="none" />
                </div>
              </EducationExperienceModal>
              <ConfirmAlert
                title="This action can't be undone"
                message="Are you sure you want to remove?"
                onYes={() => dispatch(removeData("/portfolio/education_experience", education._id))}
              >
                <div className={Style.buttonWrapper}>
                  <EFCircleIconButton icon={faTrashAlt} colorTheme="transparent" shadowTheme="none" />
                </div>
              </ConfirmAlert>
            </>
          )}
        </h6>
        {education.degree && <p className={Style.subTitle}>{education.degree}</p>}
        <p className={Style.date}>
          {formatDate(education.startDate)}
          {" - "}
          {education.present ? "Present" : formatDate(education.endDate)}
        </p>
        <p className={Style.description}>
          <EFHtmlParser>{education.description ? education.description : "No description"}</EFHtmlParser>
        </p>
      </div>
    </div>
  ));
  return (
    <CardLayout>
      {isCurrentUser && (
        <div className={Style.add}>
          <EducationExperienceModal>
            <EFCircleIconButton icon={faPlus} />
          </EducationExperienceModal>{" "}
        </div>
      )}
      <h5 className={Style.heading}>Education</h5>
      {educations}
      {user?.educationExperience?.length > 2 && (
        <div className={Style.showMoreButton}>
          <EFRectangleButton
            text={showMore ? "Show Less" : "Show More"}
            onClick={() => {
              setShowMore(!showMore);
            }}
            colorTheme="light"
          />
        </div>
      )}
    </CardLayout>
  );
};

export default Education;
