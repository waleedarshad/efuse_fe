import React from "react";

import PortfolioScores from "./PortfolioScores";
import FEATURE_FLAGS from "../../../common/featureFlags";
import FeatureFlag from "../../General/FeatureFlag/FeatureFlag";
import { mountWithStore } from "../../../common/testUtils";
import PortfolioIcon from "../PortfolioIcon/PortfolioIcon";

describe("PortfolioScores", () => {
  const stats = {
    hypeScore: 500,
    totalEngagements: 1000,
    postViews: 4500
  };
  const streak = 57;
  const state = {
    features: {
      social_impact_scores: true
    }
  };
  it("renders the correct feature flag", () => {
    const { subject } = mountWithStore(<PortfolioScores stats={stats} streak={streak} />, state);
    expect(subject.find(FeatureFlag).prop("name")).toEqual(FEATURE_FLAGS.SOCIAL_IMPACT_SCORES);
  });

  it("renders the correct portfolio icons", () => {
    const { subject } = mountWithStore(<PortfolioScores stats={stats} streak={streak} />, state);
    expect(
      subject
        .find(PortfolioIcon)
        .at(0)
        .prop("name")
    ).toEqual("HYPE SCORE");
    expect(
      subject
        .find(PortfolioIcon)
        .at(1)
        .prop("name")
    ).toEqual("STREAK");
    expect(
      subject
        .find(PortfolioIcon)
        .at(2)
        .prop("name")
    ).toEqual("ENGAGEMENTS");
    expect(
      subject
        .find(PortfolioIcon)
        .at(3)
        .prop("name")
    ).toEqual("VIEWS");
  });
  it("renders icons with the correct values", () => {
    const { subject } = mountWithStore(<PortfolioScores stats={stats} streak={streak} />, state);
    expect(
      subject
        .find(PortfolioIcon)
        .at(0)
        .prop("value")
    ).toEqual(stats.hypeScore);
    expect(
      subject
        .find(PortfolioIcon)
        .at(1)
        .prop("value")
    ).toEqual(streak);
    expect(
      subject
        .find(PortfolioIcon)
        .at(2)
        .prop("value")
    ).toEqual(stats.totalEngagements);
    expect(
      subject
        .find(PortfolioIcon)
        .at(3)
        .prop("value")
    ).toEqual(stats.postViews);
  });
});
