import { faBolt } from "@fortawesome/pro-duotone-svg-icons";
import { faEye, faFlame, faHandshakeAlt } from "@fortawesome/pro-solid-svg-icons";
import React from "react";
import FEATURE_FLAGS from "../../../common/featureFlags";
import FeatureFlag from "../../General/FeatureFlag/FeatureFlag";
import FeatureFlagVariant from "../../General/FeatureFlag/FeatureFlagVariant/FeatureFlagVariant";
import PortfolioIcon from "../PortfolioIcon/PortfolioIcon";
import Style from "./PortfolioScores.module.scss";

const PortfolioScores = ({ stats, streak, alignRight }) => {
  return (
    <div>
      <FeatureFlag name={FEATURE_FLAGS.SOCIAL_IMPACT_SCORES}>
        <FeatureFlagVariant flagState>
          <div className={`${alignRight && Style.alignScores} ${Style.socialImpactIconsWrap}`}>
            <PortfolioIcon icon={faFlame} value={stats?.hypeScore} name="HYPE SCORE" color="#ff0000" />

            <PortfolioIcon icon={faBolt} value={streak} name="STREAK" color="#6000ff" />

            <PortfolioIcon icon={faHandshakeAlt} value={stats?.totalEngagements} name="ENGAGEMENTS" color="#15151a" />
            <PortfolioIcon icon={faEye} value={stats?.postViews} name="VIEWS" color="#15151a" />
          </div>
        </FeatureFlagVariant>
      </FeatureFlag>
    </div>
  );
};

PortfolioScores.defaultProps = {
  portfolio: false
};

export default PortfolioScores;
