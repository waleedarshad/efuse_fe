import React from "react";

import Style from "./CardLayout.module.scss";

const CardLayout = ({ children }) => {
  return <div className={Style.cardWrapper}>{children}</div>;
};

export default CardLayout;
