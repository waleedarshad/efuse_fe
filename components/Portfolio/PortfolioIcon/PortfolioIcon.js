import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import Style from "./PortfolioIcon.module.scss";

const PortfolioIcon = ({ icon, value, name, color }) => {
  return (
    <div className={Style.infoWrap}>
      <div className={`${Style.portfolioIcon}`}>
        <FontAwesomeIcon icon={icon} className={Style.icon} style={{ color }} />
      </div>
      <div className={Style.portfolioInfo}>
        <span className={Style.portfolioInfoText}>{value || 0}</span>
      </div>
      <div className={Style.nameWrapper}>
        <span className={Style.iconName}>{name}</span>
      </div>
    </div>
  );
};
export default PortfolioIcon;
