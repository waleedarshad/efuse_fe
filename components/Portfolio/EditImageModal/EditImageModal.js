import React, { useState } from "react";
import { Col } from "react-bootstrap";
import { useDispatch } from "react-redux";

import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";
import ImageUploader from "../../ImageUploader/ImageUploader";
import EFPrimaryModal from "../../Modals/EFPrimaryModal/EFPrimaryModal";
import InputRow from "../../InputRow/InputRow";
import { updateCurrentUser } from "../../../store/actions/userActions";
import { toggleCropModal } from "../../../store/actions/settingsActions";

const EditImageModal = ({ title, isOpen, closeModal, imageUrl, imageProp, aspectRatioHeight, aspectRatioWidth }) => {
  const dispatch = useDispatch();
  const [image, setImage] = useState({});

  const onDrop = selectedFile => {
    setImage(selectedFile[0]);
  };

  const onCropChange = croppedImage => {
    const { blob } = croppedImage;
    const croppedFile = new File([blob], blob.name, { type: blob.type });
    setImage(croppedFile);
  };

  const toggleCropperModal = (val, name) => {
    dispatch(toggleCropModal(val, name));
  };

  const userSubmission = () => {
    const userData = new FormData();
    userData.append(imageProp, image);
    dispatch(updateCurrentUser(userData));
    closeModal();
  };

  return (
    <EFPrimaryModal title={title} allowBackgroundClickClose={false} isOpen={isOpen} onClose={closeModal}>
      <InputRow>
        <Col sm={12}>
          <ImageUploader
            text="Select Custom Image"
            name="image"
            onDrop={onDrop}
            theme="internal"
            value={imageUrl || "https://cdn.efuse.gg/uploads/static/global/mindblown_white.png"}
            onCropChange={onCropChange}
            toggleCropperModal={toggleCropperModal}
            aspectRatioWidth={aspectRatioWidth}
            aspectRatioHeight={aspectRatioHeight}
            showCropper
          />
        </Col>
      </InputRow>
      <InputRow>
        <Col className="text-center">
          <EFRectangleButton disabled={!image.name} text="Update" onClick={() => userSubmission()} />
        </Col>
      </InputRow>
    </EFPrimaryModal>
  );
};

export default EditImageModal;
