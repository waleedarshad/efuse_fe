import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { faEdit, faTrashAlt, faPlus } from "@fortawesome/pro-regular-svg-icons";

import Style from "./Experiences.module.scss";
import { formatDate } from "../../../helpers/GeneralHelper";
import { sortArrayByDate } from "../../../helpers/UserPortfolioHelper";
import { EFHtmlParser } from "../../EFHtmlParser/EFHtmlParser";
import BusinessExperienceModal from "../../GlobalModals/BusinessExperienceModal/BusinessExperienceModal";
import { removeData } from "../../../store/actions/portfolioActions";
import ConfirmAlert from "../../ConfirmAlert/ConfirmAlert";
import CardLayout from "../CardLayout/CardLayout";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";
import EFCircleIconButton from "../../Buttons/EFCircleIconButton/EFCircleIconButton";
import EFImage from "../../EFImage/EFImage";

const Experiences = () => {
  const dispatch = useDispatch();
  const [showMore, setShowMore] = useState(false);
  const user = useSelector(state => state.user.currentUser);
  const currentUser = useSelector(state => state.auth.currentUser);
  const mockExternalUser = useSelector(state => state.portfolio.mockExternalUser);
  let isCurrentUser = currentUser && user._id === currentUser._id;
  if (mockExternalUser) {
    isCurrentUser = false;
  }

  const sortedExperience = sortArrayByDate(user?.businessExperience, "startDate");

  let businessExperiences = sortedExperience?.slice(0, 2);
  if (showMore) {
    businessExperiences = sortedExperience;
  }

  const experiences = businessExperiences?.map((experience, index) => (
    <div className={Style.experience} key={index}>
      <div className={Style.experienceImage}>
        <EFImage src={experience?.image?.url} width={50} height={50} />
      </div>
      <div className={Style.experienceDetail}>
        <h6 className={Style.title}>
          {experience.title}
          {isCurrentUser && (
            <>
              <BusinessExperienceModal experience={experience} edit>
                <div className={Style.buttonWrapper}>
                  <EFCircleIconButton icon={faEdit} colorTheme="transparent" shadowTheme="none" />
                </div>
              </BusinessExperienceModal>
              <ConfirmAlert
                title="This action can't be undone"
                message="Are you sure you want to remove?"
                onYes={() => {
                  analytics.track("PORTFOLIO_WORK_EXPERIENCE_DELETE", {
                    experienceId: experience._id
                  });
                  dispatch(removeData("/portfolio/business_experience", experience._id));
                }}
              >
                <div className={Style.buttonWrapper}>
                  <EFCircleIconButton icon={faTrashAlt} colorTheme="transparent" shadowTheme="none" />
                </div>
              </ConfirmAlert>
            </>
          )}
        </h6>
        <p className={Style.subTitle}>{experience.subTitle}</p>
        <p className={Style.date}>
          {formatDate(experience.startDate)}
          {" - "}
          {experience.present ? "Present" : formatDate(experience.endDate)}
        </p>
        <p className={Style.description}>
          <EFHtmlParser>{experience.description ? experience.description : "No description"}</EFHtmlParser>
        </p>
      </div>
    </div>
  ));
  return (
    <CardLayout>
      <h5 className={Style.heading}>Experience</h5>
      {experiences}
      {isCurrentUser && (
        <>
          <div className={Style.add}>
            <BusinessExperienceModal>
              <EFCircleIconButton icon={faPlus} />
            </BusinessExperienceModal>
          </div>
        </>
      )}
      {user?.businessExperience?.length > 2 && (
        <div className={Style.showMoreButton}>
          <EFRectangleButton
            text={showMore ? "Show Less" : "Show More"}
            onClick={() => {
              setShowMore(!showMore);
            }}
            colorTheme="light"
          />
        </div>
      )}
    </CardLayout>
  );
};

export default Experiences;
