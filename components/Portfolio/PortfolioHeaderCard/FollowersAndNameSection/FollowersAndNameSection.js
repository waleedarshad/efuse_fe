import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBolt } from "@fortawesome/pro-duotone-svg-icons";
import { useSelector } from "react-redux";

import EFInfoPill from "../../../EFInfoPill/EFInfoPill";
import VerifiedIcon from "../../../VerifiedIcon/VerifiedIcon";
import { rescueNil } from "../../../../helpers/GeneralHelper";
import Style from "./FollowersAndNameSection.module.scss";

const FollowersAndNameSection = ({ user, streak }) => {
  const followers = useSelector(state => state.followers.totalFollowers);
  const followees = useSelector(state => state.followers.totalFollowees);

  const verified = rescueNil(user, "verified");

  return (
    <>
      <EFInfoPill>{`${followers || 0} Followers`} </EFInfoPill>
      <EFInfoPill>{`${followees || 0} Following`} </EFInfoPill>
      <span className={Style.fullname} date-cy="portfolio_user_name">
        {user?.name}
        {verified && <VerifiedIcon />}
      </span>
      <span>
        <span>{`@${rescueNil(user, "username")}`}</span>
        {streak > 1 && (
          <span className={`${Style.boltWrapper} ml-2`}>
            <FontAwesomeIcon icon={faBolt} className={Style.boltIcon} />
            <span className={Style.streakDuration}>{streak}</span>
          </span>
        )}
      </span>
    </>
  );
};

export default FollowersAndNameSection;
