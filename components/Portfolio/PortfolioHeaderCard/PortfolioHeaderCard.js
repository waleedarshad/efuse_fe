import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCalendarAlt, faCamera } from "@fortawesome/pro-solid-svg-icons";

import { getImage, rescueNil, formatDate } from "../../../helpers/GeneralHelper";
import ProgressBar from "../../PortfolioProgress/PortfolioProgressBar";
import PortfolioScores from "../PortfolioScores/PortfolioScores";
import EFAvatar from "../../EFAvatar/EFAvatar";
import { EFHtmlParser } from "../../EFHtmlParser/EFHtmlParser";
import FollowersAndNameSection from "./FollowersAndNameSection/FollowersAndNameSection";
import ButtonsSection from "./ButtonsSection/ButtonsSection";
import EditImageModal from "../EditImageModal/EditImageModal";
import EFCircleIconButtonTooltip from "../../Buttons/EFCircleIconButtonTooltip/EFCircleIconButtonTooltip";
import Style from "./PortfolioHeaderCard.module.scss";

const PortfolioHeaderCard = ({ user, currentUser, isOwner, followButton }) => {
  const [openModal, setOpenModal] = useState(false);

  const streak = user?.streaks?.daily?.streakDuration;
  const stats = user?.stats;

  const closeModal = () => {
    setOpenModal(false);
  };

  return (
    <div className={Style.headerCard}>
      <div className="row m-0">
        <div className={`col-lg-2 ${Style.profilePicture}`}>
          <EFAvatar
            profilePicture={getImage(user.profilePicture)}
            size="extraLarge"
            online={user?.online && user?.showOnline}
          />
          {isOwner && (
            <>
              <div className={Style.editButton}>
                <EFCircleIconButtonTooltip
                  icon={faCamera}
                  tooltipContent="Edit Profile Picture"
                  tooltipPlacement="top"
                  onClick={() => setOpenModal(true)}
                />
              </div>
              <EditImageModal
                title="Update Profile Picture"
                imageUrl={user.profilePicture?.url}
                isOpen={openModal}
                closeModal={closeModal}
                imageProp="profilePicture"
                aspectRatioWidth={160}
                aspectRatioHeight={160}
              />
            </>
          )}
        </div>
        <div className="col-lg-4 pt-3">
          <FollowersAndNameSection user={user} streak={streak} />
        </div>
        <div className={`col-lg-6 pt-3 ${Style.portfolioLinksWrap}`}>
          <ButtonsSection user={user} followButton={followButton} isOwner={isOwner} />
        </div>
      </div>
      <div className="row m-0 mt-4">
        <div className="col-lg-6">
          <div>
            <p className={Style.bioText} data-cy="portfolio_bio_text">
              <EFHtmlParser>{rescueNil(user, "bio")}</EFHtmlParser>
            </p>
          </div>
          <div>
            <small>
              <FontAwesomeIcon icon={faCalendarAlt} />
              {currentUser && ` Joined ${formatDate(user?.createdAt)}`}
            </small>
          </div>
        </div>
        <div className="col-lg-6">
          <div className={Style.socialImpactScoresBox}>
            <PortfolioScores stats={stats} streak={streak} alignRight />
          </div>
          {isOwner && (
            <div className={Style.progressWrapper}>
              <ProgressBar user={user} />
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default PortfolioHeaderCard;
