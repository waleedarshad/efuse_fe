import React from "react";
import { useDispatch } from "react-redux";
import { faUserSlash, faEllipsisV } from "@fortawesome/pro-solid-svg-icons";
import { useMutation, useQuery } from "@apollo/client";

import CustomDropdown from "../../../../CustomDropdown/CustomDropdown";
import Style from "./PortfolioHeaderDropdown.module.scss";
import EFCircleIconButton from "../../../../Buttons/EFCircleIconButton/EFCircleIconButton";
import { blockUser } from "../../../../../store/actions/blockingActions/blockingActions";
import { GET_BLOCKED_USERS, UNBLOCK_USER } from "../../../../../graphql/BlockedUsersQuery";
import { sendNotification } from "../../../../../helpers/FlashHelper";

const PortfolioHeaderDropdown = ({ userId, username }) => {
  const dispatch = useDispatch();

  const { data } = useQuery(GET_BLOCKED_USERS);
  const blockedUsers = data?.blockedUsers || [];

  const [unblockUser] = useMutation(UNBLOCK_USER, {
    refetchQueries: [{ query: GET_BLOCKED_USERS }],
    awaitRefetchQueries: true
  });

  const unblock = (blockeeId, _username) => {
    // eslint-disable-next-line @typescript-eslint/no-floating-promises
    unblockUser({ variables: { blockeeId } }).then(() => {
      sendNotification(`${_username} unblocked successfully`, "success", "Success");
    });
  };

  let items = [
    {
      title: "Block",
      icon: faUserSlash,
      as: "button",
      type: "button",
      onClick: () => {
        dispatch(blockUser(userId, username));
      }
    }
  ];

  const blockedIds = blockedUsers.map(u => u._id);

  if (blockedIds.length > 0 && blockedIds.includes(userId)) {
    items = [
      {
        title: "Unblock",
        icon: faUserSlash,
        as: "button",
        type: "button",
        onClick: () => unblock(userId, username)
      }
    ];
  }

  const customToggle = <EFCircleIconButton icon={faEllipsisV} />;

  return (
    <CustomDropdown
      items={items}
      customClass={Style.portfolioHeaderDropdown}
      icon={Style.toggle}
      customToggle={customToggle}
    />
  );
};

export default PortfolioHeaderDropdown;
