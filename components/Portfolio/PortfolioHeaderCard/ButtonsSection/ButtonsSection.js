import React from "react";
import { useSelector } from "react-redux";
import Router from "next/router";
import { faShare } from "@fortawesome/pro-solid-svg-icons";
import { faEdit } from "@fortawesome/pro-regular-svg-icons";

import TwitchButton from "../../../Buttons/TwitchButton/TwitchButton";
import MessageButton from "../../../MessageButton/MessageButton";
import ShareLinkButton from "../../../ShareLinkButton";
import EFCircleIconButton from "../../../Buttons/EFCircleIconButton/EFCircleIconButton";
import SignupModal from "../../../SignupModal/SignupModal";
import Style from "./ButtonsSection.module.scss";
import FeatureFlag from "../../../General/FeatureFlag/FeatureFlag";
import FeatureFlagVariant from "../../../General/FeatureFlag/FeatureFlagVariant/FeatureFlagVariant";
import FEATURE_FLAGS from "../../../../common/featureFlags";
import PortfolioHeaderDropdown from "./PortfolioHeaderDropdown/PortfolioHeaderDropdown";

const ButtonsSection = ({ user, followButton, isOwner }) => {
  const messagingEnabled = useSelector(state => state.features.messaging);
  const currentUser = useSelector(state => state.auth.currentUser);

  const isLive = user?.twitch?.stream?.isLive;
  const { twitchVerified } = user;
  const userLoggedIn = currentUser?.id;

  const openPortfolioEditPage = () => {
    Router.push("/settings");
  };

  return (
    <>
      {messagingEnabled && !isOwner && (
        <div className={Style.buttonWrapper}>
          <MessageButton userName={user.name} userId={user._id} />
        </div>
      )}
      {isOwner && (
        <div className={Style.buttonWrapper}>
          <EFCircleIconButton onClick={openPortfolioEditPage} icon={faEdit} />
        </div>
      )}
      <div className={Style.buttonWrapper}>
        <ShareLinkButton
          userId={user._id}
          title={`Share ${isOwner ? "your" : `${user.name}'s`} Portfolio`}
          url={`https://efuse.gg/u/${user.username}`}
          icon={faShare}
          theme="roundButton"
        />
      </div>
      {!isOwner && <div className={Style.buttonWrapper}>{followButton}</div>}
      {twitchVerified &&
        (userLoggedIn ? (
          <TwitchButton href={`https://www.twitch.tv/${user?.twitchUserName}`} isLive={isLive} />
        ) : (
          <SignupModal
            title={`Create an account to view @${user?.twitchUserName} on Twitch.`}
            message={`Connect with ${user.name} and other gamers now. Sign up today!`}
          >
            <TwitchButton href="" isLive={isLive} />
          </SignupModal>
        ))}
      {userLoggedIn && (
        <FeatureFlag name={FEATURE_FLAGS.BLOCK_USER}>
          <FeatureFlagVariant flagState>
            {!isOwner && <PortfolioHeaderDropdown userId={user._id} username={user.username} />}
          </FeatureFlagVariant>
        </FeatureFlag>
      )}
    </>
  );
};

export default ButtonsSection;
