import React from "react";
import { connect } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMinusCircle, faPlus } from "@fortawesome/pro-solid-svg-icons";

import Style from "./Skills.module.scss";
import gamingSkills from "../../User/Portfolio/SkillsComponent/GamingSkills/data/gamingSkills.json";
import businessSkills from "../../User/Portfolio/SkillsComponent/BusinessSkills/data/businessSkills.json";
import ConfirmAlert from "../../ConfirmAlert/ConfirmAlert";
import { deleteSkill } from "../../../store/actions/portfolioActions";
import SkillsModal from "../../GlobalModals/SkillsModal/SkillsModal";
import CardLayout from "../CardLayout/CardLayout";
import EFCircleIconButton from "../../Buttons/EFCircleIconButton/EFCircleIconButton";

const Skills = props => {
  const onSubmit = (id, type) => {
    analytics.track(`PORTFOLIO_${type.toUpperCase()}_SKILLS_DELETE`, {
      skillId: id
    });
    props.deleteSkill(`/portfolio/${type}_skill`, id);
  };
  const userSkills = (skills, type, isCurrentUser) => {
    const skillsList = type === "Gaming" ? gamingSkills : businessSkills;
    const content = [];
    skills.map((skill, index) => {
      return skillsList.find(item => {
        if (item.value === skill.value) {
          return content.push(
            <span className={Style.skill} key={index}>
              {item.label}
              {isCurrentUser && (
                <ConfirmAlert
                  title="This action can't be undone"
                  message="Are you sure you want to remove this skill?"
                  onYes={() => onSubmit(skill.value, type)}
                >
                  <FontAwesomeIcon icon={faMinusCircle} className={Style.icon} />
                </ConfirmAlert>
              )}
            </span>
          );
        }
      });
    });
    return content;
  };
  const { user, currentUser, mockExternalUser } = props;
  let isCurrentUser = currentUser && user._id === currentUser._id;
  if (mockExternalUser) {
    isCurrentUser = false;
  }
  const businessSkill = () => {
    if (user?.businessSkills?.length > 0) {
      return userSkills(user.businessSkills, "Business", isCurrentUser);
    }

    return isCurrentUser ? <p>Add Business Skills</p> : <></>;
  };

  const gameSkills = () => {
    if (user.gamingSkills?.length > 0) {
      return userSkills(user.gamingSkills, "Gaming", isCurrentUser);
    }

    return isCurrentUser ? <p>Add Gaming Skills</p> : <></>;
  };

  const getSectionTitle = (title, check) => {
    if (mockExternalUser) {
      return check ? title : "";
    }
    return title;
  };

  return (
    <CardLayout>
      <h5 className={Style.heading}>Skills</h5>
      <div className={Style.sectionWrapper}>
        <p className={Style.subHeader}>{getSectionTitle("Business", user.businessSkills?.length > 0)}</p>
        {isCurrentUser && (
          <SkillsModal type="Business" trackAnalytics={() => analytics.track("PORTFOLIO_BUSINESS_SKILLS_MODAL_OPEN")}>
            <div className={Style.addButton}>
              <EFCircleIconButton icon={faPlus} size="small" />
            </div>
          </SkillsModal>
        )}
      </div>

      <div className={Style.skills}>{businessSkill()}</div>

      <div className={Style.sectionWrapper}>
        <p className={Style.subHeader}>{getSectionTitle("Gaming", user.gamingSkills?.length > 0)}</p>
        {isCurrentUser && (
          <SkillsModal type="Gaming" trackAnalytics={() => analytics.track("PORTFOLIO_GAMING_SKILLS_MODAL_OPEN")}>
            <div className={Style.addButton}>
              <EFCircleIconButton icon={faPlus} size="small" />
            </div>
          </SkillsModal>
        )}
      </div>

      <div className={Style.skills}>{gameSkills()}</div>
    </CardLayout>
  );
};

const mapStateToProps = state => ({
  user: state.user.currentUser,
  currentUser: state.auth.currentUser,
  mockExternalUser: state.portfolio.mockExternalUser
});
export default connect(mapStateToProps, { deleteSkill })(Skills);
