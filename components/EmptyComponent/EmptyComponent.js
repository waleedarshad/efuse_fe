import { Card } from "react-bootstrap";

const EmptyComponent = ({ text, style }) => {
  return (
    <Card className="customCard" style={style}>
      <Card.Body>{text}</Card.Body>
    </Card>
  );
};

export default EmptyComponent;
