import React from "react";
import EFScaledImage from "./EFScaledImage";

export default {
  title: "Shared/EFScaledImage",
  component: EFScaledImage,
  argTypes: {
    imageSrc: "text"
  }
};

const Story = args => (
  <div style={{ width: "500px", height: "500px", position: "relative" }}>
    {/* eslint-disable-next-line react/jsx-props-no-spreading */}
    <EFScaledImage {...args} />
  </div>
);

export const Default = Story.bind({});
Default.args = {
  imageSrc: "https://i.ytimg.com/vi/3KgmY5NrEzU/maxresdefault.jpg"
};
