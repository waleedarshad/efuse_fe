import EFImage from "../EFImage/EFImage";
import Style from "./EFScaledImage.module.scss";

const EFScaledImage = ({ imageSrc }) => <EFImage className={Style.backgroundImage} src={imageSrc} />;

export default EFScaledImage;
