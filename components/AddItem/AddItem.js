import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus } from "@fortawesome/pro-solid-svg-icons";
import Style from "./AddItem.module.scss";
import Modal from "../Modal/Modal";

const AddItem = props => {
  const { text, fontSize, removeBorder, removeShadow, children, modalComponent, modalTitle, adIndex, editMode } = props;
  return (
    <Modal
      displayCloseButton
      allowBackgroundClickClose={false}
      openOnLoad={false}
      component={modalComponent}
      textAlignCenter={false}
      title={modalTitle}
    >
      {editMode ? (
        children
      ) : (
        <div className={Style.wrapper}>
          <p className={`${Style.textAction} ${fontSize === "small" && Style.smallText}`}>
            <FontAwesomeIcon icon={faPlus} className={Style.plusIcon} />
            {text}
          </p>
          <div
            className={`${Style.overlay} ${removeBorder && Style.removeBorder} ${removeShadow && Style.removeShadow}`}
          />
          {children}
        </div>
      )}
    </Modal>
  );
};

export default AddItem;
