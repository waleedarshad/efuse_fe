import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Style from "./PageTitle.module.scss";
import LEADERBOARDS from "../../../common/leaderboards";

const PageTitle = ({ leaderboardType, icon }) => {
  return (
    <div className={Style.titleBar}>
      <div className={Style.textContainer}>
        <FontAwesomeIcon className={`${Style.titleIcon} ${LEADERBOARDS[leaderboardType].iconColor}`} icon={icon} />
        <h1 className={Style.titleText}>{LEADERBOARDS[leaderboardType].pageHeaderText}</h1>
      </div>
    </div>
  );
};

export default PageTitle;
