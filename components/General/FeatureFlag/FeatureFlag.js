import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import PropTypes from "prop-types";

const FeatureFlag = ({ name, experiment, children }) => {
  const isFeatureFlagOn = useSelector(state => state.features[name]);
  const flagsLoaded = useSelector(state => state.features.flags_loaded);

  useEffect(() => {
    // We must wait until real user feature flags are loaded into store (flagsLoaded) before triggering $experiment_started event. Otherwise we risk sending the default variant.
    if (experiment && flagsLoaded) {
      let variantName;

      if (typeof isFeatureFlagOn === "boolean") {
        variantName = `${name}-${isFeatureFlagOn ? "enabled" : "disabled"}`;
      } else {
        variantName = isFeatureFlagOn;
      }

      analytics.track("$experiment_started", {
        "Experiment name": `${name}`,
        "Variant name": variantName
      });
    }
  }, [flagsLoaded]);

  return React.Children.map(children, child => {
    return child.props.flagState === isFeatureFlagOn ? child : <></>;
  });
};

FeatureFlag.propTypes = {
  name: PropTypes.string.isRequired,
  experiment: PropTypes.bool,
  children: PropTypes.element.isRequired
};

FeatureFlag.defaultProps = {
  experiment: false
};

export default FeatureFlag;
