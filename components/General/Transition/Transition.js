import React from "react";
import { animated, useTransition } from "react-spring";

const defaultTransitionProps = {
  from: { opacity: 0 },
  enter: { opacity: 1 },
  leave: { opacity: 0 }
};

const Transition = ({ isMounted, children, containerStyles, transitionProps = defaultTransitionProps }) => {
  const transitions = useTransition(isMounted, null, transitionProps);

  return transitions.map(
    ({ item, props, key }) =>
      item && (
        <animated.div key={key} style={{ ...props, ...containerStyles }}>
          {children}
        </animated.div>
      )
  );
};

export default Transition;
