import React, { useEffect } from "react";
import { NextSeo } from "next-seo";

import ClippyPublicNav from "./ClippyPublicNav/ClippyPublicNav";
import ClippyHeaderSection from "./ClippyHeaderSection/ClippyHeaderSection";
import ClippyHowItWorksSection from "./ClippyHowItWorksSection/ClippyHowItWorksSection";
import ClippyQuoteSection from "./ClippyQuoteSection/ClippyQuoteSection";
import ClippyCTASection from "./ClippyCTASection/ClippyCTASection";
import EFFooter from "../EFFooter/EFFooter";
import StreamToolsPage from "../ClippyConfigPage/pages/StreamToolsPage";
import FEATURE_FLAGS from "../../common/featureFlags";

const ClippyLandingPage = ({ isUserAuthenticated, flagSmithDataSSR }) => {
  useEffect(() => {
    analytics.page("Clippy Landing Page", { isLandingPage: !isUserAuthenticated });
  }, []);

  const isNewNavEnabled = flagSmithDataSSR?.flags?.find(
    flag => flag.feature.name === FEATURE_FLAGS.FEATURE_WEB_CLIPPY_NAV_LAYOUT
  )?.enabled;

  return isUserAuthenticated ? (
    <StreamToolsPage isNewNavEnabled={isNewNavEnabled} />
  ) : (
    <>
      <NextSeo
        title="Clippy - Interactive Streaming Tools | eFuse.gg"
        description="Use Clippy to shoutout and showcase your favourite streamers. Download Clippy for free today and level up your content."
        openGraph={{
          images: [{ url: "https://cdn.efuse.gg/static/images/clippy_og_image_2022.jpg" }]
        }}
        canonical="https://efuse.gg/clippy"
        twitter={{
          handle: "@ClippyTV",
          site: "@site",
          cardType: "summary_large_image"
        }}
      />
      <ClippyPublicNav />
      <ClippyHeaderSection />
      <ClippyHowItWorksSection />
      <ClippyQuoteSection />
      <ClippyCTASection />
      <EFFooter />
    </>
  );
};

export default ClippyLandingPage;
