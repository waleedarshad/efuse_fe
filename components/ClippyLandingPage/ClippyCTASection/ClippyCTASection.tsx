import React from "react";
import styled from "styled-components";

import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";
import DynamicModal from "../../DynamicModal/DynamicModal";
import { mediaQueries } from "../../../styles/sharedStyledComponents/breakpoints";

const ClippyCTASection = () => {
  return (
    <SectionContainer>
      <RoundedRectangle>
        <LargeCTATitle>Elite tools to help you focus on what you do best</LargeCTATitle>
        <SubText>Take your content to the next level with Clippy today</SubText>
        <ButtonContainer>
          <DynamicModal
            flow="LoginSignup"
            openOnLoad={false}
            startView="login"
            displayCloseButton
            allowBackgroundClickClose={false}
          >
            <EFRectangleButton
              colorTheme="transparent"
              fontColorTheme="light"
              fontWeightTheme="semibold"
              shadowTheme="small"
              text="Login"
            />
          </DynamicModal>
          <DynamicModal
            flow="LoginSignup"
            openOnLoad={false}
            startView="signup"
            customRedirect="/clippy"
            displayCloseButton
            allowBackgroundClickClose={false}
            signupModalTitle="Sign up for Clippy"
          >
            <EFRectangleButton colorTheme="secondary" shadowTheme="small" fontWeightTheme="semibold" text="Sign up" />
          </DynamicModal>
        </ButtonContainer>
        <RedBlurEffectRight />
        <BlueBlurEffectRight />
        <ScreenshotImage src="https://cdn.efuse.gg/static/images/clippy_landing_pages/clippy_screenshot_large.png" />
      </RoundedRectangle>
    </SectionContainer>
  );
};

const SectionContainer = styled.div`
  padding: 60px;

  ${mediaQueries("lg")`
  padding: 40px;
  `};

  ${mediaQueries("md")`
  padding: 20px;
  `};

  ${mediaQueries("sm")`
  padding: 10px;
  `};
`;

const RoundedRectangle = styled.div`
  background: #000000;
  box-shadow: 0px 4px 15px rgba(0, 0, 0, 0.25);
  border-radius: 10px;
  position: relative;
  overflow: hidden;
  padding: 90px 80px;

  ${mediaQueries("xl")`
  padding: 60px;
  `};

  ${mediaQueries("md")`
  padding: 30px;
  padding-bottom: 210px;
  `};
`;

const LargeCTATitle = styled.div`
  font-family: Poppins;
  font-style: normal;
  font-weight: 600;
  font-size: 45px;
  line-height: 56px;
  color: #ffffff;
  max-width: 50%;

  ${mediaQueries("xl")`
  max-width: 30%;
  font-size: 28px;
  line-height: 34px;
  `};

  ${mediaQueries("md")`
  max-width: 100%;
  `};
`;

const SubText = styled.div`
  font-family: Open Sans;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 20px;
  margin-top: 20px;
  margin-bottom: 50px;
  color: #ffffff;
  max-width: 50%;
  ${mediaQueries("xl")`
  max-width: 30%;
  `};

  ${mediaQueries("lg")`
  margin: 20px 0;
  `};

  ${mediaQueries("md")`
  max-width: 100%;
  `};
`;

const ButtonContainer = styled.div`
  display: flex;
`;

const ScreenshotImage = styled.img`
  position: absolute;
  max-width: 550px;
  width: 90%;
  height: auto;
  right: 0;
  bottom: 0;
  z-index: 3;

  ${mediaQueries("lg")`
  max-width: 400px;
  `};

  ${mediaQueries("md")`
  max-width: 300px;
  `};
`;

const BlueBlurEffectRight = styled.div`
  position: absolute;
  width: 200px;
  height: 200px;
  top: 55%;
  right: 35%;

  background: #00b7ff;
  filter: blur(60px);
  z-index: 1;

  ${mediaQueries("md")`
  top: 75%;
  right: 35%;
  `};
`;

const RedBlurEffectRight = styled.div`
  position: absolute;
  width: 200px;
  height: 200px;
  top: 20%;
  right: 10%;

  background: #ff00f5;
  filter: blur(90px);
  z-index: 1;

  ${mediaQueries("md")`
  top: 65%;
  right: 10%;
  `};
`;

export default ClippyCTASection;
