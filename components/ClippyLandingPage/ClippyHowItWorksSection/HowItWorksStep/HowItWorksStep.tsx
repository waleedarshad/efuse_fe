import React from "react";
import styled from "styled-components";

const HowItWorksStep = ({ stepNumber, image, title, body }) => {
  return (
    <StepContainer>
      <ImageContainer>
        <StepNumber>{stepNumber}</StepNumber>
        <Image src={image} />
      </ImageContainer>
      <Title>{title}</Title>
      <Description>{body}</Description>
    </StepContainer>
  );
};

const StepContainer = styled.div``;

const ImageContainer = styled.div`
  position: relative;
`;

const StepNumber = styled.div`
  position: absolute;
  left: 10px;
  top: 10px;
  font-family: Poppins;
  font-style: normal;
  font-weight: 600;
  font-size: 24px;
  line-height: 30px;
  color: #ffffff;
`;

const Image = styled.img`
  background: #020202;
  border-radius: 8px;
  margin-bottom: 10px;
  width: 100%;
  height: auto;
`;

const Title = styled.div`
  font-family: Poppins;
  font-style: normal;
  font-weight: 600;
  font-size: 18px;
  line-height: 24px;
  text-align: center;
  color: #12151d;
  margin-bottom: 10px;
`;

const Description = styled.div`
  font-family: Open Sans;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 20px;
  text-align: center;
  color: #12151d;
`;

export default HowItWorksStep;
