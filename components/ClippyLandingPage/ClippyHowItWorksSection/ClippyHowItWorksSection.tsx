import React from "react";
import styled from "styled-components";
import { Container } from "react-bootstrap";
import uniqueId from "lodash/uniqueId";
import { Stream } from "@cloudflare/stream-react";

import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";
import HowItWorksStep from "./HowItWorksStep/HowItWorksStep";
import { mediaQueries } from "../../../styles/sharedStyledComponents/breakpoints";
import DynamicModal from "../../DynamicModal/DynamicModal";

const steps = [
  {
    image: "https://cdn.efuse.gg/static/images/clippy_landing_pages/step1.png",
    title: "Connect Your Twitch",
    body: "To get things up and running, we need to connect to your Twitch."
  },
  {
    image: "https://cdn.efuse.gg/static/images/clippy_landing_pages/step2.png",
    title: "Customize your Clippy",
    body: "One size rarely fits all. Customize Clippy to fit your content, not the other way around."
  },
  {
    image: "https://cdn.efuse.gg/static/images/clippy_landing_pages/step3.png",
    title: "Add Clippy to OBS",
    body: "Drop Clippy in as a browser source to your streaming software of choice. That’s all there is to it!"
  },
  {
    image: "https://cdn.efuse.gg/static/images/clippy_landing_pages/step4.png",
    title: "!so your favorite creators",
    body: "You’re up and away, it’s time to enjoy Clippy with your community."
  }
];
const ClippyHowItWorksSection = () => {
  const videoIdOrSignedUrl = "4b88c5cc8574bc34c4aabfe9514f652f";
  return (
    <SectionContainer>
      <Container>
        <VideoContainer>
          <Stream
            controls
            src={videoIdOrSignedUrl}
            muted
            autoplay
            poster="https://cdn.efuse.gg/static/images/clippy_landing_pages/video_placeholder.png"
          />
        </VideoContainer>
        <SectionTitle>Add the Clippy overlay to engage your community today</SectionTitle>
        <StepsContainer>
          {steps.map((step, index) => (
            <HowItWorksStep
              stepNumber={index + 1}
              key={uniqueId()}
              image={step.image}
              title={step.title}
              body={step.body}
            />
          ))}
        </StepsContainer>
        <ButtonContainer>
          <DynamicModal
            flow="LoginSignup"
            openOnLoad={false}
            startView="signup"
            customRedirect="/clippy"
            displayCloseButton
            allowBackgroundClickClose={false}
            signupModalTitle="Sign up for Clippy"
          >
            <EFRectangleButton
              colorTheme="secondary"
              fontWeightTheme="semibold"
              shadowTheme="small"
              text="Start Using Clippy Now"
            />
          </DynamicModal>
        </ButtonContainer>
      </Container>
    </SectionContainer>
  );
};

const SectionContainer = styled.div`
  background: white;
  width: 100%;
  padding-top: 150px;
  padding-bottom: 100px;
  position: relative;

  ${mediaQueries("md")`
  padding-bottom: 40px;
  `};
`;

const VideoContainer = styled.div`
  position: absolute;
  margin: auto;
  max-width: 760px;
  width: 90%;
  height: 430px;
  top: -350px;
  left: 50%;
  transform: translateX(-50%);
  z-index: 3;

  ${mediaQueries("md")`
  top: -300px;
  height: 350px;
  `};

  ${mediaQueries("sm")`
  top: -110px;
  height: 200px;
  `};
`;

const SectionTitle = styled.div`
  text-align: center;
  font-family: Poppins;
  font-style: normal;
  font-weight: 600;
  font-size: 50px;
  line-height: 58px;

  ${mediaQueries("md")`
  font-size: 32px;
  line-height: 36px;
  `};

  ${mediaQueries("sm")`
  font-size: 28px;
  line-height: 34px;
  `};
`;

const StepsContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  column-gap: 40px;
  row-gap: 25px;
  margin: 50px auto;

  ${mediaQueries("md")`
  grid-template-columns: repeat(2, 1fr);
  `};

  ${mediaQueries("sm")`
  grid-template-columns: repeat(1, 1fr);
  `};
`;

const ButtonContainer = styled.div`
  display: flex;
  justify-content: center;
`;

export default ClippyHowItWorksSection;
