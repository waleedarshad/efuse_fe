import React, { useEffect } from "react";
import styled from "styled-components";
import { Container } from "react-bootstrap";
import { useSelector } from "react-redux";
import { Fade } from "react-reveal";

import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";
import DynamicModal from "../../DynamicModal/DynamicModal";
import { mediaQueries } from "../../../styles/sharedStyledComponents/breakpoints";
import FEATURE_FLAGS from "../../../common/featureFlags";
import { getFlagForFeature } from "../../../store/selectors/featureFlagSelectors";

const ClippyHeaderSection = () => {
  const headerText = useSelector(state => getFlagForFeature(state, FEATURE_FLAGS.EXP_WEB_CLIPPY_LANDING_PAGE_TEXT));
  const flagsLoaded = useSelector(state => state.features.flags_loaded);

  useEffect(() => {
    // We must wait until real user feature flags are loaded into store (flagsLoaded) before triggering $experiment_started event. Otherwise we risk sending the default variant.
    if (flagsLoaded) {
      analytics.track("$experiment_started", {
        "Experiment name": FEATURE_FLAGS.EXP_WEB_CLIPPY_LANDING_PAGE_TEXT,
        "Variant name": headerText
      });
    }
  }, [flagsLoaded]);

  return (
    <HeaderContainer>
      <Container>
        <HeaderTextContainer>
          {headerText && (
            <Fade bottom distance="50px">
              <MainHeaderText>{headerText}</MainHeaderText>
            </Fade>
          )}
        </HeaderTextContainer>
        <ButtonContainer>
          <DynamicModal
            flow="LoginSignup"
            openOnLoad={false}
            startView="signup"
            customRedirect="/clippy"
            displayCloseButton
            allowBackgroundClickClose={false}
            signupModalTitle="Sign up for Clippy"
          >
            <EFRectangleButton
              colorTheme="secondary"
              fontWeightTheme="semibold"
              shadowTheme="small"
              text="Sign up for free today"
            />
          </DynamicModal>
        </ButtonContainer>
        <BlueBlurEffect />
        <RedBlurEffect />
      </Container>
    </HeaderContainer>
  );
};

const HeaderContainer = styled.div`
  background: #000000;
  height: 650px;
  padding-top: 50px;
  position: relative;
  overflow: hidden;

  ${mediaQueries("sm")`
  padding-top: 40px;
  height: 360px;
  `};
`;

const HeaderTextContainer = styled.div`
  min-height: 140px;

  ${mediaQueries("sm")`
  min-height: 100px;
  `};
`;

const MainHeaderText = styled.div`
  font-family: Poppins;
  font-style: normal;
  font-weight: 600;
  font-size: 60px;
  line-height: 70px;
  text-align: center;
  color: #ffffff;
  text-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
  text-align: center;

  ${mediaQueries("xl")`
    font-size: 50px;
    line-height: 60px;
  `};

  ${mediaQueries("lg")`
    font-size: 35px;
    line-height: 45px;
  `};

  ${mediaQueries("sm")`
    font-size: 28px;
    line-height: 34px;
  `};
`;

const ButtonContainer = styled.div`
  position: absolute;
  z-index: 5;
  margin: 30px auto;
  display: flex;
  justify-content: center;
  width: 100%;
  left: 0;
`;

const BlueBlurEffect = styled.div`
  position: absolute;
  width: 457px;
  height: 387px;
  top: 75%;
  left: 50%;
  transform: translate(-100%, -50%);
  opacity: 0.5;

  background: #00b7ff;
  filter: blur(65px);
  z-index: 1;
`;

const RedBlurEffect = styled.div`
  position: absolute;
  width: 457px;
  height: 387px;
  top: 75%;
  left: 50%;
  transform: translate(0%, -50%);
  opacity: 0.5;

  background: #ff00f5;
  filter: blur(65px);
  z-index: 1;
`;

export default ClippyHeaderSection;
