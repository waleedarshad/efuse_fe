import React from "react";
import styled from "styled-components";
import { Container } from "react-bootstrap";

import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";
import { mediaQueries } from "../../../styles/sharedStyledComponents/breakpoints";
import DynamicModal from "../../DynamicModal/DynamicModal";

const ClippyPublicNav = () => {
  return (
    <BlackBackground>
      <Container>
        <NavContainer>
          <LogoContainer>
            <Logo src="https://cdn.efuse.gg/static/images/clippy_landing_pages/logo.svg" />
            <PoweredByText>Powered by eFuse</PoweredByText>
          </LogoContainer>
          <ButtonContainer>
            <DynamicModal
              flow="LoginSignup"
              openOnLoad={false}
              startView="login"
              displayCloseButton
              allowBackgroundClickClose={false}
            >
              <EFRectangleButton
                colorTheme="transparent"
                fontWeightTheme="semibold"
                fontColorTheme="light"
                shadowTheme="small"
                text="Login"
              />
            </DynamicModal>
            <DynamicModal
              flow="LoginSignup"
              openOnLoad={false}
              startView="signup"
              customRedirect="/clippy"
              displayCloseButton
              allowBackgroundClickClose={false}
              signupModalTitle="Sign up for Clippy"
            >
              <EFRectangleButton colorTheme="secondary" shadowTheme="small" text="Sign up" fontWeightTheme="semibold" />
            </DynamicModal>
          </ButtonContainer>
        </NavContainer>
      </Container>
    </BlackBackground>
  );
};

const BlackBackground = styled.div`
  background: black;
`;

const NavContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 15px 10px;
`;

const LogoContainer = styled.div``;

const Logo = styled.img`
  width: 155px;
  height: auto;

  ${mediaQueries("sm")`
  width: 125px;
  `};
`;

const PoweredByText = styled.div`
  font-family: Poppins;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 18px;
  color: #ffffff;
  text-align: right;

  ${mediaQueries("sm")`
  font-size: 10px;
  `};
`;

const ButtonContainer = styled.div`
  display: flex;
`;

export default ClippyPublicNav;
