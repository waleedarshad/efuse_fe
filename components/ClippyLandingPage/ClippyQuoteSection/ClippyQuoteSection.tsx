import React from "react";
import styled from "styled-components";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";
import DynamicModal from "../../DynamicModal/DynamicModal";
import { mediaQueries } from "../../../styles/sharedStyledComponents/breakpoints";

const ClippyQuoteSection = () => {
  return (
    <SectionContainer>
      <BlueBlurEffectLeft />
      <RedBlurEffectLeft />
      <RedBlurEffectRight />
      <BlueBlurEffectRight />
      <LeftImageContainer>
        <img src="https://cdn.efuse.gg/static/images/clippy_landing_pages/ogpickle3.png" alt="OGPickle" />
      </LeftImageContainer>
      <QuoteContainer>
        <LargeQuoteText>
          <span>&ldquo;</span>
          <span>Clippy provides an experience unlike anything I’ve run in to in the past.&rdquo;</span>
        </LargeQuoteText>
        <QuoteAuthor>itsOGPICKLE</QuoteAuthor>
        <ButtonContainer>
          <DynamicModal
            flow="LoginSignup"
            openOnLoad={false}
            startView="signup"
            customRedirect="/clippy"
            displayCloseButton
            allowBackgroundClickClose={false}
            signupModalTitle="Sign up for Clippy"
          >
            <EFRectangleButton
              colorTheme="secondary"
              shadowTheme="small"
              text="Try Clippy for Yourself"
              fontWeightTheme="semibold"
            />
          </DynamicModal>
        </ButtonContainer>
      </QuoteContainer>
    </SectionContainer>
  );
};

const SectionContainer = styled.div`
  background: #000000;
  height: 720px;
  position: relative;
  overflow: hidden;

  ${mediaQueries("md")`
  height: 612px;
  `};
`;

const LeftImageContainer = styled.div`
  height: 100%;
  position: absolute;
  z-index: 1;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  overflow: hidden;

  & > img {
    height: 100%;
    width: auto;
  }

  ${mediaQueries("md")`
  & > img {
    width: 130%;
    max-width: 400px;
    height: auto;
  }
  `};
`;

const QuoteContainer = styled.div`
  position: absolute;
  margin-left: 50%;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  z-index: 2;
  padding: 30px 20px;

  ${mediaQueries("md")`
  margin-left: 0;
  justify-content: flex-start;
  `};
`;

const LargeQuoteText = styled.div`
  font-family: Poppins;
  font-style: normal;
  font-weight: 600;
  font-size: 50px;
  line-height: 56px;
  text-shadow: 0px 2px 8px rgba(0, 0, 0, 0.5);

  display: flex;
  color: #ffffff;
  max-width: 520px;
  margin-bottom: 14px;

  ${mediaQueries("lg")`
  font-size: 40px;
  line-height: 46px;
  `};

  ${mediaQueries("md")`
  font-size: 30px;
  line-height: 36px;
  `};
`;

const QuoteAuthor = styled.div`
  font-family: Poppins;
  font-style: normal;
  font-weight: 600;
  font-size: 18px;
  line-height: 24px;
  color: #ffffff;
  margin-bottom: 40px;
  margin-left: 24px;

  ${mediaQueries("md")`
  margin-left: 14px;
  `};
`;

const ButtonContainer = styled.div`
  margin-left: 24px;

  ${mediaQueries("md")`
  margin-left: 14px;
  `};
`;

const BlueBlurEffectLeft = styled.div`
  position: absolute;
  width: 200px;
  height: 200px;
  top: 65%;
  left: 25%;
  transform: translate(-130%, -50%);

  background: #00b7ff;
  filter: blur(60px);
  z-index: 1;
`;

const RedBlurEffectLeft = styled.div`
  position: absolute;
  width: 200px;
  height: 200px;
  top: 35%;
  left: 20%;
  transform: translate(-130%, -50%);

  background: #ff00f5;
  filter: blur(90px);
  z-index: 1;
`;

const BlueBlurEffectRight = styled.div`
  position: absolute;
  width: 220px;
  height: 220px;
  top: 55%;
  right: -10%;

  background: #00b7ff;
  filter: blur(60px);
  z-index: 1;
`;

const RedBlurEffectRight = styled.div`
  position: absolute;
  width: 200px;
  height: 200px;
  top: 15%;
  right: -5%;

  background: #ff00f5;
  filter: blur(90px);
  z-index: 1;
`;

export default ClippyQuoteSection;
