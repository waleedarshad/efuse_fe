import styled from "styled-components";
import React from "react";
import EFFooter from "../../EFFooter/EFFooter";

const ClippyFooter = () => {
  return <Footer isClippy />;
};

const Footer = styled(EFFooter)`
  padding: 0;
  margin: 0 30px;
`;
export default ClippyFooter;
