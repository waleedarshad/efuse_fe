import styled from "styled-components";
import React from "react";
import ClippyLogo from "./ClippyLogo";
import EFHeaderMenu from "./EFHeaderMenu";

const ClippyNavbar = () => {
  return (
    <Nav>
      <ClippyLogo />
      <EFHeaderMenu />
    </Nav>
  );
};

const Nav = styled.nav`
  width: 100%;
  height: 100%;
  padding: 0 30px;
  border-bottom: 1px solid #c7d3ea;
  background: white;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export default ClippyNavbar;
