import styled from "styled-components";
import EFInputControl from "../../FormControls/EFInputControl";

const CommandInputControl = styled(EFInputControl)`
  font-family: Poppins, sans-serif;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 20px;
`;

export default CommandInputControl;
