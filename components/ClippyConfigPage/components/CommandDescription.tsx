import styled from "styled-components";

const CommandDescription = styled.p`
  font-family: Open Sans, sans-serif;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 20px;
  color: ${props => (props.isDisabled ? "rgba(#0a0c12, 0.5)" : "#0a0c12")};
  margin-bottom: 20px;
`;

export default CommandDescription;
