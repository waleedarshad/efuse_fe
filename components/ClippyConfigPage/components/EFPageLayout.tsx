import styled from "styled-components";
import React from "react";

const EFPageLayout = ({ header, sidebar, footer, children }) => {
  return (
    <Page>
      <div>
        <Header>{header}</Header>
        <Main>
          <AsideLeft>{sidebar}</AsideLeft>
          <ContentWrapper fullWidth={!sidebar}>
            <Content>{children}</Content>
          </ContentWrapper>
        </Main>
      </div>
      <Footer>{footer}</Footer>
    </Page>
  );
};

const Page = styled.div`
  min-height: 100vh;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

const Header = styled.header`
  position: sticky;
  top: 0;
  z-index: 1;
  height: 80px;
  width: 100%;
  margin-bottom: 20px;
`;

const Main = styled.main`
  display: flex;
  flex-direction: row;
  flex-grow: 1;
`;

const AsideLeft = styled.aside`
  position: fixed;
  padding-bottom: 200px;
  top: calc(80px + 20px);
  bottom: 0;
  width: 240px;
  overflow: hidden auto;
  flex-grow: 0;
  flex-shrink: 0;
`;

const ContentWrapper = styled.section`
  margin-left: ${({ fullWidth }) => (fullWidth ? "0px" : "240px")};
  width: 100%;
  display: flex;
  justify-content: center;
`;

const Content = styled.section`
  margin: 0 30px;
  height: 100%;
  flex-grow: 1;
  max-width: 1140px;
`;

const Footer = styled.footer`
  height: 200px;
  z-index: 2;
  background: white;
`;

export default EFPageLayout;
