import React from "react";
import styled from "styled-components";
import { faChevronDown, faPowerOff } from "@fortawesome/pro-regular-svg-icons";
import { useDispatch, useSelector } from "react-redux";
import { useQuery } from "@apollo/client";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import EFButton from "../../Buttons/EFButton/EFButton";
import EFAvatar from "../../EFAvatar/EFAvatar";
import { GET_USER_BY_ID } from "../../../graphql/UserQuery";
import { TruncateWithEllipsis } from "../../../styles/sharedStyledComponents/sharedStyles";
import EFNavigationDropdownMenu, { MenuItem } from "./EFNavigationDropdownMenu";
import { logoutUser } from "../../../store/actions/authActions";

const EFHeaderMenu = () => {
  const dispatch = useDispatch();

  const authUser = useSelector(state => state.auth.currentUser);
  const { data, loading } = useQuery(GET_USER_BY_ID, { variables: { id: authUser?._id } });

  if (loading) {
    return <></>;
  }

  const renderTrigger = onClick => {
    return (
      <Trigger shadowTheme="none" colorTheme="light" onClick={onClick}>
        <UserInfo>
          <EFAvatar
            size="extraSmall"
            borderTheme="none"
            online
            profilePicture={data?.getUserById?.profilePicture.url}
          />
          <UsernameContainer>
            <TopUsername>{data?.getUserById?.username}</TopUsername>
            <BottomUsername>@{data?.getUserById?.username}</BottomUsername>
          </UsernameContainer>
        </UserInfo>
        <Icon icon={faChevronDown} />
      </Trigger>
    );
  };

  const logout = () => {
    analytics.track("NAVIGATION_BUTTON_CLICKED", { type: "logout" });
    dispatch(logoutUser());
  };

  const menuItems: MenuItem[] = [
    {
      icon: faPowerOff,
      name: "Log out",
      onClick: logout
    } as MenuItem
  ];

  return <EFNavigationDropdownMenu renderTrigger={renderTrigger} openDirection="left" menuItems={menuItems} />;
};

const Icon = styled(FontAwesomeIcon)`
  font-weight: normal;
  font-size: 12px;
  line-height: 12px;
  color: rgba(18, 21, 29, 0.6);
`;

const Trigger = styled(EFButton)`
  background: #ffffff;
  border: 1px solid #ced7e7;
  border-radius: 6px;
  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 10px;
  height: 50px;
`;

const UserInfo = styled.div`
  display: flex;
  align-items: center;
`;

const TopUsername = styled.div`
  font-family: Poppins, sans-serif;
  font-style: normal;
  font-weight: 500;
  font-size: 12px;
  line-height: 15px;
  max-width: 63px;
  ${TruncateWithEllipsis};
  color: #12151d;
`;

const BottomUsername = styled.div`
  font-family: Open Sans, sans-serif;
  font-style: normal;
  font-weight: normal;
  font-size: 8px;
  line-height: 11px;
  max-width: 63px;
  ${TruncateWithEllipsis};
  color: rgba(18, 21, 29, 0.6);
`;

const UsernameContainer = styled.div`
  margin-left: 10px;
  margin-right: 20px;
  text-align: left;
`;

export default EFHeaderMenu;
