import EFPageLayout from "./EFPageLayout";
import ClippyNavbar from "./ClippyNavbar";
import ClippyFooter from "./ClippyFooter";
import ClippySidebar from "./navigation/ClippySidebar";
import useWindowDimensions from "../../../hooks/useWindowDimensions";
import { breakpoints } from "../../../styles/sharedStyledComponents/breakpoints";

const ClippyLayout = ({ children }) => {
  const { width } = useWindowDimensions();

  const isWiderThanMediumBreakpoint = width > breakpoints.md;

  return (
    <EFPageLayout
      header={<ClippyNavbar />}
      sidebar={isWiderThanMediumBreakpoint ? <ClippySidebar /> : undefined}
      footer={<ClippyFooter />}
    >
      {children}
    </EFPageLayout>
  );
};

export default ClippyLayout;
