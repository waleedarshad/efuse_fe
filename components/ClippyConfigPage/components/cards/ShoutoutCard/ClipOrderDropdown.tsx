import React, { useEffect, useState } from "react";
import EFSelectDropdown from "../../../../Dropdowns/EFSelectDropdown/EFSelectDropdown";
import { ClippySortOrderEnum } from "../../../../../enums";
import DropdownItem from "./DropdownItem";

const OPTIONS = [
  {
    label: "Top",
    value: ClippySortOrderEnum.TOP
  },
  {
    label: "Random",
    value: ClippySortOrderEnum.RANDOM
  }
];

const ClipOrderDropdown = ({ defaultClipOrder, onOrderSelected }) => {
  const [selectedClipOrder, setSelectedClipOrder] = useState(defaultClipOrder);

  useEffect(() => {
    onOrderSelected(selectedClipOrder);
  }, [selectedClipOrder]);

  const styledOptions = OPTIONS.map(option => ({
    ...option,
    label: (
      <DropdownItem selected={option.value === selectedClipOrder}>{`${
        option.value === selectedClipOrder ? "Clip Order: " : ""
      } ${option.label}`}</DropdownItem>
    )
  }));

  return <EFSelectDropdown defaultValue={selectedClipOrder} onSelect={setSelectedClipOrder} options={styledOptions} />;
};

export default ClipOrderDropdown;
