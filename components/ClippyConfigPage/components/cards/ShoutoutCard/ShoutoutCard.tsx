import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import styled from "styled-components";
import { useMutation } from "@apollo/client";
import SettingsCardLayout from "../../../../Settings/SettingsCardLayout/SettingsCardLayout";
import EFRectangleButton from "../../../../Buttons/EFRectangleButton/EFRectangleButton";
import { ClippyCommandKindEnum, ClippySortOrderEnum } from "../../../../../enums";
import DateRangeDropdown from "./DateRangeDropdown";
import ClipOrderDropdown from "./ClipOrderDropdown";
import { GET_CLIPPY, UPDATE_CLIPPY_COMMAND } from "../../../queries";
import { sendNotification } from "../../../../../helpers/FlashHelper";
import DescriptionCommandAndRoles, { rolesWithLabels } from "../../DescriptionCommandAndRoles";
import { ClippyCommand } from "../../../types";
import EFCheckboxControl from "../../../../FormControls/EFCheckboxControl";
import CommandDescription from "../../CommandDescription";
import CommandInputControl from "../../CommandInputControl";

const ShoutoutCard = ({ commandConfig }: { commandConfig: ClippyCommand }) => {
  const [dateRange, setDateRange] = useState();
  const [clipOrder, setClipOrder] = useState();

  const [updateClippyCommand] = useMutation(UPDATE_CLIPPY_COMMAND, {
    onCompleted: () => sendNotification("Shoutout configuration updated ", "success", "Clippy"),
    onError: () => sendNotification("There was a problem changing shoutout configuration ", "danger", "Clippy"),
    refetchQueries: [{ query: GET_CLIPPY }]
  });

  const form = useForm({ mode: "onChange" });

  const { formState, watch, handleSubmit, clearErrors, setValue } = form;

  useEffect(() => {
    if (clipOrder !== ClippySortOrderEnum.RANDOM) {
      setValue("minimumViews", commandConfig.minimumViews);
      clearErrors("minimumViews");
    }
  }, [clipOrder]);

  if (!commandConfig) {
    return <></>;
  }

  const haveFormValuesChanged = currentFormValues => {
    if (currentFormValues[ClippyCommandKindEnum.SHOUTOUT] !== commandConfig.command) {
      return true;
    }

    if (
      rolesWithLabels.some(role =>
        currentFormValues[role.name]
          ? !commandConfig.allowedRoles.includes(role.name)
          : commandConfig.allowedRoles.includes(role.name)
      )
    ) {
      return true;
    }

    if (commandConfig.queueClips !== currentFormValues.queueClips) {
      return true;
    }

    if (commandConfig.minimumViews !== currentFormValues.minimumViews) {
      return true;
    }

    return dateRange !== commandConfig.dateRange || clipOrder !== commandConfig.sortOrder;
  };

  const isFormReady = formState.isValid && haveFormValuesChanged(watch());

  const onSubmit = async formData => {
    const roleValues = rolesWithLabels.filter(role => !!formData[role.name]).map(selectedRole => selectedRole.name);

    const updatedConfig = {
      _id: commandConfig._id,
      command: formData[ClippyCommandKindEnum.SHOUTOUT],
      sortOrder: clipOrder,
      dateRange,
      allowedRoles: roleValues,
      minimumViews: formData.minimumViews,
      queueClips: formData.queueClips
    };

    await updateClippyCommand({ variables: { clippyCommandParams: updatedConfig } });
  };

  return (
    <SettingsCardLayout title="Stream Command Shoutout">
      <DescriptionCommandAndRoles
        description="Utilizing this Clippy chat command will allow you to shoutout a fellow streamer on your platform & show your audience a clip from their stream along with their username."
        defaultCommandValue={commandConfig.command}
        form={form}
        commandKind={ClippyCommandKindEnum.SHOUTOUT}
        defaultRoleValues={commandConfig.allowedRoles}
      />
      <DropdownContainer>
        <DateRangeDropdown defaultSelectedDateRange={commandConfig.dateRange} onDateSelected={setDateRange} />
        <ClipOrderDropdown defaultClipOrder={commandConfig.sortOrder} onOrderSelected={setClipOrder} />
      </DropdownContainer>

      {clipOrder === ClippySortOrderEnum.RANDOM && (
        <>
          <CommandDescription isDisabled={clipOrder && clipOrder !== ClippySortOrderEnum.RANDOM}>
            Set the minimum views required when selecting a random clip
          </CommandDescription>
          <MinimumViewInputContainer>
            <MinimumViewsInput
              control={form.control}
              errors={formState.errors}
              type="number"
              name="minimumViews"
              required={clipOrder === ClippySortOrderEnum.RANDOM}
              valueAsNumber
              disabled={clipOrder && clipOrder !== ClippySortOrderEnum.RANDOM}
              placeholder="Minimum Views"
              minValue={0}
              defaultValue={commandConfig.minimumViews}
            />
          </MinimumViewInputContainer>
        </>
      )}

      <CommandDescription>
        Clippy ignores extra play commands when a clip is already playing. Enable this to queue those extra clips
        instead!
      </CommandDescription>
      <EFCheckboxControl
        name="queueClips"
        control={form.control}
        label="Queue Clips"
        defaultValue={commandConfig.queueClips}
      />
      <SaveButtonContainer>
        <EFRectangleButton
          text="Save"
          buttonType="submit"
          colorTheme="light"
          shadowTheme="none"
          disabled={!isFormReady}
          onClick={handleSubmit(onSubmit)}
        />
      </SaveButtonContainer>
    </SettingsCardLayout>
  );
};

const DropdownContainer = styled.div`
  margin-bottom: 30px;

  > :first-child {
    margin-bottom: 10px;
  }
`;

const SaveButtonContainer = styled.div`
  display: flex;
  justify-content: right;
`;

const MinimumViewInputContainer = styled.div`
  margin-bottom: 30px;
`;

const MinimumViewsInput = styled(CommandInputControl)`
  &[type="number"]::-webkit-inner-spin-button {
    display: none;
  }
`;

export default ShoutoutCard;
