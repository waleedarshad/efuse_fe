import styled from "styled-components";

const DropdownItem = styled.p`
  margin: 0;
  padding: 0 0 0 20px;
  display: flex;
  align-items: center;
  height: 40px;
  font-family: Poppins, sans-serif;
  font-style: normal;
  font-weight: ${props => (props.selected ? "600" : "500")};
  font-size: 14px;
  color: #0a0c12;
`;

export default DropdownItem;
