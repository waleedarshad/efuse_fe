import React, { useEffect, useState } from "react";
import EFSelectDropdown from "../../../../Dropdowns/EFSelectDropdown/EFSelectDropdown";
import { ClippyDateRangeEnum } from "../../../../../enums";
import DropdownItem from "./DropdownItem";

const OPTIONS = [
  {
    label: "Last 24hr",
    value: ClippyDateRangeEnum.DAY
  },
  {
    label: "Last 7 days",
    value: ClippyDateRangeEnum.WEEK
  },
  {
    label: "Last Month",
    value: ClippyDateRangeEnum.MONTH
  },
  {
    label: "Last Year",
    value: ClippyDateRangeEnum.YEAR
  },
  {
    label: "Any Date",
    value: ClippyDateRangeEnum.ALL
  }
];

const DateRangeDropdown = ({ defaultSelectedDateRange, onDateSelected }) => {
  const [selectedDateRange, setSelectedDateRange] = useState(defaultSelectedDateRange);

  useEffect(() => {
    onDateSelected(selectedDateRange);
  }, [selectedDateRange]);

  const styledOptions = OPTIONS.map(option => ({
    ...option,
    label: (
      <DropdownItem selected={option.value === selectedDateRange}>{`${
        option.value === selectedDateRange ? "Date Range: " : ""
      } ${option.label}`}</DropdownItem>
    )
  }));

  return (
    <EFSelectDropdown defaultValue={defaultSelectedDateRange} onSelect={setSelectedDateRange} options={styledOptions} />
  );
};

export default DateRangeDropdown;
