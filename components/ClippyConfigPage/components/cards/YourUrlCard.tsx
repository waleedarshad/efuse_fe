import React from "react";
import getConfig from "next/config";
import { faRedo } from "@fortawesome/pro-light-svg-icons";
import styled from "styled-components";
import { useMutation } from "@apollo/client";
import SettingsCardLayout from "../../../Settings/SettingsCardLayout/SettingsCardLayout";
import EFCopyURL from "../../../EFCopyURL/EFCopyUrl";
import EFCircleIconButton from "../../../Buttons/EFCircleIconButton/EFCircleIconButton";
import { GET_CLIPPY, REGENERATE_CLIPPY_TOKEN } from "../../queries";
import { sendNotification } from "../../../../helpers/FlashHelper";
import Paragraph from "../CommandDescription";

const { publicRuntimeConfig } = getConfig();
const { feBaseUrl } = publicRuntimeConfig;

const CopyURLWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;

  > :nth-child(1) {
    padding-right: 10px;
  }
`;

const ButtonContainer = styled.div`
  margin-top: 7px;

  svg {
    color: #747d8b;
  }
`;

const Description = styled(Paragraph)`
  margin-bottom: 12px;
`;

const YourUrlCard = ({ userToken }: { userToken: string }) => {
  const [regenerateToken] = useMutation(REGENERATE_CLIPPY_TOKEN, {
    onCompleted: () => sendNotification("Your URL was updated ", "success", "Clippy"),
    onError: () => sendNotification("There was a problem updating your URL ", "danger", "Clippy"),
    refetchQueries: [{ query: GET_CLIPPY }]
  });

  return (
    <SettingsCardLayout title="Your URL">
      <Description>
        To use the shoutout source, copy the URL and create a new browser source in OBS or your chosen streaming
        software. You don&apos;t need to change the size settings. Use the settings to configure your options.
      </Description>
      <CopyURLWrapper>
        <EFCopyURL url={`${feBaseUrl}/overlay/${userToken}`} showPreview />
        <ButtonContainer>
          {/** @ts-ignore * */}
          <EFCircleIconButton
            icon={faRedo}
            shadowTheme="none"
            colorTheme="transparent"
            text="Reset"
            onClick={regenerateToken}
          />
        </ButtonContainer>
      </CopyURLWrapper>
    </SettingsCardLayout>
  );
};

export default YourUrlCard;
