import React, { forwardRef, useEffect, useImperativeHandle, useRef } from "react";
import { useForm } from "react-hook-form";
import { useMutation } from "@apollo/client";
import DescriptionCommandAndRoles, { rolesWithLabels } from "../../DescriptionCommandAndRoles";
import { UPDATE_CLIPPY_COMMAND } from "../../../queries";

const CommandSection = ({ commandConfig, description, onCommandValidationChange, onValueChange }, ref) => {
  if (!commandConfig) {
    return <></>;
  }

  const [updateClippyCommand] = useMutation(UPDATE_CLIPPY_COMMAND);

  const isCommandSectionDirty = useRef();

  const form = useForm({ mode: "onChange" });

  useImperativeHandle(ref, () => ({
    submitForm: form.handleSubmit(formData => {
      if (isCommandSectionDirty.current) {
        const roleValues = rolesWithLabels.filter(role => !!formData[role.name]).map(selectedRole => selectedRole.name);

        const updatedConfig = {
          _id: commandConfig._id,
          command: formData[commandConfig.kind],
          allowedRoles: roleValues
        };
        return updateClippyCommand({ variables: { clippyCommandParams: updatedConfig } });
      }
      return Promise.resolve();
    })
  }));

  const { isValid } = form.formState;

  useEffect(() => {
    onCommandValidationChange(isValid, commandConfig.kind);
  }, [isValid]);

  const onValueChangeHandler = hasValueChanged => {
    isCommandSectionDirty.current = hasValueChanged;
    onValueChange(hasValueChanged, commandConfig.kind);
  };

  return (
    <DescriptionCommandAndRoles
      onValueChange={onValueChangeHandler}
      description={description}
      form={form}
      commandKind={commandConfig.kind}
      defaultCommandValue={commandConfig.command}
      defaultRoleValues={commandConfig.allowedRoles}
    />
  );
};

export default forwardRef(CommandSection);
