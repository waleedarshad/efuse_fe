import React, { useRef, useState } from "react";
import { useLazyQuery } from "@apollo/client";
import styled from "styled-components";
import SettingsCardLayout from "../../../../Settings/SettingsCardLayout/SettingsCardLayout";
import { ClippyCommandKindEnum } from "../../../../../enums";
import EFRectangleButton from "../../../../Buttons/EFRectangleButton/EFRectangleButton";
import CommandSection from "./CommandSection";
import { sendNotification } from "../../../../../helpers/FlashHelper";
import { GET_CLIPPY } from "../../../queries";

const ButtonContainer = styled.div`
  display: flex;
  justify-content: right;
`;

const COMMANDS = [
  ClippyCommandKindEnum.STOP,
  ClippyCommandKindEnum.STOP_ALL,
  ClippyCommandKindEnum.MUTE,
  ClippyCommandKindEnum.UNMUTE,
  ClippyCommandKindEnum.WATCH,
  ClippyCommandKindEnum.REPLAY,
  ClippyCommandKindEnum.RESET
];

const AdditionalCommands = ({ allCommands }) => {
  const [updateCacheWithLatest] = useLazyQuery(GET_CLIPPY, { fetchPolicy: "no-cache" });

  // eslint-disable-next-line react-hooks/rules-of-hooks
  const refs = COMMANDS.reduce((prev, curr) => ({ ...prev, [curr]: useRef(null) }), {});

  const [commandValidation, setCommandValidation] = useState(() =>
    COMMANDS.reduce(
      (prev, curr) => ({
        ...prev,
        [curr]: false
      }),
      {}
    )
  );

  const [isFormDirty, setIsFormDirty] = useState(() =>
    COMMANDS.reduce(
      (prev, curr) => ({
        ...prev,
        [curr]: false
      }),
      {}
    )
  );

  const onSaveClick = () => {
    Promise.all(Object.values(refs).map(ref => ref.current.submitForm()))
      .then(() => {
        updateCacheWithLatest();
        sendNotification("Configuration updated ", "success", "Clippy");
      })
      .catch(() => sendNotification("There was a problem updating the configuration ", "danger", "Clippy"));
  };

  const formIsValid = Object.values(commandValidation).every(sectionStatus => sectionStatus);
  const formIsReady = Object.values(isFormDirty).some(isDirty => isDirty);

  const onCommandValidationChange = (isValid, commandKind) => {
    setCommandValidation({ ...commandValidation, [commandKind]: isValid });
  };

  const onValueChange = (hasValueChanged, commandKind) => {
    setIsFormDirty({ ...isFormDirty, [commandKind]: hasValueChanged });
  };

  return (
    <SettingsCardLayout title="Additional Stream Commands">
      <CommandSection
        onValueChange={onValueChange}
        onCommandValidationChange={onCommandValidationChange}
        commandConfig={allCommands[ClippyCommandKindEnum.STOP]}
        description="This command will immediately stop playing the clip that is playing."
        ref={refs[ClippyCommandKindEnum.STOP]}
      />
      <CommandSection
        onValueChange={onValueChange}
        onCommandValidationChange={onCommandValidationChange}
        commandConfig={allCommands[ClippyCommandKindEnum.STOP_ALL]}
        description="Ends current clip and clears clip queue"
        ref={refs[ClippyCommandKindEnum.STOP_ALL]}
      />
      <CommandSection
        onValueChange={onValueChange}
        onCommandValidationChange={onCommandValidationChange}
        commandConfig={allCommands[ClippyCommandKindEnum.MUTE]}
        description="This command will mute the audio of the playing clip or clips."
        ref={refs[ClippyCommandKindEnum.MUTE]}
      />
      <CommandSection
        onValueChange={onValueChange}
        onCommandValidationChange={onCommandValidationChange}
        commandConfig={allCommands[ClippyCommandKindEnum.UNMUTE]}
        description="This command will unmute the audio of the playing clip or clips."
        ref={refs[ClippyCommandKindEnum.UNMUTE]}
      />
      <CommandSection
        onValueChange={onValueChange}
        onCommandValidationChange={onCommandValidationChange}
        commandConfig={allCommands[ClippyCommandKindEnum.WATCH]}
        description="If you use this command it will play the last posted clip in chat. If you add a link with this command, it will play that specific clip."
        ref={refs[ClippyCommandKindEnum.WATCH]}
      />
      <CommandSection
        onValueChange={onValueChange}
        onCommandValidationChange={onCommandValidationChange}
        commandConfig={allCommands[ClippyCommandKindEnum.REPLAY]}
        description="This command will replay the last played clip"
        ref={refs[ClippyCommandKindEnum.REPLAY]}
      />
      <CommandSection
        onValueChange={onValueChange}
        onCommandValidationChange={onCommandValidationChange}
        commandConfig={allCommands[ClippyCommandKindEnum.RESET]}
        description="This command will refresh the clippy URL"
        ref={refs[ClippyCommandKindEnum.RESET]}
      />
      <ButtonContainer>
        <EFRectangleButton
          text="Save"
          buttonType="submit"
          colorTheme="light"
          shadowTheme="none"
          disabled={!formIsReady || !formIsValid}
          onClick={onSaveClick}
        />
      </ButtonContainer>
    </SettingsCardLayout>
  );
};
export default AdditionalCommands;
