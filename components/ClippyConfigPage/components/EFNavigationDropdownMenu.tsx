import React, { useState } from "react";
import styled from "styled-components";
import { FontAwesomeIconProps } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/pro-solid-svg-icons";
import { mediaQueries } from "../../../styles/sharedStyledComponents/breakpoints";
import EFCircleIconButton from "../../Buttons/EFCircleIconButton/EFCircleIconButton";
import EFPillButton from "../../Buttons/EFPillButton/EFPillButton";

export type MenuItem = {
  icon: FontAwesomeIconProps;
  name: string;
  onClick: () => any;
};

interface EFNavigationDropdownMenuProps {
  renderTrigger: (onClick: () => any) => React.ReactNode;
  openDirection: "right" | "left";
  menuItems: MenuItem[];
}

const EFNavigationDropdownMenu = ({ renderTrigger, openDirection, menuItems }: EFNavigationDropdownMenuProps) => {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <div style={{ position: "relative" }}>
      {renderTrigger(() => setIsOpen(!isOpen))}

      {isOpen && (
        <DropdownContainer openDirection={openDirection}>
          <div style={{ width: "100%", textAlign: "right" }}>
            <EFCircleIconButton icon={faTimes} colorTheme="dark" size="small" onClick={() => setIsOpen(false)} />
          </div>
          {menuItems.map(menuItem => {
            return (
              <EFPillButton
                key={menuItem.name}
                width="fullWidth"
                colorTheme="dark"
                icon={menuItem.icon}
                text={menuItem.name}
                size="medium"
                fontWeightTheme="normal"
                onClick={() => {
                  menuItem.onClick();
                  setIsOpen(false);
                }}
              />
            );
          })}
        </DropdownContainer>
      )}
    </div>
  );
};

const DropdownContainer = styled.div`
  position: absolute;
  ${({ openDirection }) => `${openDirection === "right" ? "left" : "right"}: 0`};
  margin-top: 5px;
  background: #121212;
  border: 1px solid #2e2e2e;
  box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.15);
  border-radius: 8px;
  width: 318px;
  height: fit-content;
  padding: 10px;
  z-index: 999999;
`;

export default EFNavigationDropdownMenu;
