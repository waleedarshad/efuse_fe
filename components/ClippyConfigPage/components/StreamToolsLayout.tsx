import React from "react";
import styled from "styled-components";

interface ClippyPageLayoutProps {
  top: React.ReactNode;
  leftColumn: React.ReactNode;
  rightColumn: React.ReactNode;
}

const StreamToolsLayout = ({ top, leftColumn, rightColumn }: ClippyPageLayoutProps) => {
  return (
    <>
      <TopContainer>{top}</TopContainer>
      <ColumnsContainer>
        <Column column={1}>{leftColumn}</Column>
        <Column column={2}>{rightColumn}</Column>
      </ColumnsContainer>
    </>
  );
};

const TopContainer = styled.div`
  margin-bottom: 10px;
`;

const ColumnsContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(2, minmax(0, 1fr));
  gap: 10px;

  @media only screen and (max-width: 991px) {
    display: block;
  }
`;

const Column = styled.div`
  grid-column: ${props => props.column};

  @media only screen and (max-width: 991px) {
    margin-bottom: 10px;
  }
`;

export default StreamToolsLayout;
