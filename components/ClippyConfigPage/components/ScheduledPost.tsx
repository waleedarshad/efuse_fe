import React from "react";
import styled from "styled-components";
import moment from "moment";
import { faTrash } from "@fortawesome/pro-regular-svg-icons";
import { useDispatch } from "react-redux";
import EFCard from "../../Cards/EFCard/EFCard";
import EFCircleIconButton from "../../Buttons/EFCircleIconButton/EFCircleIconButton";
import { deleteScheduledPost } from "../../../store/actions/userActions";
import ConfirmAlert from "../../ConfirmAlert/ConfirmAlert";

const ScheduledPost = ({ post }) => {
  const dispatch = useDispatch();

  return (
    <OutsideContainer>
      <EFCard widthTheme="fullWidth" shadow="none">
        <InsideContainer>
          {post.media && <PostMedia src={post.media.file.url} />}
          <TimeContentContainer>
            <PostScheduledTime>{moment(post.scheduledAt).format("L LT")}</PostScheduledTime>
            <PostContent>{post.content}</PostContent>
          </TimeContentContainer>
          <ButtonSocialsContainer>
            <ButtonContainer>
              <ConfirmAlert
                title="Confirm Deletion"
                message="Are you sure you want to delete this scheduled post?"
                onYes={() => {
                  analytics.track("CREATE_POST_SCHEDULED_POSTS_REMOVE");
                  dispatch(deleteScheduledPost(post._id));
                }}
              >
                <EFCircleIconButton
                  onClick={() => {}}
                  shadowTheme="none"
                  buttonType="button"
                  icon={faTrash}
                  colorTheme="transparent"
                />
              </ConfirmAlert>
            </ButtonContainer>
            <CrossPostingIconContainer>
              {post.crossPosts.includes("twitter") && (
                <TwitterIcon
                  src="https://cdn.efuse.gg/static/images/clippy_landing_pages/twitter.png"
                  alt="Twitter logo icon"
                />
              )}
            </CrossPostingIconContainer>
          </ButtonSocialsContainer>
        </InsideContainer>
      </EFCard>
    </OutsideContainer>
  );
};
const OutsideContainer = styled.div`
  margin-bottom: 10px;
`;

const InsideContainer = styled.div`
  padding: 20px;
  display: flex;
  min-height: 148px;
`;

const PostMedia = styled.div`
  width: 190px;
  height: 108px;
  background: url(${props => props.src}) no-repeat center center;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
  border-radius: 8px;
  margin-right: 20px;
`;

const TimeContentContainer = styled.div`
  flex: 1;
`;

const PostScheduledTime = styled.div`
  margin-bottom: 8px;
  font-family: Poppins;
  font-style: normal;
  font-weight: 500;
  font-size: 12px;
  line-height: 20px;
`;

const PostContent = styled.div`
  display: -webkit-box;
  overflow: hidden;
  -webkit-line-clamp: 4;
  -webkit-box-orient: vertical;
  font-family: Poppins;
  font-style: normal;
  font-weight: 500;
  font-size: 12px;
  line-height: 20px;
`;

const ButtonSocialsContainer = styled.div`
  width: 200px;
  margin-left: 20px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

const TwitterIcon = styled.img`
  width: 18px;
  height: auto;
`;

const ButtonContainer = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;

  & > * {
    margin-left: 15px;
  }
`;

const CrossPostingIconContainer = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;

  & > img {
    margin-left: 15px;
  }
`;

export default ScheduledPost;
