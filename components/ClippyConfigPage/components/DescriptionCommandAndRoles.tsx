import React, { useEffect } from "react";
import styled from "styled-components";
import CommandDescription from "./CommandDescription";
import { ClippyAllowedRolesEnum } from "../../../enums";
import EFCheckboxControl from "../../FormControls/EFCheckboxControl";
import CommandInputControl from "./CommandInputControl";

const AllowedRoles = styled.div`
  margin: 20px 0 40px 0;
  padding: 0 10px;
  display: flex;
  flex-wrap: wrap;
  row-gap: 10px;

  > * {
    padding-right: 20px;
  }

  > :last-child {
    padding-right: 0;
  }
`;

export const rolesWithLabels = [
  { name: ClippyAllowedRolesEnum.MODERATOR, label: "Mods" },
  { name: ClippyAllowedRolesEnum.SUBSCRIBER, label: "Subs" },
  { name: ClippyAllowedRolesEnum.VIP, label: "VIPs" },
  { name: ClippyAllowedRolesEnum.EVERYONE, label: "Everyone" }
];

const DescriptionCommandAndRoles = ({
  description,
  defaultCommandValue,
  form,
  commandKind,
  defaultRoleValues,
  onValueChange = () => {}
}) => {
  const { control, formState, watch } = form;

  const formValues = watch();

  const hasAFormValueChanged =
    formValues[commandKind] !== defaultCommandValue ||
    rolesWithLabels.some(role =>
      formValues[role.name] ? !defaultRoleValues.includes(role.name) : defaultRoleValues.includes(role.name)
    );

  useEffect(() => {
    onValueChange(hasAFormValueChanged);
  }, [hasAFormValueChanged]);

  return (
    <>
      <CommandDescription>{description}</CommandDescription>
      <CommandInputControl
        control={control}
        errors={formState.errors}
        name={commandKind}
        required
        maxLength={40}
        validate={userInput =>
          new RegExp(/^([a-zA-Z0-9]+)$/g).test(userInput) || "Commands can only contain letters and/or numbers"
        }
        prepend="!"
        defaultValue={defaultCommandValue}
      />
      <AllowedRoles>
        {rolesWithLabels.map(role => (
          <EFCheckboxControl
            key={role.label}
            control={control}
            name={role.name}
            label={role.label}
            defaultValue={defaultRoleValues.includes(role.name)}
          />
        ))}
      </AllowedRoles>
    </>
  );
};
export default DescriptionCommandAndRoles;
