import styled from "styled-components";
import React from "react";
import EFSidebarContainer from "../../../Navigation/SidebarNav/EFSidebarContainer/EFSidebarContainer";
import { NavSection } from "../../../Navigation/SidebarNav/EFSidebarNavSection/NavSection";
import clippyNavigationLinks from "./clippyNavigationLinks";

const ClippySidebar = () => {
  const navSections: NavSection[] = [
    {
      navItems: clippyNavigationLinks
    }
  ];

  return (
    <SidebarWrapper>
      <EFSidebarContainer navSections={navSections} />
    </SidebarWrapper>
  );
};

const SidebarWrapper = styled.div`
  .ef-sidebar-nav-item__nav-item-row {
    padding: 0 30px;

    &:first-child {
      margin-top: 0;
    }
  }
`;

export default ClippySidebar;
