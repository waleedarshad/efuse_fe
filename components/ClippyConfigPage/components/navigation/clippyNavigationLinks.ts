import { faCalendarAlt, faVideo } from "@fortawesome/pro-regular-svg-icons";

const clippyNavigationLinks = [
  {
    icon: faCalendarAlt,
    name: "Scheduled Posts",
    href: "/clippy/scheduled"
  },
  {
    icon: faVideo,
    name: "Stream Tools",
    href: "/clippy"
  }
];

export default clippyNavigationLinks;
