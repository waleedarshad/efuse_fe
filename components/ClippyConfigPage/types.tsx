import { ClippyCommandKindEnum } from "../../enums";

export interface ClippyCommand {
  _id: string;
  command: string;
  kind: ClippyCommandKindEnum;
  sortOrder: string;
  dateRange: string;
  allowedRoles: string[];
  minimumViews: number;
  queueClips: boolean;
}
