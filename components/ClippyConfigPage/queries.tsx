import { gql } from "@apollo/client";

export const GET_CLIPPY = gql`
  query getClippy {
    getClippy {
      token
      commands {
        _id
        command
        kind
        sortOrder
        dateRange
        allowedRoles
        minimumViews
        queueClips
      }
    }
  }
`;

export const REGENERATE_CLIPPY_TOKEN = gql`
  mutation RegenerateClippyTokenMutation {
    regenerateClippyToken {
      _id
      token
      user {
        username
        name
      }
    }
  }
`;

export const UPDATE_CLIPPY_COMMAND = gql`
  mutation UpdateClippyCommand($clippyCommandParams: ClippyCommandInput!) {
    updateClippyCommand(params: $clippyCommandParams) {
      _id
      command
      sortOrder
      dateRange
      allowedRoles
      queueClips
    }
  }
`;
