import React, { useEffect } from "react";
import { useLazyQuery } from "@apollo/client";
import styled from "styled-components";
import { useSelector } from "react-redux";
import keyBy from "lodash/keyBy";
import { GET_CLIPPY } from "../queries";
import ExternalAccounts from "../../Settings/ExternalAccounts";
import StreamToolsLayout from "../components/StreamToolsLayout";
import YourUrlCard from "../components/cards/YourUrlCard";
import ShoutoutCard from "../components/cards/ShoutoutCard/ShoutoutCard";
import { ClippyCommandKindEnum } from "../../../enums";
import AdditionalCommands from "../components/cards/AdditionalCommands/AdditionalCommands";
import usePrevious from "../../../hooks/usePrevious";
import AuthAwareLayout from "../../Layouts/AuthAwareLayout/AuthAwareLayout";
import ClippyLogo from "../components/ClippyLogo";
import ClippyLayout from "../components/ClippyLayout";
import MetaHead from "../../Layouts/MetaHead";

const Layout = ({ children, isNewNavEnabled }) => {
  if (isNewNavEnabled) {
    return (
      <>
        <MetaHead metaTitle="Clippy | Stream Tools" />
        <ClippyLayout>{children}</ClippyLayout>
      </>
    );
  }

  return (
    <AuthAwareLayout metaTitle="Clippy | Steam Tools" containsSubheader={false}>
      <ClippyHeader>
        <ClippyLogo />
      </ClippyHeader>
      {children}
    </AuthAwareLayout>
  );
};

const StreamToolsPage = ({ isNewNavEnabled }) => {
  const isCurrentUserTwitchVerified = useSelector(state => state.user.currentUser.twitchVerified);
  const previousTwitchVerification = usePrevious(isCurrentUserTwitchVerified);
  const [getClippy, { loading, error, data }] = useLazyQuery(GET_CLIPPY);

  const clippy = data?.getClippy;

  useEffect(() => {
    getClippy();
  }, []);

  useEffect(() => {
    if (!previousTwitchVerification && isCurrentUserTwitchVerified) {
      getClippy();
    }
  }, [isCurrentUserTwitchVerified]);

  if (error || loading || !clippy) {
    return <Layout isNewNavEnabled={isNewNavEnabled}>{error && <NeedAuthenticationMessage />}</Layout>;
  }

  const allCommands = keyBy(clippy?.commands, "kind");

  return (
    <Layout isNewNavEnabled={isNewNavEnabled}>
      <StreamToolsLayout
        top={<YourUrlCard userToken={clippy?.token} />}
        leftColumn={<ShoutoutCard commandConfig={allCommands[ClippyCommandKindEnum.SHOUTOUT]} />}
        rightColumn={<AdditionalCommands allCommands={allCommands} />}
      />
    </Layout>
  );
};

const NeedAuthenticationMessage = () => {
  return (
    <AuthenticateOverlay>
      <h4>Please authenticate with Twitch</h4>
      <ExternalAccounts
        onlyShowLinks
        displaySteam={false}
        displayBattlenet={false}
        displayLinkedin={false}
        displayTwitter={false}
        displayDiscord={false}
        displayFacebook={false}
        displayTwitch
        displayGoogle={false}
        displaySnapchat={false}
        displayAimLab={false}
        displayValorant={false}
        avoidGrid
      />
    </AuthenticateOverlay>
  );
};

const AuthenticateOverlay = styled.div`
  width: 400px;
  margin: 0 auto;

  h4 {
    text-align: center;
    padding-bottom: 10px;
    color: #0a0c12;
  }
`;

const ClippyHeader = styled.div`
  margin-bottom: 20px;
  width: 100%;
`;

export default StreamToolsPage;
