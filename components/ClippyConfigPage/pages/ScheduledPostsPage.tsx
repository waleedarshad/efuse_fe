import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getScheduledPosts } from "../../../store/actions/userActions";
import ScheduledPost from "../components/ScheduledPost";
import MetaHead from "../../Layouts/MetaHead";
import ClippyLayout from "../components/ClippyLayout";
import EFCreatePostComposer from "../../EFCreatePostComposer/EFCreatePostComposer";

const ScheduledPostsPage = () => {
  const dispatch = useDispatch();
  const { scheduledPosts } = useSelector(state => state.user);

  useEffect(() => {
    dispatch(getScheduledPosts());
  }, []);

  const onModalClose = () => {
    setTimeout(() => {
      dispatch(getScheduledPosts());
    }, 500);
  };

  return (
    <>
      <MetaHead metaTitle="Clippy | Scheduled Posts" />
      <ClippyLayout>
        <EFCreatePostComposer placeholderText="Create scheduled post" onClose={onModalClose} isScheduleOnly />
        {scheduledPosts.map(post => (
          <ScheduledPost post={post} key={post._id} />
        ))}
      </ClippyLayout>
    </>
  );
};

export default ScheduledPostsPage;
