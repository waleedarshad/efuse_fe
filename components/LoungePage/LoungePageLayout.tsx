import { ReactNode, useContext } from "react";
import { useSelector } from "react-redux";
import EFCreatePostComposer from "../EFCreatePostComposer/EFCreatePostComposer";
import EFSideOpportunities from "../EFSideOpportunities/EFSideOpportunities";
import FeedAd from "../FeedAd/FeedAd";
import FeedFilters from "../FeedFilters/FeedFilters";
import { GraphqlContext } from "../GraphqlContext/GraphqlContext";
import ImportantLinks from "../ImportantLinks/ImportantLinks";
import ThreeColumnLayout from "../Layouts/ThreeColumnLayout/ThreeColumnLayout";
import TwitchPlayerIframe from "../TwitchPlayerIframe/TwitchPlayerIframe";
import FeatureFlag from "../General/FeatureFlag/FeatureFlag";
import FeatureFlagVariant from "../General/FeatureFlag/FeatureFlagVariant/FeatureFlagVariant";
import PortfolioProgressReminder from "../PortfolioProgress/PortfolioProgressReminder";
import EmptyComponent from "../EmptyComponent/EmptyComponent";
import { LoungeFeedType } from "./types";

interface LoungePageLayoutProps {
  children: ReactNode;
  loungeFeedType: LoungeFeedType;
}

const LoungePageLayout = ({ children, loungeFeedType }: LoungePageLayoutProps) => {
  const { data } = useContext(GraphqlContext);
  const event = data?.getFeaturedEvent;

  const currentUser = useSelector(state => state.auth.currentUser);
  const isWindowView = useSelector(state => state.messages.isWindowView);
  const loungeTwitchStreamHighlight = useSelector(state => state.features.lounge_twitch_stream_highlight);
  const loungeTopBanner = useSelector(state => state.features.lounge_top_banner);

  return (
    <ThreeColumnLayout
      metaTitle="eFuse | Lounge"
      leftChildren={<EFSideOpportunities />}
      rightChildren={
        <>
          <FeedFilters currentFilter={loungeFeedType} />
          <ImportantLinks />
        </>
      }
      isWindowView={isWindowView}
      setThirdToTopMobile
    >
      {loungeTwitchStreamHighlight && event && (
        <TwitchPlayerIframe twitchChannel={event?.twitchChannel} roundedCorners backgroundShadow marginBottom />
      )}

      <FeatureFlag name="feature_web_create_post_composer" experiment>
        <FeatureFlagVariant flagState>
          <EFCreatePostComposer placeholderText="What's on your mind?" isScheduleOnly={false} />
        </FeatureFlagVariant>
      </FeatureFlag>
      {loungeTopBanner && <FeedAd adID="178881" setID="431992" />}

      <FeatureFlag name="complete_portfolio_component">
        <FeatureFlagVariant flagState>
          <PortfolioProgressReminder />
        </FeatureFlagVariant>
      </FeatureFlag>

      <FeatureFlag name="killswitch:global_disable_lounge_feeds">
        <FeatureFlagVariant flagState>
          <EmptyComponent
            text={
              <>
                <h6>This feed is currently disabled. Please check back soon.</h6>
                <h6>We are sorry for the inconvenience.</h6>
              </>
            }
          />
        </FeatureFlagVariant>
        <FeatureFlagVariant flagState={false}>{children}</FeatureFlagVariant>
      </FeatureFlag>
    </ThreeColumnLayout>
  );
};
export default LoungePageLayout;
