import LoungePageLayout from "./LoungePageLayout";
import { LoungeFeedType } from "./types";
import GET_ERENA_FEATURED_EVENT from "../../graphql/ERenaFeaturedEventQuery";
import { GraphqlContextProvider } from "../GraphqlContext/GraphqlContext";
import FeaturedFeed from "./feeds/FeaturedFeed";
import FollowingFeed from "./feeds/FollowingFeed";
import VerifiedFeed from "./feeds/VerifiedFeed";
import OnboardingModal from "../Modals/OnboardingModal";

interface LoungePageProps {
  loungeFeedType: LoungeFeedType;
}

const LoungePage = ({ loungeFeedType }: LoungePageProps) => {
  return (
    <GraphqlContextProvider query={GET_ERENA_FEATURED_EVENT}>
      <LoungePageLayout loungeFeedType={loungeFeedType}>
        {loungeFeedType === "featured" && <FeaturedFeed />}
        {loungeFeedType === "following" && <FollowingFeed />}
        {loungeFeedType === "verified" && <VerifiedFeed />}
      </LoungePageLayout>
      <OnboardingModal />
    </GraphqlContextProvider>
  );
};

export default LoungePage;
