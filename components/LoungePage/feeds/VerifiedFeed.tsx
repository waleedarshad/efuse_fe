import { useDispatch } from "react-redux";
import Feed from "../../Feed/Feed";
import { getVerifiedFeed } from "../../../store/actions/feedActions";

const VerifiedFeed = () => {
  const dispatch = useDispatch();

  return <Feed feedType="verified" getFeed={page => dispatch(getVerifiedFeed(page, 10))} />;
};

export default VerifiedFeed;
