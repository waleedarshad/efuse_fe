import { useDispatch } from "react-redux";
import Feed from "../../Feed/Feed";
import { getFollowingFeed } from "../../../store/actions/feedActions";

const FollowingFeed = () => {
  const dispatch = useDispatch();

  return <Feed feedType="following" getFeed={page => dispatch(getFollowingFeed(page, 10))} />;
};

export default FollowingFeed;
