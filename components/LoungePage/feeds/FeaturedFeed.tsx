import { useDispatch } from "react-redux";
import Feed from "../../Feed/Feed";
import { getFeaturedFeed } from "../../../store/actions/feedActions";

const FeaturedFeed = () => {
  const dispatch = useDispatch();

  return <Feed feedType="featured" getFeed={page => dispatch(getFeaturedFeed(page, 10))} />;
};

export default FeaturedFeed;
