import React, { Component } from "react";
import { connect } from "react-redux";
import EFAlert from "../EFAlert/EFAlert";

import { getMembers, changeOwner, clearMembers } from "../../store/actions/organizationActions";
import EFRectangleButton from "../Buttons/EFRectangleButton/EFRectangleButton";
import TransferModal from "./TransferModal/TransferModal";

class TransferOwnership extends Component {
  state = {
    showModal: false
  };

  loadMembers = () => {
    this.props.getMembers(1, 15, this.props.organization._id);
    this.setState({ showModal: true });
  };

  onClose = () => {
    this.props.clearMembers();
    this.setState({ showModal: false });
  };

  onMemberSelect = user => {
    const fullName = user.name;

    const title = "Transfer Ownership";
    const message = `Are you sure you would like to transfer ownership to ${fullName}? This will remove your admin ability and make ${fullName} the official admin.`;
    const confirmLabel = "Yes";
    const cancelLabel = "No";
    const onConfirm = () =>
      this.props.changeOwner(`organizations/changeowner/${this.props.organization._id}`, user._id);

    EFAlert(title, message, confirmLabel, cancelLabel, onConfirm);
  };

  render() {
    const { showModal } = this.state;
    const { organization, members } = this.props;
    return (
      <>
        <EFRectangleButton text="Transfer Ownership" onClick={this.loadMembers} />
        {members && (
          <TransferModal
            show={showModal}
            organization={organization}
            members={members}
            onMemberSelect={this.onMemberSelect}
            onHide={this.onClose}
          />
        )}
      </>
    );
  }
}

const mapStateToProps = state => ({
  organization: state.organizations.organization,
  members: state.organizations.members
});

export default connect(mapStateToProps, { getMembers, changeOwner, clearMembers })(TransferOwnership);
