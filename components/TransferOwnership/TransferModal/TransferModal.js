import Checkbox from "../../Checkbox/Checkbox";
import EFPrimaryModal from "../../Modals/EFPrimaryModal/EFPrimaryModal";

const TransferModal = ({ show, onHide, members, onMemberSelect, organization }) => {
  const memberCheckboxes = members
    .filter(m => m.isBanned == false)
    .map((member, index) => (
      <span className="mr-2" key={index}>
        <Checkbox
          custom
          name={member.user._id}
          type="checkbox"
          label={member.user.name}
          id={`memberradio-${index}`}
          theme="internal"
          checked={organization.user === member.user._id}
          value={member.user._id}
          onChange={() => onMemberSelect(member.user)}
        />
      </span>
    ));
  return (
    <EFPrimaryModal title="Members" isOpen={show} allowBackgroundClickClose={false} onClose={onHide}>
      <div className="d-inline-flex">
        {members.length === 0 ? <p>No members exist in this organization</p> : memberCheckboxes}
      </div>
    </EFPrimaryModal>
  );
};

export default TransferModal;
