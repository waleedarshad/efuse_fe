import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLink } from "@fortawesome/pro-regular-svg-icons";
import PropTypes from "prop-types";
import Style from "./EFMicrolink.module.scss";

const ANALYTICS_KEY = "MICROLINK_CLICKED";

const EFMicrolink = ({ href, title, description, imageUrl }) => {
  const shouldRenderComponent = !!href && !!title && !!description && !!imageUrl;

  if (!shouldRenderComponent) {
    return <></>;
  }

  return (
    <a
      href={href}
      className={Style.container}
      target="_blank"
      rel="noopener noreferrer nofollow"
      onClick={() => analytics.track(ANALYTICS_KEY)}
    >
      <div className={Style.imageContainer}>
        <div className={Style.image} style={{ backgroundImage: `url(${imageUrl})` }} />
      </div>
      <div className={Style.caption}>
        <FontAwesomeIcon icon={faLink} className={Style.linkIcon} />
        <h6 className={Style.title}>{title}</h6>
        <p className={Style.description}>{description}</p>
      </div>
    </a>
  );
};

EFMicrolink.propTypes = {
  /**
   * Url to redirect when clicked
   */
  href: PropTypes.string,
  /**
   * Title of the content
   */
  title: PropTypes.string,
  /**
   * Description of the content
   */
  description: PropTypes.string,
  /**
   * Image src url to render
   */
  imageUrl: PropTypes.string
};

EFMicrolink.defaultProps = {
  href: "",
  title: "",
  description: "",
  imageUrl: ""
};

export default EFMicrolink;
