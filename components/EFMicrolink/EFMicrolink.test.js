import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { shallow } from "enzyme";
import { faLink } from "@fortawesome/pro-regular-svg-icons";

import EFMicrolink from "./EFMicrolink";

describe("EFMicrolink", () => {
  let subject;

  beforeEach(() => {
    subject = shallow(
      <EFMicrolink
        href="bass.com/getmesome"
        title="get some bass"
        description="I caught you a delicious bass"
        imageUrl="some-bass-pic.png"
      />
    );
  });

  it("does NOT render anything when title is missing", () => {
    subject = shallow(
      <EFMicrolink href="bass.com/getmesome" imageUrl="some-bass-pic.png" description="I caught you a delicious bass" />
    );

    expect(subject).toEqual({});
  });

  it("does NOT render anything when description is missing", () => {
    subject = shallow(<EFMicrolink href="bass.com/getmesome" title="get some bass" imageUrl="some-bass-pic.png" />);

    expect(subject).toEqual({});
  });

  it("does NOT render anything when image is missing", () => {
    subject = shallow(
      <EFMicrolink href="bass.com/getmesome" title="get some bass" description="I caught you a delicious bass" />
    );

    expect(subject).toEqual({});
  });

  it("does NOT render anything when href is missing", () => {
    subject = shallow(
      <EFMicrolink title="get some bass" description="I caught you a delicious bass" imageUrl="some-bass-pic.png" />
    );

    expect(subject).toEqual({});
  });

  it("renders clickable content with correct relationship props that opens in new tab", () => {
    const link = subject.find("a");

    expect(link.prop("href")).toEqual("bass.com/getmesome");
    expect(link.prop("target")).toEqual("_blank");
    expect(link.prop("rel")).toEqual("noopener noreferrer nofollow");
  });

  it("renders the provided image", () => {
    expect(subject.find(".image").prop("style")).toEqual({ backgroundImage: "url(some-bass-pic.png)" });
  });

  it("renders the provided title", () => {
    expect(subject.find("h6").text()).toBe("get some bass");
  });

  it("renders the provided description", () => {
    expect(subject.text()).toContain("I caught you a delicious bass");
  });

  it("renders a link icon", () => {
    expect(subject.find(FontAwesomeIcon).prop("icon")).toEqual(faLink);
  });

  it("tracks analytics when microlink is clicked", () => {
    subject.find(".container").simulate("click");

    expect(analytics.track).toHaveBeenCalledWith("MICROLINK_CLICKED");
  });
});
