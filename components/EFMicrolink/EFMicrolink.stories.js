/* eslint-disable react/jsx-props-no-spreading */
import React from "react";
import EFMicrolink from "./EFMicrolink";

export default {
  title: "Shared/EFMicrolink",
  component: EFMicrolink
};

const Story = args => (
  <div style={{ width: "400px" }}>
    <EFMicrolink {...args} />
  </div>
);

export const Default = Story.bind({});
Default.args = {
  href: "#",
  title:
    "Franzen pop-up hammock, organic air plant viral direct trade. Iceland kickstarter viral lomo four dollar toast",
  description:
    "Polaroid bushwick hashtag, actually fanny pack vexillologist tattooed mustache kickstarter plaid fam umami.",
  imageUrl: "https://i.ytimg.com/vi/3KgmY5NrEzU/maxresdefault.jpg"
};
