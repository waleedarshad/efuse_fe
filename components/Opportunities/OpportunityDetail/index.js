import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft } from "@fortawesome/pro-solid-svg-icons";
import { NextSeo } from "next-seo";
import { useRouter } from "next/router";
import React, { useEffect } from "react";
import { Col } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import Error from "../../../pages/_error";
import {
  clearOpportunity,
  getOpportunityById,
  getOpportunityBySlug,
  setOpportunity
} from "../../../store/actions/opportunityActions";
import sanitizeMetaDescription from "../../../store/utils/sanitizeMetaDescription";
import EFIsLoggedIn from "../../EFIsLoggedIn/EFIsLoggedIn";
import EFSideOpportunities from "../../EFSideOpportunities/EFSideOpportunities";
import GlobalBackButton from "../../GlobalBackButton/GlobalBackButton";
import PublicInternal from "../../Layouts/PublicInternal";
import TwoColumnLayout from "../../Layouts/TwoColumnLayout/index";
import AdditionalInformation from "./AdditionalInformation/AdditionalInformation";
import Style from "./index.module.scss";
import OpportunityFullView from "./OpportunityFullView/OpportunityFullView";

const OpportunityDetail = ({ pageProps }) => {
  const router = useRouter();
  const dispatch = useDispatch();

  const authUser = useSelector(state => state.auth.currentUser);
  const opportunity = useSelector(state => state.opportunities.opportunity);

  useEffect(() => {
    const { id } = router.query;
    analytics.track("OPPORTUNITY_VIEW", {
      opportunityId: pageProps?.opportunity ? pageProps.opportunity._id : id,
      opportunityType: pageProps?.opportunity?.opportunityType,
      externalSource: pageProps?.opportunity?.external_source ? pageProps.opportunity.external_source : "none"
    });
    analytics.page("Opportunities View");
    grabOpportunity();

    return () => {
      dispatch(clearOpportunity());
    };
  }, []);

  const grabOpportunity = () => {
    const { o, id } = router.query;

    if (!opportunity?._id) {
      if (pageProps?.opportunity) {
        dispatch(setOpportunity(pageProps.opportunity));
      } else if (o) {
        dispatch(getOpportunityBySlug(o));
      } else if (id) {
        dispatch(getOpportunityById(id));
      }
    }
  };

  if (pageProps && (!pageProps.ssrStatusCode || pageProps.ssrStatusCode === 422 || pageProps.ssrStatusCode === 400)) {
    return <Error statusCode={pageProps.ssrStatusCode} />;
  }

  let opportunityUrl = "";
  if (pageProps?.opportunity?.shortName) {
    opportunityUrl = `o/${pageProps?.opportunity.shortName}`;
  } else if (pageProps?.opportunity?._id) {
    opportunityUrl = `opportunities/show?id=${pageProps.opportunity._id}`;
  }

  const mainComponent = (
    <>
      {pageProps?.opportunity && (
        <NextSeo
          title={`${pageProps.opportunity.title} | eFuse Opportunity`}
          description={sanitizeMetaDescription(pageProps.opportunity.description)}
          canonical={`https://efuse.gg/${opportunityUrl}`}
          openGraph={{
            type: "website",
            url: `https://efuse.gg/${opportunityUrl}`,
            title: `${pageProps.opportunity.title} | eFuse Opportunity`,
            site_name: "eFuse.gg",
            description: sanitizeMetaDescription(pageProps.opportunity.description),
            images: [
              {
                url: `${pageProps.opportunity?.image?.url}?id=${pageProps.opportunity._id}`
              }
            ]
          }}
          twitter={{
            handle: "@eFuseOfficial",
            site: `https://efuse.gg/${opportunityUrl}`,
            cardType: "summary_large_image"
          }}
        />
      )}
      <OpportunityFullView opportunity={opportunity} currentUser={authUser} />
    </>
  );

  return (
    <EFIsLoggedIn
      altComponent={
        <PublicInternal>
          <Col lg={9} className="no-gutters">
            {mainComponent}
          </Col>
          <Col lg={3}>
            <div className={Style.additionalInformationWrapper}>
              <AdditionalInformation opportunity={opportunity} />
            </div>
            <EFSideOpportunities />
          </Col>
        </PublicInternal>
      }
    >
      <TwoColumnLayout
        leftChildren={
          <>
            <GlobalBackButton>
              <div className={Style.backContainer}>
                <FontAwesomeIcon icon={faChevronLeft} className={Style.chevron} />
                <span className={Style.backText}>Back to Opportunities</span>
              </div>
            </GlobalBackButton>

            <div className={Style.additionalInformationWrapper}>
              <AdditionalInformation opportunity={opportunity} />
            </div>
            <EFSideOpportunities />
          </>
        }
        noBioCard
        subHeader
      >
        {mainComponent}
      </TwoColumnLayout>
    </EFIsLoggedIn>
  );
};

export default OpportunityDetail;
