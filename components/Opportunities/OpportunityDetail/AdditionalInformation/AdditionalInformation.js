import uniqueId from "lodash/uniqueId";
import React from "react";
import { formatDate } from "../../../../helpers/GeneralHelper";
import EFCard from "../../../Cards/EFCard/EFCard";
import Style from "./AdditionalInformation.module.scss";

const AdditionalInformation = ({ opportunity }) => {
  const noInformationToRender =
    !opportunity.startDate &&
    !opportunity.dueDate &&
    !opportunity.applicationResponseDate &&
    !opportunity.followUpInstructions &&
    !opportunity.openings &&
    !opportunity.requirements?.length > 0;

  if (noInformationToRender) {
    return <></>;
  }

  return (
    <EFCard widthTheme="fullWidth">
      <p className={Style.title}>Additional Information</p>
      <div className={Style.additionalInformation}>
        {opportunity.startDate && (
          <div className={Style.oppDetails}>
            <p>Application Start Date</p>
            <p>
              <b>{formatDate(opportunity.startDate)}</b>
            </p>
          </div>
        )}
        {opportunity.dueDate && (
          <div className={Style.oppDetails}>
            <p>Application Due Date</p>
            <p>
              <b>{formatDate(opportunity.dueDate)}</b>
            </p>
          </div>
        )}
        {opportunity.applicationResponseDate && (
          <div className={Style.oppDetails}>
            <p>Expected Application Response</p>
            <p>
              <b>{formatDate(opportunity.applicationResponseDate)}</b>
            </p>
          </div>
        )}
        {opportunity.followUpInstructions && (
          <div className={Style.oppDetails}>
            <p>Application Follow-up Instructions</p>
            <p>
              <b>{opportunity.followUpInstructions}</b>
            </p>
          </div>
        )}
        {opportunity.openings && (
          <div className={Style.oppDetails}>
            <p>Openings</p>
            <p>
              <b>{opportunity.openings}</b>
            </p>
          </div>
        )}
        {opportunity.requirements?.length > 0 && (
          <div className={Style.oppDetails}>
            <p className={Style.applyText}>Requirements</p>
            <ul>
              {opportunity.requirements.map(requirement => (
                <li key={uniqueId()}>{requirement.name}</li>
              ))}
            </ul>
          </div>
        )}
      </div>
    </EFCard>
  );
};

export default AdditionalInformation;
