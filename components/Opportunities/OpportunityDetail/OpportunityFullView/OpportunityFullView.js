import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import getConfig from "next/config";
import Link from "next/link";
import { Col } from "react-bootstrap";
import { useSelector } from "react-redux";
import { faShareSquare, faMapMarkerAlt } from "@fortawesome/pro-solid-svg-icons";

import { getImage } from "../../../../helpers/GeneralHelper";
import { getFullOpportunityUrl } from "../../../../helpers/UrlHelper";
import DynamicModal from "../../../DynamicModal/DynamicModal";
import { EFHtmlParser } from "../../../EFHtmlParser/EFHtmlParser";
import ShareLinkButton from "../../../ShareLinkButton";
import UserImageList from "../../OpportunityLanding/UserImageList/UserImageList";
import ApplicantStatusButton from "../../OpportunityWrapper/ApplicantStatusButton/ApplicantStatusButton";
import OpportunityContextMenu from "../../OpportunityWrapper/OpportunityContextMenu/OpportunityContextMenu";
import AdditionalInformation from "../AdditionalInformation/AdditionalInformation";
import Style from "./OpportunityFullView.module.scss";

const { publicRuntimeConfig } = getConfig();

const OpportunityFullView = ({ opportunity, currentUser }) => {
  const { image, onBehalfOfOrganization, associatedOrganization } = opportunity;

  const enableApplicantsLisProfileImages = useSelector(state => state.features.opportunity_applicants_list);

  const portfolioUrl = `/u/${opportunity?.associatedUser?.username}`;
  const profileUrl = onBehalfOfOrganization ? `/organizations/show?id=${opportunity.organization}` : portfolioUrl;
  const owner = currentUser && currentUser.id === opportunity.user;
  const { feBaseUrl } = publicRuntimeConfig;

  const userLoggedIn = currentUser?.id;
  return (
    <Col>
      <div className={Style.fullContainer}>
        <img className={Style.cardImage} src={getImage(image, "opportunity")} />
        <OpportunityContextMenu opportunity={opportunity} wrapperClass={Style.menu} icon="dots" />
        <div className={Style.bottomContainer}>
          <Link href={onBehalfOfOrganization ? profileUrl : "/u/[u]"} as={profileUrl}>
            <div className={Style.profileImageContainer}>
              <img
                className={Style.profileImage}
                src={
                  opportunity?.associatedOrganization?.profileImage?.url
                    ? opportunity?.associatedOrganization?.profileImage?.url
                    : opportunity?.associatedUser?.profilePicture?.url
                }
              />
            </div>
          </Link>
          {userLoggedIn ? (
            <ShareLinkButton
              userId={currentUser?.id}
              buttonText="Share"
              icon={faShareSquare}
              theme="opportunityShareButton"
              title={`Share ${owner ? "your" : `${opportunity?.associatedUser?.name}'s`} Opportunity`}
              url={`${feBaseUrl}${getFullOpportunityUrl(opportunity)}`}
            />
          ) : (
            <DynamicModal
              flow="LoginSignup"
              openOnLoad={false}
              startView="login"
              displayCloseButton
              allowBackgroundClickClose={false}
            >
              <div className={Style.shareButton}>
                <FontAwesomeIcon className={Style.shareIcon} icon={faShareSquare} />
                Share
              </div>
            </DynamicModal>
          )}
          {opportunity?.applicants?.length > 0 && enableApplicantsLisProfileImages && (
            <div className={Style.applicantsList}>
              <UserImageList
                largeSize
                imageList={opportunity?.applicants?.map((applicant, index) => {
                  if (index < 4) {
                    return applicant?.profilePicture?.url;
                  }
                })}
              />
              <p className={Style.listText}>People you know who've applied</p>
            </div>
          )}

          <div className={Style.textContainer}>
            <h6 className={Style.subTitle}>
              {onBehalfOfOrganization ? (
                userLoggedIn ? (
                  <Link
                    href={`/organizations/show?id=${associatedOrganization?._id}`}
                    as={`/organizations/show?id=${associatedOrganization?._id}`}
                  >
                    <a>{associatedOrganization?.name}</a>
                  </Link>
                ) : (
                  <DynamicModal
                    flow="LoginSignup"
                    openOnLoad={false}
                    startView="login"
                    displayCloseButton
                    allowBackgroundClickClose={false}
                  >
                    <a>{associatedOrganization?.name}</a>
                  </DynamicModal>
                )
              ) : userLoggedIn ? (
                <Link href="/u/[u]" as={portfolioUrl}>
                  <a>{opportunity.self}</a>
                </Link>
              ) : (
                <DynamicModal
                  flow="LoginSignup"
                  openOnLoad={false}
                  startView="login"
                  displayCloseButton
                  allowBackgroundClickClose={false}
                >
                  <a>{opportunity.self}</a>
                </DynamicModal>
              )}
            </h6>
            <h1 className={Style.title}>{opportunity.title}</h1>
            {opportunity?.location && (
              <p className={Style.opportunityLocation}>
                <FontAwesomeIcon className={Style.locationIcon} icon={faMapMarkerAlt} />
                {opportunity.location}
              </p>
            )}
            <p className={Style.desc}>
              <EFHtmlParser>{opportunity.description}</EFHtmlParser>
            </p>
          </div>

          <div className={Style.apply}>
            <ApplicantStatusButton opportunity={opportunity} />

            {opportunity.goldRequired && (
              <div className={Style.goldRequiredContainer}>
                <img
                  className={Style.goldLogo}
                  src="https://cdn.efuse.gg/uploads/static/global/efuse_gold.png"
                  alt="eFuse Gold Logo"
                />
                <div className={Style.goldRequiredText}>
                  eFuse Gold subscription required to apply to this opportunity.
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
      <div className={Style.additionalInformationWrapper}>
        <AdditionalInformation opportunity={opportunity} />
      </div>
    </Col>
  );
};

export default OpportunityFullView;
