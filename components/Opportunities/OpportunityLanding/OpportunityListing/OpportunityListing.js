import uniqueId from "lodash/uniqueId";
import React from "react";
import { connect } from "react-redux";
import { getFullOpportunityUrl } from "../../../../helpers/UrlHelper";
import GeneralCard from "../../../Cards/GeneralCard/GeneralCard";
import HorizontalScroll from "../../../HorizontalScroll/HorizontalScroll";
import Style from "./OpportunityListing.module.scss";

const OpportunityListing = props => {
  const { opportunities, opportunityApplicantsList } = props;

  return (
    <HorizontalScroll customButtonMargin={Style.buttonMarginTop} withRightButton={opportunities?.length > 3}>
      {opportunities.map(opp => {
        const postedByOrganization = opp?.associatedOrganization?._id;
        const ownerUrl = postedByOrganization
          ? `/organizations/show?id=${opp.associatedOrganization._id}`
          : `/u/${opp?.associatedUser?.username}`;

        return (
          <GeneralCard
            key={uniqueId()}
            badge={opp?.opportunityType}
            title={opp?.title}
            subTitle={opp?.associatedOrganization?.name ? opp?.associatedOrganization?.name : opp?.associatedUser?.name}
            onImageClickLink={getFullOpportunityUrl(opp)}
            onTitleClickLink={postedByOrganization ? ownerUrl : "/u/[u]"}
            image={opp?.image?.url}
            date={opp?.createdAt}
            location={opp?.location}
            goldRequired={opp?.goldRequired}
            userImages={opportunityApplicantsList && opp?.applicants}
            titleLinkAs={ownerUrl}
          />
        );
      })}
    </HorizontalScroll>
  );
};

const mapStateToProps = state => ({
  opportunityApplicantsList: state.features.opportunity_applicants_list
});

export default connect(mapStateToProps, {})(OpportunityListing);
