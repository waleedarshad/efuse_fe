/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import { useRouter } from "next/router";
import React, { useEffect } from "react";
import { Col } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { faGamepadAlt, faPlus } from "@fortawesome/pro-solid-svg-icons";
import FEATURE_FLAGS from "../../../common/featureFlags";
import { getOpportunitiesNavigationList } from "../../../navigation/opportunities";
import { getOpportunitiesByType } from "../../../store/actions/opportunityActions";
import AnimatedLogo from "../../AnimatedLogo";
import EFCircleIconButtonTooltip from "../../Buttons/EFCircleIconButtonTooltip/EFCircleIconButtonTooltip";
import FeatureFlag from "../../General/FeatureFlag/FeatureFlag";
import FeatureFlagVariant from "../../General/FeatureFlag/FeatureFlagVariant/FeatureFlagVariant";
import HorizontalScroll from "../../HorizontalScroll/HorizontalScroll";
import EFSubHeader from "../../Layouts/Internal/EFSubHeader/EFSubHeader";
import Internal from "../../Layouts/Internal/Internal";
import PublicInternal from "../../Layouts/PublicInternal";
import OpportunitiesLanding from "../../OpportunitiesV2/OpportunitiesLanding";
import OpportunityFilters from "../OpportunityWrapper/OpportunityFilters/OpportunityFilters";
import OpportunityBucket from "./OpportunityBucket/OpportunityBucket";
import Style from "./OpportunityLanding.module.scss";
import OpportunityListing from "./OpportunityListing/OpportunityListing";
import PromotedOpportunitySection from "./PromotedOpportunitySection/PromotedOpportunitySection";

const OpportunityLanding = ({ view, isWindowView }) => {
  const dispatch = useDispatch();
  const router = useRouter();
  const recentOpportunitiesByTypes = useSelector(state => state.opportunities.recentOpportunitiesByTypes);
  const promotedOpportunities = useSelector(state => state.opportunities.promoted_opportunities);
  const loading = useSelector(state => state.loader.loading);
  const currentUser = useSelector(state => state.auth.currentUser);

  const eventOpportunities = recentOpportunitiesByTypes.events;
  const scholarshipOpportunities = recentOpportunitiesByTypes.scholarships;
  const jobOpportunities = recentOpportunitiesByTypes.jobs;
  const teamOpportunities = recentOpportunitiesByTypes.teamOpenings;

  const userLoggedIn = currentUser?.id;

  useEffect(() => {
    analytics.page("Opportunities Landing Page");
    dispatch(getOpportunitiesByType(!userLoggedIn));
  }, [userLoggedIn]);

  const applyCategory = category => {
    router.push(`opportunities/${category}`);
  };

  const loader = (
    <div className={Style.loader}>
      <AnimatedLogo theme="inline" />
    </div>
  );

  const mainComponent = (
    <>
      <div className={Style.bottomContainer}>
        <Col md={3} className="d-none d-lg-block">
          <OpportunityFilters view={view} redirectToBrowse loggedIn={currentUser?._id} />
        </Col>
        <Col fluid="md" lg={9}>
          <HorizontalScroll customStyle={Style.horizontalScroll}>
            <div className={Style.bucketsContainer}>
              <OpportunityBucket
                onClick={() => {
                  analytics.track("OPPORTUNITY_BUCKET_CLICKED", { bucketType: "team-openings" });
                  applyCategory("team-openings");
                }}
                src="https://cdn.efuse.gg/uploads/static/global/tournament_panel.png"
                bucketText="Team Openings"
                alt="Team Opening Bucket"
              />
              <OpportunityBucket
                onClick={() => {
                  analytics.track("OPPORTUNITY_BUCKET_CLICKED", { bucketType: "events" });
                  applyCategory("events");
                }}
                src="https://cdn.efuse.gg/uploads/static/global/event_panel.png"
                bucketText="Events"
                alt="Events Bucket"
              />
              <OpportunityBucket
                onClick={() => {
                  analytics.track("OPPORTUNITY_BUCKET_CLICKED", { bucketType: "scholarships" });
                  applyCategory("scholarships");
                }}
                src="https://cdn.efuse.gg/uploads/static/global/panel_scholarships.png"
                bucketText="Scholarships"
                alt="Scholarships Bucket"
              />
              <OpportunityBucket
                onClick={() => {
                  analytics.track("OPPORTUNITY_BUCKET_CLICKED", { bucketType: "jobs" });
                  applyCategory("jobs");
                }}
                src="https://cdn.efuse.gg/uploads/static/global/jobs_panel.png"
                bucketText="Jobs"
                alt="Employment Bucket"
              />
            </div>
          </HorizontalScroll>
          {loading ? (
            loader
          ) : (
            <>
              <div className={Style.filtersMobile}>
                <span>Filters</span>
              </div>
              {promotedOpportunities && (
                <div className={Style.promotedOppSection}>
                  <div className={Style.opportunitySection}>
                    <h1 className={Style.opportunityTitle}>Promoted</h1>
                  </div>
                  <div className={Style.promotedOppList}>
                    <PromotedOpportunitySection marginRight="8px" />
                  </div>
                </div>
              )}
              <div className={Style.subOppSection}>
                <div className={Style.opportunitySection}>
                  <h1 className={Style.opportunityTitle}>Events</h1>
                  <p className={Style.viewAll} onClick={() => applyCategory("events")}>
                    View All
                  </p>
                </div>
                <div className={Style.promotedOppList}>
                  <OpportunityListing opportunities={eventOpportunities} />
                </div>
              </div>
              <div className={Style.subOppSection}>
                <div className={Style.opportunitySection}>
                  <h1 className={Style.opportunityTitle}>Jobs</h1>
                  <p className={Style.viewAll} onClick={() => applyCategory("jobs")}>
                    View All
                  </p>
                </div>
                <div className={Style.promotedOppList}>
                  <OpportunityListing opportunities={jobOpportunities} />
                </div>
              </div>
              <div className={Style.subOppSection}>
                <div className={Style.opportunitySection}>
                  <h1 className={Style.opportunityTitle}>Team Openings</h1>
                  <p className={Style.viewAll} onClick={() => applyCategory("team-openings")}>
                    View All
                  </p>
                </div>
                <div className={Style.promotedOppList}>
                  <OpportunityListing opportunities={teamOpportunities} />
                </div>
              </div>
              <div className={Style.subOppSection}>
                <div className={Style.opportunitySection}>
                  <h1 className={Style.opportunityTitle}>Scholarships</h1>
                  <p className={Style.viewAll} onClick={() => applyCategory("scholarships")}>
                    View All
                  </p>
                </div>
                <div className={Style.promotedOppList}>
                  <OpportunityListing opportunities={scholarshipOpportunities} />
                </div>
              </div>
            </>
          )}
        </Col>
      </div>
    </>
  );

  return userLoggedIn ? (
    <Internal metaTitle="eFuse | Opportunities" isWindowView={isWindowView} containsSubheader>
      <EFSubHeader
        headerIcon={faGamepadAlt}
        navigationList={getOpportunitiesNavigationList(router.pathname)}
        actionButtons={[
          <FeatureFlag key={0} name={FEATURE_FLAGS.ALLOW_OPPORTUNITY_CREATION}>
            <FeatureFlagVariant flagState>
              <EFCircleIconButtonTooltip
                icon={faPlus}
                colorTheme="primary"
                shadowTheme="small"
                size="medium"
                text="Create Opportunity"
                internalHref="/opportunities/create"
                tooltipContent="Create an Opportunity"
              />
            </FeatureFlagVariant>
          </FeatureFlag>
        ]}
      />
      {mainComponent}
    </Internal>
  ) : (
    <PublicInternal>{mainComponent}</PublicInternal>
  );
};

const OpportunityLandingRouter = props => {
  return (
    <FeatureFlag name={FEATURE_FLAGS.OPPORTUNITY_FILTER_MODAL}>
      <FeatureFlagVariant flagState>
        <OpportunitiesLanding />
      </FeatureFlagVariant>
      <FeatureFlagVariant flagState={false}>
        {/* eslint-disable-next-line react/jsx-props-no-spreading */}
        <OpportunityLanding {...props} />
      </FeatureFlagVariant>
    </FeatureFlag>
  );
};
export default OpportunityLandingRouter;
