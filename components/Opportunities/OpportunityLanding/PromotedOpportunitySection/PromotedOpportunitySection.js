import React, { Component } from "react";
import PromotedOpportunity from "../../../PromotedOpportunity/PromotedOpportunity";
import HorizontalScroll from "../../../HorizontalScroll/HorizontalScroll";
import Style from "./PromotedOpportunitySection.module.scss";

class PromotedOpportunitySection extends Component {
  render() {
    return (
      <HorizontalScroll customStyle={Style.addPadding} customButtonMargin={Style.buttonMarginTop}>
        <PromotedOpportunity type="ImageCard" marginRight={this.props.marginRight} />
      </HorizontalScroll>
    );
  }
}

export default PromotedOpportunitySection;
