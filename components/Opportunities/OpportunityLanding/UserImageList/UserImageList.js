import React from "react";
import Style from "./UserImageList.module.scss";

const UserImageList = props => {
  const { imageList, largeSize } = props;
  return (
    <>
      {imageList.map(image => {
        if (image) {
          return <img className={`${Style.userIcon} ${largeSize && Style.largeSize}`} src={image} />;
        }
      })}
    </>
  );
};

export default UserImageList;
