import {
  faChartLine,
  faEye,
  faEyeSlash,
  faInfo,
  faPencilAlt,
  faShareSquare,
  faUsers
} from "@fortawesome/pro-solid-svg-icons";
import getConfig from "next/config";
import Router from "next/router";
import PropTypes from "prop-types";
import { useDispatch, useSelector } from "react-redux";
import { getFullOpportunityUrl } from "../../../../helpers/UrlHelper";
import { statusUpdate, toggleOpportunityBoost } from "../../../../store/actions/opportunityActions";
import { shareLinkModal } from "../../../../store/actions/shareAction";
import CustomDropdown from "../../../CustomDropdown/CustomDropdown";

const { publicRuntimeConfig } = getConfig();

const OpportunityContextMenu = ({ icon, opportunity, webview, wrapperClass }) => {
  const dispatch = useDispatch();
  const currentUser = useSelector(state => state.auth.currentUser);

  const { feBaseUrl } = publicRuntimeConfig;
  const isAdmin = currentUser?.roles?.includes("admin");
  const owner = currentUser?.id === opportunity?.user;
  const profileUrl = opportunity?.associatedOrganization
    ? `/org/${opportunity?.associatedOrganization?.shortName}`
    : `/u/${opportunity?.associatedUser?.username}`;

  const menu = [];

  // Share option will not be available to webviews
  !webview &&
    menu.push({
      title: "Share",
      as: "button",
      icon: faShareSquare,
      onClick: () =>
        dispatch(shareLinkModal(true, "Share this opportunity", `${feBaseUrl}${getFullOpportunityUrl(opportunity)}`))
    });

  // View more vs view profile options
  opportunity?.external
    ? menu.push({
        title: "View More",
        icon: faInfo,
        as: "button",
        onClick: () => window.open(opportunity?.link, "opportunity full item")
      })
    : menu.push({
        title: "View Profile",
        icon: faInfo,
        as: "button",
        type: "button",
        onClick: () => {
          analytics.track("OPPORTUNITY_APPLICANT_VIEW_USER_PROFILE");
          Router.push(`${profileUrl}`);
        }
      });

  // Options for eFuse admin or opportunity owner
  if (owner || isAdmin) {
    const publishStatusNotClosed = opportunity?.publishStatus !== "closed";

    // Option to change opportunity status
    menu.push({
      title: publishStatusNotClosed ? "Close" : "Open",
      icon: publishStatusNotClosed ? faEyeSlash : faEye,
      as: "button",
      onClick: () =>
        dispatch(
          statusUpdate(
            `/opportunities/update_status/${opportunity?._id}`,
            { publishStatus: publishStatusNotClosed ? "closed" : "open" },
            false
          )
        )
    });

    // Options for those opportunities which are not external
    if (!opportunity?.external) {
      menu.push({
        title: "Applicants",
        icon: faUsers,
        as: "button",
        onClick: () => {
          analytics.track("OPPORTUNITY_APPLICANT_VIEW_APPLICANTS");
          Router.push(`/opportunities/applicants?id=${opportunity?._id}`);
        }
      });

      // Edit option will not be available to webviews
      !webview &&
        menu.push({
          title: "Edit",
          path: `/opportunities/create?id=${opportunity?._id}`,
          icon: faPencilAlt
        });
    }

    // Boost Opportunity
    if (isAdmin) {
      menu.push({
        title: opportunity?.metadata?.boosted ? "Un-Boost" : "Boost",
        as: "button",
        type: "button",
        icon: faChartLine,
        onClick: () => dispatch(toggleOpportunityBoost(opportunity?._id, !opportunity?.metadata?.boosted))
      });
    }
  }

  return (
    <div className={wrapperClass}>
      <CustomDropdown icon={icon} items={menu} />
    </div>
  );
};

OpportunityContextMenu.propTypes = {
  opportunity: PropTypes.shape({
    id: PropTypes.string,
    _id: PropTypes.string,
    user: PropTypes.string,
    external: PropTypes.bool,
    publishStatus: PropTypes.string,
    link: PropTypes.string,
    associatedUser: PropTypes.shape({ id: PropTypes.string, _id: PropTypes.string, username: PropTypes.string }),
    associatedOrganization: PropTypes.shape({
      id: PropTypes.string,
      _id: PropTypes.string,
      shortName: PropTypes.string
    }),
    metadata: PropTypes.shape({ boosted: PropTypes.bool })
  }),
  webview: PropTypes.bool,
  wrapperClass: PropTypes.string.isRequired,
  icon: PropTypes.string
};

OpportunityContextMenu.defaultProps = {
  opportunity: {},
  webview: false,
  icon: "darkDots"
};

export default OpportunityContextMenu;
