import React from "react";
import { useSelector } from "react-redux";
import { Row, Col } from "react-bootstrap";
import InfiniteScroll from "react-infinite-scroller";

import AnimatedLogo from "../../../AnimatedLogo";
import NoRecordFound from "../../../NoRecordFound/NoRecordFound";
import OpportunitySmallListItem from "../OpportunitySmallListing/OpportunitySmallListItem/OpportunitySmallListItem";
import OpportunityLargeListItem from "../OpportunityLargeListing/OpportunityLargeListItem/OpportunityLargeListItem";
import Style from "../OpportunityWrapper.module.scss";

const OpportunityListing = ({ loadMore, smallListView, webview }) => {
  const opportunities = useSelector(state => state.opportunities.opportunities);
  const hasNextPage = useSelector(state => state.opportunities.pagination.hasNextPage);
  const isGettingFirstOpportunity = useSelector(state => state.opportunities.isGettingFirstOpportunity);
  const displayExternalSourceOpportunity = useSelector(state => state.features.display_external_source_opportunity);

  const opportunitiesList = opportunities.map((opportunity, index) => {
    return (
      <>
        {smallListView ? (
          <Col lg={12} md={12} key={index} className={Style.smallOppContainer}>
            <OpportunitySmallListItem
              opportunity={opportunity}
              webview={webview}
              display_external_source_opportunity={displayExternalSourceOpportunity}
            />
            <hr className={Style.hr} />
          </Col>
        ) : (
          <Col lg={12} md={12} key={index}>
            <OpportunityLargeListItem
              opportunity={opportunity}
              webview={webview}
              display_external_source_opportunity={displayExternalSourceOpportunity}
            />
          </Col>
        )}
      </>
    );
  });

  return (
    <>
      <InfiniteScroll
        pageStart={1}
        loadMore={loadMore}
        hasMore={hasNextPage}
        loader={<AnimatedLogo key={0} theme="inline" />}
      >
        <Row>
          {isGettingFirstOpportunity && (
            <div className="text-center" style={{ width: "100%" }}>
              <AnimatedLogo key={0} theme="" />
            </div>
          )}
          {opportunitiesList.length === 0 ? <NoRecordFound /> : opportunitiesList}
        </Row>
      </InfiniteScroll>
    </>
  );
};

export default OpportunityListing;
