import { withRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { Col } from "react-bootstrap";
import { connect, useDispatch, useSelector } from "react-redux";
import { faGamepadAlt } from "@fortawesome/pro-solid-svg-icons";

import FEATURE_FLAGS from "../../../common/featureFlags";
import { getOpportunitiesNavigationList } from "../../../navigation/opportunities";
import { navigateToBrowse } from "../../../store/actions/opportunitiesv2/opportunitiesv2Actions";
import { clearOpportunities, getOpportunitiesNew, setCurrentPage } from "../../../store/actions/opportunityActions";
import { getFlagForFeature } from "../../../store/selectors/featureFlagSelectors";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";
import FeatureFlag from "../../General/FeatureFlag/FeatureFlag";
import FeatureFlagVariant from "../../General/FeatureFlag/FeatureFlagVariant/FeatureFlagVariant";
import EFSubHeader from "../../Layouts/Internal/EFSubHeader/EFSubHeader";
import Internal from "../../Layouts/Internal/Internal";
import PublicInternal from "../../Layouts/PublicInternal";
import OpportunitiesApplied from "../../OpportunitiesV2/OpportunitiesApplied";
import OpportunitiesOwned from "../../OpportunitiesV2/OpportunitiesOwned";
import PromotedOpportunity from "../../PromotedOpportunity/PromotedOpportunity";
import FiltersApplied from "./FiltersApplied/FiltersApplied";
import OpportunityFilters from "./OpportunityFilters/OpportunityFilters";
import OpportunityListing from "./OpportunityListing/OpportunityListing";
import Style from "./OpportunityWrapper.module.scss";

let page = 1;
const OpportunityWrapper = ({
  isWindowView,
  searchObject,
  setCurrentPage: setCurrentPageProps,
  view,
  getOpportunitiesNew: getOpportunitiesNewProps,
  clearOpportunities: clearOpportunitiesProps,
  router,
  category,
  opportunitySmallList,
  opportunityLargeList,
  pagination,
  webview,
  promotedOpportunities,
  currentUser
}) => {
  const [switchToggleValue, setToggle] = useState(false);

  useEffect(() => {
    analytics.page(`Opportunities ${returnAnalyticsPageTitle(view)}`);
    analytics.track(`OPPORTUNITY_${returnAnalyticsTrackTitle(view)}_LIST_VIEW`);
  }, []);

  const getOpportunities = nextPage => {
    if (router.pathname === "/opportunities/all" || router.asPath === `/opportunities/${category}`) {
      getOpportunitiesNewProps(nextPage, searchObject || {}, getCustomPath(currentUser));
    } else if (router.pathname === "/opportunities/owned" || router.pathname === "/opportunities/applied") {
      getOpportunitiesNewProps(nextPage, {}, router.pathname, currentUser?._id);
    } else {
      getOpportunitiesNewProps(nextPage, searchObject || {});
    }
  };

  useEffect(() => {
    clearOpportunitiesProps();
    getOpportunities(page);
    setCurrentPageProps(view);
  }, [currentUser?.id]);

  const getCustomPath = currentUserArgs => {
    return currentUserArgs?.id ? "/opportunities" : "/public/opportunities";
  };

  const returnAnalyticsPageTitle = viewArgs => {
    if (viewArgs !== "owned") {
      return "BROWSE";
    }
    if (viewArgs === "applied") {
      return "APPLIED";
    }
    return "MY";
  };

  const returnAnalyticsTrackTitle = viewArgs => {
    if (viewArgs !== "owned") {
      return "Browse";
    }
    if (viewArgs === "applied") {
      return "Applied";
    }
    return "My Opportunities";
  };

  const loadMore = () => {
    page += 1;
    getOpportunities(page);
  };

  const totalOpportunities = (
    <div className={Style.totalOpps}>
      <inline className={Style.total}>{pagination.totalDocs} Total Opportunities</inline>
    </div>
  );

  const userLoggedIn = currentUser?.id;
  const smallListView = (opportunitySmallList && switchToggleValue) || !opportunityLargeList;

  const mainComponent = (
    <>
      <Col lg={3} md={4}>
        <OpportunityFilters
          view={view}
          avoidStickySidebar
          displayToggle={opportunitySmallList}
          switchToggle={() => setToggle(!switchToggleValue)}
          switchToggleValue={switchToggleValue}
        />
        {!webview && promotedOpportunities && (
          <div className={`stickySidebar ${Style.verticalList}`}>
            <PromotedOpportunity type="ImageCard" displayNumber={3} marginTop="32px" />
            <br />
          </div>
        )}
      </Col>
      <Col md={8} lg={9}>
        <div className={Style.filters}>
          <FiltersApplied />
        </div>
        {smallListView ? (
          <div className={Style.listingContainer}>
            {totalOpportunities}
            <hr className={Style.hr} />
            <OpportunityListing loadMore={loadMore} smallListView={smallListView} webview={webview} />
          </div>
        ) : (
          <OpportunityListing loadMore={loadMore} smallListView={smallListView} webview={webview} />
        )}
      </Col>
    </>
  );

  return userLoggedIn ? (
    <Internal metaTitle="eFuse | Opportunities" isWindowView={isWindowView} containsSubheader>
      <EFSubHeader
        headerIcon={faGamepadAlt}
        navigationList={getOpportunitiesNavigationList(router.pathname)}
        actionButtons={[
          // eslint-disable-next-line react/jsx-key
          <FeatureFlag name={FEATURE_FLAGS.ALLOW_OPPORTUNITY_CREATION}>
            <FeatureFlagVariant flagState>
              <EFRectangleButton
                colorTheme="primary"
                fontWeightTheme="bold"
                shadowTheme="small"
                size="medium"
                text="Create Opportunity"
                internalHref="/opportunities/create"
              />
            </FeatureFlagVariant>
          </FeatureFlag>
        ]}
      />
      {mainComponent}
    </Internal>
  ) : (
    <PublicInternal>{mainComponent}</PublicInternal>
  );
};

const mapStateToProps = state => ({
  isWindowView: state.messages.isWindowView,
  pagination: state.opportunities.pagination,
  viewFromStore: state.opportunities.view,
  opportunitySmallList: state.features.opportunity_small_list,
  opportunityLargeList: state.features.opportunity_large_list,
  webview: state.webview.status,
  promotedOpportunities: state.features.promoted_opportunities,
  opportunities: state.opportunities.opportunities,
  searchObject: state.opportunities.searchObject,
  currentUser: state.auth.currentUser
});

const OpportunityWrapperConnected = connect(mapStateToProps, {
  getOpportunitiesNew,
  setCurrentPage,
  clearOpportunities
})(withRouter(OpportunityWrapper));

const OpportunityWrapperClientRouter = props => {
  const dispatch = useDispatch();

  const isOpportunityFilterModalOn = useSelector(state =>
    getFlagForFeature(state, FEATURE_FLAGS.OPPORTUNITY_FILTER_MODAL)
  );

  if (isOpportunityFilterModalOn) {
    if (props.view === "applied") {
      return <OpportunitiesApplied />;
    }
    if (props.view === "owned") {
      return <OpportunitiesOwned />;
    }
    if (props.view === "index") {
      if (typeof window !== "undefined") dispatch(navigateToBrowse());
    }
    return <></>;
  }

  // eslint-disable-next-line react/jsx-props-no-spreading
  return <OpportunityWrapperConnected {...props} />;
};

export default OpportunityWrapperClientRouter;
