import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { withRouter } from "next/router";
import isEmpty from "lodash/isEmpty";
import ApplicationModal from "../ApplicationModal/ApplicationModal";
import { checkUserOpportunityRequirements, apply } from "../../../../store/actions/applicantActions";
import ConfirmAlert from "../../../ConfirmAlert/ConfirmAlert";
import DynamicModal from "../../../DynamicModal/DynamicModal";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";
import { getIndexName } from "../../../../helpers/AlgoliaHelper";

const ApplicantStatusButton = ({ opportunity }) => {
  const user = useSelector(state => state.auth.currentUser);
  const isAuthenticated = useSelector(state => state.auth.isAuthenticated);
  const userRequirements = useSelector(state => state.applicants.userRequirements[opportunity?._id]);

  const dispatch = useDispatch();

  useEffect(() => {
    if (isAuthenticated && opportunity?._id) {
      dispatch(checkUserOpportunityRequirements(opportunity._id));
    }
  }, [isAuthenticated, opportunity?._id]);

  const isOrgCaptain = opportunity?.associatedOrganization?.captains?.includes(user?._id);
  let isOwner = false;
  // if user is owner, set owner to true
  if (opportunity?.associatedUser?._id === user?._id) {
    isOwner = true;
  }

  const applyToOpportunity = (opportunityId, answers) => {
    if (answers?.length > 0) {
      dispatch(apply(opportunity?.opportunityType, opportunityId, answers));
    } else {
      dispatch(apply(opportunity?.opportunityType, opportunityId));
    }
  };

  const trackAnalytics = () => {
    // mapped to Order Completed in Algolia Insights destination
    analytics.track("OPPORTUNITY_APPLY_BUTTON_CLICKED", {
      opportunityId: opportunity?._id,
      opportunityType: opportunity?.opportunityType,
      externalSource: opportunity?.external_source ? opportunity?.external_source : "none",
      products: [
        {
          objectID: opportunity?._id // used for personalization in Algolia - required
        }
      ],
      index: getIndexName("OPPORTUNITIES") // used for personalization in Algolia - required
    });
  };

  // if user is not logged in, show Apply button (when clicked displays login/signup modal)
  if (!isAuthenticated) {
    return (
      <DynamicModal
        flow="LoginSignup"
        openOnLoad={false}
        startView="login"
        displayCloseButton
        allowBackgroundClickClose={false}
      >
        <EFRectangleButton colorTheme="secondary" text="Apply" />
      </DynamicModal>
    );
  }

  // if owner or organization captain, return applicants button
  if (isOwner || isOrgCaptain) {
    return (
      <EFRectangleButton
        type="button"
        text="View Applicants"
        colorTheme="secondary"
        internalHref={`/opportunities/applicants?id=${opportunity?._id}&type=noaction`}
        onClick={() => {
          analytics.track("OPPORTUNITY_APPLICANT_VIEW_APPLICANTS");
        }}
      />
    );
  }

  // check if the opportunity is external, if so, direct to the external link instead of applying internally
  if (opportunity?.external_url) {
    return (
      <EFRectangleButton
        colorTheme="secondary"
        text="Apply"
        externalTarget="_blank"
        externalHref={opportunity.external_url}
        onClick={() => trackAnalytics()}
      />
    );
  }

  // if user has applied to this opportunity, show applied
  const applicant = opportunity?.applicant || {};

  if (!isEmpty(applicant)) {
    return <EFRectangleButton text="Applied" colorTheme="secondary" />;
  }

  // if closed, return closed button
  if (opportunity?.publishStatus === "closed") {
    return <EFRectangleButton text="Closed" colorTheme="secondary" disabled />;
  }

  // if no questions trigger a confirmation modal to apply
  if (
    (opportunity?.candidateQuestions?.length === 0 || !opportunity?.candidateQuestions) &&
    userRequirements?.canApply
  ) {
    return (
      <ConfirmAlert
        title="You are sharing your information with the creator of this organization."
        message="Do you wish to proceed?"
        onYes={() => applyToOpportunity(opportunity?._id)}
        onButtonClick={() => trackAnalytics()}
      >
        <EFRectangleButton text="Apply" colorTheme="secondary" />
      </ConfirmAlert>
    );
  }

  // Default Apply Button (render application modal if the above aren't triggered)
  return (
    <ApplicationModal
      userRequirements={userRequirements}
      opportunity={opportunity}
      applyToOpportunity={applyToOpportunity}
    >
      <EFRectangleButton text="Apply" colorTheme="secondary" onClick={() => trackAnalytics()} />
    </ApplicationModal>
  );
};

export default withRouter(ApplicantStatusButton);
