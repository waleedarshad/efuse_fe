import { connect } from "react-redux";
import dynamic from "next/dynamic";
import { Form } from "react-bootstrap";
import React, { Component } from "react";
import Error from "../../../Error/Error";
import InputLabel from "../../../InputLabel/InputLabel";
import ActionButtonGroup from "../../../Layouts/Internal/ActionButtonGroup/ActionButtonGroup";

const ReactQuill = dynamic(() => import("react-quill"), { ssr: false });

class AddApplicantQuestion extends Component {
  state = {};

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    if (this.props.questions.length != Object.keys(this.props.answers).length) {
      this.setState({
        inProgress: true
      });
    }
  }

  onEditorChange = (updatedText, i) => {
    if (this.props.questions.length != Object.keys(this.props.answers).length) {
      this.setState({
        inProgress: true
      });
    } else {
      this.setState({
        inProgress: false
      });
    }
    if (updatedText) {
      this.props.updateQuestionsState(i, updatedText);
    }
  };

  onSubmit = event => {
    event.preventDefault();
    const { answers } = this.props;
    const { validated } = this.state;
    if (!validated) {
      this.setState({ validated: true });
    }
    this.props.onSubmit(answers);
  };

  render() {
    const { show, onCancel, questions, opportunity } = this.props;
    const { inProgress } = this.state;

    return (
      <div>
        <h2>Application</h2>
        <div>
          <Error />
          <Form noValidate onSubmit={this.onSubmit}>
            {questions.map((question, i) => {
              return (
                <div key={i}>
                  <InputLabel theme="internal">{question.question}</InputLabel>
                  <ReactQuill onChange={event => this.onEditorChange(event, i)} />
                  <br />
                </div>
              );
            })}
            <ActionButtonGroup
              onCancel={onCancel}
              disabled={inProgress}
              overrideSave={opportunity?.applicationCost > 0 ? "Next" : "Submit"}
            />
          </Form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  inProgress: state.portfolio.inProgress
});

export default connect(mapStateToProps, {})(AddApplicantQuestion);
