import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMapMarkerAlt } from "@fortawesome/pro-solid-svg-icons";
import { OverlayTrigger, Tooltip } from "react-bootstrap";

import { getImage } from "../../../../../helpers/GeneralHelper";
import Style from "./OpportunitySmallListItem.module.scss";
import { getFullOpportunityUrl } from "../../../../../helpers/UrlHelper";
import OpportunityContextMenu from "../../OpportunityContextMenu/OpportunityContextMenu";

const VisibilitySensor = require("react-visibility-sensor").default;

const OpportunitySmallListItem = ({ opportunity, webview, display_external_source_opportunity }) => {
  const { image, opportunityType, subType, onBehalfOfOrganization, associatedOrganization } = opportunity;

  return (
    <VisibilitySensor
      onChange={isVisible => {
        if (isVisible) {
          analytics.track("OPPORTUNITY_BROWSE_LIST_VIEW", {
            opportunityId: opportunity._id,
            opportunityType: opportunity?.opportunityType,
            externalSource: opportunity?.external_source ? opportunity.external_source : "none"
          });
        }
      }}
    >
      <div className={Style.container}>
        <OpportunityContextMenu opportunity={opportunity} webview={webview} wrapperClass={Style.menu} />
        <img className={Style.image} src={getImage(image, "opportunity")} />
        <div className={Style.textContainer}>
          <h3 className={Style.title}>
            <a className={Style.title} href={getFullOpportunityUrl(opportunity)}>
              {opportunity?.title}
            </a>
          </h3>
          <p className={Style.createdBy}>
            {`${opportunityType} - ${subType} by `}
            {opportunity?.external ? (
              <inline className={Style.bold}>{opportunity?.company_name}</inline>
            ) : (
              <inline className={Style.bold}>
                {onBehalfOfOrganization ? (
                  <a className={Style.link} href={`/organizations/show?id=${associatedOrganization?._id}`}>
                    {associatedOrganization?.name}
                  </a>
                ) : (
                  <a className={Style.link} href={`/users/posts?id=${opportunity?.user}`}>
                    {opportunity.self}
                  </a>
                )}
              </inline>
            )}
          </p>
          <div className={Style.attributeContainer}>
            {opportunity?.location && (
              <p className={Style.attribute}>
                <OverlayTrigger
                  placement="top"
                  overlay={
                    <Tooltip id="tooltip-hype" style={{ zIndex: 9999 }}>
                      Location
                    </Tooltip>
                  }
                >
                  <FontAwesomeIcon className={`${Style.locationIcon}`} icon={faMapMarkerAlt} />
                </OverlayTrigger>
                <inline>{opportunity?.location}</inline>
              </p>
            )}
            {opportunity?.goldRequired && (
              <p className={Style.attribute}>
                <OverlayTrigger
                  placement="top"
                  overlay={
                    <Tooltip id="tooltip-hype" style={{ zIndex: 9999 }}>
                      eFuse Gold Subscription Required to Apply
                    </Tooltip>
                  }
                >
                  <img
                    className={Style.goldLogo}
                    src="https://efuse.s3.amazonaws.com/uploads/static/global/efuse_gold_noborder.png"
                    alt="eFuse Gold Logo"
                  />
                </OverlayTrigger>
              </p>
            )}
          </div>
          {opportunity?.external_source && display_external_source_opportunity && (
            <p className={`${Style.attribute} ${Style.floatRight}`}>
              <OverlayTrigger
                placement="top"
                overlay={
                  <Tooltip id="tooltip-hype" style={{ zIndex: 9999 }}>
                    This opportunity was provided by {opportunity?.external_source}
                  </Tooltip>
                }
              >
                {opportunity?.external_source === "hitmarker" && (
                  <img
                    className={Style.providerIcon}
                    src="https://cdn.efuse.gg/uploads/static/global/hitmarker_icon.png"
                  />
                )}
              </OverlayTrigger>
              <inline className={Style.externalSource}>{opportunity?.external_source}</inline>
            </p>
          )}
        </div>
      </div>
    </VisibilitySensor>
  );
};

OpportunitySmallListItem.propTypes = {
  opportunity: PropTypes.object.isRequired,
  fullView: PropTypes.bool
};

OpportunitySmallListItem.defaultProps = {
  fullView: false
};

export default OpportunitySmallListItem;
