import React, { Component } from "react";
import { FormGroup, Col, Row } from "react-bootstrap";
import { connect } from "react-redux";

import Style from "./OpportunityFilters.module.scss";
import InputLabel from "../../../InputLabel/InputLabel";
import SelectBox from "../../../SelectBox/SelectBox";
import Checkbox from "../../../Checkbox/Checkbox";
import {
  updateFilterValue,
  searchOpportunity,
  getMaxMoneyIncluded,
  searchMyOpportunity,
  searchAppliedOpportunity,
  updateSearchObject,
  searchPublicOpportunities
} from "../../../../store/actions/opportunityFilterActions";
import { mergeLabelsAndValues } from "../../../../helpers/GeneralHelper";
import { updateStateFromProps, onFilterChange, applyFilters } from "../../../../helpers/FilterHelper";
import { opportunityType } from "./dropdown.json";
import { searchByName } from "../../../../store/actions/organizationActions";

import { getGameRequirements } from "../../../../store/actions/gameRequirementActions";
import GoogleAutoComplete from "../../../GoogleAutocomplete/GoogleAutocomplete";
import Switch from "../../../Switch/Switch";
import FeatureFlag from "../../../General/FeatureFlag/FeatureFlag";
import FeatureFlagVariant from "../../../General/FeatureFlag/FeatureFlagVariant/FeatureFlagVariant";

class OpportunityFilters extends Component {
  state = {
    searchFilters: {
      location: "",
      "moneyIncluded?": "",
      opportunityType: "",
      game: "",
      "verified.status.retain.key": "",
      publishStatus: ""
    }
  };

  static getDerivedStateFromProps(props, state) {
    return updateStateFromProps(props, state);
  }

  currentView = () => {
    const { view } = this.props;
    const viewNames = {
      applied: "searchAppliedOpportunity",
      owned: "searchMyOpportunity",
      student: "searchOpportunity",
      index: "searchOpportunity"
    };
    return viewNames[view];
  };

  onChange = event => {
    const { name, value, label } = onFilterChange(event);
    analytics.track("OPPORTUNITY_FILTER_APPLIED", {
      name,
      value,
      label
    });
    this.setFilterAndSearch(name, value, label);
  };

  onSuggestSelect = suggest => {
    const value = suggest === undefined ? "" : suggest.label;
    analytics.track("OPPORTUNITY_FILTER_APPLIED", {
      value
    });
    this.setFilterAndSearch("location", value, value);
  };

  getSearchMethod = () => {
    if (this.props.redirectToBrowse && this.props.loggedIn) {
      return this.props.searchPublicOpportunities;
    }
    if (this.props.redirectToBrowse) {
      return this.props.searchOpportunity;
    }
    if (this.props.loggedIn) {
      return this.props[this.currentView()];
    }
    return this.props.searchPublicOpportunities;
  };

  setFilterAndSearch = (name, value, label) => {
    this.props.updateFilterValue(name, value, label);
    this.props.updateSearchObject({ [name]: value });
    applyFilters(
      name,
      value,
      label,
      this.props.filtersApplied,
      this.getSearchMethod(),
      false,
      "",
      false,
      this.props.redirectToBrowse
    );
  };

  onOrganizationSelect = selectedOption => {
    this.setFilterAndSearch("organization", selectedOption.value, selectedOption.label);
  };

  render() {
    const { games, view, avoidStickySidebar, displayToggle, switchToggle, switchToggleValue } = this.props;
    const isOwned = view === "owned";
    const { searchFilters } = this.state;
    const moneyIncluded = searchFilters["moneyIncluded?"];
    const moneyIncludedFilter = (
      <Col>
        <Checkbox
          custom
          name="moneyIncluded?"
          type="radio"
          label="Money Included"
          id={`verified-status-${"Money Included"}`}
          theme="filter"
          checked={moneyIncluded}
          value
          onChange={this.onChange}
        />
      </Col>
    );
    const verifiedFilter = (
      <Col>
        <Checkbox
          custom
          name="verified.status.retain.key"
          type="radio"
          label="Verified"
          id="verified-status-Verified"
          theme="filter"
          checked={searchFilters["verified.status.retain.key"]}
          value
          onChange={this.onChange}
        />
      </Col>
    );
    // const verifiedFilter = [
    //   { label: ("Yes"), value: "true" },
    //   { label: ("No"), value: "false" }
    // ].map((filter, index) => (
    //   <Col key={index}>
    //     <Checkbox
    //       custom
    //       name="verified.status.retain.key"
    //       type="radio"
    //       label={filter.label}
    //       id={`verified-status-${filter.label}`}
    //       theme="internal"
    //       checked={filter.value === searchFilters["verified.status.retain.key"]}
    //       value={filter.value}
    //       onChange={this.onChange}
    //     />
    //   </Col>
    // ));

    // const publishFilter = (
    //   <Col>
    //     <Checkbox
    //       custom
    //       name="publishStatus"
    //       type="radio"
    //       label="Publish Status"
    //       id={`verified-status-Publish Status`}
    //       theme="filter"
    //       checked={searchFilters["publishStatus"]}
    //       value={true}
    //       onChange={this.onChange}
    //     />
    //   </Col>
    // )
    const publishFilter = [
      { label: "Open", value: "open" },
      { label: "Closed", value: "closed" }
    ].map((filter, index) => (
      <Col key={index}>
        <Checkbox
          custom
          name="publishStatus"
          type="radio"
          label={filter.label}
          id={`verified-status-${filter.label}`}
          theme="filter"
          checked={filter.value === searchFilters.publishStatus}
          value={filter.value}
          onChange={this.onChange}
        />
      </Col>
    ));

    return (
      <div className={`${!avoidStickySidebar && "stickySidebar"} ${Style.filterWrapper}`}>
        <div className={Style.card}>
          <div className={Style.header}>
            <h5 className={Style.headerTitle}>Filters</h5>
          </div>
          <div className={Style.body}>
            <InputLabel theme="darkColor">Opportunity Type</InputLabel>
            <FormGroup>
              <SelectBox
                validated={false}
                options={mergeLabelsAndValues(
                  ["Select Type", "Scholarship", "Employment Opening", "Team Opening", "Event"],
                  opportunityType
                )}
                size="lg"
                theme="whiteShadow"
                block="true"
                name="opportunityType"
                value={searchFilters.opportunityType}
                onChange={this.onChange}
              />
            </FormGroup>
            <FeatureFlag name="opportunity_location_filter">
              <FeatureFlagVariant flagState={true}>
                <InputLabel theme="darkColor">Location</InputLabel>
                <FormGroup>
                  <GoogleAutoComplete
                    onSuggest={this.onSuggestSelect}
                    placeholder="Search Location"
                    className="geoselect-filter"
                    initialValue={searchFilters.location}
                  />
                </FormGroup>
              </FeatureFlagVariant>
            </FeatureFlag>

            {/* <FormGroup>
              <ReactSelect
                onChange={this.onOrganizationSelect}
                liberateOptions={this.props.searchByName}
                placeholder="Search Organization"
                searchedOptions={this.props.searchedOrganizations}
                theme="internal"
              />
            </FormGroup> */}
            {/* <div className={Style.moneyFilter}>
              <InputLabel theme="filter">
                Money Included
              </InputLabel>
            </div> */}
            <Row className={`mb-2 ${Style.paddingTop}`}>{verifiedFilter}</Row>
            <Row className={`mb-2 ${Style.paddingTop} ${Style.paddingBottom}`}>{moneyIncludedFilter}</Row>
            {/* <InputLabel theme="filter">
              Verified
            </InputLabel> */}
            <hr className={Style.hrDivider} />
            <InputLabel theme="darkColor">Publish Status</InputLabel>
            <Row className="mb-2">{publishFilter}</Row>
            {displayToggle && (
              <>
                <hr className={Style.hrDivider} />
                <div className={Style.switchContainer}>
                  <p className={Style.switchToggleText}>Small List View</p>
                  <div className={Style.switch}>
                    <Switch
                      name="listViewToggle"
                      value={switchToggleValue}
                      checked={switchToggleValue}
                      onChange={switchToggle}
                    />
                  </div>
                </div>
              </>
            )}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  searchFilters: state.opportunities.searchFilters,
  filtersApplied: state.opportunities.filtersApplied,
  maxMoney: state.opportunities.maxMoney,
  searchedOrganizations: state.organizations.searchedOrganizations,
  moneySet: state.opportunities.moneySet,
  games: state.gameRequirements.gameRequirements
});

export default connect(mapStateToProps, {
  updateFilterValue,
  searchOpportunity,
  searchPublicOpportunities,
  getMaxMoneyIncluded,
  searchByName,
  getGameRequirements,
  searchMyOpportunity,
  searchAppliedOpportunity,
  updateSearchObject
})(OpportunityFilters);
