import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck, faTimes } from "@fortawesome/pro-solid-svg-icons";
import Style from "./DisplayRequirements.module.scss";
import AuthenticateDiscord from "../../../../AuthenticateAccounts/AuthenticateDiscord/AuthenticateDiscord";
import AuthenticateFacebook from "../../../../AuthenticateAccounts/AuthenticateFacebook/AuthenticateFacebook";
import AuthenticateYoutube from "../../../../AuthenticateAccounts/AuthenticateYoutube/AuthenticateYoutube";
import AuthenticateTwitch from "../../../../AuthenticateAccounts/AuthenticateTwitch/AuthenticateTwitch";
import AuthenticateTwitter from "../../../../AuthenticateAccounts/AuthenticateTwitter/AuthenticateTwitter";
import EducationExperienceModal from "../../../../GlobalModals/EducationExperienceModal/EducationExperienceModal";
import BusinessExperienceModal from "../../../../GlobalModals/BusinessExperienceModal/BusinessExperienceModal";

const DisplayRequirements = ({ requirementsMet, requirementsNotMet }) => {
  const BlankComponent = ({ children }) => {
    return <>{children}</>;
  };

  const getLinkComponent = social => {
    let returnComponent = BlankComponent;
    switch (social) {
      case "discordVerified":
        returnComponent = AuthenticateDiscord;
        break;
      case "facebookVerified":
        returnComponent = AuthenticateFacebook;
        break;
      case "googleVerified":
        returnComponent = AuthenticateYoutube;
        break;
      case "twitchVerified":
        returnComponent = AuthenticateTwitch;
        break;
      case "twitterVerified":
        returnComponent = AuthenticateTwitter;
        break;
      case "educationExperience":
        returnComponent = EducationExperienceModal;
        break;
      case "businessExperience":
        returnComponent = BusinessExperienceModal;
        break;
      default:
        returnComponent = BlankComponent;
        break;
    }
    return returnComponent;
  };

  return (
    <>
      <p className={Style.description}>
        * You are not able to apply to this opportunity until you add the required information below.
      </p>
      <div className={Style.requirementsNeeded}>
        <p className={Style.subTitle}>Requirements</p>
        {requirementsNotMet?.map((req, index) => {
          const LinkRequirement = getLinkComponent(req?.property);
          return (
            <LinkRequirement key={index}>
              <div key={req?._id} className={`${Style.requirement} ${Style.requirementNotMet}`}>
                <FontAwesomeIcon icon={faTimes} className={Style.times} />
                {req?.name} - Click to Add
              </div>
            </LinkRequirement>
          );
        })}
        {requirementsMet?.length > 0 ? (
          requirementsMet?.map(req => {
            return (
              <div key={req?._id} className={Style.requirement}>
                <FontAwesomeIcon icon={faCheck} className={Style.check} />
                {req?.name}
              </div>
            );
          })
        ) : (
          <div className={Style.requirement}>None</div>
        )}
      </div>
    </>
  );
};

export default DisplayRequirements;
