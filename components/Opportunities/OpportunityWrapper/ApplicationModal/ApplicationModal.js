import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import isEmpty from "lodash/isEmpty";

import Modal from "../../../Modal/Modal";
import ApplicationQuestions from "./ApplicationQuestions/ApplicationQuestions";
import DisplayRequirements from "./DisplayRequirements/DisplayRequirements";
import { checkUserOpportunityRequirements } from "../../../../store/actions/applicantActions";

const ApplicationModal = ({ children, userRequirements, opportunity, applyToOpportunity }) => {
  const dispatch = useDispatch();
  const user = useSelector(state => state.user.currentUser);
  const isAuthenticated = useSelector(state => state.auth.isAuthenticated);

  useEffect(() => {
    if (isAuthenticated && opportunity?._id) {
      dispatch(checkUserOpportunityRequirements(opportunity?._id));
    }
  }, [user]);

  // the isEmpty check satisfies old opportunities and ensure old opps work
  const modalComponent = (
    <>
      {opportunity?.candidateQuestions?.length > 0 && (userRequirements?.canApply || isEmpty(userRequirements)) && (
        <ApplicationQuestions
          questions={opportunity.candidateQuestions}
          applyToOpportunity={applyToOpportunity}
          opportunityId={opportunity?._id}
        />
      )}
      {!userRequirements?.canApply && (
        <DisplayRequirements
          requirementsMet={userRequirements?.requirementsMet}
          requirementsNotMet={userRequirements?.requirementsNotMet}
        />
      )}
    </>
  );
  return (
    <Modal
      displayCloseButton
      allowBackgroundClickClose={false}
      openOnLoad={false}
      component={modalComponent}
      textAlignCenter={false}
      customMaxWidth="1000px"
      title="Apply to Opportunity"
    >
      {children}
    </Modal>
  );
};

export default ApplicationModal;
