import React, { Component } from "react";
import dynamic from "next/dynamic";
import Style from "./ApplicationQuestions.module.scss";
import InputLabel from "../../../../InputLabel/InputLabel";
import directUpload from "../../../../hoc/directUpload";
import ApplicationFileUpload from "./ApplicationFileUpload/ApplicationFileUpload";
import EFRectangleButton from "../../../../Buttons/EFRectangleButton/EFRectangleButton";

const ReactQuill = dynamic(() => import("react-quill"), { ssr: false });

class ApplicationQuestions extends Component {
  constructor() {
    super();
    this.state = {
      answers: []
    };
  }

  onEditorChange = (text, index) => {
    const { answers } = this.state;
    const newAnswers = [...answers];
    newAnswers[index] = {
      ...newAnswers[index],
      index,
      question: text
    };
    this.setState({
      answers: newAnswers
    });
  };

  onFileUpload = (file, index) => {
    const { answers } = this.state;
    const newAnswers = [...answers];
    newAnswers[index] = {
      ...newAnswers[index],
      index,
      media: file
    };
    this.setState({
      answers: newAnswers
    });
  };

  render() {
    const { questions, opportunityId, applyToOpportunity } = this.props;
    const { answers } = this.state;
    // disable array if an undefined value exists or all questions aren't answered
    const disabled = answers?.includes(undefined) || answers?.length !== questions?.length;
    return (
      <div>
        {questions.map((question, index) => {
          return (
            <div key={question?._id} className={Style.questionWrapper}>
              <InputLabel theme="darkColor">
                {index + 1}. {question?.question}
              </InputLabel>
              {(question?.inputType === "textInput" || !question?.inputType) && (
                <ReactQuill onChange={event => this.onEditorChange(event, index)} className={Style.reactQuill} />
              )}
              {question?.inputType === "fileUpload" && (
                <ApplicationFileUpload onFileUpload={e => this.onFileUpload(e, index)} />
              )}
            </div>
          );
        })}
        <div className={Style.buttonWrapper}>
          <EFRectangleButton
            colorTheme="primary"
            disabled={disabled}
            onClick={() => applyToOpportunity(opportunityId, answers)}
            text="Submit Application"
          />
        </div>
      </div>
    );
  }
}

export default directUpload(ApplicationQuestions);
