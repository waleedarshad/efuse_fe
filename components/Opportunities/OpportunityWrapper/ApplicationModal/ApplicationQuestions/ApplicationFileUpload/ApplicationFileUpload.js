import React from "react";
import DirectUpload, { UploadTypes } from "../../../../../DirectUpload/DirectUpload";
import directUpload from "../../../../../hoc/directUpload";
import UploadDisplay from "../../../../../UploadDisplay/UploadDisplay";
import { isEmptyObject } from "../../../../../../helpers/GeneralHelper";
import Style from "./ApplicationFileUpload.module.scss";

const ApplicationFileUpload = ({
  onUploadProgress,
  onUploadError,
  onUploadStart,
  handleFinishedUpload,
  uploadProgress,
  uploadStarted,
  cancelUpload,
  file
}) => {
  const disableUpload = !isEmptyObject(file);
  return (
    <>
      {disableUpload ? (
        <div className={Style.uploadDisplayWrapper}>
          <UploadDisplay
            uploadProgress={uploadProgress}
            uploadStarted={uploadStarted}
            file={file}
            cancelUpload={cancelUpload}
            removePadding
          />
        </div>
      ) : (
        <DirectUpload
          onProgress={onUploadProgress}
          onError={onUploadError}
          preprocess={onUploadStart}
          onFinish={handleFinishedUpload}
          disabled={disableUpload}
          removeMargin
          customDesign
          directory="uploads/opportunity-applicant-files/"
          accept={UploadTypes.ApplicationFiles}
        >
          <div className={Style.uploadButton}>Upload File</div>
        </DirectUpload>
      )}
    </>
  );
};

export default directUpload(ApplicationFileUpload);
