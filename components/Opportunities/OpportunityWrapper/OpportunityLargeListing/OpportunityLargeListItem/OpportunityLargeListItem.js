import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronDown, faMapMarkerAlt, faChevronUp } from "@fortawesome/pro-solid-svg-icons";
import Link from "next/link";
import Router from "next/router";
import React, { useState } from "react";
import { OverlayTrigger, Tooltip } from "react-bootstrap";
import { connect, useSelector } from "react-redux";

import { getImage } from "../../../../../helpers/GeneralHelper";
import { getFullOpportunityUrl } from "../../../../../helpers/UrlHelper";
import CardBadge from "../../../../Cards/CardBadge/CardBadge";
import { EFHtmlParser } from "../../../../EFHtmlParser/EFHtmlParser";
import UserImageList from "../../../OpportunityLanding/UserImageList/UserImageList";
import ApplicantStatusButton from "../../ApplicantStatusButton/ApplicantStatusButton";
import OpportunityContextMenu from "../../OpportunityContextMenu/OpportunityContextMenu";
import Style from "./OpportunityLargeListItem.module.scss";

const VisibilitySensor = require("react-visibility-sensor").default;

const OpportunityLargeListItem = ({ opportunity, webview, currentUser }) => {
  const opportunityApplicantsList = useSelector(state => state.features.opportunity_applicants_list);
  const [expanded, expandContent] = useState(false);

  const { image, opportunityType, onBehalfOfOrganization, associatedOrganization } = opportunity;

  return (
    <VisibilitySensor
      onChange={isVisible => {
        if (isVisible) {
          analytics.track("OPPORTUNITY_BROWSE_LIST_VIEW", {
            opportunityId: opportunity._id,
            opportunityType: opportunity?.opportunityType,
            externalSource: opportunity?.external_source ? opportunity.external_source : "none"
          });
        }
      }}
    >
      <div className={Style.container}>
        <div className={Style.flex}>
          <OpportunityContextMenu opportunity={opportunity} webview={webview} wrapperClass={Style.menu} />
          <Link href={getFullOpportunityUrl(opportunity)}>
            <div className={Style.imageContainer}>
              <img className={Style.image} src={getImage(image, "opportunity")} />
              {currentUser?.roles?.includes("admin") && (
                <b className={Style.rank}>Rank: {opportunity?.metadata?.rank}</b>
              )}

              {opportunity.goldRequired && (
                <p className={Style.attribute}>
                  <OverlayTrigger
                    placement="top"
                    overlay={
                      <Tooltip id="tooltip-hype" style={{ zIndex: 9999 }}>
                        eFuse Gold Subscription Required to Apply
                      </Tooltip>
                    }
                  >
                    <img
                      className={Style.goldLogo}
                      src="https://cdn.efuse.gg/uploads/static/global/efuse_gold_dark_background.png"
                      alt="eFuse Gold Logo"
                    />
                  </OverlayTrigger>
                </p>
              )}
              <CardBadge opportunityType={opportunityType} />
            </div>
          </Link>
          <div className={`${Style.textContainer} ${expanded && Style.expanded}`}>
            {opportunity.external ? (
              <inline className={Style.subTitle}>{opportunity.company_name}</inline>
            ) : (
              <inline className={Style.subTitle}>
                {onBehalfOfOrganization ? (
                  <a href={`/organizations/show?id=${associatedOrganization?._id}`}>{associatedOrganization?.name}</a>
                ) : (
                  <a href={`/users/posts?id=${opportunity.user}`}>{opportunity.self}</a>
                )}
              </inline>
            )}
            <h3 className={Style.title}>
              <a className={Style.title} href={getFullOpportunityUrl(opportunity)}>
                {opportunity.title}
              </a>
            </h3>
            {opportunity?.location && (
              <div className={Style.attribute}>
                <OverlayTrigger
                  placement="top"
                  overlay={
                    <Tooltip id="tooltip-hype" style={{ zIndex: 9999 }}>
                      Location
                    </Tooltip>
                  }
                >
                  <FontAwesomeIcon className={`${Style.locationIcon}`} icon={faMapMarkerAlt} />
                </OverlayTrigger>
                <inline className={Style.attribute}>{opportunity.location}</inline>
              </div>
            )}
            <div className={Style.descriptionOverlay} />
            <p className={`${Style.description} ${opportunity?.location && Style.shortClamp}`}>
              <EFHtmlParser>{opportunity.description}</EFHtmlParser>
            </p>
          </div>
        </div>
        <div className={Style.footer}>
          <div className={Style.userImageContainer}>
            {opportunity?.applicants?.length > 0 && opportunityApplicantsList && (
              <>
                <p className={Style.listText}>People you know who've applied</p>
                <div className={Style.list}>
                  <UserImageList
                    imageList={opportunity.applicants.map((applicant, index) => {
                      if (index < 4) {
                        return applicant?.profilePicture?.url;
                      }
                    })}
                  />
                </div>
              </>
            )}
          </div>
          {expanded && (
            <div className={Style.expandUp} onClick={() => expandContent(!expanded)}>
              <FontAwesomeIcon className={Style.downArrow} icon={faChevronUp} />
            </div>
          )}
          <div
            onClick={() => {
              expanded ? Router.push(getFullOpportunityUrl(opportunity)) : expandContent(!expanded);
            }}
            className={`${Style.expandButton} ${!expanded && Style.addMargin}`}
          >
            {!expanded && <FontAwesomeIcon className={Style.downArrow} icon={faChevronDown} />}
            {expanded ? "Learn More" : "Expand"}
          </div>
          <div className={Style.applyButtonWrapper}>
            <ApplicantStatusButton opportunity={opportunity} />
          </div>
        </div>
      </div>
    </VisibilitySensor>
  );
};

const mapStateToProps = state => ({
  opportunityApplicantsList: state.features.opportunity_applicants_list
});

export default connect(mapStateToProps, {})(OpportunityLargeListItem);
