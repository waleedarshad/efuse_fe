import React, { useEffect } from "react";
import { Form } from "react-bootstrap";
import { useForm } from "react-hook-form";
import AdditionalRequirementsFields from "../AdditionalRequirementsTemplate/AdditionalRequirementsFields";
import GeneralOpportunityFields from "../GeneralOpportunityInfoTemplate/GeneralOpportunityFields";
import EFSelectControl from "../../../FormControls/EFSelectControl";
import { OPPORTUNITY_TYPES } from "../../../../common/opportunities";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";

const EditOpportunity = ({
  existingOpportunity,
  onPreviewChange,
  toggleCropModal,
  candidateQuestions,
  setCandidateQuestions,
  onSubmit
}) => {
  const {
    control,
    watch,
    handleSubmit,
    reset,
    formState: { errors }
  } = useForm({ defaultValues: existingOpportunity });
  const [title, startDate, endDate, opportunityType] = watch(["title", "startDate", "dueDate", "opportunityType"]);

  useEffect(() => {
    reset(existingOpportunity);
    setCandidateQuestions(existingOpportunity?.candidateQuestions);
  }, [existingOpportunity?._id]);

  return (
    <Form>
      <EFSelectControl
        label="Opportunity Type"
        name="opportunityType"
        placeholder="Select Opportunity Type..."
        options={Object.values(OPPORTUNITY_TYPES).map(it => ({ label: it, value: it }))}
        control={control}
        errors={errors}
        required
      />
      <GeneralOpportunityFields
        isEditing
        opportunityType={opportunityType}
        onPreviewChange={onPreviewChange}
        toggleCropModal={toggleCropModal}
        control={control}
        errors={errors}
        title={title}
        startDate={startDate}
        endDate={endDate}
      />
      <AdditionalRequirementsFields
        candidateQuestions={candidateQuestions}
        setCandidateQuestions={setCandidateQuestions}
        control={control}
        errors={errors}
      />
      <EFRectangleButton text="Submit" width="fullWidth" colorTheme="secondary" onClick={handleSubmit(onSubmit)} />
    </Form>
  );
};

export default EditOpportunity;
