import React from "react";
import { Col } from "react-bootstrap";
import { useSelector } from "react-redux";
import { getSubtypes } from "../../../../common/opportunities";
import InputRow from "../../../InputRow/InputRow";
import Style from "./GeneralOpportunityInfoTemplate.module.scss";
import EFSelectControl from "../../../FormControls/EFSelectControl";
import EFInputControl from "../../../FormControls/EFInputControl";
import EFCustomURLControl from "../../../FormControls/EFCustomURLControl";
import EFGoogleAutocompleteControl from "../../../FormControls/EFGoogleAutocompleteControl";
import EFDatepickerControl from "../../../FormControls/EFDatepickerControl";
import EFSelectGamesModalControl from "../../../FormControls/EFSelectGamesModalControl";
import EFHtmlInputControl from "../../../FormControls/EFHtmlInputControl";
import EFCroppableImageControl from "../../../FormControls/uploadControls/EFCroppableImageControl";

const GeneralOpportunityFields = ({
  isEditing,
  opportunityType,
  onPreviewChange,
  toggleCropModal,
  control,
  errors,
  title,
  startDate,
  endDate
}) => {
  const currentUser = useSelector(state => state.auth.currentUser);
  const currentUserOrgs = useSelector(state => state.user.postableOrganizations);
  const selectGamesFlag = useSelector(state => state.features.temp_web_select_games_orgs);

  const formattedCreators = [
    { value: "", label: `Select ${opportunityType || "Opportunity"} Owner...` },
    ...(currentUserOrgs || []).map(org => ({ value: org?._id, label: org?.name })),
    { value: currentUser?.id, label: currentUser?.name }
  ];

  const opportunitySubtypeOptions = [
    { value: "", label: "Select Opportunity Type..." },
    ...getSubtypes(opportunityType).map(it => ({ value: it, label: it }))
  ];

  return (
    <>
      <InputRow>
        <Col sm={12}>
          <EFSelectControl
            label={`${opportunityType || ""} Type`}
            name="subType"
            control={control}
            errors={errors}
            options={opportunitySubtypeOptions}
            required
          />
        </Col>
      </InputRow>
      <InputRow>
        <Col sm={12}>
          <EFSelectControl
            label="Owner"
            name="owner"
            control={control}
            errors={errors}
            options={formattedCreators}
            required
          />
        </Col>
      </InputRow>
      <InputRow>
        <Col sm={12}>
          <EFInputControl
            label={`${opportunityType || ""} Title`}
            name="title"
            control={control}
            errors={errors}
            placeholder={`${opportunityType || ""} Title...`}
            maxLength={70}
            required
          />
        </Col>
      </InputRow>
      <EFCustomURLControl
        slugType="opportunities"
        name="shortName"
        longName={title}
        control={control}
        errors={errors}
        pathType="o"
        disabled={isEditing}
        required
      />
      <InputRow>
        <Col sm={12}>
          <EFGoogleAutocompleteControl label="Location" control={control} name="location" />
        </Col>
      </InputRow>
      <InputRow>
        <Col lg={6}>
          <EFDatepickerControl
            label="Application Start Date"
            tooltipText="The start date for accepting applications for this opportunity."
            control={control}
            errors={errors}
            name="startDate"
            required
          />
        </Col>
        <Col lg={6}>
          <EFDatepickerControl
            label="Application End Date"
            tooltipText="The end date for accepting applications for this opportunity."
            control={control}
            errors={errors}
            name="dueDate"
            required
            validate={dueDate => dueDate >= startDate || "End date must be greater than (or equal to) the start date."}
          />
        </Col>
      </InputRow>

      <InputRow>
        <Col lg={6}>
          <EFDatepickerControl
            label="Expected Application Response Date"
            tooltipText="By when should applicants expect a response from you."
            control={control}
            errors={errors}
            required
            name="applicationResponseDate"
            validate={resDate =>
              resDate >= endDate ||
              "Expected response date must be greater than (or equal to) the application end date."
            }
          />
        </Col>
      </InputRow>

      <InputRow>
        <Col lg={12}>
          <EFInputControl
            label="Application Follow-up Instructions"
            tooltip="Instructions on how you will follow-up with applicants after application submission"
            name="followUpInstructions"
            placeholder="Follow-up instructions"
            control={control}
            errors={errors}
          />
        </Col>
      </InputRow>

      <InputRow>
        <Col lg={6}>
          <EFInputControl
            label="Money Included"
            name="moneyIncluded"
            placeholder="Payment/Salary"
            control={control}
            errors={errors}
            type="number"
            prepend="$"
            step={0.01}
            validate={money => !money || money >= 1 || "Money Included must be greater than 1."}
          />
        </Col>
        <Col lg={6}>
          <EFInputControl
            label="Positions Available"
            name="openings"
            placeholder="# of Accepted Applicants..."
            control={control}
            errors={errors}
            type="number"
            validate={applicants =>
              !applicants ||
              (applicants >= 1 && applicants <= 999) ||
              "Minimum of 1 opening and maximum of 999 openings."
            }
          />
        </Col>
      </InputRow>
      <InputRow>
        <Col sm={12}>
          <EFCroppableImageControl
            label="Background Image"
            name="image"
            control={control}
            errors={errors}
            required
            onPreviewChange={onPreviewChange}
            toggleCropperModal={toggleCropModal}
          />
        </Col>
      </InputRow>
      {selectGamesFlag && (
        <div className={Style.marginBottom}>
          <EFSelectGamesModalControl
            label="Select Game"
            name="game"
            control={control}
            selectMultiple={false}
            errors={errors}
          />
        </div>
      )}
      <InputRow>
        <Col>
          <EFHtmlInputControl label="Description" name="description" control={control} errors={errors} required />
        </Col>
      </InputRow>
    </>
  );
};

export default GeneralOpportunityFields;
