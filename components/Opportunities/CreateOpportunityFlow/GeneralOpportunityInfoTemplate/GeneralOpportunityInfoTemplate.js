import React, { useEffect } from "react";
import { Form } from "react-bootstrap";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";
import GeneralOpportunityFields from "./GeneralOpportunityFields";

const GeneralOpportunityInfoTemplate = ({
  isEditing,
  form,
  onPreviewChange,
  toggleCropModal,
  opportunityType,
  onSubmit
}) => {
  const {
    control,
    watch,
    handleSubmit,
    formState: { errors }
  } = form;

  const [title, startDate, endDate] = watch(["title", "startDate", "dueDate"]);

  useEffect(() => {
    analytics.track("OPPORTUNITY_CREATION_GENERAL_STEP");
  }, []);

  return (
    <Form>
      <GeneralOpportunityFields
        isEditing={isEditing}
        opportunityType={opportunityType}
        onPreviewChange={onPreviewChange}
        toggleCropModal={toggleCropModal}
        control={control}
        errors={errors}
        title={title}
        startDate={startDate}
        endDate={endDate}
      />
      <EFRectangleButton
        text="Next Step"
        colorTheme="light"
        size="extraLarge"
        width="fullWidth"
        buttonType="button"
        shadowTheme="medium"
        onClick={handleSubmit(onSubmit)}
      />
    </Form>
  );
};

export default GeneralOpportunityInfoTemplate;
