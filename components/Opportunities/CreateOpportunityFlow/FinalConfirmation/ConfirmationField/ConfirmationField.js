import React from "react";
import Style from "./ConfirmationField.module.scss";

const ConfirmationField = ({ field, value, image, customComponent }) => {
  return (
    <div className={Style.confirmationWrapper}>
      <div className={Style.fieldTitle}>{field}</div>
      <div className={Style.fieldValue}>{value}</div>
      {image && <img className={Style.fieldImage} src={image} alt="opportunity preview" />}
      {customComponent && customComponent}
    </div>
  );
};

export default ConfirmationField;
