import React, { useEffect } from "react";
import { faEdit } from "@fortawesome/pro-solid-svg-icons";

import { useSelector } from "react-redux";
import { Col } from "react-bootstrap";
import { formatDate } from "../../../../helpers/GeneralHelper";
import getFoundGamesHook from "../../../../hooks/getHooks/getFoundGamesHook";
import EFCircleIconButtonTooltip from "../../../Buttons/EFCircleIconButtonTooltip/EFCircleIconButtonTooltip";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";
import EFGameCardList from "../../../EFGameCardList/EFGameCardList";
import { EFHtmlParser } from "../../../EFHtmlParser/EFHtmlParser";
import CandidateQuestionsList from "../AdditionalRequirementsTemplate/CandidateQuestionsList/CandidateQuestionsList";
import ConfirmationField from "./ConfirmationField/ConfirmationField";
import Style from "./FinalConfirmation.module.scss";
import InfoText from "../../../InfoText/InfoText";

const FinalConfirmation = ({ opportunity, imagePreview, onSubmit, editGeneralInfo, editAdditionalRequirements }) => {
  const gamesList = getFoundGamesHook(opportunity.game);
  const requiredFields = useSelector(state => state.opportunities.requiredFormFields);
  const requirements = Object.keys(opportunity?.requirements).filter(it => opportunity?.requirements[it]);

  useEffect(() => {
    analytics.track("OPPORTUNITY_CREATION_CONFIRM_STEP");
  }, []);

  return (
    <div className={Style.finalConfirmationWrapper}>
      <div className={Style.sectionContainer}>
        <div className={Style.editButtonWrapper} onClick={editGeneralInfo}>
          <EFCircleIconButtonTooltip
            colorTheme="light"
            size="medium"
            icon={faEdit}
            shadowTheme="small"
            tooltipContent="Edit"
          />
        </div>
        <h2 className={Style.sectionTitle}>General Information</h2>
        {opportunity?.title && <ConfirmationField field="Title" value={opportunity.title} />}
        {opportunity?.opportunityType && (
          <ConfirmationField
            field="Opportunity Type"
            value={`${opportunity.opportunityType} | ${opportunity?.subType}`}
          />
        )}
        {opportunity?.startDate && <ConfirmationField field="Start Date" value={formatDate(opportunity.startDate)} />}
        {opportunity?.dueDate && <ConfirmationField field="End Date" value={formatDate(opportunity.dueDate)} />}
        {opportunity?.applicationResponseDate && (
          <ConfirmationField
            field="Expected Application Response Date"
            value={formatDate(opportunity.applicationResponseDate)}
          />
        )}
        {opportunity?.followUpInstructions && (
          <ConfirmationField field="Application Follow-up Instructions" value={opportunity.followUpInstructions} />
        )}
        {opportunity?.link && <ConfirmationField field="Application URL" value={opportunity.link} />}
        {opportunity?.openings && <ConfirmationField field="Openings" value={opportunity.openings} />}
        {opportunity?.moneyIncluded && <ConfirmationField field="Money Included" value={opportunity.moneyIncluded} />}
        {opportunity?.location && <ConfirmationField field="Location" value={opportunity.location} />}
        {imagePreview && <ConfirmationField field="Attached Image" image={imagePreview} />}
        {opportunity?.game?.length > 0 && (
          <ConfirmationField field="Games" customComponent={<EFGameCardList games={gamesList} />} />
        )}
        <ConfirmationField field="Description" value={<EFHtmlParser>{opportunity.description}</EFHtmlParser>} />
      </div>
      {(opportunity?.candidateQuestions?.length > 0 || requirements.length > 0) && (
        <div className={Style.sectionContainer}>
          <div className={Style.editButtonWrapper} onClick={editAdditionalRequirements}>
            <EFCircleIconButtonTooltip
              colorTheme="light"
              size="medium"
              icon={faEdit}
              shadowTheme="small"
              tooltipContent="Edit"
            />
          </div>
          <h2 className={Style.sectionTitle}>Additional Requirements</h2>

          <div>
            {requirements?.map(fieldId => {
              const field = requiredFields.find(it => it._id === fieldId);
              return (
                <Col sm={12} key={fieldId}>
                  <div className={Style.requiredFieldsText}>
                    {field?.name}
                    {field?.description && <InfoText instructions={field?.description} label="" />}
                  </div>
                </Col>
              );
            })}
          </div>

          {opportunity?.candidateQuestions?.length > 0 && (
            <div className={Style.candidateQuestionsContainer}>
              <CandidateQuestionsList candidateQuestions={opportunity.candidateQuestions} />
            </div>
          )}
        </div>
      )}
      <EFRectangleButton
        onClick={onSubmit}
        width="fullWidth"
        size="extraLarge"
        shadowTheme="medium"
        colorTheme="secondary"
        text="Complete Opportunity"
      />
    </div>
  );
};

export default FinalConfirmation;
