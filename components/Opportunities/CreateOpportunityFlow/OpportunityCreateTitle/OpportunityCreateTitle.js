import React from "react";
import { faChevronLeft } from "@fortawesome/pro-solid-svg-icons";

import Style from "./OpportunityCreateTitle.module.scss";
import EFCircleIconButton from "../../../Buttons/EFCircleIconButton/EFCircleIconButton";

const OpportunityCreateTitle = ({ heading, subHeading, canGoBack, goBack }) => {
  return (
    <div className={Style.titleWrapper}>
      {canGoBack && (
        <div className={Style.buttonWrapper}>
          <EFCircleIconButton
            onClick={goBack}
            icon={faChevronLeft}
            colorTheme="light"
            shadowTheme="small"
            size="small"
          />
        </div>
      )}
      <div className={Style.opportunityTitle}>{heading}</div>
      <div className={Style.opportunitySubtitle}>{subHeading}</div>
    </div>
  );
};
export default OpportunityCreateTitle;
