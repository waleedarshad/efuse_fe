import React from "react";
import { Col } from "react-bootstrap";
import { useSelector } from "react-redux";
import FeatureFlag from "../../../General/FeatureFlag/FeatureFlag";
import FeatureFlagVariant from "../../../General/FeatureFlag/FeatureFlagVariant/FeatureFlagVariant";
import InfoText from "../../../InfoText/InfoText";
import InputRow from "../../../InputRow/InputRow";
import Style from "./AdditionalRequirementsTemplate.module.scss";
import EFRequiredFieldsList from "../../../FormControls/RequiredFieldsList/EFRequiredFieldsList";
import EFInputControl from "../../../FormControls/EFInputControl";
import AddCandidateQuestions from "./CandidateQuestionsList/AddCandidateQuestions";

const AdditionalRequirementsFields = ({ control, errors, candidateQuestions, setCandidateQuestions }) => {
  const requiredFields = useSelector(state => state.opportunities.requiredFormFields);

  const addQuestion = data => setCandidateQuestions(oldValue => [...oldValue, data]);
  const removeQuestion = data =>
    setCandidateQuestions(oldValue => oldValue.filter(it => it?.question !== data?.question));

  return (
    <>
      <FeatureFlag name="opportunity_application_cost">
        <FeatureFlagVariant flagState>
          <InputRow>
            <Col sm={12} className="m-lg-auto">
              <EFInputControl
                label="Application Cost"
                placeholder="Payment Required to Apply..."
                type="number"
                name="applicationCost"
                control={control}
                errors={errors}
                validation={value => value >= 1 || "Application cost must be greater than 1."}
                step={0.01}
              />
            </Col>
          </InputRow>
        </FeatureFlagVariant>
      </FeatureFlag>
      {requiredFields?.length > 0 && (
        <>
          <div className={Style.sectionTitle}>
            Portfolio Requirements
            <div className={Style.infoTextWrapper}>
              <InfoText
                instructions="By enabling an option, applicants will be required to add the corresponding information prior to applying."
                label=""
              />
            </div>
          </div>
          <InputRow>
            <EFRequiredFieldsList
              requiredFields={requiredFields}
              disabled={false}
              control={control}
              name="requirements"
            />
          </InputRow>
        </>
      )}
      <div className={Style.sectionTitle}>
        Custom Questions
        <div className={Style.infoTextWrapper}>
          <InfoText
            instructions="Applicants will be required to answer these questions during the application process."
            label=""
          />
        </div>
      </div>
      <AddCandidateQuestions
        candidateQuestions={candidateQuestions}
        addQuestion={addQuestion}
        removeQuestion={removeQuestion}
      />
      <br />
    </>
  );
};

export default AdditionalRequirementsFields;
