import React, { useEffect } from "react";
import { Form } from "react-bootstrap";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";
import AdditionalRequirementsFields from "./AdditionalRequirementsFields";

const AdditionalRequirementsTemplate = ({ form, candidateQuestions, setCandidateQuestions, onSubmit }) => {
  useEffect(() => {
    analytics.track("OPPORTUNITY_CREATION_REQUIREMENTS_STEP");
  }, []);

  const {
    control,
    handleSubmit,
    formState: { errors }
  } = form;

  return (
    <Form>
      <AdditionalRequirementsFields
        candidateQuestions={candidateQuestions}
        setCandidateQuestions={setCandidateQuestions}
        control={control}
        errors={errors}
      />
      <EFRectangleButton
        text="Next Step"
        colorTheme="light"
        size="extraLarge"
        width="fullWidth"
        buttonType="button"
        shadowTheme="medium"
        onClick={handleSubmit(data => onSubmit({ ...data, candidateQuestions }))}
      />
    </Form>
  );
};

export default AdditionalRequirementsTemplate;
