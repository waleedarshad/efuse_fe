import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAlignLeft, faFileAlt, faTrash } from "@fortawesome/pro-solid-svg-icons";
import Style from "./CandidateQuestionsList.module.scss";

const CandidateQuestionsList = ({ candidateQuestions, removeQuestion }) => {
  return (
    <>
      {(candidateQuestions || []).map((item, index) => {
        return (
          <div className={Style.questionWrapper} key={item.question}>
            {item?.inputType && (
              <p className={Style.questionTitle}>
                {item?.inputType === "textInput" ? (
                  <FontAwesomeIcon icon={faAlignLeft} className={Style.icon} />
                ) : (
                  <FontAwesomeIcon icon={faFileAlt} className={Style.icon} />
                )}
                {item?.inputType === "textInput" ? "Text Input" : "File Input"}
              </p>
            )}
            <div className={Style.questionContainer}>
              {index + 1}.<span className={Style.questionText}>{item?.question}</span>
              {removeQuestion && (
                <FontAwesomeIcon onClick={() => removeQuestion(item)} icon={faTrash} className={Style.trashIcon} />
              )}
            </div>
          </div>
        );
      })}
    </>
  );
};

export default CandidateQuestionsList;
