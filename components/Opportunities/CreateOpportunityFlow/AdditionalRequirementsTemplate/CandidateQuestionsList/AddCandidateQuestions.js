import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus } from "@fortawesome/pro-solid-svg-icons";
import { useForm } from "react-hook-form";
import { Col } from "react-bootstrap";
import Style from "./CandidateQuestionsList.module.scss";
import InputRow from "../../../../InputRow/InputRow";
import EFInputControl from "../../../../FormControls/EFInputControl";
import EFSelectControl from "../../../../FormControls/EFSelectControl";
import CandidateQuestionsList from "./CandidateQuestionsList";

const AddCandidateQuestions = ({ candidateQuestions, removeQuestion, addQuestion }) => {
  const {
    control,
    handleSubmit,
    watch,
    reset,
    formState: { errors }
  } = useForm();
  const question = watch("question");

  const addAndReset = data => {
    addQuestion(data);
    reset({ question: "", inputType: "" });
  };

  return (
    <>
      <InputRow>
        <Col sm={8}>
          <EFInputControl
            placeholder="Add Custom Question..."
            name="question"
            required
            control={control}
            errors={errors}
          />
        </Col>
        <Col sm={4}>
          <EFSelectControl
            name="inputType"
            options={[
              { label: "Select input type", value: "" },
              { label: "Text Input", value: "textInput" },
              { label: "File Upload", value: "fileUpload" }
            ]}
            required
            control={control}
            errors={errors}
          />
        </Col>
      </InputRow>
      <CandidateQuestionsList candidateQuestions={candidateQuestions} removeQuestion={removeQuestion} />
      {question && (
        <div className={Style.addButtonContainer}>
          <button type="submit" className={Style.addButton} onClick={handleSubmit(addAndReset)}>
            <FontAwesomeIcon className={Style.plusIcon} icon={faPlus} />
            Add Question
          </button>
        </div>
      )}
    </>
  );
};

export default AddCandidateQuestions;
