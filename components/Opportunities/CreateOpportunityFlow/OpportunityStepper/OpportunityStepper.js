import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Style from "./OpportunityStepper.module.scss";

const OpportunityStepper = ({ steps, currentStep, switchStep }) => {
  return (
    <div className={Style.positionWrapper}>
      {steps.map((step, index) => {
        const isActive = currentStep >= index;
        const isNextStepActive = currentStep > index;
        return (
          <div key={`step-${step.title}`} className={Style.positionItem}>
            <div className={`${Style.leftLine} ${isActive && Style.blueBackground}`} />
            <button
              type="button"
              disabled={!isActive}
              className={`${Style.iconWrapper} ${isActive && Style.blueBackground}`}
              onClick={() => isActive && switchStep(index)}
            >
              <FontAwesomeIcon icon={step.icon} />
            </button>
            <div className={`${Style.rightLine} ${isNextStepActive && Style.blueBackground}`} />
            <p className={`${Style.positionTitle} ${isActive && Style.darkPositionText}`}>{step.title}</p>
          </div>
        );
      })}
    </div>
  );
};

export default OpportunityStepper;
