import { withRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { faClipboardCheck, faFileAlt, faHandPointer, faTasks } from "@fortawesome/pro-solid-svg-icons";
import { useForm } from "react-hook-form";
import {
  createOpportunity,
  getOpportunityRequiredFields,
  getOpportunityToEdit,
  updateOpportunity
} from "../../../store/actions/opportunityActions";
import { getPostableOrganizations } from "../../../store/actions/userActions";
import TwoColumnLayout from "../../Layouts/TwoColumnLayout";
import AdditionalRequirementsTemplate from "./AdditionalRequirementsTemplate/AdditionalRequirementsTemplate";
import Style from "./CreateOpportunityFlow.module.scss";
import EditOpportunity from "./EditOpportunity/EditOpportunity";
import FinalConfirmation from "./FinalConfirmation/FinalConfirmation";
import OpportunityStepper from "./OpportunityStepper/OpportunityStepper";
import GeneralOpportunityInfoTemplate from "./GeneralOpportunityInfoTemplate/GeneralOpportunityInfoTemplate";
import OpportunityCreateTitle from "./OpportunityCreateTitle/OpportunityCreateTitle";
import SelectTypeTemplate from "./SelectTypeTemplate/SelectTypeTemplate";
import { toggleCropModal } from "../../../store/actions/settingsActions";

const CreateOpportunityFlow = ({ router, isWindowView }) => {
  const dispatch = useDispatch();

  const [currentStep, setCurrentStep] = useState(0);
  const [imagePreview, setImagePreview] = useState();

  const existingOpportunity = useSelector(state => state.opportunities.opportunity);
  const currentUser = useSelector(state => state.auth.currentUser);
  const isEditing = !!router.query.id;

  const [opportunity, setOpportunity] = useState({});
  const [candidateQuestions, setCandidateQuestions] = useState(existingOpportunity.candidateQuestions || []);

  const parseExistingOpportunity = existing => {
    return {
      ...existing,
      requirements: existing.requirements?.reduce((acc, current) => ({ ...acc, [current]: true }), {}),
      game: existing?.associatedGame?._id,
      owner: existing?.onBehalfOfOrganization ? existing?.organization : existing?.user
    };
  };

  const generalForm = useForm(opportunity);
  const additionalRequirementsForm = useForm(opportunity);

  useEffect(() => {
    analytics.page("Opportunities New");
    if (isEditing) {
      dispatch(getOpportunityToEdit(router.query.id, router));
    }
    dispatch(getPostableOrganizations(1, 999));
    dispatch(getOpportunityRequiredFields());
  }, []);

  const canGoBack = currentStep > 0;
  const goBack = () => setCurrentStep(step => step - 1);
  const next = () => setCurrentStep(step => step + 1);

  const onSubmitPage = data => {
    setOpportunity(oldOpportunity => ({ ...oldOpportunity, ...data }));
    next();
  };

  const onSubmit = async () => {
    const formattedData = {
      ...opportunity,
      requirements: JSON.stringify(
        Object.keys(opportunity?.requirements).filter(key => opportunity?.requirements[key])
      ),
      candidateQuestions: JSON.stringify(candidateQuestions),
      self: opportunity.owner === currentUser?._id ? currentUser?.name : undefined,
      onBehalfOfOrganization: opportunity.owner !== currentUser?._id,
      associatedOrganization: opportunity.owner !== currentUser?._id ? opportunity.owner : undefined,
      organization: opportunity.owner !== currentUser?._id ? opportunity.owner : undefined
    };

    // The back end cannot handle an empty game field if the ey exists.
    if (!formattedData.game) {
      delete formattedData.game;
    }

    const formData = new FormData();
    Object.keys(formattedData).forEach(key => {
      formData.append(key, formattedData[key]);
    });

    if (isEditing) {
      dispatch(updateOpportunity(router.query.id, formData, router));
    } else {
      dispatch(createOpportunity(formData, router));
    }
  };

  const steps = {
    SELECT_TYPE: {
      stepper: {
        icon: faHandPointer,
        title: "Select Type"
      },
      heading: "Opportunity",
      subHeading: "Choose your opportunity type",
      content: (
        <SelectTypeTemplate
          selectType={type => {
            setOpportunity(oldOpportunity => ({ ...oldOpportunity, opportunityType: type }));
            next();
          }}
        />
      )
    },
    GENERAL: {
      stepper: {
        icon: faFileAlt,
        title: "General"
      },
      heading: opportunity.opportunityType,
      subHeading: "Add general information to your opportunity",
      content: (
        <GeneralOpportunityInfoTemplate
          toggleCropModal={(val, name) => dispatch(toggleCropModal(val, name))}
          onPreviewChange={setImagePreview}
          existingOpportunity={opportunity}
          opportunityType={opportunity.opportunityType}
          form={generalForm}
          onSubmit={onSubmitPage}
        />
      )
    },
    REQUIREMENTS: {
      stepper: {
        icon: faTasks,
        title: "Requirements"
      },
      heading: `Create ${opportunity.opportunityType}`,
      subHeading: "Add and select requirements to apply",
      content: (
        <AdditionalRequirementsTemplate
          form={additionalRequirementsForm}
          candidateQuestions={candidateQuestions}
          setCandidateQuestions={setCandidateQuestions}
          onSubmit={onSubmitPage}
          existingOpportunity={opportunity}
        />
      )
    },
    CONFIRMATION: {
      stepper: {
        icon: faClipboardCheck,
        title: "Confirm"
      },
      heading: `Confirm ${opportunity.opportunityType}`,
      subHeading: "Confirm the information for your opportunity",
      content: (
        <FinalConfirmation
          imagePreview={imagePreview}
          opportunity={opportunity}
          editGeneralInfo={() => setCurrentStep(isEditing ? 0 : 1)}
          editAdditionalRequirements={() => setCurrentStep(isEditing ? 0 : 2)}
          onSubmit={onSubmit}
        />
      )
    },
    EDIT: {
      heading: "Edit",
      subHeading: "Make changes to your opportunity",
      content: (
        <EditOpportunity
          existingOpportunity={parseExistingOpportunity(existingOpportunity)}
          onSubmit={onSubmitPage}
          candidateQuestions={candidateQuestions}
          setCandidateQuestions={setCandidateQuestions}
          toggleCropModal={(val, name) => dispatch(toggleCropModal(val, name))}
          onPreviewChange={setImagePreview}
        />
      )
    }
  };

  const flow = isEditing
    ? [steps.EDIT, steps.CONFIRMATION]
    : [steps.SELECT_TYPE, steps.GENERAL, steps.REQUIREMENTS, steps.CONFIRMATION];

  return (
    <TwoColumnLayout metaTitle="eFuse | Create Opportunity" isWindowView={isWindowView} subHeader>
      <div className={Style.createWrapper}>
        <OpportunityCreateTitle
          heading={flow[currentStep]?.heading}
          subHeading={flow[currentStep]?.subHeading}
          canGoBack={canGoBack}
          goBack={goBack}
        />
        {!isEditing && (
          <OpportunityStepper
            switchStep={setCurrentStep}
            currentStep={currentStep}
            steps={flow.map(step => step.stepper)}
          />
        )}
        <div className={Style.formWrapper}>{flow[currentStep]?.content}</div>
      </div>
    </TwoColumnLayout>
  );
};

export default withRouter(CreateOpportunityFlow);
