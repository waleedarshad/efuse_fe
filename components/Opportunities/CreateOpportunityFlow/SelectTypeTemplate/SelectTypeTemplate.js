import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { OPPORTUNITY_TYPES } from "../../../../common/opportunities";
import Style from "./SelectTypeTemplate.module.scss";

const SelectTypeTemplate = ({ selectType }) => {
  const displayGiveaway = useSelector(state => state.features.opportunity_creation_giveaway_type);

  const list = [
    {
      title: "Create Scholarship",
      image: "https://cdn.efuse.gg/uploads/static/opportunity-types/scholarshipType.png",
      type: OPPORTUNITY_TYPES.SCHOLARSHIP
    },
    {
      title: "Create Job",
      image: "https://cdn.efuse.gg/uploads/static/opportunity-types/jobType.png",
      type: OPPORTUNITY_TYPES.JOB
    },
    {
      title: "Create Event",
      image: "https://cdn.efuse.gg/uploads/static/opportunity-types/tournamentType.png",
      type: OPPORTUNITY_TYPES.EVENT
    },
    {
      title: "Create Team Opening",
      image: "https://cdn.efuse.gg/uploads/static/opportunity-types/teamOpeningType.png",
      type: OPPORTUNITY_TYPES.TEAM_OPENING
    }
  ];

  if (displayGiveaway) {
    list.push({
      title: "Create Giveaway",
      image: "https://cdn.efuse.gg/uploads/static/opportunity-types/giveawayType.png",
      type: "Giveaway"
    });
  }

  useEffect(() => {
    analytics.track("OPPORTUNITY_CREATION_SELECT_TYPE_STEP");
  }, []);

  return (
    <>
      {list.map(item => {
        return (
          <div className={Style.typeCard} key={item.title} onClick={() => selectType(item.type)}>
            <p className={Style.typeTitle}>{item.title}</p>
            <img className={Style.typeImage} src={item.image} />
          </div>
        );
      })}
    </>
  );
};

export default SelectTypeTemplate;
