import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheckCircle, faQuestionCircle, faMinusCircle } from "@fortawesome/pro-solid-svg-icons";

import EFAlert from "../../../../EFAlert/EFAlert";
import Style from "./ApplicationStatus.module.scss";
import EFRectangleButton from "../../../../Buttons/EFRectangleButton/EFRectangleButton";

const ApplicationStatus = ({ clickHandler, status, applicantName, applicant }) => {
  const confirmSendEmail = () => {
    const title = "Confirm";
    const message = `Would you like to send an email notification to ${applicantName} ?`;
    const confirmLabel = "Yes, send email";
    const cancelLabel = "No, do not send email";

    const onConfirm = () => {
      clickHandler("accepted", true);
    };

    const onCancel = () => {
      clickHandler("accepted");
    };

    EFAlert(title, message, confirmLabel, cancelLabel, onConfirm, onCancel);
  };

  return (
    <div className={Style.topIconContainer}>
      <div className="d-inline-block">
        <EFRectangleButton
          text={<FontAwesomeIcon className={`${Style.topIcon} ${Style.accepted}`} icon={faCheckCircle} />}
          onClick={() => confirmSendEmail()}
          disabled={status === "accepted"}
          colorTheme="transparent"
          shadowTheme="none"
        />
      </div>
      <div className="d-inline-block">
        <EFRectangleButton
          text={<FontAwesomeIcon className={`${Style.topIcon} ${Style.interested}`} icon={faQuestionCircle} />}
          onClick={() => clickHandler("undecided")}
          disabled={status === "undecided"}
          colorTheme="transparent"
          shadowTheme="none"
        />
      </div>
      <div className="d-inline-block">
        <EFRectangleButton
          text={<FontAwesomeIcon className={`${Style.topIcon} ${Style.remove}`} icon={faMinusCircle} />}
          onClick={() => clickHandler("declined")}
          disabled={status === "declined"}
          colorTheme="transparent"
          shadowTheme="none"
        />
      </div>
    </div>
  );
};
export default ApplicationStatus;
