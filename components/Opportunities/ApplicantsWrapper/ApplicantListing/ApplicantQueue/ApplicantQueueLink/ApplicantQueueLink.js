import Link from "next/link";

import { conditionalActive } from "../../../../../../helpers/GeneralHelper";

const ApplicantQueueLink = ({ id, activeTab, currentTab, title, tabStyle, activeClass, onClick }) => (
  <Link
    href={{
      pathname: "/opportunities/applicants",
      query: { id, type: currentTab }
    }}
  >
    <button
      className={`${tabStyle} ${conditionalActive(activeTab, currentTab, activeClass)}`}
      type="button"
      onClick={onClick}
    >
      {title}
    </button>
  </Link>
);

export default ApplicantQueueLink;
