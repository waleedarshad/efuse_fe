import Style from "./ApplicantQueue.module.scss";
import ApplicantQueueLink from "./ApplicantQueueLink/ApplicantQueueLink";

const ApplicantQueue = ({ id, type }) => {
  const activeClass = Style.active;
  return (
    <div className={Style.actionContainer}>
      <ApplicantQueueLink
        id={id}
        activeTab={type}
        currentTab="noaction"
        title="No Assignment"
        tabStyle={Style.actionBtn}
        activeClass={activeClass}
        onClick={() => analytics.track("OPPORTUNITY_APPLICANT_NO_ASSIGNMENT_TAB_CLICK")}
      />
      <ApplicantQueueLink
        id={id}
        activeTab={type}
        currentTab="accepted"
        title="Accepted"
        tabStyle={`${Style.actionBtn} ${Style.accepted}`}
        activeClass={activeClass}
        onClick={() => analytics.track("OPPORTUNITY_APPLICANT_ACCEPTED_TAB_CLICK")}
      />
      <ApplicantQueueLink
        id={id}
        activeTab={type}
        currentTab="undecided"
        title="Undecided"
        tabStyle={`${Style.actionBtn} ${Style.interested}`}
        activeClass={activeClass}
        onClick={() => analytics.track("OPPORTUNITY_APPLICANT_UNDECIDED_TAB_CLICK")}
      />
      <ApplicantQueueLink
        id={id}
        activeTab={type}
        currentTab="declined"
        title="Declined"
        tabStyle={`${Style.actionBtn} ${Style.declined}`}
        activeClass={activeClass}
        onClick={() => analytics.track("OPPORTUNITY_APPLICANT_DECLINED_TAB_CLICK")}
      />
    </div>
  );
};

export default ApplicantQueue;
