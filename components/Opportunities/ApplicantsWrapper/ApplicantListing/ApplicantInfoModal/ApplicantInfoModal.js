import React from "react";
import { Row, Col } from "react-bootstrap";
import LabelValue from "../../../../LabelValue";
import { formatDate, rescueNil } from "../../../../../helpers/GeneralHelper";
import Hr from "../../../../Hr/Hr";
import EFPrimaryModal from "../../../../Modals/EFPrimaryModal/EFPrimaryModal";

const ApplicantInfoModal = ({ user, show, onHide }) => {
  return (
    <EFPrimaryModal
      title={user.name}
      widthTheme="medium"
      isOpen={show}
      onClose={onHide}
      allowBackgroundClickClose={false}
    >
      <Row>
        <Col>
          <h4>General Account Information</h4>
        </Col>
      </Row>
      <Hr />
      <Row>
        <Col>
          <LabelValue label="Email Address" value={user.email} />
        </Col>
      </Row>
      <Row>
        <Col>
          <LabelValue label="Date Of Birth" value={formatDate(user.dateOfBirth)} />
        </Col>
      </Row>
      <Row>
        <Col>
          <LabelValue label="Location" value={user.address} />
        </Col>
      </Row>
      <Row>
        <Col>
          <LabelValue label="Zip code" value={user.zipCode} />
        </Col>
      </Row>
      <Row>
        <Col>
          <LabelValue label="Gender" value={user.gender} />
        </Col>
      </Row>
      <Row>
        <Col>
          <LabelValue label="Bio" value={user.bio} />
        </Col>
      </Row>
      {user.school && user.educationLevel && (
        <>
          <Row>
            <Col>
              <h4>Student Information</h4>
            </Col>
          </Row>
          <Hr />
          <Row>
            <Col>
              <LabelValue label="Education Level" value={user.educationLevel} />
            </Col>
          </Row>
          <Row>
            <Col>
              <LabelValue label="School" value={user.school} />
            </Col>
          </Row>
          <Row>
            <Col>
              <LabelValue label="Expected Graduation Year" value={user.graduationYear} />
            </Col>
          </Row>
          <Row>
            <Col>
              <LabelValue label="Major" value={user.major} />
            </Col>
          </Row>
          <Row>
            <Col>
              <LabelValue label="ACT" value={user.act} />
            </Col>
          </Row>
          <Row>
            <Col>
              <LabelValue label="SAT" value={user.sat} />
            </Col>
          </Row>
          <Row>
            <Col>
              <LabelValue label="GPA" value={user.gpa} />
            </Col>
          </Row>
        </>
      )}
      {user.title && (
        <>
          <Row>
            <Col>
              <h4>Professional Information</h4>
            </Col>
          </Row>
          <Hr />
          <Row>
            <Col>
              <LabelValue label="Title" value={user.title} />
            </Col>
          </Row>
          <Row>
            <Col>
              <LabelValue label="Phone Number" value={user.phoneNumber} />
            </Col>
          </Row>
          <Row>
            <Col>
              <LabelValue label="Organization" value={rescueNil(user.mainOrganization, "name")} />
            </Col>
          </Row>
          <Row>
            <Col>
              <LabelValue label="Resume" value={rescueNil(user.resume, "url")} />
            </Col>
          </Row>
          <Row>
            <Col>
              <LabelValue label="Interest" value={user.interest} />
            </Col>
          </Row>
        </>
      )}
    </EFPrimaryModal>
  );
};

export default ApplicantInfoModal;
