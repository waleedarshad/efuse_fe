import { Col } from "react-bootstrap";
import Style from "./ApplicantListItem.module.scss";
import UserLink from "../../../../UserLink/UserLink";
import { rescueNil, getImage, formatRoles, userLocation, formatDate } from "../../../../../helpers/GeneralHelper";
import EFAvatar from "../../../../EFAvatar/EFAvatar";
import DisplayLocation from "../../../../DisplayLocation/DisplayLocation";
import ApplicantInfoModal from "../ApplicantInfoModal/ApplicantInfoModal";
import EFRectangleButton from "../../../../Buttons/EFRectangleButton/EFRectangleButton";

const ApplicantListItem = ({ user, t }) => {
  const [modalShow, setModalShow] = React.useState(false);
  const profilePicture = rescueNil(user, "profilePicture", {});
  return (
    <Col lg={4} md={8}>
      <div className={Style.friendWrapper}>
        <EFAvatar
          displayOnlineButton={false}
          profilePicture={getImage(profilePicture, "avatar")}
          size="small"
          href={`/u/${user?.username}`}
        />

        <h5 className={Style.name}>
          <UserLink user={user} />{" "}
        </h5>
        <p className={Style.roles}>{formatRoles(user)}</p>
        <div className={Style.separator} />
        <DisplayLocation location={userLocation(user)} />
        <p className={Style.appliedOn}>Applied: {formatDate(user.applicant.createdAt)}</p>
        <EFRectangleButton onClick={() => setModalShow(true)} text="Details" />
        <ApplicantInfoModal show={modalShow} onHide={() => setModalShow(false)} user={user} t={t} />
      </div>
    </Col>
  );
};

export default ApplicantListItem;
