import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { faSearchPlus, faSchool, faComments } from "@fortawesome/pro-solid-svg-icons";

import EFCircleIconButtonTooltip from "../../../../../Buttons/EFCircleIconButtonTooltip/EFCircleIconButtonTooltip";
import { togglePortfolioModal } from "../../../../../../store/actions/portfolioActions";
import { startChatWith, clearChat } from "../../../../../../store/actions/messageActions";
import Style from "./TopButtons.module.scss";
import ViewEducation from "../ViewEducation/ViewEducation";
import MessageApplicant from "../MessageApplicant/MessageApplicant";
import ViewQuestions from "../ViewQuestions/ViewQuestions";
import StarApplicant from "./StarApplicant/StarApplicant";

const TopButtons = ({ user, applicant, opportunity }) => {
  const dispatch = useDispatch();
  const authUser = useSelector(state => state.auth.currentUser);
  const modal = useSelector(state => state.portfolio.modal);
  const modalSection = useSelector(state => state.portfolio.modalSection);

  let hasEducation = false;
  let hasQuestions = false;

  if (user.educationExperience) {
    if (user.educationExperience.length > 0) {
      hasEducation = true;
    }
  }

  if (opportunity.candidateQuestions) {
    if (opportunity.candidateQuestions.length > 0) {
      hasQuestions = true;
    }
  }

  const loadEducation = id => {
    dispatch(togglePortfolioModal(true, `education${id}`));
  };

  const loadMessenger = (applicantId, userId, name) => {
    dispatch(startChatWith(userId, name, authUser));
    dispatch(togglePortfolioModal(true, `messenger${applicantId}`));

    analytics.track("OPPORTUNITY_APPLICANT_MESSAGE_APPLICANT", {
      applicant: userId
    });
  };

  const loadQuestionsModal = id => {
    dispatch(togglePortfolioModal(true, `questions${id}`));
  };

  const onHide = (type = "") => {
    if (type === "messenger") dispatch(clearChat());
    dispatch(togglePortfolioModal(false));
  };

  return (
    <div>
      {hasEducation && (
        <div className={Style.buttonWrapper}>
          <EFCircleIconButtonTooltip
            tooltipContent="View Education"
            tooltipPlacement="top"
            onClick={() => loadEducation(applicant._id)}
            icon={faSchool}
          />
        </div>
      )}
      <div className={Style.buttonWrapper}>
        <EFCircleIconButtonTooltip
          tooltipContent="Message"
          tooltipPlacement="top"
          onClick={() => loadMessenger(applicant._id, user._id, user.username ? user.username : "username")}
          icon={faComments}
        />
      </div>
      {hasQuestions && (
        <div className={Style.buttonWrapper}>
          <EFCircleIconButtonTooltip
            tooltipContent="View More"
            tooltipPlacement="top"
            onClick={() => loadQuestionsModal(applicant._id)}
            icon={faSearchPlus}
          />
        </div>
      )}

      <div className={Style.buttonWrapper}>
        <StarApplicant applicant={applicant} recruiterId={authUser?._id} />
      </div>

      {modal && modalSection === `education${applicant._id}` && (
        <ViewEducation show={modal} onHide={onHide} user={user} />
      )}
      {modal && modalSection === `messenger${applicant._id}` && (
        <MessageApplicant show={modal} onHide={onHide} userName={user.name} />
      )}
      {modal && modalSection === `questions${applicant._id}` && (
        <ViewQuestions show={modal} onHide={onHide} applicant={applicant} opportunity={opportunity} user={user} />
      )}
    </div>
  );
};
export default TopButtons;
