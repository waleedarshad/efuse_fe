import React from "react";
import { useDispatch } from "react-redux";
import { faStar } from "@fortawesome/pro-solid-svg-icons";
import { faStar as falStar } from "@fortawesome/pro-light-svg-icons";
import { markRecruitStarred, deleteStarredRecruit } from "../../../../../../../store/actions/recruitingActions";
import EFTooltip from "../../../../../../tooltip/EFTooltip/EFTooltip";
import EFCircleIconButton from "../../../../../../Buttons/EFCircleIconButton/EFCircleIconButton";

const StarApplicant = ({ applicant, recruiterId }) => {
  const dispatch = useDispatch();

  const markStarred = applicantId => {
    analytics.track("OPPORTUNITY_STARRED_PROSPECT", {
      prospect: applicant.user._id
    });

    const starData = {
      creator: recruiterId,
      relatedDoc: applicantId,
      relatedModel: "applicants",
      owner: recruiterId,
      ownerType: "users"
    };

    dispatch(markRecruitStarred(starData, "applicant"));
  };

  const deleteStarred = applicantId => {
    analytics.track("OPPORTUNITY_UNSTARRED_PROSPECT", {
      prospect: applicant.user._id
    });

    dispatch(deleteStarredRecruit(recruiterId, applicantId, "applicant"));
  };
  return (
    <EFTooltip tooltipContent="Star for Later" tooltipPlacement="top">
      <EFCircleIconButton
        icon={applicant.favorite ? faStar : falStar}
        shadowTheme="small"
        onClick={() => {
          applicant.favorite ? deleteStarred(applicant._id) : markStarred(applicant._id);
        }}
      />
    </EFTooltip>
  );
};

export default StarApplicant;
