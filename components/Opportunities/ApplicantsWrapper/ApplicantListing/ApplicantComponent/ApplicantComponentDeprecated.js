import React, { Component } from "react";
import { connect } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheckCircle, faSearchPlus, faSchool } from "@fortawesome/pro-solid-svg-icons";
import { faEnvelope } from "@fortawesome/pro-light-svg-icons";

import { Col } from "react-bootstrap";
import Link from "next/link";
import Style from "./ApplicantComponent.module.scss";

import SocialLinksIcons from "../../../../Layouts/BioCardLayout/SocialLinksIcons/SocialLinksIcons";
import ApplicationStatus from "../ApplicationStatus/ApplicationStatus";
import { togglePortfolioModal } from "../../../../../store/actions/portfolioActions";
import { updateApplicantStatus } from "../../../../../store/actions/applicantActions";
import ViewQuestions from "./ViewQuestions/ViewQuestions";
import ViewEducation from "./ViewEducation/ViewEducation";
import MessageApplicant from "./MessageApplicant/MessageApplicant";
import DisplayLocation from "../../../../DisplayLocation/DisplayLocation";
import { getChatChannels, startChatWith, clearChat } from "../../../../../store/actions/messageActions";

class ApplicantComponentDeprecated extends Component {
  updateStatus = (status, sendEmail = false) => {
    const { applicant, filterType } = this.props;
    const { opportunity, _id } = applicant;

    let eventName = "OPPORTUNITY_APPLICANT_ACCEPTED_APPLICANT";
    if (status === "undecided") {
      eventName = "OPPORTUNITY_APPLICANT_UNDECIDED_APPLICANT";
    } else if (status === "declined") {
      eventName = "OPPORTUNITY_APPLICANT_REJECTED_APPLICANT";
    }
    analytics.track(eventName, { applicant: _id });

    this.props.updateApplicantStatus(opportunity, _id, status, sendEmail, filterType);
  };

  loadModal = id => {
    this.props.togglePortfolioModal(true, `questions${id}`);
  };

  onHide = (type = "") => {
    if (type === "messenger") this.props.clearChat();
    this.props.togglePortfolioModal(false);
    this.setState({ edit: false, event: null });
  };

  loadEducation = id => {
    this.props.togglePortfolioModal(true, `education${id}`);
  };

  loadMessenger = (applicantId, userId, name) => {
    const { togglePortfolioModal, startChatWith, currentUser } = this.props;
    startChatWith(userId, name, currentUser);
    togglePortfolioModal(true, `messenger${applicantId}`);

    analytics.track("OPPORTUNITY_APPLICANT_MESSAGE_APPLICANT", {
      applicant: userId
    });
  };

  render() {
    const { user, applicant, modal, modalSection, opportunity, currentUser } = this.props;
    let hasQuestions = false;
    let hasEducation = false;
    const isOwner = currentUser && opportunity.user === currentUser.id;
    if (opportunity.candidateQuestions) {
      if (opportunity.candidateQuestions.length > 0) {
        hasQuestions = true;
      }
    }
    if (user.educationExperience) {
      if (user.educationExperience.length > 0) {
        hasEducation = true;
      }
    }
    return (
      <Col lg={12} md={12}>
        <div className={Style.container}>
          <div className={Style.imageContainer}>
            {user.profilePicture ? (
              <img className={Style.userImage} src={user.profilePicture.url} />
            ) : (
              <img className={Style.userImage} src="https://cdn.efuse.gg/uploads/static/global/mindblown_white.png" />
            )}
          </div>
          <div className={Style.textContainer}>
            <Link href="/u/[u]" as={`/u/${user?.username}`}>
              <a
                className={Style.link}
                onClick={() => {
                  analytics.track("OPPORTUNITY_APPLICANT_VIEW_USER_PROFILE", {
                    applicant: applicant._id
                  });
                }}
              >
                <div className={`${Style.online} ${user.online ? null : Style.offline}`} />
                <h3 className={Style.name}>{user.name}</h3>
                <span className={Style.username}>@{user.username}</span>
                {user.verified && <FontAwesomeIcon icon={faCheckCircle} className={Style.check} />}
              </a>
            </Link>
            {isOwner && (
              <div className={Style.applicationStatus}>
                <ApplicationStatus
                  status={applicant.status}
                  clickHandler={this.updateStatus}
                  applicantName={user?.name}
                  applicant={applicant}
                />
              </div>
            )}

            <div className={Style.location}>
              <DisplayLocation location={user.address} />
            </div>
            <div className={Style.actionContainer}>
              {hasQuestions && (
                <>
                  <FontAwesomeIcon
                    className={Style.action}
                    onClick={() => this.loadModal(applicant._id)}
                    icon={faSearchPlus}
                  />
                  <span className={Style.actionText} onClick={() => this.loadModal(applicant._id)}>
                    View More
                  </span>
                </>
              )}
              {hasEducation && (
                <>
                  <FontAwesomeIcon
                    className={Style.action}
                    onClick={() => this.loadEducation(applicant._id)}
                    icon={faSchool}
                  />
                  <span className={Style.actionText} onClick={() => this.loadEducation(applicant._id)}>
                    Education
                  </span>
                </>
              )}
              <FontAwesomeIcon
                className={Style.action}
                icon={faEnvelope}
                onClick={() => this.loadMessenger(applicant._id, user._id, user.username ? user.username : "username")}
              />
              <span
                className={Style.actionText}
                onClick={() => this.loadMessenger(applicant._id, user._id, user.username ? user.username : "username")}
              >
                Message
              </span>
            </div>
            <div className={Style.iconContainer}>
              <SocialLinksIcons passedUser={this.props.user} />
            </div>
            {modal && modalSection === `questions${applicant._id}` && (
              <ViewQuestions
                show={modal}
                onHide={this.onHide}
                applicant={applicant}
                opportunity={opportunity}
                user={user}
              />
            )}
            {modal && modalSection === `education${applicant._id}` && (
              <ViewEducation show={modal} onHide={this.onHide} user={user} />
            )}
            {modal && modalSection === `messenger${applicant._id}` && (
              <MessageApplicant show={modal} onHide={this.onHide} userName={user.name} />
            )}
          </div>
        </div>
      </Col>
    );
  }
}

const mapStateToProps = state => ({
  modal: state.portfolio.modal,
  modalSection: state.portfolio.modalSection,
  currentUser: state.auth.currentUser
});

export default connect(mapStateToProps, {
  updateApplicantStatus,
  togglePortfolioModal,
  startChatWith,
  getChatChannels,
  clearChat
})(ApplicantComponentDeprecated);
