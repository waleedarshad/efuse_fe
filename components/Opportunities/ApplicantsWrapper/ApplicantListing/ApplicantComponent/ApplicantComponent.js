import React from "react";
import EFApplicantCard from "../../../../Cards/EFApplicantCard/EFApplicantCard";
import TopButtons from "./TopButtons/TopButtons";

import FooterButtons from "./FooterButtons/FooterButtons";
import Style from "./ApplicantComponent.module.scss";

const ApplicantComponent = ({ user, applicant, filterType, opportunity }) => {
  return (
    <div className={Style.applicantCardWrapper}>
      <EFApplicantCard
        titleImageUrl={user.profilePicture.url}
        titleImageHref={`/u/${user.username}`}
        headerText={user.name}
        subHeaderText={`@${user.username}`}
        headerTags={[user.address]}
        headerActionButtons={<TopButtons user={user} applicant={applicant} opportunity={opportunity} />}
        body={<span className={Style.bodyText}>{user.bio}</span>}
        footerActionButtons={
          <FooterButtons applicant={applicant} user={user} opportunity={opportunity} filterType={filterType} />
        }
      />
    </div>
  );
};
export default ApplicantComponent;
