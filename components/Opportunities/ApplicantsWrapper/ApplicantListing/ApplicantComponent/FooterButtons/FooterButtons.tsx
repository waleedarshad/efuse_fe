import React from "react";
import { useSelector } from "react-redux";

import ViewNotesModal from "../../../../../Recruiting/RecruitingList/ViewNotesModal/ViewNotesModal";
import FooterDropdown from "../FooterDropdown/FooterDropdown";
import Style from "./FooterButtons.module.scss";

const FooterButtons = ({ applicant, user, filterType, opportunity }) => {
  const authUser = useSelector(state => state.auth.currentUser);
  return (
    <div className={Style.buttonsContainer}>
      <div className={Style.buttonWrapper}>
        <ViewNotesModal recruitId={user._id} recruiterId={authUser?._id} recruitType="users" />
      </div>
      <div className={Style.buttonWrapper}>
        <FooterDropdown
          applicant={applicant}
          applicantName={user.name}
          filterType={filterType}
          opportunityId={opportunity._id}
        />
      </div>
    </div>
  );
};

export default FooterButtons;
