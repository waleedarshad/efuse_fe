import React from "react";
import dynamic from "next/dynamic";
import EFPrimaryModal from "../../../../../Modals/EFPrimaryModal/EFPrimaryModal";

const SingleChat = dynamic(import("../../../../../Messages/Inbox/SingleChat"), { ssr: false });

const MessageApplicant = ({ show, onHide, userName }) => {
  return (
    <EFPrimaryModal
      title={`Send message to ${userName}`}
      widthTheme="medium"
      isOpen={show}
      onClose={() => onHide("messenger")}
      allowBackgroundClickClose={false}
    >
      <SingleChat />
    </EFPrimaryModal>
  );
};

export default MessageApplicant;
