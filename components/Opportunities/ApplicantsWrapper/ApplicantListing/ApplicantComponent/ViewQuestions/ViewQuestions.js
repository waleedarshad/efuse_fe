import React from "react";
import Style from "./ViewQuestions.module.scss";
import VideoPlayer from "../../../../../VideoPlayer/VideoPlayer";
import { EFHtmlParser } from "../../../../../EFHtmlParser/EFHtmlParser";
import EFPrimaryModal from "../../../../../Modals/EFPrimaryModal/EFPrimaryModal";

const ViewQuestions = ({ show, onHide, applicant, opportunity, user }) => {
  const answers = applicant?.candidateQuestions;
  const questions = opportunity?.candidateQuestions;

  return (
    <EFPrimaryModal
      title={`${user.name}'s Answers`}
      isOpen={show}
      allowBackgroundClickClose={false}
      onClose={onHide}
      widthTheme="medium"
    >
      {answers.map((answer, index) => (
        <div key={index}>
          {answer?.question && (
            <>
              {`${index + 1}. ${questions[index].question}`}
              <div className={Style.answer}>
                <EFHtmlParser>{answer.question}</EFHtmlParser>
              </div>
            </>
          )}
          {answer?.media?.url && (
            <>
              {`${index + 1}. ${questions[index].question}`}
              <div className={Style.answer}>
                {answer?.media?.contentType?.includes("video") ? (
                  <VideoPlayer url={answer.media.url} />
                ) : (
                  <img className={Style.imageAnswer} alt="applicant answer" src={answer.media.url} />
                )}
              </div>
            </>
          )}
        </div>
      ))}
    </EFPrimaryModal>
  );
};

export default ViewQuestions;
