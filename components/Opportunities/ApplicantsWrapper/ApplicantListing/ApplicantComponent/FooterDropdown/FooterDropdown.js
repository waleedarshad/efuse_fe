import React, { useState } from "react";
import { useDispatch } from "react-redux";

import EFSelectDropdown from "../../../../../Dropdowns/EFSelectDropdown/EFSelectDropdown";
import { updateStatus } from "../../../../../../store/actions/applicantActions";
import EFAlert from "../../../../../EFAlert/EFAlert";
import Style from "./FooterDropdown.module.scss";

const FooterDropdown = ({ applicant, applicantName, filterType, opportunityId }) => {
  const dispatch = useDispatch();
  const [isInitialOnChange, setIsInitialOnChange] = useState(true);

  const applicantStatusOptions = [
    { label: "No Action", value: "noaction" },
    { label: "Accepted", value: "accepted" },
    { label: "Undecided", value: "undecided" },
    { label: "Declined", value: "declined" }
  ];

  const onChange = status => {
    if (isInitialOnChange) {
      setIsInitialOnChange(false);
      return;
    }

    if (status === "accepted") {
      const title = "Confirm";
      const message = `Would you like to send an email notification to ${applicantName} ?`;
      const confirmLabel = "Yes, send email";
      const cancelLabel = "No, do not send email";

      const onConfirm = () => {
        updateApplicantStatus(status, true);
      };

      const onCancel = () => {
        updateApplicantStatus(status);
      };

      EFAlert(title, message, confirmLabel, cancelLabel, onConfirm, onCancel);
    } else {
      updateApplicantStatus(status);
    }
    analytics.track(`OPPORTUNITY_APPLICANT_${status.toUpperCase()}_APPLICANT`, { applicant: applicant._id });
  };

  const updateApplicantStatus = (status, sendEmail = false) => {
    dispatch(updateStatus(opportunityId, applicant._id, status, sendEmail, filterType));
  };

  return (
    <div className={Style.dropdownWrapper}>
      <EFSelectDropdown
        options={applicantStatusOptions}
        defaultValue={applicant.status}
        onSelect={value => onChange(value)}
      />
    </div>
  );
};

export default FooterDropdown;
