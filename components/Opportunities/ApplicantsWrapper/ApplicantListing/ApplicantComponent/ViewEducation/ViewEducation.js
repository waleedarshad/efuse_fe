import React from "react";
import EFPrimaryModal from "../../../../../Modals/EFPrimaryModal/EFPrimaryModal";
import DisplayExperience from "../../../../../User/Portfolio/EducationExperience/DisplayExperience/DisplayExperience";

const ViewEducation = ({ show, onHide, user }) => {
  const education = user.educationExperience;
  const content = education.map((experience, index) => {
    return (
      <DisplayExperience
        key={index}
        experience={experience}
        onEdit={() => "empty"}
        onRemove={() => "empty"}
        displayHr={education.length !== index + 1}
        isCurrentUser={false}
      />
    );
  });
  return (
    <EFPrimaryModal
      title={`${user.name}'s Education Experience`}
      isOpen={show}
      allowBackgroundClickClose={false}
      onClose={onHide}
      widthTheme="medium"
    >
      {content}
    </EFPrimaryModal>
  );
};

export default ViewEducation;
