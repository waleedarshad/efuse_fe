import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Row, Col } from "react-bootstrap";
import InfiniteScroll from "react-infinite-scroller";

import AnimatedLogo from "../../../AnimatedLogo";
import NoRecordFound from "../../../NoRecordFound/NoRecordFound";
import { getApplicantsWithGraphql } from "../../../../store/actions/applicantActions";
import ApplicantComponentDeprecated from "./ApplicantComponent/ApplicantComponentDeprecated";
import ApplicantComponent from "./ApplicantComponent/ApplicantComponent";
import { getFlagForFeature } from "../../../../store/selectors/featureFlagSelectors";
import FEATURE_FLAGS from "../../../../common/featureFlags";

let page = 1;

const ApplicantListing = ({ id, filterType, opportunity }) => {
  const dispatch = useDispatch();
  const applicants = useSelector(state => state.applicants.applicants);
  const hasNextPage = useSelector(state => state.applicants.pagination.hasNextPage);
  const isGettingFirstApplicant = useSelector(state => state.applicants.isGettingFirstApplicant);
  const revampApplicantCard = useSelector(state =>
    getFlagForFeature(state, FEATURE_FLAGS.OPPORTUNITY_APPLICANT_CARD_DESIGN_REVAMP)
  );

  const loadMorePages = () => {
    page += 1;
    analytics.track("OPPORTUNITY_APPLICANT_VIEW_MORE");
    dispatch(getApplicantsWithGraphql(id, page, filterType));
  };

  const applicantsList = applicants.map(applicant => {
    return (
      <Col lg={12} md={12} key={applicant?.user?._id}>
        {revampApplicantCard ? (
          <ApplicantComponent
            user={applicant?.user}
            applicant={applicant}
            filterType={filterType}
            opportunity={opportunity}
          />
        ) : (
          <ApplicantComponentDeprecated
            user={applicant?.user}
            applicant={applicant}
            filterType={filterType}
            opportunity={opportunity}
          />
        )}
      </Col>
    );
  });

  return (
    <>
      <InfiniteScroll
        pageStart={1}
        loadMore={loadMorePages}
        hasMore={hasNextPage}
        loader={<AnimatedLogo key={0} theme="inline" />}
      >
        <Row>
          {isGettingFirstApplicant && (
            <div className="text-center" style={{ width: "100%" }}>
              <AnimatedLogo key={0} theme="" />
            </div>
          )}
          {applicantsList.length === 0 ? <NoRecordFound /> : applicantsList}
        </Row>
      </InfiniteScroll>
    </>
  );
};

export default ApplicantListing;
