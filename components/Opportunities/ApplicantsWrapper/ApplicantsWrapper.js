import isEmpty from "lodash/isEmpty";
import Router, { withRouter } from "next/router";
import React, { Component } from "react";
import { Col } from "react-bootstrap";
import { connect } from "react-redux";
import { faGamepadAlt } from "@fortawesome/pro-solid-svg-icons";

import FEATURE_FLAGS from "../../../common/featureFlags";
import { getOpportunitiesNavigationList } from "../../../navigation/opportunities";
import { clearApplicants, getApplicantsWithGraphql } from "../../../store/actions/applicantActions";
import { getOpportunityById } from "../../../store/actions/opportunityActions";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";
import FeatureFlag from "../../General/FeatureFlag/FeatureFlag";
import FeatureFlagVariant from "../../General/FeatureFlag/FeatureFlagVariant/FeatureFlagVariant";
import EFSubHeader from "../../Layouts/Internal/EFSubHeader/EFSubHeader";
import Internal from "../../Layouts/Internal/Internal";
import OpportunitiesHeader from "../../OpportunitiesV2/OpportunitiesHeader/OpportunitiesHeader";
import ApplicantFilters from "./ApplicantFilters/ApplicantFilters";
import ApplicantListing from "./ApplicantListing/ApplicantListing";
import ApplicantQueue from "./ApplicantListing/ApplicantQueue/ApplicantQueue";
import Style from "./ApplicantsWrapper.module.scss";
import FilterApplied from "./FilterApplied/FilterApplied";

const ACCEPTED_FILTER_TYPE = ["noaction", "accepted", "undecided", "declined"];

class ApplicantsWrapper extends Component {
  stateTimeout = null;

  constructor() {
    super();
    this.state = {
      filterType: ""
    };
  }

  componentDidMount() {
    analytics.page("Opportunities Applicants");
    this.loadApplicants();
  }

  componentDidUpdate() {
    this.loadApplicants();
  }

  loadApplicants = () => {
    const { pagination, query } = this.props;
    const { pathname, search } = window.location;
    const { id } = query;
    let typeParam = search.includes("type=")
      ? search
          .split("&")
          .find(param => param.includes("type="))
          .split("type=")
          .filter(param => param)[0]
      : "hardload";
    const { filterType } = this.state;
    if (typeParam === "hardload" || !ACCEPTED_FILTER_TYPE.includes(typeParam)) {
      Router.replace(`${pathname}?id=${id}&type=noaction`);
      typeParam = "noaction";
    }
    if (typeParam !== filterType && id) {
      if (!isEmpty(pagination)) {
        this.props.clearApplicants();
      }
      this.props.getOpportunityById(id);
      this.props.getApplicantsWithGraphql(id, 1, typeParam);
      this.setState({ filterType: typeParam });
    }
  };

  render() {
    const { query, totalApplicants, opportunity, router } = this.props;
    const { id, type } = query;

    return (
      <Internal metaTitle="eFuse | Applicants" isWindowView={this.props.isWindowView} containsSubheader>
        <FeatureFlag name={FEATURE_FLAGS.OPPORTUNITY_FILTER_MODAL}>
          <FeatureFlagVariant flagState>
            <OpportunitiesHeader isCompactToggleDisplayed={false} isFilterDisplayed={false} />
          </FeatureFlagVariant>
          <FeatureFlagVariant flagState={false}>
            <EFSubHeader
              headerIcon={faGamepadAlt}
              navigationList={getOpportunitiesNavigationList(router.pathname)}
              actionButtons={[
                // eslint-disable-next-line react/jsx-key
                <FeatureFlag name={FEATURE_FLAGS.ALLOW_OPPORTUNITY_CREATION}>
                  <FeatureFlagVariant flagState>
                    <EFRectangleButton
                      colorTheme="primary"
                      fontWeightTheme="bold"
                      shadowTheme="small"
                      size="medium"
                      text="Create Opportunity"
                      internalHref="/opportunities/create"
                    />
                  </FeatureFlagVariant>
                </FeatureFlag>
              ]}
            />
          </FeatureFlagVariant>
        </FeatureFlag>
        <Col lg={3} md={4} className="pl-sm-0">
          <ApplicantFilters id={id} />
        </Col>
        <Col md={8} lg={9}>
          {id && <ApplicantQueue type={type} id={id} />}
          <FilterApplied id={id} />
          <div className={Style.textbox}>
            <p className={Style.opportunityTitle}>{opportunity.title}</p>
            <p className={Style.totalApplicants}>Total Number of Applicants: {totalApplicants}</p>
          </div>
          <ApplicantListing id={id} filterType={type} opportunity={opportunity} />
        </Col>
      </Internal>
    );
  }
}
const mapStateToProps = state => ({
  isWindowView: state.messages.isWindowView,
  pagination: state.applicants.pagination,
  opportunity: state.opportunities.opportunity,
  totalApplicants: state.applicants.pagination.totalDocs
});
export default connect(mapStateToProps, {
  getApplicantsWithGraphql,
  clearApplicants,
  getOpportunityById
})(withRouter(ApplicantsWrapper));
