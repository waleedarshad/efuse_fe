import React, { Component } from "react";
import { Card, FormGroup, Col, Row } from "react-bootstrap";
import { connect } from "react-redux";
import uniq from "lodash/uniq";
import capitalize from "lodash/capitalize";
import remove from "lodash/remove";
import uniqueId from "lodash/uniqueId";
import Style from "../../OpportunityWrapper/OpportunityFilters/OpportunityFilters.module.scss";
import InputLabel from "../../../InputLabel/InputLabel";
import Checkbox from "../../../Checkbox/Checkbox";
import { updateStateFromProps, onFilterChange, applyFilters } from "../../../../helpers/FilterHelper";
import { mergeLabelsAndValues } from "../../../../helpers/GeneralHelper";
import { updateFilterValue, searchApplicant } from "../../../../store/actions/applicantFilterActions";
import InputField from "../../../InputField/InputField";
import GoogleAutoComplete from "../../../GoogleAutocomplete/GoogleAutocomplete";

class OrganizationFilters extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchFilters: {
        "email.$regex": "",
        "address.$regex": "",
        "roles.$in": []
      },
      email: ""
    };
  }

  static getDerivedStateFromProps(props, state) {
    return updateStateFromProps(props, state);
  }

  onChange = event => {
    const { name, value, label } = onFilterChange(event);
    this.setFilterAndSearch(name, value, label);
  };

  onEmailChange = event => {
    const { target } = event;
    const { value } = target;
    this.setState({
      email: value
    });
  };

  searchByEmail = event => {
    if (event.key === "Enter") {
      const value = event.target.value.trim();
      if (value.length > 0) {
        this.setFilterAndSearch("email.$regex", value, `Email: ${value}`);
        this.setState({ email: "" });
      }
    }
  };

  onSuggestSelect = suggest => {
    const value = suggest === undefined ? "" : suggest.label;
    this.setFilterAndSearch("address.$regex", value, value);
  };

  onRoleSelect = event => {
    const roles = this.state.searchFilters["roles.$in"];
    const { target } = event;
    const { value, name } = target;
    if (target.checked) {
      uniq(roles.push(value));
    } else {
      remove(roles, role => role === value);
    }
    this.setFilterAndSearch(name, roles, `Roles: ${roles.map(role => capitalize(role)).join(", ")}`);
  };

  setFilterAndSearch = (name, value, label, regex = false) => {
    this.props.updateFilterValue(name, value);
    applyFilters(
      name,
      value,
      label,
      this.props.filtersApplied,
      this.props.searchApplicant,
      false,
      this.props.id,
      regex
    );
  };

  render() {
    const roleValues = ["student", "professional", "gamer"];
    const { searchFilters, email } = this.state;
    const roleFilters = mergeLabelsAndValues(
      ["Student", "Professional", "Gamer"],
      { returnObjects: true },
      roleValues
    ).map(role => (
      <Col key={uniqueId()}>
        <Checkbox
          custom
          name="roles.$in"
          type="checkbox"
          label={role.label}
          id={`roles-${role.value}`}
          theme="internal"
          checked={searchFilters["roles.$in"].includes(role.value)}
          value={role.value}
          onChange={this.onRoleSelect}
        />
      </Col>
    ));
    return (
      <div className={Style.filterWrapper}>
        <Card className={`mb-4 ${Style.filterCard}`}>
          <Card.Header>
            <h5 className={`headerTitle ${Style.filterTitle}`}>Filters</h5>
          </Card.Header>
          <Card.Body>
            <InputLabel theme="internal">Search By Email</InputLabel>
            <FormGroup>
              <InputField
                size="lg"
                theme="internal"
                block="true"
                name="email"
                onChange={this.onEmailChange}
                value={email}
                placeholder="Enter Email"
                onKeyPress={this.searchByEmail}
              />
            </FormGroup>
            <FormGroup>
              <GoogleAutoComplete
                onSuggest={this.onSuggestSelect}
                placeholder="Search Location"
                className="geoselect-internal"
                initialValue={searchFilters["address.$regex"]}
              />
            </FormGroup>
            {this.props.enableRolesFilter && (
              <>
                <InputLabel theme="internal">Search By Role</InputLabel>
                <Row className="mb-2">{roleFilters}</Row>
              </>
            )}
          </Card.Body>
        </Card>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  searchFilters: state.applicants.searchFilters,
  filtersApplied: state.applicants.filtersApplied,
  enableRolesFilter: state.features.applicants_role_filter
});

export default connect(mapStateToProps, { updateFilterValue, searchApplicant })(OrganizationFilters);
