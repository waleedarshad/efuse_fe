import { useRouter } from "next/router";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import FEATURE_FLAGS from "../../../common/featureFlags";
import { SUBTYPES } from "../../../common/opportunities";
import { applyFilters } from "../../../helpers/FilterHelper";
import { sendNotification } from "../../../helpers/FlashHelper";
import { navigateToBrowse } from "../../../store/actions/opportunitiesv2/opportunitiesv2Actions";
import {
  searchOpportunity,
  updateFilterValue,
  updateSearchObject
} from "../../../store/actions/opportunityFilterActions";
import { getFlagForFeature } from "../../../store/selectors/featureFlagSelectors";
import OpportunityWrapper from "../OpportunityWrapper/OpportunityWrapper";

const Category = ({ view }) => {
  const router = useRouter();
  const dispatch = useDispatch();

  const filtersApplied = useSelector(state => state.opportunities.filtersApplied);

  const category = router?.query?.category;

  const searchMethod = (searchObject, filters) => {
    dispatch(searchOpportunity(searchObject, filters));
  };

  let searchParam;

  switch (category) {
    case "scholarships":
      searchParam = "Scholarship";
      break;
    case "jobs":
      searchParam = "Employment Opening";
      break;
    case "team-openings":
      searchParam = "Team Opening";
      break;
    case "events":
      searchParam = "Event";
      break;
    default:
      sendNotification("Please enter a valid category", "danger", "Invalid Category");
      if (typeof window !== "undefined") router.push("/opportunities/all");
      break;
  }

  useEffect(() => {
    dispatch(updateFilterValue("opportunityType", searchParam, searchParam));
    dispatch(updateSearchObject({ opportunityType: searchParam }));
    applyFilters("opportunityType", searchParam, searchParam, filtersApplied, searchMethod);
  }, []);

  return <OpportunityWrapper view={view} category={category} />;
};

const CategoryRouter = props => {
  const router = useRouter();
  const dispatch = useDispatch();

  const category = router?.query?.category;

  const isOpportunityFilterModalOn = useSelector(state =>
    getFlagForFeature(state, FEATURE_FLAGS.OPPORTUNITY_FILTER_MODAL)
  );

  const navigateToBrowseWithFilters = filters => {
    if (isOpportunityFilterModalOn && typeof window !== "undefined") {
      dispatch(navigateToBrowse(filters));
    }
  };

  if (isOpportunityFilterModalOn) {
    switch (category) {
      case "scholarships":
        navigateToBrowseWithFilters(Object.values(SUBTYPES.SCHOLARSHIP));
        break;
      case "jobs":
        navigateToBrowseWithFilters(Object.values(SUBTYPES.JOB));
        break;
      case "team-openings":
        navigateToBrowseWithFilters(Object.values(SUBTYPES.TEAM_OPENING));
        break;
      case "events":
        navigateToBrowseWithFilters(Object.values(SUBTYPES.EVENT));
        break;
      default:
        sendNotification("Please enter a valid category", "danger", "Invalid Category");
        navigateToBrowseWithFilters();
        break;
    }
    return <></>;
  }

  // eslint-disable-next-line react/jsx-props-no-spreading
  return <Category {...props} />;
};

export default CategoryRouter;
