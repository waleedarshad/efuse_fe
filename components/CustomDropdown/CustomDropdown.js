import React from "react";
import { useSelector } from "react-redux";
import { Dropdown, NavItem } from "react-bootstrap";
import NavLink from "react-bootstrap/NavLink";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import PropTypes from "prop-types";
import Link from "next/link";
import Style from "./CustomDropdown.module.scss";
import ConfirmAlert from "../ConfirmAlert/ConfirmAlert";

const CustomDropdown = ({ items, icon, customClass, customToggle }) => {
  const webview = useSelector(state => state.webview.status);

  const toggle = (
    <Dropdown.Toggle as={NavLink} className={icon}>
      {customToggle && customToggle}
    </Dropdown.Toggle>
  );

  const dropdownItems = items.map((item, index) => {
    const iconHtml =
      (item.icon && (
        <span className="itemIcon" key={index}>
          <FontAwesomeIcon icon={item.icon} />
        </span>
      )) ||
      (item.customIconSrc && <img src={item.customIconSrc} alt="eFuse Logo" className={Style.efuseLogo} />);

    if (item.as === "button") {
      if (item.title === "Delete") {
        return (
          <ConfirmAlert
            key={index}
            title={item.message ? item.message : "Delete this post?"}
            message="Are you sure?"
            onYes={() => item.onClick()}
          >
            <Dropdown.Item as={item.as} type={item.type}>
              {iconHtml}
              {item.title}
            </Dropdown.Item>
          </ConfirmAlert>
        );
      }
      return (
        <div key={index}>
          <Dropdown.Item key={index} as={item.as} onClick={item.onClick} type={item.type} className={item.customClass}>
            {item.customComponent}
            {iconHtml}
            {item.title}
          </Dropdown.Item>
          <div className="line" />
        </div>
      );
    }
    const itemLink = (
      <a {...(item.dynamic ? {} : { href: item.path })} key={index} className="dropdown-item">
        {iconHtml}
        {item.title}
      </a>
    );
    const buildLink = item.dynamic ? (
      <Link {...item.dynamicUrl} key={index}>
        {itemLink}
      </Link>
    ) : (
      <div key={index}>
        <Link href={item.path} key={index}>
          {itemLink}
        </Link>
        {item.link === "admin" && <div className="line" />}
      </div>
    );
    return webview ? itemLink : buildLink;
  });

  return (
    <Dropdown as={NavItem} className={`customDropdown ${customClass}`} alignRight>
      {toggle}

      <Dropdown.Menu>
        <span className="caret" />
        {dropdownItems}
      </Dropdown.Menu>
    </Dropdown>
  );
};

CustomDropdown.propTypes = {
  icon: PropTypes.string,
  items: PropTypes.arrayOf(
    PropTypes.shape({
      // eslint-disable-next-line react/forbid-prop-types
      title: PropTypes.any,
      path: PropTypes.string,
      // eslint-disable-next-line react/forbid-prop-types
      icon: PropTypes.any,
      as: PropTypes.string,
      type: PropTypes.string,
      onClick: PropTypes.func,
      customIconSrc: PropTypes.string
    })
  ).isRequired,
  customClass: PropTypes.string
};

CustomDropdown.defaultProps = {
  icon: "downArrow",
  customClass: ""
};

export default CustomDropdown;
