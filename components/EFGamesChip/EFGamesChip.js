import React from "react";
import EFChip from "../EFChip/EFChip";
import EFImage from "../EFImage/EFImage";
import Style from "./EFGamesChip.module.scss";

const BaseGameChip = ({ image, renderChip }) => {
  return (
    <div className={Style.chipWrapper}>
      {renderChip()}
      <div className={Style.imageWrapper}>
        <EFImage src={image} className={Style.image} />
      </div>
    </div>
  );
};

const Info = ({ image, title }) => {
  return (
    <BaseGameChip
      image={image}
      renderChip={() => <EFChip.Info content={<span className={Style.chipTitle}>{title}</span>} />}
    />
  );
};

const Removable = ({ image, title, onRemove }) => {
  return (
    <BaseGameChip
      image={image}
      renderChip={() => (
        <EFChip.Removable content={<span className={Style.chipTitle}>{title}</span>} onRemove={onRemove} />
      )}
    />
  );
};

const EFGamesChip = {
  Info,
  Removable
};

export default EFGamesChip;
