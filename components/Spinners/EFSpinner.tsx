import { Spinner } from "react-bootstrap";
import React from "react";

const EFSpinner = () => {
  return <Spinner animation="border" role="status" />;
};

export default EFSpinner;
