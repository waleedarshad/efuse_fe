import React from "react";
import styled from "styled-components";
import EFSpinner from "./EFSpinner";

const LoadingOverlay = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  flex: 1;
`;

const EFSpinnerOverlay = ({ isLoading, children }) => {
  if (isLoading) {
    return (
      <LoadingOverlay>
        <EFSpinner />
      </LoadingOverlay>
    );
  }

  return children;
};

export default EFSpinnerOverlay;
