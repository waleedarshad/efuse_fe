import React from "react";
import { connectHits } from "react-instantsearch-dom";
import Style from "./SideOpportunitiesList.module.scss";
import EFGeneralSideCard from "../../Cards/EFGeneralSideCard/EFGeneralSideCard";
import { triggerDebouncedSegmentEvent } from "../../../helpers/AlgoliaHelper";

const SideOpportunitiesList = connectHits(({ hits, indexName }) => {
  triggerDebouncedSegmentEvent(hits, "SIDE_CARD_LIST_VIEW", indexName);

  return (
    <ul className={Style.opportunitiesList}>
      {hits.map(hit => (
        <li key={hit.objectID}>
          <div className={Style.cardWrapper}>
            <EFGeneralSideCard
              imageUrl={hit?.image?.url}
              title={hit?.title}
              alt={hit?.shortName}
              date={hit?.createdAt}
              buttonOnClick={() => {
                // mapping to Product Clicked in Algolia Insights destination
                analytics.track("SIDE_CARD_VIEW_BUTTON_CLICKED", {
                  opportunityId: hit?._id,
                  objectID: hit.objectID,
                  position: hit.__position,
                  index: indexName,
                  queryID: hit.__queryID
                });
              }}
              buttonUrl={`/opportunities/show?id=${hit?._id}`}
              badgeTheme={hit?.opportunityType}
              badgeText={hit?.opportunityType === "Employment Opening" ? "Job" : hit?.opportunityType}
            />
          </div>
        </li>
      ))}
    </ul>
  );
});

export default SideOpportunitiesList;
