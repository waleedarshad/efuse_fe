import React, { memo } from "react";
import { useSelector } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faGamepadAlt } from "@fortawesome/pro-regular-svg-icons";
import { InstantSearch, Configure } from "react-instantsearch-dom";
import Style from "./EFSideOpportunities.module.scss";
import SideOpportunitiesList from "./SideOpportunitiesList/SideOpportunitiesList";
import { returnAlgoliaSearchClient, getIndexName } from "../../helpers/AlgoliaHelper";

const EFSideOpportunities = () => {
  const algoliaIndexName = getIndexName("OPPORTUNITIES");
  const currentUser = useSelector(state => state.auth.currentUser);

  // don't render anything until the user is loaded
  if (!currentUser?._id) return <></>;

  return (
    <InstantSearch searchClient={returnAlgoliaSearchClient()} indexName={algoliaIndexName}>
      <Configure enablePersonalization userToken={currentUser?._id} hitsPerPage={4} distinct analytics={false} />
      <div className={`stickySidebar ${Style.sideOpportunitiesWrapper}`}>
        <p className={Style.sectionTitle}>
          <FontAwesomeIcon icon={faGamepadAlt} className={Style.icon} />
          Recommended Opportunities
        </p>
        <SideOpportunitiesList indexName={algoliaIndexName} />
      </div>
    </InstantSearch>
  );
};

export default memo(EFSideOpportunities);
