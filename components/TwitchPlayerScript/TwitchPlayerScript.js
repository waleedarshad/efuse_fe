import React, { useEffect } from "react";

const TwitchPlayerScript = props => {
  const { twitchChannel, theme } = props;
  useEffect(() => {
    if (twitchChannel) {
      const options = {
        width: "100%",
        height: "100%",
        channel: twitchChannel,
        theme,
        layout: "video"
      };
      const scriptEmbed = () => {
        const embed = new window.Twitch.Embed("twitch-embed", options);
      };
      const script = document.createElement("script");
      script.src = "https://player.twitch.tv/js/embed/v1.js";
      script.async = true;
      script.setAttribute("type", "text/javascript");
      script.id = "twitch-script";
      document.body.appendChild(script);
      script.addEventListener("load", () => {
        const embed = new window.Twitch.Embed("twitch-embed", scriptEmbed());
      });
      return () => {
        // used to ensure no twitch embed script or iframe exists after component unmounts - Austin
        document.removeEventListener("load", scriptEmbed());
        const scriptRemove = document.getElementById("twitch-script");
        document.body.removeChild(scriptRemove);
        const twitchEmbedDiv = document.getElementById("twitch-embed");
        twitchEmbedDiv.innerHTML = "";
      };
    }
  });

  return <div id="twitch-embed" className="embed-responsive-item" />;
};

export default TwitchPlayerScript;
