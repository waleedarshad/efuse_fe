import React from "react";
import Internal from "../Layouts/Internal/Internal";
import getGamesHook from "../../hooks/getHooks/getGamesHook";

const Games = () => {
  const gamesList = getGamesHook();
  return <Internal metaTitle="eFuse | Games">{gamesList?.map(game => game?.title)}</Internal>;
};

export default Games;
