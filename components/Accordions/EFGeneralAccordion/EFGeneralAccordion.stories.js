import React from "react";
import EFGeneralAccordion from "./EFGeneralAccordion";

export default {
  title: "Accordions/EFGeneralAccordion",
  component: EFGeneralAccordion,
  argTypes: {
    isOpenOnMount: {
      control: {
        type: "boolean"
      }
    },
    text: {
      control: {
        type: "text"
      }
    },
    infoText: {
      control: {
        type: "text"
      }
    }
  }
};

const Story = args => <EFGeneralAccordion {...args}>Inner content!</EFGeneralAccordion>;

export const Default = Story.bind({});
Default.args = {
  isOpenOnMount: false,
  text: "Click Me!"
};

export const OpenOnMount = Story.bind({});
OpenOnMount.args = {
  isOpenOnMount: true,
  text: "Click Me!"
};

export const InfoText = Story.bind({});
InfoText.args = {
  isOpenOnMount: false,
  text: "Click Me!",
  infoText: "This is information!"
};
