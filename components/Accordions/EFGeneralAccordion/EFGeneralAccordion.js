import React, { useState } from "react";
import EFAccordion from "../EFAccordion/EFAccordion";
import GeneralAccordionTrigger from "../AccordionTriggers/GeneralAccordionTrigger/GeneralAccordionTrigger";

const EFGeneralAccordion = ({ transitionSpeed, isOpenOnMount, text, infoText, children }) => {
  const [isOpen, toggleOpenState] = useState(isOpenOnMount);

  const triggerComponent = (
    <GeneralAccordionTrigger text={text} isOpen={isOpen} onClickEvent={toggleOpenState} infoText={infoText} />
  );

  return (
    <EFAccordion transitionSpeed={transitionSpeed} isOpen={isOpen} triggerComponent={triggerComponent}>
      {children}
    </EFAccordion>
  );
};

EFGeneralAccordion.defaultProps = {
  isOpenOnMount: false
};

export default EFGeneralAccordion;
