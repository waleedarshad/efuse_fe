import { mount } from "enzyme";
import EFGeneralAccordion from "./EFGeneralAccordion";
import GeneralAccordionTrigger from "../AccordionTriggers/GeneralAccordionTrigger/GeneralAccordionTrigger";
import EFAccordion from "../EFAccordion/EFAccordion";

describe("EFGeneralAccordion", () => {
  it("renders an EFAccordion with correct trigger", () => {
    const children = <h1>Hello World</h1>;
    const subject = mount(
      <EFGeneralAccordion
        transitionSpeed={500}
        isOpenOnMount={false}
        text="cool text"
        infoText="cool info text"
        children={children}
      />
    );

    expect(subject.find(EFAccordion).prop("triggerComponent").type).toEqual(GeneralAccordionTrigger);
  });

  it("renders an EFAccordion with the correct props", () => {
    const children = <h1>Hello World</h1>;
    const subject = mount(
      <EFGeneralAccordion
        transitionSpeed={500}
        isOpenOnMount={false}
        text="cool text"
        infoText="cool info text"
        children={children}
      />
    );

    expect(subject.find(EFAccordion).prop("transitionSpeed")).toEqual(500);
    expect(subject.find(EFAccordion).prop("isOpen")).toBe(false);
  });

  it("renders an EFAccordion with the correct children", () => {
    const children = <h1>Hello World</h1>;
    const subject = mount(
      <EFGeneralAccordion
        transitionSpeed={500}
        isOpenOnMount={false}
        text="cool text"
        infoText="cool info text"
        children={children}
      />
    );

    expect(subject.find("h1").text()).toEqual("Hello World");
  });
});
