import React, { useState } from "react";
import EFAccordion from "../EFAccordion/EFAccordion";
import PointRaceLeaderboardItem from "../AccordionTriggers/PointRaceLeaderboardItem/PointRaceLeaderboardItem";

const EFLeaderboardAccordion = ({
  transitionSpeed,
  isOpenOnMount,
  teamName,
  teamScore,
  teamRank,
  isActive,
  displayDropdown,
  children
}) => {
  const [isOpen, toggleOpenState] = useState(isOpenOnMount);
  const triggerComponent = (
    <PointRaceLeaderboardItem
      name={teamName}
      score={teamScore}
      scoreLabel="Points"
      rank={teamRank}
      isOpen={isOpen}
      isActive={isActive}
      displayDropdown={displayDropdown}
      onClickEvent={toggleOpenState}
    />
  );
  return (
    <EFAccordion transitionSpeed={transitionSpeed} isOpen={isOpen} triggerComponent={triggerComponent}>
      {children}
    </EFAccordion>
  );
};

EFLeaderboardAccordion.defaultProps = {
  isOpenOnMount: false,
  isActiveTeam: true,
  displayDropdown: true
};

export default EFLeaderboardAccordion;
