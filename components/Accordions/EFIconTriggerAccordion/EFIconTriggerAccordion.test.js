import { mount } from "enzyme";
import React from "react";
import EFIconTriggerAccordion from "./EFIconTriggerAccordion";
import EFAccordionTriggerWrapper from "../AccordionTriggers/EFAccordionTriggerWrapper/EFAccordionTriggerWrapper";
import EFAccordion from "../EFAccordion/EFAccordion";

describe("EFIconTriggerAccordion", () => {
  const children = <p>I am a child.</p>;
  const sibling = () => <h1>I am the trigger sibling.</h1>;

  let subject;

  beforeEach(() => {
    // eslint-disable-next-line react/no-children-prop
    subject = mount(<EFIconTriggerAccordion isOpenOnMount={false} sibling={sibling} children={children} />);
  });

  it("renders an accordion with the correct trigger", () => {
    expect(subject.find(EFAccordion).prop("triggerComponent").type).toEqual(EFAccordionTriggerWrapper);
  });

  it("renders an accordion with the correct props", () => {
    expect(subject.find(EFAccordion).prop("isOpen")).toEqual(false);
    expect(subject.find(EFAccordion).prop("classParentString")).toEqual("parentAccordion");
  });

  it("renders an EFAccordion with the correct children", () => {
    expect(subject.find("h1").text()).toEqual("I am the trigger sibling.");
  });
});
