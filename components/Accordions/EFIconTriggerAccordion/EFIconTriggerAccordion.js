import React, { useState } from "react";

import EFAccordionTriggerWrapper from "../AccordionTriggers/EFAccordionTriggerWrapper/EFAccordionTriggerWrapper";
import Style from "./EFIconTriggerAccordion.module.scss";
import EFAccordion from "../EFAccordion/EFAccordion";

const EFIconTriggerAccordion = ({ isOpenOnMount, sibling, children }) => {
  const [isOpen, toggleOpenState] = useState(isOpenOnMount);

  const triggerComponent = <EFAccordionTriggerWrapper isOpen={isOpen} onClickEvent={toggleOpenState} />;

  return (
    <EFAccordion
      classParentString={`${Style.parentAccordion}`}
      triggerComponent={triggerComponent}
      isOpen={isOpen}
      triggerSibling={sibling}
    >
      {children}
    </EFAccordion>
  );
};

export default EFIconTriggerAccordion;
