import React from "react";
import Collapsible from "react-collapsible";
// https://www.npmjs.com/package/react-collapsible

const EFAccordion = ({
  transitionSpeed,
  isOpen,
  triggerComponent,
  classParentString,
  triggerSibling,
  children,
  onOpening,
  onClosing
}) => {
  return (
    <Collapsible
      trigger={triggerComponent}
      open={isOpen}
      transitionTime={transitionSpeed}
      classParentString={classParentString}
      triggerSibling={triggerSibling}
      onOpening={onOpening}
      onClosing={onClosing}
    >
      {children}
    </Collapsible>
  );
};

EFAccordion.defaultProps = {
  transitionSpeed: 250,
  // isOpen can be used to control state of this component from a parent component
  // the component will properly open and close WITHOUT a parent handling state, this is for an override
  isOpen: false
};

export default EFAccordion;
