import React from "react";
import Style from "./PointRaceLeaderboardChild.module.scss";

const PointRaceLeaderboardChild = ({ name, rank, score, scoreLabel }) => {
  return (
    <div className={Style.memberItem}>
      <div className={Style.place}>{rank}</div>
      <p className={Style.text}>{name}</p>
      <p className={Style.kills}>
        {score} {scoreLabel}
      </p>
    </div>
  );
};

export default PointRaceLeaderboardChild;
