import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronDown, faInfoCircle } from "@fortawesome/pro-solid-svg-icons";
import Style from "./GeneralAccordionTrigger.module.scss";
import EFTooltip from "../../../tooltip/EFTooltip/EFTooltip";

const GeneralAccordionTrigger = ({ text, infoText, isOpen, onClickEvent }) => {
  return (
    <div
      className={`${Style.generalAccordionWrapper} ${isOpen && Style.isOpen}`}
      onClick={() => onClickEvent && onClickEvent(!isOpen)}
    >
      <p className={Style.generalText}>
        {text}
        {infoText && (
          <EFTooltip tooltipContent={infoText} tooltipPlacement="top">
            <FontAwesomeIcon icon={faInfoCircle} className={Style.infoCircle} />
          </EFTooltip>
        )}
      </p>
      <div className={Style.iconContainer}>
        <FontAwesomeIcon icon={faChevronDown} className={Style.icon} />
      </div>
    </div>
  );
};

export default GeneralAccordionTrigger;
