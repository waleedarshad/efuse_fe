import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronDown } from "@fortawesome/pro-regular-svg-icons";
import React from "react";
import { mount } from "enzyme";
import Style from "../GeneralAccordionTrigger/GeneralAccordionTrigger.module.scss";
import EFAccordionTriggerWrapper from "./EFAccordionTriggerWrapper";

const mockOnClickEvent = jest.fn();

describe("EFAccordionTriggerWrapper", () => {
  const triggerIcon = (
    <div className={Style.iconContainer}>
      <FontAwesomeIcon icon={faChevronDown} className={Style.icon} />
    </div>
  );

  it("renders the correct triggerIcon", () => {
    const subject = mount(
      <EFAccordionTriggerWrapper triggerIcon={triggerIcon} isOpen onClickEvent={mockOnClickEvent} />
    );

    expect(subject.find(FontAwesomeIcon).prop("icon")).toEqual(faChevronDown);
  });

  it("fires the correct method on click", () => {
    const subject = mount(
      <EFAccordionTriggerWrapper triggerIcon={triggerIcon} isOpen onClickEvent={mockOnClickEvent} />
    );

    subject.simulate("click");

    expect(mockOnClickEvent).toHaveBeenCalledWith(false);
  });

  it("fires the correct method on key down", () => {
    const subject = mount(
      <EFAccordionTriggerWrapper triggerIcon={triggerIcon} isOpen onClickEvent={mockOnClickEvent} />
    );

    subject.simulate("keydown", { keyCode: 13 });

    expect(mockOnClickEvent).toHaveBeenCalledWith(false);
  });
});
