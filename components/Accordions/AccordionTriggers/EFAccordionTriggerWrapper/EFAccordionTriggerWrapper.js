import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronDown } from "@fortawesome/pro-regular-svg-icons";
import Style from "./EFAccordionTriggerWrapper.module.scss";

const EFAccordionTriggerWrapper = ({ isOpen, onClickEvent }) => (
  <div
    role="button"
    tabIndex={0}
    className={`${Style.generalAccordionWrapper} ${isOpen && Style.isOpen}`}
    onClick={() => onClickEvent && onClickEvent(!isOpen)}
    onKeyDown={() => onClickEvent && onClickEvent(!isOpen)}
  >
    <div className={Style.iconContainer}>
      <FontAwesomeIcon icon={faChevronDown} className={Style.icon} />
    </div>
  </div>
);

export default EFAccordionTriggerWrapper;
