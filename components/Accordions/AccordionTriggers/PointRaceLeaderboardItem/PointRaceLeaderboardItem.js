import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronDown } from "@fortawesome/pro-solid-svg-icons";
import Style from "./PointRaceLeaderboardItem.module.scss";

const PointRaceLeaderboardItem = ({
  name,
  score,
  scoreLabel,
  rank,
  isOpen,
  isActive,
  displayDropdown,
  onClickEvent
}) => {
  return (
    <div
      className={`${Style.teamItem} ${isOpen && Style.isOpen} ${!isActive && Style.notActive}`}
      onClick={() => onClickEvent && onClickEvent(!isOpen)}
    >
      <div className={Style.place}>{rank}</div>
      <p className={Style.text}>{name}</p>
      <p className={Style.kills}>
        {score} {scoreLabel}
      </p>

      {displayDropdown && (
        <div className={Style.iconContainer}>
          <FontAwesomeIcon icon={faChevronDown} className={Style.icon} />
        </div>
      )}
    </div>
  );
};

export default PointRaceLeaderboardItem;
