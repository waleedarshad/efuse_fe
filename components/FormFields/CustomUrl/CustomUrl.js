import { Col } from "react-bootstrap";
import React, { useEffect, useState } from "react";

import InputLabelWithHelpTooltip from "../../InputLabelWithHelpTooltip/InputLabelWithHelpTooltip";
import InputField from "../../InputField/InputField";
import InputRow from "../../InputRow/InputRow";
import Style from "./CustomUrl.module.scss";
import { generateCustomUrl } from "../../../helpers/FormHelper";
import EFTooltip from "../../tooltip/EFTooltip/EFTooltip";

const CustomUrl = ({
  onChange,
  shortName,
  longName,
  pathType,
  isEditing,
  disabled,
  disabledTooltipText,
  name,
  isUrlValid,
  invalidUrlMessage,
  onBlur,
  autoPopulateUrl
}) => {
  const [fieldValue, setFieldValue] = useState(shortName || "");

  const getShortNameSuggestion = () => {
    return !longName ? "" : generateCustomUrl(longName);
  };

  const onValueChange = event => {
    setFieldValue(event.target.value);
    onChange(event);
  };

  useEffect(() => {
    if (!isEditing && autoPopulateUrl) {
      setFieldValue(getShortNameSuggestion());
      const event = {
        target: {
          value: getShortNameSuggestion(),
          name
        }
      };
      onChange(event);
    }
  }, [longName]);

  const inputField = (
    <InputField
      helperText="Must be lower case, short, and meaningful. This affects your page traffic from search engines."
      disabled={disabled}
      name={name}
      size="lg"
      placeholder="my-custom-url"
      theme="whiteShadow"
      onChange={onValueChange}
      required
      errorMessage="3-24 lower-case character alphanumeric and hyphen characters only."
      value={fieldValue}
      maxLength={24}
      minLength={3}
      pattern="^[0-9a-z-]*$"
      style={{ textTransform: "lowercase" }}
      onBlur={onBlur}
    />
  );

  return (
    <InputRow>
      <Col sm={12}>
        <InputLabelWithHelpTooltip
          labelText="Display URL"
          tooltipText="This will be the custom URL for your page. Choose carefully as you will not be able to edit this field in the future."
        />
        <div className={Style.shortNameContainer}>
          <span className={Style.shortNameURL}>https://efuse.gg/{pathType}/</span>
          <span style={{ width: "100%" }}>
            {disabled && disabledTooltipText ? (
              <EFTooltip tooltipContent={disabledTooltipText} tooltipPlacement="top">
                {inputField}
              </EFTooltip>
            ) : (
              inputField
            )}
            {!isUrlValid && <div className={Style.invalidUrl}>{invalidUrlMessage}</div>}
          </span>
        </div>
      </Col>
    </InputRow>
  );
};

CustomUrl.defaultProps = {
  name: "shortName",
  autoPopulateUrl: true
};

export default CustomUrl;
