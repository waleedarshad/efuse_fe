import React from "react";
import Dropdown from "react-dropdown";
import { FormControl } from "react-bootstrap";
import Style from "./DropdownField.module.scss";

const DropdownField = ({ name, options, onChange, value, placeholder, disabled, errorMessage, invalid }) => {
  return (
    <>
      <Dropdown
        name={name}
        options={options}
        onChange={e => onChange({ target: { ...e, name } })}
        value={value}
        placeholder={placeholder}
        className={Style.wrapperClass}
        controlClassName={`${Style.dropdownControl} ${disabled && Style.disabled}`}
        menuClassName={Style.menuClass}
        arrowClassName={`${Style.arrowClass} ${disabled && Style.hide}`}
        disabled={disabled}
        placeholderClassName={`${Style.placeholder} ${value && Style.placeholderActive}`}
      />
      {invalid && (
        <FormControl.Feedback type="invalid" style={{ display: "block" }}>
          {errorMessage}
        </FormControl.Feedback>
      )}
    </>
  );
};

export default DropdownField;
