import React, { useContext, useEffect } from "react";
import { UseFormReturn } from "react-hook-form/dist/types";
import EFCreateFlowLayout from "../../EFCreateFlowLayout/EFCreateFlowLayout";
import EFInputControl from "../../FormControls/EFInputControl";
import EFCustomURLControl from "../../FormControls/EFCustomURLControl";
import Style from "./CreateOrganizationFlow.module.scss";
import { EFWizardContext } from "../../EFWizard/EFWizard";
import EFSelectGamesControl from "../../FormControls/EFSelectGamesControl";

interface StepOneInfo {
  name: string;
  slug: string;
  associatedGames: [string];
}

interface StepOneProps {
  form: UseFormReturn;
  onSubmit: (data: StepOneInfo) => void;
}

const StepOne = ({ form, onSubmit }: StepOneProps) => {
  const {
    control,
    handleSubmit,
    watch,
    formState: { errors }
  } = form;

  useEffect(() => {
    analytics.track("ORGANIZATION_CREATION_STEP_ONE");
  }, []);

  const { goNextStep } = useContext(EFWizardContext);
  const onValid = data => {
    onSubmit(data);
    goNextStep();
  };

  const [name] = watch(["name"]);

  return (
    <EFCreateFlowLayout
      titleText="Create Organization"
      titleSubText="Join hundreds of elite organizations on eFuse. Create your organization portfolio, post content, and invite members to join."
      backgroundImageSrc="https://www.callofduty.com/content/dam/atvi/callofduty/cod-touchui/blog/hero/bocw/BOCW-S4-Announcement-TOUT.jpg"
      onNext={handleSubmit(onValid)}
    >
      <div>
        <div className={Style.inputMargin}>
          <EFInputControl
            control={control}
            errors={errors}
            required
            label="Organization Name"
            name="name"
            maxLength={70}
            placeholder="Enter organization name"
          />
        </div>
        <div className={Style.inputMargin}>
          <EFCustomURLControl
            slugType="organizations"
            control={control}
            name="shortName"
            pathType="org"
            longName={name}
            errors={errors}
            required
            disabled={false}
          />
        </div>
        <div className={Style.inputMargin}>
          <EFSelectGamesControl
            label="Select games your organization is associated with"
            errors={errors}
            name="associatedGames"
            required={false}
            control={control}
            selectMultiple
          />
        </div>
      </div>
    </EFCreateFlowLayout>
  );
};

export default StepOne;
