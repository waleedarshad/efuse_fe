import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";
import { useForm } from "react-hook-form";
import { useMutation } from "@apollo/client";
import parse from "url-parse";

import EFWizard from "../../EFWizard/EFWizard";
import StepOne from "./StepOne";
import StepTwo from "./StepTwo";
import StepThree from "./StepThree";
import {
  CREATE_ORGANIZATION,
  CreateOrganizationDataResponse,
  CreateOrganizationVars
} from "../../../graphql/organizations/Organizations";
import { Organization } from "../../../graphql/interface/Organization";

const CreateOrganizationFlow = () => {
  const router = useRouter();
  const [organizationInfo, setOrganizationInfo] = useState<Organization>({});
  const [createOrganization, { loading }] = useMutation<CreateOrganizationDataResponse, CreateOrganizationVars>(
    CREATE_ORGANIZATION
  );

  useEffect(() => {
    analytics.page("Create Organization Page");
  }, []);

  const submitPage = data => {
    setOrganizationInfo(info => ({ ...info, ...data }));
  };

  const submitLastPage = async data => {
    const finalData = { ...organizationInfo, ...data };

    const newOrganizationResponse = await createOrganization({ variables: { organization: finalData } });

    // custom redirect / optional / primarily used in league flow
    // eslint-disable-next-line camelcase
    const { redirect_url } = router.query;

    analytics.track("ORGANIZATION_CREATE", { formVersion: 2 });

    // eslint-disable-next-line camelcase
    const finalRedirect = redirect_url
      ? `${parse(redirect_url, true).href}?new_org_id=${newOrganizationResponse.data.createOrganization._id}`
      : `/org/${newOrganizationResponse.data.createOrganization.shortName}`;

    router.push(finalRedirect);
  };

  return (
    <EFWizard>
      <StepOne form={useForm()} onSubmit={submitPage} />
      <StepTwo form={useForm()} onSubmit={submitPage} />
      <StepThree form={useForm()} onSubmit={submitLastPage} isLoading={loading} />
    </EFWizard>
  );
};

export default CreateOrganizationFlow;
