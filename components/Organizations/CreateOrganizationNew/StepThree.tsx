/* eslint-disable no-unused-vars */
import React, { useEffect } from "react";
import { UseFormReturn } from "react-hook-form/dist/types";
import EFCreateFlowLayout from "../../EFCreateFlowLayout/EFCreateFlowLayout";
import Style from "./CreateOrganizationFlow.module.scss";
import EFCardSelectionControl from "../../FormControls/EFCardSelectionControl";
import dropdowns from "../EditOrganization/OrganizationForm/data/dropdowns.json";

interface StepThreeInfo {
  status: string;
}

interface StepThreeProps {
  form: UseFormReturn;
  onSubmit: (data: StepThreeInfo) => void;
  isLoading: boolean;
}

const StepThree = ({ form, onSubmit, isLoading }: StepThreeProps) => {
  const {
    control,
    handleSubmit,
    formState: { errors }
  } = form;

  useEffect(() => {
    analytics.track("ORGANIZATION_CREATION_STEP_THREE");
  }, []);

  return (
    <EFCreateFlowLayout
      titleText="Last step and you’re all set!"
      titleSubText=""
      backgroundImageSrc="https://www.callofduty.com/content/dam/atvi/callofduty/cod-touchui/blog/hero/bocw/BOCW-S4-Announcement-TOUT.jpg"
      onNext={handleSubmit(onSubmit)}
      isLoading={isLoading}
      isRightButtonDisabled={isLoading}
    >
      <div>
        <div className={Style.inputMargin}>
          <EFCardSelectionControl
            control={control}
            errors={errors}
            name="status"
            label="Who can join this organization?"
            cards={dropdowns.status}
            size="large"
            required
          />
        </div>
      </div>
    </EFCreateFlowLayout>
  );
};

export default StepThree;
