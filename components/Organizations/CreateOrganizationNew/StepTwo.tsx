import React, { useContext, useEffect } from "react";
import { UseFormReturn } from "react-hook-form/dist/types";
import EFCreateFlowLayout from "../../EFCreateFlowLayout/EFCreateFlowLayout";
import EFCardSelectionControl from "../../FormControls/EFCardSelectionControl";
import Style from "./CreateOrganizationFlow.module.scss";
import { EFWizardContext } from "../../EFWizard/EFWizard";
import dropdowns from "../EditOrganization/OrganizationForm/data/dropdowns.json";

interface StepTwoInfo {
  type: string;
}

interface StepTwoProps {
  form: UseFormReturn;
  onSubmit: (data: StepTwoInfo) => void;
}

const StepTwo = ({ form, onSubmit }: StepTwoProps) => {
  const {
    control,
    handleSubmit,
    formState: { errors }
  } = form;

  useEffect(() => {
    analytics.track("ORGANIZATION_CREATION_STEP_TWO");
  }, []);

  const { goNextStep } = useContext(EFWizardContext);
  const onValid = data => {
    onSubmit(data);
    goNextStep();
  };

  return (
    <EFCreateFlowLayout
      titleText="You’re almost done!"
      titleSubText=""
      backgroundImageSrc="https://www.callofduty.com/content/dam/atvi/callofduty/cod-touchui/blog/hero/bocw/BOCW-S4-Announcement-TOUT.jpg"
      onNext={handleSubmit(onValid)}
    >
      <div>
        <div className={Style.inputMargin}>
          <EFCardSelectionControl
            control={control}
            errors={errors}
            name="organizationType"
            label="Organization Type"
            cards={dropdowns.organizationType}
            size="large"
            required
          />
        </div>
      </div>
    </EFCreateFlowLayout>
  );
};

export default StepTwo;
