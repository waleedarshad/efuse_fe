import { Col } from "react-bootstrap";
import React from "react";
import TableItemOrganization from "../../Table/TableItem/TableItemOrganization";

import EFIconTriggerAccordion from "../../../Accordions/EFIconTriggerAccordion/EFIconTriggerAccordion";

const ChildOrganizationsList = ({ organizations, authUser, organizationSlug }) => {
  const organizationsList = organizations.map((organization, index) => {
    const childOrgs = organization.children
      ? organization?.children.map((org, ind) => {
          return (
            <Col lg={12} md={12} key={ind} style={{ paddingRight: "0px" }}>
              <TableItemOrganization organization={org} currentUser={authUser} institutionSlug={organizationSlug} />
            </Col>
          );
        })
      : [];

    if (childOrgs.length > 0) {
      return (
        <Col lg={12} md={12} key={index}>
          <EFIconTriggerAccordion
            isOpenOnMount={false}
            sibling={() => (
              <TableItemOrganization
                organization={organization}
                currentUser={authUser}
                institutionSlug={organizationSlug}
              />
            )}
          >
            {childOrgs}
          </EFIconTriggerAccordion>
        </Col>
      );
    }
    return (
      <Col lg={12} md={12} key={index}>
        <TableItemOrganization organization={organization} currentUser={authUser} institutionSlug={organizationSlug} />
      </Col>
    );
  });

  return organizationsList;
};

export default ChildOrganizationsList;
