import React, { Component } from "react";
import { connect } from "react-redux";

import FollowButton from "./FollowButton/FollowButton";
import { followOrganization, unfollowOrganization } from "../../../store/actions/organizationFollowerActions";
import SignupModal from "../../SignupModal/SignupModal";

class FollowOrganization extends Component {
  render() {
    const { organization, followOrganization, unfollowOrganization, currentUser, theme } = this.props;

    const userLoggedIn = currentUser?.id;
    return (
      <>
        {organization.isCurrentUserAFollower ? (
          <FollowButton
            children="Unfollow"
            disabled={organization.disableFollow || !organization._id}
            onClick={() => unfollowOrganization(organization._id)}
            theme={theme}
          />
        ) : userLoggedIn ? (
          <FollowButton
            children="Follow"
            disabled={organization.disableFollow || !organization._id}
            onClick={() => followOrganization(organization._id)}
            theme={theme}
          />
        ) : (
          <SignupModal
            title={`Create an account to follow ${organization?.name}.`}
            message={`Never miss an update from ${organization?.name}. Sign up today!`}
          >
            <FollowButton children="Follow" disabled={organization.disableFollow} theme={theme} />
          </SignupModal>
        )}
      </>
    );
  }
}

const mapStateToProps = state => ({
  currentUser: state.auth.currentUser
});

export default connect(mapStateToProps, {
  followOrganization,
  unfollowOrganization
})(FollowOrganization);
