import React, { memo } from "react";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";

const FollowButton = ({ children, disabled, onClick }) => {
  return <EFRectangleButton text={children} disabled={disabled} onClick={onClick} />;
};

export default memo(FollowButton);
