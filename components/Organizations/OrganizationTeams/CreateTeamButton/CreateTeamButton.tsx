import React, { useState } from "react";
import { useDispatch } from "react-redux";

import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";
import TeamModal from "../../../Modals/TeamModal/TeamModal";
import { createTeam } from "../../../../store/actions/teamActions";

interface CreateTeamButtonProps {
  organizationId: String;
  buttonWidth?: "fullWidth" | "autoWidth" | "fitContent";
}

const CreateTeamButton: React.FC<CreateTeamButtonProps> = ({ organizationId, buttonWidth = "fitContent" }) => {
  const dispatch = useDispatch();

  const [isModalOpen, toggleModal] = useState(false);

  const onClose = () => {
    toggleModal(false);
  };

  const onSubmit = team => {
    const teamData = {
      name: team.name,
      game: team.game,
      owner: organizationId,
      ownerType: "organizations"
    };

    dispatch(createTeam(teamData, team.userIds));
  };
  return (
    <div>
      <TeamModal isOpen={isModalOpen} onClose={onClose} onSubmit={onSubmit} />
      <EFRectangleButton text="Create Team" width={buttonWidth} onClick={() => toggleModal(true)} />
    </div>
  );
};

export default CreateTeamButton;
