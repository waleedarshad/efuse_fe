import React from "react";
import { Col, Row } from "react-bootstrap";
import uniqueId from "lodash/uniqueId";

import EFTeamDetailCard from "../../../Cards/EFTeamDetailCard/EFTeamDetailCard";
import CreateTeamButton from "../CreateTeamButton/CreateTeamButton";

const OrganizationTeamsList = ({ teams, isAdmin, organizationId }) => {
  const teamsList = teams.map(team => {
    return (
      <Col lg={4} md={6} className="mb-4" key={uniqueId()}>
        <EFTeamDetailCard
          imageUrl={team.image.url}
          teamName={team.name}
          teamMembers={team.members}
          nbTeamMembers={team.members.length}
          isAdmin={isAdmin}
          addMembers={() => {}}
          manage={() => {}}
        />
      </Col>
    );
  });
  return (
    <>
      {isAdmin && (
        <div className="mb-5">
          <CreateTeamButton organizationId={organizationId} />
        </div>
      )}
      <Row>{teamsList}</Row>
    </>
  );
};

export default OrganizationTeamsList;
