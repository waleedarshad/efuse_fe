import React from "react";

import CreateTeamButton from "../CreateTeamButton/CreateTeamButton";
import Style from "./CreateFirstTeam.module.scss";

const CreateFirstTeam = ({ organizationId }) => {
  return (
    <div className={Style.mainWrapper}>
      <div className={Style.contentWrapper}>
        <div className={Style.heading}>
          <p>Your Org currently has no teams</p>
        </div>
        <div>
          <CreateTeamButton organizationId={organizationId} buttonWidth="fullWidth" />
        </div>
      </div>
    </div>
  );
};

export default CreateFirstTeam;
