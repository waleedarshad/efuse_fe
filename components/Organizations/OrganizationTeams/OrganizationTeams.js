import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useRouter } from "next/router";

import OrganizationLayout from "../../Layouts/OrganizationLayout/OrganizationLayout";
import { getImage, rescueNil } from "../../../helpers/GeneralHelper";
import EFCard from "../../Cards/EFCard/EFCard";
import CreateFirstTeam from "./CreateFirstTeam/CreateFirstTeam";
import { getOwnerAndCaptain } from "../../../helpers/OrganizationHelper";
import { getTeamsByOwner } from "../../../store/actions/teamActions";
import OrganizationTeamsList from "./OrganizationTeamsList/OrganizationTeamsList";

const OrganizationTeams = () => {
  const dispatch = useDispatch();
  const router = useRouter();

  const organization = useSelector(state => state.organizations.organization);
  const currentUser = useSelector(state => state.auth.currentUser);
  const teams = useSelector(state => state.teams.teams);

  useEffect(() => {
    dispatch(getTeamsByOwner(router.query.id));
  }, [router.query.id]);

  const { owner, captain } = getOwnerAndCaptain(currentUser, organization);
  const isAdmin = owner || captain;

  return (
    <OrganizationLayout
      headerImage={getImage(organization?.headerImage, "organization")}
      avatar={rescueNil(organization, "profileImage", {}) || {}}
      bar="Organizations"
      orgType={organization?.status}
    >
      {isAdmin && !teams?.length > 0 && (
        <EFCard widthTheme="fullWidth" shadow="none">
          <CreateFirstTeam organizationId={organization?._id} />
        </EFCard>
      )}
      {teams?.length > 0 && (
        <OrganizationTeamsList teams={teams} isAdmin={isAdmin} organizationId={organization?._id} />
      )}
    </OrganizationLayout>
  );
};

export default OrganizationTeams;
