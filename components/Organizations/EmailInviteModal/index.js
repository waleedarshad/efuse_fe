import React, { Component } from "react";
import { connect } from "react-redux";
import { ReactMultiEmail, isEmail } from "react-multi-email";
import Style from "./Style.module.scss";
import "react-multi-email/style.css";
import { inviteUsersViaEmail } from "../../../store/actions/organizationActions";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";

class EmailInviteModal extends Component {
  state = {
    emails: []
  };

  render() {
    const { organization, inviteCode, closeModal } = this.props;
    const { emails } = this.state;

    return (
      <div className={Style.modalContainer}>
        <div className={Style.inputContainer}>
          <label>Email</label>
          <ReactMultiEmail
            placeholder="type emails here"
            emails={emails}
            onChange={_emails => {
              this.setState({ emails: _emails });
            }}
            validateEmail={email => {
              return isEmail(email); // return boolean
            }}
            getLabel={(email, index, removeEmail) => {
              return (
                <div data-tag key={index}>
                  {email}
                  <span data-tag-handle onClick={() => removeEmail(index)}>
                    ×
                  </span>
                </div>
              );
            }}
          />
          <div className={Style.ButtonContainer}>
            <EFRectangleButton
              text="Invite Users"
              onClick={() => {
                this.props.inviteUsersViaEmail(organization._id, emails, inviteCode);
                closeModal();
              }}
            />
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  user: state.user.currentUser,
  currentUser: state.auth.currentUser,
  organization: state.organizations.organization
});

export default connect(mapStateToProps, { inviteUsersViaEmail })(EmailInviteModal);
