import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { useRouter } from "next/router";
import { Col, Nav } from "react-bootstrap";
import { faBinoculars } from "@fortawesome/pro-solid-svg-icons";
import Link from "next/link";
import { InstantSearch, Configure } from "react-instantsearch-dom";

import OrganizationListing from "./OrganizationListing/OrganizationListing";
import OrganizationFilters from "./OrganizationFilters/OrganizationFilters";
import Style from "./OrganizationWrapper.module.scss";
import { getDiscoverNavigationList } from "../../../navigation/discover";
import EFSubHeader from "../../Layouts/Internal/EFSubHeader/EFSubHeader";
import AuthAwareLayout from "../../Layouts/AuthAwareLayout/AuthAwareLayout";
import { algoliaServerSideSearch, getIndexName } from "../../../helpers/AlgoliaHelper";
import NameSearchBox from "./OrganizationFilters/NameSearchBox";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";
import { SEARCH_ORGANIZATION } from "../../../graphql/algolia/SearchQuery";

const OrganizationWrapper = ({ view }) => {
  const router = useRouter();

  const algoliaIndexName = getIndexName("ORGANIZATIONS");

  const organizationCreateButtons = useSelector(state => state.features.organization_create_buttons);
  const currentUser = useSelector(state => state.auth.currentUser);
  const isAuthenticated = useSelector(state => state.auth.isAuthenticated);
  const showErenaLink = useSelector(state => state.features.web_erena_in_discover_navigation);

  useEffect(() => {
    analytics.page(`Organizations ${view !== "owned" ? "Browse" : "Applied"}`);
    analytics.track(`ORGANIZATION_${view !== "owned" ? "BROWSE" : "OWNED"}_LIST_VIEW`);
  }, [currentUser]);

  return (
    <AuthAwareLayout metaTitle="eFuse | Organizations" containsSubheader>
      <EFSubHeader
        headerIcon={faBinoculars}
        navigationList={getDiscoverNavigationList(router.pathname, showErenaLink)}
      />
      <InstantSearch
        searchClient={{
          search: requests => algoliaServerSideSearch(SEARCH_ORGANIZATION, { requests }, "OrganizationAlgoliaSearch")
        }}
        indexName={algoliaIndexName}
      >
        <Configure
          enablePersonalization
          userToken={currentUser?._id}
          hitsPerPage={9}
          distinct={false}
          analytics={false}
          filters={isAuthenticated && router.pathname === "/organizations/owned" ? `user._id:${currentUser?._id}` : ""}
        />
        <Col lg={3} md={4} className={`pl-sm-0 ${!isAuthenticated && Style.topContainer}`}>
          <OrganizationFilters />
        </Col>
        <Col md={8} lg={9} className={`${!isAuthenticated && Style.topContainer}`}>
          <div className={Style.orgBtnWrapper}>
            <Link href="/organizations">
              <Nav.Link
                href="/organizations"
                className={`${Style.link} ${router.route === "/organizations" && Style.active}`}
              >
                All
              </Nav.Link>
            </Link>
            {isAuthenticated && (
              <Link href="/organizations/owned">
                <Nav.Link
                  href="/organizations/owned"
                  className={`${Style.link} ${router.route === "/organizations/owned" && Style.active}`}
                >
                  My Organizations
                </Nav.Link>
              </Link>
            )}
            {(organizationCreateButtons || currentUser?.verified) && isAuthenticated && (
              <div className={Style.create}>
                <EFRectangleButton internalHref="/organizations/create" text="Create Organization" />
              </div>
            )}
          </div>
          <div className={Style.searchBox}>
            <NameSearchBox attribute="name" />
          </div>

          <OrganizationListing indexName={algoliaIndexName} />
        </Col>
      </InstantSearch>
    </AuthAwareLayout>
  );
};

export default OrganizationWrapper;
