import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Col, Row } from "react-bootstrap";
import { connectStateResults } from "react-instantsearch-dom";
import uniqueId from "lodash/uniqueId";

import {
  toggleModalShow,
  changePublishStatus,
  removeOrganization,
  joinOrganization,
  leaveOrganization
} from "../../../../store/actions/organizationActions";
import OrganizationListItem from "./OrganizationListItem/OrganizationListItem";
import NoRecordFound from "../../../NoRecordFound/NoRecordFound";
import { triggerDebouncedSegmentEvent } from "../../../../helpers/AlgoliaHelper";
import Pagination from "../../../Algolia/Pagination/Pagination";
import SkeletonLoader from "../../../SkeletonLoader/SkeletonLoader";

const OrganizationListing = connectStateResults(({ indexName, searchResults, searching }) => {
  if (searchResults?.hits) {
    // segment event needed for personalization
    triggerDebouncedSegmentEvent(searchResults.hits, "ORGANIZATIONS_LISTING", indexName);
  }

  const dispatch = useDispatch();

  const currentUser = useSelector(state => state.auth.currentUser);
  const modalShow = useSelector(state => state.organizations.modalShow);

  const onJoinClick = (id, canJoin, status) => {
    if (canJoin) dispatch(joinOrganization(`organizations/join/${id}`, status));
    else leaveOrganization(id, status);
  };

  const changeOrgStatus = (orgId, status) => {
    const publishStatus = { status };
    dispatch(changePublishStatus(`organizations/${orgId}/change_status`, publishStatus));
  };

  const statusSwitch = {
    pending: "Request Sent",
    banned: "Banned",
    approved: "Joined",
    kicked: "Join",
    new: "Join",
    owner: "Owned"
  };

  let mainComponent = searchResults?.hits.map(organization => (
    <OrganizationListItem
      key={organization._id}
      organization={organization}
      currentUser={currentUser}
      onJoinClick={onJoinClick}
      statusSwitch={status => statusSwitch[status]}
      toggleModalShow={orgId => dispatch(toggleModalShow(orgId))}
      modalShow={modalShow}
      changeOrgStatus={changeOrgStatus}
      removeOrganization={orgId => dispatch(removeOrganization(orgId))}
    />
  ));

  // Displaying loading skeleton effect if searching
  if (searching)
    mainComponent = [1, 2, 3].map(() => (
      <Col key={uniqueId()}>
        <SkeletonLoader skeletons={3} postHeight={400} engagementHeight={0} />
      </Col>
    ));

  // Display message if no search results are found
  if (searchResults?.nbHits === 0) mainComponent = <NoRecordFound />;

  return (
    <>
      <Row>{mainComponent}</Row>
      {/* Pagination component needs to show on every render for algolia to work */}
      {searchResults?.nbPages > 1 && <Pagination />}
    </>
  );
});

export default OrganizationListing;
