import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Col, Row } from "react-bootstrap";
import uniqueId from "lodash/uniqueId";
import { useQuery } from "@apollo/client";

import {
  toggleModalShow,
  changePublishStatus,
  removeOrganization,
  joinOrganization,
  leaveOrganization
} from "../../../../store/actions/organizationActions";
import OrganizationListItem from "./OrganizationListItem/OrganizationListItem";
import NoRecordFound from "../../../NoRecordFound/NoRecordFound";
import SkeletonLoader from "../../../SkeletonLoader/SkeletonLoader";
import {
  GET_USER_ORGANIZATIONS,
  getUserOrganizationsData,
  getUserOrganizationsVars
} from "../../../../graphql/organizations/Organizations";
import { StatusSwitch } from "../../../../enums";

const MyOrganizationListing = ({ search }) => {
  const dispatch = useDispatch();

  const currentUser = useSelector(state => state.auth.currentUser);
  const modalShow = useSelector(state => state.organizations.modalShow);
  const { data, loading } = useQuery<getUserOrganizationsData, getUserOrganizationsVars>(GET_USER_ORGANIZATIONS, {
    variables: { userId: currentUser?._id, includeOrgsWhereMember: true }
  });

  const onJoinClick = (id, canJoin, status) => {
    if (canJoin) dispatch(joinOrganization(`organizations/join/${id}`, status));
    else leaveOrganization(id, status);
  };

  const changeOrgStatus = (orgId, status) => {
    const publishStatus = { status };
    dispatch(changePublishStatus(`organizations/${orgId}/change_status`, publishStatus));
  };

  let mainComponent = data?.getUserOrganizations
    .filter(organization => organization.name.toLowerCase().includes(search.toLowerCase()))
    .map(organization => (
      <OrganizationListItem
        key={organization._id}
        organization={organization}
        currentUser={currentUser}
        onJoinClick={onJoinClick}
        statusSwitch={status => StatusSwitch[status]}
        toggleModalShow={orgId => dispatch(toggleModalShow(orgId))}
        modalShow={modalShow}
        changeOrgStatus={changeOrgStatus}
        removeOrganization={orgId => dispatch(removeOrganization(orgId))}
        isMyOrganization
      />
    ));

  // Displaying loading skeleton effect if searching
  if (loading)
    mainComponent = [1, 2, 3].map(() => (
      <Col key={uniqueId()}>
        <SkeletonLoader skeletons={3} postHeight={400} engagementHeight={0} />
      </Col>
    ));

  // Display message if no search results are found
  if (data?.getUserOrganizations?.length === 0) mainComponent = <NoRecordFound />;

  return (
    <>
      <Row>{mainComponent}</Row>
      {/* Pagination component needs to show on every render for algolia to work */}
      {/* {searchResults?.nbPages > 1 && <Pagination />} */}
    </>
  );
};

export default MyOrganizationListing;
