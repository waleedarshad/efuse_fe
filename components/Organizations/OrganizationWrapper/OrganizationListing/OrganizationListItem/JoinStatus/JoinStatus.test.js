import { mount } from "enzyme";
import React from "react";
import JoinStatus from "./JoinStatus";
import ConfirmAlert from "../../../../../ConfirmAlert/ConfirmAlert";
import SignupModal from "../../../../../SignupModal/SignupModal";
import EFRectangleButton from "../../../../../Buttons/EFRectangleButton/EFRectangleButton";

describe("JoinStatus", () => {
  const mockOnJoinClick = jest.fn();
  const mockT = jest.fn();
  const mockStatusSwitch = jest.fn();

  mockT.mockImplementation(() => "a string");

  it("renders an action button with a confirmation alert if user is logged in and can join", () => {
    const organization = {
      member: false
    };
    const subject = mount(
      <JoinStatus
        canJoin
        loggedIn
        onJoinClick={mockOnJoinClick}
        statusSwitch={mockStatusSwitch}
        requestStatus="new"
        organization={organization}
        t={mockT}
      />
    );

    expect(subject.find(ConfirmAlert)).toHaveLength(1);
  });

  it("renders an action button wrapped in a sign up modal if user is not logged in", () => {
    const organization = {};
    const subject = mount(
      <JoinStatus
        onJoinClick={mockOnJoinClick}
        statusSwitch={mockStatusSwitch}
        requestStatus="new"
        organization={organization}
        t={mockT}
      />
    );

    expect(subject.find(SignupModal)).toHaveLength(1);
  });

  it("renders an action button with status if user is logged in but cannot join", () => {
    const organization = {};
    const subject = mount(
      <JoinStatus
        loggedIn
        onJoinClick={mockOnJoinClick}
        statusSwitch={mockStatusSwitch}
        requestStatus="new"
        organization={organization}
        t={mockT}
      />
    );

    expect(subject.find(EFRectangleButton)).toHaveLength(1);
    expect(subject.find(ConfirmAlert)).toHaveLength(0);
    expect(subject.find(SignupModal)).toHaveLength(0);
  });
});
