import React, { memo } from "react";
import ConfirmAlert from "../../../../../ConfirmAlert/ConfirmAlert";
import SignupModal from "../../../../../SignupModal/SignupModal";
import EFRectangleButton from "../../../../../Buttons/EFRectangleButton/EFRectangleButton";

const JoinStatus = ({ canJoin, loggedIn, onJoinClick, statusSwitch, requestStatus, organization }) => {
  const isLoggedInAndCanJoin = () => {
    return loggedIn && canJoin && !organization.member;
  };

  const status = statusSwitch(requestStatus);

  if (isLoggedInAndCanJoin()) {
    return (
      <ConfirmAlert
        title="Confirmation"
        message="Please confirm that you would like to join this organization."
        onYes={() => onJoinClick(organization._id, canJoin, organization.status)}
      >
        <EFRectangleButton text={status} />
      </ConfirmAlert>
    );
  }

  if (!loggedIn) {
    return (
      <SignupModal
        title={`Create an account to follow ${organization?.name}.`}
        message={`Never miss an update from ${organization?.name}. Sign up today!`}
      >
        <EFRectangleButton text={status} />
      </SignupModal>
    );
  }

  return <EFRectangleButton text={organization.member ? "Joined" : status} disabled />;
};

export default memo(JoinStatus);
