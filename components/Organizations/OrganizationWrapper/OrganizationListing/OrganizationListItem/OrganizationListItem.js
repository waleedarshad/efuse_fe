import { Card, Col, OverlayTrigger, Tooltip } from "react-bootstrap";
import Link from "next/link";
import { faPlus, faEyeSlash, faEye, faPencilAlt, faTrash } from "@fortawesome/pro-solid-svg-icons";

import Style from "./OrganizationListItem.module.scss";
import CustomDropdown from "../../../../CustomDropdown/CustomDropdown";
import OrganizationVerififed from "../../../../OrganizationVerified/OrganizationVerified";
import JoinStatus from "./JoinStatus/JoinStatus";
import ModalPopup from "../../../ModalPopup/ModalPopup";
import FollowOrganization from "../../../FollowOrganization/FollowOrganization";

import { withCdn } from "../../../../../common/utils";
import { getOrganizationUrl } from "../../../../../helpers/UrlHelper";
import EFRectangleButton from "../../../../Buttons/EFRectangleButton/EFRectangleButton";
import Highlight from "../../../../Algolia/Highlight/Highlight";

const OrganizationListItem = ({
  organization,
  currentUser,
  onJoinClick,
  statusSwitch,
  toggleModalShow,
  modalShow,
  changeOrgStatus,
  removeOrganization,
  isMyOrganization
}) => {
  const isOwner = organization.user?._id === currentUser?.id;
  const isAdmin = currentUser?.roles?.includes("admin");
  const canJoin = !organization.currentUserRequestToJoin || organization.currentUserRequestToJoin?.status === "kicked";
  const userLoggedIn = currentUser?._id;
  const requestStatus = !organization.currentUserRequestToJoin ? "new" : organization.currentUserRequestToJoin?.status;
  let menuItems = [
    {
      title: "Quick Details",
      path: getOrganizationUrl(organization),
      icon: faPlus
    }
  ];
  const ownerMenu = [];
  if (
    currentUser &&
    isAdmin
    // in case we want users to be able to publish or hide themselves
    // currentUser &&
    // organization.user &&
    // currentUser.id === organization.user
  ) {
    if (organization.publishStatus === undefined || organization.publishStatus === "visible") {
      ownerMenu.push({
        title: "Hide",
        path: "",
        icon: faEyeSlash,
        as: "button",
        onClick: () => changeOrgStatus(organization._id, "hidden")
      });
    } else {
      ownerMenu.push({
        title: "Publish",
        path: "",
        icon: faEye,
        as: "button",
        onClick: () => changeOrgStatus(organization._id, "visible")
      });
    }
  }
  if ((currentUser && organization.user && currentUser.id === organization.user) || isAdmin) {
    menuItems = [
      {
        title: "Edit",
        path: `/organizations/edit/${organization._id}`,
        icon: faPencilAlt
      },
      ...ownerMenu,
      ...menuItems,
      {
        title: "Delete",
        as: "button",
        icon: faTrash,
        onClick: () => removeOrganization(organization._id),
        message: `WARNING: You are about to delete the organization named: ${organization.name}. Would you like to proceed?`
      }
    ];
  }

  return (
    <Col lg={4} md={6} className="mb-4">
      <Card className={`customCard ${Style.cardWrapper}`}>
        <div className={Style.cardTopWrapper}>
          <OverlayTrigger
            placement="top"
            overlay={
              <Tooltip id={`tooltip-${organization.status}`} style={{ zIndex: 9999 }}>
                {organization.status === "Open" && "Users can join at any time"}
                {organization.status === "Closed" && "Users cannot join"}
                {organization.status === "Request" && "Users must request to join"}
                {organization.status === "Invite" && "Users must be invited to join"}
              </Tooltip>
            }
          >
            <span className={Style.status}>{organization.status}</span>
          </OverlayTrigger>
          <span className={Style.menu}>
            <CustomDropdown icon="dots" items={menuItems} customClass="whiteBg" />
          </span>

          <img
            className={Style.headerImage}
            src={
              (organization.headerImage && organization.headerImage?.url) || withCdn("/static/images/placeholder.png")
            }
            alt="header"
          />
        </div>
        <span className={Style.extendedBg} />
        <Link href={getOrganizationUrl(organization)}>
          <div className={Style.cardImageWrapper}>
            <Card.Img
              className={Style.cardImage}
              src={
                (organization.profileImage && organization.profileImage?.url) ||
                withCdn("/static/images/placeholder.png")
              }
            />
          </div>
        </Link>
        <Card.Body className={Style.cardBody}>
          <Card.Title>
            <Link href={getOrganizationUrl(organization)}>
              <a
                href={getOrganizationUrl(organization)}
                className={Style.titleLink}
                data-cy={`org-${organization.name}`}
              >
                <h5 className={Style.title}>
                  {isMyOrganization ? organization?.name : <Highlight hit={organization} attribute="name" />}
                </h5>
                <OrganizationVerififed verified={organization.verified} />
              </a>
            </Link>
            <h6 className={Style.subTitle}>{organization.organizationType}</h6>
          </Card.Title>
          <Card.Text className={Style.description}>
            {isMyOrganization ? organization?.name : <Highlight hit={organization} attribute="description" />}
          </Card.Text>
          <div className={Style.actionWrapper}>
            {organization.status !== "Closed" && organization.status !== "Invite" && !isOwner && (
              <JoinStatus
                canJoin={canJoin}
                loggedIn={userLoggedIn}
                statusSwitch={statusSwitch}
                onJoinClick={onJoinClick}
                requestStatus={requestStatus}
                organization={organization}
              />
            )}
            {organization.status === "Invite" && isOwner && (
              <>
                <EFRectangleButton text="Invite" onClick={() => toggleModalShow(organization._id)} />
                <ModalPopup modalShow={modalShow} toggleModalShow={toggleModalShow} />
              </>
            )}
            {!isOwner && organization && <FollowOrganization organization={organization} />}
          </div>
        </Card.Body>
        <Card.Footer className={Style.cardFooter}>
          <div className={Style.footerItem}>
            <span className={Style.footerLabel}>Followers</span>
            <span className={Style.footerValue}>{organization.followersCount || 0}</span>
          </div>
          <div className={Style.footerItem}>
            <span className={Style.footerLabel}>Members</span>
            <span className={Style.footerValue}>{organization.membersCount || 0}</span>
          </div>
        </Card.Footer>
      </Card>
    </Col>
  );
};

export default OrganizationListItem;
