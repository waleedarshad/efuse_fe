import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useRouter } from "next/router";
import { Col, Nav } from "react-bootstrap";
import { faBinoculars } from "@fortawesome/pro-solid-svg-icons";
import Link from "next/link";
import MyOrganizationListing from "./OrganizationListing/MyOrganizationListing";
import Style from "./OrganizationWrapper.module.scss";
import { getDiscoverNavigationList } from "../../../navigation/discover";
import EFSubHeader from "../../Layouts/Internal/EFSubHeader/EFSubHeader";
import AuthAwareLayout from "../../Layouts/AuthAwareLayout/AuthAwareLayout";
import SearchField from "../../SearchField/SearchField";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";

const MyOrganizationWrapper = ({ view }) => {
  const router = useRouter();

  const organizationCreateButtons = useSelector(state => state.features.organization_create_buttons);
  const currentUser = useSelector(state => state.auth.currentUser);
  const isAuthenticated = useSelector(state => state.auth.isAuthenticated);
  const showErenaLink = useSelector(state => state.features.web_erena_in_discover_navigation);
  const [search, setSearch] = useState("");

  useEffect(() => {
    analytics.page(`Organizations ${view !== "owned" ? "Browse" : "Applied"}`);
    analytics.track(`ORGANIZATION_${view !== "owned" ? "BROWSE" : "OWNED"}_LIST_VIEW`);
  }, [currentUser]);

  return (
    <AuthAwareLayout metaTitle="eFuse | Organizations" containsSubheader>
      <EFSubHeader
        headerIcon={faBinoculars}
        navigationList={getDiscoverNavigationList(router.pathname, showErenaLink)}
      />

      <Col md={12} lg={12} className={`${!isAuthenticated && Style.topContainer}`}>
        <div className={Style.orgBtnWrapper}>
          <Link href="/organizations">
            <Nav.Link
              href="/organizations"
              className={`${Style.link} ${router.route === "/organizations" && Style.active}`}
            >
              All
            </Nav.Link>
          </Link>
          {isAuthenticated && (
            <Link href="/organizations/owned">
              <Nav.Link
                href="/organizations/owned"
                className={`${Style.link} ${router.route === "/organizations/owned" && Style.active}`}
              >
                My Organizations
              </Nav.Link>
            </Link>
          )}
          {(organizationCreateButtons || currentUser?.verified) && isAuthenticated && (
            <div className={Style.create}>
              <EFRectangleButton internalHref="/organizations/create" text="Create Organization" />
            </div>
          )}
        </div>
        <div className={Style.searchBox}>
          <SearchField
            placeholder="Search"
            value={search}
            name="search"
            onChange={e => {
              setSearch(e.target.value);
            }}
          />
        </div>

        <MyOrganizationListing search={search} />
      </Col>
    </AuthAwareLayout>
  );
};

export default MyOrganizationWrapper;
