import { connectCurrentRefinements } from "react-instantsearch-dom";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";

const ClearFilters = ({ items, refine }) =>
  items.length > 0 && (
    <EFRectangleButton
      text="Clear all filters"
      width="fullWidth"
      onClick={() => refine(items)}
      colorTheme="light"
      shadowTheme="none"
    />
  );

export default connectCurrentRefinements(ClearFilters);
