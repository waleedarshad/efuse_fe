import React from "react";
import { Card, FormGroup } from "react-bootstrap";

import InputLabel from "../../../InputLabel/InputLabel";
import DropdownMenu from "./DropdownMenu";
import ClearFilters from "./ClearFilters";

const OrganizationFilters = () => {
  return (
    <div className={"stickySidebar {Style.filterWrapper}"}>
      <Card className="customCard mb-4">
        <Card.Header>
          <h5 className="headerTitle">Filters</h5>
        </Card.Header>
        <Card.Body>
          <InputLabel theme="darkColor">Type</InputLabel>
          <FormGroup>
            <DropdownMenu attribute="organizationType" defaultLabel="All Types" name="organizationType" />
          </FormGroup>
          <InputLabel theme="darkColor">Status</InputLabel>
          <FormGroup>
            <DropdownMenu attribute="status" defaultLabel="All Statuses" name="status" />
          </FormGroup>
          <InputLabel theme="darkColor">Size</InputLabel>
          <FormGroup>
            <DropdownMenu attribute="scale" defaultLabel="All Sizes" name="scale" />
          </FormGroup>
          <InputLabel theme="internal">Verified</InputLabel>
          <FormGroup>
            <DropdownMenu
              attribute="verified.status"
              defaultLabel="Verified and Unverfied"
              name="verified.status"
              labelOverrides={[
                { before: "false", after: "Unverified" },
                { before: "true", after: "Verified" }
              ]}
            />
          </FormGroup>
          <ClearFilters />
        </Card.Body>
      </Card>
    </div>
  );
};

export default OrganizationFilters;
