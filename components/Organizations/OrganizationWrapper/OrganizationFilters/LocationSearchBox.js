import { connectSearchBox } from "react-instantsearch-dom";
import { useState } from "react";

import GoogleAutoComplete from "../../../GoogleAutocomplete/GoogleAutocomplete";

const LocationSearchBox = ({ refine }) => {
  const [location, setLocation] = useState("");

  return (
    <GoogleAutoComplete
      onSuggest={event => {
        setLocation(event?.label);
        refine(event?.label);
      }}
      placeholder="Search Location"
      className="geoselect-internal"
      initialValue={location}
    />
  );
};

export default connectSearchBox(LocationSearchBox);
