import { connectMenu } from "react-instantsearch-dom";
import SelectBox from "../../../SelectBox/SelectBox";

const DropdownMenu = ({ items, refine, defaultLabel, name, currentRefinement, labelOverrides }) => {
  const dropdownItems = [{ label: defaultLabel, value: "", isRefined: !currentRefinement }];
  items.forEach(item => {
    const labelOverride = labelOverrides.find(override => override.before === item.label);
    const label = labelOverride ? labelOverride.after : item.label;

    dropdownItems.push({
      label: `${label} (${item.count})`,
      value: item.label,
      isRefined: item.isRefined
    });
  });
  return (
    <SelectBox
      validated={false}
      options={dropdownItems}
      size="lg"
      theme="whiteShadow"
      block="true"
      name={name}
      value={currentRefinement ?? ""}
      onChange={event => refine(event.target.value)}
    />
  );
};

DropdownMenu.defaultProps = {
  labelOverrides: []
};

export default connectMenu(DropdownMenu);
