import { connectRefinementList } from "react-instantsearch-dom";
import { Col } from "react-bootstrap";

import Checkbox from "../../../Checkbox/Checkbox";

const RadioOptions = ({ items, refine, name }) => {
  return items.map(item => {
    return (
      <Col key={item.label}>
        <Checkbox
          custom
          name={name}
          type="radio"
          label={item.label === "true" ? "Yes" : "No"}
          id={`verified-status-${item.label}`}
          theme="internal"
          checked={item.isRefined}
          value={item.label}
          onChange={() => refine(item.label)}
        />
      </Col>
    );
  });
};

export default connectRefinementList(RadioOptions);
