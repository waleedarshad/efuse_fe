import { connectSearchBox } from "react-instantsearch-dom";
import { useState } from "react";

import SearchField from "../../../SearchField/SearchField";

const NameSearchBox = ({ refine }) => {
  const [search, setSearch] = useState("");
  return (
    <SearchField
      placeholder="Search"
      value={search}
      name="search"
      onChange={e => {
        setSearch(e.target.value);
        refine(e.target.value);
      }}
    />
  );
};

export default connectSearchBox(NameSearchBox);
