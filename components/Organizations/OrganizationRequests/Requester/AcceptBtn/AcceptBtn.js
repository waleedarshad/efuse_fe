import React, { Component } from "react";
import { connect } from "react-redux";
import { Row, Col } from "react-bootstrap";

import { approveOrgRequest, rejectOrgRequest } from "../../../../../store/actions/organizationActions";
import ConfirmAlert from "../../../../ConfirmAlert/ConfirmAlert";
import EFRectangleButton from "../../../../Buttons/EFRectangleButton/EFRectangleButton";

class AcceptBtn extends Component {
  acceptRequest = () => {
    this.props.approveOrgRequest(this.props.requestId);
  };

  rejectRequest = () => {
    this.props.rejectOrgRequest(this.props.requestId);
  };

  render() {
    const { firstName, requestStatus } = this.props;
    const disable = requestStatus == "rejected";
    const content = (
      <Row>
        <Col md={6}>
          <ConfirmAlert
            title="Add as member"
            message={`Are you sure you want to add ${firstName} as a member`}
            onYes={() => this.acceptRequest()}
          >
            <EFRectangleButton text="Accept" />
          </ConfirmAlert>
        </Col>
        <Col md={6}>
          {!disable ? (
            <ConfirmAlert
              title="Reject Request"
              message="Are you sure you want to reject request"
              onYes={() => this.rejectRequest()}
            >
              <EFRectangleButton text="Reject" />
            </ConfirmAlert>
          ) : (
            <EFRectangleButton text="Rejected" disabled />
          )}
        </Col>
      </Row>
    );
    return <>{content}</>;
  }
}

export default connect(null, { approveOrgRequest, rejectOrgRequest })(AcceptBtn);
