import { Col } from "react-bootstrap";
import Style from "./Requester.module.scss";
import EFAvatar from "../../../EFAvatar/EFAvatar";
import { rescueNil, getImage, formatRoles } from "../../../../helpers/GeneralHelper";
import AcceptBtn from "./AcceptBtn/AcceptBtn";
import UserLink from "../../../UserLink/UserLink";

const Requester = ({ user, request, t }) => {
  const profilePicture = rescueNil(user, "profilePicture", {});
  return (
    <Col md={3} sm={4}>
      <div className={Style.friendWrapper}>
        <EFAvatar displayOnlineButton={false} profilePicture={getImage(profilePicture, "avatar")} size="extraLarge" />

        <h5 className={Style.name}>
          <UserLink user={user} />{" "}
        </h5>
        <p className={Style.roles}>{formatRoles(user)}</p>
        <AcceptBtn
          background="funBlue"
          requestId={request._id}
          requestStatus={request.status}
          firstName={user?.name}
          t={t}
        />
      </div>
    </Col>
  );
};

export default Requester;
