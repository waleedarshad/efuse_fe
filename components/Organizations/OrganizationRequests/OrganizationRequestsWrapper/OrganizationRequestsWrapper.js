import React, { Component } from "react";
import { connect } from "react-redux";
import "react-table/react-table.css";
import Style from "./OrganizationRequestsWrapper";
import OrganizationLayout from "../../../Layouts/OrganizationLayout/OrganizationLayout";
import { getImage, rescueNil } from "../../../../helpers/GeneralHelper";
import { getOwnerAndCaptain } from "../../../../helpers/OrganizationHelper";
import OrganizationBioCard from "../../../OrganizationBioCard";
import { getOrganizationRequests } from "../../../../store/actions/organizationActions";
import RequestersList from "../RequesterList/RequesterList";
import Error from "../../../../pages/_error";
import EmptyComponent from "../../../EmptyComponent/EmptyComponent";

let page = 1;
class OrganizationRequests extends Component {
  constructor(props, context) {
    super(props);
  }

  componentDidMount() {
    analytics.page("Organizations Requests");
    this.props.getOrganizationRequests(page, this.props.query.id);
  }

  loadMore = () => {
    page += 1;
    this.props.getOrganizationRequests(page, this.props.query.id);
  };

  render() {
    const { query, organization, requests, hasNextPage, currentUser, responseCode, t } = this.props;
    const { owner, captain } = getOwnerAndCaptain(currentUser, organization);
    if (responseCode === 422 || responseCode === 400) {
      return <Error statusCode={responseCode} />;
    }
    return (
      <OrganizationLayout
        headerImage={getImage({}, "organization")}
        owner={owner || captain}
        avatar={rescueNil(organization, "profileImage", {}) || {}}
        bioCard={<OrganizationBioCard t={t} />}
        bar="Organizations"
        query={query}
        t={t}
      >
        <div className={Style.titleWrapper}>
          <div className={Style.linkWrapper}>
            {requests.length ? (
              <RequestersList
                requests={requests}
                hasNextPage={hasNextPage}
                loadMore={this.loadMore}
                organization={organization}
                t={t}
              />
            ) : (
              <EmptyComponent text="No requests exist. Users will appear here when they request to join your organization." />
            )}
          </div>
        </div>
      </OrganizationLayout>
    );
  }
}
const mapStateToProps = state => ({
  organization: state.organizations.organization,
  currentUser: state.auth.currentUser,
  requests: state.organizations.requesters,
  hasNextPage: state.organizations.hasNextPage,
  responseCode: state.errors.responseCode
});
export default connect(mapStateToProps, {
  getOrganizationRequests
})(OrganizationRequests);
