import React, { Component } from "react";

import InfiniteScroll from "react-infinite-scroller";
import { Row } from "react-bootstrap";
import NoRecordFound from "../../../NoRecordFound/NoRecordFound";
import AnimatedLogo from "../../../AnimatedLogo";
import Requester from "../Requester/Requester";

class RequestersList extends Component {
  render() {
    const { requests, hasNextPage, loadMore, organization, t } = this.props;
    const content = requests.map((request, index) => (
      <Requester user={request.associatedUser} key={index} request={request} t={t} />
    ));

    return (
      <InfiniteScroll
        pageStart={1}
        loadMore={loadMore}
        hasMore={hasNextPage}
        loader={<AnimatedLogo key={0} theme="inline" />}
      >
        <Row>{requests.length === 0 ? <NoRecordFound /> : content}</Row>
      </InfiniteScroll>
    );
  }
}
export default RequestersList;
