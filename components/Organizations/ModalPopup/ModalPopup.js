import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Col, Form, FormControl } from "react-bootstrap";
import AsyncSelect from "react-select/lib/Async";

import { getImage } from "../../../helpers/GeneralHelper";
import { searchForUsers } from "../../../helpers/SearchHelper";
import { sendInvite } from "../../../store/actions/organizationActions";
import InputRow from "../../InputRow/InputRow";
import { withCdn } from "../../../common/utils";
import EFPrimaryModal from "../../Modals/EFPrimaryModal/EFPrimaryModal";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";

const ModalPopup = ({ modalShow, placeholder, toggleModalShow }) => {
  const dispatch = useDispatch();

  const orgId = useSelector(state => state.organizations.modalOrgId);

  const [validated, setValidated] = useState(false);
  const [inviteesEmptyError, setInviteesEmptyError] = useState(false);
  const [invitees, setInvitees] = useState([]);

  const onChange = selectedOptions => {
    setInviteesEmptyError(!selectedOptions.length > 0);
    setInvitees(selectedOptions.map(opt => opt));
  };

  const loadOptions = (input, callback) => {
    const inviteeIds = invitees.map(invitee => invitee.value);

    searchForUsers(input).then(response => {
      const options = response.data.users.docs
        .map(user => ({
          value: user._id,
          label: (
            <div className="optionWithImage">
              <img src={getImage(user.profilePicture, "avatar")} alt="Profile" />{" "}
              <span>
                <strong>{user.name}</strong>{" "}
              </span>
              {user.verified && (
                <span>
                  <img
                    style={{ width: "16px", height: "auto" }}
                    src={withCdn("/static/images/efuse_verify.png")}
                    alt="eFuse Verify"
                  />
                </span>
              )}
              <span>@{user.username}</span>
            </div>
          ),
          avatar: user.profilePicture,
          url: user.url,
          username: user.username,
          type: user.type
        }))
        .filter(u => !inviteeIds.includes(u.value));

      callback(options);
    });
  };

  const onSubmit = event => {
    event.preventDefault();

    setInviteesEmptyError(invitees.length === 0);
    setValidated(true);

    if (event.currentTarget.checkValidity() && invitees.length > 0) {
      const data = {
        invitees: invitees.map(invite => invite.value),
        orgId
      };

      dispatch(sendInvite(data));
      toggleModalShow();
      setInvitees([]);
    }
  };

  return (
    <EFPrimaryModal title="Invitation" isOpen={modalShow} onClose={toggleModalShow} allowBackgroundClickClose={false}>
      <Form onSubmit={e => onSubmit(e)} noValidate validated={validated}>
        <InputRow>
          <Col>
            <AsyncSelect
              cacheOptions
              isMulti
              onChange={onChange}
              loadOptions={loadOptions}
              className="nav-home nav-search"
              value={invitees}
              placeholder={placeholder}
              id="dynamic-id-search-users"
              key={invitees.length}
            />
            {inviteesEmptyError && (
              <FormControl.Feedback type="invalid" style={{ display: "block" }}>
                Select one or more to invite
              </FormControl.Feedback>
            )}
          </Col>
        </InputRow>
        <InputRow>
          <Col>
            <EFRectangleButton text="Invite" buttonType="submit" />
          </Col>
        </InputRow>
      </Form>
    </EFPrimaryModal>
  );
};

export default ModalPopup;
