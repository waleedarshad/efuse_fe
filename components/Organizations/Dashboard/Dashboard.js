import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useRouter } from "next/router";

import OrganizationLayout from "../../Layouts/OrganizationLayout/OrganizationLayout";
import { getImage, rescueNil } from "../../../helpers/GeneralHelper";
import { getOwnerAndCaptain } from "../../../helpers/OrganizationHelper";
import EFCard from "../../Cards/EFCard/EFCard";
import InviteButtons from "./InviteButtons/InviteButtons";
import DashboardTools from "./DashboardTools/DashboardTools";
import FeaturedMembers from "./FeaturedMembers/FeaturedMembers";
import Badge from "./Badge/Badge";
import { getOpportunities } from "../../../store/actions/opportunityActions";
import { getMembers } from "../../../store/actions/organizationActions";
import Error from "../../../pages/_error";
import EmptyComponent from "../../EmptyComponent/EmptyComponent";
import Style from "./Dashboard.module.scss";

const Dashboard = ({ canOrgCreateLeagues, leagues }) => {
  const dispatch = useDispatch();
  const organization = useSelector(state => state.organizations.organization);
  const opportunities = useSelector(state => state.opportunities.pagination.totalDocs);
  const currentUser = useSelector(state => state.auth.currentUser);
  const members = useSelector(state => state.organizations.members);
  const responseCode = useSelector(state => state.errors.responseCode);
  const router = useRouter();

  useEffect(() => {
    dispatch(getOpportunities(1, 9, {}, true, false, router.query.id, "organizations/opportunities"));
    dispatch(getMembers(1, 5, router.query.id));
  }, []);

  const { owner, captain } = getOwnerAndCaptain(currentUser, organization);
  const admin = owner || captain;

  if (responseCode === 422 || responseCode === 400) {
    return <Error statusCode={responseCode} />;
  }

  let mainContent = (
    <>
      <InviteButtons organization={organization} />
      <DashboardTools
        followers={organization?.followersCount}
        members={organization?.membersCount}
        opportunities={opportunities}
        inviteCodes={organization.inviteCodes}
        organizationId={organization._id}
        canOrgCreateLeagues={canOrgCreateLeagues}
        leagues={leagues}
      />
      <FeaturedMembers members={members} />
      <Badge organization={organization} />
    </>
  );

  if (!admin) {
    mainContent = <EmptyComponent text="You need to be an owner or captain of this organization to see this page" />;
  }

  return (
    <OrganizationLayout
      headerImage={getImage(organization.headerImage, "organization")}
      avatar={rescueNil(organization, "profileImage", {}) || {}}
      bar="Organizations"
      orgType={organization.status}
    >
      <div className={Style.mainContainer}>
        <EFCard widthTheme="fullWidth">
          <div className={Style.dashboardContentWrapper}>{mainContent}</div>
        </EFCard>
      </div>
    </OrganizationLayout>
  );
};

export default Dashboard;
