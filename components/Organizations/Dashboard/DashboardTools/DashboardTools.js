import React, { useState } from "react";
import CardDeck from "react-bootstrap/CardDeck";
import { useDispatch, useSelector } from "react-redux";
import {
  faTrophyAlt,
  faBuilding,
  faGamepadAlt,
  faFileInvoice,
  faBooks,
  faShieldAlt
} from "@fortawesome/pro-light-svg-icons";

import Modal from "../../../Modal/Modal";
import NewArticleModal from "../../../EsportsNews/NewArticleModal/NewArticleModal";
import DynamicModal from "../../../DynamicModal/DynamicModal";
import EFVerticalStatCard from "../../../Cards/EFVerticalStatCard/EFVerticalStatCard";
import EFFeaturedButton from "../../../Buttons/EFFeaturedButton/EFFeaturedButton";
import { shareLinkModal } from "../../../../store/actions/shareAction";
import { SHARE_LINK_KINDS } from "../../../ShareLinkModal/ShareLinkModal";
import FeatureFlag from "../../../General/FeatureFlag/FeatureFlag";
import FeatureFlagVariant from "../../../General/FeatureFlag/FeatureFlagVariant/FeatureFlagVariant";
import CreatePostModal from "../../../Modals/CreatePostModal/CreatePostModal";
import Style from "./DashboardTools.module.scss";

const DashboardTools = ({
  followers = 0,
  members = 0,
  opportunities = 0,
  teams = 0,
  leagues = 0,
  canOrgCreateLeagues = false,
  inviteCodes,
  organizationId
}) => {
  const dispatch = useDispatch();

  const [openCreatePostModal, setOpenCreatePostModal] = useState(false);

  const showOrganizationTeams = useSelector(state => state.features.organization_teams);

  const memberInviteCodes = inviteCodes?.filter(code => code.type === "ORGANIZATION_MEMBER_INVITE");
  const followerInviteCodes = inviteCodes?.filter(code => code.type === "ORGANIZATION");

  return (
    <div className={`row ${Style.dashboardToolsContainer}`}>
      <div className="col-xl-5">
        <small className={`d-block ${Style.headerText}`}>DASHBOARD</small>
        <CardDeck className={Style.cardsContainer}>
          <EFVerticalStatCard
            imageLink="https://efuse.s3.amazonaws.com/uploads/static/erena/efuse-icon.png"
            value={followers}
            text="Followers"
            buttonText="Invite"
            buttonColorTheme="primary"
            onClick={() => {
              analytics.track("ORGANIZATION_INVITE_MODAL_OPEN", { inviteCodeType: "ORGANIZATION" });
              dispatch(
                shareLinkModal(
                  true,
                  "Share Organization Invite Link",
                  `https://efuse.gg/i/${followerInviteCodes[0].code}`,
                  SHARE_LINK_KINDS.ORGANIZATION
                )
              );
            }}
            disableButton={!followerInviteCodes?.length && true}
          />

          <EFVerticalStatCard
            icon={faBuilding}
            value={members}
            text="Members"
            buttonText="Invite"
            buttonColorTheme="secondary"
            onClick={() => {
              analytics.track("ORGANIZATION_INVITE_MODAL_OPEN", { inviteCodeType: "ORGANIZATION_MEMBER_INVITE" });
              dispatch(
                shareLinkModal(
                  true,
                  "Share Organization Invite Link",
                  `https://efuse.gg/i/${memberInviteCodes[0].code}`,
                  SHARE_LINK_KINDS.ORGANIZATION
                )
              );
            }}
            disableButton={!memberInviteCodes?.length && true}
          />

          {showOrganizationTeams && (
            <EFVerticalStatCard
              icon={faShieldAlt}
              value={teams}
              text="Teams"
              buttonText="Manage"
              buttonColorTheme="light"
              internalHref={`/organizations/teams?id=${organizationId}`}
            />
          )}

          <EFVerticalStatCard
            icon={faFileInvoice}
            value={opportunities}
            text="Opportunities"
            buttonText="Manage"
            buttonColorTheme="light"
            internalHref={`/organizations/opportunities?id=${organizationId}`}
          />

          {(canOrgCreateLeagues || leagues > 0) && (
            <EFVerticalStatCard
              icon={faShieldAlt}
              value={leagues}
              text="Leagues"
              buttonText="Manage"
              buttonColorTheme="light"
              internalHref={`/leagues/dashboard/${organizationId}`}
            />
          )}
        </CardDeck>
      </div>
      <div className={`col-xl-7 ${Style.buttons}`}>
        <small className={`d-block ${Style.headerText}`}>TOOLS</small>
        <EFFeaturedButton icon={faBuilding} subText="Lounge Post" onClick={() => setOpenCreatePostModal(true)} />
        {openCreatePostModal && <CreatePostModal onClose={() => setOpenCreatePostModal(false)} />}
        <EFFeaturedButton icon={faGamepadAlt} subText="Opportunity" internalHref="/opportunities/create" />
        <FeatureFlag name="allow_erena_creation">
          <FeatureFlagVariant flagState>
            <DynamicModal
              flow="CreateErenaEvent"
              openOnLoad={false}
              startView="general"
              displayCloseButton
              allowBackgroundClickClose
            >
              <EFFeaturedButton icon={faTrophyAlt} subText="eRena Tournament" />
            </DynamicModal>
          </FeatureFlagVariant>
        </FeatureFlag>
        <FeatureFlag name="allow_learning_article_creation">
          <FeatureFlagVariant flagState>
            <Modal
              displayCloseButton
              title="Create News Article"
              onOpenModal={() => {}}
              component={<NewArticleModal />}
            >
              <EFFeaturedButton icon={faBooks} subText="News Article" />
            </Modal>
          </FeatureFlagVariant>
        </FeatureFlag>
      </div>
    </div>
  );
};

export default DashboardTools;
