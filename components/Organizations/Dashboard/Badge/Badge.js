import EFExternalEfuseBadge from "../../../EFExternalEfuseBadge/EFExternalEfuseBadge";
import Style from "./Badge.module.scss";

const Badge = ({ organization }) => {
  let imageUrl = "";
  let linkUrl = "";

  const organizationVerified = organization?.verified?.status ? "verified_" : "";

  if (organization._id) {
    imageUrl = `https://cdn.efuse.gg/uploads/static/badges/efuse_${organizationVerified}organization.png`;
    linkUrl = `http://efuse.gg/org/${organization.shortName}`;
  }
  return (
    <div className={Style.badgeWrapper}>
      <p className={Style.headerText}>HTML Snippet</p>
      <EFExternalEfuseBadge
        id={organization._id}
        linkUrl={linkUrl}
        imageUrl={imageUrl}
        badgeType="organization"
        altText="eFuse Organization Portfolio Badge"
      />
    </div>
  );
};

export default Badge;
