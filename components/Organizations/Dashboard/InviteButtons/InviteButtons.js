import React from "react";
import { useDispatch } from "react-redux";

import { shareLinkModal } from "../../../../store/actions/shareAction";
import { SHARE_LINK_KINDS } from "../../../ShareLinkModal/ShareLinkModal";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";
import Style from "./InviteButtons.module.scss";

const InviteButtons = ({ organization }) => {
  const dispatch = useDispatch();
  const { inviteCodes } = organization;

  if (!inviteCodes || inviteCodes.length === 0) return <></>;

  const memberInviteCodes = inviteCodes.filter(code => code.type === "ORGANIZATION_MEMBER_INVITE");
  const followerInviteCodes = inviteCodes.filter(code => code.type === "ORGANIZATION");

  return (
    <>
      {followerInviteCodes.map((code, index) => {
        let buttonText = "Invite Followers";
        if (followerInviteCodes.length > 1) {
          buttonText += ` Code:${code.code}`;
        }

        return (
          <div className={Style.InviteButton} key={index}>
            <EFRectangleButton
              text={buttonText}
              onClick={() => {
                analytics.track("ORGANIZATION_INVITE_MODAL_OPEN", { inviteCodeType: "ORGANIZATION" });
                dispatch(
                  shareLinkModal(
                    true,
                    "Share Organization Invite Link",
                    `https://efuse.gg/i/${code.code}`,
                    SHARE_LINK_KINDS.ORGANIZATION
                  )
                );
              }}
            />
          </div>
        );
      })}
      {memberInviteCodes.map((code, index) => {
        let buttonText = "Invite Members";
        if (memberInviteCodes.length > 1) {
          buttonText += ` Code:${code.code}`;
        }

        return (
          <div className={Style.InviteButton} key={index}>
            <EFRectangleButton
              colorTheme="secondary"
              text={buttonText}
              onClick={() => {
                analytics.track("ORGANIZATION_INVITE_MODAL_OPEN", { inviteCodeType: "ORGANIZATION_MEMBER_INVITE" });
                dispatch(
                  shareLinkModal(
                    true,
                    "Share Organization Invite Link",
                    `https://efuse.gg/i/${code.code}`,
                    SHARE_LINK_KINDS.ORGANIZATION
                  )
                );
              }}
            />
          </div>
        );
      })}
    </>
  );
};

export default InviteButtons;
