import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Router from "next/router";
import { Col, Row } from "react-bootstrap";
import { getImage, rescueNil } from "../../helpers/GeneralHelper";
import OrganizationLayout from "../Layouts/OrganizationLayout/OrganizationLayout";
import { getOwnerAndCaptain } from "../../helpers/OrganizationHelper";
import OrganizationBioCard from "../OrganizationBioCard";
import { isUserLoggedIn } from "../../helpers/AuthHelper";
import Feed from "../Feed/Feed";
import { getOrganizationFeed } from "../../store/actions/feedActions";
import EFSideOpportunities from "../EFSideOpportunities/EFSideOpportunities";

const OrganizationFeed = ({ query }) => {
  const dispatch = useDispatch();

  const { currentUser } = useSelector(state => state.auth);
  const { isWindowView } = useSelector(state => state.messages);
  const { organization } = useSelector(state => state.organizations);

  const { owner, captain } = getOwnerAndCaptain(currentUser, organization);

  useEffect(() => {
    analytics.page("Organizations Timeline");

    const isLoggedIn = isUserLoggedIn();
    if (!isLoggedIn) {
      Router.push("/login");
    }
  }, []);

  return (
    <OrganizationLayout
      isWindowView={isWindowView}
      headerImage={getImage(organization.headerImage, "organization")}
      query={query}
      avatar={rescueNil(organization, "profileImage", {}) || {}}
      bioCard={<OrganizationBioCard />}
      bar="Organizations"
      owner={owner || captain}
    >
      <Row>
        <Col className="col-lg-8">
          <Feed feedType="profile" getFeed={page => dispatch(getOrganizationFeed(page, 10, query?.id))} />;
        </Col>
        <Col className="col-md-4 d-none d-lg-block">
          <EFSideOpportunities />
        </Col>
      </Row>
    </OrganizationLayout>
  );
};

export default OrganizationFeed;
