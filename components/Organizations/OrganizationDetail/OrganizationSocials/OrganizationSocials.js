import React, { Component } from "react";
import { faLinkedin, faTwitch, faFacebook, faYoutube, faTwitter } from "@fortawesome/free-brands-svg-icons";

import SocialCard from "../../../User/Portfolio/SocialCardList/SocialCard/SocialCard";
import Style from "../../../User/Portfolio/SocialCardList/SocialCardList.module.scss";

export class OrganizationSocials extends Component {
  render() {
    // We could reuse the SocialCardList.js however it seems like we will need to create new
    // auth functions for organizations (because current authentication functions use the current user)
    return (
      <div className={Style.cardList}>
        <SocialCard noStats social="linkedin" icon={faLinkedin} />
        <SocialCard noStats social="twitch" icon={faTwitch} />
        <SocialCard noStats social="facebook" icon={faFacebook} />
        <SocialCard noStats social="youtube" icon={faYoutube} />
        <SocialCard noStats social="twitter" icon={faTwitter} />
      </div>
    );
  }
}

export default OrganizationSocials;
