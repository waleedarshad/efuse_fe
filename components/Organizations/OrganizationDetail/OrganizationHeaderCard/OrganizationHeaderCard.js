import React from "react";
import Router from "next/router";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCalendarAlt, faShare } from "@fortawesome/pro-solid-svg-icons";
import { faEdit } from "@fortawesome/pro-regular-svg-icons";

import VerifiedIcon from "../../../VerifiedIcon/VerifiedIcon";
import { rescueNil, getImage, formatDate } from "../../../../helpers/GeneralHelper";
import OrganizationProgressBar from "../../../PortfolioProgress/OrganizationProgress/OrganizationProgressBar";
import EFCircleIconButton from "../../../Buttons/EFCircleIconButton/EFCircleIconButton";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";
import EFAvatar from "../../../EFAvatar/EFAvatar";
import ShareLinkButton from "../../../ShareLinkButton";
import EFInfoPill from "../../../EFInfoPill/EFInfoPill";
import Style from "./OrganizationHeaderCard.module.scss";

const OrganizationHeaderCard = ({ organization, currentUser, isOwner, followButton, captain }) => {
  const openOrganizationEditPage = () => {
    Router.push(`/organizations/edit/${organization?._id}`);
  };
  const verifiedObject = rescueNil(organization, "verified", {});

  return (
    <div className={Style.headerCard}>
      <div className="row m-0">
        <div className={`col-lg-2 ${Style.profilePicture}`}>
          <EFAvatar
            profilePicture={getImage(organization.profileImage)}
            size="extraLarge"
            displayOnlineButton={false}
          />
        </div>
        <div className="col-lg-4 pt-3">
          <EFInfoPill>{`${organization.followersCount || 0} Followers`} </EFInfoPill>
          <EFInfoPill>{`${organization.membersCount || 0} Members`}</EFInfoPill>
          <span className={`d-block ${Style.fullname}`} data-cy="org_name">
            {organization.name}
            {verifiedObject.status && <VerifiedIcon organization={organization} />}
          </span>
        </div>
        <div className={`col-lg-6 pt-3 ${Style.portfolioLinksWrap}`}>
          {(isOwner || captain) && (
            <div className={Style.buttonContainer}>
              <EFCircleIconButton onClick={openOrganizationEditPage} icon={faEdit} />
            </div>
          )}
          <div className={Style.buttonContainer}>
            <ShareLinkButton
              userId={currentUser?._id}
              title={`Share ${organization?.name}`}
              url={`https://efuse.gg/org/${organization?.shortName}`}
              icon={faShare}
              theme="roundButton"
            />
          </div>
          {followButton}
          {organization.website && (
            <div className={Style.buttonContainer}>
              <EFRectangleButton colorTheme="light" externalHref={organization.website} text="Visit Website" />
            </div>
          )}
        </div>
      </div>
      <div className="row m-0 mt-4">
        <div className="col-lg-6">
          <div>
            <p className={Style.bioText} data-cy="org_description">
              {organization.description}
            </p>
          </div>
          <div>
            <small>
              <FontAwesomeIcon icon={faCalendarAlt} />
              {currentUser && ` Joined ${formatDate(organization?.createdAt)}`}
            </small>
          </div>
        </div>
        <div className="col-lg-6">
          {(isOwner || captain) && (
            <div className={Style.progressWrapper}>
              <OrganizationProgressBar organization={organization} />
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default OrganizationHeaderCard;
