import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { faEdit as farEdit } from "@fortawesome/pro-regular-svg-icons";
import { faPlus } from "@fortawesome/pro-solid-svg-icons";
import { removeAccolades, toggleOrganizationModal } from "../../../../../store/actions/organizationActions";
import EventsModal from "../../../../GlobalModals/EventsModal/EventsModal";
import EFCircleIconButton from "../../../../Buttons/EFCircleIconButton/EFCircleIconButton";
import ShowMore from "../../../../ShowMore/ShowMore";
import AddOrgEvent from "../../OrganizationAccolades/OrganizationEvents/AddOrgEvent";
import DisplayEventAndHonor from "../DisplayHonorAndEvent/DisplayHonorAndEvent";
import Style from "./Events.module.scss";

const Events = ({ isOwner }) => {
  const dispatch = useDispatch();
  const [event, setEvent] = useState(null);
  const organization = useSelector(state => state.organizations.organization);
  const modal = useSelector(state => state.organizations.modal);
  const modalSection = useSelector(state => state.organizations.modalSection);
  const { events } = organization;

  const loadModal = () => {
    dispatch(toggleOrganizationModal(true, "event"));
  };

  const onHide = () => {
    dispatch(toggleOrganizationModal(false));
    setEvent(null);
  };

  const displayEvents = info => {
    const content = [];
    info.map(data => {
      return content.push(
        <DisplayEventAndHonor
          isOwner={isOwner}
          data={data}
          key={data._id}
          onRemove={() => dispatch(removeAccolades(organization._id, "events", data._id))}
          editModal={
            <EventsModal portfolioEvent={data} edit organizationId={organization._id}>
              <div className={Style.buttonWrapper}>
                <EFCircleIconButton icon={farEdit} colorTheme="transparent" shadowTheme="none" />
              </div>
            </EventsModal>
          }
        />
      );
    });
    return content;
  };

  const organizationEvents = events && displayEvents(events);

  return (
    <ShowMore
      showMoreContent="See All"
      lineClamp={8}
      showMoreButtonClass={Style.seeAllBtn}
      showLessButtonClass={Style.seeLessBtn}
    >
      {isOwner && (
        <div className={Style.add}>
          <EFCircleIconButton icon={faPlus} onClick={() => loadModal()} />
          {modalSection === "event" && (
            <AddOrgEvent show={modal} event={event} onHide={onHide} organizationId={organization._id} />
          )}
        </div>
      )}
      {organizationEvents}
    </ShowMore>
  );
};
export default Events;
