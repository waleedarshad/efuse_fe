import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { faEdit } from "@fortawesome/pro-regular-svg-icons";
import { faPlus } from "@fortawesome/pro-solid-svg-icons";

import { removeAccolades, toggleOrganizationModal } from "../../../../../store/actions/organizationActions";
import HonorsModal from "../../../../GlobalModals/HonorsModal/HonorsModal";
import ShowMore from "../../../../ShowMore/ShowMore";
import AddOrgHonor from "../../OrganizationAccolades/OrganizationHonors/AddOrgHonor";
import EFCircleIconButton from "../../../../Buttons/EFCircleIconButton/EFCircleIconButton";
import DisplayEventAndHonor from "../DisplayHonorAndEvent/DisplayHonorAndEvent";
import Style from "../OrganizationEvents/Events.module.scss";

const Honors = ({ isOwner }) => {
  const dispatch = useDispatch();

  const [honor, setHonor] = useState(null);
  const organization = useSelector(state => state.organizations.organization);
  const modal = useSelector(state => state.organizations.modal);
  const modalSection = useSelector(state => state.organizations.modalSection);
  const { honors } = organization;
  const loadModal = () => {
    dispatch(toggleOrganizationModal(true, "honor"));
  };
  const onHide = () => {
    dispatch(toggleOrganizationModal(false));
    setHonor(null);
  };
  const displayHonors = info => {
    const content = [];
    info.map(data => {
      return content.push(
        <DisplayEventAndHonor
          isOwner={isOwner}
          data={data}
          key={data._id}
          onRemove={() => dispatch(removeAccolades(organization._id, "honors", data._id))}
          editModal={
            <HonorsModal portfolioHonor={data} edit organizationId={organization._id}>
              <div className={Style.buttonWrapper}>
                <EFCircleIconButton icon={faEdit} colorTheme="transparent" shadowTheme="none" />
              </div>
            </HonorsModal>
          }
        />
      );
    });
    return content;
  };
  const organizationHonors = honors && displayHonors(honors);
  return (
    <ShowMore
      showMoreContent="See All"
      lineClamp={8}
      showMoreButtonClass={Style.seeAllBtn}
      showLessButtonClass={Style.seeLessBtn}
    >
      {isOwner && (
        <div className={Style.add}>
          <EFCircleIconButton icon={faPlus} onClick={() => loadModal()} />
          {modalSection === "honor" && (
            <AddOrgHonor show={modal} honor={honor} onHide={onHide} organizationId={organization._id} />
          )}
        </div>
      )}
      {organizationHonors}
    </ShowMore>
  );
};
export default Honors;
