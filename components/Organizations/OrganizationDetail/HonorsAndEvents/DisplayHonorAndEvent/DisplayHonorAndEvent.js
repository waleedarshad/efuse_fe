import React from "react";
import { faTrash } from "@fortawesome/pro-solid-svg-icons";

import Style from "./DisplayHonorAndEvent.module.scss";
import { formatDate } from "../../../../../helpers/GeneralHelper";
import ConfirmAlert from "../../../../ConfirmAlert/ConfirmAlert";
import { EFHtmlParser } from "../../../../EFHtmlParser/EFHtmlParser";
import EFCircleIconButton from "../../../../Buttons/EFCircleIconButton/EFCircleIconButton";

const DisplayHonorAndEvent = ({ data, onRemove, editModal, isOwner }) => {
  return (
    <div className={Style.event}>
      <h6 className={Style.title}>
        {data.title}
        {isOwner && (
          <>
            {editModal}
            <ConfirmAlert
              title="Confirm Remove"
              message="Are you sure you want to remove the honor/event? Once done, this action can not be undone."
              onYes={onRemove}
            >
              <div className={Style.buttonWrapper}>
                <EFCircleIconButton icon={faTrash} colorTheme="transparent" shadowTheme="none" />
              </div>
            </ConfirmAlert>
          </>
        )}
      </h6>
      <p className={Style.description}>
        <EFHtmlParser>{data.description ? data.description : "No description"}</EFHtmlParser>
      </p>
      <p className={Style.subTitle}>{data.subTitle}</p>
      {data.eventDate && <small>{formatDate(data.eventDate)}</small>}
    </div>
  );
};
export default DisplayHonorAndEvent;
