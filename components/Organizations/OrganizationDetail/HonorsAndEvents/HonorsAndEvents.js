import React from "react";
import Tab from "react-bootstrap/Tab";
import Tabs from "react-bootstrap/Tabs";

import Style from "./HonorsAndEvents.module.scss";
import CardLayout from "../../../Portfolio/CardLayout/CardLayout";
import Events from "./OrganizationEvents/Events";
import Honors from "./OrganizationHonors/Honors";

const HonorsAndEvents = ({ isOwner, honors, events }) => {
  let displayEvents = <></>;
  let displayHonors = <></>;
  const eventsTab = (
    <Tab eventKey="events" tabClassName={Style.tab} title="Events">
      <Events isOwner={isOwner} />
    </Tab>
  );
  const honorsTab = (
    <Tab tabClassName={Style.tab} eventKey="honors" title="Honors">
      <Honors isOwner={isOwner} />
    </Tab>
  );

  let activeTab = "events";
  if (!isOwner) {
    if (events?.length === 0 && honors?.length === 0) {
      return <></>;
    }
    if (events?.length) {
      displayEvents = eventsTab;
    }
    if (honors?.length) {
      displayHonors = honorsTab;
      activeTab = "honors";
    }
    if (events?.length && honors?.length) {
      displayEvents = eventsTab;
      displayHonors = honorsTab;
      activeTab = "events";
    }
  } else {
    displayEvents = eventsTab;
    displayHonors = honorsTab;
  }
  return (
    <CardLayout>
      <Tabs className={Style.tabsWrapper} defaultActiveKey={activeTab}>
        {displayEvents}
        {displayHonors}
      </Tabs>
    </CardLayout>
  );
};
export default HonorsAndEvents;
