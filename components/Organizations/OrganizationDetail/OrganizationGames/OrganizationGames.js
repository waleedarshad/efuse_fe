import React, { useState } from "react";
import { useDispatch } from "react-redux";
import EFGameCardList from "../../../EFGameCardList/EFGameCardList";
import EFProfileCard from "../../../Cards/EFProfileCard/EFProfileCard";
import SelectGamesModal from "../../../Modals/SelectGamesModal/SelectGamesModal";
import { updateOrganizationGames } from "../../../../store/actions/organizationActions";

//games is the list of selected game ids
const OrganizationGames = ({ organizationId, hasGames, games, isAdmin }) => {
  const dispatch = useDispatch();
  const [isOpen, toggleModal] = useState(false);

  const updateGames = newGames => {
    dispatch(updateOrganizationGames(organizationId, newGames));
  };

  return (
    <EFProfileCard
      buttonType={hasGames ? "edit" : "plus"}
      showButton={isAdmin}
      onButtonClick={() => toggleModal(true)}
      title="Games"
    >
      {isAdmin && (
        <SelectGamesModal isOpen={isOpen} value={games} onChange={updateGames} onClose={() => toggleModal(false)} />
      )}
      {hasGames && <EFGameCardList games={games} />}
    </EFProfileCard>
  );
};

export default OrganizationGames;
