import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { faPencilAlt, faPlus } from "@fortawesome/pro-solid-svg-icons";
import Style from "./OrganizationVideo.module.scss";
import VideoPlayer from "../../../VideoPlayer/VideoPlayer";
import AddButton from "../../../User/Portfolio/AddButton/AddButton";
import { toggleOrganizationModal } from "../../../../store/actions/organizationActions";
import AddOrgVideo from "./AddOrgVideo";
import { rescueNil } from "../../../../helpers/GeneralHelper";

const OrganizationVideo = ({ organization, owner, playingVideo, setCurrentVideo }) => {
  const [edit, setEdit] = useState(false);
  const dispatch = useDispatch();
  const { modal, modalSection } = useSelector(state => state.organizations);

  const loadModal = () => {
    dispatch(toggleOrganizationModal(true, "video"));
  };

  const onHide = () => {
    dispatch(toggleOrganizationModal(false));
    setEdit(false);
  };

  const editModal = () => {
    dispatch(toggleOrganizationModal(true, "video"));
    setEdit(true);
  };

  const { promoVideo } = organization;
  let hasVideo = false;
  if (promoVideo && rescueNil(promoVideo, "title", false)) {
    hasVideo = true;
  }

  const videoUrl = promoVideo?.isVideo ? promoVideo?.videoFile.url : promoVideo?.embeddedUrl;

  return (
    <div className={Style.container}>
      {promoVideo && hasVideo && (
        <div className={Style.containerTop}>
          <div className={Style.videoContainer}>
            <VideoPlayer
              url={videoUrl}
              height="260px"
              autoplay={playingVideo === videoUrl}
              onPlay={() => setCurrentVideo(videoUrl)}
            />
          </div>
          <div className={Style.textContainer}>
            <h3 className={Style.title}>{promoVideo.title}</h3>
            <p className={Style.description}>{promoVideo.description}</p>
          </div>
        </div>
      )}
      {owner && (
        <>
          <div className={Style.add}>
            {hasVideo ? (
              <AddButton desc="Edit Video" icon={faPencilAlt} onClick={() => editModal(organization.promoVideo)} />
            ) : (
              <AddButton desc="Add Video" icon={faPlus} onClick={loadModal} />
            )}
          </div>
          {modal && modalSection === "video" && (
            <AddOrgVideo
              show={modal}
              onHide={onHide}
              edit={edit}
              promoVideo={organization.promoVideo}
              organization={organization}
            />
          )}
        </>
      )}
    </div>
  );
};

export default OrganizationVideo;
