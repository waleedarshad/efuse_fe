import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Form, Col } from "react-bootstrap";
import isEmpty from "lodash/isEmpty";
import InputRow from "../../../InputRow/InputRow";
import InputField from "../../../InputField/InputField";
import InputLabel from "../../../InputLabel/InputLabel";
import { validateHtml } from "../../../../helpers/ValidationHelper";
import ActionButtonGroup from "../../../Layouts/Internal/ActionButtonGroup/ActionButtonGroup";
import Checkbox from "../../../Checkbox/Checkbox";
import UploadVideoButton from "../../../UploadVideoButton";
import { updateVideoSection } from "../../../../store/actions/organizationActions";
import EFPrimaryModal from "../../../Modals/EFPrimaryModal/EFPrimaryModal";

const AddOrgVideo = ({ promoVideo, edit, organization, show, onHide }) => {
  const dispatch = useDispatch();

  const [validated, setValidated] = useState(false);
  const [localPromoVideo, setLocalPromoVideo] = useState({
    title: "",
    description: "",
    embeddedUrl: "",
    isVideo: false,
    videoFile: {}
  });

  const inProgress = useSelector(state => state.portfolio.inProgress);

  useEffect(() => {
    if (edit) {
      setLocalPromoVideo({ ...promoVideo });
    }
  }, []);

  const onChange = event => {
    setLocalPromoVideo({
      ...localPromoVideo,
      [event.target.name]: event.target.value
    });
  };

  const onVideoChange = event => {
    setLocalPromoVideo({
      ...localPromoVideo,
      isVideo: event.target.checked
    });
  };

  const onFileUpload = file => {
    setLocalPromoVideo({
      ...localPromoVideo,
      videoFile: file
    });
  };

  const onClearMedia = () => {
    setLocalPromoVideo({
      ...localPromoVideo,
      removeMedia: true,
      videoFile: {}
    });
  };

  const onSubmit = event => {
    event.preventDefault();

    if (!validated) {
      setValidated(true);
    } else if (event.currentTarget.checkValidity()) {
      let videoData = { ...localPromoVideo };
      videoData = {
        ...videoData,
        description: validateHtml(videoData.description)
      };
      dispatch(updateVideoSection(organization._id, videoData));
    }
  };

  const filePresent = !isEmpty(localPromoVideo.videoFile);
  const { title, description, embeddedUrl, isVideo } = localPromoVideo;
  return (
    <EFPrimaryModal
      title="Add Video"
      isOpen={show}
      allowBackgroundClickClose={false}
      displayCloseButton
      widthTheme="medium"
      onClose={onHide}
    >
      <Form noValidate validated={validated} onSubmit={onSubmit}>
        <InputRow>
          <Col sm={6} className={isVideo ? "" : "mt-3"}>
            <Checkbox
              custom
              name="isVideo"
              type="checkbox"
              label="Upload New Video"
              id="presentExp"
              theme="internal"
              checked={isVideo}
              value={isVideo}
              onChange={onVideoChange}
            />
          </Col>
          <Col sm={6}>
            {isVideo ? (
              <UploadVideoButton
                onFileUpload={onFileUpload}
                onClearMedia={onClearMedia}
                filePresent={filePresent}
                directory="uploads/organization/"
                placeholder="Upload Video"
              />
            ) : (
              <>
                <InputLabel theme="internal">Video Link</InputLabel>
                <InputField
                  name="embeddedUrl"
                  placeholder="Add Video Link"
                  theme="internal"
                  size="lg"
                  onChange={onChange}
                  value={embeddedUrl}
                  required
                  errorMessage="Video link is required"
                />
              </>
            )}
          </Col>
        </InputRow>
        <InputRow>
          <Col sm={12}>
            <InputLabel theme="internal">Title (50 characters)</InputLabel>
            <InputField
              name="title"
              placeholder="Add Title"
              theme="internal"
              size="lg"
              maxLength={50}
              onChange={onChange}
              value={title}
              required
              errorMessage="Title is required"
            />
          </Col>
        </InputRow>
        <InputRow>
          <Col sm={12}>
            <InputLabel theme="internal">Description (280 characters)</InputLabel>
            <InputField
              as="textarea"
              name="description"
              rows={3}
              maxLength={280}
              theme="internal"
              value={description}
              onChange={onChange}
              placeholder="Description"
              required
              errorMessage="Description is required"
            />
          </Col>
        </InputRow>
        <ActionButtonGroup onCancel={onHide} disabled={inProgress} />
      </Form>
    </EFPrimaryModal>
  );
};

export default AddOrgVideo;
