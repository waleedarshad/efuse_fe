import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";

import ErrorBoundary from "../../ErrorBoundary/ErrorBoundary";
import HonorsAndEvents from "./HonorsAndEvents/HonorsAndEvents";
import { rescueNil, getImage } from "../../../helpers/GeneralHelper";
import { getOwnerAndCaptain } from "../../../helpers/OrganizationHelper";
import { sendNotification } from "../../../helpers/FlashHelper";
import OrganizationLayout from "../../Layouts/OrganizationLayout/OrganizationLayout";
import PlayerCard from "./PlayerCard/PlayerCard";
import OrganizationVideo from "./OrganizationVideo/OrganizationVideo";
import OrgVideoCarousel from "./OrgVideoCarousel/OrgVideoCarousel";
import OrganizationGames from "./OrganizationGames/OrganizationGames";
import OrganizationAbout from "./OrganizationAbout/OrganizationAbout";
import FeatureFlag from "../../General/FeatureFlag/FeatureFlag";
import FeatureFlagVariant from "../../General/FeatureFlag/FeatureFlagVariant/FeatureFlagVariant";
import PreviewOpportunities from "./PreviewOpportunities/PreviewOpportunities";
import { getOpportunitiesNew } from "../../../store/actions/opportunityActions";
import OrganizationCloutCards from "./OrganizationCloutCards/OrganizationCloutCards";
import Error from "../../../pages/_error";

const OrganizationDetail = ({ pageProps, orgIdFromInviteCode }) => {
  const dispatch = useDispatch();
  const router = useRouter();

  const [playingVideo, setPlayingVideo] = useState(null);

  const currentUser = useSelector(state => state.auth.currentUser);
  const organization = useSelector(state => state.organizations.organization);
  const selectGamesFlag = useSelector(state => state.features.temp_web_select_games_orgs);
  const mockExternalOrganization = useSelector(state => state.organizations.mockExternalOrganization);
  const opportunities = useSelector(state => state.opportunities.opportunities);

  if (pageProps.error) {
    sendNotification(pageProps.error, "danger", "Organization");
    return <Error statusCode={422} />;
  }

  const userLoggedIn = currentUser?.id;
  let { owner, captain } = getOwnerAndCaptain(currentUser, pageProps.organization);
  let hasPlayerCards = false;

  if (mockExternalOrganization) {
    owner = false;
    captain = false;
  }
  if (pageProps.organization?.playerCards?.length) {
    hasPlayerCards = true;
  }
  const { events, honors, promoVideo, videoCarousel } = pageProps.organization;

  // check if user is owner or captain
  const isAdmin = owner || captain;

  // check if a video exists
  let hasVideo = false;
  if (promoVideo && rescueNil(promoVideo, "title", false)) {
    hasVideo = true;
  }

  // check if a video in carousel exists
  const hasVideoCarousel = videoCarousel?.length > 0;

  // check if organization opportunities exists
  const hasOpportunities = opportunities?.length > 0;

  // check if games exist
  const hasGames = organization?.associatedGames?.length > 0;

  useEffect(() => {
    dispatch(getOpportunitiesNew(1, {}, "organizations/opportunities", pageProps.organization?._id));
  }, []);

  const setCurrentVideo = video => {
    setPlayingVideo(video);
  };

  return (
    <OrganizationLayout
      userLoggedIn={userLoggedIn}
      webview={router.query.webview}
      headerImage={getImage(organization?.headerImage, "organization")}
      orgIdFromInviteCode={orgIdFromInviteCode}
    >
      <FeatureFlag name="temp_clout_card_web">
        <FeatureFlagVariant flagState>
          <ErrorBoundary>
            <OrganizationCloutCards
              owner={owner}
              ownerId={pageProps.organization?._id}
              organization={pageProps.organization}
            />
          </ErrorBoundary>
        </FeatureFlagVariant>
      </FeatureFlag>
      <ErrorBoundary>
        <OrganizationAbout
          about={rescueNil(organization, "about")}
          organizationId={organization?._id}
          isAdmin={isAdmin}
        />
      </ErrorBoundary>
      {hasOpportunities && (
        <ErrorBoundary>
          <PreviewOpportunities opportunities={opportunities?.slice(0, 5)} organizationId={organization?._id} />
        </ErrorBoundary>
      )}
      <ErrorBoundary>
        <HonorsAndEvents isOwner={isAdmin} events={events} honors={honors} />
      </ErrorBoundary>
      {/* //do not check for admin for games section until we can add data from portfolio */}
      {selectGamesFlag && (
        <ErrorBoundary>
          <OrganizationGames
            organizationId={organization?._id}
            hasGames={hasGames}
            games={rescueNil(organization, "associatedGames")}
            isAdmin={isAdmin}
          />
        </ErrorBoundary>
      )}
      {(isAdmin || hasVideo) && (
        <ErrorBoundary>
          <OrganizationVideo
            owner={owner}
            organization={organization}
            setCurrentVideo={setCurrentVideo}
            playingVideo={playingVideo}
          />
        </ErrorBoundary>
      )}
      {(isAdmin || hasVideoCarousel) && (
        <ErrorBoundary>
          <OrgVideoCarousel
            owner={owner}
            organization={organization}
            setCurrentVideo={setCurrentVideo}
            playingVideo={playingVideo}
          />
        </ErrorBoundary>
      )}
      {(isAdmin || hasPlayerCards) && (
        <ErrorBoundary>
          <PlayerCard owner={owner} />
        </ErrorBoundary>
      )}
    </OrganizationLayout>
  );
};

export default OrganizationDetail;
