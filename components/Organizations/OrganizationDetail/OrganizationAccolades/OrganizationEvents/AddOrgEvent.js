import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Form, Col, FormControl } from "react-bootstrap";
import InputRow from "../../../../InputRow/InputRow";
import InputField from "../../../../InputField/InputField";
import InputLabel from "../../../../InputLabel/InputLabel";
import DatePickerInput from "../../../../DatePickerInput/DatePickerInput";
import DatepickerMonthElement from "../../../../DatepickerMonthElement/DatepickerMonthElement";
import { validateHtml } from "../../../../../helpers/ValidationHelper";
import ActionButtonGroup from "../../../../Layouts/Internal/ActionButtonGroup/ActionButtonGroup";
import { updateAccolades } from "../../../../../store/actions/organizationActions";
import EFHtmlInput from "../../../../EFHtmlInput/EFHtmlInput";
import EFPrimaryModal from "../../../../Modals/EFPrimaryModal/EFPrimaryModal";

const AddOrgEvent = ({ event, organizationId, show, onHide, edit }) => {
  const dispatch = useDispatch();

  const [validated, setValidated] = useState(false);
  const [localEvent, setLocalEvent] = useState({ title: "", subTitle: "", eventDate: null, description: "", _id: "" });
  const [eventDateError, setEventDateError] = useState(false);

  const inProgress = useSelector(state => state.portfolio.inProgress);

  useEffect(() => {
    if (edit) setLocalEvent(...event);
  }, []);

  const onChange = e => {
    setLocalEvent({ ...localEvent, [e.target.name]: e.target.value });
  };

  const onDateChange = (name, date) => {
    setLocalEvent({ ...localEvent, [name]: date.toISOString() });
  };

  const onEditorStateChange = editorState => {
    setLocalEvent({
      ...localEvent,
      description: editorState
    });
  };

  const onSubmit = formEvent => {
    formEvent.preventDefault();
    const { eventDate } = localEvent;
    if (!validated) {
      setValidated(true);
    }
    if (!eventDate) {
      setEventDateError(true);
    } else if (formEvent.currentTarget.checkValidity() && eventDate) {
      let eventData = { ...localEvent };
      const formData = new FormData();
      eventData = {
        ...eventData,
        description: validateHtml(eventData.description),
        key: "events"
      };
      Object.keys(eventData).forEach(key => formData.append(key, eventData[key]));
      dispatch(updateAccolades(organizationId, formData));
      setLocalEvent({
        title: "",
        subTitle: "",
        eventDate: null,
        description: "",
        _id: ""
      });
    }
  };

  return (
    <EFPrimaryModal
      title="Add Event"
      isOpen={show}
      allowBackgroundClickClose={false}
      onClose={onHide}
      widthTheme="medium"
    >
      <Form noValidate validated={validated} onSubmit={onSubmit}>
        <InputRow>
          <Col sm={6}>
            <InputLabel theme="internal">Title</InputLabel>
            <InputField
              name="title"
              placeholder="Add Title"
              theme="internal"
              size="lg"
              onChange={onChange}
              value={localEvent.title}
              required
              errorMessage="Title is required"
              maxLength={30}
            />
          </Col>
          <Col sm={6}>
            <InputLabel theme="internal">Sub Title</InputLabel>
            <InputField
              name="subTitle"
              placeholder="Add Sub Title"
              theme="internal"
              size="lg"
              onChange={onChange}
              value={localEvent.subTitle}
              required
              errorMessage="Sub title is required"
              maxLength={30}
            />
          </Col>
        </InputRow>
        <InputRow>
          <Col sm={6}>
            <InputLabel theme="internal">Event Date</InputLabel>
            <DatePickerInput
              date={localEvent.eventDate}
              dateHandler={date => onDateChange("eventDate", date)}
              isOutsideRange={() => false}
              renderMonthElement={calenderProps => <DatepickerMonthElement {...calenderProps} includeCurrentYear />}
              numberOfMonths={1}
              direction="down"
            />
            {eventDateError && (
              <FormControl.Feedback type="invalid" style={{ display: "block" }}>
                Event Date is required
              </FormControl.Feedback>
            )}
          </Col>
        </InputRow>
        <InputRow>
          <Col sm={12}>
            <InputLabel theme="internal">Description</InputLabel>
          </Col>
          <Col sm={12}>
            <EFHtmlInput value={localEvent.description} onChange={onEditorStateChange} />
          </Col>
        </InputRow>
        <ActionButtonGroup onCancel={onHide} disabled={inProgress} />
      </Form>
    </EFPrimaryModal>
  );
};

export default AddOrgEvent;
