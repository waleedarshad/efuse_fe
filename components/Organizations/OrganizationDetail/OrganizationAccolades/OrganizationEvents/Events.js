import React, { Component } from "react";
import { connect } from "react-redux";
import { faPlus } from "@fortawesome/pro-solid-svg-icons";

import DisplayEvent from "../../../../User/Portfolio/Accolades/Events/DisplayEvent";
import AddButton from "../../../../User/Portfolio/AddButton/AddButton";
import AddOrgEvent from "./AddOrgEvent";
import { removeAccolades, toggleOrganizationModal } from "../../../../../store/actions/organizationActions";
import { getOwnerAndCaptain } from "../../../../../helpers/OrganizationHelper";

class Events extends Component {
  state = {
    edit: false,
    event: null
  };

  loadModal = () => {
    this.props.toggleOrganizationModal(true, "event");
  };

  onHide = () => {
    this.props.toggleOrganizationModal(false);
    this.setState({ edit: false, event: null });
  };

  editModal = eventObj => {
    this.props.toggleOrganizationModal(true, "event");
    this.setState({ edit: true, event: eventObj });
  };

  render() {
    const { modal, modalSection, currentUser, organization, mockExternalOrganization } = this.props;
    const { events, _id } = organization;
    let { owner } = getOwnerAndCaptain(currentUser, organization);
    if (mockExternalOrganization) {
      owner = false;
    }
    const { edit, event } = this.state;
    const content =
      events &&
      events.map((eventObj, index) => {
        return (
          <DisplayEvent
            key={index}
            event={eventObj}
            displayHr={events.length !== index + 1}
            onEdit={() => this.editModal(eventObj)}
            onRemove={() => {
              this.props.removeAccolades(_id, "events", eventObj._id);
            }}
            owner={owner}
            organizationId={_id}
          />
        );
      });
    return (
      <div>
        {events && events.length > 0 && content}
        {owner && (
          <>
            <AddButton desc="Add Event" icon={faPlus} onClick={this.loadModal} />
            {modal && modalSection === "event" && !edit && (
              <AddOrgEvent show={modal} onHide={this.onHide} event={event} organizationId={_id} />
            )}
          </>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  modal: state.organizations.modal,
  modalSection: state.organizations.modalSection,
  currentUser: state.auth.currentUser,
  organization: state.organizations.organization,
  mockExternalOrganization: state.organizations.mockExternalOrganization
});

export default connect(mapStateToProps, {
  toggleOrganizationModal,
  removeAccolades
})(Events);
