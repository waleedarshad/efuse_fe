import portfolioTabsLayout from "../../../hoc/portfolioTabsLayout/portfolioTabsLayout";
import Events from "./OrganizationEvents/Events";
import Honors from "./OrganizationHonors/Honors";

export default portfolioTabsLayout({
  feature: "organization_accolades_section",
  id: "organization-accolades-section",
  tabs: [
    {
      title: "Events",
      component: Events,
      tabFeature: "organization_event_section",
      auth: "events"
    },
    {
      title: "Honors",
      component: Honors,
      tabFeature: "organization_honor_section",
      auth: "honors"
    }
  ]
});
