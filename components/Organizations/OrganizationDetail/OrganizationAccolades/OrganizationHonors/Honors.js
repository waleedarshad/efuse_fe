import React, { Component } from "react";
import { connect } from "react-redux";
import { faPlus } from "@fortawesome/pro-solid-svg-icons";

import DisplayHonor from "../../../../User/Portfolio/Accolades/Honors/DisplayHonor";
import AddOrgHonor from "./AddOrgHonor";
import AddButton from "../../../../User/Portfolio/AddButton/AddButton";
import { removeAccolades, toggleOrganizationModal } from "../../../../../store/actions/organizationActions";
import { getOwnerAndCaptain } from "../../../../../helpers/OrganizationHelper";

class Honors extends Component {
  state = {
    edit: false,
    honor: null
  };

  loadModal = () => {
    this.props.toggleOrganizationModal(true, "honor");
  };

  onHide = () => {
    this.props.toggleOrganizationModal(false);
    this.setState({ edit: false, honor: null });
  };

  editModal = honorObj => {
    this.props.toggleOrganizationModal(true, "honor");
    this.setState({ edit: true, honor: honorObj });
  };

  render() {
    const { modal, modalSection, currentUser, organization, mockExternalOrganization } = this.props;
    const { honors, _id } = organization;
    let { owner } = getOwnerAndCaptain(currentUser, organization);
    if (mockExternalOrganization) {
      owner = false;
    }
    const { edit, honor } = this.state;
    const content =
      honors &&
      honors.map((honorObj, index) => {
        return (
          <DisplayHonor
            key={index}
            honor={honorObj}
            displayHr={honors.length !== index + 1}
            onEdit={() => this.editModal(honorObj)}
            onRemove={() => {
              this.props.removeAccolades(_id, "honors", honorObj._id);
            }}
            owner={owner}
            organizationId={_id}
          />
        );
      });
    return (
      <div>
        {honors && honors.length > 0 && content}
        {owner && (
          <>
            <AddButton desc="Add Honors" icon={faPlus} onClick={this.loadModal} />
            {modal && modalSection === "honor" && !edit && (
              <AddOrgHonor show={modal} onHide={this.onHide} honor={honor} organizationId={_id} />
            )}
          </>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  modal: state.organizations.modal,
  modalSection: state.organizations.modalSection,
  currentUser: state.auth.currentUser,
  organization: state.organizations.organization,
  mockExternalOrganization: state.organizations.mockExternalOrganization
});

export default connect(mapStateToProps, {
  toggleOrganizationModal,
  removeAccolades
})(Honors);
