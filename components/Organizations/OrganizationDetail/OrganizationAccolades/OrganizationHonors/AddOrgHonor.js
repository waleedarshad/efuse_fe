import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Form, Col } from "react-bootstrap";
import InputRow from "../../../../InputRow/InputRow";
import InputField from "../../../../InputField/InputField";
import InputLabel from "../../../../InputLabel/InputLabel";
import { validateHtml } from "../../../../../helpers/ValidationHelper";
import ActionButtonGroup from "../../../../Layouts/Internal/ActionButtonGroup/ActionButtonGroup";
import { updateAccolades } from "../../../../../store/actions/organizationActions";
import EFHtmlInput from "../../../../EFHtmlInput/EFHtmlInput";
import EFPrimaryModal from "../../../../Modals/EFPrimaryModal/EFPrimaryModal";

const AddOrgHonor = ({ edit, honor, show, onHide, organizationId }) => {
  const dispatch = useDispatch();

  const [validated, setValidated] = useState(false);
  const [localHonor, setLocalHonor] = useState({
    title: "",
    subTitle: "",
    description: "",
    _id: ""
  });

  const inProgress = useSelector(state => state.portfolio.inProgress);

  useEffect(() => {
    if (edit) {
      setLocalHonor(...honor);
    }
  }, []);

  const onChange = e => {
    setLocalHonor({
      ...localHonor,
      [e.target.name]: e.target.value
    });
  };

  const onEditorStateChange = editorState => {
    setLocalHonor({
      ...localHonor,
      description: editorState
    });
  };

  const onSubmit = event => {
    event.preventDefault();
    if (!validated) {
      setValidated(true);
    }
    if (event.currentTarget.checkValidity()) {
      let honorData = { ...localHonor };
      honorData = {
        ...honorData,
        description: validateHtml(honorData.description),
        key: "honors"
      };
      const formData = new FormData();
      Object.keys(honorData).forEach(key => formData.append(key, honorData[key]));
      dispatch(updateAccolades(organizationId, formData));
      setLocalHonor({
        title: "",
        subTitle: "",
        description: "",
        _id: ""
      });
    }
  };

  return (
    <EFPrimaryModal
      title="Add Honor"
      isOpen={show}
      allowBackgroundClickClose={false}
      onClose={onHide}
      widthTheme="medium"
    >
      <Form noValidate validated={validated} onSubmit={onSubmit}>
        <InputRow>
          <Col sm={6}>
            <InputLabel theme="internal">Title</InputLabel>
            <InputField
              name="title"
              placeholder="Add Title"
              theme="internal"
              size="lg"
              onChange={onChange}
              value={localHonor.title}
              required
              errorMessage="Title is required"
              maxLength={30}
            />
          </Col>
          <Col sm={6}>
            <InputLabel theme="internal">Sub Title</InputLabel>
            <InputField
              name="subTitle"
              placeholder="Add Sub Title"
              theme="internal"
              size="lg"
              onChange={onChange}
              value={localHonor.subTitle}
              required
              errorMessage="Sub title is required"
              maxLength={30}
            />
          </Col>
        </InputRow>
        <InputRow>
          <Col sm={12}>
            <InputLabel theme="internal">Description</InputLabel>
          </Col>
          <Col sm={12}>
            <EFHtmlInput value={localHonor.description} onChange={onEditorStateChange} />
          </Col>
        </InputRow>
        <ActionButtonGroup onCancel={onHide} disabled={inProgress} />
      </Form>
    </EFPrimaryModal>
  );
};

export default AddOrgHonor;
