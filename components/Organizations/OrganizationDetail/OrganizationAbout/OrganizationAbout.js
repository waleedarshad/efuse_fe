import React from "react";
import { useDispatch, useSelector } from "react-redux";

import { toggleOrganizationModal } from "../../../../store/actions/organizationActions";
import { EFHtmlParser } from "../../../EFHtmlParser/EFHtmlParser";
import EFProfileCard from "../../../Cards/EFProfileCard/EFProfileCard";
import AddOrgAbout from "./AddOrgAbout";

const OrganizationAbout = ({ about, organizationId, isAdmin }) => {
  const dispatch = useDispatch();
  const modal = useSelector(state => state.organizations.modal);
  const modalSection = useSelector(state => state.organizations.modalSection);

  const loadModal = () => {
    dispatch(toggleOrganizationModal(true, "about"));
  };

  const onHide = () => {
    dispatch(toggleOrganizationModal(false));
  };

  return (
    <EFProfileCard title="About" onButtonClick={loadModal} buttonType={about ? "edit" : "plus"} showButton={isAdmin}>
      <>
        {isAdmin && (
          <>
            {modalSection === "about" && (
              <AddOrgAbout show={modal} onHide={onHide} edit about={about} organizationId={organizationId} />
            )}
          </>
        )}
        <EFHtmlParser>{about}</EFHtmlParser>
      </>
    </EFProfileCard>
  );
};

export default OrganizationAbout;
