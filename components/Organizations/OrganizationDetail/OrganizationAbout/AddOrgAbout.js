import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Form, Col } from "react-bootstrap";
import InputRow from "../../../InputRow/InputRow";
import ActionButtonGroup from "../../../Layouts/Internal/ActionButtonGroup/ActionButtonGroup";
import { getTextFromHtmlString } from "../../../../helpers/GeneralHelper";
import { updateAboutSection } from "../../../../store/actions/organizationActions";
import EFHtmlInput from "../../../EFHtmlInput/EFHtmlInput";
import EFPrimaryModal from "../../../Modals/EFPrimaryModal/EFPrimaryModal";

const AddOrgAbout = ({ show, onHide, about, organizationId }) => {
  const dispatch = useDispatch();

  const [localAbout, setLocalAbout] = useState(about);
  const [contentError, setContentError] = useState(false);

  const inProgress = useSelector(state => state.portfolio.inProgress);

  const emptyAboutText = aboutText => getTextFromHtmlString(aboutText).trim().length === 0;

  const onEditorStateChange = editorState => {
    setLocalAbout(editorState);
    setContentError(emptyAboutText(editorState));
  };

  const onSubmit = event => {
    event.preventDefault();
    let error = contentError;

    if (contentError || emptyAboutText(localAbout)) {
      error = true;
    } else {
      error = false;
      dispatch(updateAboutSection(organizationId, localAbout));
    }
    setContentError(error);
  };

  return (
    <EFPrimaryModal
      title="Edit About Section"
      isOpen={show}
      allowBackgroundClickClose={false}
      onClose={onHide}
      widthTheme="medium"
    >
      <Form noValidate onSubmit={onSubmit}>
        <InputRow>
          <Col sm={12}>
            <EFHtmlInput
              value={localAbout}
              onChange={onEditorStateChange}
              displayError={contentError}
              errorMessage="Content is required"
            />
          </Col>
        </InputRow>
        <ActionButtonGroup onCancel={onHide} disabled={inProgress} />
      </Form>
    </EFPrimaryModal>
  );
};

AddOrgAbout.defaultProps = {
  about: ""
};

export default AddOrgAbout;
