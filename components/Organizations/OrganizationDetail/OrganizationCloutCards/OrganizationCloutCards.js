import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import EFCloutCard from "../../../EFCloutCards/EFCloutCard/EFCloutCard";
import SOCIAL_LINKING_STATIC_DATA from "../../../../common/socialLinkingStaticData";
import buildAccountData from "../../../../builders/SocialDynamicContextBuilder";
import {
  getTwitterAccountInfo,
  getTwitterAccountLinkedStatus,
  getYouTubeAccountInfo,
  getYouTubeAccountLinkedStatus,
  getTwitchAccountInfo,
  getTwitchAccountLinkedStatus
} from "../../../../store/actions/socialLinkingActions/socialLinkingActions";
import EfuseCloutCard from "../../../EfuseCloutCard/EfuseCloutCard";
import Style from "./OrganizationCloutCards.module.scss";

const OrganizationCloutCards = ({ owner, ownerId, organization }) => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getTwitterAccountLinkedStatus(ownerId));
    dispatch(getTwitterAccountInfo(ownerId));
    dispatch(getYouTubeAccountLinkedStatus(ownerId));
    dispatch(getYouTubeAccountInfo(ownerId));
    dispatch(getTwitchAccountLinkedStatus(ownerId));
    dispatch(getTwitchAccountInfo(ownerId));
  }, []);

  const {
    twitterLinked,
    twitterAccountInfo,
    youTubeLinked,
    youTubeAccountInfo,
    twitchLinked,
    twitchAccountInfo
  } = useSelector(state => state.socialLinking);

  const twitterAccountUrl = `https://www.twitter.com/${twitterAccountInfo.accountName}`;
  const youTubeAccountUrl = `https://www.youtube.com/channel/${youTubeAccountInfo.id}`;
  const twitchAccountUrl = `https://www.twitch.tv/${twitchAccountInfo.accountName}`;

  const twitterAccount = { ...buildAccountData(twitterLinked, twitterAccountInfo), accountUrl: twitterAccountUrl };
  const youTubeAccount = { ...buildAccountData(youTubeLinked, youTubeAccountInfo), accountUrl: youTubeAccountUrl };
  const twitchAccount = { ...buildAccountData(twitchLinked, twitchAccountInfo), accountUrl: twitchAccountUrl };

  const ownerContext = {
    owner,
    ownerId,
    ownerKind: "organization",
    analyticsLocation: "organization"
  };

  return (
    <div className={Style.cardsContainer}>
      <div className={Style.efuseCard}>
        <EfuseCloutCard cardSize="large" username={organization.shortName} followers={organization.followersCount} />
      </div>

      {(twitterAccount?.accountLinked || owner) && (
        <EFCloutCard
          socialType={SOCIAL_LINKING_STATIC_DATA.TWITTER}
          accountData={twitterAccount}
          owner={ownerContext}
        />
      )}
      {(youTubeAccount?.accountLinked || owner) && (
        <EFCloutCard
          socialType={SOCIAL_LINKING_STATIC_DATA.YOUTUBE}
          accountData={youTubeAccount}
          owner={ownerContext}
        />
      )}
      {(twitchAccount?.accountLinked || owner) && (
        <EFCloutCard socialType={SOCIAL_LINKING_STATIC_DATA.TWITCH} accountData={twitchAccount} owner={ownerContext} />
      )}
    </div>
  );
};

export default OrganizationCloutCards;
