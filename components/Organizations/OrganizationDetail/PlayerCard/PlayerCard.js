import React, { Component } from "react";
import { connect } from "react-redux";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus, faPencilAlt, faTrash } from "@fortawesome/pro-solid-svg-icons";
import Link from "next/link";
import AddPlayerCard from "./AddPlayerCard/AddPlayerCard";
import Style from "./PlayerCard.module.scss";
import { getImage } from "../../../../helpers/GeneralHelper";
import { removePlayerCard, toggleOrganizationModal } from "../../../../store/actions/organizationActions";
import ConfirmAlert from "../../../ConfirmAlert/ConfirmAlert";
import { withCdn } from "../../../../common/utils";

class PlayerCard extends Component {
  state = {
    edit: false,
    card: null
  };

  loadModal = () => {
    this.props.toggleOrganizationModal(true, "card");
    this.setState({ edit: false });
  };

  onHide = () => {
    this.props.toggleOrganizationModal(false);
    this.setState({ edit: false, card: null });
  };

  editModal = cardObj => {
    this.props.toggleOrganizationModal(true, "card");
    this.setState({ edit: true, playerCard: cardObj });
  };

  removePlayerCard = (orgId, cardId) => {
    this.props.removePlayerCard(orgId, cardId);
  };

  render() {
    const { modal, modalSection, currentUser, organization, owner } = this.props;
    const { edit, playerCard } = this.state;
    const { playerCards } = organization;
    return (
      <div className={Style.container}>
        {owner && (
          <div className={Style.memberCard} onClick={this.loadModal}>
            <div className={Style.shadow} />
            <img className={`${Style.image} ${Style.overlay}`} src={withCdn("/static/images/landingbg_mobile.jpg")} />
            <div className={Style.textContainer}>
              <p className={Style.add}>
                <FontAwesomeIcon icon={faPlus} title="add" className={Style.addIcon} />
                Add
              </p>
            </div>
            <p className={Style.viewProfile}>Create Card</p>
          </div>
        )}
        {modal && modalSection === "card" && (
          <AddPlayerCard
            show={modal}
            onHide={this.onHide}
            edit={edit}
            organization={organization}
            playerCard={playerCard}
          />
        )}
        {playerCards &&
          playerCards.map((playerCard, i) => (
            <div className={Style.memberCard} key={i}>
              <a href={playerCard.url}>
                <div className={Style.shadow} />
                <img className={Style.image} alt="player card" src={getImage(playerCard.image, "url")} />
                <div className={Style.textContainer}>
                  <p>{playerCard.title}</p>
                </div>
                <p className={Style.viewProfile}>{playerCard.subTitle}</p>
              </a>
              {owner && (
                <div className={Style.cardActions}>
                  <FontAwesomeIcon icon={faPencilAlt} title="edit" onClick={() => this.editModal(playerCard)} />
                  <ConfirmAlert
                    title="This action can't be undone"
                    message="Are you sure you want to remove?"
                    onYes={() => this.removePlayerCard(organization._id, playerCard._id)}
                  >
                    <FontAwesomeIcon icon={faTrash} title="delete" />
                  </ConfirmAlert>
                </div>
              )}
            </div>
          ))}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  modal: state.organizations.modal,
  modalSection: state.organizations.modalSection,
  currentUser: state.auth.currentUser,
  organization: state.organizations.organization
});

export default connect(mapStateToProps, {
  toggleOrganizationModal,
  removePlayerCard
})(PlayerCard);
