import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Form, Col } from "react-bootstrap";
import InputRow from "../../../../InputRow/InputRow";
import InputField from "../../../../InputField/InputField";
import InputLabel from "../../../../InputLabel/InputLabel";
import { rescueNil } from "../../../../../helpers/GeneralHelper";
import ActionButtonGroup from "../../../../Layouts/Internal/ActionButtonGroup/ActionButtonGroup";
import { managePlayerCards } from "../../../../../store/actions/organizationActions";
import ImageUploader from "../../../../ImageUploader/ImageUploader";
import EFPrimaryModal from "../../../../Modals/EFPrimaryModal/EFPrimaryModal";

const AddPlayerCard = ({ show, onHide, edit, playerCard, organization }) => {
  const dispatch = useDispatch();

  const inProgress = useSelector(state => state.portfolio.inProgress);

  const [validated, setValidated] = useState(false);
  const [player, setPlayer] = useState({
    title: "",
    subTitle: "",
    url: "",
    image: {},
    _id: ""
  });

  useEffect(() => {
    if (edit) {
      setPlayer({ ...playerCard });
    }
  }, []);

  const onChange = event => {
    setPlayer({
      ...player,
      [event.target.name]: event.target.value
    });
  };

  const onDrop = (selectedFile, name) => {
    setPlayer({
      ...player,
      [name]: selectedFile[0]
    });
  };

  const onSubmit = event => {
    event.preventDefault();
    const cardData = new FormData();
    if (!validated) {
      setValidated(true);
    }
    if (event.currentTarget.checkValidity()) {
      Object.keys(player).forEach(key => {
        if (player[key] !== undefined) {
          cardData.append(key, player[key]);
        }
      });
      dispatch(managePlayerCards(organization._id, cardData));
      setPlayer({
        title: "",
        subTitle: "",
        url: "",
        image: {},
        _id: ""
      });
    }
  };

  const { title, subTitle, url, image } = player;
  return (
    <EFPrimaryModal
      title={`${edit ? "Edit" : "Add"} Card`}
      widthTheme="medium"
      allowBackgroundClickClose={false}
      isOpen={show}
      onClose={onHide}
    >
      <Form noValidate validated={validated} onSubmit={onSubmit}>
        <InputRow>
          <Col sm={6}>
            <InputLabel theme="internal">Title (16 characters)</InputLabel>
            <InputField
              name="title"
              placeholder="Add Title"
              theme="internal"
              size="lg"
              onChange={onChange}
              value={title}
              required
              errorMessage="Title is required"
              maxLength={16}
            />
          </Col>
          <Col sm={6}>
            <InputLabel theme="internal">Sub Title (24 characters)</InputLabel>
            <InputField
              name="subTitle"
              placeholder="Add Sub Title"
              theme="internal"
              size="lg"
              onChange={onChange}
              value={subTitle}
              required
              errorMessage="Sub title is required"
              maxLength={24}
            />
          </Col>
        </InputRow>
        <InputRow>
          <Col sm={6}>
            <InputLabel theme="internal">Redirect URL (redirect when clicked)</InputLabel>
            <InputField
              name="url"
              placeholder="ex: https://efuse.gg/lounge"
              theme="internal"
              size="lg"
              onChange={onChange}
              value={url}
              required
              errorMessage="Redirect URL is required and must contain https://"
              pattern="(http|ftp|https)://([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?^=%&:/~+#-]*[\w@?^=%&/~+#-])?"
            />
          </Col>
          <Col sm={6}>
            <ImageUploader
              text="Select Custom Image"
              name="image"
              onDrop={onDrop}
              theme="internal"
              value={rescueNil(image, "url")}
              showCropper={false}
            />
          </Col>
        </InputRow>
        <ActionButtonGroup onCancel={onHide} disabled={inProgress} />
      </Form>
    </EFPrimaryModal>
  );
};

export default AddPlayerCard;
