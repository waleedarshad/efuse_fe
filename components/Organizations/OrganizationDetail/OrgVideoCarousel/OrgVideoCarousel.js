import React, { Component } from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus, faFilm, faTrash } from "@fortawesome/pro-solid-svg-icons";
import { connect } from "react-redux";
import { withRouter } from "next/router";
import VideoPlayer from "../../../VideoPlayer/VideoPlayer";
import Style from "./OrgVideoCarousel.module.scss";
import AddVideoCarousel from "./AddVideoCarousel/AddVideoCarousel";
import Carousel from "../../../Carousels/Carousel/Carousel";
import { removeVideoCarousel, toggleOrganizationModal } from "../../../../store/actions/organizationActions";
import ConfirmAlert from "../../../ConfirmAlert/ConfirmAlert";

class VideoCarousel extends Component {
  state = {
    edit: false,
    videoInfo: null
  };

  loadModal = () => {
    this.props.toggleOrganizationModal(true, "videoInfo");
  };

  onHide = () => {
    this.props.toggleOrganizationModal(false);
    this.setState({ edit: false, videoInfo: null });
  };

  editModal = () => {
    const { videoInfo } = this.props.organization;
    this.props.toggleOrganizationModal(true, "videoInfo");
    this.setState({ edit: true, videoInfo });
  };

  remove = videoId => {
    this.props.removeVideoCarousel(this.props.organization._id, videoId);
  };

  render() {
    const { modal, modalSection, organization, owner, playingVideo, setCurrentVideo } = this.props;
    const { edit } = this.state;
    const videoInfo = organization?.videoCarousel;
    return (
      <div className={Style.container}>
        <Carousel items={videoInfo}>
          {this.props.owner && (
            <div className={Style.addContainer} onClick={this.loadModal}>
              <FontAwesomeIcon icon={faFilm} className={Style.film} />
              <div className={Style.plus}>
                <FontAwesomeIcon icon={faPlus} className={Style.icon} />
                Add Video/Clip
              </div>
            </div>
          )}
          {videoInfo &&
            videoInfo.map(item => {
              return (
                <div className={Style.item} key={item}>
                  <VideoPlayer
                    url={item.url ? item.url : item?.video?.url || ""}
                    height="200px"
                    light={!!item.url}
                    autoplay={playingVideo === item.url}
                    onPlay={() => setCurrentVideo(item.url)}
                  />
                  <p className={Style.title}>{item.title}</p>
                  {owner && (
                    <ConfirmAlert
                      title="Delete this video/clip?"
                      message="Are you sure?"
                      onYes={() => this.remove(item._id)}
                    >
                      <FontAwesomeIcon className={Style.trash} icon={faTrash} />
                    </ConfirmAlert>
                  )}
                </div>
              );
            })}
        </Carousel>
        {modal && modalSection === "videoInfo" && (
          <AddVideoCarousel
            show={modal}
            onHide={this.onHide}
            edit={edit}
            videoInfo={this.state.videoInfo}
            organization={organization}
          />
        )}
      </div>
    );
  }
}
const mapStateToProps = state => ({
  modal: state.organizations.modal,
  modalSection: state.organizations.modalSection,
  currentUser: state.auth.currentUser
});

export default connect(mapStateToProps, {
  toggleOrganizationModal,
  removeVideoCarousel
})(withRouter(VideoCarousel));
