import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Form, Col } from "react-bootstrap";
import isEmpty from "lodash/isEmpty";
import InputRow from "../../../../InputRow/InputRow";
import InputField from "../../../../InputField/InputField";
import InputLabel from "../../../../InputLabel/InputLabel";
import ActionButtonGroup from "../../../../Layouts/Internal/ActionButtonGroup/ActionButtonGroup";
import { updateVideoCarousel } from "../../../../../store/actions/organizationActions";
import Error from "../../../../Error/Error";
import UploadVideoButton from "../../../../UploadVideoButton";
import Checkbox from "../../../../Checkbox/Checkbox";
import EFPrimaryModal from "../../../../Modals/EFPrimaryModal/EFPrimaryModal";

const AddVideoCarousel = ({ organization, show, onHide }) => {
  const dispatch = useDispatch();

  const inProgress = useSelector(state => state.portfolio.inProgress);

  const [validated, setValidated] = useState(false);
  const [videoInfo, setVideoInfo] = useState({
    title: "",
    url: "",
    video: {},
    uploaded: false,
    _id: ""
  });

  const onChange = event => {
    setVideoInfo({ ...videoInfo, [event.target.name]: event.target.value });
  };

  const onFileUpload = file => {
    setVideoInfo({
      ...videoInfo,
      video: file
    });
  };

  const onVideoChange = event => {
    setVideoInfo({
      ...videoInfo,
      uploaded: event.target.checked
    });
  };

  const onClearMedia = () => {
    setVideoInfo({
      ...videoInfo,
      removeMedia: true,
      videoFile: {}
    });
  };

  const onSubmit = event => {
    event.preventDefault();
    if (!validated) {
      setValidated(true);
    }
    if (event.currentTarget.checkValidity()) {
      dispatch(updateVideoCarousel(organization._id, videoInfo));
    }
  };

  const { title, url, video, uploaded } = videoInfo;

  const filePresent = !isEmpty(video);
  return (
    <EFPrimaryModal
      title="Add a Video/Clip"
      isOpen={show}
      allowBackgroundClickClose={false}
      onClose={onHide}
      widthTheme="medium"
    >
      <Error />
      <Form noValidate validated={validated} onSubmit={onSubmit}>
        <InputRow>
          <Col sm={6}>
            <br />
            <Checkbox
              custom
              name="uploaded"
              type="checkbox"
              id="uploadVideo"
              label={uploaded ? "Add a video link" : "Upload a video"}
              theme="internal"
              checked={uploaded}
              value={uploaded}
              onChange={onVideoChange}
            />
          </Col>
          <Col sm={6}>
            {uploaded ? (
              <>
                <br />
                <UploadVideoButton
                  onFileUpload={onFileUpload}
                  onClearMedia={onClearMedia}
                  filePresent={filePresent}
                  directory="uploads/organization/"
                  placeholder="Upload Video"
                />
              </>
            ) : (
              <>
                <InputLabel theme="internal">Video Link</InputLabel>
                <InputField
                  name="url"
                  placeholder="Add a video URL"
                  theme="internal"
                  size="lg"
                  onChange={onChange}
                  value={url}
                  required
                  errorMessage="URL is required"
                  maxLength={500}
                />
              </>
            )}
          </Col>
        </InputRow>
        <InputRow>
          <Col sm={12}>
            <InputLabel theme="internal">Title (Max: 80 characters)</InputLabel>
            <InputField
              name="title"
              placeholder="Add Title"
              theme="internal"
              size="lg"
              onChange={onChange}
              value={title}
              required
              errorMessage="Title is required"
              maxLength={80}
            />
          </Col>
        </InputRow>
        <ActionButtonGroup onCancel={onHide} disabled={inProgress} />
      </Form>
    </EFPrimaryModal>
  );
};

export default AddVideoCarousel;
