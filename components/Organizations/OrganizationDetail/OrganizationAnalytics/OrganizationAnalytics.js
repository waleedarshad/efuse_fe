import React, { Component } from "react";
import Style from "./OrganizationAnalytics.module.scss";

class OrganizationAnalytics extends Component {
  render() {
    return (
      <div className={Style.container}>
        <div className={Style.analyticContainer}>
          <img
            className={Style.icon}
            src="https://i.pinimg.com/originals/72/75/5a/72755a0272c5f87266c80e3a28b85c76.png"
          />
          <div className={Style.text}>
            <span className={Style.title}>People Engaged</span>
            <p className={Style.number}>891K</p>
          </div>
        </div>
        <div className={Style.analyticContainer}>
          <img
            className={Style.icon}
            src="https://cdn0.iconfinder.com/data/icons/kameleon-free-pack-rounded/110/Analytics-512.png"
          />
          <div className={Style.text}>
            <span className={Style.title}>Page Views</span>
            <p className={Style.number}>2.1M</p>
          </div>
        </div>
        <div className={Style.analyticContainer}>
          <img className={Style.icon} src="https://www.pngrepo.com/png/190371/170/analytics-data-analytics.png" />
          <div className={Style.text}>
            <span className={Style.title}>Impressions</span>
            <p className={Style.number}>8.74M</p>
          </div>
        </div>
      </div>
    );
  }
}
export default OrganizationAnalytics;
