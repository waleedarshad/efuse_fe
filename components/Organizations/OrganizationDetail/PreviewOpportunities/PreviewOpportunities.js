import React from "react";
import { faExternalLinkAlt } from "@fortawesome/pro-solid-svg-icons";

import OpportunityList from "../../../OpportunitiesV2/OpportunityListLayout/OpportunityList/OpportunityList";
import EFProfileCard from "../../../Cards/EFProfileCard/EFProfileCard";
import EFCircleIconButtonTooltip from "../../../Buttons/EFCircleIconButtonTooltip/EFCircleIconButtonTooltip";
import Style from "./PreviewOpportunities.module.scss";

const PreviewOpportunities = ({ opportunities, organizationId }) => {
  return (
    <div className={Style.opportunitiesWrapper}>
      <EFProfileCard customTitle="Opportunities">
        <div className={Style.linkButton}>
          <EFCircleIconButtonTooltip
            icon={faExternalLinkAlt}
            tooltipContent="View All Opportunities"
            internalHref={`/organizations/opportunities?id=${organizationId}`}
          />
        </div>
        <div className="mt-3">
          <OpportunityList opportunityList={opportunities} />
        </div>
      </EFProfileCard>
    </div>
  );
};

export default PreviewOpportunities;
