import React, { Component } from "react";
import Style from "./OrganizationImpact.module.scss";

class OrganizationImpact extends Component {
  render() {
    return (
      <div className={Style.container}>
        <div className={Style.header}>Impact</div>
        <div className={Style.impactContainer}>
          <div className={Style.impact}>
            <h3 className={Style.title}>Opportunities Created</h3>
            <p className={Style.number}>48</p>
            <p className={Style.viewMore}>View More</p>
          </div>
          <div className={Style.impact}>
            <h3 className={Style.title}>Thought Pieces Created</h3>
            <p className={Style.number}>501</p>
            <p className={Style.viewMore}>View More</p>
          </div>
          <div className={Style.impact}>
            <h3 className={Style.title}>Affiliate Gamers</h3>
            <p className={Style.number}>119</p>
            <p className={Style.viewMore}>View More</p>
          </div>
          <div className={Style.impact}>
            <h3 className={Style.title}>Sponserships Created</h3>
            <p className={Style.number}>27</p>
            <p className={Style.viewMore}>View More</p>
          </div>
        </div>
      </div>
    );
  }
}
export default OrganizationImpact;
