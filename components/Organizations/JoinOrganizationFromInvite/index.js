import React, { Component } from "react";
import { connect } from "react-redux";
import { Row, Col, Card } from "react-bootstrap";
import { joinOrganization } from "../../../store/actions/organizationActions";
import OrganizationLayout from "../../Layouts/OrganizationLayout/OrganizationLayout";
import OrganizationBioCard from "../../OrganizationBioCard";
import { getImage, rescueNil } from "../../../helpers/GeneralHelper";
import { getOwnerAndCaptain } from "../../../helpers/OrganizationHelper";
import Style from "./Style.module.scss";
import OrganizationAbout from "../OrganizationDetail/OrganizationAbout/OrganizationAbout";
import DynamicModal from "../../DynamicModal/DynamicModal";
import { toggleLoader } from "../../../store/actions/loaderActions";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";

class JoinOrganizationFromInvite extends Component {
  componentDidMount() {
    analytics.page("Organizations Invite Page");
    this.props.toggleLoader(true);
  }

  render() {
    const {
      query,
      organization,
      currentUser,
      isWindowView,
      orgIdFromInviteCode,
      inviteCode,
      joinOrganization: joinOrganizationAction,
      pageProps
    } = this.props;
    const { owner, captain } = getOwnerAndCaptain(currentUser, organization);
    const isPublic = currentUser?.id === undefined;

    const acceptButton = (
      <EFRectangleButton
        text="Accept"
        onClick={() => {
          if (!isPublic) joinOrganizationAction(`organizations/join/${organization._id}`, "Open", inviteCode.code);
        }}
      />
    );

    let mainContent = <></>;

    if (organization?._id) {
      mainContent = (
        <Card className="customCard pb-0">
          <Card.Body>
            <h4 className={Style.title}>You&apos;ve been invited to join {organization.name}.</h4>
            <p className={Style.description}>
              As part of an organization, you&apos;ll have access to it&apos;s page, appear as a member, and be the
              first to hear from them when they post.
            </p>
            <div className={Style.instructionText}>Please accept or decline this invitation:</div>
            {isPublic ? (
              <div className={Style.buttonContainer}>
                <DynamicModal
                  flow="LoginSignup"
                  openOnLoad={false}
                  startView="signup"
                  displayCloseButton
                  allowBackgroundClickClose={false}
                  inviteCode={inviteCode}
                >
                  {acceptButton}
                </DynamicModal>

                <DynamicModal
                  openOnLoad={false}
                  startView="signup"
                  displayCloseButton
                  allowBackgroundClickClose={false}
                  inviteCode={inviteCode}
                >
                  <EFRectangleButton text="Decline" />
                </DynamicModal>
              </div>
            ) : (
              <div className={Style.buttonContainer}>
                {acceptButton}
                <EFRectangleButton text="Decline" internalHref="/" />
              </div>
            )}
          </Card.Body>
        </Card>
      );
    }

    if (organization?.member) {
      mainContent = (
        <Card className="customCard pb-0">
          <Card.Body>
            <h4 className={Style.title}>You have successfully joined {organization.name}.</h4>
            <p className={Style.description}>
              As part of an organization, you&apos;ll have access to it&apos;s page, appear as a member, and be the
              first to hear from them when they post.
            </p>
            <div className={Style.buttonContainer}>
              <EFRectangleButton text={`View ${organization.name}`} internalHref={`/org/${organization.shortName}`} />
            </div>
          </Card.Body>
        </Card>
      );
    }

    return (
      <OrganizationLayout
        headerImage={getImage(organization.headerImage, "organization")}
        avatar={rescueNil(organization, "profileImage", {}) || {}}
        bioCard={<OrganizationBioCard />}
        bar="Organizations"
        query={query}
        isWindowView={isWindowView}
        owner={owner || captain}
        orgType={organization.status}
        showList={false}
        orgIdFromInviteCode={orgIdFromInviteCode}
        organizationFromSSR={pageProps.organization}
      >
        <Row>
          <Col sm={12}>{mainContent}</Col>
        </Row>
        <br />
        <OrganizationAbout about={organization.about} />
      </OrganizationLayout>
    );
  }
}

const mapStateToProps = state => ({
  organization: state.organizations.organization,
  currentUser: state.auth.currentUser,
  isWindowView: state.messages.isWindowView,
  followers: state.organizations.followers
});

export default connect(mapStateToProps, {
  joinOrganization,
  toggleLoader
})(JoinOrganizationFromInvite);
