import React, { useState } from "react";
import { Col, Form, FormControl } from "react-bootstrap";
import AsyncSelect from "react-select/lib/Async";
import isEmpty from "lodash/isEmpty";

import EFPrimaryModal from "../../Modals/EFPrimaryModal/EFPrimaryModal";
import InputRow from "../../InputRow/InputRow";
import SelectBox from "../../SelectBox/SelectBox";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";
import { isEmptyObject, getImage } from "../../../helpers/GeneralHelper";
import { searchForUsers } from "../../../helpers/SearchHelper";
import InputLabelWithHelpTooltip from "../../InputLabelWithHelpTooltip/InputLabelWithHelpTooltip";
import RadioInput from "./RadioInput";
import AccessRoles from "../../../static/data/AccessRoles.json";

// eslint-disable-next-line no-unused-vars
const AddMemberModal = ({ institution, isOpen, onClose }) => {
  const [member, setMember] = useState({});
  const [role, setRole] = useState("");
  const [title, setTitle] = useState("");
  const [validated, setValidated] = useState(false);
  const [memberEmptyError, setMemberEmptyError] = useState(false);
  const [roleEmptyError, setRoleEmptyError] = useState(false);

  const onChange = selectedOption => {
    setMemberEmptyError(isEmptyObject(selectedOption));
    setMember(selectedOption);
  };

  const loadOptions = (input, callback) => {
    searchForUsers(input).then(response => {
      const options = response.data.users.docs.map(user => ({
        value: user._id,
        label: (
          <div className="optionWithImage">
            <img src={getImage(user.profilePicture, "avatar")} alt="Profile" />{" "}
            <span>
              <strong>{user.name}</strong>{" "}
            </span>
          </div>
        )
      }));

      callback(options);
    });
  };

  const selectRole = roleText => {
    setRole(roleText);
    setRoleEmptyError(false);
  };

  const onSubmit = event => {
    event.preventDefault();

    setMemberEmptyError(isEmptyObject(member));
    setRoleEmptyError(isEmpty(role));
    setValidated(true);

    if (!isEmptyObject(member)) {
      // TODO: Wire up add user to work for orgs instead of institutions

      /* const data = {
        user: member.value,
        role,
        title
      };
      dispatch(addUser(institution, data)); */
      setMember({});
      setRole("");
      setTitle("");
      onClose();
    }
  };

  return (
    <EFPrimaryModal
      title="Add member to institution"
      allowBackgroundClickClose={false}
      isOpen={isOpen}
      onClose={onClose}
    >
      <Form onSubmit={e => onSubmit(e)} noValidate validated={validated}>
        <InputRow>
          <Col>
            <InputLabelWithHelpTooltip
              labelText="Select Member"
              tooltipText="Search for eFuse users by typing in their username or first and last name."
            />
            <AsyncSelect
              cacheOptions
              onChange={onChange}
              loadOptions={loadOptions}
              className="nav-home nav-search"
              value={member}
              placeholder="Select"
              id="dynamic-id-search-users"
            />

            {memberEmptyError && (
              <FormControl.Feedback type="invalid" style={{ display: "block" }}>
                Select one or more to invite
              </FormControl.Feedback>
            )}
          </Col>
        </InputRow>

        <InputRow>
          <Col>
            <InputLabelWithHelpTooltip
              labelText="Select Role"
              tooltipText="This controls how much access this user gets in your institution. You can always change this later."
            />
            <RadioInput title="Owner" onClick={() => selectRole("OWNER")} description={AccessRoles.roles.owner} />
            <RadioInput title="Admin" onClick={() => selectRole("ADMIN")} description={AccessRoles.roles.admin} />
            <RadioInput title="Member" onClick={() => selectRole("MEMBER")} description={AccessRoles.roles.member} />
            {roleEmptyError && (
              <FormControl.Feedback type="invalid" style={{ display: "block" }}>
                Select one role fot the member
              </FormControl.Feedback>
            )}
          </Col>
        </InputRow>
        <InputRow>
          <Col>
            <InputLabelWithHelpTooltip
              labelText="Select Title"
              tooltipText="Select one of the available options to easily identify your members. This does not have any affect on their features or permissions."
            />
            <SelectBox
              name="title"
              validated={validated}
              options={[
                { value: "", label: "Select Title" },
                { value: "coach", label: "Coach" },
                { value: "student", label: "Student" },
                { value: "director", label: "Director" }
              ]}
              size="lg"
              theme="whiteShadow"
              value={title}
              onChange={e => setTitle(e.target.value)}
              required
              errorMessage="Title is required"
            />
          </Col>
        </InputRow>
        <InputRow>
          <Col>
            <EFRectangleButton buttonType="submit" text="Add Member" />
          </Col>
        </InputRow>
      </Form>
    </EFPrimaryModal>
  );
};
export default AddMemberModal;
