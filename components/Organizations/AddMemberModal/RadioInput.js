import Style from "./RadioInput.module.scss";

const RadioInput = ({ title, onClick, description, checked }) => {
  return (
    <div class="radio">
      <label>
        <span className={Style.role}>{title}</span>
        <input type="radio" name="role" onClick={onClick} checked={checked} />
        <p>{description}</p>
      </label>
    </div>
  );
};

export default RadioInput;
