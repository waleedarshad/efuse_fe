import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import "react-table/react-table.css";
import { useRouter } from "next/router";
import InfiniteScroll from "react-infinite-scroller";
import { Col, Row } from "react-bootstrap";
import { faUser } from "@fortawesome/pro-light-svg-icons";
import { getImage, rescueNil } from "../../../../helpers/GeneralHelper";
import { getOwnerAndCaptain } from "../../../../helpers/OrganizationHelper";
import { getOrganizationMembers } from "../../../../store/actions/organizationActions";
import EFOrganizationInviteSection from "../../../EFOrganizationInviteSection/EFOrganizationInviteSection";
import Error from "../../../../pages/_error";
import OrganizationBioCard from "../../../OrganizationBioCard";
import OrganizationLayout from "../../../Layouts/OrganizationLayout/OrganizationLayout";
import TableItemMember from "../../Table/TableItem/TableItemMember";
import MainHeadingBar from "../../MainHeadingBar/MainHeadingBar";
import TableHead from "../../Table/TableHead/TableHead";
import AnimatedLogo from "../../../AnimatedLogo";
import NoRecordFound from "../../../NoRecordFound/NoRecordFound";

let page = 1;
const OrganizationMembersWrapper = ({ pageProps }) => {
  const router = useRouter();
  const { query } = router;

  const dispatch = useDispatch();

  const authUser = useSelector(state => state.auth.currentUser);
  const members = useSelector(state => state.organizations.members);
  const isWindowView = useSelector(state => state.messages.isWindowView);
  const responseCode = useSelector(state => state.errors.responseCode);
  const hasNextPage = useSelector(state => state.organizations.hasNextPage);
  const isGettingFirstMembers = useSelector(state => state.organizations.isGettingFirstMembers);

  const [membersHaveLoaded, setMembersHaveLoaded] = useState(false);

  const { owner, captain } = getOwnerAndCaptain(authUser, pageProps.organization);

  const loggedInUserIsAdmin = owner || captain;

  const organizationCaptains = pageProps.organization.captains;
  const organizationOwner = pageProps.organization.user;

  const hasChildOrgs = pageProps.organization?.children?.length > 0;

  const memberInviteCodes =
    pageProps.organization.inviteCodes &&
    pageProps.organization.inviteCodes.length > 0 &&
    pageProps.organization.inviteCodes.filter(code => code.type === "ORGANIZATION_MEMBER_INVITE");

  const orgHasMembersAndHasNotLoaded = () => {
    return pageProps.organization.membersCount !== members.length && pageProps.organization.membersCount !== 0;
  };

  useEffect(() => {
    if (orgHasMembersAndHasNotLoaded()) {
      dispatch(getOrganizationMembers(pageProps.organization._id, 1));
      setMembersHaveLoaded(true);
    } else if (pageProps.organization.membersCount === 0) {
      setMembersHaveLoaded(true);
    }
  }, [membersHaveLoaded]);

  const loadMore = () => {
    if (hasNextPage) {
      page += 1;
      dispatch(getOrganizationMembers(pageProps.organization._id, page));
    }
  };

  if (responseCode === 422 || responseCode === 400) {
    return <Error statusCode={responseCode} />;
  }

  const membersList = members.map((member, index) => {
    return (
      <Col lg={12} md={12} key={index}>
        <TableItemMember
          member={member}
          currentUser={authUser}
          slug={router.query.org}
          isAdmin={loggedInUserIsAdmin}
          owner={organizationOwner}
          captains={organizationCaptains}
          hasChildOrgs={hasChildOrgs}
          organizationId={pageProps.organization._id}
        />
      </Col>
    );
  });

  return (
    <OrganizationLayout
      headerImage={getImage(pageProps.organization.headerImage, "organization")}
      owner={owner || captain}
      avatar={rescueNil(pageProps.organization, "profileImage", {}) || {}}
      bioCard={<OrganizationBioCard />}
      bar="Organizations"
      query={query}
      isWindowView={isWindowView}
      orgType={pageProps.organization.status}
      organizationFromSSR={pageProps.organization}
    >
      {memberInviteCodes && loggedInUserIsAdmin && (
        <>
          {memberInviteCodes.map((code, index) => {
            return (
              <EFOrganizationInviteSection
                key={index}
                inviteCodes={code.code}
                numberOfSignups={code.signupCount}
                titleText="Invite users to become a member"
                bodyText=" Use your custom invite url to invite your members. Anyone with this link will be able to join this
                  organization as a member. Members are recognized as being a contributing individual to the
                  organization."
                buttonText="Invite Members"
                buttonColorTheme="secondary"
              />
            );
          })}
        </>
      )}
      {pageProps.organization.membersCount === 0 ? (
        <p>This organization does not have any members yet.</p>
      ) : (
        <div className="container pb-2">
          <MainHeadingBar
            heading="Members"
            icon={faUser}
            organization={router.query.org}
            isAdmin={loggedInUserIsAdmin}
            hasChildOrgs={hasChildOrgs}
          />
          {hasChildOrgs && (
            <TableHead column1="name" column2="organization" column2Size="3" column3="title" column3Size="2" />
          )}
          <InfiniteScroll
            pageStart={1}
            loadMore={loadMore}
            hasMore={hasNextPage}
            loader={<AnimatedLogo key={0} theme="inline" />}
          >
            <Row>
              {isGettingFirstMembers && (
                <div className="text-center" style={{ width: "100%" }}>
                  <AnimatedLogo key={0} theme="" />
                </div>
              )}
              {membersList.length === 0 ? <NoRecordFound /> : membersList}
            </Row>
          </InfiniteScroll>
        </div>
      )}
    </OrganizationLayout>
  );
};

export default OrganizationMembersWrapper;
