import React, { Component } from "react";
import { connect } from "react-redux";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBan } from "@fortawesome/pro-solid-svg-icons";
import {
  makeCaptain,
  removeCaptain,
  kickMember,
  banMember,
  unBanMember
} from "../../../../../store/actions/organizationActions";
import ConfirmAlert from "../../../../ConfirmAlert/ConfirmAlert";
import Style from "./MemberAction.module.scss";
import EFRectangleButton from "../../../../Buttons/EFRectangleButton/EFRectangleButton";

class MemberAction extends Component {
  makeCaptain = () => {
    const data = { userId: this.props.userId };
    this.props.makeCaptain(data, this.props.organization._id);
  };

  removeCaptain = () => {
    const data = { userId: this.props.userId };
    this.props.removeCaptain(data, this.props.organization._id);
  };

  kickOutMember = () => {
    this.props.kickMember(this.props.userId, this.props.organization._id);
  };

  banMember = () => {
    const data = { userId: this.props.userId };
    this.props.banMember(data, this.props.organization._id);
  };

  unBanMember = () => {
    const data = { userId: this.props.userId };
    this.props.unBanMember(data, this.props.organization._id);
  };

  render() {
    const { organization, userId, member, firstName, currentUser } = this.props;
    const captain = organization.captains && organization.captains.includes(userId);
    const isOwnerCard = currentUser && currentUser.id === member.user._id;
    const disable = false;
    const noContent = "";
    const content = (
      <>
        {!member.isBanned && (
          <>
            <ConfirmAlert
              title={captain ? "Remove as Captain" : "Add as a Captain"}
              message={`${
                captain
                  ? "Are you sure you want to remove"
                  : "Captains will be able to view and interact with the applicants screen on behalf of your organization. Are you sure you want to add"
              } ${firstName} as captain`}
              onYes={() => (captain ? this.removeCaptain() : this.makeCaptain())}
            >
              <EFRectangleButton text={captain ? "Remove as Captain" : "Make Captain"} />
            </ConfirmAlert>
            <ConfirmAlert
              title="Kick Member"
              message={`Are you sure you want to kick out ${firstName} out of the organization?`}
              onYes={() => this.kickOutMember()}
            >
              <EFRectangleButton text="Kick Out" />
            </ConfirmAlert>
          </>
        )}
        <ConfirmAlert
          title={member.isBanned ? "Unban Member" : "Ban Member"}
          message={`Are you sure you want to ${member.isBanned ? "unban" : "ban"} this user`}
          onYes={() => (!member.isBanned ? this.banMember() : this.unBanMember())}
        >
          <div className={Style.ban}>
            <FontAwesomeIcon icon={faBan} className={Style.banIcon} />
            {member.isBanned ? "Unban" : "Ban"}
          </div>
        </ConfirmAlert>
      </>
    );
    return <>{isOwnerCard ? noContent : content}</>;
  }
}

export default connect(null, {
  makeCaptain,
  removeCaptain,
  kickMember,
  banMember,
  unBanMember
})(MemberAction);
