import { Col } from "react-bootstrap";
import React from "react";
import Style from "./Member.module.scss";
import EFAvatar from "../../../EFAvatar/EFAvatar";
import { rescueNil, getImage, formatRoles } from "../../../../helpers/GeneralHelper";
import MemberAction from "./MemberAction/MemberAction";
import UserLink from "../../../UserLink/UserLink";

class Member extends React.PureComponent {
  render() {
    const { user, member, organization, currentUser, follower } = this.props;
    let isCaptainRole = false;
    let isOwner = false;
    let isCaptain = false;
    let isAdminCard = false;
    if (!follower) {
      isCaptainRole = organization.captains && organization.captains.includes(currentUser.id);
      isOwner = organization.user === currentUser.id;
      isCaptain = organization.captains && organization.captains.includes(user._id);
      isAdminCard = organization.user === member.user._id;
    }
    const profilePicture = rescueNil(user, "profilePicture", {});
    return (
      <Col md={3} sm={4}>
        <div className={Style.friendWrapper}>
          <EFAvatar
            displayOnlineButton={false}
            profilePicture={getImage(profilePicture, "avatar")}
            size="large"
            href={`/u/${user?.username}`}
          />
          <h5 className={Style.name}>
            <UserLink user={user} />{" "}
          </h5>
          <p className={Style.roles}>{formatRoles(user)}</p>
          {!follower && (isOwner || isCaptainRole) && !isAdminCard && (
            <MemberAction
              background="funBlue"
              userId={user._id}
              firstName={user.name}
              organization={organization}
              member={member}
              currentUser={currentUser}
            />
          )}
        </div>
      </Col>
    );
  }
}

export default Member;
