import React from "react";
import InfiniteScroll from "react-infinite-scroller";
import { Col, Row } from "react-bootstrap";
import { faUser } from "@fortawesome/pro-light-svg-icons";

import MainHeadingBar from "../MainHeadingBar/MainHeadingBar";
import TableHead from "../Table/TableHead/TableHead";
import AnimatedLogo from "../../AnimatedLogo";
import NoRecordFound from "../../NoRecordFound/NoRecordFound";
import TableItemMember from "../Table/TableItem/TableItemMember";

const OrganizationMembers = ({
  members,
  loadMore,
  organizationSlug,
  authUser,
  hasNextPage,
  isGettingFirstMembers,
  isAdmin,
  owner,
  captains,
  hasChildOrgs,
  organizationId,
  t
}) => {
  const membersList = members.map((member, index) => {
    return (
      <Col lg={12} md={12} key={index}>
        <TableItemMember
          member={member}
          currentUser={authUser}
          slug={organizationSlug}
          isAdmin={isAdmin}
          owner={owner}
          captains={captains}
          hasChildOrgs={hasChildOrgs}
          organizationId={organizationId}
        />
      </Col>
    );
  });

  return (
    <>
      <div className="container pb-2">
        <MainHeadingBar
          heading="Members"
          icon={faUser}
          t={t}
          organization={organizationSlug}
          isAdmin={isAdmin}
          hasChildOrgs={hasChildOrgs}
        />
        {hasChildOrgs && (
          <TableHead column1="name" column2="organization" column2Size="3" column3="title" column3Size="2" />
        )}
        <InfiniteScroll
          pageStart={1}
          loadMore={loadMore}
          hasMore={hasNextPage}
          loader={<AnimatedLogo key={0} theme="inline" />}
        >
          <Row>
            {isGettingFirstMembers && (
              <div className="text-center" style={{ width: "100%" }}>
                <AnimatedLogo key={0} theme="" />
              </div>
            )}
            {membersList.length === 0 ? <NoRecordFound /> : membersList}
          </Row>
        </InfiniteScroll>
      </div>
    </>
  );
};
export default OrganizationMembers;
