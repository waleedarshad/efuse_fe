import InfiniteScroll from "react-infinite-scroller";
import { mount } from "enzyme";
import React from "react";
import MembersList from "./MembersList";
import NoRecordFound from "../../../NoRecordFound/NoRecordFound";
import Member from "../Member/Member";

describe("MembersList", () => {
  const pagination = {
    hasMore: false
  };
  const loadMore = jest.fn();
  const organization = { _id: "98765" };
  const currentUser = { _id: "999999" };
  const follower = {};

  it("renders and infinite scroll with no record found if no members", () => {
    const orgMembers = [];

    const subject = mount(
      <MembersList
        members={orgMembers}
        pagination={pagination}
        loadMore={loadMore}
        organization={organization}
        currentUser={currentUser}
        follower={follower}
      />
    );

    expect(subject.find(InfiniteScroll)).toHaveLength(1);
    expect(subject.find(NoRecordFound)).toHaveLength(1);
  });

  it("renders an infinite scroll with the correct members", () => {
    const members = [
      {
        id: "1234567",
        isBanned: false,
        organization: { _id: "5db9a4401654c1153bd951f9" },
        user: { _id: "22222222" }
      },
      {
        id: "2345678",
        isBanned: false,
        organization: { _id: "5db9a4401654c1153bd951f9" },
        user: { _id: "33333333" }
      },
      {
        id: "3456789",
        isBanned: false,
        organization: { _id: "5db9a4401654c1153bd951f9" },
        user: { _id: "44444444" }
      }
    ];

    const subject = mount(
      <MembersList
        members={members}
        pagination={pagination}
        loadMore={loadMore}
        organization={organization}
        currentUser={currentUser}
        follower={follower}
      />
    );

    expect(subject.find(Member)).toHaveLength(3);
    expect(
      subject
        .find(Member)
        .at(0)
        .props()
    ).toEqual({
      user: { _id: "22222222" },
      member: {
        id: "1234567",
        isBanned: false,
        organization: { _id: "5db9a4401654c1153bd951f9" },
        user: { _id: "22222222" }
      },
      organization,
      currentUser,
      follower: {}
    });
  });
});
