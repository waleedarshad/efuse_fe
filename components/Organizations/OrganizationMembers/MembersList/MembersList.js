import React from "react";
import InfiniteScroll from "react-infinite-scroller";
import { Row } from "react-bootstrap";
import NoRecordFound from "../../../NoRecordFound/NoRecordFound";
import AnimatedLogo from "../../../AnimatedLogo";
import Member from "../Member/Member";

const MembersList = ({ members, pagination, loadMore, organization, currentUser, follower, t }) => {
  const content = members.map((member, index) => (
    <Member
      user={member.user || member.follower}
      key={index}
      member={member}
      organization={organization}
      currentUser={currentUser}
      follower={follower}
    />
  ));

  const hasMore = pagination && pagination.hasNextPage;

  return (
    <InfiniteScroll
      pageStart={1}
      loadMore={loadMore}
      hasMore={hasMore}
      loader={<AnimatedLogo key={0} theme="inline" />}
    >
      <Row>{members.length === 0 ? <NoRecordFound /> : content}</Row>
    </InfiniteScroll>
  );
};
export default MembersList;
