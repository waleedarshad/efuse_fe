import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import Style from "./MainHeadingBar.module.scss";

const MainHeadingBar = ({ heading, icon }) => {
  return (
    <div className={Style.mainHeading}>
      <div className={Style.headingSection}>
        <FontAwesomeIcon className={Style.icon} icon={icon} />
        <h4 className={Style.heading}>{heading}</h4>
      </div>
    </div>
  );
};

export default MainHeadingBar;
