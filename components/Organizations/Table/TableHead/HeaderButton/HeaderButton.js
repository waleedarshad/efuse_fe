import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronDown } from "@fortawesome/pro-solid-svg-icons";
import Style from "./HeaderButton.module.scss";

const HeaderButton = ({ btnText }) => {
  return (
    <button className={Style.transparentBtn} type="button">
      <span className="text-uppercase">{btnText}</span>
      <FontAwesomeIcon className={Style.btnIcon} icon={faChevronDown} />
    </button>
  );
};
export default HeaderButton;
