import React from "react";

import HeaderButton from "./HeaderButton/HeaderButton";
import Style from "./TableHead.module.scss";

const TableHead = ({ column1, column2, column2Size, column3, column3Size }) => {
  return (
    <>
      <div className={`row ${Style.tableHead}`}>
        <div className="col-md-3">
          <HeaderButton btnText={column1} />
        </div>
        <div className={`col-md-${column2Size}`}>
          <HeaderButton btnText={column2} />
        </div>
        <div className={`col-md-${column3Size}`}>
          <HeaderButton btnText={column3} />
        </div>
      </div>
    </>
  );
};
export default TableHead;
