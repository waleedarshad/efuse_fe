import React from "react";
import getConfig from "next/config";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faShareSquare, faEnvelope } from "@fortawesome/pro-regular-svg-icons";

import EFRectangleButtonTooltip from "../../../Buttons/EFRectangleButtonTooltip/EFRectangleButtonTooltip";
import ShareLinkButton from "../../../ShareLinkButton";
import { rescueNil, getImage } from "../../../../helpers/GeneralHelper";
import EFAvatar from "../../../EFAvatar/EFAvatar";
import TableItemCard from "./TableItemCard/TableItemCard";
import EFuseLink from "./EfuseLink/EfuseLink";
import OrganizationsDropdownButton from "./DropdownButton/OrganizationsDropdownButton";
import Style from "./TableItem.module.scss";

const { publicRuntimeConfig } = getConfig();

//TODO: Wire up OrganizationsDropdown button to work for orgs instead of institutions

const TableItemOrganization = ({ organization, currentUser, institutionSlug }) => {
  const { feBaseUrl } = publicRuntimeConfig;
  const profilePicture = rescueNil(organization, "profileImage");
  const organizationMemberInfo = `${organization?.captains ? organization?.captains.length : "No"} Captains, ${
    organization?.membersCount ? organization.membersCount : "No"
  } Members`;

  return (
    <TableItemCard>
      <div className="col-md-3">
        <div className={Style.userData}>
          <div className={Style.userPicture}>
            <div className={Style.avatar}>
              <EFAvatar
                displayOnlineButton={false}
                profilePicture={getImage(profilePicture, "profileImage")}
                size="small"
              />
            </div>
          </div>
          <span className={Style.name}>{organization?.name}</span>
        </div>
      </div>
      <div className="col-md-2">
        <p className={Style.organizationName}>{organization?.status || "Not set"} </p>
      </div>
      <div className="col-md-3">
        <p className={Style.organizationName}>{organizationMemberInfo}</p>
      </div>
      <div className={`col-md-4 ${Style.iconsSection}`}>
        <EFuseLink
          href={`/organizations/show?id=${organization?._id}`}
          tooltipText="View Profile"
          tooltipPlacement="top"
        />
        <ShareLinkButton
          userId={currentUser?.id}
          icon={faShareSquare}
          title={`Share ${organization?.name}`}
          url={`${feBaseUrl}/organizations/show?id=${organization?._id}`}
          childComponent={
            <div className={Style.messageButtonContainer}>
              <EFRectangleButtonTooltip
                theme="light"
                text={<FontAwesomeIcon icon={faShareSquare} className={Style.icon} />}
                shadowTheme="none"
                tooltipPlacement="top"
                tooltipText="Share"
                colorTheme="transparent"
                size="large"
              />
            </div>
          }
        />
        <div className={Style.shareButtonContainer}>
          <EFRectangleButtonTooltip
            theme="light"
            text={<FontAwesomeIcon icon={faEnvelope} className={Style.icon} />}
            shadowTheme="none"
            tooltipPlacement="top"
            tooltipText="Email Owner"
            colorTheme="transparent"
            externalHref={`mailto:${organization?.user?.email}`}
            size="large"
          />
        </div>
        <div className={Style.shareButtonContainer}>
          {/*<OrganizationsDropdownButton institutionSlug={institutionSlug} organizationId={organization?._id} />*/}
        </div>
      </div>
    </TableItemCard>
  );
};
export default TableItemOrganization;
