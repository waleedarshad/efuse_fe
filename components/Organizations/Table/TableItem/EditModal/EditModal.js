import React, { useState } from "react";
import { Col, Form } from "react-bootstrap";
import { useDispatch } from "react-redux";
import InputRow from "../../../../InputRow/InputRow";
import EFRectangleButton from "../../../../Buttons/EFRectangleButton/EFRectangleButton";
import AccessRoles from "../../../../../static/data/AccessRoles.json";
import InputField from "../../../../InputField/InputField";
import {
  makeCaptain,
  removeCaptain,
  updateOrganizationMemberTitle
} from "../../../../../store/actions/organizationActions";
import EFCardSelection from "../../../../EFCardSelection/EFCardSelection";

const EditModal = ({ editableValue, field, memberId, closeModal, organizationId, userId }) => {
  const dispatch = useDispatch();

  const [inputValue, setInputValue] = useState(editableValue);
  const [validated, setValidated] = useState(false);

  const updateOrganizationMemberRole = () => {
    inputValue === "Captain"
      ? dispatch(makeCaptain({ userId }, organizationId))
      : dispatch(removeCaptain({ userId }, organizationId));
  };

  const onSubmit = event => {
    event.preventDefault();
    setValidated(true);

    if (inputValue) {
      field === "title"
        ? dispatch(updateOrganizationMemberTitle(organizationId, memberId, { [field]: inputValue }))
        : updateOrganizationMemberRole();
    }

    closeModal();
  };

  return (
    <Form onSubmit={e => onSubmit(e)} noValidate validated={validated}>
      <InputRow>
        <Col>
          {field === "title" ? (
            <InputField
              name="title"
              validated={validated}
              errorMessage="Title is required"
              value={inputValue}
              onChange={e => setInputValue(e.target.value)}
              required
            />
          ) : (
            <>
              <EFCardSelection
                cards={AccessRoles.organizationMemberRoles}
                onChange={role => setInputValue(role)}
                value={inputValue}
                valueKey="name"
                size="large"
              />
            </>
          )}
        </Col>
      </InputRow>
      <InputRow>
        <Col>
          <EFRectangleButton colorTheme="primary" text={`Save ${field}`} buttonType="submit" />
        </Col>
      </InputRow>
    </Form>
  );
};
export default EditModal;
