import React from "react";
import TableItemCard from "./TableItemCard/TableItemCard";
import Style from "./TableItem.module.scss";
import TableItemMemberInfo from "./TableItemUserInfo/TableItemMemberInfo";
import TableItemIcons from "./TableItemIcons/TableItemIcons";

const TableItemMember = ({ member, currentUser, slug, isAdmin, owner, captains, hasChildOrgs, organizationId }) => {
  // TODO: Determine if new field is needed for member for related orgs
  const organizations = member?.relatedOrganizations?.map((organization, index) => {
    return (
      <p key={index} className={Style.organizationName}>
        {organization.name}
        {addCommaToEnd(index)}
      </p>
    );
  });

  const getRelatedOrganizations = () => {
    if (hasChildOrgs) {
      member?.relatedOrganizations?.length > 0 ? (
        organizations
      ) : (
        <p className={Style.organizationName}>No related organizations</p>
      );
    }
    return "";
  };

  const addCommaToEnd = index => {
    return member?.relatedOrganizations?.length - 1 !== index ? ", " : "";
  };

  const getMemberTitle = () => {
    return hasChildOrgs ? member?.title || "N/A" : "";
  };

  return (
    <TableItemCard theme={member?.isRevoked && "greyBackground"}>
      <TableItemMemberInfo member={member} owner={owner} captains={captains} hasChildOrgs={hasChildOrgs} />
      <div className="col-md-3">{getRelatedOrganizations()}</div>
      <div className="col-md-2">
        <p className={Style.organizationName}>{getMemberTitle()}</p>
      </div>
      <TableItemIcons
        member={member}
        currentUser={currentUser}
        slug={slug}
        isAdmin={isAdmin}
        organizationId={organizationId}
        hasChildOrgs={hasChildOrgs}
        owner={owner}
        captains={captains}
      />
    </TableItemCard>
  );
};

export default TableItemMember;
