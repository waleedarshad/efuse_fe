import React from "react";
import { useDispatch } from "react-redux";
import { faEllipsisH } from "@fortawesome/pro-regular-svg-icons";

import EFDropdown from "../../../../EFDropdown/EFDropdown";
import EditModal from "../EditModal/EditModal";
import Modal from "../../../../Modal/Modal";
import { kickMember } from "../../../../../store/actions/organizationActions";
import { getMemberRole } from "../../../../../helpers/OrganizationHelper";

const MembersDropdownButton = ({ member, organizationSlug, organizationId, hasChildOrgs, owner, captains }) => {
  const dispatch = useDispatch();

  const userId = member?.user?._id;

  const editTitle = {
    text: (
      <Modal
        displayCloseButton
        openOnLoad={false}
        component={
          <EditModal
            editableValue={member?.title}
            field="title"
            memberId={member?._id}
            organizationId={organizationId}
            userId={userId}
          />
        }
        textAlignCenter={false}
        title="Edit Member Title"
        hideOverflow
      >
        Edit Title
      </Modal>
    )
  };

  const menuItems = [
    {
      text: "Revoke Access",
      onClick: () => {
        dispatch(kickMember(userId, organizationId));
      }
    },
    {
      text: (
        <Modal
          displayCloseButton
          openOnLoad={false}
          component={
            <EditModal
              editableValue={getMemberRole(userId, owner, captains)}
              field="role"
              institutionSlug={organizationSlug}
              memberId={member?._id}
              organizationId={organizationId}
              userId={userId}
            />
          }
          textAlignCenter={false}
          title="Edit Member Role"
          hideOverflow
        >
          Edit Role
        </Modal>
      )
    }
  ];

  if (hasChildOrgs) {
    menuItems.push(editTitle);
  }

  return <EFDropdown items={menuItems} icon={faEllipsisH} />;
};

export default MembersDropdownButton;
