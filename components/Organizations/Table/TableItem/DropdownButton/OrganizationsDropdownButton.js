import React from "react";
import { faEllipsisV } from "@fortawesome/pro-solid-svg-icons";
import EFDropdown from "../../../../EFDropdown/EFDropdown";

// eslint-disable-next-line no-unused-vars
const OrganizationsDropdownButton = ({ organizationId, institutionSlug }) => {
  return (
    <EFDropdown
      items={[
        {
          text: "Unlink Organization",
          onClick: () => {
            // TODO: Wire up OrganizationsDropdown button to work for orgs instead of institutions
            // dispatch(unlinkOrganization(institutionSlug, organizationId));
          }
        }
      ]}
      icon={faEllipsisV}
    />
  );
};

export default OrganizationsDropdownButton;
