import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEnvelope, faShareSquare, faShare } from "@fortawesome/pro-regular-svg-icons";
import Router from "next/router";
import React from "react";
import getConfig from "next/config";
import Style from "../TableItem.module.scss";

import ShareLinkButton from "../../../../ShareLinkButton";
import EFRectangleButtonTooltip from "../../../../Buttons/EFRectangleButtonTooltip/EFRectangleButtonTooltip";
import { startChatWith } from "../../../../../store/actions/messageActions";
import { getMemberRole } from "../../../../../helpers/OrganizationHelper";
import MembersDropdownButton from "../DropdownButton/MembersDropdownButton";

const TableItemIcons = ({ member, currentUser, slug, isAdmin, organizationId, hasChildOrgs, owner, captains }) => {
  const { publicRuntimeConfig } = getConfig();
  const { feBaseUrl } = publicRuntimeConfig;
  const memberRole = getMemberRole(member?.user?._id, owner, captains);
  return (
    <div className={`col-md-4 ${Style.iconsSection}`}>
      <ShareLinkButton
        userId={currentUser?.id}
        title={`Share ${member?.user?.name}'s portfolio`}
        url={`${feBaseUrl}/u/${member?.user?.username}`}
        icon={faShareSquare}
        childComponent={
          <div className={Style.messageButtonContainer}>
            <EFRectangleButtonTooltip
              theme="light"
              text={<FontAwesomeIcon icon={faShare} className={Style.icon} />}
              shadowTheme="none"
              tooltipPlacement="top"
              tooltipText="Share"
              colorTheme="transparent"
              size="large"
            />
          </div>
        }
      />

      <div className={Style.messageButtonContainer}>
        <EFRectangleButtonTooltip
          theme="light"
          text={<FontAwesomeIcon icon={faEnvelope} className={Style.icon} />}
          shadowTheme="none"
          tooltipPlacement="top"
          tooltipText="Message"
          colorTheme="transparent"
          size="large"
          onClick={() => {
            startChatWith(member?.user?._id, member?.user?.name);
            Router.push("/messages");
          }}
        />
      </div>
      {memberRole !== "Owner" && isAdmin && (
        <div className={Style.messageButtonContainer}>
          <MembersDropdownButton
            member={member}
            organizationSlug={slug}
            organizationId={organizationId}
            hasChildOrgs={hasChildOrgs}
            owner={owner}
            captains={captains}
          />
        </div>
      )}
    </div>
  );
};

export default TableItemIcons;
