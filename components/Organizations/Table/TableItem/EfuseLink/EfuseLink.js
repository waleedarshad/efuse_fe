import React from "react";
import Link from "next/link";

import EFTooltip from "../../../../tooltip/EFTooltip/EFTooltip";
import EFBomb from "../../../../Icons/EFBomb/EFBomb";
import Style from "./EfuseLink.module.scss";

const EfuseLink = ({ tooltipText, tooltipPlacement, tooltipDelay, href }) => {
  return (
    <div style={{ display: "inline-block" }}>
      <EFTooltip tooltipPlacement={tooltipPlacement} delay={tooltipDelay} tooltipContent={tooltipText}>
        <div>
          <Link href={href} as={href}>
            <button className={Style.button} type="button">
              <EFBomb size="small" darkBomb />
            </button>
          </Link>
        </div>
      </EFTooltip>
    </div>
  );
};
export default EfuseLink;
