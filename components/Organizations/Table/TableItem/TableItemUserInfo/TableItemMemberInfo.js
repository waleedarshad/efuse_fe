import React from "react";
import Style from "../TableItem.module.scss";
import EFAvatar from "../../../../EFAvatar/EFAvatar";
import { rescueNil, getImage } from "../../../../../helpers/GeneralHelper";
import EFTooltip from "../../../../tooltip/EFTooltip/EFTooltip";
import { getMemberRole } from "../../../../../helpers/OrganizationHelper";

const TableItemMemberInfo = ({ member, owner, captains, hasChildOrgs }) => {
  const profilePicture = rescueNil(member?.user, "profilePicture");

  const toolTipText = {
    member: "No access to institution portal.",
    admin:
      "Partial access to institution portal. Permission to modify any organization. Permission to add, remove or modify any member or admin roles. Unable to add, remove or modify owner users.",
    owner: "Full access to institution portal. Permission to modify any organization or member.",
    default: ""
  };

  const memberRole = getMemberRole(member?.user?._id, owner, captains);

  const returnTooltipText = () => {
    return memberRole.toLowerCase() in toolTipText ? toolTipText[memberRole.toLowerCase()] : toolTipText.default;
  };

  const memberText = <p className={Style.role}>Role | {memberRole}</p>;

  return (
    <div className="col-md-3">
      <div className={Style.userData}>
        <div className={Style.userPicture}>
          <div className={Style.avatar}>
            <EFAvatar
              displayOnlineButton={false}
              profilePicture={getImage(profilePicture, "avatar")}
              size="small"
              href={`/u/${member?.user?.username}`}
            />
          </div>
        </div>
        <div className={Style.userInfo}>
          <p className="m-0 ">
            <span className={Style.username}>@{member?.user?.username}</span>
          </p>
          {hasChildOrgs ? (
            <EFTooltip tooltipContent={returnTooltipText()} tooltipPlacement="right">
              {memberText}
            </EFTooltip>
          ) : (
            memberText
          )}
        </div>
      </div>
    </div>
  );
};

export default TableItemMemberInfo;
