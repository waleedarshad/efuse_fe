import React from "react";

import EFCard from "../../../../Cards/EFCard/EFCard";
import Style from "./TableItemCard.module.scss";

const TableItemCard = ({ children, theme }) => {
  return (
    <div className={Style.listItemWrapper}>
      <EFCard widthTheme="fullWidth" overflowTheme="visible" shadow="none">
        <div className={`row ${Style.listItem} ${Style[theme]}`}>{children}</div>
      </EFCard>
    </div>
  );
};

export default TableItemCard;
