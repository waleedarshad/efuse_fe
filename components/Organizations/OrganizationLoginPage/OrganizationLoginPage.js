import React, { useEffect } from "react";
import { useRouter } from "next/router";
import DynamicModal from "../../DynamicModal/DynamicModal";
import { toggleLoader } from "../../../store/actions/loaderActions";
import OrganizationDetail from "../OrganizationDetail/OrganizationDetail";
import { isUserLoggedIn } from "../../../helpers/AuthHelper";
import Error from "../../Error/Error";

const OrganizationLoginPage = ({ pageProps }) => {
  const router = useRouter();
  const mainOrganizationPage = `/org/${router.query.org}`;

  const userIsLoggedIn = isUserLoggedIn();

  if (pageProps && (!pageProps.ssrStatusCode || pageProps.ssrStatusCode === 422 || pageProps.ssrStatusCode === 400)) {
    return <Error statusCode={pageProps.ssrStatusCode} />;
  }

  useEffect(() => {
    toggleLoader(false);
    analytics.page("Organization Branded Login");
    if (userIsLoggedIn) {
      router.push(mainOrganizationPage);
    }
  }, []);

  return (
    <>
      <OrganizationDetail />
      {!userIsLoggedIn && (
        <DynamicModal
          flow="LoginSignup"
          openOnLoad
          organization={pageProps?.organization}
          startView="login"
          displayCloseButton
          allowBackgroundClickClose={false}
          onCloseModal={() => {
            router.push(mainOrganizationPage);
          }}
          customRedirect={mainOrganizationPage}
        />
      )}
    </>
  );
};

export default OrganizationLoginPage;
