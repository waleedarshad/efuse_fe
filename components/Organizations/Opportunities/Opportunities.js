import React, { Component } from "react";
import { connect } from "react-redux";
import { Col, Row } from "react-bootstrap";
import InfiniteScroll from "react-infinite-scroller";
import isEmpty from "lodash/isEmpty";

import OrganizationLayout from "../../Layouts/OrganizationLayout/OrganizationLayout";
import { getImage, rescueNil } from "../../../helpers/GeneralHelper";
import { getOwnerAndCaptain } from "../../../helpers/OrganizationHelper";
import OrganizationBioCard from "../../OrganizationBioCard";
import { clearOpportunities, getOpportunitiesNew, setCurrentPage } from "../../../store/actions/opportunityActions";
import { apply } from "../../../store/actions/applicantActions";
import Error from "../../../pages/_error";
import AnimatedLogo from "../../AnimatedLogo";
import NoRecordFound from "../../NoRecordFound/NoRecordFound";
import OpportunitySmallListItem from "../../Opportunities/OpportunityWrapper/OpportunitySmallListing/OpportunitySmallListItem/OpportunitySmallListItem";
import Style from "./Opportunities.module.scss";
import EmptyComponent from "../../EmptyComponent/EmptyComponent";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";

let page = 1;
class Opportunities extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fetchRecords: true
    };
  }

  componentDidMount() {
    analytics.page("Organizations Opportunities");

    const {
      getOpportunitiesNew: getOpportunitiesNewAction,
      clearOpportunities: clearOpportunitiesAction,
      query,
      setCurrentPage: setCurrentPageAction,
      view
    } = this.props;
    const { id } = query;
    if (id && this.state.fetchRecords) {
      clearOpportunitiesAction();
      this.setState({ fetchRecords: false });
      getOpportunitiesNewAction(page, {}, "organizations/opportunities", id);
      setCurrentPageAction(view);
    }
  }

  componentDidUpdate() {
    const { getOpportunitiesNew: getOpportunitiesNewAction, setCurrentPage: setCurrentPageAction, view } = this.props;
    const { id } = this.props.query;
    if (id && this.state.fetchRecords) {
      // eslint-disable-next-line react/no-did-update-set-state
      this.setState({ fetchRecords: false });
      getOpportunitiesNewAction(page, {}, "organizations/opportunities", id);
      setCurrentPageAction(view);
    }
  }

  loadMorePages = () => {
    const { id } = this.props.query;
    page += 1;
    this.props.getOpportunitiesNew(page, {}, "organizations/opportunities", id);
  };

  render() {
    const {
      organization,
      currentUser,
      query,
      isWindowView,
      responseCode,
      opportunities,
      displayExternalSourceOpportunity,
      webview,
      apply: applyAction,
      pagination,
      hasNextPage
    } = this.props;
    const { owner, captain } = getOwnerAndCaptain(currentUser, organization);
    const admin = owner || captain;
    if (responseCode === 400 || responseCode === 422) {
      return <Error statusCode={responseCode} />;
    }
    const opportunitiesList = opportunities.map((opportunity, index) => {
      return (
        <Col lg={12} md={12} className={Style.smallOppContainer} key={index}>
          <OpportunitySmallListItem
            opportunity={opportunity}
            apply={applyAction}
            colLg={12}
            colMd={12}
            webview={webview}
            display_external_source_opportunity={displayExternalSourceOpportunity}
          />
          <hr className={Style.hr} />
        </Col>
      );
    });
    return (
      <OrganizationLayout
        isWindowView={isWindowView}
        headerImage={getImage(organization.headerImage, "organization")}
        avatar={rescueNil(organization, "profileImage", {}) || {}}
        bioCard={<OrganizationBioCard />}
        bar="Organizations"
        query={query}
        owner={admin}
        orgType={organization.status}
      >
        {admin && (
          <div className={Style.createButton}>
            <EFRectangleButton colorTheme="secondary" text="Create Opportunity" internalHref="/opportunities/create" />
          </div>
        )}
        {opportunities.length ? (
          <div className={Style.cardWrapper}>
            <InfiniteScroll
              pageStart={1}
              loadMore={this.loadMorePages}
              hasMore={hasNextPage}
              loader={<AnimatedLogo key={0} theme="inline" />}
            >
              <Row>
                {isEmpty(pagination) && (
                  <div className="text-center" style={{ width: "100%" }}>
                    <AnimatedLogo key={0} theme="" />
                  </div>
                )}
                {opportunitiesList.length === 0 ? <NoRecordFound /> : opportunitiesList}
              </Row>
            </InfiniteScroll>
          </div>
        ) : (
          <EmptyComponent text={`${organization.name} has not created any opportunities on eFuse.`} />
        )}
      </OrganizationLayout>
    );
  }
}

const mapStateToProps = state => ({
  organization: state.organizations.organization,
  currentUser: state.auth.currentUser,
  isWindowView: state.messages.isWindowView,
  pagination: state.opportunities.pagination,
  hasNextPage: state.opportunities.pagination.hasNextPage,
  responseCode: state.errors.responseCode,
  opportunities: state.opportunities.opportunities,
  webview: state.webview.status,
  displayExternalSourceOpportunity: state.features.display_external_source_opportunity
});

export default connect(mapStateToProps, {
  getOpportunitiesNew,
  clearOpportunities,
  setCurrentPage,
  apply
})(Opportunities);
