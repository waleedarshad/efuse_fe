import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Row, Col } from "react-bootstrap";

import EFCard from "../../Cards/EFCard/EFCard";
import Error from "../../../pages/_error";
import EFExternalEfuseBadge from "../../EFExternalEfuseBadge/EFExternalEfuseBadge";

const Followers = ({ organization, responseCode }) => {
  useEffect(() => {
    analytics.page("Organizations Badges");
  }, []);

  if (responseCode === 400 || responseCode === 422) {
    return <Error statusCode={responseCode} />;
  }

  let imageUrl = "";
  let linkUrl = "";

  if (organization._id) {
    imageUrl = `https://cdn.efuse.gg/uploads/static/badges/efuse_${
      organization.verified.status ? "verified_" : ""
    }organization.png`;
    linkUrl = `https://efuse.gg/org/${organization.shortName}`;
  }

  return (
    <Row>
      <Col className="mb-3" md="12">
        <EFCard widthTheme="fullWidth">
          <div className="p-4">
            <EFExternalEfuseBadge
              id={organization._id}
              linkUrl={linkUrl}
              imageUrl={imageUrl}
              badgeType="organization"
              altText="eFuse Organization Portfolio Badge"
            />
          </div>
        </EFCard>
      </Col>
    </Row>
  );
};

const mapStateToProps = state => ({
  organization: state.organizations.organization,
  responseCode: state.errors.responseCode
});

export default connect(mapStateToProps, {})(Followers);
