import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

import OrganizationLayout from "../../Layouts/OrganizationLayout/OrganizationLayout";
import OrganizationBioCard from "../../OrganizationBioCard";
import { getImage, rescueNil } from "../../../helpers/GeneralHelper";
import { getOwnerAndCaptain } from "../../../helpers/OrganizationHelper";
import { getFollowers } from "../../../store/actions/organizationFollowerActions";
import MembersList from "../OrganizationMembers/MembersList/MembersList";
import EFOrganizationInviteSection from "../../EFOrganizationInviteSection/EFOrganizationInviteSection";
import Error from "../../../pages/_error";

const Followers = ({ query }) => {
  const dispatch = useDispatch();

  const organization = useSelector(state => state.organizations.organization);
  const currentUser = useSelector(state => state.auth.currentUser);
  const isWindowView = useSelector(state => state.messages.isWindowView);
  const followers = useSelector(state => state.organizations.followers);
  const responseCode = useSelector(state => state.errors.responseCode);
  const pagination = useSelector(state => state.organizations.pagination);

  useEffect(() => {
    analytics.page("Organizations Followers");
    dispatch(getFollowers(query.id));
  }, []);

  const loadMore = () => {
    const nextPage = pagination ? pagination.page + 1 : 1;
    dispatch(getFollowers(query.id, nextPage));
  };

  const { owner, captain } = getOwnerAndCaptain(currentUser, organization);

  if (responseCode === 400 || responseCode === 422) {
    return <Error statusCode={responseCode} />;
  }

  const followerInviteCodes =
    organization.inviteCodes &&
    organization.inviteCodes.length > 0 &&
    organization.inviteCodes.filter(code => code.type === "ORGANIZATION");

  const admin = owner || captain;

  return (
    <OrganizationLayout
      headerImage={getImage(organization.headerImage, "organization")}
      avatar={rescueNil(organization, "profileImage", {}) || {}}
      bioCard={<OrganizationBioCard />}
      bar="Organizations"
      query={query}
      isWindowView={isWindowView}
      owner={owner || captain}
      orgType={organization.status}
    >
      {followerInviteCodes && admin && (
        <>
          {followerInviteCodes.map((code, index) => {
            return (
              <EFOrganizationInviteSection
                inviteCodes={code.code}
                numberOfSignups={code.signupCount}
                titleText="Invite users to become a follower"
                bodyText="Use your custom invite url to invite your following. Anyone with this link will be directed to your
                  organization page to sign-up and follow your organization. Followers are individuals who would like
                  to stay up to date with the organization."
                buttonText="Invite Followers"
                buttonColorTheme="primary"
                key={index}
              />
            );
          })}
        </>
      )}
      <MembersList
        pagination={pagination}
        loadMore={loadMore}
        organization={organization}
        members={followers}
        currentUser={currentUser}
        follower
      />
    </OrganizationLayout>
  );
};

export default Followers;
