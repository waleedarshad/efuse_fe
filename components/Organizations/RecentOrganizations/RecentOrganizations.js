import React, { Component } from "react";
import { connect } from "react-redux";
import { ListGroup } from "react-bootstrap";
import AnimatedLogo from "../../AnimatedLogo";
import ListStyle from "../../List/List.module.scss";
import Style from "../../Opportunities/RecentOpportunities/RecentOpportunities.module.scss";
import { getRecentOrganizations } from "../../../store/actions/organizationActions";
import RecentItem from "../../RecentItem/RecentItem";
import { getImage } from "../../../helpers/GeneralHelper";
import { getOrganizationUrl } from "../../../helpers/UrlHelper";

class RecentOrganizations extends Component {
  componentDidMount() {
    const { recentOrganizations } = this.props;
    if (recentOrganizations.length === 0) this.props.getRecentOrganizations();
  }

  render() {
    const { recentOrganizations, contentInstances } = this.props;

    let displayContent = "";
    if (contentInstances.includes("recentOrganizations")) {
      displayContent = (
        <ListGroup.Item className={ListStyle.listItem}>
          <AnimatedLogo key={0} theme="content" />
        </ListGroup.Item>
      );
    } else if (recentOrganizations.length === 0) {
      displayContent = <ListGroup.Item className={ListStyle.listItem}>Not Organizations Found</ListGroup.Item>;
    } else {
      displayContent = recentOrganizations.map((organization, index) => (
        <RecentItem
          parentStyleSheet={ListStyle}
          key={index}
          id={organization._id}
          name={organization.name}
          type="organizations"
          avatar={getImage(organization.profileImage, "avatar")}
          itemType={organization.organizationType}
          shortNameUrl={getOrganizationUrl(organization)}
        />
      ));
    }
    return (
      <ListGroup>
        <ListGroup.Item className={`${ListStyle.listItem} ${Style.listHeader}`}>Organizations to Follow</ListGroup.Item>
        {displayContent}
      </ListGroup>
    );
  }
}

const mapStateToProps = state => ({
  recentOrganizations: state.organizations.recentOrganizations,
  contentInstances: state.loader.contentInstances
});

export default connect(mapStateToProps, { getRecentOrganizations })(RecentOrganizations);
