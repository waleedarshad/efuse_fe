import React, { useEffect } from "react";
import Router, { useRouter } from "next/router";
import { useSelector } from "react-redux";
import { Col, Row } from "react-bootstrap";
import { faBuilding } from "@fortawesome/pro-light-svg-icons";

import OrganizationLayout from "../../Layouts/OrganizationLayout/OrganizationLayout";
import { getImage, rescueNil } from "../../../helpers/GeneralHelper";
import { getOwnerAndCaptain } from "../../../helpers/OrganizationHelper";
import OrganizationBioCard from "../../OrganizationBioCard";
import MainHeadingBar from "../MainHeadingBar/MainHeadingBar";
import TableHead from "../Table/TableHead/TableHead";
import TableItemOrganization from "../Table/TableItem/TableItemOrganization";

const ChildOrganizationsWrapper = ({ pageProps }) => {
  const router = useRouter();
  const { query } = router;
  const organizationSlug = query.org;

  const isWindowView = useSelector(state => state.messages.isWindowView);
  const authUser = useSelector(state => state.auth.currentUser);

  const { owner, captain } = getOwnerAndCaptain(authUser, pageProps.organization);

  const organizationDoesNotHaveChildren =
    !pageProps.organization?.children || pageProps.organization?.children?.length === 0;

  const organizationsList = pageProps.organization?.children.map((childOrganization, index) => {
    return (
      <Col lg={12} md={12} key={index}>
        <TableItemOrganization
          organization={childOrganization}
          currentUser={authUser}
          institutionSlug={organizationSlug}
        />
      </Col>
    );
  });

  useEffect(() => {
    if (organizationDoesNotHaveChildren) {
      Router.push(`/org/${organizationSlug}`);
    }
  }, []);

  return (
    <OrganizationLayout
      headerImage={getImage(pageProps.organization?.headerImage, "organization")}
      owner={owner || captain}
      avatar={rescueNil(pageProps.organization, "profileImage", {}) || {}}
      bioCard={<OrganizationBioCard />}
      bar="Organizations"
      query={query}
      isWindowView={isWindowView}
      orgType={pageProps.organization?.status}
      organizationFromSSR={pageProps.organization}
    >
      <div className="container pb-2">
        <MainHeadingBar heading="Organizations" icon={faBuilding} institution={organizationSlug} />
        <TableHead column1="name" column2="status" column2Size="2" column3="organization members" column3Size="3" />
        <Row>{organizationsList}</Row>
      </div>
    </OrganizationLayout>
  );
};

export default ChildOrganizationsWrapper;
