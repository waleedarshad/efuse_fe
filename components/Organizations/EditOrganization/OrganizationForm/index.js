import { Col, FormControl } from "react-bootstrap";
import { useSelector } from "react-redux";
import CharacterCounter from "react-character-counter";
import Form from "react-bootstrap/Form";

import InputRow from "../../../InputRow/InputRow";
import InputField from "../../../InputField/InputField";
import InputLabel from "../../../InputLabel/InputLabel";
import InputLabelWithHelpTooltip from "../../../InputLabelWithHelpTooltip/InputLabelWithHelpTooltip";
import MaskedInput from "../../../MaskedInput/MaskedInput";
import { rescueNil } from "../../../../helpers/GeneralHelper";
import dropdowns from "./data/dropdowns.json";
import GoogleAutoComplete from "../../../GoogleAutocomplete/GoogleAutocomplete";
import ImageUploader from "../../../ImageUploader/ImageUploader";
import EFCardSelection from "../../../EFCardSelection/EFCardSelection";
import EFSelectGamesField from "../../../EFSelectGamesField/EFSelectGamesField";

const OrganizationForm = props => {
  const {
    onChange,
    name,
    scale,
    status,
    location,
    phoneNumber,
    website,
    organizationType,
    description,
    headerImage,
    profileImage,
    onDrop,
    nameErrorMessage,
    onCropChange,
    toggleCropperModal,
    customErrors,
    saveGames,
    games,
    validateShortName
  } = props;

  const selectGamesFlag = useSelector(state => state.features.temp_web_select_games_orgs);

  return (
    <>
      <InputRow>
        <Col>
          <InputLabel theme="darkColor">Organization Name</InputLabel>
          <CharacterCounter value={name} maxLength={70}>
            <InputField
              name="name"
              placeholder={"ex: The Ohio State University"}
              size="lg"
              theme="whiteShadow"
              onChange={onChange}
              required
              errorMessage={nameErrorMessage}
              value={name}
              maxLength={70}
              onBlur={validateShortName}
            />
          </CharacterCounter>
        </Col>
      </InputRow>
      <InputRow>
        <Col>
          <InputLabelWithHelpTooltip
            labelText={`What best describes ${name || "your organization"}?`}
            tooltipText="Select a category that will allows users to find your organization on eFuse."
          />
          <EFCardSelection
            cards={dropdowns.organizationType}
            onChange={value => onChange({ target: { name: "organizationType", value } })}
            value={organizationType}
            size="small"
          />
          {customErrors.organizationType && (
            <FormControl.Feedback type="invalid" style={{ display: "block" }}>
              {customErrors.organizationType}
            </FormControl.Feedback>
          )}
        </Col>
      </InputRow>

      <InputRow>
        <Col>
          <InputLabelWithHelpTooltip
            labelText={`Who can join ${name || "this organization"}?`}
            tooltipText="Decide who should be called members of your organization. You can change this later."
          />
          <EFCardSelection
            cards={dropdowns.status}
            onChange={value => onChange({ target: { name: "status", value } })}
            value={status}
            size="small"
          />
          {customErrors.status && (
            <FormControl.Feedback type="invalid" style={{ display: "block" }}>
              {customErrors.status}
            </FormControl.Feedback>
          )}
        </Col>
      </InputRow>

      <InputRow>
        <Col>
          <InputLabel theme="darkColor">Profile Image / Logo</InputLabel>
          <ImageUploader
            text="Profile Image"
            name="profileImage"
            onDrop={onDrop}
            value={rescueNil(profileImage, "url")}
            theme="lightButton"
            onCropChange={onCropChange}
            toggleCropperModal={toggleCropperModal}
            aspectRatioWidth={300}
            aspectRatioHeight={300}
            showCropper
            showImageDescription
          />
          {customErrors.profileImage && (
            <FormControl.Feedback type="invalid" style={{ display: "block" }}>
              {customErrors.profileImage}
            </FormControl.Feedback>
          )}
        </Col>
      </InputRow>

      <InputRow>
        <Col>
          <InputLabelWithHelpTooltip
            labelText="Page Header Background Image"
            tooltipText="This is a large image shown at the top of your organization page. Use this to show off your team, arena, branding, etc."
          />
          <ImageUploader
            text="Header Image"
            name="headerImage"
            onDrop={onDrop}
            value={rescueNil(headerImage, "url")}
            theme="lightButton"
            onCropChange={onCropChange}
            toggleCropperModal={toggleCropperModal}
            aspectRatioWidth={1800}
            aspectRatioHeight={500}
            showCropper
            showImageDescription
          />
          {customErrors.headerImage && (
            <FormControl.Feedback type="invalid" style={{ display: "block" }}>
              {customErrors.headerImage}
            </FormControl.Feedback>
          )}
        </Col>
      </InputRow>

      <InputRow>
        <Col>
          <InputLabelWithHelpTooltip
            labelText="Short Organization Bio"
            tooltipText="In 1-2 sentences describing your organization. Be meaningful and brief in your description."
          />
          <CharacterCounter value={description} maxLength={160}>
            <InputField
              helperText="In 1-2 sentences describing your organization. Be meaningful and brief in your description."
              as="textarea"
              name="description"
              rows={3}
              maxLength={160}
              theme="whiteShadow"
              value={description}
              onChange={onChange}
              placeholder="ex: College esports team in Columbus, OH. Go Bucks!"
              required
              errorMessage="Organization bio is required"
            />
          </CharacterCounter>
        </Col>
      </InputRow>

      <hr />

      {selectGamesFlag && (
        <>
          <InputLabelWithHelpTooltip
            labelText="Select Organization Games"
            tooltipText="These games will be displayed on your organization page."
            optional
          />
          <EFSelectGamesField onChange={saveGames} value={games} selectMultiple />
        </>
      )}

      <InputRow>
        <Col>
          <InputLabelWithHelpTooltip
            optional
            labelText={`How many people are in ${name || "your organization"}?`}
            tooltipText="Just an estimate. This helps eFuse gamers understand the size of your organization."
          />
          <EFCardSelection
            cards={dropdowns.scale}
            onChange={value => onChange({ target: { name: "scale", value } })}
            value={scale}
            size="small"
          />
        </Col>
      </InputRow>

      <InputRow>
        <Col>
          <InputLabelWithHelpTooltip
            labelText="Location"
            optional
            tooltipText="If your organization is based in a specific city, add it here. This will be shown on your organization page to all users."
          />
          <Form.Text className="text-muted" style={{ paddingBottom: "5px" }}>
            Is {name || "your organization"} based in a specific city? Enter that city name below, otherwise leave
            blank.
          </Form.Text>
          <GoogleAutoComplete
            onSuggest={props.onSuggestSelect}
            placeholder={"ex: Columbus, OH"}
            className="geoselect-internal"
            initialValue={location}
          />
        </Col>
      </InputRow>

      <InputRow>
        <Col>
          <InputLabelWithHelpTooltip
            labelText="Website"
            optional
            tooltipText="Link your organization page to your existing external website. Users can easily visit this website from eFuse. Please include https://"
          />
          <Form.Text className="text-muted" style={{ paddingBottom: "5px" }}>
            If you have a website for {name || "your organization"}, enter it below.
          </Form.Text>
          <InputField
            name="website"
            placeholder={"ex: https://osu.edu"}
            size="lg"
            theme="whiteShadow"
            onChange={onChange}
            value={website}
            pattern="https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)"
            errorMessage="Invalid website URL"
          />
        </Col>
      </InputRow>

      <InputRow>
        <Col>
          <InputLabelWithHelpTooltip
            labelText="Phone Number"
            tooltipText="Only used for communicatation with eFuse."
            optional
          />
          <Form.Text className="text-muted" style={{ paddingBottom: "5px" }}>
            Only used for communicatation with eFuse.
          </Form.Text>
          <MaskedInput
            mask={[
              "+",
              /[1-9]/,
              " ",
              "(",
              /[1-9]/,
              /\d/,
              /\d/,
              ")",
              "-",
              /\d/,
              /\d/,
              /\d/,
              "-",
              /\d/,
              /\d/,
              /\d/,
              /\d/,
              /\d/,
              /\d/,
              /\d/
            ]}
            size="lg"
            name="phoneNumber"
            type="tel"
            value={phoneNumber}
            onChange={props.onChange}
            placeholder={"ex: +1 (000)-000-0000"}
            theme="whiteShadow"
            errorMessage="Phone Number is required"
          />
        </Col>
      </InputRow>
    </>
  );
};

export default OrganizationForm;
