import React, { Component } from "react";
import { connect } from "react-redux";
import Router, { withRouter } from "next/router";
import PropTypes from "prop-types";
import { Card, Form, Spinner } from "react-bootstrap";
import { faBinoculars } from "@fortawesome/pro-solid-svg-icons";

import { toggleLoader } from "../../../store/actions/loaderActions";
import OrganizationForm from "./OrganizationForm";
import { isEmptyObject } from "../../../helpers/GeneralHelper";
import Internal from "../../Layouts/Internal/Internal";
import EFSubHeader from "../../Layouts/Internal/EFSubHeader/EFSubHeader";
import { getDiscoverNavigationList } from "../../../navigation/discover";
import EFCard from "../../Cards/EFCard/EFCard";
import {
  getOrganizationToEdit,
  updateOrganization,
  clearOrganization
} from "../../../store/actions/organizationActions";
import { dispatchNotification } from "../../../store/actions/notificationActions";
import { toggleCropModal } from "../../../store/actions/settingsActions";
import Style from "./index.module.scss";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";

class EditOrganization extends Component {
  constructor(props) {
    super(props);
    this.state = {
      validated: false,
      headerImageCropped: null,
      organization: {
        name: "",
        scale: "1-99",
        email: "",
        location: "",
        phoneNumber: "",
        website: "",
        organizationType: "Brand",
        status: "Open",
        description: "",
        shortName: "",
        profileImage: {},
        headerImage: {},
        toBeVerified: false,
        games: []
      },
      nameErrorMessage: "Name is required",
      phoneRequired: false,
      allowEdit: false,
      customErrors: {},
      isUrlValid: true
    };
  }

  componentDidMount() {
    analytics.page("Edit Organization");
    this.props.toggleLoader(false);
    this.props.getOrganizationToEdit(this.props.router.query.organizationId, Router);
  }

  componentWillUnmount() {
    this.props.clearOrganization();
  }

  onCancel = () => {
    Router.push("/organizations");
  };

  static getDerivedStateFromProps(props, state) {
    if (!isEmptyObject(props.organization) && !state.allowEdit) {
      return {
        allowEdit: true,
        organization: { ...state.organization, ...props.organization }
      };
    }
    return {};
  }

  onChange = event => {
    const { name, value } = event.target;
    this.setState(prevState => ({
      organization: {
        ...prevState.organization,
        [name]: value
      },
      customErrors: {
        ...prevState.customErrors,
        [name]: null
      }
    }));
  };

  setNameErrorMessage = newMsg => {
    const { nameErrorMessage } = this.state;
    if (nameErrorMessage !== newMsg) {
      this.setState({ nameErrorMessage: newMsg });
    }
  };

  setEmailErrorMessage = newMsg => {
    const { emailErrorMessage } = this.state;
    if (emailErrorMessage !== newMsg) {
      this.setState({ emailErrorMessage: newMsg });
    }
  };

  onSuggestSelect = suggest => {
    this.setState(prevState => ({
      organization: {
        ...prevState.organization,
        location: suggest === undefined ? "" : suggest.label
      }
    }));
  };

  onDrop = (selectedFile, name) => {
    this.setState(prevState => ({
      ...prevState,
      organization: {
        ...prevState.organization,
        [name]: selectedFile[0]
      },
      customErrors: {
        ...prevState.customErrors,
        [name]: null
      }
    }));
  };

  onCropChange = (croppedImage, name) => {
    const { blob } = croppedImage;
    const croppedFile = new File([blob], blob.name, { type: blob.type });
    this.setState(prevState => ({
      organization: { ...prevState.organization, [name]: croppedFile }
    }));
  };

  toggleCropperModal = (val, name) => {
    this.props.toggleCropModal(val, name);
  };

  saveGames = games => {
    this.setState(prevState => ({
      ...prevState,
      organization: {
        ...prevState.organization,
        games
      }
    }));
  };

  onSubmit = event => {
    event.preventDefault();
    let valid = true;
    const mainFormValid = event.currentTarget.checkValidity();
    const customErrors = {};
    const { organization, isUrlValid } = this.state;
    const { updateOrganization: updateOrganizationProps, fromAdmin, router } = this.props;

    if (!this.validateImage("profileImage")) {
      customErrors.profileImage = "Profile image is required";
      valid = false;
    }

    if (!this.validateImage("headerImage")) {
      customErrors.headerImage = "Header image is required";
      valid = false;
    }

    if (!organization.organizationType) {
      customErrors.organizationType = "Type is required";
      valid = false;
    }

    if (!organization.status) {
      customErrors.status = "Join policy is required";
      valid = false;
    }

    if (!isUrlValid) {
      customErrors.invalidUrlMessage = "This URL already exists. Please use a different URL.";
      valid = false;
    }

    this.setState(prevState => ({
      ...prevState,
      validated: true,
      customErrors
    }));

    if (!this.props.loading && mainFormValid && valid) {
      const orgData = new FormData();
      Object.keys(organization).forEach(key => {
        if (key === "games") {
          orgData.append(key, JSON.stringify(organization[key]));
        } else if (!["verified", "captains"].includes(key)) {
          orgData.append(key, organization[key]);
        }
      });
      const existingOrganizationId = router.query.organizationId;
      updateOrganizationProps(existingOrganizationId, orgData, Router, fromAdmin);
    }
  };

  validateImage(name) {
    let isValid = false;

    // Check if image was uploaded (.name will be present) or image was already uploaded previously (.url will be present)
    if (this.state.organization[name].name || this.state.organization[name].url) {
      isValid = true;
    }
    return isValid;
  }

  render() {
    const { currentUser, router, loading } = this.props;
    const { validated, organization, nameErrorMessage, phoneRequired, isUrlValid } = this.state;

    return (
      <Internal metaTitle="Edit Organization | eFuse.gg" containsSubheader>
        <EFSubHeader headerIcon={faBinoculars} navigationList={getDiscoverNavigationList(router.pathname)} />
        <div className={Style.formContainer}>
          <h1 className={Style.mainHeader}>Edit {organization.name}</h1>
          <p>Update your organization and save your changes below</p>
          <EFCard widthTheme="fullWidth" shadow="medium">
            <Card.Body>
              <Form noValidate validated={validated} role="form" onSubmit={this.onSubmit}>
                <OrganizationForm
                  validated={validated}
                  name={organization.name}
                  scale={organization.scale}
                  location={organization.location}
                  phoneNumber={organization.phoneNumber}
                  website={organization.website}
                  organizationType={organization.organizationType}
                  description={organization.description}
                  shortName={organization.shortName}
                  toBeVerified={organization.toBeVerified}
                  profileImage={organization.profileImage}
                  headerImage={organization.headerImage}
                  status={organization.status}
                  games={organization.games}
                  onChange={this.onChange}
                  onSuggestSelect={this.onSuggestSelect}
                  onDrop={this.onDrop}
                  nameErrorMessage={nameErrorMessage}
                  phoneRequired={phoneRequired}
                  onVerficationRequest={this.onVerficationRequest}
                  currentUser={currentUser}
                  captains={organization.captains}
                  verified={organization.verified}
                  onCropChange={this.onCropChange}
                  toggleCropperModal={this.toggleCropperModal}
                  customErrors={this.state.customErrors}
                  saveGames={this.saveGames}
                  validateShortName={this.validateShortName}
                  isUrlValid={isUrlValid}
                />
                <div className={Style.buttonContainer}>
                  <EFRectangleButton
                    buttonType="submit"
                    text={
                      <>
                        {loading && <Spinner className="mr-2" animation="border" variant="light" size="sm" />}
                        {loading ? " Submitting..." : "Save Changes"}
                      </>
                    }
                    width="fullWidth"
                    disabled={loading}
                    colorTheme="secondary"
                    shadowTheme="small"
                  />
                </div>
              </Form>
            </Card.Body>
          </EFCard>
        </div>
      </Internal>
    );
  }
}

const mapStateToProps = state => ({
  organization: state.organizations.organization,
  currentUser: state.auth.currentUser,
  loading: state.organizations.loading
});

EditOrganization.propTypes = {
  fromAdmin: PropTypes.bool
};

EditOrganization.defaultProps = {
  fromAdmin: false
};

export default connect(mapStateToProps, {
  toggleLoader,
  getOrganizationToEdit,
  updateOrganization,
  clearOrganization,
  dispatchNotification,
  toggleCropModal
})(withRouter(EditOrganization));
