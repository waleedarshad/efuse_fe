/* eslint-disable react/jsx-props-no-spreading */
import { useState } from "react";
import Form from "react-bootstrap/Form";
import FormControl, { FormControlProps } from "react-bootstrap/FormControl";
import { InputGroup } from "react-bootstrap";

import Style from "./InputField.module.scss";

interface InputFieldProps extends FormControlProps {
  errorMessage?: string;
  theme?: "external" | "borderless" | "internal" | "whiteShadow" | "success";
  prepend?: string;
  helperText?: string;
  onBlur?: (event: any) => void;
  name?: string;
  placeholder?: string;
  maxLength?: number;
  step?: number;
  min?: number;
  disabled?: boolean;
  defaultChecked?: boolean;
}

const InputField = ({
  errorMessage,
  theme = "external",
  prepend,
  helperText,
  onBlur,
  name,
  placeholder,
  maxLength,
  step,
  min,
  disabled = false,
  defaultChecked = false,
  className,
  ...other
}: InputFieldProps) => {
  const [focus, setFocus] = useState(false);
  const mainContent = (
    <>
      <Form.Control
        {...other}
        name={name}
        disabled={disabled}
        defaultChecked={defaultChecked}
        placeholder={placeholder}
        maxLength={maxLength}
        min={min}
        step={step}
        className={`${Style.inputField} ${Style[theme]} ${className}`}
        onBlur={e => {
          if (onBlur) onBlur(e);
          setFocus(false);
        }}
        onFocus={() => setFocus(true)}
      />
      {focus && helperText && <Form.Text className="text-muted">{helperText}</Form.Text>}
      {errorMessage && <FormControl.Feedback type="invalid">{errorMessage}</FormControl.Feedback>}
    </>
  );

  if (prepend) {
    return (
      <InputGroup>
        <InputGroup.Prepend>
          <InputGroup.Text className={`${Style.inputTextPrepend}`}>{prepend}</InputGroup.Text>
        </InputGroup.Prepend>
        {mainContent}
      </InputGroup>
    );
  }
  return mainContent;
};

export default InputField;
