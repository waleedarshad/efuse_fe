import React from "react";
import EFFilterChip from "./EFFilterChip";

export default {
  title: "Inputs/EFFilterChip",
  component: EFFilterChip,
  argTypes: {
    isChecked: {
      control: {
        type: "boolean"
      }
    }
  }
};

// eslint-disable-next-line react/jsx-props-no-spreading
const Story = args => <EFFilterChip {...args} />;

export const Checked = Story.bind({});

Checked.args = {
  isChecked: true,
  text: "EFFilterChip"
};

export const Unchecked = Story.bind({});

Unchecked.args = {
  isChecked: false,
  text: "EFFilterChip"
};
