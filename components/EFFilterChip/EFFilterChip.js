import React from "react";

import Style from "./EFFilterChip.module.scss";

const EFFilterChip = ({ text, isChecked, id, name, onChange }) => {
  return (
    <label htmlFor={id}>
      <input className={Style.input} id={id} type="checkbox" name={name} checked={isChecked} onChange={onChange} />
      <span className={`${Style.chip} ${isChecked && Style.checked}`}>{text}</span>
    </label>
  );
};

EFFilterChip.defaultProps = {
  isChecked: false
};

export default EFFilterChip;
