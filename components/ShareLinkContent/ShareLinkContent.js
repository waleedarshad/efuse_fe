import { createRef, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCopy } from "@fortawesome/pro-solid-svg-icons";
import { faFacebook, faTwitter, faLinkedin } from "@fortawesome/free-brands-svg-icons";
import Style from "./ShareLinkContent.module.scss";
import { sendNotification } from "../../helpers/FlashHelper";
import { SHARE_LINK_EVENTS } from "../../common/analyticEvents";
import EFRectangleButton from "../Buttons/EFRectangleButton/EFRectangleButton";

export const SHARE_TYPES = {
  LINKEDIN: "linkedin",
  FACEBOOK: "facebook",
  TWITTER: "twitter"
};

const ShareLinkContent = ({ link, onShareButtonClick, shareLinkKind }) => {
  const [linkCopiedToClipboard, setLinkCopiedToClipboard] = useState(false);

  // Reference the text input so we can copy to clipboard
  const linkInput = createRef();

  const copyLink = () => {
    analytics.track(SHARE_LINK_EVENTS.SHARE_TO_CLIPBOARD, {
      kind: shareLinkKind,
      link
    });

    linkInput.current.select();
    document.execCommand("copy");
    setLinkCopiedToClipboard(true);

    sendNotification("Link copied!", "success", "Success");
  };

  const shareViaFacebook = () => {
    analytics.track(SHARE_LINK_EVENTS.SHARE_TO_FACEBOOK, {
      kind: shareLinkKind,
      link
    });
    if (onShareButtonClick) {
      onShareButtonClick(SHARE_TYPES.FACEBOOK);
    }

    FB.ui(
      {
        method: "share",
        href: link
      },
      response => {
        // eslint-disable-next-line no-console
        console.log(JSON.stringify(response, null, 2));
      }
    );
  };

  const shareViaTwitter = () => {
    analytics.track(SHARE_LINK_EVENTS.SHARE_TO_TWITTER, {
      kind: shareLinkKind,
      link
    });
    if (onShareButtonClick) {
      onShareButtonClick(SHARE_TYPES.TWITTER);
    }

    window.open(`https://twitter.com/share?url=${link}`, "share-link");
  };

  const shareViaLinkedIn = () => {
    analytics.track(SHARE_LINK_EVENTS.SHARE_TO_LINKEDIN, {
      kind: shareLinkKind,
      link
    });
    if (onShareButtonClick) {
      onShareButtonClick(SHARE_TYPES.LINKEDIN);
    }

    window.open(`http://www.linkedin.com/shareArticle?url=${link}`, "share-link");
  };

  return (
    <div>
      <div className={Style.modalContainer}>
        <div className={Style.inputContainer}>
          <input value={link} ref={linkInput} onClick={copyLink} readOnly />
          <FontAwesomeIcon icon={faCopy} className={Style.copyButton} onClick={copyLink} />
          {linkCopiedToClipboard && <div className={Style.linkCopied}>Link copied!</div>}
        </div>

        <EFRectangleButton
          className={Style.shareButton}
          text="Share on Facebook"
          icon={faFacebook}
          onClick={() => shareViaFacebook()}
        />
        <EFRectangleButton
          className={Style.shareButton}
          text="Share on Twitter"
          icon={faTwitter}
          onClick={() => shareViaTwitter()}
        />
        <EFRectangleButton
          className={Style.shareButton}
          text="Share on Linkedin"
          icon={faLinkedin}
          onClick={() => shareViaLinkedIn()}
        />
      </div>
    </div>
  );
};

export default ShareLinkContent;
