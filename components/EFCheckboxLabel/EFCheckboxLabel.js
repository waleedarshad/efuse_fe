import React, { useState } from "react";
import EFCheckbox from "../EFCheckbox/EFCheckbox";
import Style from "./EFCheckboxLabel.module.scss";

const EFCheckboxLabel = ({ isChecked, onChecked, onRemoveChecked, label, subLabel }) => {
  const [checked, setChecked] = useState(isChecked);

  const checkboxClicked = () => {
    if (onChecked && !checked) onChecked();
    if (onRemoveChecked && checked) onRemoveChecked();
    setChecked(!checked);
  };

  return (
    <label className={Style.checkboxWrapper}>
      <div onClick={checkboxClicked}>
        <EFCheckbox isChecked={checked} />
      </div>
      <div className={Style.textWrapper}>
        <p className={Style.label}>{label}</p>
        {subLabel && <p className={Style.subLabel}>{subLabel}</p>}
      </div>
    </label>
  );
};

EFCheckboxLabel.defaultProps = {
  isChecked: false
};

export default EFCheckboxLabel;
