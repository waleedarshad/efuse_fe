import React from "react";
import EFCheckboxLabel from "./EFCheckboxLabel";

export default {
  title: "Inputs/EFCheckboxLabel",
  component: EFCheckboxLabel,
  argTypes: {
    isChecked: {
      control: {
        type: "boolean"
      }
    },
    label: {
      control: {
        type: "text"
      }
    },
    subLabel: {
      control: {
        type: "text"
      }
    }
  }
};

const Story = args => <EFCheckboxLabel {...args} />;

export const Checked = Story.bind({});

Checked.args = {
  isChecked: true,
  label: "This is a label"
};

export const Unchecked = Story.bind({});

Unchecked.args = {
  isChecked: false,
  label: "This is a label"
};

export const SubLabel = Story.bind({});

SubLabel.args = {
  isChecked: true,
  label: "This is a Checkbox",
  subLabel: "This is a sub label"
};
