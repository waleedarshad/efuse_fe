import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import Slide from "react-reveal/Slide";
import Fade from "react-reveal/Fade";
import { NextSeo } from "next-seo";
import { faCouch, faBuilding, faGamepadAlt, faUser } from "@fortawesome/pro-solid-svg-icons";

import Style from "./WelcomeSplashPage.module.scss";
import FeatureCard from "./FeatureCard/FeatureCard";
import EFMobileAppScreen from "../EFMobileAppScreen/EFMobileAppScreen";
import DesktopOnboarding from "../DesktopOnboarding/DesktopOnboarding";

const WelcomeSplashPage = () => {
  const currentUser = useSelector(state => state.auth.currentUser);

  useEffect(() => {
    analytics.page("Welcome Splash Screen");
  }, []);

  const [ifMobile] = useState(typeof window !== "undefined" ? window.matchMedia("(max-width: 520px)").matches : false);

  const cardContent = [
    {
      title: "Lounge",
      description: "The home for your esports community. All gaming, all the time.",
      link: "/lounge/featured",
      buttonText: "Network with Gamers",
      icon: faCouch
    },
    {
      title: "Organizations",
      description: "Centralize and organize your esports team or organization.",
      link: "/organizations",
      buttonText: "Find an Organization",
      icon: faBuilding
    },
    {
      title: "Opportunities",
      description: "Esports jobs, team openings, scholarships, and events at your finger tips.",
      link: "/opportunities",
      buttonText: "Apply to Opportunities",
      icon: faGamepadAlt
    },
    {
      title: "Portfolio",
      description: "Chart your gaming journey. Connect game stats, social accounts, and create a gaming resume.",
      link: `/u/${currentUser?.username}`,
      buttonText: "Build your Portfolio",
      icon: faUser
    }
  ];

  return (
    <div>
      <NextSeo title="Welcome to eFuse" />
      {/* New Onboarding Flow */}
      <div>{!ifMobile && <DesktopOnboarding />}</div>
      {/* Force mobile app download for mobile web users */}
      <div>{ifMobile && <EFMobileAppScreen />}</div>
      <div className={Style.backgroundImage}>
        <div className={Style.textContainer}>
          <Fade delay={1000}>
            <h2 className={Style.welcomeText}>
              <span className={Style.nameText}>{currentUser?.name ? `${currentUser?.name}, ` : ""}</span>Welcome to
              efuse
            </h2>
          </Fade>
          <h1 className={Style.eFuseHeaderText}>
            <Slide top>All Gaming, All the Time</Slide>
          </h1>
          <Fade delay={1000}>
            <h3 className={Style.subheaderText}>eFuse is the platform for gamers. Your esports journey starts here.</h3>
          </Fade>
        </div>
        <div className={Style.cardContainer}>
          {cardContent.map((card, index) => (
            <FeatureCard
              index={index}
              title={card.title}
              description={card.description}
              link={card.link}
              buttonText={card.buttonText}
              icon={card.icon}
              key={card.title}
            />
          ))}
        </div>
      </div>
    </div>
  );
};

export default WelcomeSplashPage;
