import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Fade from "react-reveal/Fade";
import Style from "./FeatureCard.module.scss";
import EFCard from "../../Cards/EFCard/EFCard";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";
import useWindowDimensions from "../../../hooks/useWindowDimensions";

const FeatureCard = ({ title, description, link, buttonText, index, icon }) => {
  const { width } = useWindowDimensions();

  return (
    <div className={Style.singleCardContainer}>
      {/* On mobile devices, Fade animation will have no delay */}
      <Fade bottom delay={width < 767 ? 0 : index * 100}>
        <EFCard widthTheme="fullWidth">
          <div className={Style.insideCardContainer}>
            <FontAwesomeIcon className={Style.icon} icon={icon} />

            <div className={Style.title}>{title}</div>
            <div className={Style.description}>{description}</div>
            <EFRectangleButton
              internalHref={link}
              buttonType="button"
              colorTheme="secondary"
              text={buttonText}
              width="fullWidth"
              onClick={() => {
                analytics.track("WELCOME_CARD_BUTTON_CLICK", {
                  buttonLink: link,
                  buttonLabel: buttonText
                });
              }}
            />
          </div>
        </EFCard>
      </Fade>
    </div>
  );
};

export default FeatureCard;
