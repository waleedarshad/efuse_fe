import React from "react";
import { Form, Col } from "react-bootstrap";
import InputRow from "../../../InputRow/InputRow";
import Style from "../GeneralErenaTemplate/GeneralErenaTemplate.module.scss";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";
import EFSelectGamesModalControl from "../../../FormControls/EFSelectGamesModalControl";
import EFSelectControl from "../../../FormControls/EFSelectControl";
import Switch from "../../../Switch/Switch";

const TournamentErenaTemplate = ({
  onSubmit,
  form: {
    control,
    watch,
    handleSubmit,
    formState: { errors }
  },
  showRequirementsView,
  setShowRequirementsView
}) => {
  const bracketType = watch("bracketType");

  const tournamentTypes = [
    { label: "Select Type", value: "" },
    { label: "Leaderboard", value: "pointRace" },
    { label: "Bracket", value: "bracket" }
  ];

  const teamOptions = [
    { label: "Select Number", value: 0 },
    { label: "4 Teams", value: 4 },
    { label: "8 Teams", value: 8 },
    { label: "16 Teams", value: 16 },
    { label: "32 Teams", value: 32 }
  ];

  return (
    <Form className={Style.leftAlign}>
      <InputRow>
        <Col sm={12}>
          <EFSelectGamesModalControl
            name="gameSelection"
            label="Game"
            control={control}
            errors={errors}
            selectMultiple={false}
            required
          />
        </Col>
      </InputRow>

      <InputRow>
        <Col sm={12}>
          <EFSelectControl
            label="Tournament Type"
            control={control}
            errors={errors}
            required
            name="bracketType"
            options={tournamentTypes}
          />
        </Col>
      </InputRow>

      {bracketType === "bracket" && (
        <InputRow>
          <Col sm={12}>
            <EFSelectControl
              label="Number of Teams"
              name="totalTeams"
              control={control}
              errors={errors}
              options={teamOptions}
              required
            />
          </Col>
        </InputRow>
      )}

      <InputRow>
        <Col>
          <div className={Style.requirementSwitchContainer}>
            <span>Do you want people to apply to this event?</span>
            <Switch
              name="showRequirementsView"
              checked={showRequirementsView}
              value={showRequirementsView}
              onChange={() => setShowRequirementsView(!showRequirementsView)}
            />
          </div>
        </Col>
      </InputRow>

      <div className={Style.alignRight}>
        <EFRectangleButton
          colorTheme="primary"
          text={showRequirementsView ? "Next Page" : "Create eRena Event"}
          onClick={handleSubmit(onSubmit)}
        />
      </div>
    </Form>
  );
};

export default TournamentErenaTemplate;
