import React from "react";
import { Col, Form } from "react-bootstrap";
import InputRow from "../../../InputRow/InputRow";
import Style from "./GeneralErenaTemplate.module.scss";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";
import EFSelectControl from "../../../FormControls/EFSelectControl";
import EFInputControl from "../../../FormControls/EFInputControl";
import EFHtmlInputControl from "../../../FormControls/EFHtmlInputControl";
import EFCustomURLControl from "../../../FormControls/EFCustomURLControl";
import EFDatetimeInputControl from "../../../FormControls/EFDatetimeInputControl";
import statuses from "../../tournamentStatus.json";

const GeneralErenaTemplate = ({
  currentUser,
  currentUserOrgs,
  onSubmit,
  form: {
    control,
    watch,
    handleSubmit,
    formState: { errors }
  }
}) => {
  const [tournamentName, startDatetime] = watch(["tournamentName", "startDatetime"]);

  const creatorOptions = [
    { label: "Create event as...", value: "" },
    ...currentUserOrgs.map(org => ({ value: org._id, label: org.name })),
    ...(currentUser?.id ? [{ label: currentUser.name, value: currentUser.id }] : [])
  ];

  return (
    <Form className={Style.leftAlign}>
      <InputRow>
        <Col sm={12}>
          <EFSelectControl
            errors={errors}
            label="Owner"
            name="owner"
            control={control}
            options={creatorOptions}
            required
          />
        </Col>
      </InputRow>
      <InputRow>
        <Col sm={12}>
          <EFInputControl
            label="Event Name"
            name="tournamentName"
            control={control}
            placeholder="Add an event name"
            maxLength={50}
            required
            errors={errors}
          />
        </Col>
      </InputRow>
      <InputRow>
        <Col sm={12}>
          <EFHtmlInputControl
            label="Description"
            name="tournamentDescription"
            control={control}
            errors={errors}
            required
          />
        </Col>
      </InputRow>
      <InputRow>
        <Col sm={12}>
          <EFCustomURLControl
            pathType="e"
            slugType="erenatournaments"
            control={control}
            name="slug"
            longName={tournamentName}
            errors={errors}
            required
          />
        </Col>
      </InputRow>
      <InputRow>
        <Col sm={6}>
          <EFDatetimeInputControl
            label="Start Date & Time of Event"
            name="startDatetime"
            minDate="today"
            errors={errors}
            control={control}
            required
          />
        </Col>
        <Col sm={6}>
          <EFDatetimeInputControl
            label="End Date & Time of Event"
            instructions="End date is not required."
            name="endDatetime"
            minDate={startDatetime}
            errors={errors}
            control={control}
          />
        </Col>
      </InputRow>
      <InputRow>
        <Col sm={12}>
          <EFSelectControl label="Status" name="status" control={control} options={statuses} errors={errors} />
        </Col>
      </InputRow>

      <div className={Style.buttonContainer}>
        <EFRectangleButton colorTheme="primary" text="Next Page" onClick={handleSubmit(onSubmit)} />
      </div>
    </Form>
  );
};

export default GeneralErenaTemplate;
