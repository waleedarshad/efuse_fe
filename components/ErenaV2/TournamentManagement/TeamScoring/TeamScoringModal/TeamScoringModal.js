import React, { useState, useEffect } from "react";
import { Form } from "react-bootstrap";

import { useMutation } from "@apollo/client";
import Style from "../TeamScoring.module.scss";
import { sendNotification } from "../../../../../helpers/FlashHelper";
import EFRectangleButton from "../../../../Buttons/EFRectangleButton/EFRectangleButton";
import InputLabel from "../../../../InputLabel/InputLabel";
import InputField from "../../../../InputField/InputField";
import { ADD_ERENA_SCORE, UPDATE_ERENA_SCORE } from "../../../../../graphql/eRena/eRenaScore";
import { addScoreToCache, updateScoreInCache } from "../../TournamentManagementCacheActions/TeamScoringCacheActions";

const TeamScoringModal = ({ tournament, tournamentId, team, closeModal }) => {
  const erenaScores = tournament?.scores || [];
  const [stateScores, setScores] = useState([]);

  const [createScore] = useMutation(ADD_ERENA_SCORE, {
    update: addScoreToCache
  });

  const [updateScore] = useMutation(UPDATE_ERENA_SCORE, {
    update: updateScoreInCache
  });

  useEffect(() => {
    initializeStateScores();
  }, [erenaScores, team?.players]);

  // recommending we investigate a better way to handle this, currently we are grabbing the player and checking if a score exists
  const initializeStateScores = () => {
    const foundPlayerScores = team?.players.map(player => {
      const foundScore = erenaScores.find(eScore => eScore.ownerId === player._id && eScore.erenaKind === "pointRace");

      if (foundScore) {
        // add the score id to the player object if the score is found
        return { ...player, score: foundScore.score, scoreId: foundScore._id };
      }

      return { ...player };
    });

    const newScores = foundPlayerScores || [];

    setScores(newScores);
  };

  // when a player value is changed, update the score in state
  const onPlayerChange = (event, player) => {
    const newScores = [...stateScores];
    const playerScoreIndex = stateScores.findIndex(x => x._id === player._id);
    // set the value to value as number and to event.target.value if there is no value
    newScores[playerScoreIndex].score = event.target.valueAsNumber || event.target.value;
    setScores(newScores);
  };

  // find the score for the player id
  const findScore = player => {
    let returnScore = 0;
    const foundPlayer = stateScores.findIndex(x => x._id === player._id);
    // check if the user has removed the score, if so allow the score to be set to ""
    if (stateScores[foundPlayer]?.score || stateScores[foundPlayer]?.score === "") {
      returnScore = stateScores[foundPlayer].score;
    }
    return returnScore;
  };

  // loop through the player scores and call the proper function for each score
  const onSubmit = e => {
    e.preventDefault();
    stateScores.forEach(player => {
      // update the score if it exists, create a score if it doesn't
      if (player?.scoreId) {
        updatePlayerScore(player);
      } else {
        createPlayerScore(player);
      }
    });
    if (closeModal) closeModal();
    sendNotification("Successfully updated the player scores", "success", "Updating Scores");
  };

  const createPlayerScore = player => {
    // setting score to 0 in data if it doesn't exist
    const data = {
      ownerId: player._id,
      ownerKind: "player",
      score: Number(player.score || "0")
    };

    createScore({
      variables: {
        body: data,
        tournamentIdOrSlug: tournamentId
      }
    });
  };

  const updatePlayerScore = player => {
    // setting score to 0 in data if it doesn't exist
    const data = {
      score: Number(player.score || "0")
    };

    updateScore({
      variables: {
        id: player.scoreId,
        body: data
      }
    });
  };

  return (
    <Form onSubmit={onSubmit}>
      <div className={Style.playersWrapper}>
        {team?.players?.map(player => {
          return (
            <div key={player?._id}>
              <InputLabel theme="darkColor">{player?.name}</InputLabel>
              <InputField
                name="playerScore"
                theme="whiteShadow"
                size="lg"
                onChange={event => onPlayerChange(event, player)}
                value={findScore(player)}
                type="number"
              />
            </div>
          );
        })}
      </div>
      <div className={Style.buttonWrapper}>
        <EFRectangleButton colorTheme="primary" text="Save Scores" buttonType="submit" />
      </div>
    </Form>
  );
};

export default TeamScoringModal;
