import React, { useState } from "react";
import { faEdit } from "@fortawesome/pro-solid-svg-icons";

import Style from "./TeamScoring.module.scss";
import TeamScoringModal from "./TeamScoringModal/TeamScoringModal";
import Modal from "../../../Modal/Modal";
import EmptyComponent from "../../../EmptyComponent/EmptyComponent";
import EFCircleIconButtonTooltip from "../../../Buttons/EFCircleIconButtonTooltip/EFCircleIconButtonTooltip";

const TeamScoring = ({ tournament }) => {
  const erenaTeams = tournament?.teams || [];
  const erenaScores = tournament?.scores || [];
  const [forceCloseModal, setForceCloseModal] = useState(false);

  if (erenaTeams?.length === 0) {
    return <EmptyComponent text="You have not created any teams. Navigate to the Teams tab to begin adding teams." />;
  }

  // get individual player score
  const getPlayerScore = playerId => {
    const playerScore = erenaScores.find(x => x.ownerId === playerId);
    return playerScore?.score || 0;
  };

  // sum of player scores
  const addTeamScores = players => {
    let totalScore = 0;
    players?.forEach(player => {
      totalScore += getPlayerScore(player?._id);
    });
    return totalScore;
  };

  const closeModal = () => {
    setForceCloseModal(true);
  };

  return (
    <div className={Style.teamsWrapper}>
      {erenaTeams &&
        erenaTeams.map(team => {
          return (
            <div className={Style.teamContainer} key={team?._id}>
              <div className={Style.teamNameWrapper}>
                <p className={Style.teamName}>{team?.name}</p>
                <Modal
                  displayCloseButton
                  allowBackgroundClickClose={false}
                  component={
                    <TeamScoringModal
                      tournament={tournament}
                      team={team}
                      tournamentId={tournament?._id}
                      closeModal={closeModal}
                    />
                  }
                  textAlignCenter={false}
                  customMaxWidth="800px"
                  title="Edit Scores"
                  forceClose={forceCloseModal}
                  onOpenModal={() => {
                    setForceCloseModal(false);
                  }}
                >
                  <EFCircleIconButtonTooltip
                    tooltipContent="Edit Scores"
                    icon={faEdit}
                    size="small"
                    shadowTheme="small"
                  />
                </Modal>

                <p className={Style.teamScore}>
                  Total Score: <b>{addTeamScores(team?.players)}</b>
                </p>
              </div>

              {team?.players?.map(player => {
                return (
                  <div className={Style.playerContainer} key={player?._id}>
                    <span className={Style.playerName}>{player?.name}</span>
                    <span className={Style.playerScore}>: {getPlayerScore(player?._id)}</span>
                  </div>
                );
              })}
            </div>
          );
        })}
    </div>
  );
};

export default TeamScoring;
