import React, { useEffect, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus } from "@fortawesome/pro-solid-svg-icons";
import { useDispatch, useSelector } from "react-redux";
import { useMutation } from "@apollo/client";
import cloneDeep from "lodash/cloneDeep";
import Style from "./CreateTeams.module.scss";
import InputRow from "../../../InputRow/InputRow";

import EmptyComponent from "../../../EmptyComponent/EmptyComponent";
import CreateTeamItem from "./CreateTeamItem/CreateTeamItem";
import { getApplicants } from "../../../../store/actions/applicantActions";
import PageTitleDetails from "../../PageTitleDetails/PageTitleDetails";
import { ADD_ERENA_TEAMS, DELETE_ERENA_TEAMS, UPDATE_ERENA_TEAMS } from "../../../../graphql/eRena/eRenaTeam";
import { ADD_ERENA_PLAYER, DELETE_ERENA_PLAYER, UPDATE_ERENA_PLAYER } from "../../../../graphql/eRena/eRenaPlayer";
import {
  addTeamToCache,
  deleteTeamFromCache,
  updateTeamInCache
} from "../TournamentManagementCacheActions/TeamCacheActions";
import {
  addPlayerToCache,
  deletePlayerFromCache,
  updatePlayerInCache
} from "../TournamentManagementCacheActions/TeamPlayerCacheActions";

const CreateTeams = ({ tournament, isBracket }) => {
  const dispatch = useDispatch();

  // selectors
  const erenaTeams = tournament?.teams || [];
  const applicants = useSelector(state => state.applicants.applicants);

  // set states
  const [stateTeams, setTeams] = useState([]);

  const [createTeam] = useMutation(ADD_ERENA_TEAMS, {
    update: addTeamToCache
  });

  const [deleteTeam] = useMutation(DELETE_ERENA_TEAMS, {
    update: deleteTeamFromCache
  });

  const [updateTeam] = useMutation(UPDATE_ERENA_TEAMS, {
    update: updateTeamInCache
  });

  const [createPlayer] = useMutation(ADD_ERENA_PLAYER, {
    update: addPlayerToCache
  });

  const [deletePlayer] = useMutation(DELETE_ERENA_PLAYER, {
    update: deletePlayerFromCache
  });

  const [updatePlayer] = useMutation(UPDATE_ERENA_PLAYER, {
    update: updatePlayerInCache
  });

  useEffect(() => {
    setStats();
  }, [JSON.stringify(erenaTeams), tournament?.opportunity, tournament?._id, applicants?.length]);

  const setStats = () => {
    if (tournament?._id) {
      setTeams(erenaTeams);
    }
    if (tournament?.opportunity) {
      dispatch(getApplicants(tournament?.opportunity?._id || tournament?.opportunity, 1, true, {}, "accepted"));
    }
  };

  const findTeam = teamId => {
    return stateTeams.find(x => x._id === teamId);
  };

  const findTeamIndex = teamId => {
    return stateTeams.findIndex(x => x._id === teamId);
  };

  const findPlayerIndex = (teamIndex, playerId) => {
    const players = stateTeams[teamIndex]?.players;
    return players.findIndex(x => x._id === playerId);
  };

  // retrieves all ids of eFuse players already on a team
  const getEfusePlayers = () => {
    // get the players from the teams
    const currentPlayers = [].concat(...stateTeams.map(team => team?.players));

    // get just the player ids, and filter out nulls
    const eFusePlayers = currentPlayers.map(player => player?.user?._id).filter(id => id);

    return eFusePlayers;
  };

  const onTeamChange = (event, teamId) => {
    const newTeams = cloneDeep(stateTeams);
    let team = findTeam(teamId);
    const teamIndex = findTeamIndex(teamId);
    team = {
      ...team,
      name: event.target.value
    };
    newTeams[teamIndex] = team;
    setTeams(newTeams);
  };

  const onTeamBlur = (event, teamId) => {
    const data = {
      name: event.target.value,
      type: "EFUSE"
    };

    postTeamUpdates(teamId, data);
  };

  const postTeamUpdates = (teamId, data) => {
    updateTeam({
      variables: {
        id: teamId,
        body: data
      },
      optimisticResponse: {
        updateTeam: {
          __typename: "ERenaTeam",
          _id: teamId,
          ...data
        }
      }
    });
  };

  const onPlayerChange = (event, teamId, playerId) => {
    const team = findTeam(teamId);
    const teamIndex = findTeamIndex(teamId);
    const playerIndex = findPlayerIndex(teamIndex, playerId);

    let player = team.players[playerIndex];
    player = {
      ...player,
      name: event.target.value
    };
    const newTeams = cloneDeep(stateTeams);
    newTeams[teamIndex].players[playerIndex] = player;
    setTeams([...newTeams]);
  };

  const onPlayerBlur = (event, teamId, playerId) => {
    const data = {
      name: event.target.value
    };

    updatePlayer({
      variables: {
        id: playerId,
        body: data
      },
      optimisticResponse: {
        updatePlayer: {
          __typename: "ERenaPlayer",
          _id: playerId,
          team: teamId,
          ...data
        }
      }
    });
  };

  const createTeamAction = () => {
    const body = {
      name: "Team Name",
      isActive: true,
      type: "EFUSE"
    };

    createTeam({
      variables: {
        tournamentIdOrSlug: tournament._id,
        body
      }
    });
  };

  const deleteTeamAction = team => {
    deleteTeam({
      variables: {
        id: team._id
      },
      optimisticResponse: {
        deleteTeam: team
      }
    });
  };

  return (
    <>
      <PageTitleDetails
        title="Build Your Teams and Players"
        tooltipText="There are three options to add new players seen below."
      />

      <div className={Style.headerContainer}>
        {!isBracket && (
          <div
            className={Style.addTeam}
            role="button"
            tabIndex="0"
            onClick={() => createTeamAction()}
            onKeyPress={() => createTeamAction()}
          >
            <FontAwesomeIcon className={Style.plus} icon={faPlus} />
            Add Team
          </div>
        )}
      </div>
      {stateTeams?.length > 0 ? (
        <InputRow>
          {stateTeams.map((team, teamIndex) => {
            return (
              <CreateTeamItem
                teamIndex={teamIndex}
                team={team}
                applicants={applicants}
                onTeamChange={onTeamChange}
                onTeamBlur={onTeamBlur}
                onPlayerChange={onPlayerChange}
                onPlayerBlur={onPlayerBlur}
                createPlayer={createPlayer}
                deletePlayer={deletePlayer}
                deleteTeam={() => deleteTeamAction(team)}
                getEfusePlayers={getEfusePlayers}
                isBracket={isBracket}
                key={teamIndex}
              />
            );
          })}
        </InputRow>
      ) : (
        <>
          <br />
          <EmptyComponent text="You have not created any teams. Click the button above to begin adding teams." />
        </>
      )}
    </>
  );
};

export default CreateTeams;
