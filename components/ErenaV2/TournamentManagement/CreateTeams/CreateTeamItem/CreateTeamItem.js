import React from "react";
import { Col } from "react-bootstrap";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrash } from "@fortawesome/pro-solid-svg-icons";
import { faTwitch } from "@fortawesome/free-brands-svg-icons";
import AddPlayerButtons from "./AddPlayerButtons/AddPlayerButtons";
import Style from "./CreateTeamItem.module.scss";
import ConfirmAlert from "../../../../ConfirmAlert/ConfirmAlert";
import { withCdn } from "../../../../../common/utils";
import CustomDropdown from "../../../../CustomDropdown/CustomDropdown";

const CreateTeamItem = ({
  team,
  teamIndex,
  onTeamChange,
  onTeamBlur,
  onPlayerBlur,
  onPlayerChange,
  deleteTeam,
  isBracket,
  applicants,
  createPlayer,
  deletePlayer,
  getEfusePlayers
}) => {
  const deletePlayerAction = (player, teamObj) => {
    deletePlayer({
      variables: {
        id: player?._id
      },
      optimisticResponse: {
        deletePlayer: { ...player, team: teamObj._id }
      }
    });
  };

  return (
    <Col sm={6} className={Style.teamInput} key={teamIndex}>
      <div className={Style.teamCard}>
        <div className={Style.inputHeader}>
          <div className={Style.fullWidth}>
            <p className={Style.teamCardTitle}>Team</p>
            <input
              className={Style.teamNameInputField}
              value={team?.name}
              maxLength={50}
              onChange={event => onTeamChange(event, team?._id)}
              onBlur={event => onTeamBlur(event, team?._id)}
            />
          </div>
          {!isBracket && (
            <ConfirmAlert title="Delete this Team?" message="Are you sure?" onYes={() => deleteTeam(team?._id)}>
              <FontAwesomeIcon className={`${Style.deleteIcon} ${Style.postitionAbsolute}`} icon={faTrash} />
            </ConfirmAlert>
          )}
        </div>
        <div className={Style.playerContainer}>
          <div className={Style.playerTitleWrapper}>
            <p className={Style.teamCardTitle}>Players</p>
            <AddPlayerButtons
              applicants={applicants}
              team={team}
              createPlayer={createPlayer}
              excludedPlayers={getEfusePlayers}
            />
          </div>

          {team?.players &&
            team?.players.map((player, playerIndex) => {
              const menu = [];

              if (player?.user) {
                menu.push({
                  title: `@${player?.user?.username}`,
                  as: "button",
                  type: "button",
                  customIconSrc: withCdn("/static/images/eFuse_Primary_Logo.svg"),
                  onClick: () => {
                    window.open(`/u/${player.user.username}`);
                  }
                });
              }

              if (player?.user?.twitch?.username) {
                menu.push({
                  title: "Twitch",
                  as: "button",
                  type: "button",
                  icon: faTwitch,
                  onClick: () => {
                    window.open(`/stream/${player.user.twitch?.username}`);
                  }
                });
              }

              menu.push({
                title: "Delete",
                as: "button",
                type: "button",
                icon: faTrash,
                message: "Delete this player?",
                onClick: () => {
                  deletePlayerAction(player, team);
                }
              });

              return (
                <div key={`${playerIndex}-${player?._id}`}>
                  <div className={Style.inputPlayerHeader}>
                    <input
                      className={Style.playersInputField}
                      value={player?.name}
                      maxLength={50}
                      onChange={event => onPlayerChange(event, team?._id, player?._id)}
                      onBlur={event => onPlayerBlur(event, team?._id, player?._id)}
                    />
                    {player?.user && (
                      <a target="_blank" href={`/u/${player?.user?.username}`} rel="noreferrer">
                        <img
                          alt="eFuse Profile Logo"
                          src={withCdn("/static/images/eFuse_Secondary_Logo.svg")}
                          className={Style.efuseLogo}
                        />
                      </a>
                    )}
                    <CustomDropdown icon="darkDots" items={menu} customClass={Style.dropdown} />
                  </div>
                </div>
              );
            })}
        </div>
      </div>
    </Col>
  );
};

export default CreateTeamItem;
