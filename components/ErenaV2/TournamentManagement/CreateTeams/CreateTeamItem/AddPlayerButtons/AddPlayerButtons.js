import React, { useState } from "react";
import { faPlus, faUser, faUsers } from "@fortawesome/pro-solid-svg-icons";
import { useDispatch } from "react-redux";

import Style from "./AddPlayerButtons.module.scss";
import SelectApplicantPlayer from "../../SelectApplicantPlayer/SelectApplicantPlayer";
import EFPrimaryModal from "../../../../../Modals/EFPrimaryModal/EFPrimaryModal";
import SelectUserModal from "../../../../../GlobalModals/SelectUserModal/SelectUserModal";
import EFCircleIconButtonTooltip from "../../../../../Buttons/EFCircleIconButtonTooltip/EFCircleIconButtonTooltip";
import { sendNotification } from "../../../../../../helpers/FlashHelper";
import { setErrors } from "../../../../../../store/actions/errorActions";

const AddPlayerButtons = ({ applicants, team, createPlayer, excludedPlayers }) => {
  const dispatch = useDispatch();

  const [isOpen, toggleModal] = useState(false);

  const addBasicPlayer = teamId => {
    const data = {
      name: "Player Name",
      type: "EFUSE",
      isActive: true
    };
    createPlayer({
      variables: {
        teamId,
        body: data
      }
    })
      .then(() => {
        sendNotification("Player added successfully", "success", "Success");
      })
      .catch(error => {
        dispatch(setErrors({ error: error.message }));
      });
  };

  const addEfusePlayer = (teamId, user) => {
    const data = {
      name: user?.username,
      type: "EFUSE",
      user: user?.value
    };

    createPlayer({
      variables: {
        teamId,
        body: data
      }
    })
      .then(() => {
        sendNotification("Player added successfully", "success", "Success");
      })
      .catch(error => {
        dispatch(setErrors({ error: error.message }));
      });
  };

  const selectPlayers = players => {
    players.forEach(player => addEfusePlayer(team?._id, player));
  };

  const selectApplicants = players => {
    players.forEach(player => addEfusePlayer(team?._id, player));
    toggleModal(false);
  };

  return (
    <div className={Style.buttonWrapper}>
      <div className={Style.marginLeft}>
        <EFCircleIconButtonTooltip
          theme="light"
          icon={faPlus}
          shadowTheme="small"
          tooltipPlacement="bottom"
          size="medium"
          tooltipContent="Create Player"
          onClick={() => addBasicPlayer(team?._id)}
        />
      </div>

      <div className={Style.marginLeft}>
        <SelectUserModal
          submitFunction={selectPlayers}
          excludedUsers={excludedPlayers()}
          title="Select Users"
          placeholder="Search..."
        >
          <EFCircleIconButtonTooltip
            theme="light"
            icon={faUser}
            shadowTheme="small"
            tooltipPlacement="bottom"
            size="medium"
            tooltipContent="Select eFuse Users"
          />
        </SelectUserModal>
      </div>

      <>
        <div className={Style.marginLeft}>
          <EFCircleIconButtonTooltip
            theme="light"
            icon={faUsers}
            shadowTheme="small"
            tooltipPlacement="bottom"
            size="medium"
            tooltipContent={
              !(applicants?.length > 0)
                ? "An opportunity with accepted applicants must be linked to add applicants"
                : "Select Opportunity Applicants"
            }
            onClick={() => toggleModal(true)}
            disabled={!(applicants?.length > 0)}
          />
        </div>
        <EFPrimaryModal
          title="Select Applicants"
          isOpen={isOpen}
          allowBackgroundClickClose={false}
          displayCloseButton
          onClose={() => toggleModal(false)}
        >
          <SelectApplicantPlayer applicants={applicants} submitFunction={selectApplicants} />
        </EFPrimaryModal>
      </>
    </div>
  );
};

export default AddPlayerButtons;
