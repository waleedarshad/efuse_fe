import React, { useEffect, useState } from "react";
import { withRouter } from "next/router";
import { useSelector, useDispatch } from "react-redux";
import { useLazyQuery } from "@apollo/client";
import Internal from "../../Layouts/Internal/Internal";
import Style from "./TournamentManagement.module.scss";
import CreateTeams from "./CreateTeams/CreateTeams";
import TeamScoring from "./TeamScoring/TeamScoring";
import EmptyComponent from "../../EmptyComponent/EmptyComponent";
import { getPostableOrganizations } from "../../../store/actions/userActions";
import BracketV2 from "../../BracketV2/BracketV2";
import InternalNavigation from "../InternalNavigation/InternalNavigation";
import { erenaNavigationListV2 } from "../../../navigation/erena";
import EFSubHeader from "../../Layouts/Internal/EFSubHeader/EFSubHeader";
import { withCdn } from "../../../common/utils";
import { GET_ERENA_TOURNAMENT } from "../../../graphql/ERenaTournamentQuery";

const TournamentManagement = props => {
  const currentUser = useSelector(state => state.auth.currentUser);
  const [getTournament, { data }] = useLazyQuery(GET_ERENA_TOURNAMENT);
  const currentUserOrgs = useSelector(state => state.user.postableOrganizations);
  const [userRoles, setRoles] = useState([]);
  const [tournament, setTournament] = useState(null);
  const { router } = props;

  const dispatch = useDispatch();

  // always read data from cache on changes
  useEffect(() => {
    if (data?.getERenaTournament) {
      setTournament(data?.getERenaTournament);
    }
  }, [JSON.stringify(data?.getERenaTournament)]);

  useEffect(() => {
    analytics.page("eRena Manage Teams");
    const { event } = props.pageProps;

    // call once to cache tournament
    getTournament({ variables: { tournamentIdOrSlug: event?.tournamentId } });

    if (userRoles) {
      setRoles(userRoles);
    }
    // check if current user
    dispatch(getPostableOrganizations(1));
  }, []);

  const scoringParameter = router.query.scoring;
  const bracketParameter = router.query.bracket;
  const teamsPage = router.query.teams;
  let isOwner = false;
  let isAdministrator = false;
  let actionButtons;

  const hasAccess = () => {
    return isOwner || isAdministrator;
  };

  const orgOwner = currentUserOrgs?.find(org => {
    return org?.id === tournament?.owner;
  });

  if (tournament) {
    // Check if current user is owner/admin of tournament
    if (currentUser?.id === tournament?.owner || orgOwner) {
      isOwner = true;
    }
    // set action buttons for nav
    actionButtons = InternalNavigation(tournament);
  }

  if (userRoles) {
    isAdministrator = userRoles.find(role => role.user._id === currentUser?.id) !== undefined;
  }

  // white_label or efuse
  return (
    <Internal metaTitle="eFuse | eRena Team Management" containsSubheader>
      <EFSubHeader
        headerImage={withCdn("/static/images/eRenaLogo.png")}
        navigationList={
          hasAccess && erenaNavigationListV2(router?.query, router?.pathname, tournament?.bracketType === "bracket")
        }
        actionButtons={actionButtons}
      />
      {hasAccess ? (
        <>
          {scoringParameter && (
            <div className={Style.scoringContainer}>
              <TeamScoring tournament={tournament} />
            </div>
          )}
          {bracketParameter && <BracketV2 editMatch tournament={tournament} />}
          {teamsPage && (
            <div className={Style.teamsContainer}>
              <CreateTeams tournament={tournament} isBracket={tournament?.bracketType === "bracket"} />
            </div>
          )}
        </>
      ) : (
        <EmptyComponent text="You do not have authority to edit or view these teams." />
      )}
    </Internal>
  );
};

export default withRouter(TournamentManagement);
