import { useMutation } from "@apollo/client";
import React, { useEffect, useState } from "react";
import { Form, Spinner } from "react-bootstrap";
import { useDispatch } from "react-redux";
import { sendNotification } from "../../../../../helpers/FlashHelper";
import { BULK_CREATE_ERENA_SCORES, BULK_UPDATE_ERENA_SCORES } from "../../../../../graphql/eRena/eRenaScore";
import Style from "../TeamBulkScoring.module.scss";
import InputLabel from "../../../../InputLabel/InputLabel";
import InputField from "../../../../InputField/InputField";
import EFRectangleButton from "../../../../Buttons/EFRectangleButton/EFRectangleButton";
import {
  bulkAddScoresToCache,
  bulkUpdateScoresInCache
} from "../../TournamentManagementCacheActions/TeamScoringCacheActions";
import EFScrollbar from "../../../../EFScrollbar/EFScrollbar";
import { setErrors } from "../../../../../store/actions/errorActions";

const TeamBulkScoringModal = ({ tournament, closeModal }) => {
  const dispatch = useDispatch();

  const erenaScores = tournament?.scores || [];
  const [stateScores, setStateScores] = useState([]);
  const [isCreateScoresLoading, setIsCreateScoresLoading] = useState(false);
  const [isUpdateScoresLoading, setIsUpdateScoresLoading] = useState(false);

  const [bulkCreateErenaScores] = useMutation(BULK_CREATE_ERENA_SCORES, {
    update: bulkAddScoresToCache
  });

  const [bulkUpdateErenaScores] = useMutation(BULK_UPDATE_ERENA_SCORES, {
    update: bulkUpdateScoresInCache
  });

  useEffect(() => {
    initializeStateScores();
  }, []);

  const initializeStateScores = () => {
    const foundPlayerScores = tournament?.teams.map(team => {
      return team?.players.map(player => {
        const foundScore = erenaScores.find(
          eScore => eScore.ownerId === player._id && eScore.erenaKind === "pointRace"
        );

        if (foundScore) {
          // add the score id to the player object if the score is found
          return { ...player, score: foundScore.score, scoreId: foundScore._id, previousScore: foundScore.score };
        }

        return { ...player, previousScore: 0 };
      });
    });
    const playerScores = [].concat(...foundPlayerScores.map(item => item));

    const newScores = playerScores || [];
    setStateScores(newScores);
  };

  // when a player value is changed, update the score in state
  const onPlayerChange = (event, player) => {
    const newScores = [...stateScores];
    const playerScoreIndex = stateScores.findIndex(x => x._id === player._id);
    // set the value to value as number and to event.target.value if there is no value
    if (playerScoreIndex === -1) {
      newScores[stateScores.length].score = event.target.valueAsNumber || Number(event.target.value);
    } else {
      newScores[playerScoreIndex].score = event.target.valueAsNumber || Number(event.target.value);
    }
    setStateScores(newScores);
  };

  // find the score for the player id
  const findScore = player => {
    let returnScore = 0;
    const foundPlayer = stateScores.findIndex(x => x._id === player._id);
    // check if the user has removed the score, if so allow the score to be set to ""
    if (stateScores[foundPlayer]?.score || stateScores[foundPlayer]?.score === "") {
      returnScore = stateScores[foundPlayer].score;
    }
    return returnScore;
  };

  const scoreChanged = player => {
    let changed = false;
    const playerScore = findScore(player);
    const prevPlayerScore = findPreviousScore(player);
    if (playerScore !== prevPlayerScore) {
      changed = true;
    }
    return changed;
  };

  const findPreviousScore = player => {
    let returnScore = 0;
    const playerScoreIndex = stateScores.findIndex(x => x._id === player._id);
    if (stateScores[playerScoreIndex]?.previousScore || stateScores[playerScoreIndex]?.previousScore === "") {
      returnScore = stateScores[playerScoreIndex].previousScore;
    }
    return returnScore;
  };

  // loop through the player scores and call the proper function for each score
  const onSubmit = e => {
    e.preventDefault();

    setIsCreateScoresLoading(true);
    setIsUpdateScoresLoading(true);

    const updatedScores = [];
    const createdScores = [];

    stateScores.forEach(player => {
      // update the score if it exists, create a score if it doesn't
      if (player?.scoreId) {
        // only updating teams that have had score changes
        if (scoreChanged(player)) {
          const data = {
            ownerId: player._id,
            ownerKind: "player",
            score: Number(player.score || "0")
          };
          updatedScores.push(data);
        }
      } else {
        const data = {
          ownerId: player._id,
          ownerKind: "player",
          score: Number(player.score || "0")
        };
        createdScores.push(data);
      }
    });

    const promises = [];
    if (createdScores.length > 0) promises.push(bulkCreateScores(createdScores));
    if (updatedScores.length > 0) promises.push(bulkUpdateScores(updatedScores));

    // close modal if no scores were created nor updated
    if (promises.length === 0) {
      closeModal();
    } else {
      Promise.all(promises)
        .then(() => {
          sendNotification("Successfully updated the player scores", "success", "Updating Scores");
        })
        .catch(error => {
          dispatch(setErrors({ error: error.message }));
        });
    }
  };

  // calling the GraphQL endpoint for creating many scores at once
  const bulkCreateScores = async createdScores => {
    return bulkCreateErenaScores({
      variables: {
        body: createdScores,
        tournamentIdOrSlug: tournament._id
      }
    })
      .then(() => {
        setIsCreateScoresLoading(false);
      })
      .catch(error => {
        dispatch(setErrors({ error: error.message }));
      });
  };

  // calling the GraphQL endpoint for updating many scores at once
  const bulkUpdateScores = async updatedScores => {
    return bulkUpdateErenaScores({
      variables: {
        body: updatedScores,
        tournamentIdOrSlug: tournament._id
      }
    })
      .then(() => {
        setIsUpdateScoresLoading(false);
      })
      .catch(error => {
        dispatch(setErrors({ error: error.message }));
      });
  };

  return (
    <Form onSubmit={onSubmit}>
      <div className={Style.wrapper}>
        <EFScrollbar>
          {tournament?.teams?.map(team => {
            return (
              <div key={team?._id} className={Style.teamScoringWrapper}>
                <h5>{`${team.name}`}</h5>
                <div className={Style.playersWrapper}>
                  {team?.players?.map(player => {
                    return (
                      <div key={player?._id}>
                        <InputLabel theme="darkColor">{player?.name}</InputLabel>
                        <div className={Style.inputField}>
                          <InputField
                            name="playerScore"
                            theme={scoreChanged(player) ? "success" : "whiteShadow"}
                            size="lg"
                            onChange={event => onPlayerChange(event, player)}
                            value={findScore(player)}
                            type="number"
                            disabled={isCreateScoresLoading || isUpdateScoresLoading}
                          />
                        </div>
                        {(scoreChanged(player) && (
                          <p className={Style.previous}>{`Previous score: ${findPreviousScore(player)}`}</p>
                        )) || <p className={Style.previous} />}
                      </div>
                    );
                  })}
                </div>
              </div>
            );
          })}
        </EFScrollbar>
      </div>
      <div className={Style.buttonWrapper}>
        <EFRectangleButton
          colorTheme="primary"
          buttonType="submit"
          text={
            <>
              {(isCreateScoresLoading || isUpdateScoresLoading) && (
                <Spinner className="mr-2" animation="border" variant="light" size="sm" />
              )}
              {"Save Scores"}
            </>
          }
          disabled={isCreateScoresLoading || isUpdateScoresLoading}
        />
      </div>
    </Form>
  );
};

export default TeamBulkScoringModal;
