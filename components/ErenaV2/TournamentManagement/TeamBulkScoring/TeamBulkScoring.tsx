import React, { useState } from "react";
import { faEdit } from "@fortawesome/pro-solid-svg-icons";

import TeamBulkScoringModal from "./TeamBulkScoringModal/TeamBulkScoringModal";
import Modal from "../../../Modal/Modal";

import EFCircleIconButtonTooltip from "../../../Buttons/EFCircleIconButtonTooltip/EFCircleIconButtonTooltip";

const TeamBulkScoring = ({ tournament }) => {
  const [forceCloseModal, setForceCloseModal] = useState(false);

  const closeModal = () => {
    setForceCloseModal(true);
  };

  return (
    <div>
      <Modal
        displayCloseButton
        allowBackgroundClickClose={false}
        component={<TeamBulkScoringModal tournament={tournament} closeModal={closeModal} />}
        textAlignCenter={false}
        customMaxWidth="800px"
        title="Edit Scores"
        forceClose={forceCloseModal}
        onOpenModal={() => {
          setForceCloseModal(false);
        }}
      >
        <EFCircleIconButtonTooltip
          icon={faEdit}
          shadowTheme="small"
          tooltipPlacement="bottom"
          size="medium"
          tooltipContent="Edit All Scores"
        />
      </Modal>
    </div>
  );
};

export default TeamBulkScoring;
