import { ADD_SCORE_CACHE_FRAGMENT, UPDATE_SCORE_CACHE_FRAGMENT } from "../../../../graphql/eRena/eRenaScore";

export const addScoreToCache = (cache, { data: { createScore: scoreData } }) => {
  cache.modify({
    id: `ERenaTournament:${scoreData.tournament._id}`,
    fields: {
      scores: exisitingScores => {
        const newScoreRef = cache.writeFragment({
          data: scoreData,
          fragment: ADD_SCORE_CACHE_FRAGMENT
        });
        return [...exisitingScores, newScoreRef];
      }
    }
  });
};

export const updateScoreInCache = (cache, { data: { updateScore: scoreData } }) => {
  cache.writeQuery({
    query: UPDATE_SCORE_CACHE_FRAGMENT,
    data: scoreData,
    variables: {
      id: scoreData._id
    }
  });
};

export const bulkAddScoresToCache = (cache, { data: { bulkCreateScores: scoreData } }) => {
  scoreData.forEach(item => {
    addScoreToCache(cache, { data: { createScore: item } });
  });
};

export const bulkUpdateScoresInCache = (cache, { data: { bulkUpdateScores: scoreData } }) => {
  scoreData.forEach(item => {
    updateScoreInCache(cache, { data: { updateScore: item } });
  });
};
