import { TEAM_UPDATE_CACHE_QUERY } from "../../../../graphql/eRena/eRenaTeam";
import { TOURNAMENT_CACHE_FRAGMENT } from "../../../../graphql/ERenaTournamentQuery";

export const addTeamToCache = (cache, { data: { createErenaTeam: teamData } }) => {
  if (teamData) {
    cache.modify({
      id: `ERenaTournament:${teamData?.tournament?._id}`,
      fields: {
        teams: exisitingTeams => {
          const newTeamRef = cache.writeFragment({
            data: teamData,
            fragment: TOURNAMENT_CACHE_FRAGMENT
          });
          return [...exisitingTeams, newTeamRef];
        }
      }
    });
  }
};

export const deleteTeamFromCache = (cache, { data: { deleteErenaTeam: teamData } }) => {
  if (teamData) {
    cache.modify({
      id: `ERenaTournament:${teamData?.tournament?._id}`,
      fields: {
        teams: (exisitingTeams, { readField }) => {
          return exisitingTeams.filter(team => readField("_id", team) !== teamData?._id);
        }
      }
    });
  }
};

export const updateTeamInCache = (cache, { data: { updateErenaTeam: teamData } }) => {
  if (teamData) {
    cache.writeQuery({
      query: TEAM_UPDATE_CACHE_QUERY,
      data: teamData,
      variables: {
        id: teamData?._id
      }
    });
  }
};
