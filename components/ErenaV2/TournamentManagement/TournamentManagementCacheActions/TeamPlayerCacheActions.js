import { ADD_PLAYER_CACHE_QUERY, UPDATE_PLAYER_CACHE_QUERY } from "../../../../graphql/eRena/eRenaPlayer";

export const addPlayerToCache = (cache, { data: { createPlayer: playerData } }) => {
  cache.modify({
    id: `ERenaTeam:${playerData.team._id}`,
    fields: {
      players: eTeamPlayers => {
        const newPlayerRef = cache.writeFragment({
          data: playerData,
          fragment: ADD_PLAYER_CACHE_QUERY
        });
        return [...eTeamPlayers, newPlayerRef];
      }
    }
  });
};

export const deletePlayerFromCache = (cache, { data: { deletePlayer: playerData } }) => {
  cache.modify({
    id: `ERenaTeam:${playerData.team._id}`,
    fields: {
      players: (exisitingPlayers, { readField }) => {
        return exisitingPlayers.filter(player => readField("_id", player) !== playerData._id);
      }
    }
  });
};

export const updatePlayerInCache = (cache, { data: { updatePlayer: playerData } }) => {
  cache.writeQuery({
    query: UPDATE_PLAYER_CACHE_QUERY,
    data: playerData,
    variables: {
      id: playerData._id
    }
  });
};
