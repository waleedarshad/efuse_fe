import React, { Component } from "react";
import { connect } from "react-redux";
import { NextSeo } from "next-seo";
import Internal from "../../Layouts/Internal/Internal";
import EmptyComponent from "../../EmptyComponent/EmptyComponent";
import CreateErenaEventFlow from "../CreateErenaEventFlow/CreateErenaEventFlow";
import Modal from "../../Modal/Modal";

class CreateEventPage extends Component {
  componentDidMount() {
    analytics.page("Create Event Page");
  }

  render() {
    const { allowCreation } = this.props;

    return (
      <>
        <NextSeo title="eRena powered by eFuse | Duke it out on a Global Stage" />
        <Internal isAuthenticated noCollapse>
          <Modal
            displayCloseButton={false}
            allowBackgroundClickClose={false}
            openOnLoad={allowCreation}
            component={<CreateErenaEventFlow />}
          />
          {!allowCreation && (
            <EmptyComponent
              style={{ display: "block", width: "100%" }}
              text="You do not have permission to create eRena events."
            />
          )}
        </Internal>
      </>
    );
  }
}

const mapStateToProps = state => ({
  isWindowView: state.messages.isWindowView,
  currentUser: state.auth.currentUser,
  allowCreation: state.features.allow_erena_creation
});

export default connect(mapStateToProps, {})(CreateEventPage);
