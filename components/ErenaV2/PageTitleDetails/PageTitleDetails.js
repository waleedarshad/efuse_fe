import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus, faUser, faUsers } from "@fortawesome/pro-solid-svg-icons";

import Style from "./PageTitleDetails.module.scss";
import HelpToolTip from "../../HelpToolTip/HelpToolTip";

const PageTitleDetails = ({ title, tooltipText, selectOpportuntyTitle }) => {
  return (
    <div className={Style.pageContainer}>
      <h3 className={Style.pageTitle}>
        {title} <HelpToolTip text={tooltipText} placement="bottom" />
      </h3>
      {selectOpportuntyTitle ? (
        <ul className={Style.pageDescription}>
          <li>The opportunity must be an Event opportunity to appear in this list</li>
          <li>The owner of the event must also own the opportunity</li>
          <li>
            Linking an opportunity is <b>not required</b> to build your teams
          </li>
        </ul>
      ) : (
        <ul className={Style.pageDescription}>
          <li>
            Click <FontAwesomeIcon className={Style.pageIcon} icon={faPlus} /> to create a general player
          </li>
          <li>
            Click <FontAwesomeIcon className={Style.pageIcon} icon={faUser} /> to create a player from eFuse
          </li>
          <li>
            Click <FontAwesomeIcon className={Style.pageIcon} icon={faUsers} /> to create a player from an eFuse
            opportunity <i>(an opportunity must be linked on settings)</i>
          </li>
        </ul>
      )}
    </div>
  );
};

export default PageTitleDetails;
