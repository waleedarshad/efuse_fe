import React from "react";
import Style from "./ErenaCallToAction.module.scss";
import EFCard from "../../Cards/EFCard/EFCard";
import DynamicModal from "../../DynamicModal/DynamicModal";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";
import FeatureFlag from "../../General/FeatureFlag/FeatureFlag";
import FeatureFlagVariant from "../../General/FeatureFlag/FeatureFlagVariant/FeatureFlagVariant";

const ErenaCallToAction = () => {
  return (
    <FeatureFlag name="feature_web_erena_call_to_action">
      <FeatureFlagVariant flagState>
        <div className={Style.addGlow}>
          <EFCard widthTheme="fullWidth" shadow="small">
            <div className={Style.callWrapper}>
              <span className={Style.callText}>Don&apos;t miss your chance to enter other tournaments like this.</span>
              <div className={Style.buttonWrapper}>
                <DynamicModal
                  flow="LoginSignup"
                  openOnLoad={false}
                  startView="signup"
                  displayCloseButton
                  allowBackgroundClickClose={false}
                >
                  <EFRectangleButton
                    text="Join eFuse"
                    colorTheme="primary"
                    onClick={() => analytics.track("ERENA_CALL_TO_ACTION_SIGNUP_CLICKED")}
                  />
                </DynamicModal>
              </div>
            </div>
          </EFCard>
        </div>
      </FeatureFlagVariant>
    </FeatureFlag>
  );
};

export default ErenaCallToAction;
