import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faVideo } from "@fortawesome/pro-solid-svg-icons";

import ErenaCallToAction from "../ErenaCallToAction/ErenaCallToAction";
import EFPromoCards from "../ErenaDetails/EFPromoCards/EFPromoCards";
import Style from "../ErenaV2.module.scss";
import AddItem from "../../AddItem/AddItem";
import BracketV2 from "../../BracketV2/BracketV2";
import DynamicModal from "../../DynamicModal/DynamicModal";
import FeatureFlag from "../../General/FeatureFlag/FeatureFlag";
import FeatureFlagVariant from "../../General/FeatureFlag/FeatureFlagVariant/FeatureFlagVariant";
import LinkTwitchStream from "../ErenaDetails/LinkTwitchStream/LinkTwitchStream";
import MockExternalUser from "../../MockExternalUser/MockExternalUser";
import EFPointRaceLeaderboard from "../../EFPointRaceLeaderboard/EFPointRaceLeaderboard";
import TwitchMultipleStreams from "../../TwitchMultipleStreams/TwitchMultipleStreams";
import TwitchVideoPlayerCustomChat from "../../TwitchVideoPlayerCustomChat/TwitchVideoPlayerCustomChat";

const TournamentEventComponent = ({
  currentUser,
  tournament,
  eventsStreamButton,
  leaderboardRoute,
  isOwner,
  mockExternalErenaEvent,
  ads,
  router
}) => {
  const erenaTeams = useSelector(state => state.erenaV2.allTeams);
  const eventsLogin = useSelector(state => state.features.events_login);

  const [streams, setStreams] = useState([]);

  useEffect(() => {
    if (erenaTeams) {
      setStreams(getPlayersStreams());
    }
  }, [erenaTeams]);

  const getPlayersStreams = () => {
    const playerStreams = [];
    erenaTeams?.forEach(team => {
      const playersList = [];
      team?.players?.forEach(player => {
        if (player?.user?.twitchUserName) {
          playersList.push({
            name: player?.name,
            channel: player?.user?.twitchUserName,
            type: "twitch"
          });
        }
      });
      if (playersList?.length > 0) {
        playerStreams.push({ name: team?.name, players: [...playersList] });
      }
    });
    return playerStreams;
  };

  return (
    <>
      <div className={Style.mockExternalUserWrapper}>
        <MockExternalUser currentUser={currentUser} type="erena" erenaEvent owner={isOwner} />
      </div>

      {/* erena background image */}
      {tournament?.backgroundImageUrl ? (
        <img className={Style.backgroundImage} src={tournament.backgroundImageUrl} alt="erena background" />
      ) : tournament.brandLogoUrl ? (
        <img className={Style.backgroundLogo} src={tournament?.brandLogoUrl} alt="erena logo" />
      ) : (
        <></>
      )}

      {eventsLogin && (
        <DynamicModal
          flow="LoginSignup"
          openOnLoad
          startView="login"
          displayCloseButton={false}
          allowBackgroundClickClose={false}
        />
      )}

      {eventsStreamButton && leaderboardRoute && (
        <a href={router.query.e} className={Style.link}>
          <p className={Style.viewStream}>
            View Stream
            <FontAwesomeIcon className={Style.icon} icon={faVideo} />
          </p>
        </a>
      )}

      <div className={Style.container}>
        {/* twitch channel */}
        {!leaderboardRoute && (
          <>
            {tournament?.twitchChannel ? (
              <div className={Style.twitchWrapper}>
                {streams?.length > 0 ? (
                  <TwitchMultipleStreams
                    mainStream={{
                      players: [{ name: tournament.twitchChannel, channel: tournament.twitchChannel, type: "twitch" }]
                    }}
                    streams={streams}
                    streamTitle={tournament.tournamentName}
                    startingChannel={tournament.twitchChannel}
                    startingName={tournament.twitchChannel}
                  />
                ) : (
                  <TwitchVideoPlayerCustomChat channel={tournament.twitchChannel} title={tournament.tournamentName} />
                )}
              </div>
            ) : isOwner && !mockExternalErenaEvent ? (
              <AddItem text="Twitch Channel" modalTitle="Manage Twitch Channel" modalComponent={<LinkTwitchStream />}>
                <div className={Style.twitchWrapper} />
              </AddItem>
            ) : (
              <></>
            )}
          </>
        )}

        {!currentUser?.id && (
          <div className={Style.marginTop}>
            <ErenaCallToAction />
          </div>
        )}

        <div className={Style.marginTop}>
          <EFPromoCards
            gridLayout="double"
            ads={ads}
            isOwner={isOwner}
            mockExternalErenaEvent={mockExternalErenaEvent}
          />
        </div>

        {/* leaderboard or bracket */}
        {!tournament?.hideLeaderboard && (
          <>
            {tournament?.bracketType === "pointRace" ? (
              <div className={Style.leaderboardWrapper}>
                <FeatureFlag name="erena_leaderboard">
                  <FeatureFlagVariant flagState>
                    <EFPointRaceLeaderboard twitchView={false} />
                  </FeatureFlagVariant>
                </FeatureFlag>
              </div>
            ) : (
              <div className={Style.marginTop}>
                <BracketV2 tournament={tournament} fromEventMainPage />
              </div>
            )}
          </>
        )}

        {tournament?.challongeLeaderboard && (
          <div className={Style.borderRadius}>
            <iframe
              title="challonge leaderboard"
              src={tournament?.challongeLeaderboard}
              width="100%"
              height="500"
              frameBorder="0"
              scrolling="auto"
              allowtransparency="true"
            />
          </div>
        )}
      </div>
    </>
  );
};

export default TournamentEventComponent;
