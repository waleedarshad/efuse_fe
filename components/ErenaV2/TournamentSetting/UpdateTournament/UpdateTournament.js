import React, { useState, useEffect } from "react";
import { Col, Form } from "react-bootstrap";
import { useDispatch } from "react-redux";

import { useMutation } from "@apollo/client";
import InputLabel from "../../../InputLabel/InputLabel";
import SelectBox from "../../../SelectBox/SelectBox";
import InputRow from "../../../InputRow/InputRow";
import InputField from "../../../InputField/InputField";
import HelpToolTip from "../../../HelpToolTip/HelpToolTip";
import Style from "./UpdateTournament.module.scss";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";
import EFHtmlInput from "../../../EFHtmlInput/EFHtmlInput";
import { UPDATE_TOURNAMENT } from "../../../../graphql/ERenaTournamentQuery";
import { sendNotification } from "../../../../helpers/FlashHelper";
import { setErrors } from "../../../../store/actions/errorActions";
import statuses from "../../tournamentStatus.json";

const UpdateTournament = ({ tournament, currentUserOrgs, currentUser }) => {
  const validated = false;
  const dispatch = useDispatch();

  const [updateTournament] = useMutation(UPDATE_TOURNAMENT);

  const [erenaEvent, setErenaEvent] = useState({
    owner: "",
    tournamentName: "",
    tournamentDescription: "",
    rulesMarkdown: "",
    twitchChannel: "",
    websiteUrl: "",
    status: ""
  });

  useEffect(() => {
    if (tournament) {
      setErenaEvent({
        ...erenaEvent,
        owner: tournament.owner._id,
        tournamentName: tournament.tournamentName,
        tournamentDescription: tournament.tournamentDescription,
        rulesMarkdown: tournament.rulesMarkdown,
        twitchChannel: tournament.twitchChannel,
        websiteUrl: tournament.websiteUrl,
        status: tournament.status
      });
    }
  }, [tournament]);

  const getOwnerType = owner => {
    const ownerType = currentUserOrgs.find(org => {
      return org._id === owner;
    })
      ? "organizations"
      : "users";
    return ownerType;
  };

  // add orgs to owner selection list
  let formatedCreators = [];
  if (currentUserOrgs) {
    currentUserOrgs.map(org => {
      return formatedCreators.push({ value: org._id, label: org.name });
    });
  }

  // add current user to owner selection list
  if (currentUser?.id) {
    formatedCreators = [{ label: currentUser.name, value: currentUser.id }, ...formatedCreators];
  }

  const onChange = e => {
    setErenaEvent({
      ...erenaEvent,
      [e.target.name]: e.target.value
    });
  };

  const onEditorStateChange = (editorState, type) => {
    // previous state needs to be used here or else erena events will be saved with just description and rules
    setErenaEvent(previousState => {
      return {
        ...previousState,
        [type]: editorState
      };
    });
  };

  const onSubmit = event => {
    event.preventDefault();
    if (event.currentTarget.checkValidity()) {
      updateTournament({
        variables: {
          tournamentIdOrSlug: tournament._id,
          body: {
            owner: erenaEvent?.owner,
            ownerType: getOwnerType(erenaEvent?.owner),
            tournamentName: erenaEvent?.tournamentName,
            tournamentDescription: erenaEvent?.tournamentDescription,
            slug: tournament?.slug,
            rulesMarkdown: erenaEvent?.rulesMarkdown,
            twitchChannel: erenaEvent?.twitchChannel,
            websiteUrl: erenaEvent?.websiteUrl,
            status: erenaEvent?.status
          }
        }
      })
        .then(() => {
          sendNotification("Tournament was successfully updated.", "success", "Success");
        })
        .catch(error => {
          dispatch(setErrors({ error: error.message }));
        });
    }
  };

  return (
    <Form noValidate validated role="form" onSubmit={onSubmit}>
      <InputRow>
        <Col sm={12}>
          <div className={Style.labelContainer}>
            <InputLabel theme="darkColor">
              Owner
              <span className={Style.requiredAsterisk}>*</span>
            </InputLabel>
            <HelpToolTip text="Select the name of the owner for this tournament" />
          </div>
          <SelectBox
            name="owner"
            validated={validated}
            options={formatedCreators}
            size="md"
            value={erenaEvent?.owner}
            theme="whiteShadow"
            required
            onChange={e => onChange(e)}
            errorMessage="Owner is required."
          />
        </Col>
      </InputRow>
      <InputRow>
        <Col sm={12}>
          <div className={Style.labelContainer}>
            <InputLabel theme="darkColor">
              Event Name
              <span className={Style.requiredAsterisk}>*</span>
            </InputLabel>
            <HelpToolTip text="Enter the name of event" />
          </div>
          <InputField
            name="tournamentName"
            size="lg"
            theme="whiteShadow"
            onChange={e => onChange(e)}
            required
            placeholder="Add an event name"
            value={erenaEvent?.tournamentName}
            maxLength={70}
          />
        </Col>
      </InputRow>
      <InputRow>
        <Col sm={12}>
          <div className={Style.labelContainer}>
            <InputLabel theme="darkColor">
              Description
              <span className={Style.requiredAsterisk}>*</span>
            </InputLabel>
            <HelpToolTip text="Enter details about the tournament" />
          </div>
          <EFHtmlInput
            value={erenaEvent?.tournamentDescription || ""}
            onChange={e => onEditorStateChange(e, "tournamentDescription")}
          />
        </Col>
      </InputRow>
      <InputRow>
        <Col sm={12}>
          <div className={Style.labelContainer}>
            <InputLabel theme="darkColor">Rules</InputLabel>
            <HelpToolTip text="Enter the rules for the tournament" />
          </div>
          <EFHtmlInput
            value={erenaEvent?.rulesMarkdown || ""}
            onChange={e => onEditorStateChange(e, "rulesMarkdown")}
          />
        </Col>
      </InputRow>
      <InputRow>
        <Col sm={12}>
          <div className={Style.labelContainer}>
            {/* [PAVEL] THIS YOUTUBE FUNCTIONALITY IS TEMPORARY AND HACKY */}
            <InputLabel theme="darkColor">Twitch Channel / Youtube Live Video URL</InputLabel>
            <HelpToolTip text="If Twitch, this should only be the channel name, not the URL. Youtube should be the full live video URL." />
          </div>
          <InputField
            name="twitchChannel"
            size="lg"
            theme="whiteShadow"
            onChange={onChange}
            placeholder="Add a Twitch channel"
            value={erenaEvent?.twitchChannel || ""}
          />
        </Col>
      </InputRow>
      <InputRow>
        <Col sm={12}>
          <div className={Style.labelContainer}>
            <InputLabel theme="darkColor">Website URL</InputLabel>
            <HelpToolTip text="asfsasf" />
          </div>
          <InputField
            name="websiteUrl"
            size="lg"
            theme="whiteShadow"
            onChange={e => onChange(e)}
            placeholder="Add a website URL"
            value={erenaEvent?.websiteUrl || ""}
            pattern="https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)"
            errorMessage="The URL must be a valid URL and contain https://"
          />
        </Col>
      </InputRow>
      <InputRow>
        <Col sm={12}>
          <div className={Style.labelContainer}>
            <InputLabel theme="darkColor">Status</InputLabel>
            <HelpToolTip text="Select the status for this tournament" />
          </div>
          <SelectBox
            name="status"
            options={statuses}
            size="md"
            value={erenaEvent?.status}
            theme="whiteShadow"
            onChange={e => onChange(e)}
          />
        </Col>
      </InputRow>
      <div className={Style.buttonContainer}>
        <EFRectangleButton buttonType="submit" colorTheme="primary" text="Submit" />
      </div>
    </Form>
  );
};
export default UpdateTournament;
