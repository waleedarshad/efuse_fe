import { ERENA_TOURNAMENT_STAFF_FRAGMENT } from "../../../../graphql/eRena/eRenaStaff";

export const addStaffMemberToCache = (cache, { data: { createStaff: staffData } }) => {
  cache.modify({
    id: `ERenaTournament:${staffData.tournament._id}`,
    fields: {
      staff: existingStaff => {
        const staffRef = cache.writeFragment({
          data: staffData,
          fragment: ERENA_TOURNAMENT_STAFF_FRAGMENT
        });
        return [...existingStaff, staffRef];
      }
    }
  });
};

export const deleteStaffMemberFromCache = (cache, { data: { deleteStaff: staffData } }) => {
  cache.modify({
    id: `ERenaTournament:${staffData.tournament._id}`,
    fields: {
      staff: (existingStaff, { readField }) => {
        return existingStaff.filter(staff => readField("_id", staff) !== staffData._id);
      }
    }
  });
};
