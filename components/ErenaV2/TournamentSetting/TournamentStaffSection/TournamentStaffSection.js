import React from "react";
import { Table } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrash, faPlus } from "@fortawesome/pro-solid-svg-icons";
import { useDispatch } from "react-redux";
import Link from "next/link";
import { useMutation } from "@apollo/client";

import EmptyComponent from "../../../EmptyComponent/EmptyComponent";
import ConfirmAlert from "../../../ConfirmAlert/ConfirmAlert";
import Style from "./TournamentStaffSection.module.scss";
import EFCircleIconButtonTooltip from "../../../Buttons/EFCircleIconButtonTooltip/EFCircleIconButtonTooltip";
import SelectUserModal from "../../../GlobalModals/SelectUserModal/SelectUserModal";
import { CREATE_STAFF_MEMBER, DELETE_STAFF_MEMBER } from "../../../../graphql/eRena/eRenaStaff";
import { sendNotification } from "../../../../helpers/FlashHelper";
import { setErrors } from "../../../../store/actions/errorActions";
import EFRenderConditionally from "../../../EFRenderConditionally/EFRenderConditionally";
import HelpToolTip from "../../../HelpToolTip/HelpToolTip";
import {
  addStaffMemberToCache,
  deleteStaffMemberFromCache
} from "../TournamentSettingsCacheActions/StaffMemberCacheActions";

const TournamentStaffSection = ({ tournament }) => {
  const dispatch = useDispatch();

  const allStaffMembers = tournament?.staff || [];

  const [addStaffMember] = useMutation(CREATE_STAFF_MEMBER, {
    update: addStaffMemberToCache
  });

  const [deleteStaffMember] = useMutation(DELETE_STAFF_MEMBER, {
    update: deleteStaffMemberFromCache
  });

  const removeStaffMember = staffId => {
    deleteStaffMember({
      variables: {
        id: staffId
      }
    })
      .then(() => {
        sendNotification("Staff member removed successfully", "success");
      })
      .catch(error => {
        dispatch(setErrors({ error: error.message }));
      });
  };

  const addTournamentStaffMember = (tournamentId, user) => {
    const data = {
      user: user.value,
      role: "administrator"
    };

    addStaffMember({
      variables: {
        body: data,
        tournamentIdOrSlug: tournamentId
      }
    })
      .then(() => {
        sendNotification("New staff member added successfully", "success");
      })
      .catch(error => {
        dispatch(setErrors({ error: error.message }));
      });
  };

  const selectStaffMember = staffMembers => {
    staffMembers.forEach(member => addTournamentStaffMember(tournament?._id, member));
  };

  return (
    <>
      <div className={Style.addBtn}>
        <SelectUserModal submitFunction={selectStaffMember} title="Select Users" placeholder="Search...">
          <EFCircleIconButtonTooltip
            theme="light"
            icon={faPlus}
            shadowTheme="small"
            tooltipPlacement="bottom"
            size="medium"
            tooltipContent="Create Staff"
          />
        </SelectUserModal>
      </div>
      {allStaffMembers.length > 0 ? (
        <Table striped borderless hover>
          <thead>
            <tr>
              <th>User Name</th>
              <th>Display Name</th>
              <th>Role</th>
              <th aria-label="blank-header" />
            </tr>
          </thead>
          <tbody>
            {allStaffMembers?.map(staff => (
              <tr key={staff._id}>
                <th>
                  <Link href={`/u/${staff.user?.username}`}>{staff.user?.username || ""}</Link>
                </th>
                <td>{staff.user?.name}</td>
                <td>{staff.role}</td>
                <td>
                  <EFRenderConditionally condition={staff?.role === "owner"}>
                    <HelpToolTip
                      placement="right"
                      text="Cannot delete or modify an owner at this time. Please see eFuse admin for help."
                    />
                  </EFRenderConditionally>
                  <EFRenderConditionally condition={staff?.role !== "owner"}>
                    <ConfirmAlert
                      title="Remove Staff Member?"
                      message={`Are you sure you want to remove ${staff.user?.name} from the tournament staff members?`}
                      onYes={() => removeStaffMember(staff?._id)}
                    >
                      <FontAwesomeIcon className={Style.deleteIcon} icon={faTrash} />
                    </ConfirmAlert>
                  </EFRenderConditionally>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      ) : (
        <div className={Style.emptyComponent}>
          <EmptyComponent text='You have not added any staff members yet. Click the "+" button above to add a new staff member.' />
        </div>
      )}
    </>
  );
};

export default TournamentStaffSection;
