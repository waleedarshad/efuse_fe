import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { withRouter } from "next/router";
import { useLazyQuery } from "@apollo/client";
import SelectTournamentOpportunity from "./SelectTournamentOpportunity/SelectTournamentOpportunity";
import Style from "./TournamentSettings.module.scss";
import TournamentDesign from "./TournamentDesign/TournamentDesign";
import UpdateTournament from "./UpdateTournament/UpdateTournament";
import InternalNavigation from "../InternalNavigation/InternalNavigation";
import EFSubHeader from "../../Layouts/Internal/EFSubHeader/EFSubHeader";
import SettingsContainer from "../../Settings/Recruiting/SettingsContainer/SettingsContainer";
import Internal from "../../Layouts/Internal/Internal";
import { erenaNavigationListV2 } from "../../../navigation/erena";
import { getPostableOrganizations } from "../../../store/actions/userActions";
import { withCdn } from "../../../common/utils";
import TournamentStaffSection from "./TournamentStaffSection/TournamentStaffSection";
import { GET_ERENA_TOURNAMENT } from "../../../graphql/ERenaTournamentQuery";

const TournamentSetting = ({ pageProps, router }) => {
  const dispatch = useDispatch();
  const [getTournament, { data }] = useLazyQuery(GET_ERENA_TOURNAMENT);
  const [userRoles, setRoles] = useState([]);

  const currentUser = useSelector(state => state.auth.currentUser);
  const currentUserOrgs = useSelector(state => state.user.postableOrganizations);
  const [tournament, setTournament] = useState(null);

  // always read data from cache on changes
  useEffect(() => {
    if (data?.getERenaTournament) {
      setTournament(data?.getERenaTournament);
    }
  }, [JSON.stringify(data?.getERenaTournament)]);

  useEffect(() => {
    const { event, userRoles: roles } = pageProps;

    if (event?.tournamentId) {
      setTournament(event);

      dispatch(getPostableOrganizations(1));

      // call once to cache tournament
      getTournament({ variables: { tournamentIdOrSlug: event?.tournamentId } });

      if (roles) {
        setRoles(roles);
      }
    }
  }, [pageProps.event]);

  let actionButtons = [];
  let isOwner = false;
  let isAdministrator = false;

  const hasAccess = () => {
    return isOwner || isAdministrator;
  };

  const orgOwner = currentUserOrgs.find(org => {
    return org?.id === tournament?.owner;
  });

  if (tournament) {
    // Check if current user is owner/admin of tournament
    if (currentUser?.id === tournament?.owner || orgOwner) {
      isOwner = true;
    }
    // set action buttons for nav
    actionButtons = InternalNavigation(tournament);
  }

  if (userRoles) {
    isAdministrator = userRoles.find(role => role.user._id === currentUser?.id) !== undefined;
  }

  return (
    <Internal isAuthenticated containsSubheader>
      <EFSubHeader
        headerImage={withCdn("/static/images/eRenaLogo.png")}
        navigationList={
          hasAccess && erenaNavigationListV2(router?.query, router?.pathname, tournament?.bracketType === "bracket")
        }
        actionButtons={actionButtons}
      />
      <div className={Style.tournamentSettingsWrapper}>
        <SelectTournamentOpportunity tournament={tournament} ownerId={tournament?.owner} />
        <SettingsContainer title="Tournament Details">
          <UpdateTournament tournament={tournament} currentUserOrgs={currentUserOrgs} currentUser={currentUser} />
        </SettingsContainer>
        <SettingsContainer title="Tournament Design">
          <TournamentDesign tournament={tournament} />
        </SettingsContainer>
        <SettingsContainer title="Tournament Staff">
          <TournamentStaffSection tournament={tournament} />
        </SettingsContainer>
      </div>
    </Internal>
  );
};

export default withRouter(TournamentSetting);
