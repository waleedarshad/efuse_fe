import React, { useEffect } from "react";
import { useDispatch } from "react-redux";

import { Col } from "react-bootstrap";
import { useMutation } from "@apollo/client";
import { useForm } from "react-hook-form";
import InputRow from "../../../InputRow/InputRow";
import Style from "./TournamentDesign.module.scss";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";
import { UPDATE_TOURNAMENT } from "../../../../graphql/ERenaTournamentQuery";
import { sendNotification } from "../../../../helpers/FlashHelper";
import { setErrors } from "../../../../store/actions/errorActions";
import EFSwitchControl from "../../../FormControls/EFSwitchControl";
import EFColorControl from "../../../FormControls/EFColorControl";
import EFImageUploadControl from "../../../FormControls/uploadControls/EFImageUploadControl";

const TournamentDesign = ({ tournament }) => {
  const dispatch = useDispatch();

  const defaultValues = {
    primaryColor: tournament?.primaryColor || Style.efuseBlue,
    secondaryColor: tournament?.secondaryColor || Style.efuseDark,
    brandLogoUrl: tournament?.brandLogoUrl,
    backgroundImageUrl: tournament?.backgroundImageUrl,
    hideLeaderboard: tournament?.hideLeaderboard
  };

  const {
    handleSubmit,
    control,
    reset,
    formState: { errors }
  } = useForm({ defaultValues });
  const [updateTournament] = useMutation(UPDATE_TOURNAMENT);

  useEffect(() => {
    reset(defaultValues);
  }, [tournament]);

  const onSubmit = data => {
    analytics.track("ERENA_MANAGE_TOURNAMENT_DESIGN");
    updateTournament({
      variables: {
        tournamentIdOrSlug: tournament._id,
        body: data
      }
    })
      .then(() => {
        sendNotification("Tournament was successfully updated.", "success", "Success");
      })
      .catch(error => {
        dispatch(setErrors({ error: error.message }));
      });
  };

  return (
    <div>
      <InputRow>
        <Col sm={12}>
          <EFSwitchControl
            label={`Hide ${tournament?.bracketType === "bracket" ? "Bracket" : "Leaderboard"}`}
            control={control}
            name="hideLeaderboard"
          />
        </Col>
      </InputRow>
      <InputRow>
        <Col sm={6}>
          <EFColorControl name="primaryColor" label="Primary Color" control={control} />
        </Col>
        <Col sm={6}>
          <EFColorControl name="secondaryColor" label="Secondary Color" control={control} />
        </Col>
      </InputRow>
      <InputRow>
        <Col sm={12}>
          <EFImageUploadControl
            label="Logo Image"
            control={control}
            name="brandLogoUrl"
            errors={errors}
            directory="uploads/erena/"
          />
        </Col>
      </InputRow>
      <InputRow>
        <Col sm={12}>
          <EFImageUploadControl
            label="Background Image"
            control={control}
            name="backgroundImageUrl"
            errors={errors}
            directory="uploads/erena/"
          />
        </Col>
      </InputRow>
      <div className={Style.buttonContainer}>
        <EFRectangleButton buttonType="button" colorTheme="primary" text="Submit" onClick={handleSubmit(onSubmit)} />
      </div>
    </div>
  );
};

export default TournamentDesign;
