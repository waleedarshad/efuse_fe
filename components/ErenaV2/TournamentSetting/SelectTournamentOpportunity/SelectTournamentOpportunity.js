import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useMutation } from "@apollo/client";
import { getOpportunities } from "../../../../store/actions/opportunityActions";
import Style from "./SelectTournamentOpportunity.module.scss";
import PageTitleDetails from "../../PageTitleDetails/PageTitleDetails";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";
import { UPDATE_TOURNAMENT } from "../../../../graphql/ERenaTournamentQuery";
import { sendNotification } from "../../../../helpers/FlashHelper";
import { setErrors } from "../../../../store/actions/errorActions";
import OpportunitySelectModal from "./OpportunitySelectModal/OpportunitySelectModal";

const SelectTournamentOpportunity = ({ tournament, ownerId }) => {
  const dispatch = useDispatch();
  const opportunities = useSelector(state => state.opportunities.opportunities);
  const [updateTournament] = useMutation(UPDATE_TOURNAMENT);

  useEffect(() => {
    getOpportunity();
  }, [JSON.stringify(tournament?.opportunity?._id), opportunities?.length]);

  const getOpportunity = () => {
    dispatch(getOpportunities(1, 20, { opportunityType: "Event" }, true, false, ownerId, "/opportunities/owned"));
  };

  const onChange = (opportunityId, closeModal) => {
    updateTournament({
      variables: {
        tournamentIdOrSlug: tournament._id,
        body: { opportunity: opportunityId }
      }
    })
      .then(() => {
        closeModal();
        sendNotification("Tournament was successfully updated.", "success", "Success");
      })
      .catch(error => {
        dispatch(setErrors({ error: error.message }));
      });
  };

  let opportunityList = opportunities?.map(opp => {
    return { label: `${opp?.title} | ${opp?.opportunityType}`, value: opp?._id };
  });

  opportunityList = [{ label: "Select Opportunity", value: "" }, ...opportunityList];

  return (
    <>
      <PageTitleDetails
        title="Select an Opportunity"
        tooltipText="Selecting an opportunity will provide you the ability to choose players from the list of opportunity applicants."
        selectOpportuntyTitle
      />
      <div className={`${Style.selectOpportunityWrapper} ${!tournament?.opportunity?._id && Style.addMarginBottom}`}>
        <div className={Style.selectBoxWrapper}>
          <OpportunitySelectModal
            selectedOpportunity={tournament?.opportunity?._id || ""}
            opportunitiesList={opportunityList}
            onChange={onChange}
          />
        </div>
        <div className={Style.createOpportunityWrapper}>
          <EFRectangleButton
            text="Create Opportunity"
            colorTheme="primary"
            size="medium"
            shadowTheme="small"
            internalHref="/opportunities/create"
          />
        </div>
      </div>
    </>
  );
};

export default SelectTournamentOpportunity;
