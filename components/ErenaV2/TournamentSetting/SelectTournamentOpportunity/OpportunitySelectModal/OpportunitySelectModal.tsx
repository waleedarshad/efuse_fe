import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck } from "@fortawesome/pro-regular-svg-icons";
import EFPrimaryModal from "../../../../Modals/EFPrimaryModal/EFPrimaryModal";
import InputField from "../../../../InputField/InputField";
import Style from "./OpportunitySelectModal.module.scss";
import EFRectangleButton from "../../../../Buttons/EFRectangleButton/EFRectangleButton";

interface OpportunitySelectModalProps {
  selectedOpportunity: string;
  opportunitiesList: any;
  onChange: (id: string, callback: () => void) => void;
}

const OpportunitySelectModal: React.FC<OpportunitySelectModalProps> = ({
  selectedOpportunity,
  opportunitiesList,
  onChange
}) => {
  const [isModalOpen, toggleModal] = useState(false);
  const [currentOpportunity, setCurrentOpportunity] = useState({ value: "", label: "" });

  useEffect(() => {
    const selectedOption = opportunitiesList.filter(opportunity => opportunity.value === selectedOpportunity);

    if (selectedOption.length) {
      setCurrentOpportunity(selectedOption[0]);
    }
  }, [selectedOpportunity, opportunitiesList]);

  return (
    <>
      <InputField
        name="selectedOpportunity"
        size="sm"
        theme="whiteShadow"
        onClick={() => toggleModal(true)}
        value={currentOpportunity.label}
      />
      <EFPrimaryModal
        title="Select an Opportunity"
        isOpen={isModalOpen}
        onClose={() => toggleModal(false)}
        allowBackgroundClickClose={false}
        displayCloseButton
      >
        <div className={Style.optionsContainer}>
          {opportunitiesList?.map((option, index) => (
            <li
              key={index}
              value={option.value}
              onClick={() => setCurrentOpportunity(option)}
              className={`${Style.option}  ${currentOpportunity.value === option.value ? Style.selected : ""}`}
            >
              {option.label}
              {currentOpportunity.value === option.value && <FontAwesomeIcon icon={faCheck} className={Style.icon} />}
            </li>
          ))}
        </div>
        <div className={Style.buttonContainer}>
          <EFRectangleButton text="Save" onClick={() => onChange(currentOpportunity.value, () => toggleModal(false))} />
        </div>
      </EFPrimaryModal>
    </>
  );
};

export default OpportunitySelectModal;
