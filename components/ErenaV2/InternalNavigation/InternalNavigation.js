import { faGavel, faGlobe, faShare, faCity } from "@fortawesome/pro-solid-svg-icons";

import Modal from "../../Modal/Modal";
import RulesModal from "../ErenaDetails/RulesModal/RulesModal";
import TeamBulkScoring from "../TournamentManagement/TeamBulkScoring/TeamBulkScoring";
import ShareLinkButton from "../../ShareLinkButton";
import EFCircleIconButtonTooltip from "../../Buttons/EFCircleIconButtonTooltip/EFCircleIconButtonTooltip";

const InternalNavigation = tournament => {
  const rightButtons = [
    tournament?.websiteUrl && (
      <EFCircleIconButtonTooltip
        theme="light"
        icon={faGlobe}
        shadowTheme="small"
        externalHref={tournament.websiteUrl}
        externalTarget="_blank"
        tooltipPlacement="bottom"
        size="medium"
        tooltipContent="View Website"
        key={1}
      />
    ),
    <ShareLinkButton
      key={2}
      roundedButton
      buttonText="Share"
      title={`Share ${tournament?.tournamentName}`}
      url={`https://efuse.gg/e/${tournament?.slug}`}
      childComponent={
        <EFCircleIconButtonTooltip
          theme="light"
          icon={faShare}
          shadowTheme="small"
          tooltipPlacement="bottom"
          size="medium"
          tooltipContent="Share Event"
        />
      }
    />
  ];

  if (tournament?.rulesMarkdown && tournament?.rulesMarkdown.length > 0) {
    rightButtons.push(
      <Modal
        component={<RulesModal rules={tournament?.rulesMarkdown} />}
        displayCloseButton
        title={`${tournament?.tournamentName} Rules`}
        key={0}
        textAlignCenter={false}
      >
        <EFCircleIconButtonTooltip
          theme="light"
          icon={faGavel}
          shadowTheme="small"
          size="medium"
          tooltipPlacement="bottom"
          tooltipContent="Event Rules"
        />
      </Modal>
    );
  }

  if (tournament?.organization) {
    rightButtons.push(
      <EFCircleIconButtonTooltip
        theme="light"
        icon={faCity}
        shadowTheme="small"
        tooltipPlacement="bottom"
        tooltipContent="View Organization"
        size="medium"
        internalHref={`/organizations/show?id=${tournament?.organization}`}
      />
    );
  }

  if (tournament?.ownerType === "organizations") {
    rightButtons.push(
      <EFCircleIconButtonTooltip
        theme="light"
        icon={faCity}
        shadowTheme="small"
        tooltipPlacement="bottom"
        tooltipContent="View Organization"
        internalHref={`/organizations/show?id=${tournament?.owner}`}
        size="medium"
        key={3}
      />
    );
  }

  if (tournament?.bracketType === "pointRace") {
    rightButtons.push(<TeamBulkScoring tournament={tournament} />);
  }

  return rightButtons;
};

export default InternalNavigation;
