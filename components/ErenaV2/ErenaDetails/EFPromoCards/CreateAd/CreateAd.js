import React, { useEffect, useState } from "react";
import validator from "validator";
import { useDispatch, useSelector } from "react-redux";
import { Col, Form, FormControl } from "react-bootstrap";
import { useMutation } from "@apollo/client";
import { faTrash } from "@fortawesome/pro-solid-svg-icons";
import Style from "./CreateAd.module.scss";
import EFRectangleButton from "../../../../Buttons/EFRectangleButton/EFRectangleButton";
import InputField from "../../../../InputField/InputField";
import InputLabel from "../../../../InputLabel/InputLabel";
import InputRow from "../../../../InputRow/InputRow";
import { setTournamentSSR } from "../../../../../store/actions/erenaV2/erenaActionsV2";
import UploadImage from "../../../../DirectUpload/UploadImage/UploadImage";
import DisplayMedia from "../../../../DirectUpload/DisplayMedia/DisplayMedia";
import WithUploadContext from "../../../../DirectUpload/UploadContext";
import { DELETE_TOURNAMENT_AD, UPDATE_TOURNAMENT_AD } from "../../../../../graphql/ERenaTournamentQuery";
import { setErrors } from "../../../../../store/actions/errorActions";
import { sendNotification } from "../../../../../helpers/FlashHelper";
import FileUploadButton from "../../../../ImageUploader/FileUploadButton/FileUploadButton";

const ManageTournament = ({ adIndex, editAdImage, editAdUrl, editMode, closeModal }) => {
  const dispatch = useDispatch();
  const [validated, setValidated] = useState(false);
  const [adImage, setAdImage] = useState({});
  const [adLink, setAdLink] = useState("");
  const [adLinkIsValid, setAdLinkIsValid] = useState(true);
  const [updateTournamentAd] = useMutation(UPDATE_TOURNAMENT_AD);
  const [removeTournamentAd] = useMutation(DELETE_TOURNAMENT_AD);

  const tournament = useSelector(state => state.erenaV2.tournament);

  useEffect(() => {
    if (editAdUrl && editAdImage) {
      setAdImage({ url: editAdImage, edit: true });
      setAdLink(editAdUrl);
    }
  }, []);

  const removeAd = event => {
    event.preventDefault();

    removeTournamentAd({
      variables: {
        tournamentIdOrSlug: tournament._id,
        index: adIndex + 1
      }
    })
      .then(response => {
        analytics.track("ERENA_ADS_DELETE");
        sendNotification("Ad successfully removed.", "success", "Success");
        dispatch(setTournamentSSR(response?.data?.removeTournamentAd));
        if (closeModal) {
          closeModal();
        }
      })
      .catch(error => {
        dispatch(setErrors({ error: error.message }));
        if (closeModal) {
          closeModal();
        }
      });
  };

  const onSubmit = event => {
    event.preventDefault();
    if (!validated) {
      setValidated(true);
    }

    if (event.currentTarget.checkValidity()) {
      const adFormData = { link: adLink, imageURL: adImage.url, index: adIndex + 1 };

      if (!validator.isURL(adFormData.link)) {
        setAdLinkIsValid(false);
        return;
      }

      analytics.track("ERENA_ADS_EDIT", {
        adImage,
        adLink
      });

      if (closeModal) {
        closeModal();
      }

      updateTournamentAd({
        variables: {
          tournamentIdOrSlug: tournament._id,
          ...adFormData
        }
      })
        .then(response => {
          sendNotification("Ad successfully updated.", "success", "Success");
          dispatch(setTournamentSSR(response?.data?.updateTournamentAd));
        })
        .catch(error => {
          dispatch(setErrors({ error: error.message }));
        });
    }
  };

  const onDrop = selectedFile => {
    setAdImage(selectedFile);
  };

  let nextButtonInactive = false;
  if (adLink === "" || !adImage?.url) {
    nextButtonInactive = true;
  }

  return (
    <Form noValidate validated={validated} onSubmit={onSubmit}>
      <InputRow>
        <Col sm={12}>
          <InputLabel theme="darkColor">Reference Link</InputLabel>
          <InputField
            name="adLink"
            placeholder="https://efuse.gg"
            theme="whiteShadow"
            size="lg"
            onChange={e => setAdLink(e.target.value)}
            value={adLink}
            maxLength={250}
          />
          {!adLinkIsValid && (
            <FormControl.Feedback type="invalid" className={Style.formFeedback}>
              Please enter a valid reference link.
            </FormControl.Feedback>
          )}
        </Col>
      </InputRow>
      <WithUploadContext>
        <InputRow>
          <Col sm={12}>
            <InputLabel theme="darkColor">Promo Image</InputLabel>
            <DisplayMedia file={adImage} clearUploadedFile={setAdImage} />
            <UploadImage onFileUpload={onDrop} isMediaUploaded={!!adImage?.url} directory="uploads/erena/">
              <FileUploadButton theme="lightButton" />
            </UploadImage>
          </Col>
        </InputRow>
      </WithUploadContext>
      <div className={Style.buttonContainer}>
        {editMode && (
          <EFRectangleButton
            buttonType="submit"
            onClick={e => removeAd(e)}
            disabled={nextButtonInactive}
            colorTheme="transparent"
            shadowTheme="none"
            text="Delete"
            icon={faTrash}
          />
        )}
        <EFRectangleButton buttonType="submit" disabled={nextButtonInactive} colorTheme="primary" text="Submit" />
      </div>
    </Form>
  );
};

export default ManageTournament;
