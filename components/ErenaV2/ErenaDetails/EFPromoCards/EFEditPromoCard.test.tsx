import { render, screen } from "@testing-library/react";
import EFEditPromoCard from "./EFEditPromoCard";
import { ErenaAd } from "../../../../interfaces/erena/erena-ad";

describe("EFEditPromoCard", () => {
  it("renders the correct text", () => {
    const ad: ErenaAd = {
      image: "www.imageurl.com",
      url: "www.efuse.gg"
    };

    render(<EFEditPromoCard ad={ad} index={1} />);

    expect(screen.getByTitle("edit-promo"));
  });
});
