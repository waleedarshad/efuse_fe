import React from "react";
import styled from "styled-components";
import CreateAd from "./CreateAd/CreateAd";
import AddItem from "../../../AddItem/AddItem";
import EFEditPromoCard from "./EFEditPromoCard";
import colors from "../../../../styles/sharedStyledComponents/colors";
import { PromoCardBoxShadow } from "../../../../styles/sharedStyledComponents/sharedStyles";
import { ErenaAd } from "../../../../interfaces/erena/erena-ad";

interface EFPromoCardsProps {
  gridLayout: "single" | "double" | "triple";
  ads: [ErenaAd];
  isOwner: boolean;
  mockExternalErenaEvent: boolean;
}

const promoTheme = {
  single: {
    gridTemplateColumns: "1fr"
  },
  double: {
    gridTemplateColumns: "1fr 1fr"
  },
  triple: {
    gridTemplateColumns: "1fr 1fr 1fr"
  }
};

const EFPromoCards: React.FC<EFPromoCardsProps> = ({ gridLayout = "double", ads, isOwner, mockExternalErenaEvent }) => {
  const getPromoCardType = (ad, index) => {
    if (!mockExternalErenaEvent && isOwner && (!ad?.image || !ad?.url)) {
      return (
        <AddItem
          text="Create Promo"
          modalTitle="Create Promo"
          key={index}
          modalComponent={<CreateAd adIndex={index} />}
        >
          <AdContainer />
        </AddItem>
      );
    }

    if (isOwner && ad?.image) {
      return (
        <AdContainer>
          <AdImage src={ad.image} alt="erena-ad" />
          <EFEditPromoCard index={index} ad={ad} />
        </AdContainer>
      );
    }

    if (!isOwner && ad?.image) {
      return (
        <a href={ad.url} target="_blank" key={index} rel="noreferrer">
          <AdContainer>
            <AdImage src={ad.image} alt="erena-ad" />
          </AdContainer>
        </a>
      );
    }

    return <></>;
  };

  return (
    <PromoCardsListSection gridLayout={gridLayout}>
      {ads.map((ad, index) => {
        return getPromoCardType(ad, index);
      })}
    </PromoCardsListSection>
  );
};

const PromoCardsListSection = styled.div<{ gridLayout: string }>`
  width: 100%;
  display: grid;
  grid-gap: 1em;
  grid-template-columns: ${props => promoTheme[props.gridLayout].gridTemplateColumns};
`;

const AdContainer = styled.div`
  border-radius: 10px;
  background-color: ${colors.white};
  height: 100%;
  overflow: hidden;
  min-height: 100px;
  position: relative;
  border: 1px solid ${colors.outerBorder};
  ${PromoCardBoxShadow}
`;

const AdImage = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

export default EFPromoCards;
