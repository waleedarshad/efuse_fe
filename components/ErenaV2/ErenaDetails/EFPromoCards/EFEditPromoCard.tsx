import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPencilAlt } from "@fortawesome/pro-solid-svg-icons";
import React from "react";
import styled from "styled-components";
import AddItem from "../../../AddItem/AddItem";
import CreateAd from "./CreateAd/CreateAd";
import { ErenaAd } from "../../../../interfaces/erena/erena-ad";
import colors from "../../../../styles/sharedStyledComponents/colors";

interface EFEditPromoCardProps {
  ad: ErenaAd;
  index: number;
}

const EFEditPromoCard: React.FC<EFEditPromoCardProps> = ({ ad, index }) => {
  return (
    <>
      <AddItem
        text="Edit Promo"
        modalTitle="Edit Promo"
        editMode
        modalComponent={<CreateAd adIndex={index} editAdImage={ad.image} editAdUrl={ad.url} editMode />}
      >
        <EditButton>
          <EditIcon icon={faPencilAlt} title="edit-promo" />
          Edit
        </EditButton>
      </AddItem>
    </>
  );
};

const EditButton = styled.div`
  position: absolute;
  cursor: pointer;
  top: 0;
  right: 0;
  background-color: rgba(#fff, 0.8);
  color: ${colors.textStrong};
  margin: 20px;
  font-size: 1rem;
  padding: 5px 20px;
  border-radius: 3px;
  &:hover {
    background-color: rgba(#fff, 1);
  }
`;

const EditIcon = styled(FontAwesomeIcon)`
  margin-right: 10px;
  font-size: 0.8rem;
`;

export default EFEditPromoCard;
