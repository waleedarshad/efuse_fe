import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { withRouter } from "next/router";
import adImages from "./AdImages/AdImages";
import ErenaSeo from "./ErenaSeo/ErenaSeo";
import InternalNavigation from "./InternalNavigation/InternalNavigation";
import Style from "./ErenaV2.module.scss";
import TournamentEventComponent from "./TournamentEventComponent/TournamentEventComponent";
import EFSubHeader from "../Layouts/Internal/EFSubHeader/EFSubHeader";
import EmptyComponent from "../EmptyComponent/EmptyComponent";
import Internal from "../Layouts/Internal/Internal";
import LandingNav from "../Layouts/LandingNav/LandingNav";
import { erenaNavigationListV2 } from "../../navigation/erena";
import { getPostableOrganizations } from "../../store/actions/userActions";
import { setTournamentSSR, setUserRolesSSR, getTeamsPublic } from "../../store/actions/erenaV2/erenaActionsV2";

// TODO: Refactor this so permissions logic works correctly.

const ErenaV2 = ({ router, pageProps }) => {
  const { event: eventSSR, userRoles: userRolesSSR } = pageProps;
  const dispatch = useDispatch();

  const currentUser = useSelector(state => state.auth.currentUser);
  const eventsStreamButton = useSelector(state => state.features.events_stream_button);
  const tournament = useSelector(state => state.erenaV2.tournament);
  const currentUserOrgs = useSelector(state => state.user.postableOrganizations);
  const mockExternalErenaEvent = useSelector(state => state.erenaV2.mockExternalErenaEvent);
  const userRoles = useSelector(state => state.erenaV2.userRoles);

  let isOwner = false;
  let isAdministrator = false;

  useEffect(() => {
    analytics.page("eRena");
    dispatch(setTournamentSSR(eventSSR));
    dispatch(setUserRolesSSR(userRolesSSR));
    dispatch(getTeamsPublic(eventSSR?._id));
  });

  useEffect(() => {
    if (currentUser?.id) {
      dispatch(getPostableOrganizations(1));
    }
  }, [currentUser?.id, currentUserOrgs?.length]);

  const hasAccess = () => {
    return isOwner || isAdministrator;
  };

  const isTournamentOwner = () => {
    return currentUser?.id === eventSSR?.owner;
  };

  const orgOwner = () => {
    return currentUserOrgs.find(org => org?._id === eventSSR?.owner);
  };

  const userLoggedIn = currentUser?.id;
  if (userLoggedIn) {
    isOwner = isTournamentOwner() || orgOwner();

    if (userRoles) {
      isAdministrator = userRoles.find(role => role.user._id === userLoggedIn) !== undefined;
    }
  }

  if (pageProps.bracketData && tournament) {
    tournament.bracket = pageProps.bracketData;
  }

  // set the tournament event data if it exists
  // else, set the inner component to no event
  let actionButtons = <></>;
  let innerComponent = <></>;
  if (tournament) {
    // adImages
    let ads = [];
    ads = adImages(tournament);
    // set buttons for navigation
    actionButtons = InternalNavigation(tournament);
    const leaderboardRoute = router.query.leaderboard;

    innerComponent = (
      <TournamentEventComponent
        currentUser={currentUser}
        tournament={tournament}
        eventsStreamButton={eventsStreamButton}
        leaderboardRoute={leaderboardRoute}
        isOwner={hasAccess}
        mockExternalErenaEvent={mockExternalErenaEvent}
        ads={ads}
        router={router}
      />
    );
  } else {
    innerComponent = (
      <EmptyComponent style={{ display: "block", width: "100%" }} text="No eRena event found for this url." />
    );
  }

  return (
    <>
      <ErenaSeo eventSSR={eventSSR} />
      {!userLoggedIn ? (
        <>
          <LandingNav preventScrollFade />
          {tournament && (
            <EFSubHeader
              headerImage="https://cdn.efuse.gg/static/images/Erena_Logo.png"
              actionButtons={actionButtons}
              customImageStyle={Style.customImageStyle}
            />
          )}

          <div className={Style.internalWrapper}>
            <div className={Style.zIndex}>{innerComponent}</div>
          </div>
        </>
      ) : (
        <Internal metaTitle="eFuse | eRena" isAuthenticated containsSubheader>
          <EFSubHeader
            headerImage="https://cdn.efuse.gg/static/images/Erena_Logo.png"
            navigationList={
              hasAccess && erenaNavigationListV2(router?.query, router?.pathname, tournament?.bracketType === "bracket")
            }
            actionButtons={actionButtons}
            customImageStyle={Style.customImageStyle}
          />
          <div className={Style.zIndex}>{innerComponent}</div>
        </Internal>
      )}
    </>
  );
};

export default withRouter(ErenaV2);
