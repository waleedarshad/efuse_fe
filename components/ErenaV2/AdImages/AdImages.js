const AdImages = tournament => {
  const ads = [];
  // Its done like this to keep the proper order of images when viewing them in editing
  // Otherwise image 6 could be right next to image 1 and therefore you'd assume it was image 2
  /* eslint-disable no-unused-expressions */
  tournament?.imageUrl1 ? ads.push({ image: tournament.imageUrl1, url: tournament?.imageLink1 }) : ads.push({});
  tournament?.imageUrl2 ? ads.push({ image: tournament.imageUrl2, url: tournament?.imageLink2 }) : ads.push({});
  tournament?.imageUrl3 ? ads.push({ image: tournament.imageUrl3, url: tournament?.imageLink3 }) : ads.push({});
  tournament?.imageUrl4 ? ads.push({ image: tournament.imageUrl4, url: tournament?.imageLink4 }) : ads.push({});
  tournament?.imageUrl5 ? ads.push({ image: tournament.imageUrl5, url: tournament?.imageLink5 }) : ads.push({});
  tournament?.imageUrl6 ? ads.push({ image: tournament.imageUrl6, url: tournament?.imageLink6 }) : ads.push({});
  /* eslint-enable no-unused-expressions */
  return ads;
};

export default AdImages;
