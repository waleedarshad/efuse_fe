import { useRouter } from "next/router";
import React from "react";
import DynamicModal from "../DynamicModal/DynamicModal";
import LandingPage from "../Join/LandingPage";

const SignupPage = ({ logo, organizationId }) => {
  const router = useRouter();

  return (
    <>
      <LandingPage />
      <DynamicModal
        flow="LoginSignup"
        openOnLoad
        logo={logo}
        organizationId={organizationId}
        startView="signup"
        customRedirect="/welcome"
        displayCloseButton
        allowBackgroundClickClose={false}
        onCloseModal={() => {
          router.push("/");
        }}
      />
    </>
  );
};

export default SignupPage;
