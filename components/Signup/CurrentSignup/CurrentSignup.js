import React from "react";
import Style from "./CurrentSignup.module.scss";
import LeftContainer from "./LeftContainer/LeftContainer";

const CurrentSignup = ({ form, institutionLogo }) => (
  <div>
    <div className={Style.bgOverlay} />
    <div className={Style.signupImage} />
    <div className={Style.signupContainer}>
      <LeftContainer institutionLogo={institutionLogo} />
      {form}
    </div>
  </div>
);

export default CurrentSignup;
