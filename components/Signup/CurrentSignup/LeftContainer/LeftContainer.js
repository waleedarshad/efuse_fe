import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlug, faChartBar, faSearch } from "@fortawesome/pro-solid-svg-icons";

import Style from "./LeftContainer.module.scss";
import { withCdn } from "../../../../common/utils";

const LeftContainer = ({ institutionLogo }) => {
  return (
    <div className={Style.leftContainer}>
      <img
        className={Style.leftIconBG}
        src="https://d3j2bju5c7tc02.cloudfront.net/2017_45/cropped-dreamstime_s_4545613.jpg"
        alt="Background"
      />
      <div className={Style.leftBody}>
        {institutionLogo ? (
          <div className={Style.institutionLogo}>
            <img className={Style.logo} src={institutionLogo} alt="Institution Logo" />
          </div>
        ) : (
          <div className={Style.leftTitle}>
            <img className={Style.logo} src="https://cdn.efuse.gg/uploads/static/global/efuseLogo.png" alt="Logo" />
            <img className={Style.logoText} src="https://cdn.efuse.gg/uploads/static/global/efuseName.png" alt="Logo" />
          </div>
        )}
        <div className={Style.leftTextContainer}>
          <div className={Style.textItem}>
            <FontAwesomeIcon className={Style.icon} icon={faPlug} />
            <p className={Style.leftText}>Connect with gamers all around the world</p>
          </div>
          <div className={Style.textItem}>
            <FontAwesomeIcon className={Style.icon} icon={faSearch} />
            <p className={Style.leftText}>Discover new scholarships, tournaments, teams and events</p>
          </div>
          <div className={Style.textItem}>
            <FontAwesomeIcon className={Style.icon} icon={faChartBar} />
            <p className={Style.leftText}>Track and share your professional gaming resume</p>
          </div>
          <div className={Style.socialContainer}>
            <p className={Style.followSocials}>FOLLOW OUR SOCIALS</p>
            <a
              className={Style.socialLink}
              href="https://www.linkedin.com/company/efuseofficial/"
              target="_blank"
              rel="noopener noreferrer"
            >
              <img className={Style.socialIcon} src={withCdn("/static/images/linkedin_icon.png")} alt="Linkedin" />
            </a>
            <a
              className={Style.socialLink}
              href="https://twitter.com/efuseofficial?lang=en"
              target="_blank"
              rel="noopener noreferrer"
            >
              <img className={Style.socialIcon} src={withCdn("/static/images/twitter_icon.png")} alt="Twitter" />
            </a>
            <a
              className={Style.socialLink}
              href="https://www.facebook.com/eFuseOfficial/"
              target="_blank"
              rel="noopener noreferrer"
            >
              <img className={Style.socialIcon} src={withCdn("/static/images/facebook_icon.png")} alt="Facebook" />
            </a>
            <a
              className={Style.socialLink}
              href="https://www.instagram.com/efuseofficial/?hl=en"
              target="_blank"
              rel="noopener noreferrer"
            >
              <img className={Style.socialIcon} src={withCdn("/static/images/instagram_icon.png")} alt="Instagram" />
            </a>
            <a
              className={Style.socialLink}
              href="https://www.youtube.com/channel/UCeUQ6DGmB_nV57eVEJePXAQ"
              target="_blank"
              rel="noopener noreferrer"
            >
              <img className={Style.socialIcon} src={withCdn("/static/images/youtube_icon.png")} alt="Youtube" />
            </a>
            <a
              className={Style.socialLink}
              href="https://www.tiktok.com/@efuseofficial?language=en&sec_uid=MS4wLjABAAAAytHYOk2QIDUu31C2Hzsow52OTsrCTo3C3AFZouh47duj67x3IFvdh5btXuFehwBA&u_code=d910f5a3390i3e&utm_campaign=client_share&app=musically&utm_medium=ios&user_id=6750684578179220485&tt_from=copy&utm_source=copy&enter_from=h5_m"
              target="_blank"
              rel="noopener noreferrer"
            >
              <img className={Style.socialIcon} src={withCdn("/static/images/tiktok_icon.png")} alt="Tiktok" />
            </a>
            <p className={Style.official}>@eFuseOfficial</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default LeftContainer;
