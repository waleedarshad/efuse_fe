import capitalize from "lodash/capitalize";
import PropTypes from "prop-types";

import Checkbox from "../../../Checkbox/Checkbox";

const GenderRadio = ({ value, checked, onChange, label, theme }) => (
  <Checkbox
    custom
    name="gender"
    type="radio"
    label={capitalize(label)}
    id={`${value}-radio`}
    value={value}
    onChange={onChange}
    checked={checked}
    theme={theme}
  />
);

GenderRadio.propTypes = {
  value: PropTypes.string.isRequired,
  checked: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired,
  label: PropTypes.string.isRequired
};

export default GenderRadio;
