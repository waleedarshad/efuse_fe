import { Col, Row } from "react-bootstrap";
import GenderRadio from "../GenderRadio/GenderRadio";

const GenderCheckboxes = ({ user, onChange, theme = "external" }) => {
  const genderOptions = [
    {
      label: "Male",
      value: "Male"
    },
    {
      label: "Female",
      value: "Female"
    },
    {
      label: "Other",
      value: "Other"
    }
  ];
  const genderCheckboxes = genderOptions.map((gender, index) => (
    <Col xs={4} key={index}>
      <GenderRadio
        label={gender.label}
        value={gender.value}
        checked={user.gender === gender.value}
        onChange={onChange}
        theme={theme}
      />
    </Col>
  ));
  return <Row>{genderCheckboxes}</Row>;
};

export default GenderCheckboxes;
