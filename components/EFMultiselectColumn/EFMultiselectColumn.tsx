import React, { ReactNode } from "react";

import Styles from "./EFMultiselectColumn.module.scss";
import EFScrollbar from "../EFScrollbar/EFScrollbar";
import EFMultiCheckboxSelect from "../EFMultiCheckboxSelect/EFMultiCheckboxSelect";
import EFLightDropdown from "../Dropdowns/EFLightDropdown/EFLightDropdown";

interface Item {
  title: string;
  imageUrl?: string;
  _id: string;
  isChecked?: boolean;
  disabled?: boolean;
}

interface EFMultiselectColumnProps {
  title: string;
  items: Item[];
  itemTypeSingular?: string;
  itemTypePlural?: string;
  enableHorizontalScroll?: boolean;
  setSelectedItems: (items: Item[]) => void;
  button?: React.ReactNode;
  menuItems?: { option: ReactNode | string; onClick: (event: any) => void }[];
  allowEdit?: boolean;
}

const EFMultiselectColumn = ({
  title,
  items,
  itemTypeSingular = "team",
  itemTypePlural = "teams",
  enableHorizontalScroll = false,
  setSelectedItems,
  button,
  menuItems,
  allowEdit
}: EFMultiselectColumnProps) => {
  return (
    <div className={`${Styles.column} ${enableHorizontalScroll ? Styles.enableHorizontalScroll : ""}`}>
      <div className={Styles.columnContainer}>
        <div className={Styles.headerSection}>
          <div className={Styles.columnTitle}>{title}</div>
          <div className={Styles.subheader}>{`${items.length} ${
            items.length === 1 ? itemTypeSingular : itemTypePlural
          }`}</div>
          {allowEdit && menuItems?.length > 0 && <EFLightDropdown menuItems={menuItems} />}
        </div>
        <EFScrollbar>
          <EFMultiCheckboxSelect inputs={items} onSelect={setSelectedItems} />
        </EFScrollbar>
        {button}
      </div>
    </div>
  );
};

export default EFMultiselectColumn;
