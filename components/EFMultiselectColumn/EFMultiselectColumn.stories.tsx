import React from "react";
import EFMultiselectColumn from "./EFMultiselectColumn";

export default {
  title: "Layouts/EFMultiselectColumn",
  component: EFMultiselectColumn,
  argTypes: {
    title: {
      control: {
        type: "text"
      }
    },
    itemTypeSingular: {
      control: {
        type: "text"
      }
    },
    itemTypePlural: {
      control: {
        type: "text"
      }
    },
    items: {
      control: {
        type: "array"
      }
    }
  }
};

const Story = args => <EFMultiselectColumn {...args} />;

const testItems = [
  {
    _id: "612e33823f9a2718149dd00e",
    name: "Team Charmander",
    image: {
      url: "https://cdn2.bulbagarden.net/upload/thumb/2/21/001Bulbasaur.png/250px-001Bulbasaur.png"
    }
  },
  {
    _id: "612e338d3f9a2718149dd020",
    name: "Team Bulbasaur",
    image: {
      url: "https://ranktopten.com/upload/dt/vm/charmander_400x400.png"
    }
  },
  {
    _id: "612e33ac3f9a2718149dd032",
    name: "Team Squirtle",
    image: {
      url: "https://efuse.gg/static/images/no_image.jpg"
    }
  }
];

export const Default = Story.bind({});
Default.args = {
  title: "Column Title",
  itemTypeSingular: "team",
  itemTypePlural: "teams",
  items: testItems
};
