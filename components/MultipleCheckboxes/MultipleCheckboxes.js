import React from "react";
import Checkbox from "../Checkbox/Checkbox";

export default function MultipleCheckboxes({ options, values, onChange, name }) {
  return (
    <div>
      {options.map((option, index) => (
        <Checkbox
          type="checkbox"
          name={name}
          label={option.label}
          id={`${name}-checkbox-${index}`}
          theme="internal"
          checked={values.filter(value => value === option.value).length === 1}
          value={option.value}
          onChange={onChange}
        />
      ))}
    </div>
  );
}
