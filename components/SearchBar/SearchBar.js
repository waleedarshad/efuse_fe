import React, { Component } from "react";
import PropTypes from "prop-types";

import SearchField from "../SearchField/SearchField";
import { applyFilters } from "../../helpers/FilterHelper";

class SearchBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      "$text.$search": ""
    };
  }

  onChange = event => {
    this.setState({ "$text.$search": event.target.value });
  };

  onSearch = event => {
    if (event.key === "Enter") {
      const value = event.target.value.trim();
      if (value.length > 0) {
        applyFilters(
          "$text.$search",
          value,
          value,
          this.props.filtersApplied,
          this.props.searchCallback,
          false,
          this.props.id
        );
        this.setState({ "$text.$search": "" });
      }
    }
  };

  render() {
    const value = this.state["$text.$search"];
    return <SearchField placeholder="Search" onKeyPress={this.onSearch} onChange={this.onChange} value={value} />;
  }
}

SearchBar.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  filtersApplied: PropTypes.array.isRequired,
  searchCallback: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired
};

export default SearchBar;
