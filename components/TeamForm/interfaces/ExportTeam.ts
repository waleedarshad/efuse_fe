export interface ExportTeam {
  _id: string;
  name: string;
  game: string;
}
