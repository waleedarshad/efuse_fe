import { ExportTeam } from "./ExportTeam";
import { Team } from "./Team";

export interface TeamFormProperties {
  team: Team;
  onClose: () => void;
  onCreateTeam: (item: ExportTeam) => ExportTeam;
  onAddMember: (item: ExportTeam) => ExportTeam;
}
