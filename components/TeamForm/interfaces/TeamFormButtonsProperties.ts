export interface TeamFormButtonsProperties {
  teamId: string;
  onCloseModal: () => void;
  viewAddMembers: (clickEvent: any) => any;
}
