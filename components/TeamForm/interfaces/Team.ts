/**
 *  This is not the full team object. Just what is currently needed for form.
 */
export interface Team {
  _id: string;
  name: string;
  game: {
    _id: string;
    name: string;
  };
}
