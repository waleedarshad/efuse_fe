import React, { useEffect, useState } from "react";
import { Form } from "react-bootstrap";
import Style from "./TeamForm.module.scss";
import InputRow from "../InputRow/InputRow";
import InputLabel from "../InputLabel/InputLabel";
import InputField from "../InputField/InputField";
import EFGameSelect from "../EFGameSelect/EFGameSelect";
import TeamFormButtons from "./TeamFormButtons/TeamFormButtons";
import { TeamFormProperties } from "./interfaces/TeamFormProperties";

const TeamForm: React.FC<TeamFormProperties> = ({ onAddMember, onClose, onCreateTeam, team }) => {
  const [adjustedTeam, setTeam] = useState({
    _id: team?._id || null,
    name: team?.name || null,
    game: team?.game?._id || null
  });

  const [gameIdValid, setGameIdValid] = useState(true);
  const [validated, setValidated] = useState(false);

  // check if team game is valid after the user starts selecting or tries submitting values
  useEffect(() => {
    if (validated) {
      setGameIdValid(adjustedTeam.game != null && adjustedTeam.game.length > 0);
    }
  }, [adjustedTeam.game, validated]);

  const createTeamSubmit = event => {
    setValidated(true);
    event.preventDefault();
    if (event.currentTarget.checkValidity() && hasAllRequiredFields()) {
      onCreateTeam(adjustedTeam);
    }
  };

  const addMembersSubmit = event => {
    if (!adjustedTeam._id) {
      setValidated(true);
      if (event.currentTarget.checkValidity() && hasAllRequiredFields()) {
        onAddMember(adjustedTeam);
      }
    }
  };

  const hasAllRequiredFields = () => {
    return adjustedTeam.name && adjustedTeam.name.length > 0 && adjustedTeam.game && adjustedTeam.game.length > 0;
  };

  const onNameChange = e => {
    if (e) {
      const updatedName = e.target.value;
      setTeam({ ...adjustedTeam, name: updatedName });
    }
  };

  const onGameSelect = games => {
    setTeam({
      ...adjustedTeam,
      game: games[0]?._id || ""
    });
  };

  const closeModal = () => {
    if (team?._id) {
      if (onClose) {
        onClose();
      }
    }
  };

  return (
    <Form className={Style.formHeight} noValidate validated={validated} role="form" onSubmit={createTeamSubmit}>
      <div>
        <InputRow>
          <InputLabel theme="internal">
            Team Name
            <span className={Style.requiredAsterisk}>*</span>
          </InputLabel>
          <InputField
            theme="internal"
            onChange={e => onNameChange(e)}
            placeholder="Enter Team Name"
            value={adjustedTeam?.name || ""}
            maxLength={70}
            required
            errorMessage="Team Name is required"
          />
        </InputRow>
        <div>
          <div className={Style.gameSelectText}>
            Select Team Game
            <span className={Style.requiredAsterisk}>*</span>
          </div>
          <div className={Style.gameSelect}>
            <EFGameSelect
              horizontalScroll
              onSelect={games => onGameSelect(games)}
              selectMultiple={false} // Teams currently only support one game per team
              preselectedGameIds={adjustedTeam?.game}
            />
            {!gameIdValid && <div className={Style.error}>A Game is required</div>}
          </div>
        </div>
        <TeamFormButtons teamId={team?._id} onCloseModal={closeModal} viewAddMembers={addMembersSubmit} />
      </div>
    </Form>
  );
};

export default TeamForm;
