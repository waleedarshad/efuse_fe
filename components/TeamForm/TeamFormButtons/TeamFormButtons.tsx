import React, { useEffect, useState } from "react";
import Style from "./TeamFormButtons.module.scss";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";
import { TeamFormButtonsProperties } from "../interfaces/TeamFormButtonsProperties";

const TeamFormButtons: React.FC<TeamFormButtonsProperties> = ({ teamId, onCloseModal, viewAddMembers }) => {
  const [submitButtonText, setSubmitButtonText] = useState("Create Team");
  const [memberButtonText, setMemberButtonText] = useState("Add Members");
  const [submitButtonType, setSubmitButtonType] = useState("submit");
  const [memberButtonType, setMemberButtonType] = useState("button");

  useEffect(() => {
    if (teamId) {
      setSubmitButtonText("Cancel");
      setMemberButtonText("Save");
      setSubmitButtonType("button");
      setMemberButtonType("submit");
    }
  }, [teamId]);

  return (
    <div className={Style.buttonWrapper}>
      <div className={Style.leftButton}>
        <EFRectangleButton
          colorTheme="white"
          shadowTheme="none"
          width="fullWidth"
          buttonType={submitButtonType}
          text={submitButtonText}
          onClick={() => onCloseModal()}
        />
      </div>
      <div className={Style.rightButton}>
        <EFRectangleButton
          colorTheme="azul"
          shadowTheme="none"
          width="fullWidth"
          buttonType={memberButtonType}
          onClick={e => viewAddMembers(e)}
          text={memberButtonText}
        />
      </div>
    </div>
  );
};

export default TeamFormButtons;
