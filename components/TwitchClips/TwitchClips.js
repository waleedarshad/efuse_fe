import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTwitch } from "@fortawesome/free-brands-svg-icons";
import React, { Component } from "react";
import { connect } from "react-redux";
import { faEye } from "@fortawesome/pro-solid-svg-icons";

import { withCdn } from "../../common/utils";
import { getTwitchClips } from "../../store/actions/userActions";
import EFPaginationBar from "../EFPaginationBar/EFPaginationBar";
import Style from "./TwitchClips.module.scss";

class TwitchClips extends Component {
  componentDidMount() {
    if (this.props.currentUser?.id) {
      this.getTwitchClips();
    }
  }

  getTwitchClips = () => {
    this.props.getTwitchClips(this.props.currentUser.id, 1, 6);
  };

  clickPaginatedPage = page => this.props.getTwitchClips(this.props.currentUser.id, page, 6);

  render() {
    const { twitchClips, onClick } = this.props;

    if (!twitchClips?.docs?.length) {
      return (
        <div className={Style.noClipsFoundContainer}>
          <div className={Style.middleContainer}>
            <FontAwesomeIcon icon={faTwitch} className={Style.icon} />
            <h4>It looks like you don&rsquo;t have any clips</h4>
          </div>
        </div>
      );
    }

    return (
      <>
        <div className={Style.twitchClipsContainer}>
          {twitchClips.docs.map((clip, index) => {
            return (
              <div className={Style.clipContainer} key={clip._id} onClick={() => onClick(clip)}>
                {/* dynamically create a new reference to each twitch clip and if the twitch clip 403s display a backup image */}
                <img
                  className={Style.clipImage}
                  src={clip.thumbnail_url}
                  ref={img => (this[`${index}_ref`] = img)}
                  onError={() => (this[`${index}_ref`].src = withCdn("/static/images/image-not-found.png"))}
                />
                <div className={Style.clipText}>
                  <p className={Style.clipTitle}>{clip.title}</p>
                  <div className={Style.subTitleFlex}>
                    <p className={Style.subTitle}>{clip.creator_name}</p>
                    <p className={Style.clipViews}>
                      <FontAwesomeIcon className={Style.eyeIcon} icon={faEye} title="views" />
                      {clip.view_count}
                    </p>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
        <div className={Style.paginationWrapper}>
          <EFPaginationBar
            totalPages={twitchClips.totalPages}
            onPageChange={this.clickPaginatedPage}
            marginPages={1}
            pageRange={2}
          />
        </div>
      </>
    );
  }
}

const mapStateToProps = state => ({
  currentUser: state.auth.currentUser,
  twitchClips: state.user.twitchClips
});

export default connect(mapStateToProps, { getTwitchClips })(TwitchClips);
