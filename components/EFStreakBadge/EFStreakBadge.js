import React from "react";
import { faBolt } from "@fortawesome/pro-duotone-svg-icons";
import EFPostBadge from "../EFPostBadge/EFPostBadge";

const EFStreakBadge = ({ streak, size }) => {
  return <EFPostBadge icon={faBolt} colorTheme={streak > 0 ? "purple" : "grey"} size={size} text={streak} />;
};

EFStreakBadge.defaultProps = {
  size: "small"
};

export default EFStreakBadge;
