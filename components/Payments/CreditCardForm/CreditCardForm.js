import React from "react";
import { ElementsConsumer, CardElement } from "@stripe/react-stripe-js";
import { connect } from "react-redux";
import CardSection from "../CardSection/CardSection";
import { getPaymentMethods } from "../../../store/actions/userActions";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";
import Style from "./CreditCardForm.module.scss";
import withStripe from "../../hoc/withStripe";

class CreditCardForm extends React.Component {
  handleSubmit = async (event, setupIntent) => {
    // We don't want to let default form submission happen here,
    // which would refresh the page.
    event.preventDefault();

    const { stripe, elements, onSubmit } = this.props;

    if (!stripe || !elements) {
      // Stripe.js has not yet loaded.
      // Make  sure to disable form submission until Stripe.js has loaded.
      return;
    }

    const result = await stripe.confirmCardSetup(setupIntent, {
      payment_method: {
        card: elements.getElement(CardElement),
        billing_details: {}
      }
    });

    if (result.error) {
      // Display result.error.message in your UI.
    } else {
      this.props.getPaymentMethods();
      if (onSubmit) onSubmit();
    }
  };

  render() {
    const { setupIntent, onCancel } = this.props;

    return (
      <div>
        <label className={Style.formLabel}>Add New Card</label>
        <CardSection />

        {onCancel && <EFRectangleButton text="Cancel" disabled={!this.props.stripe} onClick={e => onCancel(e)} />}
        <div className={Style.saveButton}>
          <EFRectangleButton
            text="Save Card"
            disabled={!this.props.stripe}
            onClick={e => this.handleSubmit(e, setupIntent)}
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({});

const InjectedCardSetupForm = props => {
  return (
    <ElementsConsumer>
      {({ stripe, elements }) => (
        <CreditCardForm
          stripe={stripe}
          elements={elements}
          onCancel={props.onCancel}
          onSubmit={props.onSubmit}
          setupIntent={props.setupIntent}
          closeModal={props.closeModal}
          getPaymentMethods={props.getPaymentMethods}
        />
      )}
    </ElementsConsumer>
  );
};

export default connect(mapStateToProps, { getPaymentMethods })(withStripe(InjectedCardSetupForm));
