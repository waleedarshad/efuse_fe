import PropTypes from "prop-types";

import Style from "./Switch.module.scss";

const Switch = ({ checked, value, name, label, onChange, disabled }) => {
  return (
    <div className={Style.switchWrapper}>
      {label && <span className={Style.switchLabel}>{label}</span>}
      <label className={Style.switch}>
        <input
          type="checkbox"
          value={value}
          name={name}
          onChange={onChange}
          checked={checked}
          id={name}
          disabled={disabled}
        />
        <span
          className={`${Style.slider} ${checked ? Style.onSwitchStyle : Style.offSwitchStyle} ${disabled &&
            Style.disabled}`}
        />
      </label>
    </div>
  );
};
Switch.propTypes = {
  checked: PropTypes.bool,
  value: PropTypes.bool,
  name: PropTypes.string.isRequired,
  label: PropTypes.string,
  onChange: PropTypes.func,
  disabled: PropTypes.bool,
  sliderAddedStyle: PropTypes.string
};

Switch.defaultProps = {
  sliderAddedStyle: ""
};

export default Switch;
