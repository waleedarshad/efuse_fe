import React from "react";
import Style from "./EFDynamicLink.module.scss";

const EFDynamicLink = ({ text, linkText, onClick }) => {
  return (
    <p className={Style.switchButton}>
      {text}
      <span className={Style.switch} onClick={onClick}>
        {linkText}
      </span>
    </p>
  );
};

export default EFDynamicLink;
