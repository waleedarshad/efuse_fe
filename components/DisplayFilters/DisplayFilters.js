import React, { Component } from "react";
import { Card } from "react-bootstrap";
import PropTypes from "prop-types";

import Style from "./DisplayFilters.module.scss";
import { applyFilters } from "../../helpers/FilterHelper";
import Tag from "../Tag/Tag";
import EFRectangleButton from "../Buttons/EFRectangleButton/EFRectangleButton";

class DisplayFilters extends Component {
  clearFilters = () => {
    const { id } = this.props;
    id ? this.props.clearFilters(id) : this.props.clearFilters();
  };

  removeFilter = filter => {
    applyFilters(
      filter.key,
      filter.value,
      filter.label,
      this.props.filtersApplied,
      this.props.searchCallback,
      true,
      this.props.id
    );
    if (
      filter.key.split(".").length === 1 ||
      filter.key.includes("verified.status") ||
      filter.key.includes("$regex") ||
      filter.key.includes("$in")
    )
      this.props.removeFilter(filter.key);
  };

  render() {
    const { filtersApplied } = this.props;
    const displayFiltersApplied = (
      <Card.Body>
        {filtersApplied.map((filter, index) => {
          return <Tag key={index} title={filter.label} onClose={() => this.removeFilter(filter)} />;
        })}
        <div className={Style.clearFilters}>
          <EFRectangleButton
            text="Clear Filters"
            colorTheme="transparent"
            shadowTheme="none"
            onClick={this.clearFilters}
          />
        </div>
      </Card.Body>
    );
    return <>{displayFiltersApplied}</>;
  }
}

DisplayFilters.propTypes = {
  filtersApplied: PropTypes.array,
  clearFilters: PropTypes.func.isRequired,
  removeFilter: PropTypes.func.isRequired,
  searchCallback: PropTypes.func.isRequired,
  id: PropTypes.string
};

export default DisplayFilters;
