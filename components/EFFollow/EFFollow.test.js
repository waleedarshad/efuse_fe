import { mountWithStore } from "../../common/testUtils";

import EFFollow from "./EFFollow";

describe("EFFollow", () => {
  let state;
  beforeEach(() => {
    state = {
      auth: {
        currentUser: {
          id: "me",
          firstName: "test",
          lastName: "test"
        }
      },
      user: {
        currentUser: {
          id: "me",
          firstName: "test",
          lastName: "test"
        }
      },
      followers: {
        disabled: false
      }
    };
  });

  it("renders a Follow Button", () => {
    const { subject } = mountWithStore(<EFFollow renderAs="followees" id="user._id" isFollowed />, state);
    expect(subject.find("button")).toHaveLength(1);
  });

  it("renders a unfollow Button if Followed true", () => {
    const { subject } = mountWithStore(<EFFollow renderAs="followers" id="user._id" isFollowed={false} />, state);
    expect(subject.find("button").text()).toContain("Follow");
  });

  it("renders a follow Button if Followed false", () => {
    const { subject } = mountWithStore(<EFFollow renderAs="followees" id="user._id" isFollowed />, state);
    expect(subject.find("button").text()).toContain("Following");
  });
});
