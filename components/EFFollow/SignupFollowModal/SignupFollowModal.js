import React from "react";

import SignupModal from "../../SignupModal/SignupModal";

import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";

const SignupFollowModal = ({ user, disable }) => (
  <SignupModal
    title={`Create an account to follow ${user.name}.`}
    message={`Never miss an update from ${user.name}. Sign up today!`}
  >
    <EFRectangleButton disabled={disable} text="Follow" />
  </SignupModal>
);
export default SignupFollowModal;
