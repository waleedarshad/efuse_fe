import React from "react";

import { useSelector, useDispatch } from "react-redux";
import PropTypes from "prop-types";

import EFRectangleButton from "../Buttons/EFRectangleButton/EFRectangleButton";
import { follow, unfollow } from "../../store/actions/followerActions";
import SignupFollowModal from "./SignupFollowModal/SignupFollowModal";

const EFFollow = ({ id, renderAs, unfollowOveride, followOveride, isFollowed }) => {
  const currentUser = useSelector(state => state.auth.currentUser);
  const user = useSelector(state => state.user.currentUser);
  const disable = useSelector(state => state.followers.disable);
  const dispatch = useDispatch();

  const followUser = () => {
    dispatch(follow(id, true, renderAs));
  };

  const unfollowUser = () => {
    dispatch(unfollow(id, true, currentUser.id, renderAs));
  };

  let action = followOveride || followUser;
  if (isFollowed) action = unfollowOveride || unfollowUser;

  const userLoggedIn = currentUser?.id;
  const text = isFollowed ? "Following" : "Follow";

  if (id === undefined || (currentUser && currentUser.id === id)) return <></>;
  if (!userLoggedIn) return <SignupFollowModal user={user} disable={disable} />;

  return <EFRectangleButton onClick={action} disabled={disable} text={text} />;
};

export default EFFollow;

EFFollow.propTypes = {
  id: PropTypes.string
};
