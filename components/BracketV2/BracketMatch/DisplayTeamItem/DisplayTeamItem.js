import React from "react";
import Style from "../BracketMatch.module.scss";

const DisplayTeamItem = ({ team, checkTeamWinner, roundNumber, score }) => {
  return (
    <div
      className={`${Style.team} ${checkTeamWinner(team?._id) === "winner" && Style.teamWinner} ${checkTeamWinner(
        team?._id
      ) === "loser" && Style.teamLoser} ${!team?.name && roundNumber !== 0 && Style.emptyTeam}`}
      key={team?._id}
    >
      {/* //update to team name */}
      <span className={Style.teamName}>{team?.name}</span>
      {team?._id && <span className={Style.score}>{score}</span>}
    </div>
  );
};

export default DisplayTeamItem;
