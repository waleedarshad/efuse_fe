import React, { useState, useEffect } from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit } from "@fortawesome/pro-solid-svg-icons";
import Style from "./BracketMatch.module.scss";
import BracketMatchModal from "../BracketMatchModal/BracketMatchModal";
import DisplayTeamItem from "./DisplayTeamItem/DisplayTeamItem";

const BracketMatch = ({ tournament, match, editMatch, tournamentId, roundNumber, finalRound, fromEventMainPage }) => {
  const erenaScores = tournament?.scores || [];
  const erenaTeams = tournament?.teams || [];
  // TODO suggest we build a more dynamic way to handle multiple teams in brackets
  // right now team one and two are very static
  const [teamOne, setTeamOne] = useState();
  const [teamTwo, setTeamTwo] = useState();
  const [teamOneScore, setTeamOneScore] = useState(0);
  const [teamTwoScore, setTeamTwoScore] = useState(0);

  useEffect(() => {
    // check if team one and two have been set by the user for the match and set the value to team id
    const teamOneId = match?.teams[0]?._id || match?.teams[0];
    const teamTwoId = match?.teams[1]?._id || match?.teams[1];

    // grab the team from the state to display name values
    // use this to pass the team from the match to find the team in the redux state
    setTeamOne(erenaTeams.find(x => x._id === teamOneId));
    setTeamTwo(erenaTeams.find(x => x._id === teamTwoId));
    // set team scores from redux
    setTeamOneScore(erenaScores.find(x => x.match._id === match?._id && x.ownerId === teamOneId) || { score: 0 });
    setTeamTwoScore(erenaScores.find(x => x.match._id === match?._id && x.ownerId === teamTwoId) || { score: 0 });

    if (fromEventMainPage && match) {
      setTeamOne(match?.teams[0]);
      setTeamTwo(match?.teams[1]);
      setTeamOneScore(match?.teams[0]);
      setTeamTwoScore(match?.teams[1]);
    }
  }, [match?.teams[0], match?.teams[1], erenaScores]);

  // check if the team being displayed is winner or loser of the match
  const checkTeamWinner = teamId => {
    const winner = match?.winner?._id || match?.winner;
    if (!winner) {
      return "none";
    }
    if (teamId === winner) {
      return "winner";
    }
    return "loser";
  };

  return (
    <div className={Style.match}>
      <div className={Style.positionEditButton}>
        {((editMatch && teamOne?._id && teamTwo?._id) || (editMatch && roundNumber === 0)) && (
          <BracketMatchModal
            match={match}
            tournamentId={tournamentId}
            roundNumber={roundNumber}
            teamOne={teamOne}
            teamTwo={teamTwo}
            teamOneScore={teamOneScore}
            teamTwoScore={teamTwoScore}
            tournament={tournament}
          >
            <div className={Style.editMatchOverlay}>
              <FontAwesomeIcon icon={faEdit} className={Style.editIcon} />
              Edit Match
            </div>
          </BracketMatchModal>
        )}
      </div>

      <DisplayTeamItem
        team={teamOne}
        score={teamOneScore?.score || 0}
        checkTeamWinner={checkTeamWinner}
        rounderNumber={roundNumber}
      />
      {!finalRound && (
        <DisplayTeamItem
          team={teamTwo}
          score={teamTwoScore?.score || 0}
          checkTeamWinner={checkTeamWinner}
          rounderNumber={roundNumber}
        />
      )}
    </div>
  );
};

export default BracketMatch;
