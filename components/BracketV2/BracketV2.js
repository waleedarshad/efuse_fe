import React from "react";

import ScrollContainer from "react-indiana-drag-scroll";
import BracketMatch from "./BracketMatch/BracketMatch";
import Style from "./BracketV2.module.scss";

const BracketV2 = ({ tournament, editMatch, fromEventMainPage }) => {
  const bracket = tournament?.bracket;
  const allMatches = tournament?.matches || [];

  // retrieve the match from the redux state (this allows easier editing of redux instead of trying to find the correct position in the bracket object)
  const getMatch = matchId => {
    const match = allMatches.find(x => x._id === matchId);
    return match;
  };

  return (
    <ScrollContainer className={Style.bracketWrapper}>
      <section className={Style.bracketContainer}>
        <div
          className={Style.headerColor}
          style={{ backgroundColor: tournament?.primaryColor || Style.primaryLeaderboardColor }}
        />
        <div
          className={Style.bracketBackground}
          style={{
            backgroundImage: `${`linear-gradient(${tournament?.secondaryColor ||
              Style.secondaryLeaderboardColor}, rgba(255, 255, 255, 0))`}`
          }}
        />
        {bracket?.rounds?.map((round, index) => {
          return (
            <div className={Style.roundWrapper} key={round?._id}>
              <div className={Style.roundTitle}>{round?.roundTitle}</div>
              <div className={Style.round}>
                {round?.matches?.map(match => {
                  return (
                    <BracketMatch
                      match={fromEventMainPage ? match : getMatch(match?._id)}
                      editMatch={editMatch}
                      tournamentId={tournament?._id}
                      roundNumber={index}
                      finalRound={bracket?.rounds.length === index + 1}
                      key={match?._id}
                      tournament={tournament}
                      fromEventMainPage={fromEventMainPage}
                    />
                  );
                })}
              </div>
            </div>
          );
        })}
      </section>
    </ScrollContainer>
  );
};

export default BracketV2;
