import React from "react";
import Modal from "../../Modal/Modal";
import UpdateMatch from "../UpdateMatch/UpdateMatch";

const BracketMatchModal = ({
  children,
  match,
  tournamentId,
  roundNumber,
  teamOne,
  teamTwo,
  teamOneScore,
  teamTwoScore,
  tournament
}) => {
  const component = (
    <UpdateMatch
      match={match}
      tournamentId={tournamentId}
      roundNumber={roundNumber}
      teamOne={teamOne}
      teamTwo={teamTwo}
      teamOneScore={teamOneScore}
      teamTwoScore={teamTwoScore}
      tournament={tournament}
    />
  );
  return (
    <Modal
      displayCloseButton
      allowBackgroundClickClose={false}
      openOnLoad={false}
      component={component}
      textAlignCenter={false}
      customMaxWidth="800px"
      title="Edit Match"
    >
      {children}
    </Modal>
  );
};

export default BracketMatchModal;
