import React, { useEffect } from "react";
import { useMutation } from "@apollo/client";
import { useForm, Controller } from "react-hook-form";
import Style from "./UpdateMatch.module.scss";
import { sendNotification } from "../../../helpers/FlashHelper";
import MatchTeamCard from "./MatchTeamCard/MatchTeamCard";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";
import { SAVE_SCREENSHOT, UPDATE_MATCH } from "../../../graphql/eRena/eRenaMatch";
import { ADD_ERENA_SCORE, UPDATE_ERENA_SCORE } from "../../../graphql/eRena/eRenaScore";
import {
  addScoreToCache,
  updateScoreInCache
} from "../../ErenaV2/TournamentManagement/TournamentManagementCacheActions/TeamScoringCacheActions";

const UpdateMatch = ({ match, tournament, roundNumber, teamOne, teamTwo, teamOneScore, teamTwoScore }) => {
  const [updateMatch] = useMutation(UPDATE_MATCH);
  const [createScore] = useMutation(ADD_ERENA_SCORE, { update: addScoreToCache });
  const [updateScore] = useMutation(UPDATE_ERENA_SCORE, { update: updateScoreInCache });
  const [updateScreenshot] = useMutation(SAVE_SCREENSHOT);

  const defaultValues = {
    winner: match?.winner?._id,
    teamOne: {
      _id: teamOne?._id,
      score: teamOneScore?.score,
      image: match?.teamOneImage
    },
    teamTwo: {
      _id: teamTwo?._id,
      score: teamTwoScore?.score,
      image: match?.teamTwoImage
    }
  };
  const {
    control,
    watch,
    handleSubmit,
    reset,
    formState: { errors }
  } = useForm({ defaultValues });

  useEffect(() => {
    reset(defaultValues);
  }, [teamOneScore, teamTwoScore, teamOne, teamTwo, match]);

  const onSubmit = async data => {
    await updateMatch({
      variables: {
        id: match?._id,
        body: {
          winner: data.winner,
          teams: [{ team: data.teamOne._id }, { team: data.teamTwo._id }]
        }
      }
    });

    await createOrUpdateScore(data.teamOne.score, teamOneScore, data.teamOne._id);
    await createOrUpdateScore(data.teamTwo.score, teamTwoScore, data.teamTwo._id);

    if (data.teamOne.image?.url && data.teamOne.image?.url !== match?.teamOneImage?.url) {
      await updateScreenshot({ variables: { id: match?._id, body: { teamOneImage: data.teamOne.image } } });
    }

    if (data.teamTwo.image?.url && data.teamTwo.image?.url !== match?.teamTwoImage?.url) {
      await updateScreenshot({ variables: { id: match?._id, body: { teamTwoImage: data.teamTwo.image } } });
    }

    sendNotification("Match Updated", "success", "Success");
  };

  const createOrUpdateScore = async (newScore, existingScore, teamId) => {
    if (newScore !== existingScore?.score) {
      if (existingScore?._id) {
        await updateScore({ variables: { id: existingScore?._id, body: { score: Number(newScore) || 0 } } });
      } else {
        const createData = {
          ownerId: teamId,
          ownerKind: "team",
          score: Number(newScore) || 0,
          match: match._id
        };
        await createScore({ variables: { tournamentIdOrSlug: tournament?._id, body: createData } });
      }
    }
  };

  return (
    <>
      <div className={Style.marginItem}>
        <Controller
          control={control}
          name="winner"
          render={({ field: { value: winner, onChange: setWinner } }) => (
            <>
              <MatchTeamCard
                control={control}
                errors={errors}
                watch={watch}
                tournament={tournament}
                roundNumber={roundNumber}
                winner={winner}
                setWinner={setWinner}
                team="teamOne"
                otherTeam="teamTwo"
              />
              <div className={Style.vsContainer}>
                <i>VS</i>
              </div>
              <MatchTeamCard
                errors={errors}
                control={control}
                watch={watch}
                tournament={tournament}
                roundNumber={roundNumber}
                winner={winner}
                setWinner={setWinner}
                team="teamTwo"
                otherTeam="teamOne"
              />
            </>
          )}
        />
      </div>
      <div className={Style.buttonWrapper}>
        <EFRectangleButton text="Save" onClick={handleSubmit(onSubmit)} />
      </div>
    </>
  );
};

export default UpdateMatch;
