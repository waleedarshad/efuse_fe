import React from "react";
import { faTrophy } from "@fortawesome/pro-regular-svg-icons";
import { faCamera } from "@fortawesome/pro-solid-svg-icons";
import Style from "./MatchTeamCard.module.scss";
import EFCard from "../../../Cards/EFCard/EFCard";
import EFCircleIconButtonTooltip from "../../../Buttons/EFCircleIconButtonTooltip/EFCircleIconButtonTooltip";
import EFImage from "../../../EFImage/EFImage";
import EFSelectControl from "../../../FormControls/EFSelectControl";
import EFInputControl from "../../../FormControls/EFInputControl";
import EFImageUploadButtonControl from "../../../FormControls/uploadControls/EFImageUploadButtonControl";

const MatchTeamCard = ({ team, otherTeam, winner, setWinner, roundNumber, tournament, control, errors, watch }) => {
  const teamId = watch(`${team}._id`);
  const otherTeamId = watch(`${otherTeam}._id`);
  const image = watch(`${team}.image`);

  const teamOptions = [
    ...[{ label: "Select a Team...", value: "" }],
    ...(tournament?.teams || [])?.filter(it => it?._id !== otherTeamId).map(it => ({ label: it?.name, value: it?._id }))
  ];

  const imageExists = image?.url && !image?.url?.includes("no_image.jpg");

  return (
    <EFCard widthTheme="fullWidth">
      <div className={Style.teamContainer}>
        {team?.name && roundNumber !== 0 && <span className={Style.teamName}>{team?.name}</span>}
        {roundNumber === 0 && (
          <div className={Style.selectBoxWrapper}>
            <EFSelectControl control={control} errors={errors} name={`${team}._id`} options={teamOptions} required />
          </div>
        )}

        <div className={Style.inputFieldContainer}>
          <EFInputControl type="number" name={`${team}.score`} control={control} placeholder={0} errors={errors} />
        </div>

        <EFImageUploadButtonControl control={control} name={`${team}.image`} directory="uploads/matches/">
          <div className={Style.cameraButtonWrapper}>
            <EFCircleIconButtonTooltip
              tooltipContent="Upload an Image for the Match"
              icon={faCamera}
              size="small"
              shadowTheme="small"
              colorTheme={imageExists ? "primary" : "light"}
              disabled={!teamId}
            />
          </div>
        </EFImageUploadButtonControl>

        <EFCircleIconButtonTooltip
          tooltipContent="Select as Winner"
          icon={faTrophy}
          size="small"
          shadowTheme="small"
          disabled={!teamId}
          colorTheme={winner === teamId ? "primary" : "light"}
          onClick={() => setWinner(teamId)}
        />
      </div>

      <div className={Style.updateImageWrapper}>
        {imageExists && (
          <div className={Style.imageWrapper}>
            <EFImage src={image?.url} className={Style.matchImage} alt="Team image" />
          </div>
        )}
      </div>
    </EFCard>
  );
};

export default MatchTeamCard;
