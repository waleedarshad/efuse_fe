import React, { Component } from "react";

class ErrorBoundary extends Component {
  constructor(props) {
    super(props);

    this.state = { hasError: false };
  }

  componentDidCatch(error) {
    this.setState({ hasError: true });

    console.error("Exception caught in ErrorBoundary", error);
  }

  render() {
    const { children, fallbackComponent } = this.props;

    if (this.state.hasError) {
      return fallbackComponent || <></>;
    }
    return children;
  }
}

export default ErrorBoundary;
