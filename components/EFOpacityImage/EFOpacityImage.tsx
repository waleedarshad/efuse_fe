import React from "react";
import Style from "./EFOpacityImage.module.scss";

const EFOpacityImage = ({ imgSrc }: { imgSrc: string }) => {
  return (
    <div className={Style.imageWrapper}>
      <div className={Style.opacityOverlay} />
      <div className={Style.darkOverlay} />
      <img className={Style.backgroundImage} src={imgSrc} alt="create" />
    </div>
  );
};

export default EFOpacityImage;
