import { memo } from "react";
import Style from "./OrganizationVerified.module.scss";
import { rescueNil } from "../../helpers/GeneralHelper";
import VerifiedIcon from "../VerifiedIcon/VerifiedIcon";

const OrganizationVerififed = ({ verified }) => {
  return (
    <>
      {rescueNil(verified, "status", false) && (
        <h5 className={Style.verified}>
          <VerifiedIcon organization />
        </h5>
      )}
    </>
  );
};

export default memo(OrganizationVerififed);
