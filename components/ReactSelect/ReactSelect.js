import React, { useState } from "react";
import AsyncSelect from "react-select/lib/Async";
import PropTypes from "prop-types";
import camelCase from "lodash/camelCase";
import { useDispatch } from "react-redux";
import { searchFollowers } from "../../store/actions/messageActions";

const ReactSelect = ({ placeholder, onChange, searchedOptions, theme }) => {
  const [selectedOption, setSelectedOption] = useState();
  const dispatch = useDispatch();

  const changeHandler = option => {
    setSelectedOption(option);
    onChange(option);
  };

  const liberateOptionsFunc = (inputValue, callback) => {
    dispatch(searchFollowers(inputValue));
    callback(searchedOptions);
  };

  return (
    <AsyncSelect
      cacheOptions
      onChange={changeHandler}
      loadOptions={liberateOptionsFunc}
      className={`async-search ${theme}`}
      value={selectedOption}
      placeholder={placeholder}
      id={`dynamic-id-${camelCase(placeholder)}`}
    />
  );
};

ReactSelect.propTypes = {
  placeholder: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  theme: PropTypes.string
};

ReactSelect.defaultProps = {
  theme: ""
};

export default ReactSelect;
