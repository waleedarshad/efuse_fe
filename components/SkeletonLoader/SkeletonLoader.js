import { Fragment } from "react";
import uniqueId from "lodash/uniqueId";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
import PropTypes from "prop-types";

const SkeletonLoader = props => {
  const { skeletons, postHeight, engagementHeight } = props;
  const skeleton = new Array(skeletons).fill(true).map(() => (
    <Fragment key={uniqueId()}>
      <span>
        <SkeletonTheme color="#fff">
          <Skeleton height={postHeight} />
          <Skeleton height={engagementHeight} />
        </SkeletonTheme>
      </span>
    </Fragment>
  ));
  return <>{skeleton}</>;
};

SkeletonLoader.propTypes = {
  skeletons: PropTypes.number,
  postHeight: PropTypes.number,
  engagementHeight: PropTypes.number
};
SkeletonLoader.defaultProps = {
  skeletons: 5,
  postHeight: 100,
  engagementHeight: 10
};

export default SkeletonLoader;
