import { useRouter } from "next/router";
import { useSelector } from "react-redux";
import ConfirmAlert from "../ConfirmAlert/ConfirmAlert";
import TwitchClipsModal from "../GlobalModals/TwitchClipsModal/TwitchClipsModal";

const EFTwitchClipSelect = ({ onTwitchClipSelected, renderTrigger, modalStackOrder, disableTrigger }) => {
  const isTwitchAcccountLinked = useSelector(state => state.user.currentUser.twitchVerified);

  const Router = useRouter();

  if (!isTwitchAcccountLinked) {
    return (
      <ConfirmAlert
        title="Link Twitch Account"
        message="To add Twitch clips, a Twitch account must be linked. Would you like to leave this page to link your Twitch account?"
        onYes={() => Router.push("/settings/external_accounts")}
      >
        {renderTrigger()}
      </ConfirmAlert>
    );
  }

  if (disableTrigger) {
    return renderTrigger();
  }

  return (
    <TwitchClipsModal onClick={onTwitchClipSelected} highStackOrder={modalStackOrder}>
      {renderTrigger()}
    </TwitchClipsModal>
  );
};

export default EFTwitchClipSelect;
