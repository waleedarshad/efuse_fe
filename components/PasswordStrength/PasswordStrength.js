import React, { useState } from "react";
import dynamic from "next/dynamic";
import { FormControl } from "react-bootstrap";
import PropTypes from "prop-types";
import { faEye, faEyeSlash } from "@fortawesome/pro-solid-svg-icons";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Style from "./PasswordStrength.module.scss";

const ReactPasswordStrength = dynamic(() => import("react-password-strength"), {
  ssr: false
});

const Feedback = ({ message, display = "block" }) => (
  <FormControl.Feedback type="invalid" style={{ display }}>
    {message}
  </FormControl.Feedback>
);

const PasswordStrength = props => {
  const [passwordFeedback, setPasswordFeedback] = useState("");
  const [passwordSuggestions, setPasswordSuggestions] = useState([]);
  const [password, setPassword] = useState("");
  const [passwordType, setPasswordType] = useState("password");
  const { validPassword, placeholder, defaultValue } = props;

  const handlePasswordChange = (stateObject, result) => {
    const { isValid, password, score } = stateObject;
    setPassword(password);
    props.changeCallback(password, isValid);
    if (result && score < 2) {
      const { warning, suggestions } = result.feedback;
      setPasswordFeedback(warning);
      setPasswordSuggestions(suggestions);
    } else {
      setPasswordFeedback("");
      setPasswordSuggestions([]);
    }
  };
  const passwordSuggestionsBullets = (
    <ul className={Style.suggestionUl}>
      {passwordSuggestions.map((suggestion, index) => (
        <li key={index}>
          <Feedback message={suggestion} display="inline" />
        </li>
      ))}
    </ul>
  );
  const handlePasswordDisplay = () => {
    setPasswordType(passwordType === "password" ? "text" : "password");
  };
  return (
    <>
      <div onFocus={() => analytics.track("SIGNUP_STEP_ONE_INPUT", { inputName: "password" })}>
        <ReactPasswordStrength
          className={Style.passwordStrength}
          defaultValue={defaultValue}
          minLength={8}
          minScore={2}
          scoreWords={["very weak", "weak", "okay", "good", "strong"]}
          changeCallback={handlePasswordChange}
          inputProps={{
            name: "password",
            autoComplete: "off",
            placeholder,
            type: passwordType,
            required: true,
            id: "password-strength"
          }}
        />
      </div>
      <FontAwesomeIcon
        icon={passwordType === "password" ? faEye : faEyeSlash}
        className={`${Style.eye} ${props.dark && Style.dark}`}
        onClick={handlePasswordDisplay}
      />
      {!validPassword && password.length === 0 && <Feedback message="Password is required" />}
      {passwordFeedback && <Feedback message={passwordFeedback} />}
      {passwordSuggestionsBullets}
    </>
  );
};

PasswordStrength.propTypes = {
  placeholder: PropTypes.string
};

PasswordStrength.defaultProps = {
  placeholder: "eg. Password!234"
};

export default PasswordStrength;
