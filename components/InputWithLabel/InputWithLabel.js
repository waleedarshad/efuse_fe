import React, { useState } from "react";
import PropTypes from "prop-types";
import { FormControl, Form } from "react-bootstrap";

import Style from "./InputWithLabel.module.scss";

const InputWithLabel = props => {
  const [focus, setFocus] = useState(false);
  const { label, errorMessage, dark, inputRef, hotjarWhiteList, helperText, onBlur, ...rest } = props;
  return (
    <>
      <Form.Group controlId={props.name}>
        <Form.Label className={`${Style.formLabel} ${dark && Style.dark}`}>{props.label}</Form.Label>
        <Form.Control
          className={Style.formField}
          ref={inputRef}
          data-hj-whitelist={hotjarWhiteList}
          onFocus={() => setFocus(true)}
          onBlur={e => {
            setFocus(false);
            onBlur(e);
          }}
          {...rest}
        />
        {focus && helperText && <Form.Text className="text-muted">{helperText}</Form.Text>}
        {errorMessage && <FormControl.Feedback type="invalid">{errorMessage}</FormControl.Feedback>}
      </Form.Group>
    </>
  );
};

InputWithLabel.propTypes = {
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired
};

export default InputWithLabel;
