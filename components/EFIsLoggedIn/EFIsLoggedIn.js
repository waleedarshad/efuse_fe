import React from "react";
import { useSelector } from "react-redux";

const EFIsLoggedIn = ({ children, altComponent }) => {
  const currentUser = useSelector(state => state.auth.currentUser);
  return <>{currentUser?.id ? children : altComponent}</>;
};

export default EFIsLoggedIn;
