import React from "react";
import Style from "./EFComingSoonOverlay.module.scss";

interface ComingSoonOverlayProps {
  children: React.ReactNode;
  comingSoonMessage?: string;
  size?: "small" | "medium" | "large";
  hideOverlay?: boolean;
}

const EFComingSoonOverlay = ({
  children,
  comingSoonMessage = "Coming Soon",
  size = "small",
  hideOverlay = false
}: ComingSoonOverlayProps) =>
  hideOverlay ? (
    <>{children}</>
  ) : (
    <>
      <div className={Style.wrapper}>
        {children}
        <div className={Style.gloss} />
      </div>
      <div className={Style.whiteGloss} />
      <div className={Style.whiteGlossBottom} />
      <p className={`${Style.comingSoon} ${Style[size]}`}>{comingSoonMessage}</p>
    </>
  );

export default EFComingSoonOverlay;
