import Link from "next/link";
import React from "react";
import Style from "./LandingFooter.module.scss";
import { getCurrentYear } from "../../../helpers/GeneralHelper";
import FooterSocialIcons from "./FooterSocialIcons";

const LandingFooter = () => {
  const currentYear = getCurrentYear();
  const copyrightText = `Copyright © ${currentYear} eFuse. All rights reserved.`;

  return (
    <div className={Style.footer}>
      <div>
        <Link href="/privacy">
          <p className={Style.link}>Privacy Policy</p>
        </Link>
        <Link href="/terms">
          <p className={Style.link}>Terms of Service</p>
        </Link>
        <Link href="/careers">
          <p className={Style.link}>Careers</p>
        </Link>
        <Link href="https://efuse-public.nolt.io">
          <p className={Style.link}>Feedback</p>
        </Link>
        <Link href="https://efuse.gg/news/efuse-help">
          <p className={Style.link}>Help</p>
        </Link>
        <p className={Style.copyright}>{copyrightText}</p>
      </div>
      <FooterSocialIcons />
    </div>
  );
};

export default LandingFooter;
