import React from "react";
import Style from "./LandingFooter.module.scss";
import EFSocialIcon from "../../EFSocialIcon/EFSocialIcon";

const FooterSocialIcons = () => {
  return (
    <div className={Style.socialContainer}>
      <p className={Style.followSocials}>Follow Our Socials</p>
      <EFSocialIcon
        linkUrl="https://www.linkedin.com/company/efuseofficial/"
        imageUrl="https://cdn.efuse.gg/uploads/static/global/linkedin_icon.png"
        imageAltText="linkedin icon"
      />
      <EFSocialIcon
        linkUrl="https://twitter.com/efuseofficial?lang=en"
        imageUrl="https://cdn.efuse.gg/uploads/static/global/twitter_icon.png"
        imageAltText="twitter icon"
      />
      <EFSocialIcon
        linkUrl="https://www.facebook.com/eFuseOfficial/"
        imageUrl="https://cdn.efuse.gg/uploads/static/global/facebook_icon.png"
        imageAltText="facebook icon"
      />
      <EFSocialIcon
        linkUrl="https://www.instagram.com/efuseofficial/?hl=en"
        imageUrl="https://cdn.efuse.gg/uploads/static/global/instagram_icon.png"
        imageAltText="instagram icon"
      />
      <EFSocialIcon
        linkUrl="https://www.youtube.com/channel/UCeUQ6DGmB_nV57eVEJePXAQ"
        imageUrl="https://cdn.efuse.gg/uploads/static/global/youtube_icon.png"
        imageAltText="youtube icon"
      />
      <EFSocialIcon
        linkUrl="https://www.tiktok.com/@efuseofficial?language=en&sec_uid=MS4wLjABAAAAytHYOk2QIDUu31C2Hzsow52OTsrCTo3C3AFZouh47duj67x3IFvdh5btXuFehwBA&u_code=d910f5a3390i3e&utm_campaign=client_share&app=musically&utm_medium=ios&user_id=6750684578179220485&tt_from=copy&utm_source=copy&enter_from=h5_m"
        imageUrl="https://cdn.efuse.gg/uploads/static/global/tiktok_icon.png"
        imageAltText="tiktok icon"
      />
      <p className={Style.official}>@eFuseOfficial</p>
    </div>
  );
};

export default FooterSocialIcons;
