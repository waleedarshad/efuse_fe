import ReactPlayer from "react-player";

const VideoPlayer = ({ url, height, light, width, autoplay, muted, controls, loop, onPlay }) => {
  return (
    <ReactPlayer
      width={width}
      height={height}
      url={url}
      controls={controls}
      light={light}
      playing={autoplay}
      muted={muted}
      loop={loop}
      onPlay={onPlay}
    />
  );
};
VideoPlayer.defaultProps = {
  width: "100%",
  height: "400px",
  autoplay: false,
  muter: false,
  light: false,
  controls: true,
  loop: false
};
export default VideoPlayer;
