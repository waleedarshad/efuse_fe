import Link from "next/link";
import PropTypes from "prop-types";

import Style from "./Anchor.module.scss";

const Anchor = ({ path, color, innerHtml, style }) => (
  <Link href={path}>
    <a style={style} className={`${Style.anchor} ${Style[color]}`}>
      {innerHtml}
    </a>
  </Link>
);

Anchor.propTypes = {
  path: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
  innerHtml: PropTypes.string.isRequired,
  style: PropTypes.object
};

Anchor.defaultProps = {
  color: "black",
  style: {}
};

export default Anchor;
