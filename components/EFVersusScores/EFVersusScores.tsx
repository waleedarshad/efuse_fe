import React, { useEffect, useState } from "react";
import styled from "styled-components";
import EFImage from "../EFImage/EFImage";
import InputField from "../InputField/InputField";
import Style from "./EFVersusScores.module.scss";

export type TeamData = {
  imageUrl: string;
  score?: number;
  isInputDisabled?: boolean;
  onScoreChange: (score: string) => void;
  teamName: string;
};

interface EFVersusScoresProps {
  title?: string;
  subtitle: string;
  leftTeamData: TeamData;
  rightTeamData: TeamData;
}

const EFVersusScores = ({ title, subtitle, leftTeamData, rightTeamData }: EFVersusScoresProps) => {
  const [leftTeamScore, setLeftTeamScore] = useState(`${leftTeamData.score}`);
  const [rightTeamScore, setRightTeamScore] = useState(`${rightTeamData.score}`);

  useEffect(() => {
    setLeftTeamScore(`${leftTeamData.score}`);
  }, [leftTeamData.score]);

  useEffect(() => {
    setRightTeamScore(`${rightTeamData.score}`);
  }, [rightTeamData.score]);

  useEffect(() => {
    leftTeamData.onScoreChange(leftTeamScore);
  }, [leftTeamScore]);

  useEffect(() => {
    rightTeamData.onScoreChange(rightTeamScore);
  }, [rightTeamScore]);

  return (
    <Container>
      <div className={Style.header}>
        <p className={Style.title}>{title}</p>
        <p className={Style.subtitle}>{subtitle}</p>
      </div>

      <TeamNamesContainer>
        <TeamName>{leftTeamData.teamName}</TeamName>
        <TeamName>{rightTeamData.teamName}</TeamName>
      </TeamNamesContainer>
      <ScoresSection>
        <TeamContainer>
          <ScoreContainer>
            <EFImage src={leftTeamData.imageUrl} className={Style.image} alt="left team avatar" />
            <StyledInputField
              type="number"
              placeholder="0"
              min={0}
              disabled={leftTeamData.isInputDisabled}
              value={leftTeamScore}
              onChange={event => setLeftTeamScore(event.target.value)}
            />
          </ScoreContainer>
        </TeamContainer>

        <div className={Style.versus}>
          <hr />
          <p>VS</p>
          <hr />
        </div>
        <TeamContainer>
          <ScoreContainer>
            <EFImage src={rightTeamData.imageUrl} className={Style.image} alt="right team avatar" />
            <StyledInputField
              type="number"
              placeholder="0"
              min={0}
              disabled={rightTeamData.isInputDisabled}
              value={rightTeamScore}
              onChange={event => setRightTeamScore(event.target.value)}
            />
          </ScoreContainer>
        </TeamContainer>
      </ScoresSection>
    </Container>
  );
};

const Container = styled.div`
  border: 1px solid #c7d3ea;
  border-radius: 8px;
  height: 100%;
  padding: 10px 20px;
`;

const StyledInputField = styled(InputField)`
  padding: 8px;
  width: 60px;
  height: 40px !important;
  background: #ffffff;
  border: 1px solid #c7d3ea !important;
  border-radius: 3px !important;
  font-family: Poppins, sans-serif !important;
  font-style: normal !important;
  font-weight: 400 !important;
  font-size: 16px !important;
  line-height: 22px !important;

  color: #15151a !important;

  &[type="number"]::-webkit-inner-spin-button {
    opacity: 1;
  }
`;

const TeamContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

const ScoreContainer = styled.div`
  display: flex;
  gap: 5px;
`;

const TeamNamesContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-end;
`;

const TeamName = styled.p`
  margin: 0 0 5px 0;
  padding: 0;
  font-family: Poppins, sans-serif;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 20px;
  color: #737385;
  width: 105px;
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  -webkit-line-clamp: 2;
  -webkit-box-orient: vertical;
`;

const ScoresSection = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export default EFVersusScores;
