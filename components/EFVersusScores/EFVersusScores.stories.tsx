import { withCdn } from "../../common/utils";
import EFVersusScores from "./EFVersusScores";

export default {
  title: "Shared/EFVersusScores",
  component: EFVersusScores
};

const Story = args => <EFVersusScores {...args} />;

export const Default = Story.bind({});
Default.args = {
  title: "OpTic Chicago Submission",
  subtitle: "Game 3",
  leftTeamData: {
    imageUrl: withCdn("/static/images/league_of_legends.jpg"),
    score: 1,
    onScoreChange: console.log
  },
  rightTeamData: {
    imageUrl: withCdn("/static/images/league_of_legends.jpg"),
    score: 10,
    isInputDisabled: true,
    onScoreChange: console.log
  }
};
