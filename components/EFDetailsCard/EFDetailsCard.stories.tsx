import React from "react";
import { withCdn } from "../../common/utils";
import EFDetailsCard from "./EFDetailsCard";

export default {
  title: "Cards/EFDetailsCard",
  component: EFDetailsCard
};

const Story = args => <EFDetailsCard {...args} />;

export const Default = Story.bind({});

Default.args = {
  title: "Event title",
  image: withCdn("/static/images/league_of_legends.jpg"),
  line1: "Round Robin",
  line2: "Warzone • Round 2",
  line3: "Oct. 8 2021 | 3:00 PM"
};
