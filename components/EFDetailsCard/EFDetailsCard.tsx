import React, { ReactNode } from "react";
import { Col, Row } from "react-bootstrap";
import EFCard from "../Cards/EFCard/EFCard";
import EFImage from "../EFImage/EFImage";
import Style from "./EFDetailsCard.module.scss";
import { getTextFromHtmlString } from "../../helpers/GeneralHelper";
import { ITeamCardMenuItem } from "../Cards/EFTeamCard/EFTeamCard";
import EFLightDropdown from "../Dropdowns/EFLightDropdown/EFLightDropdown";

interface EFDetailsCardProps {
  title?: string;
  image: string;
  line1: string;
  line2?: string;
  line3: string | ReactNode;
  dropdownMenuItems?: ITeamCardMenuItem[];
  size?: "small" | "medium";
  onClick?: (event: any) => void;
  isSelected?: boolean;
  fitContent?: boolean;
}

const EFDetailsCard: React.FC<EFDetailsCardProps> = ({
  title = "",
  image,
  line1,
  line2,
  line3,
  dropdownMenuItems,
  size = "medium",
  onClick = undefined,
  isSelected = false
}) => {
  return (
    <div className={Style[size]}>
      <EFCard
        widthTheme="fullWidth"
        heightTheme="fullHeight"
        onClick={onClick}
        isSelected={isSelected}
        hoverEffect="default"
        shadow="none"
      >
        <div className={Style.content}>
          <div className={Style.header}>
            <div className={Style.gradient} />
            <EFImage src={image} className={Style.headerImage} alt={title} />
            {title && <div className={Style.title}> {title}</div>}
          </div>
          <Row>
            <Col>
              <div className={Style.body}>
                {line1 && <div className={Style.line1}>{getTextFromHtmlString(line1)}</div>}
                {line2 && <div>{getTextFromHtmlString(line2)}</div>}
                {line3 && <div className={Style.lightGrey}>{line3}</div>}
              </div>
            </Col>

            {dropdownMenuItems && (
              <Col md={3}>
                <div className={Style.dropdown}>
                  <EFLightDropdown menuItems={dropdownMenuItems} />
                </div>
              </Col>
            )}
          </Row>
        </div>
      </EFCard>
    </div>
  );
};

export default EFDetailsCard;
