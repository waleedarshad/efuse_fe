import Slider, { createSliderWithTooltip } from "rc-slider";
import Tooltip from "rc-tooltip";

import PropTypes from "prop-types";

import Style from "./RangeSlider.module.scss";

const Range = createSliderWithTooltip(Slider.Range);
const { Handle } = Slider;

const handle = props => {
  const { value, dragging, index, ...restProps } = props;
  return (
    <Tooltip prefixCls="rc-slider-tooltip" overlay={value} visible placement="top" key={index}>
      <Handle value={value} {...restProps} />
    </Tooltip>
  );
};

const RangeSlider = props => {
  const { min, max, range, ...rest } = props;
  return (
    <div className={Style.rangeWrapper}>
      {range ? (
        <Range {...rest} allowCross={false} min={min} max={max} />
      ) : (
        <Slider {...rest} min={min} max={max} handle={handle} />
      )}
      <span className={Style.minValue}>{min}</span>
      <span className={Style.maxValue}>{max}</span>
    </div>
  );
};

RangeSlider.propTypes = {
  min: PropTypes.number,
  max: PropTypes.number,
  range: PropTypes.bool
};

RangeSlider.defaultProps = {
  min: 0,
  max: 100,
  range: true
};

export default RangeSlider;
