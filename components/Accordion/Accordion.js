import React, { Component } from "react";
import TriggerLogoTemplate from "./TriggerLogoTemplate/TriggerLogoTemplate";
import KillsHeader from "./TournamentKillsTemplate/KillsHeader";
import KillsComponent from "./TournamentKillsTemplate/KillsComponent";
import EFAccordion from "../Accordions/EFAccordion/EFAccordion";

class Accordion extends Component {
  constructor() {
    super();

    this.state = {
      tabArray: []
    };
  }

  static getDerivedStateFromProps(props, state) {
    return props?.tabArray !== state?.tabArray ? { tabArray: props.tabArray } : null;
  }

  componentDidMount() {
    // set state dynamically for different sized lists
    const { openOnMount, openSpot, openAllOnMount } = this.props;

    this.state.tabArray.forEach((tab, index) => {
      if (openAllOnMount || (openOnMount && index === openSpot)) {
        this.setState({
          [index]: true
        });
      } else {
        this.setState({
          [index]: false
        });
      }
    });
  }

  active = spot => {
    // set state dynamically for different sized lists
    const { openAllOnMount } = this.props;
    // set active state to the selected spot (tab) and the others to inactive
    this.state.tabArray.forEach((tab, index) => {
      if (openAllOnMount || index === spot) {
        this.setState({
          [index]: true
        });
      } else {
        this.setState({
          [index]: false
        });
      }
    });
  };

  inactive = spot => {
    // set state dynamically for different sized lists
    const { openAllOnMount } = this.props;
    // set selected spot (tab) to an inactive state
    this.state.tabArray.forEach((tab, index) => {
      if (!openAllOnMount && index === spot) {
        this.setState({
          [index]: false
        });
      }
    });
  };

  render() {
    const { transitionSpeed, marginTop, customTemplate } = this.props;
    const tabArray = this.state?.tabArray;

    if (!tabArray?.length > 0) {
      return <></>;
    }

    return (
      <>
        {tabArray.map((tab, index) => {
          if (tab.display) {
            let WrappedComponent;
            let Trigger;
            if (customTemplate === "TournamentKillsTemplate") {
              WrappedComponent = <KillsComponent players={tab.data.players} key={index} />;
              Trigger = (
                <KillsHeader
                  teamName={tab.data.teamName}
                  rank={tab.data.rank}
                  isActiveTournament={tab.data.isActive}
                  kills={tab.data.stats?.kills}
                  points={tab.data.stats?.value}
                  pointsName={tab.data.stats?.name}
                  active={this.state[index]}
                  twitchView={tab.twitchView}
                  key={index}
                />
              );
            } else {
              WrappedComponent = tab.component;
              Trigger = <TriggerLogoTemplate title={tab.title} active={this.state[index]} key={tab.title} />;
            }
            return (
              <div
                style={{
                  ...(marginTop && index !== 0 && { marginTop })
                }}
                key={tab.tabFeature}
              >
                <EFAccordion
                  triggerComponent={Trigger}
                  isOpen={this.state[index]}
                  transitionSpeed={transitionSpeed}
                  onOpening={() => this.active(index)}
                  onClosing={() => this.inactive(index)}
                >
                  {customTemplate ? WrappedComponent : <WrappedComponent />}
                </EFAccordion>
              </div>
            );
          }
          return <></>;
        })}
      </>
    );
  }
}

// transitionSpeed - animation transition speed of clicking an accordion
// tabArray - pass in an array of tabs for the accordion to display
// a tab could look like the following:
//-----------------------
// title: tab.title,
// component: tab.component,
// display: user[tab.auth]?.stats?.length > 0     (if anyUserCanView is false, the tabs will only display if true)
//-----------------------

Accordion.defaultProps = {
  transitionSpeed: 250,
  tabArray: [
    {
      title: "Insert Title",
      component: <div>Insert Component</div>,
      open: false,
      display: true
    }
  ],
  anyUserCanView: true,
  marginTop: "0px",
  customTemplate: "",
  openOnMount: false,
  openSpot: 0,
  openAllOnMount: false
};

export default Accordion;
