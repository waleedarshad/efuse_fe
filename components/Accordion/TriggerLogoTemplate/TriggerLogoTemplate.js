import React, { Component } from "react";
import Style from "./TriggerLogoTemplate.module.scss";

export default class TriggerLogoTemplate extends Component {
  render() {
    const { title, logo, active } = this.props;
    return <div className={`${Style.header} ${active && Style.active}`}>{title}</div>;
  }
}
