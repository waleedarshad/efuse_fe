import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronDown } from "@fortawesome/pro-solid-svg-icons";
import Style from "./KillsHeader.module.scss";

export default class KillsHeader extends Component {
  render() {
    const { teamName, rank, isActiveTournament, active, points, pointsName, twitchView, removeAccordion } = this.props;

    return (
      <div
        className={`${Style.teamItem} ${active && Style.active} ${twitchView &&
          Style.twitchTeamItem} ${removeAccordion && Style.removeAccordion} ${!isActiveTournament &&
          Style.isNotActiveTournament}`}
      >
        <div className={Style.place}>{rank}</div>
        <p className={Style.text}>{teamName}</p>
        <p className={Style.kills}>
          {points} {pointsName}
        </p>
        {removeAccordion ? (
          <></>
        ) : (
          <div className={Style.iconContainer}>
            <FontAwesomeIcon icon={faChevronDown} className={Style.icon} />
          </div>
        )}
      </div>
    );
  }
}
