import React, { Component } from "react";
import Style from "./KillsComponent.module.scss";

export default class KillsComponent extends Component {
  render() {
    const { players } = this.props;
    return (
      <div className={Style.teamDetails}>
        {players.map((player, index) => {
          return (
            <div className={Style.memberItem} key={index}>
              <div className={Style.place}>{player?.rank}</div>
              <p className={Style.text}>{player?.name}</p>
              <p className={Style.kills}>
                {player?.stats?.value} {player?.stats?.name}
              </p>
            </div>
          );
        })}
      </div>
    );
  }
}
