import React from "react";
import Style from "./AccordionHeader.module.scss";

const AccordionHeader = ({ text, component }) => (
  <div className={Style.header}>
    <div className={Style.headerText}>{text}</div>
    <>{component}</>
  </div>
);

export default AccordionHeader;
