import React from "react";
import EFBomb from "../Icons/EFBomb/EFBomb";
import Style from "./EFIconInfoBox.module.scss";

const EFIconInfoBox = ({ dynamicContent, staticContent }) => {
  return (
    <div className={Style.wrapper}>
      <EFBomb />
      <p className={Style.text}>
        {dynamicContent} {staticContent}
      </p>
    </div>
  );
};

export default EFIconInfoBox;
