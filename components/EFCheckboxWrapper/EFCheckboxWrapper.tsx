import React, { ReactNode, FC } from "react";

import Style from "./EFCheckboxWrapper.module.scss";

interface EFCheckboxWrapperProps {
  children: ReactNode;
  disabled: boolean;
  id: string;
  name: string;
  isChecked: boolean;
  onSelect: () => void;
}

const EFCheckboxWrapper: FC<EFCheckboxWrapperProps> = ({ children, disabled, id, name, isChecked, onSelect }) => {
  return (
    <label className={`${Style.label} ${disabled && Style.disabled}`} htmlFor={id}>
      <input
        type="checkbox"
        name={name}
        id={id}
        className={Style.checkboxInput}
        onChange={onSelect}
        checked={isChecked}
        disabled={disabled}
      />
      {children}
    </label>
  );
};

export default EFCheckboxWrapper;
