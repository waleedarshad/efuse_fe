import React from "react";
import { OverlayTrigger, Tooltip } from "react-bootstrap";
import Style from "./OnlineCircle.module.scss";
import OnlineCircleSizes from "./OnlineCircleSizes.module.scss";

const OnlineCircle = ({ online, size }) => {
  return (
    <OverlayTrigger
      placement="right"
      overlay={
        <Tooltip id="tooltip-top" style={{ zIndex: 9999 }}>
          {online ? "Online" : "Offline"}
        </Tooltip>
      }
    >
      <span className={`${Style.circle} ${online ? Style.online : Style.offline} ${OnlineCircleSizes[size]}`} />
    </OverlayTrigger>
  );
};

OnlineCircle.defaultProps = {
  online: false,
  size: "small"
};

export default OnlineCircle;
