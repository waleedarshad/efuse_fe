import { OverlayTrigger, Tooltip } from "react-bootstrap";
import React from "react";
import Style from "./VerifiedIcon.module.scss";

const VerifiedIcon = ({ organization }) => {
  return (
    <OverlayTrigger
      placement="top"
      overlay={
        <Tooltip id="tooltip-hype" style={{ zIndex: 9999 }}>
          {organization ? "Verified Organization" : "Verified Influencer"}
        </Tooltip>
      }
    >
      <span className={Style.verified}>
        <img
          alt="Verified"
          className={Style.verifyIcon}
          src={`${
            organization
              ? "https://cdn.efuse.gg/uploads/static/global/efuse_verify_green.png"
              : "https://cdn.efuse.gg/uploads/static/global/efuse_verify.png"
          }`}
        />
      </span>
    </OverlayTrigger>
  );
};
export default VerifiedIcon;
