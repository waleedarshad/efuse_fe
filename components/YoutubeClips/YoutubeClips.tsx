import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faYoutube } from "@fortawesome/free-brands-svg-icons";
import { useState } from "react";

import uniqueId from "lodash/uniqueId";
import { useQuery } from "@apollo/client";
import moment from "moment";
import { faChevronLeft, faChevronRight } from "@fortawesome/pro-regular-svg-icons";

import Style from "./Youtube.module.scss";
import { GET_PAGINATED_YOUTUBE_VIDEOS, YoutubeVideo, Clip } from "../../graphql/feeds/PostClipQuery";
import AnimatedLogo from "../AnimatedLogo";
import Error from "../Error/Error";
import EFDetailsCheckboxCard from "../Cards/EFDetailsCheckboxCard/EFDetailsCheckboxCard";
import EFRectangleButton from "../Buttons/EFRectangleButton/EFRectangleButton";

const EmptyContent = () => {
  return (
    <div className={Style.noClipsFoundContainer}>
      <div className={Style.middleContainer}>
        <FontAwesomeIcon icon={faYoutube} className={Style.icon} />
        <h4>You don’t have any YouTube videos uploaded</h4>
      </div>
    </div>
  );
};

export interface YoutubeVideoVars {
  ownerKind: string;
  owner: string;
  page: number;
  limit: number;
  onYoutubeClipSelected?: (clip: Clip) => void;
  showClips?: (key: boolean) => void;
}

const YoutubeClips = ({ ownerKind, owner, page, limit, onYoutubeClipSelected, showClips }: YoutubeVideoVars) => {
  const [clipId, setClipId] = useState(null);
  const [disableHover, setDisableHover] = useState(false);

  const { data, error, loading, refetch } = useQuery<YoutubeVideo, YoutubeVideoVars>(GET_PAGINATED_YOUTUBE_VIDEOS, {
    variables: { ownerKind, owner, page, limit }
  });

  if (loading) return <AnimatedLogo theme="content" />;
  if (error) return <Error />;

  const videos = data.GetPaginatedYoutubeVideos;
  const youtubeClips = videos.docs;

  if (youtubeClips.length === 0) {
    return <EmptyContent />;
  }

  const setYoutubeClipId = event => {
    setClipId(event.target.id);
  };

  const formatDate = date => moment(new Date(date)).format("MM/DD/YYYY");

  const previousPage = () =>
    refetch({
      ownerKind,
      owner,
      page: videos.page - 1,
      limit
    });

  const nextPage = () =>
    refetch({
      ownerKind,
      owner,
      page: videos.page + 1,
      limit
    });

  return (
    <div className={Style.cardWrapper}>
      {youtubeClips.map(clip => {
        return (
          <div
            className={`${Style.card} ${clipId && clipId !== clip._id && Style.disabled}`}
            key={uniqueId()}
            onMouseEnter={() => {
              if (clipId) {
                setDisableHover(true);
              }
            }}
            onMouseLeave={() => setDisableHover(false)}
          >
            <EFDetailsCheckboxCard
              image={clip.thumbnails.medium.url}
              onChange={setYoutubeClipId}
              checkboxId={clip._id}
              checkboxName={clip.title}
              isChecked={clipId && clipId === clip._id}
              disableHover={disableHover}
            />
            <div className={Style.dateSection}>
              <span className={Style.title}>{clip.title}</span>
              <span className={Style.date}>{formatDate(clip.videoPublishedAt)}</span>
            </div>
          </div>
        );
      })}
      <div className={Style.button}>
        <div className={Style.pagination}>
          <div onClick={previousPage} aria-hidden="true">
            <FontAwesomeIcon icon={faChevronLeft} className={Style.icon} />
          </div>
          {videos.hasNextPage && (
            <div onClick={nextPage} aria-hidden="true">
              <FontAwesomeIcon icon={faChevronRight} className={Style.icon} />
            </div>
          )}
        </div>
        <EFRectangleButton
          text="Post this video"
          onClick={() => {
            onYoutubeClipSelected(youtubeClips.find(clip => clip._id === clipId));
            showClips(false);
          }}
          colorTheme="light"
          shadowTheme="none"
          fontWeightTheme="normal"
          disabled={!clipId}
        />
      </div>
    </div>
  );
};

export default YoutubeClips;
