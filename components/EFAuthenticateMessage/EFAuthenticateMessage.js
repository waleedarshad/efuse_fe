import React from "react";

import Style from "./EFAuthenticateMessage.module.scss";

const EFAuthenticateMessage = ({ login, signup, animationSize, fontSize }) => {
  const animationDimensions = {};
  switch (animationSize) {
    case "small":
      animationDimensions.height = 100;
      animationDimensions.width = 100;
      break;
    case "large":
      animationDimensions.height = 200;
      animationDimensions.width = 200;
      break;

    default:
      animationDimensions.height = 150;
      animationDimensions.width = 150;
      break;
  }
  return (
    <div className={Style.contentContainer}>
      {login && (
        <>
          <p className={`${Style.title} ${Style[fontSize]}`}>Login Successful</p>
          <p className={`${Style.subTitle} ${Style[fontSize]}`}>Welcome Back to eFuse</p>
        </>
      )}
      {signup && (
        <>
          <p className={`${Style.title} ${Style[fontSize]}`}>Account Created</p>
          <p className={`${Style.subTitle} ${Style[fontSize]}`}>Welcome to eFuse</p>
        </>
      )}
    </div>
  );
};

EFAuthenticateMessage.defaultProps = {
  animationSize: "medium",
  fontSize: "small"
};

export default EFAuthenticateMessage;
