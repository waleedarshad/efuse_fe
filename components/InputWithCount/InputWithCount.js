import CharacterCounter from "react-character-counter";
import PropTypes from "prop-types";

import InputField from "../InputField/InputField";

const InputWithCount = props => (
  <CharacterCounter value={props.value || ""} maxLength={props.maxLength}>
    <InputField {...props} />
  </CharacterCounter>
);

InputWithCount.propTypes = {
  value: PropTypes.string.isRequired,
  maxLength: PropTypes.number.isRequired
};

export default InputWithCount;
