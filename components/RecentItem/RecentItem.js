import { ListGroupItem, Media } from "react-bootstrap";

import PropTypes from "prop-types";

import Style from "./RecentItem.module.scss";

const RecentItem = ({ id, avatar, name, type, parentStyleSheet, imageType, itemType, publishStatus, shortNameUrl }) => {
  const itemUrl = shortNameUrl || `/${type}/show?id=${id}`;

  return (
    <ListGroupItem className={parentStyleSheet.listItem}>
      <Media className="align-items-center">
        <img
          width={50}
          height={50}
          className={`mr-3 ${Style.mediaImage} ${imageType && Style[imageType]}`}
          src={avatar}
          alt={name}
        />
        <Media.Body className={Style.container}>
          <a
            href={itemUrl}
            onClick={() => {
              analytics.track(
                `${type.toUpperCase()}_LINK_CLICKED`,
                {
                  type: "title_text",
                  link: itemUrl
                },
                {},
                () => {
                  document.location.href = itemUrl;
                }
              );
            }}
            className={Style.titleLink}
          >
            <h6 className={Style.titleName}>{name}</h6>
          </a>
          <h6 className={Style.itemType}>{itemType}</h6>
          <a
            href={itemUrl}
            onClick={() => {
              analytics.track(
                `${type.toUpperCase()}_LINK_CLICKED`,
                {
                  type: "button",
                  link: itemUrl
                },
                {},
                () => {
                  document.location.href = itemUrl;
                }
              );
            }}
          >
            <button className={Style.button}>
              {type === "opportunities" && publishStatus !== "closed" ? "View" : "Follow"}
            </button>
          </a>
        </Media.Body>
      </Media>
    </ListGroupItem>
  );
};

RecentItem.propTypes = {
  id: PropTypes.string.isRequired,
  avatar: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  parentStyleSheet: PropTypes.object.isRequired,
  imageType: PropTypes.string,
  itemType: PropTypes.string
};

export default RecentItem;
