import React from "react";
import Style from "./UsernameAndStreak.module.scss";
import OnlineCircle from "../OnlineCircle/OnlineCircle";
import EFStreakBadge from "../EFStreakBadge/EFStreakBadge";

const UsernameAndStreak = ({ online, username, streak }) => {
  return (
    <div className={Style.usernameStreakWrapper}>
      <OnlineCircle online={online} />
      <span className={Style.username}>{username}</span>
      <EFStreakBadge streak={streak} />
    </div>
  );
};

export default UsernameAndStreak;
