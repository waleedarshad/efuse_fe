import React from "react";
import UsernameAndStreak from "./UsernameAndStreak";

export default {
  title: "Users/UsernameAndStreak",
  component: UsernameAndStreak,
  argTypes: {
    online: {
      control: {
        type: "boolean"
      }
    },
    username: {
      control: {
        type: "text"
      }
    },
    streak: {
      control: {
        type: "number"
      }
    }
  }
};

const Story = args => <UsernameAndStreak {...args} />;

export const Online = Story.bind({});
Online.args = {
  online: true,
  username: "Austin May",
  streak: 120
};

export const Offline = Story.bind({});
Offline.args = {
  online: false,
  username: "Austin May",
  streak: 20
};
