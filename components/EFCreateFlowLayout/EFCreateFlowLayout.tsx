import React, { useContext } from "react";

import { Spinner, ProgressBar } from "react-bootstrap";
import Style from "./EFCreateFlowLayout.module.scss";
import EFOpacityImage from "../EFOpacityImage/EFOpacityImage";
import { EFWizardContext } from "../EFWizard/EFWizard";
import EFRectangleButton from "../Buttons/EFRectangleButton/EFRectangleButton";

interface EFCreateFlowLayoutProps {
  titleText?: string;
  titleSubText?: string;
  backgroundImageSrc?: string;
  onNext?: () => void;
  buttonText?: string;
  children: React.ReactNode;
  isRightButtonDisabled?: boolean;
  isLoading?: boolean;
}

const EFCreateFlowLayout = ({
  titleText = "",
  titleSubText = "",
  backgroundImageSrc = "https://cdn.efuse.gg/uploads/leagues/leagues-create-wizard.jpg",
  buttonText,
  onNext,
  children,
  isRightButtonDisabled = false,
  isLoading = false
}: EFCreateFlowLayoutProps) => {
  const { goNextStep, goPrevStep, isLastStep, isFirstStep, progressPercent } = useContext(EFWizardContext);

  const onNextClick = () => {
    if (onNext) {
      onNext();
    } else {
      goNextStep();
    }
  };

  return (
    <div className={Style.createContainer}>
      <div className={Style.leftContainer}>
        <EFOpacityImage imgSrc={backgroundImageSrc} />
        <div className={Style.leftTextContainer}>
          <div className={Style.leftText}>{titleText}</div>
          <div className={Style.leftSubText}>{titleSubText}</div>
        </div>
      </div>
      <div className={Style.rightContainer}>
        <div className={Style.rightContentWrapper}>{children}</div>
        <div className={Style.rightFooter}>
          <ProgressBar className={Style.progressBar} now={progressPercent} variant="blue" />
          <div className={Style.footerContentWrapper}>
            {!isFirstStep && (
              <EFRectangleButton
                className={Style.button}
                colorTheme="light"
                text="Back"
                shadowTheme="none"
                onClick={goPrevStep}
              />
            )}
            <div className={Style.rightButtonWrapper}>
              <EFRectangleButton
                className={Style.button}
                colorTheme={isLastStep ? "primary" : "secondary"}
                text={
                  <>
                    {isLoading && <Spinner className="mr-2" animation="border" variant="light" size="sm" />}
                    {buttonText || (isLastStep ? "Create" : "Next")}
                  </>
                }
                shadowTheme="none"
                onClick={onNextClick}
                disabled={isRightButtonDisabled}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default EFCreateFlowLayout;
