import React from "react";

import EFCreateFlowLayout from "./EFCreateFlowLayout";

export default {
  title: "Layouts/EFCreateFlowLayout",
  compononet: EFCreateFlowLayout
};

const Story = args => <EFCreateFlowLayout {...args} />;

export const Default = Story.bind({});

Default.args = {
  progress: 20,
  titleText: "Only a few steps left!",
  buttonText: "Next",
  titleSubText:
    "Lorem dkdmf skdnf asdfn kasdfnasd fkasjdnf asjdnf asdf fkasndf asdfjs nasjdf asdjfnaksdfn asjdkfnajksdf fklasndf djasknd j jd afsdf",
  backgroundImageSrc:
    "https://www.callofduty.com/content/dam/atvi/callofduty/cod-touchui/blog/hero/bocw/BOCW-S4-Announcement-TOUT.jpg",
  formContent: <>this is a form!</>
};
