import Tippy from "@tippyjs/react";
import React, { forwardRef } from "react";
// eslint-disable-next-line import/no-extraneous-dependencies
import "tippy.js/animations/scale.css";

const EFTippy = ({ content, interactive, delay, animation, onMount, children }) => {
  // eslint-disable-next-line react/display-name
  const Child = forwardRef((_props, ref) => {
    return <div ref={ref}>{children}</div>;
  });

  return (
    <Tippy content={content} interactive={interactive} delay={delay} animation={animation} onMount={onMount}>
      <Child />
    </Tippy>
  );
};

EFTippy.defaultProps = {
  content: "",
  interactive: false,
  delay: [550, 100],
  animation: "scale",
  onMount: ""
};

export default EFTippy;
