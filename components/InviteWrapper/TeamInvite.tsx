import { useSelector } from "react-redux";
import React, { useEffect, useState } from "react";
import { useMutation } from "@apollo/client";
import Router from "next/router";
import EFConfirmationModal from "../Modals/EFConfirmationModal/EFConfirmationModal";
import { ACCEPT_INVITE_CODE } from "../../graphql/Team/LeagueTeam";
import OrganizationDetail from "../Organizations/OrganizationDetail/OrganizationDetail";
import { sendNotification } from "../../helpers/FlashHelper";

const TeamInvite = ({ team, code }) => {
  const authUser = useSelector(state => state.auth.currentUser?.id);
  const [showConfirmModal, setShowConfirmModal] = useState(false);
  const [acceptInvite] = useMutation(ACCEPT_INVITE_CODE);

  const onConfirm = () => {
    acceptInvite({ variables: { entityId: authUser, code } }).then(response => {
      if (response?.data?.acceptInvite) {
        sendNotification(`You've successfully joined ${team.name}`, "success", "Accept Invitation");
        Router.push(`/leagues/dashboard/${team?.owner?._id}`);
      } else {
        sendNotification("Something went wrong. Unable to join team.", "danger", "Accept Invitation");
      }
    });
  };

  useEffect(() => {
    if (authUser) {
      setShowConfirmModal(true);
    }
  }, [authUser]);

  return (
    <>
      <OrganizationDetail orgIdFromInviteCode={team?.owner?._id} pageProps={{ organization: team?.owner }} />
      <EFConfirmationModal
        title="You've been invited!"
        message={`Would you like to join ${team.name}?`}
        confirmText="Join"
        isOpen={showConfirmModal}
        onClose={() => setShowConfirmModal(false)}
        onConfirm={onConfirm}
        displayCloseButton
      />
    </>
  );
};

export default TeamInvite;
