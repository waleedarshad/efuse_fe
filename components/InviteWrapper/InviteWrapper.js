import React, { useEffect } from "react";
import dynamic from "next/dynamic";
import { useSelector, useDispatch } from "react-redux";
import Cookies from "js-cookie";
import { NextSeo } from "next-seo";
import Router, { useRouter } from "next/router";
import toLower from "lodash/toLower";
import JoinOrganizationFromInvite from "../Organizations/JoinOrganizationFromInvite";
import profileLayout from "../hoc/profileLayout";
import DynamicModal from "../DynamicModal/DynamicModal";
import Error from "../../pages/_error";
import { getNameFromInviteCode, getNextSeoDescription, getNextSeoImageUrl } from "../../helpers/InviteCodeHelper";
import { updateUserSimple } from "../../store/actions/userActions";
import TeamInvite from "./TeamInvite";

// do not render these server side because the full organization document is not present for invite code
const OrganizationDetail = dynamic(import("../Organizations/OrganizationDetail/OrganizationDetail"), {
  ssr: false
});

// do not render these server side because the full user document is not present for invite code
const Portfolio = dynamic(import("../User/Portfolio/Portfolio"), {
  ssr: false
});

const InviteWrapper = ({ pageProps }) => {
  const router = useRouter();
  const dispatch = useDispatch();
  const { query } = router;

  const authUser = useSelector(state => state.auth.currentUser);
  const userLoggedIn = authUser?.id;

  const inviteCodeCookie = Cookies.get("i");

  const TWO_DAYS = 2;
  let page;

  const noCookieAlreadyExists = () => {
    return !inviteCodeCookie && query.i && query.i !== inviteCodeCookie;
  };

  const saveReferralCodeFromInviteCookie = () => {
    if (noCookieAlreadyExists(query)) {
      Cookies.set("i", query.i, { expires: TWO_DAYS });
    }
  };

  useEffect(() => {
    analytics.page("Invite Page");

    if (query && query?.i) {
      saveReferralCodeFromInviteCookie();
    }

    if (pageProps?.inviteCode?.type === "USER" && pageProps?.inviteCode?.user) {
      dispatch(updateUserSimple(pageProps.inviteCode.user));
    }

    const currentRoute = pageProps.q_params ? toLower(pageProps?.q_params) : pageProps.q_params;

    if (pageProps.q_params && pageProps.q_params !== currentRoute) {
      Router.replace(currentRoute, currentRoute, { shallow: true });
    }
  }, []);

  if (!pageProps.inviteCode) return <Error statusCode={pageProps.SsrStatusCode} />;

  switch (pageProps.inviteCode.type) {
    case "ORGANIZATION_MEMBER_INVITE":
      page = (
        <JoinOrganizationFromInvite
          orgIdFromInviteCode={pageProps.inviteCode.organization._id}
          pageProps={{ organization: pageProps.inviteCode.organization }}
          inviteCode={pageProps.inviteCode}
        />
      );
      break;
    case "ORGANIZATION":
      // if invite inviteCode is connected to org, show invite modal on top of org page
      page = (
        <OrganizationDetail
          orgIdFromInviteCode={pageProps.inviteCode.organization._id}
          pageProps={{ organization: pageProps.inviteCode.organization }}
        />
      );
      break;
    case "USER":
      // eslint-disable-next-line no-case-declarations
      const PortFolioHoc = profileLayout(Portfolio, pageProps.inviteCode.user._id);
      page = <PortFolioHoc />;
      break;
    case "TEAM":
      page = <TeamInvite team={pageProps.inviteCode.team} code={pageProps.inviteCode.code} />;
      break;
    default:
      page = <></>;
  }

  return (
    <>
      {/* TODO: add NextSeo functionality for portfolios... this only works for organizations and teams */}
      <NextSeo
        title={`${getNameFromInviteCode(pageProps.inviteCode)} invited you to join eFuse`}
        description={`${getNextSeoDescription(pageProps)}`}
        openGraph={{
          type: "website",
          url: `https://efuse.gg/i/${pageProps.inviteCode.code}`,
          title: `${
            pageProps.inviteCode.user ? pageProps.inviteCode.user?.name : pageProps.inviteCode.organization?.name
          } invited you to join eFuse`,
          site_name: "eFuse.gg",
          description: getNextSeoDescription(pageProps),
          images: [
            {
              url: getNextSeoImageUrl(pageProps)
            }
          ]
        }}
        twitter={{
          handle: "@eFuseOfficial",
          site: `https://efuse.gg/i/${pageProps.inviteCode.code}`,
          cardType: "summary_large_image"
        }}
      />
      {page}
      <div>
        {pageProps?.inviteCode && !userLoggedIn && (
          <DynamicModal
            flow="LoginSignup"
            openOnLoad
            startView="invite"
            displayCloseButton={false}
            inviteCode={pageProps.inviteCode}
            allowBackgroundClickClose={false}
          />
        )}
      </div>
    </>
  );
};

export default InviteWrapper;
