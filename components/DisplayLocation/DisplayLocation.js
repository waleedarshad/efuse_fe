import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMapMarkerAlt } from "@fortawesome/pro-solid-svg-icons";

import PropTypes from "prop-types";

import Style from "./DisplayLocation.module.scss";

const DisplayLocation = ({ location, wrapperClass }) => {
  return location ? (
    <div className={`${Style.location} ${wrapperClass}`}>
      <FontAwesomeIcon icon={faMapMarkerAlt} />{" "}
      <span className={Style.locationText}>{location.length > 65 ? `${location.substring(0, 65)}...` : location}</span>
    </div>
  ) : null;
};

DisplayLocation.propTypes = {
  location: PropTypes.string,
  wrapperClass: PropTypes.string
};

DisplayLocation.defaultProps = {
  wrapperClass: ""
};

export default DisplayLocation;
