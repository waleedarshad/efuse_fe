import React from "react";
import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrophy as faTrophySolid } from "@fortawesome/pro-solid-svg-icons";
import { faTrophy as faTrophyRegular } from "@fortawesome/pro-regular-svg-icons";
import EFCard from "../Cards/EFCard/EFCard";
import colors from "../../styles/sharedStyledComponents/colors";
import { textNormal } from "../../styles/sharedStyledComponents/sharedStyles";
import EFSquareImage from "../EFSquareImage/EFSquareImage";
import EFScoringTableCell from "./EFScoringTableCell";
import { SubmittedScoreType } from "../../graphql/interface/League";

interface Props {
  teams: {
    _id: string;
    name: string;
    owner: {
      _id: string;
      profileImage: {
        url: string;
      };
    };
  }[];
  standingWinner: string;
  games: {
    [gameNumber: number]: {
      standingWinner: string;
      type: SubmittedScoreType;
      scores: {
        [teamId: string]: {
          _id: string;
          teamId: string;
          score: number;
        };
      };
    };
  };
  gamesPerMatch: number;
  selectedGame?: number;
  onSelectGame?: (gameNumber: number) => void;
  onEditScore?: (gameNumber: number, teamId: string, newValue: number) => void;
  isViewOnly?: boolean;
}

const EFScoringTable = ({
  teams,
  games,
  selectedGame,
  standingWinner,
  gamesPerMatch,
  onSelectGame = () => {},
  onEditScore = () => {},
  isViewOnly = false
}: Props) => {
  const maxGameNumbers = [...Array(9)].map((_, index) => index + 1);

  return (
    <EFCard widthTheme="fullWidth" shadow="none">
      <Table>
        <Row>
          <EFScoringTableCell header>Game</EFScoringTableCell>
          {maxGameNumbers.map(gameNumber => (
            <EFScoringTableCell
              header
              key={gameNumber}
              onClick={gameNumber <= gamesPerMatch ? () => onSelectGame(gameNumber) : undefined}
              selected={selectedGame && gameNumber === selectedGame}
              disabled={gameNumber > gamesPerMatch}
            >
              {gameNumber}
            </EFScoringTableCell>
          ))}
          <EFScoringTableCell header>Standing</EFScoringTableCell>
        </Row>

        {teams?.map(team => {
          return (
            <Row key={team._id}>
              <EFScoringTableCell>
                <EFSquareImage src={team?.owner?.profileImage?.url} size={20} border spaceRight />
                {team.name}
              </EFScoringTableCell>
              {maxGameNumbers.map(gameNumber => {
                const game = games?.[gameNumber];
                const teamScore = game?.scores?.[team._id];
                const disabled = gameNumber > gamesPerMatch;
                const editable = !isViewOnly && !disabled && selectedGame === gameNumber;
                const clickable = !disabled && !editable;
                return (
                  <EFScoringTableCell
                    border
                    key={gameNumber + team._id}
                    greenText={game?.standingWinner === team._id}
                    hasConflict={game?.type === SubmittedScoreType.CONFLICT}
                    disabled={gameNumber > gamesPerMatch}
                    value={teamScore?.score}
                    editable={editable}
                    onClick={clickable ? () => onSelectGame(gameNumber) : undefined}
                    onChange={newValue => onEditScore(gameNumber, team._id, newValue)}
                  >
                    {teamScore?.score}
                  </EFScoringTableCell>
                );
              })}
              <EFScoringTableCell border>
                {team._id === standingWinner ? (
                  <FontAwesomeIcon color={colors.winnerYellow} icon={faTrophySolid} />
                ) : (
                  <FontAwesomeIcon icon={faTrophyRegular} />
                )}
              </EFScoringTableCell>
            </Row>
          );
        })}
      </Table>
    </EFCard>
  );
};

const Table = styled.div`
  width: 100%;
`;

const Row = styled.div`
  display: flex;
  align-items: center;
  height: 36px;
  ${textNormal};
`;

export default EFScoringTable;
