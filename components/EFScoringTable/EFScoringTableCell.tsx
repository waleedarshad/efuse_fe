import styled, { css } from "styled-components";
import React from "react";
import colors from "../../styles/sharedStyledComponents/colors";
import { textNormal } from "../../styles/sharedStyledComponents/sharedStyles";
import InputField from "../InputField/InputField";

interface Props {
  children?: any;
  value?: string | number;
  editable?: boolean;
  disabled?: boolean;
  hasConflict?: boolean;
  header?: boolean;
  border?: boolean;
  selected?: boolean;
  clickable?: boolean;
  greenText?: boolean;
  onClick?: () => void;
  onChange?: (newValue: number) => void;
}

const EFScoringTableCell = ({
  children = <></>,
  value = "",
  editable = false,
  disabled = false,
  hasConflict = false,
  header = false,
  border = false,
  selected = false,
  greenText = false,
  onChange = () => {},
  onClick = () => {}
}: Props) => {
  return (
    <Cell
      onClick={onClick}
      disabled={disabled}
      hasConflict={hasConflict}
      header={header}
      border={border}
      selected={selected}
      clickable={!disabled}
      greenText={greenText}
    >
      {editable ? (
        <InputField
          placeholder="0"
          type="number"
          theme="borderless"
          step={1}
          min={0}
          onChange={event => onChange(Number.parseInt(event?.target?.value, 10))}
          value={value}
        />
      ) : (
        children
      )}
    </Cell>
  );
};

const Border = css`
  border-left: 1px solid ${colors.outerBorder};
  border-bottom: 1px solid ${colors.outerBorder};
  border-right: none;
  border-top: none;
`;

const Header = css`
  background: ${props => (props.selected ? colors.eFuseLightBlue : colors.eFuseBlue)};
  color: ${props => (props.disabled ? colors.textDisabledWhite : colors.white)};
  width: 100%;
  border-left: 1px solid transparent;
`;

const Cell = styled.div`
  display: flex;
  flex: 1;
  height: 100%;
  width: 100%;
  align-items: center;
  justify-content: center;
  cursor: ${props => (props.clickable ? "pointer" : "default")};
  ${props => props.greenText && `color: ${colors.winnerGreen};`}
  ${props => props.disabled && `background: ${colors.hover};`}
  ${props => props.hasConflict && `background: ${colors.errorBackground};`}
  ${props => props.border && Border};
  ${props => props.header && Header};

  input {
    ${props => props.greenText && `color: ${colors.winnerGreen};`}
    border: none;
    background: transparent;
    padding: 5px;
    ${textNormal}
  }

  input[type="number"]::-webkit-inner-spin-button {
    opacity: 1;
  }

  :first-child {
    flex: 3;
    padding: 8px;
    justify-content: flex-start;
    cursor: default;
  }

  :last-child {
    flex: 2;
    cursor: default;
  }
`;

export default EFScoringTableCell;
