import React from "react";
import EFScoringTable from "./EFScoringTable";

export default {
  title: "Shared/EFScoringTable",
  component: EFScoringTable
};

const Story = args => <EFScoringTable {...args} />;

const teams = [
  {
    _id: "612e33fc3f9a2718149dd08f",
    name: "Team Blue",
    owner: {
      _id: "5d4372c7344a340017a93e34",
      profileImage: {
        url:
          "https://efuse.s3.amazonaws.com/uploads/organizations/1564701382427-lion_mane_muzzle_black_white_wind_field_40126_1920x1080.jpg"
      }
    }
  },
  {
    _id: "612e340e3f9a2718149dd0b2",
    name: "Team Cubone",
    owner: {
      _id: "610823b3b1b7c0d0fa825742",
      profileImage: {
        url: "https://staging-efuse.s3.amazonaws.com/uploads/organizations/1627923377764-il_570xN.2269543189_9vz0.jpg"
      }
    }
  }
];

const games = {
  1: {
    scores: {
      "612e33fc3f9a2718149dd08f": { score: 2, hasConflict: false },
      "612e340e3f9a2718149dd0b2": { score: 1, hasConflict: true }
    },
    standingWinner: "612e33fc3f9a2718149dd08f"
  },
  2: {
    scores: {
      "612e33fc3f9a2718149dd08f": { score: 22, hasConflict: false },
      "612e340e3f9a2718149dd0b2": { score: 10, hasConflict: true }
    },
    standingWinner: "612e33fc3f9a2718149dd08f"
  },
  3: {
    scores: {
      "612e33fc3f9a2718149dd08f": { score: 1, hasConflict: false },
      "612e340e3f9a2718149dd0b2": { score: 5, hasConflict: false }
    },
    standingWinner: "612e340e3f9a2718149dd0b2"
  }
};

export const Default = Story.bind({});
Default.args = {
  selectedGame: 2,
  numberOfGames: 3,
  standingWinner: "612e33fc3f9a2718149dd08f",
  teams,
  games
};
