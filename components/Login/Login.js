import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { useRouter } from "next/router";
import parse from "url-parse";

import { toggleLoader } from "../../store/actions/loaderActions";
import LandingPage from "../Join/LandingPage";
import DynamicModal from "../DynamicModal/DynamicModal";

const Login = ({ institutionLogo }) => {
  const router = useRouter();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(toggleLoader(false));
    analytics.page("Login");
  }, []);

  return (
    <>
      <LandingPage />

      <DynamicModal
        flow="LoginSignup"
        openOnLoad
        institutionLogo={institutionLogo}
        startView="login"
        displayCloseButton
        allowBackgroundClickClose={false}
        onCloseModal={() => {
          router.push("/");
        }}
        customRedirect={router.query.redirect_url ? parse(router.query.redirect_url, true).href : null}
      />
    </>
  );
};

export default Login;
