/* eslint-disable jsx-a11y/anchor-is-valid */

import React from "react";
import Link from "next/link";
import Style from "./ImportantLinks.module.scss";
import { getCurrentYear } from "../../helpers/GeneralHelper";

const ImportantLinks = () => {
  const copyrightText = `Copyright © ${getCurrentYear()}`;

  return (
    <div className={Style.wrapper}>
      <div className={Style.flexContainer}>
        <Link href="/privacy">
          <a target="_blank" className={Style.link}>
            Privacy
          </a>
        </Link>
        <span>•</span>
        <Link href="/terms">
          <a target="_blank" className={Style.link}>
            Terms
          </a>
        </Link>
        <span>•</span>
        <Link href="https://efuse.careers">
          <a target="_blank" className={Style.link}>
            Careers
          </a>
        </Link>
        <span>•</span>
        <Link href="https://efuse-public.nolt.io">
          <a target="_blank" className={Style.link}>
            Feedback
          </a>
        </Link>
        <span>•</span>
        <Link href="https://efuse.gg/news/efuse-help">
          <a target="_blank" className={Style.link}>
            Help
          </a>
        </Link>
      </div>
      <div className={Style.copyright}>
        {copyrightText}
        <img
          className={Style.efuseName}
          alt="eFuse"
          src="https://cdn.efuse.gg/uploads/static/global/efuseBrandNameDark.png"
        />
      </div>
    </div>
  );
};

export default ImportantLinks;

/* eslint-enable jsx-a11y/anchor-is-valid */
