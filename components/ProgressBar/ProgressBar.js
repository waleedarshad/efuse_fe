import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Progress from "react-bootstrap/ProgressBar";

import Style from "./ProgressBar.module.scss";

const ProgressBar = ({ size, percentage, icon, text }) => {
  return (
    <div className={`${Style[size]} ${Style.progress}`}>
      <Progress className={Style.progressBar} now={percentage} variant="black" />
      <div className={Style.progressBarInfo}>
        <FontAwesomeIcon icon={icon} className={Style.progressBarIcon} />
        <span className={Style.progressBarTitle}>{text}</span>
        <span className={Style.progressBarValue}>{percentage}%</span>
      </div>
    </div>
  );
};
export default ProgressBar;
