import React from "react";
import { faMedal } from "@fortawesome/pro-solid-svg-icons";

import ProgressBar from "./ProgressBar";

export default {
  title: "Shared/Progressbar",
  component: ProgressBar,
  argTypes: {
    size: {
      control: {
        type: "select",
        options: ["large", "medium"]
      }
    },
    percentage: {
      control: {
        type: "number"
      }
    },
    icon: {
      control: {
        type: "string"
      }
    },
    text: {
      control: {
        type: "string"
      }
    }
  }
};

const Story = args => (
  <div style={{ width: "300px", marginTop: "40px" }}>
    <ProgressBar {...args} />
  </div>
);

export const Large = Story.bind({});
Large.args = {
  size: "large",
  percentage: 67,
  icon: faMedal,
  text: "percentage"
};

export const Medium = Story.bind({});
Medium.args = {
  size: "medium",
  percentage: 67,
  icon: faMedal,
  text: "percentage"
};
