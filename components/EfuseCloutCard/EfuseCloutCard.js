import React from "react";

import EFCard from "../Cards/EFCard/EFCard";
import EFImage from "../EFImage/EFImage";
import Style from "./EfuseCloutCard.module.scss";

const EfuseCloutCard = ({ shadow, cardSize, username, followers }) => {
  return (
    <div className={Style[cardSize]}>
      <EFCard shadow={shadow} widthTheme="fullWidth" heightTheme="fullHeight">
        <div className={Style.contentContainer}>
          <div>
            <EFImage
              className={Style.image}
              src="https://cdn.efuse.gg/static/images/efuse_logo_on_white.svg"
              width={40}
              height={40}
            />
          </div>
          <div className={Style.title}>
            <p className={Style.titleText}>eFuse</p>
            <p className={Style.linkText}>@{username}</p>
          </div>
          <div className={Style.follower}>
            <p className={Style.titleText}>Followers</p>
            <p className={Style.linkText}>{followers}</p>
          </div>
        </div>
      </EFCard>
    </div>
  );
};

export default EfuseCloutCard;
