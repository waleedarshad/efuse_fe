import React, { Component } from "react";

import { connect } from "react-redux";
import { withRouter } from "next/router";
import { getExternalSignupValues } from "../../store/actions/onboardingActions";

class TwitchAuth extends Component {
  componentDidMount() {
    this.authenticateTwitchAccount();
  }

  authenticateTwitchAccount = () => {
    const { code } = this.props.router.query;
    const { isLogin } = this.props.router.query;
    if (code !== undefined) {
      const data = { code, isLogin };
      this.props.getExternalSignupValues(data, "/auth/twitch", response => {
        window.opener?.parent?.postMessage({ twitch: true, ...response }, "*");
        window.close();
      });
    } else {
      window.opener?.parent?.postMessage(
        {
          success: false,
          data: {
            error: "Error while authenticating Twitch account.Please try again later.",
            type: "danger",
            title: "Error"
          }
        },
        "*"
      );
      window.close();
    }
  };

  render() {
    return (
      <>
        <h4>Authenticating Twitch Account ...</h4>
      </>
    );
  }
}

export default connect(null, { getExternalSignupValues })(withRouter(TwitchAuth));
