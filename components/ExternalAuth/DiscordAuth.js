import React, { Component } from "react";

import { connect } from "react-redux";
import { withRouter } from "next/router";
import { getExternalSignupValues } from "../../store/actions/onboardingActions";

class DiscordAuth extends Component {
  componentDidMount() {
    this.authenticateDiscordAccount();
  }

  authenticateDiscordAccount = () => {
    const { code } = this.props.router.query;
    const { isLogin } = this.props.router.query;
    if (code !== undefined) {
      const data = { code, isLogin };
      this.props.getExternalSignupValues(data, "/auth/discord", response => {
        window.opener?.parent?.postMessage({ discord: true, ...response }, "*");
        window.close();
      });
    } else {
      window.opener?.parent?.postMessage(
        {
          success: false,
          data: {
            error: "Error while authenticating Discord account. Please try again later.",
            type: "danger",
            title: "Error"
          }
        },
        "*"
      );
      window.close();
    }
  };

  render() {
    return (
      <>
        <h4>Authenticating Discord Account ...</h4>
      </>
    );
  }
}

export default connect(null, { getExternalSignupValues })(withRouter(DiscordAuth));
