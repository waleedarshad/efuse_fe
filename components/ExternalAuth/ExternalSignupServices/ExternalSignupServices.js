import { Col, Row } from "react-bootstrap";
import React, { Component } from "react";
import { connect } from "react-redux";
import getConfig from "next/config";
import { withRouter } from "next/router";
import OauthButton from "../../Settings/ExternalAccounts/OauthButton/OauthButton";
import Styles from "./ExternalSignupServices.module.scss";
import WindowPortal from "../../WindowPortal";
import { loginUser3rdParty } from "../../../store/actions/authActions";
import { sendNotification } from "../../../helpers/FlashHelper";
import { setExternalSignupValues } from "../../../store/actions/onboardingActions";

const { publicRuntimeConfig } = getConfig();
const SERVICES = ["twitch", "discord"];

class ExternalSignupServices extends Component {
  state = {
    authWindow: false,
    serviceURL: ""
  };

  componentDidMount() {
    window.addEventListener("message", this.manualAuthCallback);
  }

  openAuthScreen = service => {
    if (SERVICES.includes(service)) {
      const statesTobeUpdated = {
        authWindow: true,
        serviceURL: this.getServiceUrl()[`${service}URL`]
      };

      this.setState(statesTobeUpdated, () => {
        this.setState({ authWindow: false });
      });
    }
  };

  manualAuthCallback = event => {
    if (event.data && event.data.success) {
      if (this.props.isLogin || event.data.data.token) {
        const { router, customRedirect } = this.props;
        this.props.loginUser3rdParty(event.data, customRedirect || router.asPath);
      } else {
        this.props.setExternalSignupValues(event.data);
      }
    } else if (!event.data.success && event.data?.data) {
      sendNotification(event.data?.data.error, event.data?.data.type, event.data?.data.title);

      if (event.data.discord) {
        analytics.track("LOGIN_USER_DISCORD_FAILED");
      }
      if (event.data.twitch) {
        analytics.track("LOGIN_USER_TWITCH_FAILED");
      }
    }
  };

  componentWillUnmount() {
    window.removeEventListener("message", this.manualAuthCallback);
  }

  getServiceUrl = () => {
    const {
      defaultUrl,
      discordLoginURL,
      discordSignupURL,
      twitchClientID,
      twitchLoginRedirectUrl,
      twitchSignupRedirectUrl
    } = publicRuntimeConfig;
    return {
      discordURL: this.props.isLogin ? discordLoginURL : discordSignupURL,
      twitchURL: `https://id.twitch.tv/oauth2/authorize?client_id=${twitchClientID}&redirect_uri=${
        this.props.isLogin ? twitchLoginRedirectUrl : twitchSignupRedirectUrl
      }&response_type=code&scope=user:read:email`
    };
  };

  toggleAccount = service => {
    this.openAuthScreen(service);
  };

  render() {
    return (
      <>
        <Row className={Styles.authBtns}>
          <Col>
            <OauthButton
              onClick={() => {
                analytics.track("SIGNUP_DISCORD_BUTTON_CLICKED");
                this.toggleAccount("discord");
              }}
              styleClass={Styles.discordAuth}
              img="discord"
              iconClass={Styles.unLink}
              isSignup
            />
          </Col>
          <Col>
            <OauthButton
              onClick={() => {
                analytics.track("SIGNUP_TWITCH_BUTTON_CLICKED");
                this.toggleAccount("twitch");
              }}
              styleClass={Styles.twitchAuth}
              img="twitch"
              iconClass={Styles.unLink}
              isColumnized
            />
          </Col>
        </Row>
        {this.state.authWindow && WindowPortal(this.state.serviceURL, "Verification")}
      </>
    );
  }
}
export default connect(null, {
  setExternalSignupValues,
  loginUser3rdParty
})(withRouter(ExternalSignupServices));
