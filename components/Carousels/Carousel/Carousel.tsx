import React, { ReactNode, useEffect, useState, FC } from "react";
import Slider from "react-slick";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft, faChevronRight } from "@fortawesome/pro-regular-svg-icons";
import Style from "./Carousel.module.scss";
import { getDefaultCarouselSettings } from "../../../helpers/CarouselHelper";

/**
 @category Components
 @desc A sliding carousel component that wraps any container and accepts an array of items as props
 @param {Array} items An array of items used to check if the carousel has more values
*/

interface CarouselProps {
  items?: any;
  settings?: any;
  children: ReactNode;
}

const Carousel: FC<CarouselProps> = ({ items, settings, children }) => {
  const [localItems, setLocalItems] = useState([]);

  useEffect(() => {
    items && setLocalItems(items);
  }, []);

  const SlickArrowLeft = ({ currentSlide, slideCount, ...props }) => (
    <button {...props} className={currentSlide === 0 ? Style.hide : Style.arrowLeft} aria-hidden="true" type="button">
      <FontAwesomeIcon icon={faChevronLeft} />
    </button>
  );

  const SlickArrowRight = ({ currentSlide, slideCount, ...props }) => {
    const length = items?.length;
    return (
      <button
        {...props}
        className={length && length <= currentSlide + 2 ? Style.hide : Style.arrowRight}
        aria-hidden="true"
        type="button"
      >
        <FontAwesomeIcon icon={faChevronRight} className={Style.buttonIcon} />
      </button>
    );
  };

  if (!settings) {
    settings = getDefaultCarouselSettings(<SlickArrowLeft />, <SlickArrowRight />, localItems);
  } else {
    settings.prevArrow = <SlickArrowLeft />;
    settings.nextArrow = <SlickArrowRight />;
  }

  return (
    <div className={Style.container}>
      <Slider {...settings}>{children}</Slider>
    </div>
  );
};

export default Carousel;
