import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft, faChevronRight } from "@fortawesome/pro-solid-svg-icons";

import Style from "./CustomCarousel.module.scss";
import CardBadge from "../../Cards/CardBadge/CardBadge";

// Note: to use this component you must have at least 5 items in your itemsList
import Content from "./CarouselContent/CarouselContent";
import SideCardHighLight from "./SideCardHighLight";

class CustomCarousel extends Component {
  state = {
    itemsList: [],
    spot: 0,
    spotSecondaryLeft: 0,
    spotSecondaryRight: 0,
    spotTertiaryLeft: 0,
    spotTertiaryRight: 0,
    animationClass: 0,
    animation: "",
    animationFinished: false
  };

  static getDerivedStateFromProps(props, state) {
    return props?.itemsList?.length > 0 && props?.itemsList?.length !== state?.itemsList?.length
      ? {
          itemsList: props.itemsList,
          spot: 0,
          spotSecondaryLeft: props.itemsList.length - 1,
          spotSecondaryRight: 1,
          spotTertiaryLeft: props.itemsList.length - 2,
          spotTertiaryRight: 2
        }
      : null;
  }

  startAnimation = () => {
    this.props.sideCardHighlight && this.setState({ animation: Style.scrollLeftAnimation });
  };

  onAnimationStart = () => {
    this.setState({
      animationFinished: false
    });
  };

  onAnimationEnd = () => {
    this.setState({
      animation: "",
      animationFinished: true
    });
  };

  updateClickPostiton = (itemsList, spotClicked) => {
    let newPosition = spotClicked;
    if (spotClicked === -1) {
      newPosition = itemsList.length - 1;
    }
    if (spotClicked === -2) {
      newPosition = itemsList.length - 2;
    }
    if (spotClicked - itemsList.length === 0) {
      newPosition = 0;
    }
    if (spotClicked - itemsList.length === 1) {
      newPosition = 1;
    }
    return newPosition;
  };

  moveArrayPostition = spotClicked => {
    this.startAnimation();
    const { itemsList, spot } = this.state;
    const newSpot = 0;
    let newSpotSecondaryLeft = 0;
    let newSpotSecondaryRight = 0;
    let newSpotTertiaryLeft = 0;
    let newSpotTertiaryRight = 0;

    newSpotSecondaryLeft = this.updateClickPostiton(itemsList, spotClicked - 1);
    newSpotTertiaryLeft = this.updateClickPostiton(itemsList, spotClicked - 2);

    newSpotSecondaryRight = this.updateClickPostiton(itemsList, spotClicked + 1);
    newSpotTertiaryRight = this.updateClickPostiton(itemsList, spotClicked + 2);

    this.setState({
      spot: spotClicked,
      spotSecondaryLeft: newSpotSecondaryLeft,
      spotSecondaryRight: newSpotSecondaryRight,
      spotTertiaryLeft: newSpotTertiaryLeft,
      spotTertiaryRight: newSpotTertiaryRight
    });
  };

  getLeftValue = (array, currentSpot) => {
    if (currentSpot === 0) {
      return array.length - 1;
    }
    return currentSpot - 1;
  };

  moveLeft = () => {
    const { itemsList, spot, spotSecondaryLeft, spotSecondaryRight, spotTertiaryLeft, spotTertiaryRight } = this.state;
    this.startAnimation();

    const newSpot = this.getLeftValue(itemsList, spot);
    const newSpotSecondaryLeft = this.getLeftValue(itemsList, spotSecondaryLeft);
    const newSpotSecondaryRight = this.getLeftValue(itemsList, spotSecondaryRight);
    const newSpotTertiaryLeft = this.getLeftValue(itemsList, spotTertiaryLeft);
    const newSpotTertiaryRight = this.getLeftValue(itemsList, spotTertiaryRight);

    this.setState({
      spot: newSpot,
      spotSecondaryLeft: newSpotSecondaryLeft,
      spotSecondaryRight: newSpotSecondaryRight,
      spotTertiaryLeft: newSpotTertiaryLeft,
      spotTertiaryRight: newSpotTertiaryRight
    });
  };

  getRightValue = (array, currentSpot) => {
    if (currentSpot === array.length - 1) {
      return 0;
    }
    return currentSpot + 1;
  };

  moveRight = () => {
    const { itemsList, spot, spotSecondaryLeft, spotSecondaryRight, spotTertiaryLeft, spotTertiaryRight } = this.state;
    this.startAnimation();

    const newSpot = this.getRightValue(itemsList, spot);
    const newSpotSecondaryLeft = this.getRightValue(itemsList, spotSecondaryLeft);
    const newSpotSecondaryRight = this.getRightValue(itemsList, spotSecondaryRight);
    const newSpotTertiaryLeft = this.getRightValue(itemsList, spotTertiaryLeft);
    const newSpotTertiaryRight = this.getRightValue(itemsList, spotTertiaryRight);

    this.setState({
      spot: newSpot,
      spotSecondaryLeft: newSpotSecondaryLeft,
      spotSecondaryRight: newSpotSecondaryRight,
      spotTertiaryLeft: newSpotTertiaryLeft,
      spotTertiaryRight: newSpotTertiaryRight
    });
  };

  render() {
    const { sideCardHighlight, iconLeft, iconRight } = this.props;
    const {
      itemsList,
      spot,
      spotSecondaryLeft,
      spotSecondaryRight,
      spotTertiaryLeft,
      spotTertiaryRight,
      animation
    } = this.state;

    if (!(itemsList?.length > 0)) {
      return <></>;
    }
    return (
      <>
        <div className={Style.carousel}>
          <div className={Style.leftButton} onClick={animation === "" && this.moveLeft}>
            <FontAwesomeIcon icon={iconLeft || faChevronLeft} />
          </div>
          <div className={Style.content}>
            {itemsList.map((data, index) => {
              const currentItem = itemsList[spot];
              return (
                <>
                  {sideCardHighlight && (
                    <div
                      className={`${Style.highlightLeft} ${this.state.animation}`}
                      onAnimationEnd={this.onAnimationEnd}
                      onAnimationStart={this.onAnimationStart}
                    >
                      <img
                        className={Style.sideBackgroundImage}
                        src="https://cdn.efuse.gg/uploads/static/global/opportunity_background_image.jpg"
                      />
                      <SideCardHighLight currentItem={currentItem} />
                    </div>
                  )}
                  <div
                    className={`${Style.card}
                    ${spot === index && Style.main}
                    ${spotTertiaryLeft === index && !sideCardHighlight && Style.tertiaryLeft}
                    ${spotSecondaryLeft === index && Style.secondaryLeft}
                    ${spotSecondaryRight === index && Style.secondaryRight}
                    ${spotTertiaryRight === index && Style.tertiaryRight}
                  `}
                    key={index}
                  >
                    {(spotSecondaryLeft === index ||
                      spotSecondaryRight === index ||
                      (spotTertiaryLeft === index && !sideCardHighlight) ||
                      spotTertiaryRight === index) && (
                      <div
                        className={Style.overlay}
                        onClick={() => animation === "" && this.moveArrayPostition(index)}
                      />
                    )}
                    <CardBadge badge={itemsList[index]?.type} largeCard />
                    <Content item={itemsList[index]} autoplay={spot === index} muted={spot === index} />
                  </div>
                </>
              );
            })}
          </div>
          <div className={Style.rightButton} onClick={animation === "" && this.moveRight}>
            <FontAwesomeIcon icon={iconRight || faChevronRight} />
          </div>
        </div>
      </>
    );
  }
}

export default CustomCarousel;
