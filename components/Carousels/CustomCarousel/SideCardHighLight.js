import Link from "next/link";
import Style from "./CustomCarousel.module.scss";

const SideCardHighLight = ({ currentItem }) => {
  return (
    <div className={Style.sideText}>
      <h3 className={Style.sideTitle}>{currentItem?.title}</h3>
      <p className={Style.sideSubTitle}>{currentItem?.subTitle}</p>
      <p className={Style.sideDescription}>{currentItem?.description}</p>
      <p className={Style.sideStats}>{currentItem?.stat}</p>
      <div className={Style.actionButtons}>
        {currentItem?.internalRedirect ? (
          <Link href={currentItem?.redirectLink}>
            <p className={Style.sideLeftButton}>View</p>
          </Link>
        ) : (
          <a target="_blank" href={currentItem?.redirectLink} className={Style.removeUnderline}>
            <p className={Style.sideLeftButton}>View</p>
          </a>
        )}
      </div>
    </div>
  );
};

export default SideCardHighLight;
