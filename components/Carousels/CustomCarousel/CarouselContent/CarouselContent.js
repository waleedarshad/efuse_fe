import Link from "next/link";
import Style from "./CarouselContent.module.scss";
import TwitchPlayerIframe from "../../../TwitchPlayerIframe/TwitchPlayerIframe";
import VideoPlayer from "../../../VideoPlayer/VideoPlayer";

const Content = ({ item, autoplay, muted }) => {
  const getContent = () => {
    if (item?.stream) {
      return <TwitchPlayerIframe twitchChannel={item?.stream} autoplay={false} muted />;
    }
    if (item?.videoUrl) {
      return <VideoPlayer url={item?.videoUrl} height="100%" autoplay={autoplay} muted={muted} />;
    }
    if (item?.imageUrl) {
      const itemImage = <img className={Style.centerImage} src={item?.imageUrl} alt="center carousel" />;
      return item?.internalRedirect ? (
        <Link href={item?.redirectLink}>{itemImage}</Link>
      ) : (
        <a target="_blank" rel="noopener noreferrer" href={item?.redirectLink} className={Style.removeUnderline}>
          {itemImage}
        </a>
      );
    }
    return <></>;
  };
  return <div className={Style.centerCardContent}>{getContent()}</div>;
};
export default Content;
