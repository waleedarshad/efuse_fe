import intersection from "lodash/intersection";
import { useRouter } from "next/router";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { ALL_SUBTYPES } from "../../common/opportunities";
import {
  searchOpportunitiesBySubType,
  setOpportunitiesFilters
} from "../../store/actions/opportunitiesv2/opportunitiesv2Actions";
import AuthAwareLayout from "../Layouts/AuthAwareLayout/AuthAwareLayout";
import FilterChips from "./FilterChips/FilterChips";
import OpportunitiesBrowseLayout from "./OpportunitiesBrowseLayout/OpportunitiesBrowseLayout";
import OpportunitiesHeader from "./OpportunitiesHeader/OpportunitiesHeader";

const parseSubTypes = subTypesQueryParams => {
  const subTypes = Array.isArray(subTypesQueryParams) ? subTypesQueryParams : [subTypesQueryParams];

  return intersection(ALL_SUBTYPES, subTypes);
};

const OpportunitiesBrowse = () => {
  const dispatch = useDispatch();

  const router = useRouter();
  const pagination = useSelector(state => state.opportunitiesv2.pagination) || [];

  const subTypesQueryParams = router.query.subType;
  const subTypeList = parseSubTypes(subTypesQueryParams);

  const searchOpportunities = page => {
    dispatch(searchOpportunitiesBySubType(page, 10, subTypeList));
    window.scrollTo({ top: 0, behavior: "smooth" });
  };

  useEffect(() => {
    dispatch(setOpportunitiesFilters(subTypeList));
    searchOpportunities(1);

    return () => dispatch(setOpportunitiesFilters([]));
  }, [router.asPath]);

  useEffect(() => {
    analytics.page("Opportunities Browse");
  }, []);

  const opportunities = useSelector(state => state.opportunitiesv2.opportunities) || [];

  return (
    <AuthAwareLayout metaTitle="eFuse | Opportunities" containsSubheader>
      <OpportunitiesHeader />
      <FilterChips filters={subTypeList} />
      <OpportunitiesBrowseLayout
        totalOpportunities={pagination?.totalDocs}
        opportunities={opportunities}
        totalPages={pagination?.totalPages}
        searchOpportunities={searchOpportunities}
      />
    </AuthAwareLayout>
  );
};

export default OpportunitiesBrowse;
