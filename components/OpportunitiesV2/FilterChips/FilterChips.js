import uniqueId from "lodash/uniqueId";
import { useDispatch } from "react-redux";
import { navigateToBrowse } from "../../../store/actions/opportunitiesv2/opportunitiesv2Actions";
import EFChip from "../../EFChip/EFChip";
import HorizontalScroll from "../../HorizontalScroll/HorizontalScroll";
import Style from "./FilterChips.module.scss";

const FilterChips = ({ filters }) => {
  const dispatch = useDispatch();

  const goToBrowse = filterToRemove => {
    const appliedFilters = filters.filter(filter => filter !== filterToRemove);

    dispatch(navigateToBrowse(appliedFilters));
  };

  return (
    <div className={Style.container}>
      <HorizontalScroll withLeftArrow={false} withRightArrow={false}>
        {filters.map(filter => (
          <div className={Style.chip} key={uniqueId()}>
            <EFChip.Removable content={filter.toUpperCase()} onRemove={() => goToBrowse(filter)} colorTheme="dark" />
          </div>
        ))}
      </HorizontalScroll>
    </div>
  );
};

export default FilterChips;
