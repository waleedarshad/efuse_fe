import uniqueId from "lodash/uniqueId";
import { useEffect, useRef } from "react";
import { Col, Container, Row } from "react-bootstrap";
import { useSelector } from "react-redux";
import EFPaginationBar from "../../EFPaginationBar/EFPaginationBar";
import EFSideOpportunities from "../../EFSideOpportunities/EFSideOpportunities";
import OpportunitySummaryCard from "../OpportunityCards/OpportunitySummaryCard/OpportunitySummaryCard";
import OpportunitySummaryCompactCard from "../OpportunityCards/OpportunitySummaryCompactCard/OpportunitySummaryCompactCard";
import Style from "./OpportunitiesBrowseLayout.module.scss";

const OpportunitiesBrowseLayout = ({ totalOpportunities, opportunities, totalPages, searchOpportunities }) => {
  const { filters } = useSelector(state => state.opportunitiesv2);
  const currentUserId = useSelector(state => state.auth.currentUser?._id);

  const { isCompactViewOn } = useSelector(state => state.opportunitiesv2);

  const paginationRef = useRef();

  useEffect(() => {
    paginationRef?.current?.goToPageNumber(1);
  }, [filters]);

  return (
    <Container>
      <Row>
        <Col md={4} lg={3}>
          <EFSideOpportunities />
        </Col>

        <Col md={8} lg={9}>
          <p className={Style.total}> {totalOpportunities} total opportunities</p>
          {opportunities.map(opp => (
            <div key={uniqueId()} className={Style.opportunityCard}>
              {isCompactViewOn ? (
                <OpportunitySummaryCompactCard opportunity={opp} />
              ) : (
                <OpportunitySummaryCard opportunity={opp} />
              )}
            </div>
          ))}
          {totalPages > 1 && (
            <EFPaginationBar
              ref={paginationRef}
              totalPages={totalPages}
              onPageChange={searchOpportunities}
              marginPages={1}
              pageRange={2}
            />
          )}
        </Col>
      </Row>
    </Container>
  );
};

export default OpportunitiesBrowseLayout;
