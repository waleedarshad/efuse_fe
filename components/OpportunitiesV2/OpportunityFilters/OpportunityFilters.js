import intersection from "lodash/intersection";
import React, { useEffect, useState } from "react";
import { SUBTYPES } from "../../../common/opportunities";
import EFGeneralAccordion from "../../Accordions/EFGeneralAccordion/EFGeneralAccordion";
import EFCheckboxLabel from "../../EFCheckboxLabel/EFCheckboxLabel";
import Style from "./OpportunityFilters.module.scss";

const filtersInfo = [
  {
    title: "Jobs",
    subtypes: Object.values(SUBTYPES.JOB)
  },
  {
    title: "Events",
    subtypes: Object.values(SUBTYPES.EVENT)
  },
  {
    title: "Team Openings",
    subtypes: Object.values(SUBTYPES.TEAM_OPENING)
  },
  {
    title: "Scholarships",
    subtypes: Object.values(SUBTYPES.SCHOLARSHIP)
  }
];

const OpportunityFilters = ({ initialFilterSelection, onFiltersChange }) => {
  const [selectedFilters, setSelectedFilters] = useState(initialFilterSelection);

  useEffect(() => {
    onFiltersChange(selectedFilters);
  }, [selectedFilters]);

  return (
    <div className={Style.sectionWrapper}>
      <p className={Style.sectionTitle}>Opportunities</p>
      <div className={Style.contentWrapper}>
        {filtersInfo.map((filterInfo, index) => {
          const { title, subtypes } = filterInfo;

          const isAnyFilterSelectedInAccordion = intersection(selectedFilters, subtypes).length > 0;
          const noFilterIsSelected = selectedFilters.length === 0;
          const isFirstAccordion = index === 0;

          const isAccordionOpen = isAnyFilterSelectedInAccordion || (noFilterIsSelected && isFirstAccordion);

          return (
            <EFGeneralAccordion key={title} text={title} isOpenOnMount={isAccordionOpen}>
              <div className={Style.checkboxWrapper}>
                {subtypes.map(subtype => {
                  const isFilterSelected = selectedFilters.includes(subtype);
                  return (
                    <EFCheckboxLabel
                      isChecked={isFilterSelected}
                      label={subtype}
                      key={subtype}
                      onChecked={() => setSelectedFilters([...selectedFilters, subtype])}
                      onRemoveChecked={() => setSelectedFilters(selectedFilters.filter(filter => filter !== subtype))}
                    />
                  );
                })}
              </div>
            </EFGeneralAccordion>
          );
        })}
      </div>
    </div>
  );
};

OpportunityFilters.defaultProps = {
  initialFilterSelection: []
};

export default OpportunityFilters;
