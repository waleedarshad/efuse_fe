import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  getCurrentUserOwnedOpportunities,
  setOpportunitiesFilters
} from "../../store/actions/opportunitiesv2/opportunitiesv2Actions";
import Internal from "../Layouts/Internal/Internal";
import OpportunitiesBrowseLayout from "./OpportunitiesBrowseLayout/OpportunitiesBrowseLayout";
import OpportunitiesHeader from "./OpportunitiesHeader/OpportunitiesHeader";

const OpportunitiesOwned = () => {
  const dispatch = useDispatch();

  const pagination = useSelector(state => state.opportunitiesv2.pagination) || [];

  const searchOpportunities = page => {
    dispatch(getCurrentUserOwnedOpportunities(page, 10));
    window.scrollTo({ top: 0, behavior: "smooth" });
  };

  useEffect(() => {
    searchOpportunities(1);

    return () => dispatch(setOpportunitiesFilters([]));
  }, []);

  useEffect(() => {
    analytics.page("Opportunities Created");
  }, []);

  const opportunities = useSelector(state => state.opportunitiesv2.opportunities) || [];

  return (
    <Internal metaTitle="eFuse | Opportunities" containsSubheader>
      <OpportunitiesHeader isFilterDisplayed={false} />
      <OpportunitiesBrowseLayout
        totalOpportunities={pagination?.totalDocs}
        opportunities={opportunities}
        totalPages={pagination?.totalPages}
        searchOpportunities={searchOpportunities}
      />
    </Internal>
  );
};

export default OpportunitiesOwned;
