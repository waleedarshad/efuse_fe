import { useDispatch, useSelector } from "react-redux";
import { toggleCompactView } from "../../../../store/actions/opportunitiesv2/opportunitiesv2Actions";
import EFPillButton from "../../../Buttons/EFPillButton/EFPillButton";
import Switch from "../../../Switch/Switch";
import Style from "./CompactToggle.module.scss";

const CompactToggle = () => {
  const dispatch = useDispatch();
  const isCompactViewOn = useSelector(state => state.opportunitiesv2.isCompactViewOn);

  return (
    <EFPillButton
      text={
        <div className={Style.compactToggle}>
          <p>COMPACT</p>
          <Switch name="compactToggle" value={isCompactViewOn} checked={isCompactViewOn} />
        </div>
      }
      colorTheme="light"
      shadowTheme="small"
      size="medium"
      onClick={() => dispatch(toggleCompactView())}
    />
  );
};

export default CompactToggle;
