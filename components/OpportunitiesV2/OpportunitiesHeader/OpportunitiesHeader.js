import uniqueId from "lodash/uniqueId";
import { useRouter } from "next/router";
import { faGamepadAlt, faPlus } from "@fortawesome/pro-solid-svg-icons";

import { OPPORTUNITIES_HREFS } from "../../../common/opportunities";
import { isUserLoggedIn } from "../../../helpers/AuthHelper";
import EFCircleIconButtonTooltip from "../../Buttons/EFCircleIconButtonTooltip/EFCircleIconButtonTooltip";
import EFSubHeader from "../../Layouts/Internal/EFSubHeader/EFSubHeader";
import OpportunityFilterButton from "../OpportunityFilterButton/OpportunityFilterButton";
import OpportunityFilterModal from "../OpportunityFilterModal/OpportunityFilterModal";
import CompactToggle from "./CompactToggle/CompactToggle";
import Style from "./OpportunitiesHeader.module.scss";

const publicNavigation = [
  {
    href: OPPORTUNITIES_HREFS.ALL,
    name: "all"
  },
  {
    href: OPPORTUNITIES_HREFS.BROWSE,
    name: "browse"
  }
];

const loggedInNavigation = [
  ...publicNavigation,
  ...[
    {
      href: OPPORTUNITIES_HREFS.APPLIED,
      name: "applied"
    },
    {
      href: OPPORTUNITIES_HREFS.OWNED,
      name: "created opportunities"
    }
  ]
];

const OpportunitiesHeader = ({ isFilterDisplayed, isCompactToggleDisplayed }) => {
  const router = useRouter();

  const navigationListInfo = isUserLoggedIn() ? loggedInNavigation : publicNavigation;

  const natigationList = navigationListInfo.map(item => ({
    ...item,
    isActive: router.pathname === item.href
  }));

  return (
    <EFSubHeader
      headerIcon={faGamepadAlt}
      navigationList={natigationList}
      actionButtons={[
        isCompactToggleDisplayed ? (
          <div className={Style.compactToggleWrapper}>
            <CompactToggle key={uniqueId()} />
          </div>
        ) : (
          <></>
        ),
        isFilterDisplayed ? <OpportunityFilterButton key={uniqueId()} /> : <></>,
        <EFCircleIconButtonTooltip
          key={uniqueId()}
          icon={faPlus}
          colorTheme="primary"
          shadowTheme="small"
          size="medium"
          text="Create Opportunity"
          internalHref="/opportunities/create"
          tooltipContent="Create an Opportunity"
        />,
        <OpportunityFilterModal key={2} />
      ]}
    />
  );
};

OpportunitiesHeader.defaultProps = {
  isCompactToggleDisplayed: true,
  isFilterDisplayed: true
};

export default OpportunitiesHeader;
