import { useEffect } from "react";
import { useSelector } from "react-redux";
import { InstantSearch, Configure } from "react-instantsearch-dom";
import algoliasearch from "algoliasearch/lite";
import getConfig from "next/config";

import AuthAwareLayout from "../Layouts/AuthAwareLayout/AuthAwareLayout";
import OpportunitiesHeader from "./OpportunitiesHeader/OpportunitiesHeader";
import OpportunityBuckets from "./OpportunityBuckets/OpportunityBuckets";
import OpportunityListLayout from "./OpportunityListLayout/OpportunityListLayout";
import { getIndexName } from "../../helpers/AlgoliaHelper";

const { publicRuntimeConfig } = getConfig();
const { algoliaApplicationId, algoliaSearchApiKey } = publicRuntimeConfig;

const OpportunitiesLanding = () => {
  const algoliaIndexName = getIndexName("OPPORTUNITIES");
  const searchClient = algoliasearch(algoliaApplicationId, algoliaSearchApiKey);
  const currentUser = useSelector(state => state.auth.currentUser);

  useEffect(() => {
    analytics.page("Opportunities Landing Page");
  }, []);

  return (
    <AuthAwareLayout metaTitle="eFuse | Opportunities" containsSubheader>
      <InstantSearch searchClient={searchClient} indexName={algoliaIndexName}>
        <Configure
          enablePersonalization
          userToken={currentUser?._id}
          hitsPerPage={20}
          distinct={false}
          analytics={false}
        />
        <OpportunitiesHeader isCompactToggleDisplayed={false} />
        <OpportunityBuckets />
        <OpportunityListLayout listTitle="Opportunities" />
      </InstantSearch>
    </AuthAwareLayout>
  );
};

export default OpportunitiesLanding;
