import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useDispatch, useSelector } from "react-redux";
import { faCircle } from "@fortawesome/pro-solid-svg-icons";

import { toggleOpportunitiesFilterModal } from "../../../store/actions/opportunitiesv2/opportunitiesv2Actions";
import EFPillButton from "../../Buttons/EFPillButton/EFPillButton";
import Style from "./OpportunityFilterButton.module.scss";

const OpportunityFilterButton = () => {
  const dispatch = useDispatch();
  const isAnyFilterSelected = useSelector(state => state.opportunitiesv2.filters).length > 0;

  return (
    <EFPillButton
      text={
        <span className={Style.filterButton}>
          FILTERS
          <FontAwesomeIcon
            icon={faCircle}
            className={`${Style.filterButtonIcon} ${isAnyFilterSelected ? Style.active : ""}`}
          />
        </span>
      }
      colorTheme="light"
      shadowTheme="small"
      size="medium"
      onClick={() => dispatch(toggleOpportunitiesFilterModal())}
    />
  );
};

export default OpportunityFilterButton;
