import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { faTimes } from "@fortawesome/pro-solid-svg-icons";

import {
  navigateToBrowse,
  toggleOpportunitiesFilterModal
} from "../../../store/actions/opportunitiesv2/opportunitiesv2Actions";
import EFCircleIconButton from "../../Buttons/EFCircleIconButton/EFCircleIconButton";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";
import EFCard from "../../Cards/EFCard/EFCard";
import OpportunityFilters from "../OpportunityFilters/OpportunityFilters";
import Style from "./OpportunityFilterModal.module.scss";

const OpportunityFilterModal = () => {
  const isFilterOpen = useSelector(state => state.opportunitiesv2.isFilterModalOpen);
  const initialFilterSelection = useSelector(state => state.opportunitiesv2.filters);

  const [appliedFilters, setAppliedFilters] = useState(initialFilterSelection);

  const dispatch = useDispatch();

  const goToBrowse = () => {
    analytics.track("OPPORTUNITY_FILTERS_APPLY_BUTTON_CLICKED", {});
    dispatch(navigateToBrowse(appliedFilters));
    dispatch(toggleOpportunitiesFilterModal());
  };

  const isAnyFilterSelected = appliedFilters.length > 0;

  if (!isFilterOpen) {
    return <></>;
  }

  return (
    <>
      <div className={Style.overlay} onClick={() => dispatch(toggleOpportunitiesFilterModal())} />
      <div className={Style.filterModalWrapper}>
        <EFCard shadow="large">
          <div className={Style.filterCardContent}>
            <div className={Style.filterHeaderWrapper}>
              <p className={Style.filterHeaderText}>Filters</p>
              <div className={Style.closeButtonWrapper}>
                <EFCircleIconButton
                  colorTheme="light"
                  onClick={() => dispatch(toggleOpportunitiesFilterModal())}
                  icon={faTimes}
                  size="medium"
                  shadowTheme="medium"
                />
              </div>
            </div>
            <OpportunityFilters initialFilterSelection={initialFilterSelection} onFiltersChange={setAppliedFilters} />
          </div>
          <div className={Style.applyButtonWrapper}>
            <EFRectangleButton
              onClick={goToBrowse}
              width="fullWidth"
              colorTheme="secondary"
              fontCaseTheme="upperCase"
              text="Apply My Selected Filters"
              buttonType="submit"
              disabled={!isAnyFilterSelected}
            />
          </div>
        </EFCard>
      </div>
    </>
  );
};

export default OpportunityFilterModal;
