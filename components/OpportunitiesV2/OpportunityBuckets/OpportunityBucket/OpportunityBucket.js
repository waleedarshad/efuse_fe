import React from "react";
import EFImage from "../../../EFImage/EFImage";
import Style from "./OpportunityBucket.module.scss";

const OpportunityBucket = ({ onClick, src, bucketText, alt, theme }) => {
  return (
    <button type="button" className={Style.bucket} onClick={onClick}>
      <div className={`${Style.bucketOverlay} ${Style[theme]}`} />
      <div className={Style.bucketBackground}>
        <EFImage src={src} className={Style.bucketImage} alt={alt} />
      </div>
      <p className={Style.bucketText}>{bucketText}</p>
    </button>
  );
};

export default OpportunityBucket;
