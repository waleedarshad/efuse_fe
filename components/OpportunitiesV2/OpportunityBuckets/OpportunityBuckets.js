import React from "react";
import { useDispatch } from "react-redux";
import { SUBTYPES } from "../../../common/opportunities";
import { navigateToBrowse } from "../../../store/actions/opportunitiesv2/opportunitiesv2Actions";
import OpportunityBucket from "./OpportunityBucket/OpportunityBucket";
import Style from "./OpportunityBuckets.module.scss";

const OpportunityBuckets = () => {
  const dispatch = useDispatch();

  return (
    <div className={Style.bucketsContainer}>
      <OpportunityBucket
        onClick={() => {
          analytics.track("OPPORTUNITY_BUCKET_CLICKED", { bucketType: "team-openings" });
          dispatch(navigateToBrowse(Object.values(SUBTYPES.TEAM_OPENING)));
        }}
        src="https://cdn.efuse.gg/uploads/opportunities/buckets/teamOpening.jpg"
        bucketText="Team Openings"
        alt="Team Opening Bucket"
        theme="teamOpening"
      />
      <OpportunityBucket
        onClick={() => {
          analytics.track("OPPORTUNITY_BUCKET_CLICKED", { bucketType: "events" });
          dispatch(navigateToBrowse(Object.values(SUBTYPES.EVENT)));
        }}
        src="https://cdn.efuse.gg/uploads/opportunities/buckets/event.jpg"
        bucketText="Events"
        alt="Events Bucket"
        theme="event"
        active
      />
      <OpportunityBucket
        onClick={() => {
          analytics.track("OPPORTUNITY_BUCKET_CLICKED", { bucketType: "scholarships" });
          dispatch(navigateToBrowse(Object.values(SUBTYPES.SCHOLARSHIP)));
        }}
        src="https://cdn.efuse.gg/uploads/opportunities/buckets/scholarship.jpg"
        bucketText="Scholarships"
        alt="Scholarships Bucket"
        theme="scholarship"
      />
      <OpportunityBucket
        onClick={() => {
          analytics.track("OPPORTUNITY_BUCKET_CLICKED", { bucketType: "jobs" });
          dispatch(navigateToBrowse(Object.values(SUBTYPES.JOB)));
        }}
        src="https://cdn.efuse.gg/uploads/opportunities/buckets/job.jpg"
        bucketText="Jobs"
        alt="Employment Bucket"
        theme="job"
      />
    </div>
  );
};

export default OpportunityBuckets;
