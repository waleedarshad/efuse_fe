import Style from "./OpportunityTypeBadge.module.scss";

const badgeThemes = {
  Event: "event",
  Job: "job",
  "Team Opening": "teamOpening",
  Scholarship: "scholarship"
};

const OpportunityTypeBadge = ({ opportunityType }) => {
  const getTheme = badgeThemes[opportunityType];
  return <div className={Style[getTheme]}>{opportunityType}</div>;
};

export default OpportunityTypeBadge;
