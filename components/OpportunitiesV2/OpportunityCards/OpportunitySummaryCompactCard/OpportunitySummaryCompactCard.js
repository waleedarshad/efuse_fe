import ReactTimeago from "react-timeago";
import VisibilitySensor from "react-visibility-sensor";
import { getFullOpportunityUrl } from "../../../../helpers/UrlHelper";
import EFCard from "../../../Cards/EFCard/EFCard";
import EFAvatarList from "../../../EFAvatarList/EFAvatarList";
import EFScaledImage from "../../../EFScaledImage/EFScaledImage";
import ApplicantStatusButton from "../../../Opportunities/OpportunityWrapper/ApplicantStatusButton/ApplicantStatusButton";
import OpportunityContextMenu from "../../../Opportunities/OpportunityWrapper/OpportunityContextMenu/OpportunityContextMenu";
import OpportunityTypeBadge from "../OpportunityTypeBadge/OpportunityTypeBadge";
import Style from "./OpportunitySummaryCompactCard.module.scss";

const OpportunitySummaryCompactCard = ({ opportunity }) => {
  const { onBehalfOfOrganization, associatedOrganization } = opportunity;

  return (
    <VisibilitySensor
      delayedCall
      onChange={isVisible => {
        if (isVisible) {
          analytics.track("OPPORTUNITY_BROWSE_LIST_VIEW", {
            opportunityId: opportunity._id,
            opportunityType: opportunity?.opportunityType,
            externalSource: opportunity?.external_source ? opportunity.external_source : "none"
          });
        }
      }}
    >
      <EFCard widthTheme="fullWidth" overflowTheme="visible">
        <div className={Style.container}>
          <div className={Style.imageContainer}>
            <div className={Style.image}>
              <EFScaledImage imageSrc={opportunity.image?.url} />
            </div>
            <div className={Style.opportunityBadge}>
              {/* TODO: Remove employment opening check once data has been migrated */}
              <OpportunityTypeBadge
                opportunityType={
                  opportunity.opportunityType === "Employment Opening" ? "Job" : opportunity.opportunityType
                }
              />
            </div>
          </div>
          <div className={Style.textContainer}>
            <h3 className={Style.title}>
              <a className={Style.title} href={getFullOpportunityUrl(opportunity)}>
                {opportunity.title}
              </a>
            </h3>
            <div className={Style.subtitleContainer}>
              {opportunity.external ? (
                <inline className={Style.subTitle}>{opportunity.company_name}</inline>
              ) : (
                <inline className={Style.subTitle}>
                  {onBehalfOfOrganization ? (
                    <a href={`/organizations/show?id=${associatedOrganization?._id}`}>{associatedOrganization?.name}</a>
                  ) : (
                    <a href={`/users/posts?id=${opportunity.user}`}>{opportunity.self}</a>
                  )}
                </inline>
              )}
              <p className={Style.createdAt}>
                Created <ReactTimeago date={opportunity.createdAt} />
              </p>
            </div>
          </div>
          <div className={Style.avatarList}>
            <EFAvatarList imageSrcList={opportunity.applicantAvatars.map(applicant => applicant.url)} />
          </div>
          <div className={Style.applyButton}>
            <ApplicantStatusButton opportunity={opportunity} />
          </div>
          <OpportunityContextMenu opportunity={opportunity} wrapperClass={Style.menu} />
        </div>
      </EFCard>
    </VisibilitySensor>
  );
};

export default OpportunitySummaryCompactCard;
