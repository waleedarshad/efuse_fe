import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronDown, faMapMarkerAlt, faChevronUp } from "@fortawesome/pro-solid-svg-icons";
import Router from "next/router";
import { useState } from "react";
import { OverlayTrigger, Tooltip } from "react-bootstrap";
import { useSelector } from "react-redux";
import VisibilitySensor from "react-visibility-sensor";

import { getFullOpportunityUrl } from "../../../../helpers/UrlHelper";
import EFCard from "../../../Cards/EFCard/EFCard";
import EFAvatarList from "../../../EFAvatarList/EFAvatarList";
import { EFHtmlParser } from "../../../EFHtmlParser/EFHtmlParser";
import ApplicantStatusButton from "../../../Opportunities/OpportunityWrapper/ApplicantStatusButton/ApplicantStatusButton";
import OpportunityContextMenu from "../../../Opportunities/OpportunityWrapper/OpportunityContextMenu/OpportunityContextMenu";
import OpportunityImage from "../OpportunityImage/OpportunityImage";
import Style from "./OpportunitySummaryCard.module.scss";

const OpportunitySummaryCard = ({ opportunity }) => {
  const [isExpanded, setIsExpanded] = useState(false);
  const currentUser = useSelector(state => state.auth.currentUser);

  const { onBehalfOfOrganization, associatedOrganization } = opportunity;

  return (
    <VisibilitySensor
      delayedCall
      onChange={isVisible => {
        if (isVisible) {
          analytics.track("OPPORTUNITY_BROWSE_LIST_VIEW", {
            opportunityId: opportunity._id,
            opportunityType: opportunity?.opportunityType,
            externalSource: opportunity?.external_source ? opportunity.external_source : "none"
          });
        }
      }}
    >
      <EFCard widthTheme="fullWidth" overflowTheme="visible">
        <div className={Style.container}>
          <div className={Style.body}>
            <div className={Style.contextMenuTopRight}>
              <OpportunityContextMenu opportunity={opportunity} wrapperClass={Style.menu} />
            </div>

            <div className={Style.imageContainer}>
              <OpportunityImage opportunity={opportunity} />
              {currentUser?.roles?.includes("admin") && (
                <b className={Style.rank}>Rank: {opportunity?.metadata?.rank}</b>
              )}
              <div className={Style.cardAvatarList}>
                <EFAvatarList imageSrcList={opportunity.applicantAvatars?.map(avatar => avatar.url)} />
              </div>
            </div>

            <div className={`${Style.textContainer} ${isExpanded && Style.expanded}`}>
              <div className={Style.contextMenuMiddleRight}>
                <OpportunityContextMenu opportunity={opportunity} wrapperClass={Style.menu} />
              </div>
              <div className={Style.textWrapper}>
                {opportunity.external ? (
                  <inline className={Style.subTitle}>{opportunity.company_name}</inline>
                ) : (
                  <inline className={Style.subTitle}>
                    {onBehalfOfOrganization ? (
                      <a href={`/organizations/show?id=${associatedOrganization?._id}`}>
                        {associatedOrganization?.name}
                      </a>
                    ) : (
                      <a href={`/users/posts?id=${opportunity.user}`}>{opportunity.self}</a>
                    )}
                  </inline>
                )}
                <h3 className={Style.title}>
                  <a className={Style.title} href={getFullOpportunityUrl(opportunity)}>
                    {opportunity.title}
                  </a>
                </h3>
                {opportunity?.location && (
                  <div className={Style.attribute}>
                    <OverlayTrigger
                      placement="top"
                      overlay={
                        <Tooltip id="tooltip-hype" style={{ zIndex: 9999 }}>
                          Location
                        </Tooltip>
                      }
                    >
                      <FontAwesomeIcon className={`${Style.locationIcon}`} icon={faMapMarkerAlt} />
                    </OverlayTrigger>
                    <inline className={Style.attribute}>{opportunity.location}</inline>
                  </div>
                )}
                <div className={Style.descriptionOverlay} />
                <p className={`${Style.description} ${opportunity?.location && Style.shortClamp}`}>
                  <EFHtmlParser>{opportunity.description}</EFHtmlParser>
                </p>
              </div>
            </div>
          </div>
          <div className={Style.footer}>
            <div className={Style.userImageContainer}>
              <EFAvatarList imageSrcList={opportunity.applicantAvatars?.map(avatar => avatar.url)} />
              {opportunity.applicantAvatars?.length > 0 && (
                <p className={Style.listText}>People you know who&#39;ve applied</p>
              )}
            </div>

            {isExpanded && (
              <div className={Style.expandUp} onClick={() => setIsExpanded(!isExpanded)}>
                <FontAwesomeIcon className={Style.downArrow} icon={faChevronUp} />
              </div>
            )}
            <div
              onClick={() => {
                isExpanded ? Router.push(getFullOpportunityUrl(opportunity)) : setIsExpanded(!isExpanded);
              }}
              className={`${Style.expandButton} ${!isExpanded && Style.addMargin}`}
            >
              {!isExpanded && <FontAwesomeIcon className={Style.downArrow} icon={faChevronDown} />}
              {isExpanded ? "Learn More" : "Expand"}
            </div>
            <div className={Style.applyButtonWrapper}>
              <ApplicantStatusButton opportunity={opportunity} />
            </div>
          </div>
        </div>
      </EFCard>
    </VisibilitySensor>
  );
};

export default OpportunitySummaryCard;
