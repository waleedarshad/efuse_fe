import Link from "next/link";
import React from "react";
import TimeAgo from "react-timeago";
import { getFullOpportunityUrl } from "../../../../helpers/UrlHelper";
import EFCard from "../../../Cards/EFCard/EFCard";
import EFAvatarList from "../../../EFAvatarList/EFAvatarList";
import OpportunityImage from "../OpportunityImage/OpportunityImage";
import Style from "./OpportunityHighlightCard.module.scss";

const getCreatorInfo = opportunity => {
  if (opportunity.external) {
    return {
      name: opportunity.company_name,
      href: ""
    };
  }

  if (opportunity.associatedOrganization && opportunity.onBehalfOfOrganization) {
    const { name, shortName } = opportunity.associatedOrganization;
    return { name, href: `/org/${shortName}` };
  }

  if (opportunity.associatedUser) {
    const { firstName, lastName, username } = opportunity.associatedUser;

    return { name: `${firstName} ${lastName}`, href: `/u/${username}` };
  }

  // algolia returns user information under the `author` property instead of `associatedUser`
  if (opportunity.author) {
    const { name, username } = opportunity.author;

    return { name, href: `/u/${username}` };
  }

  return { name: "", href: "" };
};

const OpportunityHighlightCard = ({ opportunity }) => {
  const creatorInfo = getCreatorInfo(opportunity);

  return (
    <EFCard widthTheme="fullWidth">
      <div className={Style.topCardContainer}>
        <div className={Style.imageContainer}>
          <OpportunityImage opportunity={opportunity} />
        </div>

        <div className={Style.avatarList}>
          <EFAvatarList imageSrcList={opportunity.applicantAvatars?.map(avatar => avatar.url)} />
        </div>
      </div>
      <div className={Style.textWrapper}>
        <Link href={getFullOpportunityUrl(opportunity)}>
          <h3 className={Style.cardTitle}>{opportunity.title}</h3>
        </Link>
        <Link href={creatorInfo.href}>
          <p className={Style.cardSubTitle}>{creatorInfo.name}</p>
        </Link>
        <p className={Style.date}>
          <TimeAgo date={opportunity.createdAt} />
        </p>
      </div>
    </EFCard>
  );
};

export default OpportunityHighlightCard;
