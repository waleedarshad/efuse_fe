import Link from "next/link";
import { getFullOpportunityUrl } from "../../../../helpers/UrlHelper";
import EFScaledImage from "../../../EFScaledImage/EFScaledImage";
import OpportunityTypeBadge from "../OpportunityTypeBadge/OpportunityTypeBadge";
import Style from "./OpportunityImage.module.scss";

const OpportunityImage = ({ opportunity }) => {
  return (
    <Link href={getFullOpportunityUrl(opportunity)}>
      <div className={Style.backgroundImageContainer}>
        <EFScaledImage imageSrc={opportunity.image?.url} />
        <div className={Style.badgeWrapper}>
          {/* TODO: Remove employment opening check once data has been migrated */}
          <OpportunityTypeBadge
            opportunityType={opportunity.opportunityType === "Employment Opening" ? "Job" : opportunity.opportunityType}
          />
        </div>
      </div>
    </Link>
  );
};

export default OpportunityImage;
