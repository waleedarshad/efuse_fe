import OpportunityHighlightCard from "../../OpportunityCards/OpportunityHighlightCard/OpportunityHighlightCard";
import Style from "./OpportunityList.module.scss";

const OpportunityList = ({ opportunityList }) => {
  return (
    <div className={Style.listWrapper}>
      {opportunityList?.map(opp => {
        return <OpportunityHighlightCard key={opp._id} opportunity={opp} />;
      })}
    </div>
  );
};

export default OpportunityList;
