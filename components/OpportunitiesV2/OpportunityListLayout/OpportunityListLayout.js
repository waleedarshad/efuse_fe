import React from "react";
import { connectStateResults } from "react-instantsearch-dom";
import Pagination from "../../Algolia/Pagination/Pagination";
import OpportunityList from "./OpportunityList/OpportunityList";
import Style from "./OpportunityListLayout.module.scss";
import { triggerDebouncedSegmentEvent } from "../../../helpers/AlgoliaHelper";

const OpportunityListLayout = connectStateResults(({ indexName, searchResults, listTitle }) => {
  if (searchResults?.hits) {
    // segment event needed for personalization
    triggerDebouncedSegmentEvent(searchResults.hits, "OPPORTUNITIES_LISTING", indexName);
  }
  return (
    <div className={Style.listWrapper}>
      <div className={Style.textWrapper}>
        <h1 className={Style.listTitle}>{listTitle}</h1>
        <span className={Style.totalOpportunities}>{searchResults?.nbHits} total</span>
      </div>
      <OpportunityList opportunityList={searchResults?.hits} />
      {searchResults?.nbPages > 1 && (
        <div className={Style.paginationBarWrapper}>
          <Pagination />
        </div>
      )}
    </div>
  );
});

export default OpportunityListLayout;
