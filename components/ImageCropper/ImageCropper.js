import React, { PureComponent } from "react";
import ReactCrop from "react-image-crop";
import { connect } from "react-redux";

import ShowImageRatioAndSize from "../ShowImageRatioAndSize/ShowImageRatioAndSize";
import Style from "./ImageCropper.module.scss";
import "react-image-crop/dist/ReactCrop.css";
import EFPrimaryModal from "../Modals/EFPrimaryModal/EFPrimaryModal";
import EFRectangleButton from "../Buttons/EFRectangleButton/EFRectangleButton";

class ImageCropper extends PureComponent {
  state = {
    src: null,
    file: null,
    crop: {
      unit: "%",
      width: 30,
      aspect: 16 / 9
    }
  };

  static getDerivedStateFromProps(props, state) {
    state.file = props.files[0];
    state.src = props.files[0].preview;
    state.crop.aspect = props.aspectRatioWidth / props.aspectRatioHeight;
    return state;
  }

  onImageLoaded = image => {
    this.imageRef = image;
  };

  onCropComplete = crop => {
    this.makeClientCrop(crop);
  };

  onCropChange = (crop, percentCrop) => {
    this.setState({ crop });
  };

  async makeClientCrop(crop) {
    if (this.imageRef && crop.width && crop.height) {
      const croppedImage = await this.getCroppedImg(this.imageRef, crop, this.state.file.name);
      this.setState({ croppedImageUrl: croppedImage.preview });
      this.props.setCroppedFiles([{ preview: croppedImage.preview, path: croppedImage.blob.name }]);
      this.props.onCropChange(croppedImage, this.props.name);
    }
  }

  getCroppedImg(image, crop, fileName) {
    const canvas = document.createElement("canvas");
    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    // maximum width/height
    const originWidth = crop.width * scaleX;
    const originHeight = crop.height * scaleY;
    const maxWidth = 1200;
    const maxHeight = 1200 / (16 / 9);
    let targetWidth = originWidth;
    let targetHeight = originHeight;
    if (originWidth > maxWidth || originHeight > maxHeight) {
      if (originWidth / originHeight > maxWidth / maxHeight) {
        targetWidth = maxWidth;
        targetHeight = Math.round(maxWidth * (originHeight / originWidth));
      } else {
        targetHeight = maxHeight;
        targetWidth = Math.round(maxHeight * (originWidth / originHeight));
      }
    }
    canvas.width = targetWidth;
    canvas.height = targetHeight;
    const ctx = canvas.getContext("2d");

    ctx.drawImage(
      image,
      crop.x * scaleX,
      crop.y * scaleY,
      crop.width * scaleX,
      crop.height * scaleY,
      0,
      0,
      targetWidth,
      targetHeight
    );

    return new Promise((resolve, reject) => {
      canvas.toBlob(
        blob => {
          if (!blob) {
            console.error("Canvas is empty");
            return;
          }
          blob.name = fileName;
          window.URL.revokeObjectURL(this.fileUrl);
          this.fileUrl = window.URL.createObjectURL(blob);
          resolve({ blob, preview: this.fileUrl });
        },
        "image/jpeg",
        1
      );
    });
  }

  render() {
    const { crop, croppedImageUrl, src } = this.state;
    const { cropperModal, toggleCropperModal, name, aspectRatioWidth, aspectRatioHeight } = this.props;
    return (
      <>
        {this.props.cropperModalName === name && (
          <EFPrimaryModal
            title={
              <>
                <div>Adjust Image</div>
                <ShowImageRatioAndSize width={aspectRatioWidth} height={aspectRatioHeight} />
              </>
            }
            isOpen={cropperModal}
            allowBackgroundClickClose={false}
            displayCloseButton={true}
            onClose={() => toggleCropperModal(false, name)}
            widthTheme="medium"
          >
            <div>
              {src && (
                <ReactCrop
                  src={src}
                  crop={crop}
                  ruleOfThirds
                  onImageLoaded={this.onImageLoaded}
                  onComplete={this.onCropComplete}
                  onChange={this.onCropChange}
                />
              )}

              <div className={Style.croppedImage}>
                {croppedImageUrl && <img alt="Crop" style={{ maxWidth: "100%" }} src={croppedImageUrl} />}
              </div>

              <div className={Style.setButton}>
                <EFRectangleButton text="Set Image" onClick={e => toggleCropperModal(false, name)} />
              </div>
            </div>
          </EFPrimaryModal>
        )}
      </>
    );
  }
}
const mapStateToProps = state => ({
  cropperModal: state.settings.cropperModal,
  cropperModalName: state.settings.cropperModalName
});
export default connect(mapStateToProps, {})(ImageCropper);
