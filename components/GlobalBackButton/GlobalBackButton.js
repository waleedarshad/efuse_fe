import React from "react";
import Router from "next/router";

const GlobalBackButton = ({ children }) => <div onClick={() => Router.back()}>{children}</div>;

export default GlobalBackButton;
