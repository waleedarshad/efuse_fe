import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import BioCardLayout from "../Layouts/BioCardLayout/BioCardLayout";
import { rescueNil, formatRoles } from "../../helpers/GeneralHelper";
import { countFriends } from "../../store/actions/followerActions";
import { getCurrentUser, getUserById } from "../../store/actions/common/userAuthActions";

const BioCard = ({ hideOnMobile }) => {
  const dispatch = useDispatch();
  const [done, setDone] = useState(false);

  const user = useSelector(state => state.user.currentUser);
  const followers = useSelector(state => state.followers.totalFollowers);
  const currentUser = useSelector(state => state.auth.currentUser);
  const followees = useSelector(state => state.followers.totalFollowees);
  const socialLinksFlag = useSelector(state => state.features.social_links);

  useEffect(() => {
    dispatch(getCurrentUser());
    dispatch(countFriends());
    if (currentUser?.id && !done) {
      dispatch(getUserById(currentUser.id));
      setDone(true);
    }
  }, []);

  const userLoggedIn = currentUser?.id;
  const og = user.og ? user.og : { number: -1, show: false };
  const username = user.username ? `@${user.username}` : "";

  if (!userLoggedIn) return <></>;

  return (
    <BioCardLayout
      name={currentUser.name}
      username={username}
      og={og.show === true ? `#${user.og.number}` : ""}
      profilePicture={rescueNil(currentUser, "profilePicture", {})}
      bio={rescueNil(currentUser, "bio")}
      subTitle={formatRoles(currentUser)}
      location={rescueNil(currentUser, "address")}
      verified={rescueNil(user, "verified")}
      followers={followers}
      followees={followees}
      userId={user._id}
      socialLinksFlag={socialLinksFlag}
      hideOnMobile={!!hideOnMobile}
      currentUser={currentUser}
      user={user}
    />
  );
};

export default BioCard;
