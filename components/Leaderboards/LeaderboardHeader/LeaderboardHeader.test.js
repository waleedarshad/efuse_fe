import { mount } from "enzyme";
import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHandshakeAlt } from "@fortawesome/pro-solid-svg-icons";
import { faBolt } from "@fortawesome/pro-duotone-svg-icons";
import Link from "next/link";
import LeaderboardHeader from "./LeaderboardHeader";
import LEADERBOARD_TYPE from "../../../common/leaderboardTypes";

describe("LeaderboardHeader", () => {
  it("Renders the correct icon with styling", () => {
    const subject = mount(
      <LeaderboardHeader
        title="A header title"
        subtext="The subtext"
        icon={faBolt}
        leaderboardType={LEADERBOARD_TYPE.STREAKS}
      />
    );
    expect(subject.find(FontAwesomeIcon).prop("className")).toEqual("icon purpleBolt");
    expect(subject.find(FontAwesomeIcon).prop("icon")).toEqual(faBolt);
  });
  it("Renders the correct subtext", () => {
    const subject = mount(
      <LeaderboardHeader
        title="A header title"
        subtext="The subtext"
        icon={faHandshakeAlt}
        leaderboardType={LEADERBOARD_TYPE.ENGAGEMENTS}
      />
    );
    expect(subject.find("p").text()).toEqual("The subtext");
  });
  it("Renders the correct title", () => {
    const subject = mount(
      <LeaderboardHeader
        title="A header title"
        subtext="The subtext"
        icon={faHandshakeAlt}
        leaderboardType={LEADERBOARD_TYPE.ENGAGEMENTS}
      />
    );
    expect(subject.find("div").text()).toContain("A header title");
  });
  it("Renders a link with the correct route", () => {
    const subject = mount(
      <LeaderboardHeader
        title="A header title"
        subtext="The subtext"
        icon={faHandshakeAlt}
        leaderboardType={LEADERBOARD_TYPE.ENGAGEMENTS}
      />
    );
    expect(subject.find(Link).prop("href")).toEqual("/leaderboards/engagements");
  });
});
