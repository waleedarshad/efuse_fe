import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Link from "next/link";
import Style from "./LeaderboardHeader.module.scss";
import LEADERBOARD_TYPE from "../../../common/leaderboardTypes";

const LeaderboardHeader = ({ title, subtext, icon, leaderboardType }) => {
  const getIconColor = () => {
    if (leaderboardType === LEADERBOARD_TYPE.STREAKS) {
      return Style.purpleBolt;
    }
    if (leaderboardType === LEADERBOARD_TYPE.VIEWS || LEADERBOARD_TYPE.ENGAGEMENTS) {
      return Style.blackIcon;
    }
    return Style.blueList;
  };
  return (
    <div className={Style.leaderboardBoxHeader}>
      <FontAwesomeIcon className={`${Style.icon} ${getIconColor()}`} icon={icon} />
      {title}
      <Link href={`/leaderboards/${leaderboardType}`}>
        <p className={Style.subtext}>{subtext}</p>
      </Link>
    </div>
  );
};

export default LeaderboardHeader;
