import React from "react";
import { mount } from "enzyme";
import { faBolt, faEye } from "@fortawesome/pro-duotone-svg-icons";
import LeaderboardList from "./LeaderboardList";
import LEADERBOARD_TYPE from "../../../common/leaderboardTypes";
import LeaderboardCard from "../../Cards/LeaderboardCard/LeaderboardCard";

describe("LeaderboardList", () => {
  const leaderboardItems = [
    {
      user: {
        name: "Jack Hanna",
        username: "jackhanna",
        profilePicture: {
          url: "jack's url"
        }
      },
      placement: 1,
      valueAtTheTimeOfCreation: 100,
      _id: "123456789"
    },
    {
      user: {
        name: "Celine Dion",
        username: "celine123",
        profilePicture: {
          url: "celine's url"
        }
      },
      placement: 2,
      valueAtTheTimeOfCreation: 99,
      _id: "999999999"
    }
  ];
  it("renders the correct subtitle", () => {
    const leaderboardType = LEADERBOARD_TYPE.VIEWS;
    const subject = mount(
      <LeaderboardList leaderboardType={leaderboardType} leaderboardItems={leaderboardItems} icon={faEye} />
    );
    expect(
      subject
        .find("p")
        .at(0)
        .text()
    ).toEqual("Views Leaderboard");
  });
  it("renders the correct list of LeaderboardCards", () => {
    const leaderboardType = LEADERBOARD_TYPE.STREAKS;
    const subject = mount(
      <LeaderboardList leaderboardType={leaderboardType} leaderboardItems={leaderboardItems} icon={faBolt} />
    );
    expect(subject.find(LeaderboardCard)).toHaveLength(2);
    expect(
      subject
        .find(LeaderboardCard)
        .at(0)
        .props()
    ).toEqual({
      leaderboardType: "streaks",
      title: "Jack Hanna",
      titleClickLink: "/u/jackhanna",
      subTitle: "@jackhanna",
      icon: faBolt,
      value: 100,
      position: 1,
      image: "jack's url"
    });
    expect(
      subject
        .find(LeaderboardCard)
        .at(1)
        .props()
    ).toEqual({
      leaderboardType: "streaks",
      title: "Celine Dion",
      titleClickLink: "/u/celine123",
      subTitle: "@celine123",
      icon: faBolt,
      value: 99,
      position: 2,
      image: "celine's url"
    });
  });
});
