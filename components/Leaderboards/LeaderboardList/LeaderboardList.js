import React from "react";
import Style from "./LeaderboardList.module.scss";
import LeaderboardCard from "../../Cards/LeaderboardCard/LeaderboardCard";
import LEADERBOARDS from "../../../common/leaderboards";

const LeaderboardList = ({ leaderboardType, leaderboardItems, icon }) => {
  return (
    <div>
      <p className={Style.leaderboardSubTitle}>{`${LEADERBOARDS[leaderboardType].subheaderText} Leaderboard`}</p>
      {leaderboardItems?.map(item => (
        <LeaderboardCard
          leaderboardType={leaderboardType}
          title={item?.user?.name}
          titleClickLink={`/u/${item?.user?.username}`}
          subTitle={`@${item?.user?.username}`}
          icon={icon}
          value={item.valueAtTheTimeOfCreation}
          position={item?.placement}
          image={item?.user?.profilePicture?.url}
          key={item?._id}
        />
      ))}
    </div>
  );
};

export default LeaderboardList;
