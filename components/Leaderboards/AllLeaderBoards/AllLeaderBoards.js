import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Col } from "react-bootstrap";
import { useRouter } from "next/router";
import { faBinoculars } from "@fortawesome/pro-solid-svg-icons";
import { faListUl } from "@fortawesome/pro-solid-svg-icons";

import { getLeaderboardsTopThree } from "../../../store/actions/leaderboardActions";
import { getCurrentUser } from "../../../store/actions/common/userAuthActions";
import LEADERBOARD_TYPE from "../../../common/leaderboardTypes";
import Internal from "../../Layouts/Internal/Internal";
import PageTitle from "../../General/PageTitle/PageTitle";
import TopThreeContainer from "../TopThreeContainer/TopThreeContainer";
import Style from "./AllLeaderboards.module.scss";
import { getDiscoverNavigationList } from "../../../navigation/discover";
import EFSubHeader from "../../Layouts/Internal/EFSubHeader/EFSubHeader";

const AllLeaderBoards = ({ isWindowView }) => {
  const dispatch = useDispatch();
  const router = useRouter();

  const topThree = useSelector(state => state.leaderboard.top);
  const currentUser = useSelector(state => state.user.currentUser);

  useEffect(() => {
    dispatch(getLeaderboardsTopThree());
    dispatch(getCurrentUser());
  }, []);

  return (
    <Internal metaTitle="eFuse | Leaderboards" isWindowView={isWindowView} containsSubheader>
      <EFSubHeader headerIcon={faBinoculars} navigationList={getDiscoverNavigationList(router.pathname)} />
      <Col md={12}>
        <PageTitle leaderboardType={LEADERBOARD_TYPE.ALL} icon={faListUl} />
        <div className={Style.allLeaderboards}>
          <TopThreeContainer
            currentUser={currentUser}
            leaderboardType={LEADERBOARD_TYPE.STREAKS}
            leaderboardData={topThree.streaksLeaderboard}
          />
          <TopThreeContainer
            currentUser={currentUser}
            leaderboardType={LEADERBOARD_TYPE.VIEWS}
            leaderboardData={topThree.viewsLeaderboard}
          />
          <TopThreeContainer
            currentUser={currentUser}
            leaderboardType={LEADERBOARD_TYPE.ENGAGEMENTS}
            leaderboardData={topThree.engagementsLeaderboard}
          />
        </div>
      </Col>
    </Internal>
  );
};

export default AllLeaderBoards;
