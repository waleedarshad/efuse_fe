import React from "react";
import { MockedProvider } from "@apollo/client/testing";
import { faListUl, faBinoculars } from "@fortawesome/pro-solid-svg-icons";
import { mountWithStore } from "../../../common/testUtils";
import AllLeaderBoards from "./AllLeaderBoards";
import EFSubHeader from "../../Layouts/Internal/EFSubHeader/EFSubHeader";
import PageTitle from "../../General/PageTitle/PageTitle";
import LEADERBOARD_TYPE from "../../../common/leaderboardTypes";
import TopThreeContainer from "../TopThreeContainer/TopThreeContainer";
import GET_ERENA_FEATURED_EVENT from "../../../graphql/ERenaFeaturedEventQuery";

jest.mock("../../../store/actions/leaderboardActions", () => ({
  getLeaderboardsTopThree: () => ({
    type: "mockedGetLeaderboardsTopThree"
  })
}));

jest.mock("../../../store/actions/common/userAuthActions.js", () => ({
  getCurrentUser: () => ({
    type: "mockedGetCurrentUser"
  })
}));

describe("AllLeaderboards", () => {
  const currentUser = {
    id: "12345"
  };

  const top = {
    streaksLeaderboard: [{ _id: "111111" }, { _id: "222222" }, { _id: "3333333" }],
    viewsLeaderboard: [{ _id: "444444" }, { _id: "555555" }, { _id: "666666" }],
    engagementsLeaderboard: [{ _id: "777777" }, { _id: "888888" }, { _id: "999999" }]
  };

  const mocks = [
    {
      request: {
        query: GET_ERENA_FEATURED_EVENT
      },
      result: {
        data: {
          getFeaturedEvent: {}
        }
      }
    }
  ];

  let state;

  beforeEach(() => {
    state = {
      leaderboard: {
        top
      },
      user: {
        currentUser
      },
      features: {
        messaging: ""
      },
      webview: {
        status: false
      },
      auth: {
        isAuthenticated: true
      },
      errors: {
        errors: ""
      },
      messages: {
        unreadMessageCount: 0
      }
    };
  });

  it("displays the correct subheader", () => {
    const { subject } = mountWithStore(
      <MockedProvider mocks={mocks} addTypename={false}>
        <AllLeaderBoards isWindowView={false} />
      </MockedProvider>,
      state
    );
    expect(subject.find(EFSubHeader).prop("headerIcon")).toEqual(faBinoculars);
    expect(subject.find(EFSubHeader).prop("navigationList")).toEqual([
      {
        href: "/discover",
        isActive: false,
        name: "Discover"
      },
      {
        href: "/organizations",
        isActive: false,
        name: "Organizations"
      },
      {
        href: "/news",
        isActive: false,
        name: "News"
      },
      {
        href: "/pipeline",
        isActive: false,
        name: "Pipeline"
      }
    ]);
  });

  it("renders the correct page title", () => {
    const { subject } = mountWithStore(
      <MockedProvider mocks={mocks} addTypename={false}>
        <AllLeaderBoards isWindowView={false} />
      </MockedProvider>,
      state
    );

    expect(subject.find(PageTitle).prop("leaderboardType")).toEqual(LEADERBOARD_TYPE.ALL);
    expect(subject.find(PageTitle).prop("icon")).toEqual(faListUl);
  });

  it("renders the correct top three containers", () => {
    const { subject } = mountWithStore(
      <MockedProvider mocks={mocks} addTypename={false}>
        <AllLeaderBoards isWindowView={false} />
      </MockedProvider>,
      state
    );

    expect(
      subject
        .find(TopThreeContainer)
        .at(0)
        .prop("currentUser")
    ).toEqual(currentUser);
    expect(
      subject
        .find(TopThreeContainer)
        .at(0)
        .prop("leaderboardType")
    ).toEqual(LEADERBOARD_TYPE.STREAKS);
    expect(
      subject
        .find(TopThreeContainer)
        .at(0)
        .prop("leaderboardData")
    ).toEqual(top.streaksLeaderboard);

    expect(
      subject
        .find(TopThreeContainer)
        .at(1)
        .prop("leaderboardType")
    ).toEqual(LEADERBOARD_TYPE.VIEWS);
    expect(
      subject
        .find(TopThreeContainer)
        .at(1)
        .prop("leaderboardData")
    ).toEqual(top.viewsLeaderboard);

    expect(
      subject
        .find(TopThreeContainer)
        .at(2)
        .prop("leaderboardType")
    ).toEqual(LEADERBOARD_TYPE.ENGAGEMENTS);
    expect(
      subject
        .find(TopThreeContainer)
        .at(2)
        .prop("leaderboardData")
    ).toEqual(top.engagementsLeaderboard);
  });
});
