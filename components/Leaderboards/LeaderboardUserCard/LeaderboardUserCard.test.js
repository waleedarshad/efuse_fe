import React from "react";
import { mount } from "enzyme";
import { faEye } from "@fortawesome/pro-solid-svg-icons";

import LeaderboardUserCard from "./LeaderboardUserCard";
import LEADERBOARD_TYPE from "../../../common/leaderboardTypes";
import LeaderboardCard from "../../Cards/LeaderboardCard/LeaderboardCard";

describe("LeaderboardUserCard", () => {
  const leaderboardType = LEADERBOARD_TYPE.VIEWS;
  const name = "Kara Thrace";
  const username = "Starbuck";
  const url = "/profile/picture/url";
  const currentUser = {
    name,
    username,
    activeLeaderboard: {
      viewsLeaderboard: {
        placement: 5,
        valueAtTheTimeOfCreation: 15000
      },
      streaksLeaderboard: {
        placement: 2
      }
    },
    profilePicture: {
      url
    }
  };

  it("renders the correct subtitle", () => {
    const subject = mount(<LeaderboardUserCard leaderboardType={leaderboardType} currentUser={currentUser} />);
    expect(
      subject
        .find("p")
        .at(0)
        .text()
    ).toEqual("Your Views Placement");
  });

  it("renders a leaderboard card with the correct props", () => {
    const subject = mount(<LeaderboardUserCard leaderboardType={leaderboardType} currentUser={currentUser} />);
    expect(subject.find(LeaderboardCard).props()).toEqual({
      leaderboardType: LEADERBOARD_TYPE.VIEWS,
      title: name,
      titleClickLink: `/u/${username}`,
      subTitle: `@${username}`,
      icon: faEye,
      value: 15000,
      position: 5,
      image: url
    });
  });

  it("renders a leaderboard card with correct position if no activeLeaderboard", () => {
    const user = {
      name,
      username,
      profilePicture: {
        url
      }
    };
    const subject = mount(<LeaderboardUserCard leaderboardType={leaderboardType} currentUser={user} />);
    expect(subject.find(LeaderboardCard).prop("position")).toEqual(null);
  });

  it("renders a leaderboard card with correct position if activeLeaderboard placement", () => {
    const subject = mount(<LeaderboardUserCard leaderboardType={leaderboardType} currentUser={currentUser} />);
    expect(subject.find(LeaderboardCard).prop("position")).toEqual(5);
  });

  it("renders a leaderboard card with unranked if no count", () => {
    const user = {
      name,
      username,
      profilePicture: {
        url
      }
    };
    const subject = mount(<LeaderboardUserCard leaderboardType={leaderboardType} currentUser={user} />);
    expect(subject.find(LeaderboardCard).prop("value")).toEqual("Unranked");
  });
});
