import React from "react";
import Style from "./LeaderboardUserCard.module.scss";
import LeaderboardCard from "../../Cards/LeaderboardCard/LeaderboardCard";

import LEADERBOARDS from "../../../common/leaderboards";
import { getCurrentUserPlacement, getUserValue } from "../../../helpers/LeaderboardHelper";

const LeaderboardUserCard = ({ leaderboardType, currentUser }) => {
  return (
    <div>
      <p className={Style.leaderboardSubTitle}>{`Your ${LEADERBOARDS[leaderboardType].subheaderText} Placement`}</p>
      <LeaderboardCard
        leaderboardType={leaderboardType}
        title={currentUser?.name}
        titleClickLink={`/u/${currentUser?.username}`}
        subTitle={`@${currentUser?.username}`}
        icon={LEADERBOARDS[leaderboardType].iconType}
        value={getUserValue(currentUser, leaderboardType)}
        position={getCurrentUserPlacement(currentUser, leaderboardType)}
        image={currentUser?.profilePicture?.url}
      />
    </div>
  );
};

export default LeaderboardUserCard;
