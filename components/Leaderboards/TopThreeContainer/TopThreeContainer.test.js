import React from "react";
import { mount } from "enzyme";
import { faEye } from "@fortawesome/pro-solid-svg-icons";
import TopThreeContainer from "./TopThreeContainer";
import LEADERBOARD_TYPE from "../../../common/leaderboardTypes";
import LeaderboardHeader from "../LeaderboardHeader/LeaderboardHeader";
import LeaderboardCard from "../../Cards/LeaderboardCard/LeaderboardCard";

describe("TopThreeContainer", () => {
  const leaderboardType = LEADERBOARD_TYPE.VIEWS;
  const name = "Ace Ventura";
  const username = "whenNatureCalls";
  const url = "/profile/picture/url";
  const currentUser = {
    name,
    username,
    activeLeaderboard: {
      viewsLeaderboard: {
        placement: 5,
        valueAtTheTimeOfCreation: 100
      }
    },
    profilePicture: {
      url
    }
  };
  const leaderboardData = [
    {
      user: {
        name: "Marie Curie",
        username: "madamecurie",
        profilePicture: {
          url: "a_profile_picture"
        }
      },
      valueAtTheTimeOfCreation: 1234,
      placement: 1,
      _id: "12345678"
    },
    {
      user: {
        name: "Albert Einstein",
        username: "e=mc2",
        profilePicture: {
          url: "another_profile_picture"
        }
      },
      valueAtTheTimeOfCreation: 123,
      placement: 2,
      _id: "987654321"
    }
  ];
  let subject;

  beforeEach(() => {
    subject = mount(
      <TopThreeContainer
        currentUser={currentUser}
        leaderboardType={leaderboardType}
        leaderboardData={leaderboardData}
      />
    );
  });

  it("renders a main header with the correct properties", () => {
    expect(subject.find(LeaderboardHeader).prop("title")).toEqual("View Leaders");
  });

  it("renders a list of the correct leaders for a leaderboard type", () => {
    const expectedProps1 = {
      leaderboardType: LEADERBOARD_TYPE.VIEWS,
      title: "Marie Curie",
      titleClickLink: "/u/madamecurie",
      subTitle: "@madamecurie",
      icon: faEye,
      value: 1234,
      position: 1,
      image: "a_profile_picture"
    };
    const expectedProps2 = {
      leaderboardType: LEADERBOARD_TYPE.VIEWS,
      title: "Albert Einstein",
      titleClickLink: "/u/e=mc2",
      subTitle: "@e=mc2",
      icon: faEye,
      value: 123,
      position: 2,
      image: "another_profile_picture"
    };

    expect(
      subject
        .find(LeaderboardCard)
        .at(0)
        .props()
    ).toEqual(expectedProps1);
    expect(
      subject
        .find(LeaderboardCard)
        .at(1)
        .props()
    ).toEqual(expectedProps2);
  });

  it("renders a card with the correct current user info", () => {
    const expectedProps = {
      title: "Ace Ventura",
      titleClickLink: "/u/whenNatureCalls",
      subTitle: "@whenNatureCalls",
      icon: faEye,
      value: 100,
      position: 5,
      image: url,
      footer: true
    };
    expect(
      subject
        .find(LeaderboardCard)
        .last()
        .props()
    ).toEqual(expectedProps);
  });
});
