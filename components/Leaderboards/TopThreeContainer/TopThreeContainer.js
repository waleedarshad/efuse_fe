import React from "react";
import Style from "./TopThreeContainer.module.scss";
import LeaderboardHeader from "../LeaderboardHeader/LeaderboardHeader";

import LeaderboardCard from "../../Cards/LeaderboardCard/LeaderboardCard";

import LEADERBOARDS from "../../../common/leaderboards";
import { getCurrentUserPlacement, getUserValue } from "../../../helpers/LeaderboardHelper";

const TopThreeContainer = ({ currentUser, leaderboardType, leaderboardData }) => {
  return (
    <div className={Style.leaderboardContainer}>
      <div className={Style.leaderboardBox}>
        <LeaderboardHeader
          title={`${LEADERBOARDS[leaderboardType].singleText} Leaders`}
          subtext="View Leaderboard"
          icon={LEADERBOARDS[leaderboardType].iconType}
          leaderboardType={leaderboardType}
        />
        {leaderboardData?.map(item => (
          <LeaderboardCard
            leaderboardType={leaderboardType}
            title={item?.user?.name}
            titleClickLink={`/u/${item?.user?.username}`}
            subTitle={`@${item?.user?.username}`}
            icon={LEADERBOARDS[leaderboardType].iconType}
            value={item?.valueAtTheTimeOfCreation}
            position={item?.placement}
            image={item?.user?.profilePicture?.url}
            key={item?._id}
          />
        ))}
        <LeaderboardCard
          title={currentUser?.name}
          titleClickLink={`/u/${currentUser?.username}`}
          subTitle={`@${currentUser?.username}`}
          icon={LEADERBOARDS[leaderboardType].iconType}
          value={getUserValue(currentUser, leaderboardType)}
          position={getCurrentUserPlacement(currentUser, leaderboardType)}
          image={currentUser?.profilePicture?.url}
          footer
        />
      </div>
    </div>
  );
};

export default TopThreeContainer;
