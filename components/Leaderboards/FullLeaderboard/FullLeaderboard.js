import { useRouter } from "next/router";
import React, { useEffect } from "react";
import { Col } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { faBinoculars } from "@fortawesome/pro-solid-svg-icons";

import LEADERBOARDS from "../../../common/leaderboards";
import { getLeaderboard, getLeaderboardAction } from "../../../helpers/LeaderboardHelper";
import { getDiscoverNavigationList } from "../../../navigation/discover";
import { getCurrentUser } from "../../../store/actions/common/userAuthActions";
import EFPaginationBar from "../../EFPaginationBar/EFPaginationBar";
import PageTitle from "../../General/PageTitle/PageTitle";
import EFSubHeader from "../../Layouts/Internal/EFSubHeader/EFSubHeader";
import Internal from "../../Layouts/Internal/Internal";
import LeaderboardList from "../LeaderboardList/LeaderboardList";
import LeaderboardUserCard from "../LeaderboardUserCard/LeaderboardUserCard";
import Style from "./FullLeaderboard.module.scss";

const FullLeaderboard = ({ isWindowView }) => {
  const MAX_PAGE_SIZE = 20;

  const router = useRouter();
  const leaderboardType = router.query.type;

  const dispatch = useDispatch();

  const leaderboard = useSelector(state => state.leaderboard);
  const currentUser = useSelector(state => state.user.currentUser);

  const clickPaginatedPage = page => {
    dispatch(getLeaderboardAction(page, MAX_PAGE_SIZE, leaderboardType));
    window.scrollTo(0, 0);
  };

  useEffect(() => {
    dispatch(getLeaderboardAction(1, MAX_PAGE_SIZE, leaderboardType));
    dispatch(getCurrentUser());
  }, []);

  return (
    <Internal metaTitle="eFuse | Leaderboards" isWindowView={isWindowView} containsSubheader>
      <EFSubHeader headerIcon={faBinoculars} navigationList={getDiscoverNavigationList(router.pathname)} />
      <Col md={12}>
        <PageTitle leaderboardType={leaderboardType} icon={LEADERBOARDS[leaderboardType].iconType} />
        <LeaderboardUserCard leaderboardType={leaderboardType} currentUser={currentUser} />
        <LeaderboardList
          leaderboardType={leaderboardType}
          leaderboardItems={getLeaderboard(leaderboardType, leaderboard).docs}
          icon={LEADERBOARDS[leaderboardType].iconType}
        />
        <div className={Style.paginationWrapper}>
          <EFPaginationBar
            totalPages={getLeaderboard(leaderboardType, leaderboard).totalPages}
            onPageChange={clickPaginatedPage}
            marginPages={1}
            pageRange={2}
          />
        </div>
      </Col>
    </Internal>
  );
};

export default FullLeaderboard;
