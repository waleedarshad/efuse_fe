import React from "react";

import Style from "./EFInfoPill.module.scss";

const EFInfoPill = ({ children }) => {
  return <span className={`${Style.info}`}>{children}</span>;
};

export default EFInfoPill;
