import React from "react";
import EFInfoPill from "./EFInfoPill";

export default {
  title: "Shared/EFInfoPill",
  component: EFInfoPill,
  argTypes: {
    children: {
      control: {
        type: "text"
      }
    }
  }
};

// eslint-disable-next-line react/jsx-props-no-spreading
const Story = args => <EFInfoPill {...args} />;

export const InfoPill = Story.bind({});
InfoPill.args = {
  children: "This is sample text"
};
