import Modal from "../Modal/Modal";
import ModalContent from "./ModalContent/ModalContent";

const SignupModal = ({ children, title, message, redirectURL }) => {
  return (
    <Modal
      displayCloseButton
      allowBackgroundClickClose
      openOnLoad={false}
      component={<ModalContent title={title} message={message} redirectURL={redirectURL} />}
      textAlignCenter={false}
      customMaxWidth="800px"
    >
      {children}
    </Modal>
  );
};

export default SignupModal;
