import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUserPlus } from "@fortawesome/pro-solid-svg-icons";

import Style from "./ModalContent.module.scss";
import DynamicModal from "../../DynamicModal/DynamicModal";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";
import FeatureFlagVariant from "../../General/FeatureFlag/FeatureFlagVariant/FeatureFlagVariant";

const ModalContent = ({ title, message, redirectURL }) => {
  return (
    <div className={Style.modalContent}>
      <div className={Style.modalIcon}>
        <FontAwesomeIcon icon={faUserPlus} />
      </div>
      <div className={Style.modalTitle}>{title}</div>
      <div className={Style.modalContent}>{message}</div>
      <div className={Style.modalButtons}>
        <DynamicModal
          flow="LoginSignup"
          openOnLoad={false}
          startView="signup"
          displayCloseButton
          allowBackgroundClickClose={false}
          customRedirect={redirectURL}
        >
          <EFRectangleButton
            buttonType="button"
            colorTheme="primary"
            fontWeightTheme="bold"
            shadowTheme="medium"
            size="large"
            text="Sign up"
          />
        </DynamicModal>

        <FeatureFlagVariant flagState={false}>
          <DynamicModal
            flow="LoginSignup"
            openOnLoad={false}
            startView="login"
            displayCloseButton
            allowBackgroundClickClose={false}
          >
            <EFRectangleButton
              buttonType="button"
              colorTheme="secondary"
              fontWeightTheme="bold"
              shadowTheme="medium"
              size="large"
              text="Login"
            />
          </DynamicModal>
        </FeatureFlagVariant>
      </div>
    </div>
  );
};

export default ModalContent;
