import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faInfoCircle } from "@fortawesome/pro-solid-svg-icons";
import { Tooltip, OverlayTrigger } from "react-bootstrap";
import PropTypes from "prop-types";
import Style from "./HelpToolTip.module.scss";

const HelpToolTip = props => {
  const { placement, text, colorTheme } = props;
  return (
    <OverlayTrigger
      placement={placement}
      overlay={
        <Tooltip id="tooltip-top" style={{ zIndex: 9999 }}>
          {text}
        </Tooltip>
      }
    >
      <FontAwesomeIcon icon={faInfoCircle} className={`${Style[colorTheme]}`} />
    </OverlayTrigger>
  );
};

HelpToolTip.propTypes = {
  placement: PropTypes.string,
  text: PropTypes.string,
  colorTheme: PropTypes.string
};

HelpToolTip.defaultProps = {
  placement: "left",
  text: "",
  colorTheme: "default"
};

export default HelpToolTip;
