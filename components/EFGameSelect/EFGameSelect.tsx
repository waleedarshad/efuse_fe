import React, { useEffect, useState } from "react";
import EFGameCardList from "../EFGameCardList/EFGameCardList";
import getGamesHook from "../../hooks/getHooks/getGamesHook";

interface Game {
  description?: string;
  gameImage: {
    _typename?: string;
    filename?: string;
    url?: string;
  };
  kind?: string;
  slug?: string;
  title?: string;
  _id?: string;
  selected?: boolean;
}

interface EFGameSelectProps {
  onSelect: (games: Game[]) => void;
  selectMultiple?: boolean;
  preselectedGameIds?: any;
  gamesToRemove?: any;
  horizontalScroll?: boolean;
}

const EFGameSelect = ({
  onSelect,
  selectMultiple = true,
  preselectedGameIds,
  gamesToRemove,
  horizontalScroll = false
}: EFGameSelectProps) => {
  const gamesFromHook = getGamesHook();
  const [games, setGames] = useState([]);

  useEffect(() => {
    let updatedGames = gamesFromHook;
    if (preselectedGameIds)
      updatedGames = gamesFromHook.map(game => {
        return {
          ...game,
          selected:
            // if it is array, find the game, else compare directly
            Array.isArray(preselectedGameIds)
              ? preselectedGameIds.find(selectedGame => selectedGame._id === game._id)
              : preselectedGameIds === game._id
        };
      });

    // this is a temporary fix to hide game.
    // We plan to implement a new flag on games collection objects and bring in through Algolia in the future.
    if (gamesToRemove) {
      updatedGames = updatedGames
        .filter(game =>
          Array.isArray(gamesToRemove)
            ? gamesToRemove.find(removeGame => removeGame.slug !== game.slug)
            : gamesToRemove !== game.slug
        )
        .map(game => game);
    }

    setGames(updatedGames);
  }, [JSON.stringify(gamesFromHook)]);

  const onGameClick = selectedGame => {
    let updatedGames = [...games];

    if (!selectMultiple) {
      updatedGames = games.map(game => {
        return { ...game, selected: false };
      });
    }

    const gameIndex = games.findIndex(x => x._id === selectedGame?._id);
    updatedGames[gameIndex] = { ...selectedGame, selected: !selectedGame?.selected };
    setGames(updatedGames);
    onSelect(updatedGames.filter(game => game.selected));
  };

  return <EFGameCardList games={games} horizontalScroll={horizontalScroll} onGameClick={onGameClick} />;
};

export default EFGameSelect;
