import React from "react";
import EFCheckbox from "./EFCheckbox";

export default {
  title: "Inputs/EFCheckbox",
  component: EFCheckbox,
  argTypes: {
    isChecked: {
      control: {
        type: "boolean"
      }
    }
  }
};

const Story = args => <EFCheckbox {...args} />;

export const Checked = Story.bind({});

Checked.args = {
  isChecked: true
};

export const Unchecked = Story.bind({});

Unchecked.args = {
  isChecked: false
};
