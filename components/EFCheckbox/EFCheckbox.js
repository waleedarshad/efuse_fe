import React from "react";
import Style from "./EFCheckbox.module.scss";

const EFCheckbox = ({ isChecked }) => {
  return (
    <div className={Style.checkboxContainer}>
      <div className={`${isChecked ? Style.checked : Style.unchecked}`}>
        <svg className={Style.checkmark} viewBox="0 0 24 24">
          <polyline points="20 6 9 17 4 12" />
        </svg>
      </div>
    </div>
  );
};

EFCheckbox.defaultProps = {
  isChecked: false
};

export default EFCheckbox;
