import React from "react";
import { faArrowLeft, faArrowRight } from "@fortawesome/pro-regular-svg-icons";

import HorizontalScroll from "./HorizontalScroll";

export default {
  title: "Shared/HorizontalScroll",
  component: HorizontalScroll,
  argTypes: {
    iconLeft: {
      control: {
        type: "object"
      }
    },
    iconRight: {
      control: {
        type: "object"
      }
    },
    scrollValue: {
      control: {
        type: "number"
      }
    }
  }
};

const Story = args => (
  // eslint-disable-next-line react/jsx-props-no-spreading
  <HorizontalScroll {...args}>
    <div style={{ display: "flex" }}>
      {["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"].map(() => (
        <div key="value" style={{ width: "200px", height: "150px", backgroundColor: "blue", marginRight: "5px" }} />
      ))}
    </div>
  </HorizontalScroll>
);

export const Default = Story.bind({});
Default.args = {
  iconLeft: faArrowLeft,
  iconRight: faArrowRight,
  scrollValue: 500,
  customStyle: "",
  withRightButton: false,
  withLeftButton: false
};
