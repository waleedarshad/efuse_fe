import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useEffect, useRef, useState } from "react";
import ReactDOM from "react-dom";
import { faChevronLeft, faChevronRight } from "@fortawesome/pro-solid-svg-icons";
import ScrollContainer from "react-indiana-drag-scroll";
import smoothscroll from "smoothscroll-polyfill";
import { IconDefinition } from "@fortawesome/fontawesome-svg-core";
import Style from "./HorizontalScroll.module.scss";

interface HorizontalScrollProps {
  scrollValue?: number;
  customStyle?: string;
  noPadding?: boolean;
  customButtonMargin?: string;
  iconRight?: IconDefinition;
  iconLeft?: IconDefinition;
  withLeftArrow?: boolean;
  withRightArrow?: boolean;
  children?: ReactDOM.Node;
  addFadingEffect?: boolean;
}

const HorizontalScroll = ({
  scrollValue = 500,
  customStyle,
  noPadding = false,
  customButtonMargin,
  iconRight = faChevronRight,
  iconLeft = faChevronLeft,
  withLeftArrow = true,
  withRightArrow = true,
  addFadingEffect = false,
  children
}: HorizontalScrollProps) => {
  const container = useRef(null);

  useEffect(() => {
    // @ts-ignore
    window.__forceSmoothScrollPolyfill__ = true;
    smoothscroll.polyfill();
    // eslint-disable-next-line react/no-find-dom-node
    ReactDOM.findDOMNode(container.current).scrollTo(0, 0);
    checkLeft();
    checkRight();
  }, []);

  const [isScrolling, setScroll] = useState(false);
  const moveScrollPositionRight = () => {
    const scrollLeftValue = container.current.container.current.scrollLeft;

    // eslint-disable-next-line react/no-find-dom-node
    ReactDOM.findDOMNode(container.current).scrollTo({
      left: scrollLeftValue + scrollValue,
      behavior: "smooth"
    });
  };
  const moveScrollPositionLeft = () => {
    const scrollLeftValue = container.current.container.current.scrollLeft;

    // eslint-disable-next-line react/no-find-dom-node
    ReactDOM.findDOMNode(container.current).scrollTo({
      left: scrollLeftValue - scrollValue,
      behavior: "smooth"
    });
  };

  const checkRight = () => {
    const scrollLeftValue = container.current.container.current.scrollLeft;
    const { scrollWidth } = container.current.container.current;
    const { offsetWidth } = container.current.container.current;

    if (offsetWidth + scrollLeftValue + 5 >= scrollWidth) {
      checkEnd(true);
    } else {
      checkEnd(false);
    }
  };

  const checkLeft = () => {
    const scrollLeftValue = container.current.container.current.scrollLeft;
    if (scrollLeftValue <= 5) {
      checkBeginning(true);
    } else {
      checkBeginning(false);
    }
  };

  const [isEnd, checkEnd] = useState(false);
  const [isBeginning, checkBeginning] = useState(false);

  return (
    <ScrollContainer
      className={`${Style.flex} ${isScrolling && Style.isScrolling} ${customStyle && customStyle} ${noPadding &&
        Style.noPadding} ${addFadingEffect && !isEnd && Style.fadeEffect} `}
      ref={container}
      vertical={false}
      onScroll={() => {
        setScroll(true);
        checkRight();
        checkLeft();
      }}
      onEndScroll={() => setScroll(false)}
    >
      {withLeftArrow && (
        <button
          type="button"
          className={`${Style.leftButton} ${isBeginning && Style.hideButton} ${customButtonMargin &&
            customButtonMargin}`}
          onClick={moveScrollPositionLeft}
        >
          <FontAwesomeIcon icon={iconLeft} />
        </button>
      )}
      {children}
      {withRightArrow && (
        <button
          type="button"
          className={`${Style.rightButton} ${isEnd && Style.hideButton} ${customButtonMargin && customButtonMargin}`}
          onClick={moveScrollPositionRight}
        >
          <FontAwesomeIcon icon={iconRight} />
        </button>
      )}
    </ScrollContainer>
  );
};

export default HorizontalScroll;
