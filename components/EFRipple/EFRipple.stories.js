import EFRipple from "./EFRipple";

export default {
  title: "Shared/EFRipple",
  component: EFRipple,
  argTypes: {
    duration: {
      control: {
        type: "number"
      }
    },
    backgroundColor: {
      control: {
        type: "text"
      }
    }
  }
};

//position relative and overflow hidden is REALLY important for the parent
const Story = args => (
  <div style={{ backgroundColor: "black", width: "100px", height: "50px", position: "relative", overflow: "hidden" }}>
    <EFRipple {...args} />
  </div>
);

export const Basic = Story.bind({});

Basic.args = {
  duration: 500,
  backgroundColor: "rgba(255, 255, 255, 0.7)"
};

export const CustomColor = Story.bind({});

CustomColor.args = {
  duration: 500,
  backgroundColor: "#2e87ff"
};

export const CustomDurationSlow = Story.bind({});

CustomDurationSlow.args = {
  duration: 2000,
  backgroundColor: "rgba(255, 255, 255, 0.7)"
};

export const CustomDurationFast = Story.bind({});

CustomDurationFast.args = {
  duration: 200,
  backgroundColor: "rgba(255, 255, 255, 0.7)"
};
