import React, { useState, useEffect } from "react";
import Style from "./EFRipple.module.scss";
import PropTypes from "prop-types";

const useDebouncedRippleCleanUp = (rippleCount, duration, cleanUpFunction) => {
  useEffect(() => {
    let bounce;
    if (rippleCount > 0) {
      clearTimeout(bounce);

      bounce = setTimeout(() => {
        cleanUpFunction();
        clearTimeout(bounce);
      }, duration * 4);
    }

    return () => clearTimeout(bounce);
  }, [rippleCount, duration, cleanUpFunction]);
};

const EFRipple = ({ duration, backgroundColor }) => {
  const [rippleArray, setRippleArray] = useState([]);

  useDebouncedRippleCleanUp(rippleArray.length, duration, () => {
    setRippleArray([]);
  });

  const addRipple = event => {
    const rippleContainer = event.currentTarget.getBoundingClientRect();
    const size = rippleContainer.width > rippleContainer.height ? rippleContainer.width : rippleContainer.height;

    const x = event.pageX - rippleContainer.x - rippleContainer.width / 2;
    const y = event.pageY - rippleContainer.y - rippleContainer.width / 2;
    const newRipple = {
      x,
      y,
      size
    };

    setRippleArray(prevState => [...prevState, newRipple]);
  };

  return (
    <div onMouseDown={addRipple} className={Style.ripple}>
      {rippleArray.length > 0 &&
        rippleArray.map((ripple, index) => {
          return (
            <span
              key={"ripple_" + index}
              style={{
                top: ripple.y,
                left: ripple.x,
                width: ripple.size,
                height: ripple.size,
                animationDuration: `${duration}ms`,
                backgroundColor: backgroundColor
              }}
            />
          );
        })}
    </div>
  );
};

EFRipple.propTypes = {
  duration: PropTypes.number,
  backgroundColor: PropTypes.string
};
EFRipple.defaultProps = {
  duration: 500,
  backgroundColor: "rgba(255, 255, 255, 0.7)"
};
export default EFRipple;
