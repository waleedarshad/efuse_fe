import { shallow } from "enzyme";
import EFAvatar from "../EFAvatar/EFAvatar";
import EFUserBadge from "./EFUserBadge";

describe("EFuserBadge", () => {
  let subject;

  beforeEach(() => {
    subject = shallow(<EFUserBadge profilePictureUrl="my-pic.png" fullname="Carlos Carlone" username="mafioso" />);
  });

  it("displays an extra small online avatar with the provided image", () => {
    expect(subject.find(EFAvatar).props()).toEqual({
      borderTheme: "lightColor",
      displayOnlineButton: true,
      online: true,
      profilePicture: "my-pic.png",
      size: "extraSmall",
      withBorderShadow: false
    });
  });

  it("renders the provided full name", () => {
    expect(subject.text()).toContain("Carlos Carlone");
  });

  it("renders the provided username", () => {
    expect(subject.text()).toContain("@mafioso");
  });
});
