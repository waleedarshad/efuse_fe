import React from "react";
import EFUserBadge from "./EFUserBadge";

export default {
  title: "Users/EFUserBadge",
  component: EFUserBadge
};

// eslint-disable-next-line react/jsx-props-no-spreading
const Story = args => <EFUserBadge {...args} />;

export const Default = Story.bind({});
Default.args = {
  profilePictureUrl: "https://cdn.efuse.gg/uploads/static/global/mindblown_white.png",
  fullname: "Aristotle Dumas",
  username: "flyingstars"
};
