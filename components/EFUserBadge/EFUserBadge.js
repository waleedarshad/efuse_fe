import EFAvatar from "../EFAvatar/EFAvatar";
import Style from "./EFUserBadge.module.scss";

const EFUserBadge = ({ profilePictureUrl, fullname, username, displayOnlineButton, badgeSize = "medium" }) => {
  let avatarSize = "extraSmall";

  if (badgeSize === "medium") {
    avatarSize = "extraSmall";
  } else if (badgeSize === "small") {
    avatarSize = "extraExtraSmall";
  }

  return (
    <div className={Style.container}>
      <EFAvatar
        profilePicture={profilePictureUrl}
        size={avatarSize}
        borderTheme="lightColor"
        displayOnlineButton={displayOnlineButton}
        online
      />
      <div className={`${Style.names} ${Style[badgeSize]}`}>
        <p className={Style.fullname}>{fullname}</p>
        {username && <p className={Style.username}>@{username}</p>}
      </div>
    </div>
  );
};

EFUserBadge.defaultProps = {
  displayOnlineButton: true
};

export default EFUserBadge;
