import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus, faCheck } from "@fortawesome/pro-solid-svg-icons";

import ProgressBar from "react-bootstrap/ProgressBar";
import Style from "../PortfolioProgress.module.scss";
import { getOrganizationFilledFields, decimalToCleanPercentage } from "../../../helpers/GeneralHelper";

class ErenaProgress extends Component {
  render() {
    const { organization } = this.props;
    let progressBarCount = 0;
    let progressBar = 0;
    const totalCount = 7;
    const organizationFields = getOrganizationFilledFields(organization);
    function updateProgress() {
      progressBarCount += 1;
      progressBar = (progressBarCount / totalCount) * 100;
      if (progressBarCount == totalCount) {
        progressBar = 100;
      }
    }

    const description = organizationFields.includes("description");
    description && updateProgress();

    const about = organizationFields.includes("about");
    about && updateProgress();

    const videoCarousel = organizationFields.includes("videoCarousel");
    videoCarousel && updateProgress();

    const playerCards = organizationFields.includes("playerCards");
    playerCards && updateProgress();

    const promoVideo = organizationFields.includes("promoVideo");
    promoVideo && updateProgress();

    const honors = organizationFields.includes("honors");
    honors && updateProgress();

    const events = organizationFields.includes("events");
    events && updateProgress();

    if (progressBar != 100) {
      progressBar = decimalToCleanPercentage(progressBar);
    }

    return (
      <div className={Style.container}>
        <div className={Style.header}>
          Erena Progress
          <p className={Style.headerPercentage}>{progressBar}%</p>
          <div className={Style.headerBg} />
        </div>
        <div className={`${Style.bodyItem} ${description && Style.bodyComplete}`}>
          <FontAwesomeIcon
            icon={description ? faCheck : faPlus}
            className={`${Style.icon} ${description && Style.check}`}
          />
          <p className={`${Style.text} ${description && Style.complete}`}>Add a Twitch channel</p>
        </div>
        <div className={`${Style.bodyItem} ${about && Style.bodyComplete}`}>
          <FontAwesomeIcon icon={about ? faCheck : faPlus} className={`${Style.icon} ${about && Style.check}`} />
          <p className={`${Style.text} ${about && Style.complete}`}>Create 2 ads</p>
        </div>
        <div className={`${Style.bodyItem} ${videoCarousel && Style.bodyComplete}`}>
          <FontAwesomeIcon
            icon={videoCarousel ? faCheck : faPlus}
            className={`${Style.icon} ${videoCarousel && Style.check}`}
          />
          <p className={`${Style.text} ${videoCarousel && Style.complete}`}>Add rules</p>
        </div>
        <div className={`${Style.bodyItem} ${playerCards && Style.bodyComplete}`}>
          <FontAwesomeIcon
            icon={playerCards ? faCheck : faPlus}
            className={`${Style.icon} ${playerCards && Style.check}`}
          />
          <p className={`${Style.text} ${playerCards && Style.complete}`}>Add website link</p>
        </div>
        <div className={`${Style.bodyItem} ${promoVideo && Style.bodyComplete}`}>
          <FontAwesomeIcon
            icon={promoVideo ? faCheck : faPlus}
            className={`${Style.icon} ${promoVideo && Style.check}`}
          />
          <p className={`${Style.text} ${promoVideo && Style.complete}`}>Add background image</p>
        </div>
        <div className={`${Style.bodyItem} ${honors && Style.bodyComplete}`}>
          <FontAwesomeIcon icon={honors ? faCheck : faPlus} className={`${Style.icon} ${honors && Style.check}`} />
          <p className={`${Style.text} ${honors && Style.complete}`}>Share the event</p>
        </div>
        <div className={`${Style.bodyItem} ${events && Style.bodyComplete}`}>
          <FontAwesomeIcon icon={events ? faCheck : faPlus} className={`${Style.icon} ${events && Style.check}`} />
          <p className={`${Style.text} ${events && Style.complete}`}>Create Leaderboard</p>
        </div>

        <ProgressBar className={Style.progressBar} now={progressBar} variant="blue" />
      </div>
    );
  }
}

export default ErenaProgress;
