import React from "react";
import { faGamepadAlt } from "@fortawesome/pro-solid-svg-icons";

import ProgressBar from "../../ProgressBar/ProgressBar";

const BadgeProgress = () => {
  return <ProgressBar size="medium" icon={faGamepadAlt} percentage={80} text="2/3 Challanges left" />;
};

export default BadgeProgress;
