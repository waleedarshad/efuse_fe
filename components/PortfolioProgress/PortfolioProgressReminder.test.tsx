import { mountWithStore } from "../../common/testUtils";
import PortfolioProgressReminder from "./PortfolioProgressReminder";

describe("Portfolio progress reminder component", () => {
  it("renders 'low' with low portfolio progress", () => {
    const state = {
      user: {
        currentUser: {
          portfolioProgress: 1
        }
      }
    };
    const { subject } = mountWithStore(<PortfolioProgressReminder />, state);

    expect(
      subject
        .find("span")
        .at(1)
        .text()
    ).toBe("Low");
  });

  it("renders 'intermidiate' with some portfolio progress", () => {
    const state = {
      user: {
        currentUser: {
          portfolioProgress: 67
        }
      }
    };
    const { subject } = mountWithStore(<PortfolioProgressReminder />, state);

    expect(
      subject
        .find("span")
        .at(1)
        .text()
    ).toBe("Intermediate");
  });

  it("does not render with complete portfolio", () => {
    const state = {
      user: {
        currentUser: {
          portfolioProgress: 100
        }
      }
    };
    const { subject } = mountWithStore(<PortfolioProgressReminder />, state);

    expect(subject.isEmptyRender()).toBe(true);
  });
});
