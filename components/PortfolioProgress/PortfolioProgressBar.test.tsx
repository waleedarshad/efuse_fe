import { mountWithStore } from "../../common/testUtils";
import PortfolioProgressBar from "./PortfolioProgressBar";

describe("Portfolio progress bar component", () => {
  it("renders porfolio progress percentage", () => {
    const { subject } = mountWithStore(<PortfolioProgressBar user={{ portfolioProgress: 67 }} />);

    expect(subject.find("span").findWhere(node => node.text().includes("67%")).length).toBe(1);
    expect(subject.find("p").findWhere(node => node.text().includes("67%")).length).toBe(1);
  });

  it("does not render when porfolio is complete", () => {
    const { subject } = mountWithStore(<PortfolioProgressBar user={{ portfolioProgress: 100 }} />);

    expect(subject.isEmptyRender()).toBe(true);
  });
});
