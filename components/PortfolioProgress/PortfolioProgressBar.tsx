import { forwardRef } from "react";
import { faMedal } from "@fortawesome/pro-solid-svg-icons";

import ProgressBar from "../ProgressBar/ProgressBar";
import PortfolioProgress from "./PortfolioProgress";
import EFTippy from "../EFTippy/EFTippy";

const PortfolioProgressBar = ({ user }) => {
  const { portfolioProgress } = user;

  const Child = forwardRef<HTMLDivElement>((props, ref) => {
    return (
      <div ref={ref}>
        <ProgressBar size="large" percentage={portfolioProgress} icon={faMedal} text="Portfolio Completion" />
      </div>
    );
  });
  Child.displayName = "PortfolioProgressBar";

  return (
    portfolioProgress !== 100 && (
      <EFTippy
        interactive
        delay={[400, 100]}
        animation="scale"
        content={<PortfolioProgress user={user} />}
        onMount={() => analytics.track("PORTFOLIO_PROGRESS_OPENED")}
      >
        <Child />
      </EFTippy>
    )
  );
};

export default PortfolioProgressBar;
