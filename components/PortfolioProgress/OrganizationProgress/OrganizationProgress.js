import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus, faCheck } from "@fortawesome/pro-solid-svg-icons";

import ProgressBar from "react-bootstrap/ProgressBar";
import Style from "../PortfolioProgress.module.scss";
import OrganizationProgressPercentage from "./OrganizationProgressPercentage";

const OrganizationProgress = props => {
  const {
    progressBar,
    description,
    about,
    videoCarousel,
    playerCards,
    promoVideo,
    honors,
    events
  } = OrganizationProgressPercentage(props.organization);

  return (
    <div className={Style.container}>
      <div className={Style.header}>
        Portfolio Progress
        <p className={Style.headerPercentage}>{progressBar}%</p>
        <div className={Style.headerBg} />
      </div>
      <div className={`${Style.bodyItem} ${description && Style.bodyComplete}`}>
        <FontAwesomeIcon
          icon={description ? faCheck : faPlus}
          className={`${Style.icon} ${description && Style.check}`}
        />
        <p className={`${Style.text} ${description && Style.complete}`}>Add a bio</p>
      </div>
      <div className={`${Style.bodyItem} ${about && Style.bodyComplete}`}>
        <FontAwesomeIcon icon={about ? faCheck : faPlus} className={`${Style.icon} ${about && Style.check}`} />
        <p className={`${Style.text} ${about && Style.complete}`}>Add about section</p>
      </div>
      <div className={`${Style.bodyItem} ${videoCarousel && Style.bodyComplete}`}>
        <FontAwesomeIcon
          icon={videoCarousel ? faCheck : faPlus}
          className={`${Style.icon} ${videoCarousel && Style.check}`}
        />
        <p className={`${Style.text} ${videoCarousel && Style.complete}`}>Add video/clips to carousel</p>
      </div>
      <div className={`${Style.bodyItem} ${playerCards && Style.bodyComplete}`}>
        <FontAwesomeIcon
          icon={playerCards ? faCheck : faPlus}
          className={`${Style.icon} ${playerCards && Style.check}`}
        />
        <p className={`${Style.text} ${playerCards && Style.complete}`}>Add a card</p>
      </div>
      <div className={`${Style.bodyItem} ${promoVideo && Style.bodyComplete}`}>
        <FontAwesomeIcon
          icon={promoVideo ? faCheck : faPlus}
          className={`${Style.icon} ${promoVideo && Style.check}`}
        />
        <p className={`${Style.text} ${promoVideo && Style.complete}`}>Add a highlight video</p>
      </div>
      <div className={`${Style.bodyItem} ${honors && Style.bodyComplete}`}>
        <FontAwesomeIcon icon={honors ? faCheck : faPlus} className={`${Style.icon} ${honors && Style.check}`} />
        <p className={`${Style.text} ${honors && Style.complete}`}>Add an honor</p>
      </div>
      <div className={`${Style.bodyItem} ${events && Style.bodyComplete}`}>
        <FontAwesomeIcon icon={events ? faCheck : faPlus} className={`${Style.icon} ${events && Style.check}`} />
        <p className={`${Style.text} ${events && Style.complete}`}>Add an event</p>
      </div>

      <ProgressBar className={Style.progressBar} now={progressBar} variant="blue" />
    </div>
  );
};

export default OrganizationProgress;
