import { decimalToCleanPercentage } from "../../../helpers/GeneralHelper";
import { getOrganizationFilledFields } from "../../../helpers/OrganizationHelper";

/**
 * Calculates the organization portfolio progress percentage
 * @param {Organization} organization The organization to calculate the portfolio percentage for
 *
 * @returns  {progressBar, description, about, videoCarousel, playerCards, promoVideo, honors, events} : `progressBar` is the equivalent percentage, description, about, videoCarousel, playerCards, promoVideo, honors, events are organization filled fields
 */
const OrganizationProgressPercentage = organization => {
  let progressBarCount = 0;
  let progressBar = 0;
  const totalCount = 7;
  const organizationFields = getOrganizationFilledFields(organization);

  const updateProgress = () => {
    progressBarCount += 1;
    progressBar = (progressBarCount / totalCount) * 100;
    if (progressBarCount === totalCount) {
      progressBar = 100;
    }
  };

  const description = organizationFields.includes("description");
  description && updateProgress();

  const about = organizationFields.includes("about");
  about && updateProgress();

  const videoCarousel = organizationFields.includes("videoCarousel");
  videoCarousel && updateProgress();

  const playerCards = organizationFields.includes("playerCards");
  playerCards && updateProgress();

  const promoVideo = organizationFields.includes("promoVideo");
  promoVideo && updateProgress();

  const honors = organizationFields.includes("honors");
  honors && updateProgress();

  const events = organizationFields.includes("events");
  events && updateProgress();

  if (progressBar !== 100) {
    progressBar = decimalToCleanPercentage(progressBar);
  }
  return {
    progressBar,
    description,
    about,
    videoCarousel,
    playerCards,
    promoVideo,
    honors,
    events
  };
};

export default OrganizationProgressPercentage;
