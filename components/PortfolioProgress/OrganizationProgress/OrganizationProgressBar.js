import React from "react";
import { faMedal } from "@fortawesome/pro-solid-svg-icons";

import ProgressBar from "../../ProgressBar/ProgressBar";
import OrganizationProgressPercentage from "./OrganizationProgressPercentage";

const OrganizationProgressBar = props => {
  const { progressBar } = OrganizationProgressPercentage(props.organization);

  return <ProgressBar size="large" icon={faMedal} text="Portfolio Completion" percentage={progressBar} />;
};

export default OrganizationProgressBar;
