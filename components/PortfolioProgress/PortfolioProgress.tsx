import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import ProgressBar from "react-bootstrap/ProgressBar";
import { faPlus, faCheck } from "@fortawesome/pro-solid-svg-icons";

import { PRIMARY_FIELD_WORTH, SECONDARY_FIELD_WORTH } from "./PortfolioProgressPercentage";
import Style from "./PortfolioProgress.module.scss";

import BusinessExperienceModal from "../GlobalModals/BusinessExperienceModal/BusinessExperienceModal";
import EducationExperienceModal from "../GlobalModals/EducationExperienceModal/EducationExperienceModal";
import EventsModal from "../GlobalModals/EventsModal/EventsModal";
import HonorsModal from "../GlobalModals/HonorsModal/HonorsModal";
import LinkGamePlatformModal from "../GlobalModals/LinkGamePlatformModal/LinkGamePlatformModal";
import LinkSocialAccountModal from "../GlobalModals/LinkSocialAccountsModal/LinkSocialAccountsModal";
import LinkStreamModal from "../GlobalModals/LinkStreamModal/LinkStreamModal";
import SkillsModal from "../GlobalModals/SkillsModal/SkillsModal";

import { getUserFilledPortfolioFields, USER_FIELD_KINDS } from "../../helpers/UserPortfolioHelper";

const PortfolioProgress = ({ user }) => {
  const userFields = getUserFilledPortfolioFields(user);

  const { portfolioProgress } = user;

  // Individual progress items
  const businessExperience = userFields.includes(USER_FIELD_KINDS.BUSINESS_EXPERIENCE);
  const businessSkills = userFields.includes(USER_FIELD_KINDS.BUSINESS_SKILLS);
  const educationExperience = userFields.includes(USER_FIELD_KINDS.EDUCATION_EXPERIENCE);
  const gamePlatforms = userFields.includes(USER_FIELD_KINDS.GAME_PLATFORM);
  const gamingSkills = userFields.includes(USER_FIELD_KINDS.GAMING_SKILLS);
  const portfolioEvents = userFields.includes(USER_FIELD_KINDS.PORTFOLIO_EVENTS);
  const portfolioHonors = userFields.includes(USER_FIELD_KINDS.PORTFOLIO_HONORS);
  const socialAccount = userFields.includes(USER_FIELD_KINDS.SOCIAL_LINKED);
  const streamLinked = userFields.includes(USER_FIELD_KINDS.STREAM_LINKED);

  return (
    <div className={Style.container}>
      <div className={Style.header}>
        Portfolio Progress
        <p className={Style.headerPercentage}>{portfolioProgress}%</p>
        <div className={Style.headerBg} />
      </div>

      {/*  PRIMARY FIELDS */}
      <BusinessExperienceModal
        trackAnalytics={() => analytics.track("PORTFOLIO_PROGRESS_ITEM_CLICKED", { name: "BUSINESS_EXPERIENCE" })}
      >
        <div className={`${Style.bodyItem} ${businessExperience && Style.bodyComplete}`}>
          <FontAwesomeIcon
            icon={businessExperience ? faCheck : faPlus}
            className={`${Style.icon} ${businessExperience && Style.check}`}
          />
          <p className={`${Style.text} ${businessExperience && Style.complete}`}>Add Work Experience</p>
          <p className={`${Style.bodyItemPercent} ${businessExperience && Style.complete}`}>{PRIMARY_FIELD_WORTH}%</p>
        </div>
      </BusinessExperienceModal>
      <EducationExperienceModal
        trackAnalytics={() => analytics.track("PORTFOLIO_PROGRESS_ITEM_CLICKED", { name: "EDUCATION_EXPERIENCE" })}
      >
        <div className={`${Style.bodyItem} ${educationExperience && Style.bodyComplete}`}>
          <FontAwesomeIcon
            icon={educationExperience ? faCheck : faPlus}
            className={`${Style.icon} ${educationExperience && Style.check}`}
          />
          <p className={`${Style.text} ${educationExperience && Style.complete}`}>Add Education Experience</p>
          <p className={`${Style.bodyItemPercent} ${educationExperience && Style.complete}`}>{PRIMARY_FIELD_WORTH}%</p>
        </div>
      </EducationExperienceModal>
      <LinkGamePlatformModal
        trackAnalytics={() => analytics.track("PORTFOLIO_PROGRESS_ITEM_CLICKED", { name: "GAME_PLATFORM" })}
      >
        <div className={`${Style.bodyItem} ${gamePlatforms && Style.bodyComplete}`}>
          <FontAwesomeIcon
            icon={gamePlatforms ? faCheck : faPlus}
            className={`${Style.icon} ${gamePlatforms && Style.check}`}
          />
          <p className={`${Style.text} ${gamePlatforms && Style.complete}`}>Add a Game Platform</p>
          <p className={`${Style.bodyItemPercent} ${gamePlatforms && Style.complete}`}>{PRIMARY_FIELD_WORTH}%</p>
        </div>
      </LinkGamePlatformModal>
      <LinkSocialAccountModal
        trackAnalytics={() => analytics.track("PORTFOLIO_PROGRESS_ITEM_CLICKED", { name: "SOCIAL_ACCOUNT" })}
      >
        <div className={`${Style.bodyItem} ${socialAccount && Style.bodyComplete}`}>
          <FontAwesomeIcon
            icon={socialAccount ? faCheck : faPlus}
            className={`${Style.icon} ${socialAccount && Style.check}`}
          />
          <p className={`${Style.text} ${socialAccount && Style.complete}`}>Add a Social Account</p>
          <p className={`${Style.bodyItemPercent} ${socialAccount && Style.complete}`}>{PRIMARY_FIELD_WORTH}%</p>
        </div>
      </LinkSocialAccountModal>

      {/* SECONDARY FIELDS */}
      <SkillsModal
        type="Business"
        trackAnalytics={() => analytics.track("PORTFOLIO_PROGRESS_ITEM_CLICKED", { name: "BUSINESS_SKILLS" })}
      >
        <div className={`${Style.bodyItem} ${businessSkills && Style.bodyComplete}`}>
          <FontAwesomeIcon
            icon={businessSkills ? faCheck : faPlus}
            className={`${Style.icon} ${businessSkills && Style.check}`}
          />
          <p className={`${Style.text} ${businessSkills && Style.complete}`}>Add Business Skills</p>
          <p className={`${Style.bodyItemPercent} ${businessSkills && Style.complete}`}>+{SECONDARY_FIELD_WORTH}%</p>
        </div>
      </SkillsModal>
      <SkillsModal type="Gaming" trackAnalytics={() => analytics.track("PORTFOLIO_GAMING_SKILLS_MODAL_OPEN")}>
        <div className={`${Style.bodyItem} ${gamingSkills && Style.bodyComplete}`}>
          <FontAwesomeIcon
            icon={gamingSkills ? faCheck : faPlus}
            className={`${Style.icon} ${gamingSkills && Style.check}`}
          />
          <p className={`${Style.text} ${gamingSkills && Style.complete}`}>Add Gaming Skills</p>
          <p className={`${Style.bodyItemPercent} ${gamingSkills && Style.complete}`}>+{SECONDARY_FIELD_WORTH}%</p>
        </div>
      </SkillsModal>
      <EventsModal trackAnalytics={() => analytics.track("PORTFOLIO_PROGRESS_ITEM_CLICKED", { name: "EVENTS" })}>
        <div className={`${Style.bodyItem} ${portfolioEvents && Style.bodyComplete}`}>
          <FontAwesomeIcon
            icon={portfolioEvents ? faCheck : faPlus}
            className={`${Style.icon} ${portfolioEvents && Style.check}`}
          />
          <p className={`${Style.text} ${portfolioEvents && Style.complete}`}>Add an Event</p>
          <p className={`${Style.bodyItemPercent} ${portfolioEvents && Style.complete}`}>+{SECONDARY_FIELD_WORTH}%</p>
        </div>
      </EventsModal>
      <HonorsModal trackAnalytics={() => analytics.track("PORTFOLIO_PROGRESS_ITEM_CLICKED", { name: "HONORS" })}>
        <div className={`${Style.bodyItem} ${portfolioHonors && Style.bodyComplete}`}>
          <FontAwesomeIcon
            icon={portfolioHonors ? faCheck : faPlus}
            className={`${Style.icon} ${portfolioHonors && Style.check}`}
          />
          <p className={`${Style.text} ${portfolioHonors && Style.complete}`}>Add an Honor</p>
          <p className={`${Style.bodyItemPercent} ${portfolioHonors && Style.complete}`}>+{SECONDARY_FIELD_WORTH}%</p>
        </div>
      </HonorsModal>
      <LinkStreamModal
        user={user}
        trackAnalytics={() => analytics.track("PORTFOLIO_PROGRESS_ITEM_CLICKED", { name: "STREAM" })}
      >
        <div className={`${Style.bodyItem} ${streamLinked && Style.bodyComplete}`}>
          <FontAwesomeIcon
            icon={streamLinked ? faCheck : faPlus}
            className={`${Style.icon} ${streamLinked && Style.check}`}
          />
          <p className={`${Style.text} ${streamLinked && Style.complete}`}>Add a Stream Account</p>
          <p className={`${Style.bodyItemPercent} ${streamLinked && Style.complete}`}>+{SECONDARY_FIELD_WORTH}%</p>
        </div>
      </LinkStreamModal>
      <ProgressBar className={Style.progressBar} now={portfolioProgress} variant="custom" />
    </div>
  );
};

export default PortfolioProgress;
