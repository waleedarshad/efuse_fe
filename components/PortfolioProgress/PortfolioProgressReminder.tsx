import { useSelector } from "react-redux";
import EFProgressBar from "../EFProgressBar/EFProgressBar";
import { rescueNil } from "../../helpers/GeneralHelper";

const PortfolioProgressReminder = () => {
  const user = useSelector(state => state.user.currentUser);

  const { portfolioProgress } = user;

  return (
    portfolioProgress < 100 && (
      <EFProgressBar
        progressText="Portfolio Strength: "
        percentage={portfolioProgress}
        progressStatus={portfolioProgress <= 1 ? "Low" : "Intermediate"}
        buttonRedirectLink={`/u/${rescueNil(user, "username")}`}
      />
    )
  );
};

export default PortfolioProgressReminder;
