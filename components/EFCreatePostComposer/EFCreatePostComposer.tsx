import React, { useState } from "react";
import { useSelector } from "react-redux";
import styled from "styled-components";
import EFCard from "../Cards/EFCard/EFCard";
import EFAvatar from "../EFAvatar/EFAvatar";
import colors from "../../styles/sharedStyledComponents/colors";
import CreatePostModal from "../Modals/CreatePostModal/CreatePostModal";

const EFCreatePostComposer = ({ placeholderText, onClose = () => {}, isScheduleOnly }) => {
  const [openCreatePostModal, setOpenCreatePostModal] = useState(false);
  const avatarImageUrl = useSelector(state => state.auth.currentUser?.profilePicture?.url);

  const onCloseHandler = () => {
    setOpenCreatePostModal(false);
    onClose();
  };

  return (
    <>
      <CardWrapper>
        <EFCard widthTheme="fullWidth" shadow="none">
          <InnerCard>
            <AvatarWrapper>
              <EFAvatar
                size="small"
                borderTheme="lightColor"
                profilePicture={avatarImageUrl}
                displayOnlineButton={false}
              />
            </AvatarWrapper>
            <PlaceholderButton type="button" onClick={() => setOpenCreatePostModal(true)}>
              {placeholderText}
            </PlaceholderButton>
          </InnerCard>
        </EFCard>
      </CardWrapper>
      {openCreatePostModal && <CreatePostModal onClose={onCloseHandler} isScheduleOnly={isScheduleOnly} />}
    </>
  );
};

const CardWrapper = styled.div`
  margin-bottom: 1em;
`;

const InnerCard = styled.div`
  display: flex;
  align-items: center;
  padding: 16px;
`;

const AvatarWrapper = styled.div`
  flex-shrink: 0;
`;

const PlaceholderButton = styled.button`
  border: none;
  outline: none;
  &:focus {
    outline: none;
  }
  color: ${colors.textWeak};
  text-align: left;
  width: 100%;
  padding: 10px 10px 10px 15px;
  margin-left: 10px;
  border-radius: 5px;
  transition: 200ms;
  background: rgba(0, 0, 0, 0.12) none;
  &:hover {
    background-color: rgba(0, 0, 0, 0.175);
  }
`;

export default EFCreatePostComposer;
