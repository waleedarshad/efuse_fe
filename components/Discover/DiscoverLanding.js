import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useRouter } from "next/router";
import { Col } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCity, faUniversity } from "@fortawesome/pro-solid-svg-icons";
import { faBinoculars } from "@fortawesome/pro-solid-svg-icons";
import { faBinoculars as lightBinoculars } from "@fortawesome/pro-light-svg-icons";
import isEmpty from "lodash/isEmpty";

import Style from "./DiscoverLanding.module.scss";
import CustomCarousel from "../Carousels/CustomCarousel/CustomCarousel";
import { getOrganizationsNew } from "../../store/actions/organizationActions";
import { getNewsArticlesNew } from "../../store/actions/newsActions";
import DisplayOrganizations from "./DisplayOrganizations/DisplayOrganizations";
import DisplayNews from "./DisplayNews/DisplayNews";
import { returnNewsObject } from "../../helpers/NewsHelper";
import { returnOrganizationObject } from "../../helpers/OrganizationHelper";
import EFSubHeader from "../Layouts/Internal/EFSubHeader/EFSubHeader";
import { getDiscoverNavigationList } from "../../navigation/discover";
import AuthAwareLayout from "../Layouts/AuthAwareLayout/AuthAwareLayout";

let organizationPage = 1;
let newsPage = 1;
const DiscoverLanding = () => {
  const organizations = useSelector(state => state.organizations.organizations);
  const organizationHasNextPage = useSelector(state => state.organizations.hasNextPage);
  const newsArticles = useSelector(state => state.news.articles);
  const newsPagination = useSelector(state => state.news.pagination);
  const featured = useSelector(state => state.features.discover_graphql_crousal);
  const showErenaLink = useSelector(state => state.features.web_erena_in_discover_navigation);
  const userIsLoggedIn = useSelector(state => state.auth.currentUser?.id);

  const dispatch = useDispatch();
  const router = useRouter();

  const loadMoreOrganizations = () => {
    organizationPage += 1;
    dispatch(getOrganizationsNew(organizationPage, 16));
  };

  const loadMoreNews = () => {
    newsPage += 1;
    dispatch(getNewsArticlesNew(!userIsLoggedIn, newsPage, 16));
  };

  const switchActiveTab = tab => {
    router.push(`/discover?${tab}`);
  };

  useEffect(() => {
    analytics.page("Discover Landing Page");
    if (!featured) {
      dispatch(getOrganizationsNew(organizationPage, 16));
      dispatch(getNewsArticlesNew(!userIsLoggedIn, newsPage, 16));
    }
  }, []);

  let itemsList = [];
  if (newsArticles?.length > 0 && organizations?.length > 0) {
    itemsList = [
      returnOrganizationObject(organizations[0]),
      returnNewsObject(newsArticles[0]),
      returnOrganizationObject(organizations[1]),
      returnNewsObject(newsArticles[1])
    ];
  }

  const mainContent = (
    <>
      <EFSubHeader
        headerIcon={faBinoculars}
        navigationList={getDiscoverNavigationList(router.pathname, showErenaLink)}
      />
      <Col sm={12} className={`${Style.mainContent} no-gutters`}>
        {itemsList.length > 0 && <CustomCarousel sideCardHighlight itemsList={itemsList} featured={featured} />}
        <div className={Style.discoverHeaderBar}>
          <div className={Style.discoverText}>
            <FontAwesomeIcon icon={lightBinoculars} className={Style.discoverIcon} />
            <h1 className={Style.discoverTitle}>Discover</h1>
          </div>
          <div className={Style.dicoverItemBar}>
            <div
              className={`${Style.discoverItemMobile} ${(router?.query.organizations !== undefined ||
                isEmpty(router?.query)) &&
                Style.active}`}
              onClick={() => switchActiveTab("organizations")}
            >
              <FontAwesomeIcon icon={faCity} className={Style.headerIcon} />
            </div>

            <div
              className={`${Style.discoverItemMobile} ${router?.query.news !== undefined && Style.active}`}
              onClick={() => switchActiveTab("news")}
            >
              <FontAwesomeIcon icon={faUniversity} className={Style.headerIcon} />
            </div>
            <div
              className={`${Style.discoverItem} ${(router?.query.organizations !== undefined ||
                isEmpty(router?.query)) &&
                Style.active}`}
              onClick={() => switchActiveTab("organizations")}
            >
              Organizations
            </div>

            <div
              className={`${Style.discoverItem} ${router?.query.news !== undefined && Style.active}`}
              onClick={() => switchActiveTab("news")}
            >
              News
            </div>
          </div>
        </div>
        <div className={Style.cardGrid}>
          {(router?.query.organizations !== undefined || isEmpty(router?.query)) && (
            <DisplayOrganizations
              loadMore={loadMoreOrganizations}
              organizationHasNextPage={organizationHasNextPage}
              organizations={organizations}
              marginTop={Style.marginTop}
            />
          )}
          {router?.query.news !== undefined && (
            <DisplayNews
              loadMore={loadMoreNews}
              newsPagination={newsPagination}
              newsArticles={newsArticles}
              marginTop={Style.marginTop}
            />
          )}
        </div>
      </Col>
    </>
  );

  return (
    <AuthAwareLayout metaTitle="eFuse | Discover" containsSubheader>
      {mainContent}
    </AuthAwareLayout>
  );
};

export default DiscoverLanding;
