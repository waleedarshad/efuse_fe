import React from "react";
import InfiniteScroll from "react-infinite-scroller";
import GeneralCard from "../../Cards/GeneralCard/GeneralCard";
import AnimatedLogo from "../../AnimatedLogo";
import { getOrganizationUrl } from "../../../helpers/UrlHelper";

const DisplayOrganizations = props => {
  const { organizations, organizationHasNextPage, loadMore } = props;
  return (
    <InfiniteScroll
      pageStart={1}
      loadMore={loadMore}
      hasMore={organizationHasNextPage}
      loader={<AnimatedLogo key={0} theme="inline" />}
      style={{ display: "flex", flexWrap: "wrap", justifyContent: "center" }}
    >
      <>
        {organizations?.map((organization, index) => {
          return (
            <GeneralCard
              badge="Organizations"
              key={index}
              title={organization?.name}
              subTitle={organization?.organizationType}
              onImageClickLink={getOrganizationUrl(organization)}
              onTitleClickLink={getOrganizationUrl(organization)}
              image={organization?.headerImage?.url}
              marginTop={props.marginTop}
              description={organization?.description}
              footerText={`${organization?.followersCount} Followers`}
              adjustToFullWidthOnMobile
            >
              test
            </GeneralCard>
          );
        })}
      </>
    </InfiniteScroll>
  );
};

export default DisplayOrganizations;
