import React from "react";
import InfiniteScroll from "react-infinite-scroller";
import GeneralCard from "../../Cards/GeneralCard/GeneralCard";
import AnimatedLogo from "../../AnimatedLogo";

const DisplayNews = ({ newsArticles, newsPagination, loadMore, marginTop }) => {
  return (
    <InfiniteScroll
      pageStart={1}
      loadMore={loadMore}
      hasMore={newsPagination?.hasNextPage}
      loader={<AnimatedLogo key={0} theme="inline" />}
      style={{ display: "flex", flexWrap: "wrap", justifyContent: "center" }}
    >
      <>
        {newsArticles?.map((article, index) => {
          const postedByOrganization = article?.authorType === "organizations";
          const ownerUrl = postedByOrganization
            ? `/organizations/show?id=${article?.author?._id}`
            : `/u/${article?.author?.username}`;
          return (
            <GeneralCard
              badge="News"
              key={index}
              title={article?.title}
              subTitle={article?.author?.name}
              onImageClickLink={article?.url}
              onTitleClickLink={postedByOrganization ? ownerUrl : "/u/[u]"}
              image={article?.image?.url}
              marginTop={marginTop}
              date={article?.createdAt}
              adjustToFullWidthOnMobile
              titleLinkAs={ownerUrl}
            >
              test
            </GeneralCard>
          );
        })}
      </>
    </InfiniteScroll>
  );
};

export default DisplayNews;
