import React from "react";
import EFResponsiveChip from "./EFResponsiveChip";

export default {
  title: "Shared/EFResponsiveChip",
  component: EFResponsiveChip,
  argsTypes: {
    theme: {
      control: {
        type: "select",
        options: ["job", "event", "scholarship", "teamOpening"]
      }
    },
    text: {
      control: {
        type: "text"
      }
    }
  }
};

const Story = args => (
  <div style={{ width: "280px", margin: "3rem" }}>
    <EFResponsiveChip {...args} />
  </div>
);

export const Job = Story.bind({});
Job.args = {
  text: "Job",
  theme: "job"
};

export const Scholarship = Story.bind({});
Scholarship.args = {
  text: "Scholarship",
  theme: "scholarship"
};

export const TeamOpening = Story.bind({});
TeamOpening.args = {
  text: "Team Opening",
  theme: "teamOpening"
};

export const Event = Story.bind({});
Event.args = {
  text: "Event",
  theme: "event"
};
