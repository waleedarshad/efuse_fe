import React from "react";
import styled, { css } from "styled-components";
import chipThemes from "../../styles/sharedStyledComponents/chipThemes";
import { BadgeText } from "../../styles/sharedStyledComponents/sharedStyles";
import colors from "../../styles/sharedStyledComponents/colors";

interface EFResponsiveChipProps {
  badgeType: string;
  size?: string;
  newVersion?: boolean;
  className?: string;
}

const EFResponsiveChip: React.FC<EFResponsiveChipProps> = ({
  className = "",
  badgeType,
  size = "regular",
  newVersion = false
}) => {
  return (
    <Chip className={className} size={size} badgeType={badgeType} newVersion={newVersion}>
      {badgeType}
    </Chip>
  );
};

const Original = css`
  background-color: rgba(0, 0, 0, 0.6);
  font-weight: 700;
  padding: 3px 6px;
  height: 15px;
`;

const Chip = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 20px;
  width: fit-content;
  color: ${colors.white};
  text-transform: ${props => (chipThemes[props.badgeType] === "news" || !props.newVersion ? "uppercase" : "none")};
  background: linear-gradient(0deg, rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6)), ${props => chipThemes[props.badgeType]};
  border: ${props => (!props.newVersion ? 2 : 3)}px solid ${props => chipThemes[props.badgeType]};
  ${BadgeText};
  ${props => !props.newVersion && Original};
  ${props => (props.size === "small" ? SmallSize : RegularSize)};
`;

const SmallSize = css`
  padding: 8px;
  height: 15px;
  font-size: 8px;
  font-weight: 600;
`;

const RegularSize = css`
  padding: 3px 10px;
  height: 25px;
  font-size: 10px;
`;

export default EFResponsiveChip;
