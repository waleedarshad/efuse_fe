import React from "react";
import { faSquare as faSquareSolid } from "@fortawesome/pro-solid-svg-icons";
import { faSquare as faSquareRegular } from "@fortawesome/pro-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Style from "./EFNewCheckbox.module.scss";
import EFSquareImage from "../EFSquareImage/EFSquareImage";

interface EFNewCheckboxProps {
  imageUrl: string;
  title: string;
  disabled: boolean;
  id: string;
  isChecked: boolean;
  onChange: () => void;
}

const EFNewCheckbox: React.FC<EFNewCheckboxProps> = ({ imageUrl, title, id, onChange, isChecked, disabled }) => {
  return (
    <label htmlFor={id} className={`${Style.label} ${isChecked && Style.selected} ${disabled && Style.disabled} `}>
      <div className={Style.contentContainer}>
        <div>
          <EFSquareImage src={imageUrl} size={30} />
          <span className={Style.title}>{title}</span>
        </div>
        <div className="form-check">
          <input
            className={`form-check-input ${Style.input}`}
            type="checkbox"
            id={id}
            onChange={onChange}
            checked={isChecked}
            disabled={disabled}
          />

          {!disabled && (
            <FontAwesomeIcon icon={isChecked ? faSquareSolid : faSquareRegular} className={Style.checkbox} />
          )}
        </div>
      </div>
    </label>
  );
};

export default EFNewCheckbox;
