import React from "react";
import EFNewCheckbox from "./EFNewCheckbox";

export default {
  title: "Inputs/EFNewCheckbox",
  component: EFNewCheckbox,
  argTypes: {
    imageUrl: {
      control: {
        type: "text"
      }
    },
    title: {
      control: {
        type: "text"
      }
    },
    id: {
      control: {
        type: "text"
      }
    },
    onSelect: {
      control: {
        type: "function"
      }
    },
    isChecked: {
      control: {
        type: "boolean"
      }
    },
    disabled: {
      control: {
        type: "boolean"
      }
    }
  }
};

// eslint-disable-next-line react/jsx-props-no-spreading
const Story = args => <EFNewCheckbox {...args} />;

export const Checked = Story.bind({});
Checked.args = {
  imageUrl: "https://i.ytimg.com/vi/0E44DClsX5Q/maxresdefault.jpg",
  title: "New York Excelsior",
  id: "1",
  isChecked: true
};

export const Unchecked = Story.bind({});
Unchecked.args = {
  imageUrl: "https://i.ytimg.com/vi/0E44DClsX5Q/maxresdefault.jpg",
  title: "New York Excelsior",
  id: "1",
  isChecked: false
};

export const Disabled = Story.bind({});
Disabled.args = {
  imageUrl: "https://i.ytimg.com/vi/0E44DClsX5Q/maxresdefault.jpg",
  title: "New York Excelsior",
  id: "1",
  isChecked: false,
  disabled: true
};
