import { Card } from "react-bootstrap";
import PropTypes from "prop-types";

import Style from "./OrgBioCardLayout.module.scss";
import DisplayLocation from "../../DisplayLocation/DisplayLocation";
import VerifiedIcon from "../../VerifiedIcon/VerifiedIcon";

const OrgBioCardLayout = ({ name, subTitle, location, bio, membersCount, children, followersCount, verified }) => (
  <Card className="customCard mb-4">
    <Card.Body className={Style.bodyWrapper}>
      <div className={Style.header}>
        <h4 className={Style.name}>
          {name}
          {verified && <VerifiedIcon organization />}
        </h4>
      </div>
      {subTitle && <h6 className={Style.userType}>{subTitle}</h6>}
      {bio && <p className={Style.bio}>{bio}</p>}
      <DisplayLocation location={location} />
      {children}
    </Card.Body>
    <Card.Footer className={Style.footer}>
      <div className={Style.counter}>
        <h6 className={Style.numberTitle}>Followers</h6>
        <h5 className={Style.number}>{followersCount}</h5>
      </div>
      <div className={Style.counter}>
        <h6 className={Style.numberTitle}>Members</h6>
        <h5 className={Style.number}>{membersCount}</h5>
      </div>
    </Card.Footer>
  </Card>
);

OrgBioCardLayout.propTypes = {
  name: PropTypes.string,
  subTitle: PropTypes.string,
  bio: PropTypes.string,
  location: PropTypes.string,
  membersCount: PropTypes.number
};

OrgBioCardLayout.defaultProps = {
  membersCount: 0
};

export default OrgBioCardLayout;
