import React, { Component } from "react";
import { connect } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faGlobe } from "@fortawesome/pro-solid-svg-icons";
import Style from "./OrganizationBioCard.module.scss";
import OrgBioCardLayout from "./OrgBioCardLayout/OrgBioCardLayout";
import FollowOrganization from "../Organizations/FollowOrganization/FollowOrganization";
import { rescueNil } from "../../helpers/GeneralHelper";
import { getOwnerAndCaptain } from "../../helpers/OrganizationHelper";

class OrganizationBioCard extends Component {
  render() {
    const { organization, currentUser } = this.props;
    const { owner } = getOwnerAndCaptain(currentUser, organization);
    const verifiedObject = rescueNil(organization, "verified", {});
    return (
      <OrgBioCardLayout
        name={organization.name}
        subTitle={organization.organizationType}
        displayAvatar={false}
        location={organization.location}
        membersCount={organization.membersCount}
        followersCount={organization.followersCount}
        verified={rescueNil(verifiedObject, "status", false)}
        bio={organization.description}
      >
        <a
          href="#"
          onClick={() => {
            analytics.track("ORGANIZATION_WEBSITE_CLICK", { organizationId: organization._id }, {}, () => {
              window.open(organization.website, "_blank");
            });
          }}
          className={Style.websiteChip}
        >
          <FontAwesomeIcon icon="globe" />
        </a>
        {!owner && <FollowOrganization organization={organization} />}
      </OrgBioCardLayout>
    );
  }
}

const mapStateToProps = state => ({
  organization: state.organizations.organization,
  members: state.organizations.members
});

export default connect(mapStateToProps)(OrganizationBioCard);
