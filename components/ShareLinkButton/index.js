import { useDispatch } from "react-redux";
import { shareLinkModal } from "../../store/actions/shareAction";
import Style from "./index.module.scss";
import { SHARE_LINK_EVENTS } from "../../common/analyticEvents";
import EFCircleIconButton from "../Buttons/EFCircleIconButton/EFCircleIconButton";

const ShareLinkButton = props => {
  const dispatch = useDispatch();

  const { url, title, childComponent, icon, style, shareLinkKind } = props;

  const onChildButtonClick = () => {
    analytics.track(SHARE_LINK_EVENTS.SHARE_BUTTON_CLICKED, {
      kind: shareLinkKind,
      url
    });
    dispatch(shareLinkModal(true, title, url, shareLinkKind));
  };

  return (
    <div
      className={icon ? "d-inline-block" : Style.topContainer}
      onClick={onChildButtonClick}
      onKeyPress={onChildButtonClick}
      role="button"
      tabIndex={0}
    >
      {childComponent || <EFCircleIconButton icon={icon} />}
    </div>
  );
};

export default ShareLinkButton;
