import React from "react";
import EFMatchup from "./EFMatchup";

export default {
  title: "Cards/EFMatchup",
  component: EFMatchup,
  argTypes: {
    teams: {
      control: {
        type: "array"
      }
    },
    onclick: {
      control: {
        type: "function"
      }
    },
    matchupIsFinal: {
      control: {
        type: "boolean"
      }
    }
  }
};

const Story = args => (
  <div className="p-4">
    {/* eslint-disable-next-line react/jsx-props-no-spreading */}
    <EFMatchup {...args} />
  </div>
);

export const Default = Story.bind({});
Default.args = {
  teams: [
    {
      image: {
        url: "https://i.ytimg.com/vi/0E44DClsX5Q/maxresdefault.jpg"
      },
      name: "Money Ball",
      score: 2
    },
    {
      image: {
        url: "https://staging-cdn.efuse.gg/uploads/users/1617984471725-Overwatch-285x380.jpg"
      },
      name: "Dollar",
      score: 0
    }
  ],
  onClick: () => {},
  matchupIsFinal: true
};
