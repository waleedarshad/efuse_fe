import React, { useEffect, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrophy } from "@fortawesome/pro-solid-svg-icons";
import max from "lodash/max";
import { useQuery } from "@apollo/client";
import styled from "styled-components";
import { TruncateWithEllipsis } from "../../styles/sharedStyledComponents/sharedStyles";
import EFImage from "../EFImage/EFImage";
import Style from "./EFMatchup.module.scss";
import {
  GET_SCORES,
  GetScoresData,
  GetScoresVars
} from "../Leagues/Pages/Bracket/FinalizeMatchResultsModal/FinalizeMatchResults/FinalizeMatchResultsQueries";
import { LeagueGameScore, SubmittedScoreType } from "../../graphql/interface/League";

interface Team {
  _id: string;
  image: {
    url: string;
  };
  name: string;
  score?: number;
  owner?: {
    profileImage?: {
      url?: string;
    };
  };
}

interface EFMatchupProps {
  teams: Team[];
  onClick: () => void;
  matchupIsFinal: boolean;
  matchId: string;
}

const EFMatchup: React.FC<EFMatchupProps> = ({ teams, onClick, matchupIsFinal, matchId }) => {
  const [numberOfWinsPerTeam, setNumberOfWinsPerTeam] = useState({});
  const [leadingTeamId, setLeadingTeamId] = useState("");

  const { data } = useQuery<GetScoresData, GetScoresVars>(GET_SCORES, {
    variables: { matchId },
    fetchPolicy: "network-only"
  });

  useEffect(() => {
    if (data) {
      const finalizedScores = data?.getLeagueGameScores.filter(score => score.type === SubmittedScoreType.FINALIZED);

      const numberOfWins = calculateNumberOfFinalWins(finalizedScores, teams);
      setNumberOfWinsPerTeam(numberOfWins);

      const maxScore = max(Object.values(numberOfWins));

      const teamsWithHighestScore = Object.keys(numberOfWins).filter(teamId => numberOfWins[teamId] === maxScore);

      setLeadingTeamId(teamsWithHighestScore.length === 1 ? teamsWithHighestScore[0] : null);
    }
  }, [data]);

  const displayTeams = teams?.map((team, index) => {
    return (
      <div
        className={`${Style.teamDetail} ${index === 0 && Style.borderBottom} ${team._id === leadingTeamId &&
          Style.blueText} `}
        key={team.name}
      >
        <TeamInfo>
          <EFImage src={team.owner?.profileImage?.url} className={Style.teamImage} alt={team.name} />
          <span className={`${Style.teamText} ml-2`}>{team.name}</span>
        </TeamInfo>
        <div>
          <span className={Style.teamText}>{numberOfWinsPerTeam[team._id] || 0}</span>
        </div>
      </div>
    );
  });

  return (
    <div
      className={`${Style.matchCard} ${matchupIsFinal && Style.showIcon}`}
      onClick={onClick}
      aria-hidden="true"
      role="button"
    >
      <div className={Style.iconContainer}>
        <FontAwesomeIcon icon={faTrophy} className={Style.icon} />
      </div>
      {displayTeams}
    </div>
  );
};

const calculateNumberOfFinalWins = (finalizedScores: LeagueGameScore[], teams: Team[]) => {
  const numberOfWinsByTeamId = teams.reduce((acc, team) => {
    return { ...acc, [team._id]: 0 };
  }, {});

  finalizedScores.forEach(finalScore => {
    const firstTeamScore = finalScore.scores[0]?.value;
    const firstTeamId = finalScore.scores[0]?.competitor?._id;

    const secondTeamScore = finalScore.scores[1]?.value;
    const secondTeamId = finalScore.scores[1]?.competitor?._id;

    if (firstTeamScore !== secondTeamScore) {
      if (firstTeamScore > secondTeamScore) {
        numberOfWinsByTeamId[firstTeamId] += 1;
      } else {
        // secondTeamScore wins this one
        numberOfWinsByTeamId[secondTeamId] += 1;
      }
    }
  });

  return numberOfWinsByTeamId;
};

const TeamInfo = styled.div`
  ${TruncateWithEllipsis};
  width: 160px;
`;

export default EFMatchup;
