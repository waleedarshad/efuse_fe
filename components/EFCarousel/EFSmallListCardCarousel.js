import React from "react";
import Slider from "react-slick";
import { faChevronLeft, faChevronRight } from "@fortawesome/pro-regular-svg-icons";
import Style from "./EFCarousel.module.scss";
import EFCircleIconButton from "../Buttons/EFCircleIconButton/EFCircleIconButton";

const EFSmallListCardCarousel = ({ children }) => {
  const length = children?.length;

  const NextArrow = ({ onClick, currentSlide, style }) => {
    return (
      <div
        style={{ ...style, position: "absolute" }}
        className={`slick-arrow ${length && length <= currentSlide + 2 ? Style.hide : Style.arrowRight}`}
      >
        <div className={Style.rightFade} />
        <EFCircleIconButton colorTheme="white" shadowTheme="carousel" icon={faChevronRight} onClick={onClick} />
      </div>
    );
  };

  const PrevArrow = ({ onClick, currentSlide, style }) => {
    return (
      <>
        <div
          style={{ ...style, position: "absolute" }}
          className={`slick-arrow ${currentSlide === 0 ? Style.hide : Style.arrowLeft}`}
        >
          <EFCircleIconButton colorTheme="white" shadowTheme="carousel" icon={faChevronLeft} onClick={onClick} />
        </div>
      </>
    );
  };

  const settings = {
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: 3.1,
    slidesToScroll: 3,
    swipeToSlide: true,
    prevArrow: <PrevArrow />,
    nextArrow: <NextArrow />,
    responsive: [
      {
        breakpoint: 1315,
        settings: {
          slidesToShow: 2.71,
          slidesToScroll: 2,
          infinite: false,
          swipeToSlide: true
        }
      },
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 2.17,
          slidesToScroll: 2,
          infinite: false,
          swipeToSlide: true
        }
      },
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 1.63,
          slidesToScroll: 2,
          infinite: false,
          swipeToSlide: true
        }
      },
      {
        breakpoint: 768,
        settings: {
          swipeToSlide: true,
          slidesToShow: 1.29,
          slidesToScroll: 1,
          infinite: false
        }
      },
      {
        breakpoint: 600,
        settings: {
          swipeToSlide: true,
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: false
        }
      }
    ]
  };

  return (
    // eslint-disable-next-line react/jsx-props-no-spreading
    length > 0 ? <Slider {...settings}>{children}</Slider> : null
  );
};

export default EFSmallListCardCarousel;
