import React, { Component } from "react";
import { connect } from "react-redux";
import Link from "next/link";
import { withRouter } from "next/router";
import Style from "./SocialLinksIcons.module.scss";
import SocialLinkItem from "./SocialLinkItem/SocialLinkItem";

const LINKS = ["facebook", "instagram", "linkedin", "tiktok", "twitch", "twitter"];

class SocialLinksIcons extends Component {
  hasValues = () => {
    let hasKey = false;
    LINKS.forEach(link => {
      if (this.props.user[`${link}Link`]) {
        hasKey = true;
      }
    });

    return hasKey;
  };

  hasValuesPassedUser = () => {
    let hasKey = false;
    LINKS.forEach(link => {
      if (this.props.passedUser[`${link}Link`]) {
        hasKey = true;
      }
    });

    return hasKey;
  };

  render() {
    const { user, router, currentUser, passedUser } = this.props;
    if (passedUser) {
      const buildLinks = LINKS.map((link, index) => (
        <SocialLinkItem
          user={passedUser}
          objectKey={`${link}Link`}
          key={index}
          icon={`https://cdn.efuse.gg/uploads/static/global/${link}_icon.png`}
        />
      ));
      return <>{this.hasValuesPassedUser() ? <div className={Style.socialIcons}>{buildLinks}</div> : <></>}</>;
    }
    const isCurrentUser = currentUser && router.query.id === currentUser.id;
    const buildLinks = LINKS.map((link, index) => (
      <SocialLinkItem
        user={user}
        objectKey={`${link}Link`}
        key={index}
        icon={`https://cdn.efuse.gg/uploads/static/global/${link}_icon.png`}
      />
    ));
    return (
      <>
        {this.hasValues() ? (
          <div className={Style.socialIcons}>{buildLinks}</div>
        ) : (
          isCurrentUser && (
            <Link href="/settings/social_links">
              <a className={`m-2 ${Style.link}`}>Click here to add Social Links</a>
            </Link>
          )
        )}
      </>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user.currentUser,
  currentUser: state.auth.currentUser
});

export default connect(mapStateToProps)(withRouter(SocialLinksIcons));
