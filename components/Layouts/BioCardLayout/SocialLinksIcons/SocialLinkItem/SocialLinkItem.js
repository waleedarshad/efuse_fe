import PropTypes from "prop-types";

import Style from "../SocialLinksIcons.module.scss";

// Making sure any social links are valid URLs and to limit XSS
function isValidURL(str) {
  const pattern = new RegExp(
    "^(https?:\\/\\/)?" + // protocol
    "((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|" + // domain name
    "((\\d{1,3}\\.){3}\\d{1,3}))" + // OR ip (v4) address
    "(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*" + // port and path
    "(\\?[;&a-z\\d%_.~+=-]*)?" + // query string
      "(\\#[-a-z\\d_]*)?$",
    "i"
  ); // fragment locator

  return !!pattern.test(str);
}

const SocialLinkItem = ({ icon, user, objectKey }) => (
  <>
    {user[objectKey] && (
      <a
        href="#"
        onClick={() => {
          analytics.track("USER_SOCIAL_ICON_CLICK", { url: user[objectKey] }, {}, () => {
            const win = window.open(isValidURL(user[objectKey]) ? user[objectKey] : "", "_blank");
            win.focus();
          });
        }}
      >
        <img className={Style.icon} src={icon} />
      </a>
    )}
  </>
);

SocialLinkItem.propTypes = {
  icon: PropTypes.string.isRequired,
  user: PropTypes.object.isRequired,
  objectKey: PropTypes.string.isRequired
};

export default SocialLinkItem;
