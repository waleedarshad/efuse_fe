import { Card } from "react-bootstrap";
import PropTypes from "prop-types";
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";
import Style from "./BioCardLayout.module.scss";
import DisplayLocation from "../../DisplayLocation/DisplayLocation";
import EFAvatar from "../../EFAvatar/EFAvatar";
import SocialLinksIcons from "./SocialLinksIcons/SocialLinksIcons";
import VerifiedIcon from "../../VerifiedIcon/VerifiedIcon";
import SignupModal from "../../SignupModal/SignupModal";
import OnlineCircle from "../../OnlineCircle/OnlineCircle";
import { getImage } from "../../../helpers/GeneralHelper";
import EFStreakBadge from "../../EFStreakBadge/EFStreakBadge";

const BioCardLayout = ({
  profilePicture,
  name,
  username,
  mainOrganization,
  location,
  bio,
  followers,
  followees,
  children,
  verified,
  displayAvatar,
  og,
  userId,
  socialLinksFlag,
  hideOnMobile,
  currentUser,
  user
}) => {
  const userLoggedIn = currentUser?.id;
  const dailyStreak = user?.streaks?.daily;
  const innerBody = (
    <Card.Body className={Style.bodyWrapper}>
      {displayAvatar && (
        <EFAvatar
          displayOnlineButton={false}
          profilePicture={getImage(profilePicture, "avatar")}
          size="large"
          href={`/u/${user?.username}`}
        />
      )}

      <h4 className={Style.name}>
        {name}
        {verified && <VerifiedIcon />}
        {dailyStreak?.isActive && dailyStreak.streakDuration > 0 && (
          <span className={Style.streak}>
            <EFStreakBadge streak={dailyStreak.streakDuration} />
          </span>
        )}
      </h4>
      <div>
        <h6 className={Style.username}>
          <span className={Style.marginRight}>{username}</span>
          <OnlineCircle online={user?.online && user?.showOnline} />
        </h6>
        <h5 className={Style.og}>{og}</h5>
      </div>
      {mainOrganization && <h6 className={Style.userType}>{mainOrganization.name}</h6>}
      {bio && <p className={Style.bio}>{bio}</p>}
      <div className={Style.location}>
        <DisplayLocation location={location} />
      </div>
      {children}
      {socialLinksFlag && <SocialLinksIcons />}
    </Card.Body>
  );

  return (
    <Card className={`customCard mb-4 ${Style.profileCard} ${hideOnMobile ? Style.hideOnMobile : ""}`}>
      {userId ? (
        innerBody
      ) : (
        <>
          <div className={Style.skeleton}>
            <SkeletonTheme color="#fff">
              <Skeleton circle height={80} width={80} />
            </SkeletonTheme>
          </div>
        </>
      )}
      <Card.Footer className={Style.footer}>
        {userLoggedIn ? (
          <>
            <a className={Style.link} href={`/users/followers?id=${userId}`}>
              <div className={Style.counter}>
                <h6 className={Style.numberTitle}>Followers</h6>
                <h5 className={Style.number}>{followers}</h5>
              </div>
            </a>
            <a className={Style.link} href={`/users/followees?id=${userId}`}>
              <div className={Style.counter}>
                <h6 className={Style.numberTitle}>Following</h6>
                <h5 className={Style.number}>{followees}</h5>
              </div>
            </a>
          </>
        ) : (
          <>
            <SignupModal
              title={`Create an account to view ${user?.name}'s followers.`}
              message={`Never miss an update from ${user?.name}. Sign up today!`}
            >
              <div className={Style.counter}>
                <h6 className={Style.numberTitle}>Followers</h6>
                <h5 className={Style.number}>{followers}</h5>
              </div>
            </SignupModal>
            <SignupModal
              title={`Create an account to view ${user?.name}'s following.`}
              message={`Never miss an update from ${user?.name}. Sign up today!`}
            >
              <div className={Style.counter}>
                <h6 className={Style.numberTitle}>Following</h6>
                <h5 className={Style.number}>{followees}</h5>
              </div>
            </SignupModal>
          </>
        )}
      </Card.Footer>
    </Card>
  );
};

BioCardLayout.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  profilePicture: PropTypes.object,
  name: PropTypes.string,
  bio: PropTypes.string,
  location: PropTypes.string,
  followers: PropTypes.number,
  followees: PropTypes.number,
  displayAvatar: PropTypes.bool
};

BioCardLayout.defaultProps = {
  followers: 0,
  followees: 0,
  displayAvatar: true,
  name: "",
  bio: "",
  location: "",
  profilePicture: {}
};

export default BioCardLayout;
