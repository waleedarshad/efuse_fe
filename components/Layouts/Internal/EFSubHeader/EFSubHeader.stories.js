import React from "react";
import { faGamepad, faPlus, faHeadset } from "@fortawesome/pro-solid-svg-icons";

import EFSubHeader from "./EFSubHeader";
import EFCircleIconButtonTooltip from "../../../Buttons/EFCircleIconButtonTooltip/EFCircleIconButtonTooltip";

export default {
  title: "Header/EFSubHeader",
  component: EFSubHeader,
  argTypes: {
    headerIcon: {
      control: {
        type: "select",
        options: ["headset", "binoculars", "couch", "bell"]
      }
    },
    navigationList: {
      control: {
        type: "array"
      }
    },
    actionButtons: {
      control: {
        type: "array"
      }
    }
  }
};

const Story = args => <EFSubHeader {...args} />;

export const Basic = Story.bind({});
Basic.args = {
  headerIcon: faHeadset,
  navigationList: [
    { href: "/testroute", name: "Lounge", isActive: true },
    { href: "/testroute", name: "Discover", isActive: false },
    { href: "/testroute", name: "Opportunity", isActive: false },
    { href: "/testroute", name: "Connections", isActive: false },
    { href: "/testroute", name: "Portfolio", isActive: false }
  ],
  actionButtons: []
};

export const BasicTwo = Story.bind({});
BasicTwo.args = {
  headerIcon: faHeadset,
  navigationList: [
    { href: "/testroute", name: "Lounge", isActive: false },
    { href: "/testroute", name: "Discover", isActive: true },
    { href: "/testroute", name: "Opportunity", isActive: false },
    { href: "/testroute", name: "Connections", isActive: false },
    { href: "/testroute", name: "Portfolio", isActive: false }
  ],
  actionButtons: []
};

export const Buttons = Story.bind({});
Buttons.args = {
  headerIcon: faHeadset,
  actionButtons: [
    <EFCircleIconButtonTooltip
      theme="light"
      icon={faGamepad}
      shadowTheme="small"
      tooltipPlacement="bottom"
      tooltipContent="Button Here!"
      size="medium"
      key={1}
    />,
    <EFCircleIconButtonTooltip
      theme="light"
      icon={faPlus}
      shadowTheme="small"
      tooltipPlacement="bottom"
      tooltipContent="Button Here!"
      size="medium"
      key={2}
    />
  ],
  navigationList: [
    { href: "/testroute", name: "Lounge", isActive: true },
    { href: "/testroute", name: "Discover", isActive: false },
    { href: "/testroute", name: "Opportunity", isActive: false },
    { href: "/testroute", name: "Connections", isActive: false },
    { href: "/testroute", name: "Portfolio", isActive: false }
  ]
};
