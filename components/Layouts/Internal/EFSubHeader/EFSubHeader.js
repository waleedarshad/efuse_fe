import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import uniqueId from "lodash/uniqueId";
import HorizontalScroll from "../../../HorizontalScroll/HorizontalScroll";
import EFImage from "../../../EFImage/EFImage";
import EFPillButton from "../../../Buttons/EFPillButton/EFPillButton";
import Style from "./EFSubHeader.module.scss";

const EFSubHeader = ({
  headerIcon,
  headerImage,
  navigationList,
  actionButtons,
  // align navigation list items in the center
  alignNavigationCenter,
  // buttons can exist outside the scroll component or inside, this prop will place them inside the horizontal scroll
  includeButtonsInScroll,
  // this prop can be used to ignore margin/shadow designs (used if you want the navigation component as a non-subheader)
  positionUnderNav,
  // Custom style of the image (Erena specific)
  customImageStyle
}) => {
  const buttons = (
    <>
      {actionButtons?.length > 0 && (
        <div className={Style.actionButtons}>
          {actionButtons?.map(button => {
            return (
              <React.Fragment key={uniqueId()}>
                <div className={Style.addMarginLeft}>{button}</div>
              </React.Fragment>
            );
          })}
        </div>
      )}
    </>
  );

  return (
    <div className={`${Style.entireWrapper} ${!positionUnderNav && Style.positionNotUnderNav}`}>
      <div className={Style.topContentWrapper}>
        <div className={Style.alignTopLeft}>
          {headerIcon && <FontAwesomeIcon icon={headerIcon} className={Style.icon} />}
          {headerImage && (
            <div className={Style.headerImageWrapper}>
              <div className={customImageStyle}>
                <EFImage height="40" width="40" src={headerImage} />
              </div>
            </div>
          )}
          <div className={Style.navWrapper}>
            {navigationList?.length > 0 && (
              <HorizontalScroll noPadding withLeftArrow={false} withRightArrow={false}>
                <div className={`${Style.subheaderContent} ${alignNavigationCenter && Style.alignNavigationCenter}`}>
                  {navigationList?.map(item => {
                    return (
                      <div key={item.name} className={Style.buttonWrapper}>
                        <EFPillButton
                          text={item?.name}
                          colorTheme={item?.isActive ? "dark" : "transparent-grey"}
                          shadowTheme="none"
                          internalHref={item?.href}
                          fontWeightTheme="bold"
                          fontCaseTheme="upperCase"
                          onClick={item.onClick}
                        />
                      </div>
                    );
                  })}
                  {includeButtonsInScroll && buttons}
                </div>
              </HorizontalScroll>
            )}
          </div>
          {!includeButtonsInScroll && buttons}
        </div>
      </div>
    </div>
  );
};

EFSubHeader.defaultProps = {
  positionUnderNav: true
};

export default EFSubHeader;
