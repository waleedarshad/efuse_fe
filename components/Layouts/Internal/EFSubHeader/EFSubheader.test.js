import { mount, shallow } from "enzyme";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faGamepad } from "@fortawesome/pro-solid-svg-icons";
import React from "react";

import EFSubHeader from "./EFSubHeader";
import EFPillButton from "../../../Buttons/EFPillButton/EFPillButton";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";

describe("EFSubheader", () => {
  it("does not display FontAwesomeIcon if no header icon prop", () => {
    const subject = shallow(<EFSubHeader navigationList={[]} actionButtons={[]} />);

    expect(subject.find(FontAwesomeIcon)).toHaveLength(0);
  });

  it("displays FontAwesomeIcon if header icon prop", () => {
    const subject = shallow(<EFSubHeader headerIcon={faGamepad} navigationList={[]} actionButtons={[]} />);

    expect(subject.find(FontAwesomeIcon)).toHaveLength(1);
  });

  it("shows the correct navigation list if navigation list", () => {
    const navigationList = [
      {
        href: "my-href",
        name: "My Button",
        isActive: true,
        showOnPortfolio: true
      },
      {
        href: "another-href",
        name: "Another Button",
        isActive: false,
        showOnPortfolio: true
      }
    ];
    const subject = mount(<EFSubHeader navigationList={navigationList} actionButtons={[]} />);

    expect(subject.find(EFPillButton)).toHaveLength(2);

    expect(
      subject
        .find(EFPillButton)
        .at(0)
        .prop("text")
    ).toEqual("My Button");
    expect(
      subject
        .find(EFPillButton)
        .at(0)
        .prop("colorTheme")
    ).toEqual("dark");
    expect(
      subject
        .find(EFPillButton)
        .at(0)
        .prop("internalHref")
    ).toEqual("my-href");

    expect(
      subject
        .find(EFPillButton)
        .at(1)
        .prop("text")
    ).toEqual("Another Button");
    expect(
      subject
        .find(EFPillButton)
        .at(1)
        .prop("colorTheme")
    ).toEqual("transparent-grey");
    expect(
      subject
        .find(EFPillButton)
        .at(1)
        .prop("internalHref")
    ).toEqual("another-href");
  });

  it("does not render action buttons if array is empty", () => {
    const subject = mount(<EFSubHeader navigationList={[]} actionButtons={[]} />);

    expect(subject.find(".actionButtons")).toHaveLength(0);
  });

  it("renders buttons if present", () => {
    const buttons = [
      <EFRectangleButton
        key="a-cool-href"
        colorTheme="primary"
        fontWeightTheme="bold"
        shadowTheme="small"
        size="medium"
        text="an amazing button"
        internalHref="a-cool-href"
      />
    ];
    const subject = mount(<EFSubHeader navigationList={[]} actionButtons={buttons} />);

    expect(subject.find(".actionButtons")).toHaveLength(1);
    expect(subject.find(EFRectangleButton).prop("text")).toEqual("an amazing button");
    expect(subject.find(EFRectangleButton).prop("internalHref")).toEqual("a-cool-href");
  });
});
