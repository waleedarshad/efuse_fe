import PropTypes from "prop-types";

import Style from "./ActionButtonGroup.module.scss";

import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";

const ActionButtonGroup = props => (
  <div className={Style.wrapper}>
    {!props.removeCancelOption && (
      <EFRectangleButton
        buttonType="button"
        text="Cancel"
        width="autoWidth"
        disabled={props.disabledCancel ? !props.disabledCancel : props.disabled}
        colorTheme="light"
        onClick={props.onCancel}
        shadowTheme="small"
      />
    )}
    <EFRectangleButton
      buttonType="submit"
      text={props.overrideSave ? props.overrideSave : "Save"}
      width="autoWidth"
      disabled={props.disabled}
      colorTheme="secondary"
      shadowTheme="small"
    />
  </div>
);

ActionButtonGroup.propTypes = {
  onCancel: PropTypes.func
};

ActionButtonGroup.defaultProps = {
  onCancel: () => window.location.reload(true)
};

export default ActionButtonGroup;
