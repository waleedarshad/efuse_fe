import React from "react";
import { withRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
import {
  faBinoculars,
  faCogs,
  faComments,
  faCouch,
  faGamepadAlt,
  faNewspaper,
  faPowerOff,
  faUser,
  faUserShield
} from "@fortawesome/pro-regular-svg-icons";
import { faBuilding } from "@fortawesome/pro-light-svg-icons";
import FEATURE_FLAGS from "../../../../common/featureFlags";
import { OPPORTUNITIES_HREFS } from "../../../../common/opportunities";
import { activeUrl } from "../../../../helpers/HeaderHelper";
import useWindowDimensions from "../../../../hooks/useWindowDimensions";
import { logoutUser } from "../../../../store/actions/authActions";
import { resetUser } from "../../../../store/actions/common/userAuthActions";
import { toggleHeaderMenu } from "../../../../store/actions/headerActions";
import { getFlagForFeature } from "../../../../store/selectors/featureFlagSelectors";
import EFBadge from "../../../EFBadge/EFBadge";
import EFHeader from "../EFHeader/EFHeader";
import { faClippy } from "../../../../config/customIcons";

const HeaderWrapper = ({ router }) => {
  const dispatch = useDispatch();
  const authUser = useSelector(state => state.auth.currentUser);
  const unreadMessageCount = useSelector(state => state.messages.unreadMessageCount);
  const showMyOpportunities = useSelector(state => getFlagForFeature(state, FEATURE_FLAGS.ALLOW_OPPORTUNITY_CREATION));
  const showMyNewsArticles = useSelector(state =>
    getFlagForFeature(state, FEATURE_FLAGS.ALLOW_LEARNING_ARTICLE_CREATION)
  );
  const isClippyEnabled = useSelector(state => getFlagForFeature(state, FEATURE_FLAGS.CLIPPY_INTEGRATION));

  const flaggedLinks = [
    {
      title: "MY News Articles",
      href: "/news/owned",
      flag: showMyNewsArticles,
      icon: faNewspaper
    },
    {
      title: "MY Opportunities",
      href: OPPORTUNITIES_HREFS.OWNED,
      flag: showMyOpportunities,
      icon: faGamepadAlt
    }
  ];

  // set active url for header
  const { width } = useWindowDimensions();

  // logout function
  const logout = () => {
    analyticsTrack("logout");
    dispatch(logoutUser());
  };

  // ensure that user is reset
  const reEvaluateUser = () => {
    if (router.pathname !== "/settings") {
      analyticsTrack("settings");
      dispatch(resetUser());
      router.push("/settings");
    }
  };

  const analyticsTrack = type => {
    analytics.track("NAVIGATION_BUTTON_CLICKED", {
      type
    });
  };

  // build dropdown list for header
  let dropdownList = [];

  if (authUser?.roles?.includes("admin")) {
    dropdownList.push({
      title: "Admin",
      icon: faUserShield,
      href: "https://efuse.retool.com",
      onClick: () => {
        analyticsTrack("admin");
        dispatch(toggleHeaderMenu());
      }
    });
  }

  // if width is under 767
  if (width < 767) {
    dropdownList = [
      ...dropdownList,
      {
        title: "Lounge",
        icon: faCouch,
        href: "/lounge/featured",
        as: "/lounge/featured",
        onClick: () => {
          analyticsTrack("lounge");
          dispatch(toggleHeaderMenu());
        }
      },
      {
        title: "Discover",
        icon: faBinoculars,
        href: "/discover",
        as: "/discover",
        onClick: () => {
          analyticsTrack("discover");
          dispatch(toggleHeaderMenu());
        }
      },
      {
        title: "Opportunity",
        icon: faGamepadAlt,
        href: "/opportunities",
        as: "/opportunities",
        onClick: () => {
          analyticsTrack("opportunities");
          dispatch(toggleHeaderMenu());
        }
      }
    ];
  }

  // navigation that remains the same
  dropdownList = [
    ...dropdownList,
    {
      title: "My Portfolio",
      icon: faUser,
      href: "/u/[u]",
      as: `/u/${authUser?.username}`,
      onClick: () => {
        analyticsTrack("portfolio");
        dispatch(toggleHeaderMenu());
      }
    },
    {
      title: "MY Organizations",
      href: "/organizations/owned",
      icon: faBuilding,
      onClick: () => {
        dispatch(toggleHeaderMenu());
      }
    },
    {
      title: "clippy",
      href: "/clippy",
      icon: faClippy,
      onClick: () => {
        analyticsTrack("clippy");
        dispatch(toggleHeaderMenu());
      },
      flag: isClippyEnabled
    },
    {
      title: "Messages",
      icon: faComments,
      href: "/messages",
      onClick: () => {
        analyticsTrack("messages");
        dispatch(toggleHeaderMenu());
      },
      badge: unreadMessageCount > 0 && <EFBadge text={unreadMessageCount} />
    },
    {
      title: "Settings",
      icon: faCogs,
      href: "/settings",
      onClick: () => {
        reEvaluateUser();
        dispatch(toggleHeaderMenu());
      }
    },
    {
      title: "Logout",
      icon: faPowerOff,
      onClick: () => {
        logout();
        dispatch(toggleHeaderMenu());
      }
    }
  ];

  flaggedLinks.forEach(link => {
    if (link.flag) {
      dropdownList.splice(3, 0, {
        title: link.title,
        icon: link.icon,
        href: link.href,
        onClick: () => {
          dispatch(toggleHeaderMenu());
        }
      });
    }
  });

  return (
    <EFHeader
      fullName={authUser?.user}
      activeUrl={activeUrl(router)}
      dropdownList={dropdownList}
      currentUserId={authUser?._id}
      username={authUser?.username}
      profilePicture={authUser?.profilePicture?.url}
    />
  );
};

export default withRouter(HeaderWrapper);
