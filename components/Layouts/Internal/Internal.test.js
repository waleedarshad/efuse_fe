import React from "react";
import { MockedProvider } from "@apollo/client/testing";
import Internal from "./Internal";
import { mountWithStore } from "../../../common/testUtils";
import MetaHead from "../MetaHead";
import HeaderWrapper from "./HeaderWrapper/HeaderWrapper";
import GET_ERENA_FEATURED_EVENT from "../../../graphql/ERenaFeaturedEventQuery";

describe("Internal", () => {
  let state;

  beforeEach(() => {
    state = {
      webview: {
        status: false
      },
      auth: {
        isAuthenticated: true
      },
      errors: {
        errors: ""
      },
      messages: {
        unreadMessageCount: 0
      },
      features: {}
    };
  });

  const mocks = [
    {
      request: {
        query: GET_ERENA_FEATURED_EVENT
      },
      result: {
        data: {
          getFeaturedEvent: {}
        }
      }
    }
  ];

  it("renders a MetaHead if metaTitle", () => {
    const { subject } = mountWithStore(
      <MockedProvider mocks={mocks} addTypename={false}>
        <Internal metaTitle="My MetaTitle" />
      </MockedProvider>,
      state
    );

    expect(subject.find(MetaHead).prop("metaTitle")).toEqual("My MetaTitle");
  });

  it("renders default MetaHead if no metaTitle", () => {
    const { subject } = mountWithStore(
      <MockedProvider mocks={mocks} addTypename={false}>
        <Internal />
      </MockedProvider>,
      state
    );

    expect(subject.find(MetaHead).prop("metaTitle")).toEqual("eFuse");
  });

  it("renders a HeaderWrapper if no webview", () => {
    const { subject } = mountWithStore(
      <MockedProvider mocks={mocks} addTypename={false}>
        <Internal metaTitle="My MetaTitle" />
      </MockedProvider>,
      state
    );

    expect(subject.find(HeaderWrapper)).toHaveLength(1);
  });

  it("does not render a HeaderWrapper if webview", () => {
    const localState = {
      webview: {
        status: true
      },
      auth: {
        isAuthenticated: true
      },
      errors: {
        errors: ""
      },
      messages: {
        unreadMessageCount: 0
      },
      features: {}
    };

    const { subject } = mountWithStore(
      <MockedProvider mocks={mocks} addTypename={false}>
        <Internal metaTitle="My MetaTitle" />
      </MockedProvider>,
      localState
    );

    expect(subject.find(HeaderWrapper).exists()).toBe(false);
  });
});
