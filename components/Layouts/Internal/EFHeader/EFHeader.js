import React from "react";
import Link from "next/link";
import { useSelector, useDispatch } from "react-redux";
import { faBars } from "@fortawesome/pro-solid-svg-icons";
import { faBell } from "@fortawesome/pro-regular-svg-icons";
import dynamic from "next/dynamic";
import BellNotifications from "../../../BellNotifications/BellNotifications";
import EFAvatar from "../../../EFAvatar/EFAvatar";
import EFBomb from "../../../Icons/EFBomb/EFBomb";
import SearchUsers from "../../../SearchUsers/SearchUsers";
import EFCircleIconButton from "../../../Buttons/EFCircleIconButton/EFCircleIconButton";
import EFDropdownMenu from "./EFDropdownMenu/EFDropdownMenu";
import HeaderLinkList from "./HeaderLinkList/HeaderLinkList";
import Style from "./EFHeader.module.scss";
import { toggleHeaderMenu } from "../../../../store/actions/headerActions";
import EFCircleIconButtonTooltip from "../../../Buttons/EFCircleIconButtonTooltip/EFCircleIconButtonTooltip";

const EFCreateDropdownMenu = dynamic(() => import("./EFCreateDropdownMenu/EFCreateDropdownMenu"));

const EFHeader = ({ currentUserId, fullName, username, profilePicture, activeUrl, dropdownList }) => {
  const dispatch = useDispatch();
  const isMenuOpen = useSelector(state => state.header?.isMenuOpen);

  return (
    <div className={Style.navWrapper}>
      <div className={Style.navAlignment}>
        <Link href="/lounge/[type]" as="/lounge/featured">
          <div className={Style.iconWrapper}>
            <EFBomb size="medium" />
          </div>
        </Link>
        {currentUserId && (
          <div className={Style.searchWrapper}>
            <SearchUsers placeholder="Search eFuse" />
          </div>
        )}
        <div className={Style.navRightAlign}>
          <div className={Style.linksWrapper}>
            <HeaderLinkList activeUrl={activeUrl} />
          </div>
          {currentUserId && (
            <BellNotifications>
              <EFCircleIconButtonTooltip
                icon={faBell}
                fontColorTheme="light"
                colorTheme="transparent"
                shadowTheme="none"
                tooltipContent="Notifications"
                tooltipDelay={{ show: 400, hide: 200 }}
              />
            </BellNotifications>
          )}
          <div className={Style.navIconWrapper}>
            <EFAvatar
              profilePicture={profilePicture || "https://cdn.efuse.gg/uploads/static/global/mindblown_white.png"}
              size="extraSmall"
              displayOnlineButton={false}
              borderTheme="navColor"
              href="/u/[u]"
              as={`/u/${username}`}
            />
          </div>

          <EFCreateDropdownMenu />

          {isMenuOpen && (
            <EFDropdownMenu
              dropdownList={dropdownList}
              profilePicture={profilePicture}
              subtext={`@${username}`}
              title={fullName}
              onClick={() => dispatch(toggleHeaderMenu())}
              activeUrl={activeUrl}
              positionTheme="navigationDropdown"
              colorTheme="dark"
              layoutTheme="dropdown"
            />
          )}
          <EFCircleIconButton
            icon={faBars}
            onClick={() => dispatch(toggleHeaderMenu())}
            size="small"
            colorTheme="transparent"
            fontColorTheme="light"
            shadowTheme="none"
          />
        </div>
      </div>
    </div>
  );
};

export default EFHeader;
