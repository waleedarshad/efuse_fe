import React from "react";
import EFRectangleButtonTooltip from "../../../../Buttons/EFRectangleButtonTooltip/EFRectangleButtonTooltip";
import Style from "../EFHeader.module.scss";

const EFHeaderLink = ({ icon, onClick, tooltipContent, href, as, label }) => {
  return (
    <div className={`${Style.navIconWrapper} ${Style.removePadding}`}>
      <EFRectangleButtonTooltip
        buttonType="button"
        colorTheme="transparent"
        fontColorTheme="light"
        icon={icon}
        shadowTheme="none"
        size="small"
        fontWeightTheme="bold"
        text={label}
        tooltipText={tooltipContent}
        internalHref={href}
        internalAs={as}
        onClick={onClick}
        tooltipDelay={{ show: 400, hide: 200 }}
      />
    </div>
  );
};

export default EFHeaderLink;
