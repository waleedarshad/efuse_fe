import { useDispatch } from "react-redux";
import { faPlus, faCouch, faBinoculars, faGamepadAlt } from "@fortawesome/pro-solid-svg-icons";
import {
  faCouch as farCouch,
  faBinoculars as farBinoculars,
  faGamepadAlt as farGamepadAlt
} from "@fortawesome/pro-regular-svg-icons";
import { OPPORTUNITIES_HREFS } from "../../../../../common/opportunities";
import { toggleCreateDropdown } from "../../../../../store/actions/headerActions";
import EFHeaderLink from "../EFHeaderLink/EFHeaderLink";

const HeaderLinkList = ({ activeUrl }) => {
  const dispatch = useDispatch();
  const analyticsTrack = type => {
    analytics.track("NAVIGATION_BUTTON_CLICKED", {
      type
    });
  };
  return (
    <>
      <EFHeaderLink
        icon={faPlus}
        tooltipContent="Post, Create an Opportunity, or Build an Organization"
        label="Create"
        onClick={() => {
          dispatch(toggleCreateDropdown());
          analyticsTrack("create");
        }}
      />
      <EFHeaderLink
        icon={activeUrl === "/home" ? faCouch : farCouch}
        tooltipContent="Hangout with Friends, Build Your Community"
        label="Lounge"
        href="/lounge/[type]"
        as="/lounge/featured"
        onClick={() => analyticsTrack("lounge")}
      />
      <EFHeaderLink
        icon={activeUrl === "/discover" ? faBinoculars : farBinoculars}
        tooltipContent="Explore Organizations and eFuse Learning Content"
        label="Discover"
        href="/discover"
        as="/discover"
        onClick={() => analyticsTrack("discover")}
      />

      <EFHeaderLink
        icon={activeUrl === OPPORTUNITIES_HREFS.ALL ? faGamepadAlt : farGamepadAlt}
        tooltipContent="Find Events, Jobs, Team Openings, and Scholarships"
        label="Opportunity"
        href={OPPORTUNITIES_HREFS.ALL}
        as={OPPORTUNITIES_HREFS.ALL}
        onClick={() => analyticsTrack("opportunities")}
      />
    </>
  );
};

export default HeaderLinkList;
