import React from "react";
import "@testing-library/jest-dom";
import mockAxios from "axios";
import { waitFor } from "@testing-library/dom";
import userEvent from "@testing-library/user-event";
import { faCogs } from "@fortawesome/pro-regular-svg-icons";
import { render, screen } from "../../../../test-utils";
import EFHeader from "./EFHeader";

describe("EFHeader", () => {
  const dropDownOptions = [
    {
      title: "super cool option",
      icon: faCogs,
      href: "/doesntmatter",
      onClick: () => {}
    }
  ];

  beforeAll(() => {
    mockAxios.get.mockImplementation(url => {
      switch (url) {
        case "/notifications/count":
          return Promise.resolve({ data: { newNotifications: "98" } });
        case "/notifications/clear_count":
          return Promise.resolve({ data: {} });
        default:
          return Promise.reject(new Error("call not accounted for"));
      }
    });
  });

  afterEach(() => {
    mockAxios.get.mockClear();
  });

  it("renders the standard text button options and images", () => {
    render(<EFHeader />);

    expect(screen.getByText("Create")).toBeInTheDocument();
    expect(screen.getByText("Lounge")).toBeInTheDocument();
    expect(screen.getByText("Discover")).toBeInTheDocument();
    expect(screen.getByText("Opportunity")).toBeInTheDocument();

    expect(screen.getByAltText("eFuse logo")).toBeInTheDocument();
    expect(screen.getByAltText("Profile Image")).toBeInTheDocument();
  });

  it("makes the call to get notifications", async () => {
    render(<EFHeader currentUserId="user" />);

    await waitFor(() => {
      expect(mockAxios.get).toHaveBeenCalledTimes(1);
      expect(mockAxios.get).toHaveBeenCalledWith("/notifications/count");
      expect(screen.getByText("98")).toBeInTheDocument();
    });
  });

  it("toggles the menu to be open when clicked", async () => {
    render(<EFHeader dropdownList={dropDownOptions} currentUserId="userId" />);
    expect(screen.queryByText("super cool option")).toBeNull();

    userEvent.click(screen.getByTitle("bars"));

    await waitFor(() => {
      expect(screen.getByText("super cool option")).toBeInTheDocument();
    });
  });
});
