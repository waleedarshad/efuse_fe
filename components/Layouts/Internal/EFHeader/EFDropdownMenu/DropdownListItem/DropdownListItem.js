import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Link from "next/link";
import Style from "./DropdownListItem.module.scss";

const DropdownListItem = ({ itemKey, title, icon, onClick, href, as, badge, layoutTheme }) => {
  const itemContainer = (
    <div key={itemKey} className={`${Style[layoutTheme]}`} onClick={onClick}>
      <div className={Style.iconCircleWrapper}>
        <FontAwesomeIcon className={Style.icon} icon={icon} />
      </div>
      <span className={Style.itemTitle}>{title}</span>

      {badge && <div className={Style.badge}>{badge}</div>}
    </div>
  );
  if (href) {
    return (
      <Link href={href} as={as} key={itemKey}>
        {itemContainer}
      </Link>
    );
  }
  return itemContainer;
};

export default DropdownListItem;
