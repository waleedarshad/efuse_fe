import React from "react";
import { faTimes } from "@fortawesome/pro-solid-svg-icons";

import Style from "./DropdownMenuHeader.module.scss";
import EFCircleIconButton from "../../../../../Buttons/EFCircleIconButton/EFCircleIconButton";

const DropdownMenuHeader = ({ onClick, title, subtext, colorTheme }) => {
  return (
    <div className={Style.topContainer} data-text>
      <div className={Style.topContainerNames} data-name>
        <p className={`${Style.title} ${colorTheme === "dark" ? Style.light : Style.dark}`}>{title}</p>
        <p className={Style.subtext}>{subtext}</p>
      </div>
      <div className={Style.closeButtonWrapper}>
        <EFCircleIconButton
          icon={faTimes}
          fontColorTheme={colorTheme === "light" ? "" : "light"}
          colorTheme={colorTheme === "light" ? "light" : "transparent"}
          shadowTheme="small"
          size="small"
          onClick={onClick}
        />
      </div>
    </div>
  );
};

export default DropdownMenuHeader;
