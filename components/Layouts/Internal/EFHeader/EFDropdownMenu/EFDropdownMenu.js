import React from "react";
import uniqueId from "lodash/uniqueId";
import DropdownListItem from "./DropdownListItem/DropdownListItem";
import DropdownMenuHeader from "./DropdownMenuHeader/DropdownMenuHeader";
import Style from "./EFDropdownMenu.module.scss";

const EFDropdownMenu = ({ dropdownList, title, subtext, onClick, positionTheme, colorTheme, layoutTheme }) => {
  return (
    <div className={Style[positionTheme]}>
      <div className={`${Style[layoutTheme]} ${Style[colorTheme]}`}>
        <DropdownMenuHeader title={title} subtext={subtext} onClick={onClick} colorTheme={colorTheme} />
        <div className={Style.listWrapper}>
          {dropdownList?.map(item => {
            return (
              <DropdownListItem
                key={uniqueId()}
                itemKey={item?.href}
                title={item?.title}
                icon={item?.icon}
                onClick={item?.onClick}
                href={item?.href}
                as={item?.as}
                badge={item?.badge}
                layoutTheme={layoutTheme}
              />
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default EFDropdownMenu;
