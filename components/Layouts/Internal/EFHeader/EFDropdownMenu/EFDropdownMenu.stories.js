import React from "react";
import { faCouch, faBinoculars, faGamepadAlt, faPowerOff } from "@fortawesome/pro-regular-svg-icons";

import EFDropdownMenu from "./EFDropdownMenu";

export default {
  title: "Header/EFDropdownMenu",
  component: EFDropdownMenu,
  argTypes: {
    activeUrl: {
      dropdownList: {
        type: "select",
        options: ["/home", "/discover", "/opportunities"]
      },
      title: {
        control: {
          type: "text"
        }
      },
      subtext: {
        control: {
          type: "text"
        }
      },
      positionTheme: {
        control: {
          type: "select",
          options: "navigationDropdown"
        }
      }
    }
  }
};

const Story = args => <EFDropdownMenu {...args} />;

const dropdownList = [
  {
    title: "Lounge",
    icon: faCouch,
    href: "/lounge",
    as: "/lounge"
  },
  {
    title: "Discover",
    icon: faBinoculars,
    href: "/discover",
    as: "/discover"
  },
  {
    title: "Opportunity",
    icon: faGamepadAlt,
    href: "/opportunities",
    as: "/opportunities"
  },
  {
    title: "Logout",
    icon: faPowerOff,
    onClick: () => console.log("Clicked!")
  }
];

export const Basic = Story.bind({});
Basic.args = {
  dropdownList: dropdownList,
  title: "Austin May",
  subtext: "@amay"
};
