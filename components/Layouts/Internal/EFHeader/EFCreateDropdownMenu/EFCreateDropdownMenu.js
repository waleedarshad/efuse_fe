import { withRouter } from "next/router";
import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { faBinoculars, faSave, faSwords, faComments, faTicketAlt } from "@fortawesome/pro-regular-svg-icons";
import FEATURE_FLAGS from "../../../../../common/featureFlags";
import { toggleCreateDropdown } from "../../../../../store/actions/headerActions";
import { activeUrl } from "../../../../../helpers/HeaderHelper";
import { getFlagForFeature } from "../../../../../store/selectors/featureFlagSelectors";
import NewArticleModal from "../../../../EsportsNews/NewArticleModal/NewArticleModal";
import CreatePostModal from "../../../../Modals/CreatePostModal/CreatePostModal";
import EFPrimaryModal from "../../../../Modals/EFPrimaryModal/EFPrimaryModal";
import EFDropdownMenu from "../EFDropdownMenu/EFDropdownMenu";

const ArticleModal = ({ onClose }) => {
  return (
    <EFPrimaryModal
      isOpen
      onClose={onClose}
      displayCloseButton
      allowBackgroundClickClose={false}
      title="Create News Article"
    >
      <NewArticleModal onFormSubmitted={() => onClose()} />
    </EFPrimaryModal>
  );
};

const EFCreateDropdownMenu = ({ router }) => {
  const dispatch = useDispatch();
  // // set active url for header
  // const activeUrl = activeUrl(router);

  const enableArticle = useSelector(state => getFlagForFeature(state, FEATURE_FLAGS.ALLOW_LEARNING_ARTICLE_CREATION));
  const enableErenaEvent = useSelector(state => getFlagForFeature(state, FEATURE_FLAGS.ALLOW_ERENA_CREATION));
  const enableOpportunity = useSelector(state => getFlagForFeature(state, FEATURE_FLAGS.ALLOW_OPPORTUNITY_CREATION));
  const organizationCreateButtons = useSelector(state =>
    getFlagForFeature(state, FEATURE_FLAGS.ORGANIZATION_CREATE_BUTTONS)
  );

  const isCreateDropdownOpen = useSelector(state => state.header?.isCreateDropdownOpen);
  const [openCreatePostModal, setOpenCreatePostModal] = useState(false);
  const [isNewArticleOpen, toggleArticleMenu] = useState(false);

  const analyticsTrack = type => {
    analytics.track("NAVIGATION_CREATE_BUTTON_CLICKED", {
      type
    });
  };

  // build dropdown list for header
  let dropdownList = [];

  // navigation that remains the same
  dropdownList = [
    {
      title: "Post",
      icon: faComments,
      onClick: () => {
        setOpenCreatePostModal(true);
        analyticsTrack("createPost");
        dispatch(toggleCreateDropdown());
      }
    }
  ];

  if (enableErenaEvent) {
    dropdownList.push({
      title: "eRena Event",
      icon: faTicketAlt,
      href: "/erena/create",
      onClick: () => {
        analyticsTrack("eRenaEvent");
        dispatch(toggleCreateDropdown());
      }
    });
  }

  if (enableArticle) {
    dropdownList.push({
      title: "Article",
      icon: faSwords,
      onClick: () => {
        toggleArticleMenu(true);
        analyticsTrack("article");
        dispatch(toggleCreateDropdown());
      }
    });
  }

  if (enableOpportunity) {
    dropdownList.push({
      title: "Opportunity",
      icon: faSave,
      href: "/opportunities/create",
      onClick: () => {
        analyticsTrack("opportunity");
        dispatch(toggleCreateDropdown());
      }
    });
  }

  if (organizationCreateButtons) {
    dropdownList.push({
      title: "Organization",
      icon: faBinoculars,
      href: "/organizations/create",
      onClick: () => {
        analyticsTrack("organizations");
        dispatch(toggleCreateDropdown());
      }
    });
  }

  return (
    <>
      {isCreateDropdownOpen && (
        <EFDropdownMenu
          dropdownList={dropdownList}
          title="Create"
          onClick={() => dispatch(toggleCreateDropdown())}
          activeUrl={activeUrl(router)}
          positionTheme="postNavigationDropdown"
          colorTheme="light"
          layoutTheme="postDropdown"
        />
      )}
      {isNewArticleOpen && <ArticleModal onClose={() => toggleArticleMenu(false)} />}
      {openCreatePostModal && <CreatePostModal onClose={() => setOpenCreatePostModal(false)} />}
    </>
  );
};

export default withRouter(EFCreateDropdownMenu);
