import PropTypes from "prop-types";
import { Col, Container, Row } from "react-bootstrap";
import { useSelector } from "react-redux";
import React from "react";
import Error from "../../Error/Error";
import canAccess from "../../hoc/canAccess";
import MetaHead from "../MetaHead";
import HeaderWrapper from "./HeaderWrapper/HeaderWrapper";
import Style from "./Internal.module.scss";

const Internal = ({
  customWidth,
  maxWidth,
  metaTitle,
  fluid,
  children,
  containsSubheader,
  noPadding,
  containsSubheaderExact
}) => {
  const webview = useSelector(state => state.webview.status);

  return (
    <>
      {metaTitle && <MetaHead metaTitle={metaTitle} />}
      <div id={Style.internalWrapper}>
        {!webview && <HeaderWrapper />}
        <div
          className={`${Style.internalContentSubheader} ${containsSubheader && Style.addSubheaderMargin} ${fluid &&
            Style.fluid} ${webview && Style.webviewInternalContent} ${customWidth &&
            customWidth} ${containsSubheaderExact && Style.containsSubheaderExact}`}
        >
          <Container fluid={fluid} className={`${maxWidth && Style.maxWidth} ${noPadding && Style.noPadding}`}>
            <Row>
              <Col>
                <Error />
              </Col>
            </Row>
            <Row>{children}</Row>
          </Container>
        </div>
      </div>
    </>
  );
};

Internal.propTypes = {
  metaTitle: PropTypes.string,
  containsSubheader: PropTypes.bool
};

Internal.defaultProps = {
  metaTitle: "eFuse",
  containsSubheader: false
};

export default canAccess(Internal, "authenticated", Style.bodyStyle);
