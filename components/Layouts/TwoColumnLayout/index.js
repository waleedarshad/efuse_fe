import React from "react";
import { useSelector } from "react-redux";
import { Col } from "react-bootstrap";
import { useRouter } from "next/router";
import { faGamepadAlt } from "@fortawesome/pro-solid-svg-icons";

import Internal from "../Internal/Internal";
import BioCard from "../../BioCard/BioCard";
import PublicInternal from "../PublicInternal";
import EFSubHeader from "../Internal/EFSubHeader/EFSubHeader";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";
import FeatureFlag from "../../General/FeatureFlag/FeatureFlag";
import FeatureFlagVariant from "../../General/FeatureFlag/FeatureFlagVariant/FeatureFlagVariant";
import FEATURE_FLAGS from "../../../common/featureFlags";
import { getOpportunitiesNavigationList } from "../../../navigation/opportunities";
import OpportunitiesHeader from "../../OpportunitiesV2/OpportunitiesHeader/OpportunitiesHeader";

const TwoColumnLayout = ({ metaTitle, isWindowView, subHeader, leftChildren, children, noBioCard }) => {
  const router = useRouter();

  const isUserLoggedIn = useSelector(state => !!state.auth.currentUser?.id);

  if (!isUserLoggedIn) {
    return (
      <PublicInternal>
        <Col md={12} className="mb-4">
          {children}
        </Col>
      </PublicInternal>
    );
  }

  return (
    <Internal metaTitle={metaTitle} isWindowView={isWindowView} containsSubheader={subHeader}>
      {/* Temporary check to remove header from organization creation page */}
      {subHeader && (
        <FeatureFlag name={FEATURE_FLAGS.OPPORTUNITY_FILTER_MODAL}>
          <FeatureFlagVariant flagState>
            <OpportunitiesHeader isCompactToggleDisplayed={false} isFilterDisplayed={false} />
          </FeatureFlagVariant>
          <FeatureFlagVariant flagState={false}>
            <EFSubHeader
              headerIcon={faGamepadAlt}
              navigationList={getOpportunitiesNavigationList(router.pathname)}
              actionButtons={[
                // eslint-disable-next-line react/jsx-key
                <FeatureFlag name={FEATURE_FLAGS.ALLOW_OPPORTUNITY_CREATION}>
                  <FeatureFlagVariant flagState>
                    <EFRectangleButton
                      colorTheme="primary"
                      fontWeightTheme="bold"
                      shadowTheme="small"
                      size="medium"
                      text="Create Opportunity"
                      internalHref="/opportunities/create"
                    />
                  </FeatureFlagVariant>
                </FeatureFlag>
              ]}
            />
          </FeatureFlagVariant>
        </FeatureFlag>
      )}

      <Col md={4} lg={3}>
        <div className="stickySidebar">
          {!noBioCard && <BioCard />}
          {leftChildren}
        </div>
      </Col>
      <Col md={8} lg={9} className="no-gutters">
        {children}
      </Col>
    </Internal>
  );
};

export default TwoColumnLayout;
