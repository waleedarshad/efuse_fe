import {
  faBadgeCheck,
  faBells,
  faCogs,
  faCommentAlt,
  faExternalLinkSquareAlt,
  faGamepad,
  faShoppingCart,
  faTvMusic,
  faUserCircle,
  faUserShield,
  faUserSlash
} from "@fortawesome/pro-regular-svg-icons";
import { NavItem } from "../../../Navigation/SidebarNav/EFSidebarNavItem/NavItem";

import FEATURE_FLAGS from "../../../../common/featureFlags";

const navOptions: NavItem[] = [
  {
    name: "Account Settings",
    href: "/settings",
    icon: faCogs
  },
  {
    name: "External Accounts",
    href: "/settings/external_accounts",
    icon: faExternalLinkSquareAlt
  },
  {
    name: "Blocked Users",
    href: "/settings/blocked_users",
    icon: faUserSlash
  },
  {
    name: "Social Links",
    href: "/settings/social_links",
    icon: faUserCircle,
    featureFlag: FEATURE_FLAGS.SOCIAL_LINKS
  },
  {
    name: "Payments",
    href: "/settings/payments",
    icon: faShoppingCart,
    featureFlag: FEATURE_FLAGS.WEB_PAYMENT_SETTINGS_PAGE
  },
  {
    name: "Notifications",
    href: "/settings/notifications",
    icon: faBells
  },
  {
    name: "Badges",
    href: "/settings/badges",
    icon: faUserShield
  },
  {
    name: "Recruiting",
    href: "/settings/recruiting",
    icon: faGamepad,
    featureFlag: FEATURE_FLAGS.RECRUITING_SETTINGS_PAGE
  },
  { name: "Media Settings", href: "/settings/media", icon: faTvMusic },
  {
    name: "Feedback",
    href: "https://efuse-public.nolt.io",
    icon: faCommentAlt
  },
  {
    name: "eFuse Partner",
    href: "/settings/partner",
    icon: faBadgeCheck,
    featureFlag: FEATURE_FLAGS.FEATURE_BOTH_PARTNER_BUTTON
  }
];

export default navOptions;
