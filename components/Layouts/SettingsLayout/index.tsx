import PropTypes from "prop-types";
import { useSelector } from "react-redux";
import styled from "styled-components";

import menu from "./data/menu";
import { getFlagForFeature } from "../../../store/selectors/featureFlagSelectors";
import HeaderWrapper from "../Internal/HeaderWrapper/HeaderWrapper";
import EFSidebarContainer from "../../Navigation/SidebarNav/EFSidebarContainer/EFSidebarContainer";
import { NavSection } from "../../Navigation/SidebarNav/EFSidebarNavSection/NavSection";
import { mediaQueries, customBreakpoint } from "../../../styles/sharedStyledComponents/breakpoints";

const SettingsLayout = ({ customRightLayout, title, children }) => {
  const enabledMenuItems = menu.filter(menuItem =>
    menuItem.featureFlag ? useSelector(state => getFlagForFeature(state, menuItem.featureFlag)) : true
  );
  const navSections: NavSection[] = [
    {
      navItems: enabledMenuItems
    }
  ];
  return (
    <>
      <HeaderWrapper />
      <PageContainer>
        <SidebarWrapper>
          <NavTitle>Settings</NavTitle>
          <EFSidebarContainer navSections={navSections} />
        </SidebarWrapper>
        <MainContent>
          {!customRightLayout ? (
            <>
              <SettingsTitle>{title}</SettingsTitle>
              {children}
            </>
          ) : (
            children
          )}
        </MainContent>
      </PageContainer>
    </>
  );
};

const NavTitle = styled.div`
  font-family: Poppins, sans-serif;
  font-size: 24px;
  font-style: medium;
  font-weight: 500;
  line-height: 20px;
  letter-spacing: 0;
  text-align: left;
  margin-top: 10px;
  margin-bottom: 22px;
  padding-left: 20px;
`;

const PageContainer = styled.div`
  padding-top: 70px;
`;

const SettingsTitle = styled.h5`
  margin: 0 0 16px 0;
  padding: 0;
  font-family: Poppins, sans-serif;
  font-style: normal;
  font-weight: 500;
  font-size: 24px;
  line-height: 36px;
  color: #0a0c12;
`;

const SidebarWrapper = styled.div`
  height: 100%;
  position: fixed;

  ${mediaQueries("sm")`
    position: static;
    height: auto;
  `};
`;

const MainContent = styled.div`
  margin: 0px auto;
  max-width: 1140px;
  padding: 30px 0;

  ${customBreakpoint(1677)`
    margin: 0px 30px 0 270px;
  `};

  ${mediaQueries("sm")`
    margin: 20px 10px;
  `};
`;

SettingsLayout.propTypes = {
  title: PropTypes.string.isRequired,
  customRightLayout: PropTypes.bool,
  children: PropTypes.element
};

SettingsLayout.defaultProps = {
  customRightLayout: false,
  children: <></>
};

export default SettingsLayout;
