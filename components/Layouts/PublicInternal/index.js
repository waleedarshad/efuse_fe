import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import Error from "../../Error/Error";
import LandingNav from "../LandingNav/LandingNav";
import MetaHead from "../MetaHead";
import Style from "./index.module.scss";

const PublicInternal = ({ metaTitle, children, noTopMargin, maxWidth, noPadding }) => {
  return (
    <>
      <MetaHead metaTitle={metaTitle} />
      <div className={Style.internalWrapper}>
        <LandingNav preventScrollFade />
        <div className={`${Style.internalContent} ${noTopMargin && Style.noTopMargin}`}>
          <Container className={`${maxWidth && Style.maxWidth} ${noPadding && Style.noPadding}`}>
            <Row>
              <Col>
                <Error />
              </Col>
            </Row>
            <Row>{children}</Row>
          </Container>
        </div>
      </div>
    </>
  );
};

export default PublicInternal;
