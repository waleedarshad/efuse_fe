import React, { useState } from "react";
import { faCamera } from "@fortawesome/pro-solid-svg-icons";

import MetaHead from "../MetaHead";
import LandingNav from "../LandingNav/LandingNav";
import InternalStyle from "../Internal/Internal.module.scss";
import Style from "./PortfolioLayout.module.scss";
import HeaderWrapper from "../Internal/HeaderWrapper/HeaderWrapper";
import EFIsLoggedIn from "../../EFIsLoggedIn/EFIsLoggedIn";
import EFCircleIconButtonTooltip from "../../Buttons/EFCircleIconButtonTooltip/EFCircleIconButtonTooltip";
import EditImageModal from "../../Portfolio/EditImageModal/EditImageModal";

const PortfolioLayout = ({ headerImage, webview, children, isOwner }) => {
  const [isModalOpen, setIsModalOpen] = useState(false);

  const closeModal = () => {
    setIsModalOpen(false);
  };

  return (
    <div className={Style.mainContainer}>
      <MetaHead />
      <div id={InternalStyle.internalWrapper} className={`${Style.organizationWrapper}`}>
        {!webview && (
          <>
            <EFIsLoggedIn altComponent={<LandingNav preventScrollFade />}>
              <HeaderWrapper />
            </EFIsLoggedIn>
            <div
              className={Style.profileBackgroundImage}
              style={{
                backgroundImage: `url(${headerImage})`
              }}
            />
            {isOwner && (
              <>
                <div className={Style.editButton}>
                  <EFCircleIconButtonTooltip
                    tooltipContent="Edit Header Image"
                    icon={faCamera}
                    onClick={() => setIsModalOpen(true)}
                  />
                </div>

                <EditImageModal
                  title="Update Header Picture"
                  imageUrl={headerImage}
                  isOpen={isModalOpen}
                  closeModal={closeModal}
                  imageProp="headerImage"
                  aspectRatioWidth={180}
                  aspectRatioHeight={50}
                />
              </>
            )}
            <div className="container pb-3">{children}</div>
          </>
        )}
      </div>
    </div>
  );
};
export default PortfolioLayout;
