import { ListGroup, ListGroupItem } from "react-bootstrap";
import Link from "next/link";
import { withRouter } from "next/router";
import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faHome,
  faAddressCard,
  faStream,
  faList,
  faUser,
  faUsers,
  faUserFriends,
  faClipboardList
} from "@fortawesome/pro-solid-svg-icons";
import Style from "../../../List/List.module.scss";
import LinkStyle from "./Style.module.scss";
import { activeLink } from "../../../../helpers/GeneralHelper";
import SignupModal from "../../../SignupModal/SignupModal";

const OrganizationLinks = ({ margin, owner, id, router, orgType, organization_feed, currentUser, organization }) => {
  const dashboardPath = "/organizations/dashboard";
  const aboutPath = "/organizations/show";
  const timelinePath = "/organizations/timeline";
  const membersPath = "/organizations/organization_members";
  const requestPath = "/organizations/organization_requests";
  const followerPath = "/organizations/followers";
  const opportunitiesPath = "/organizations/opportunities";
  return (
    <ListGroup className={`${margin && "mt-4"} ${LinkStyle.listGroup}`}>
      {owner && (
        <ListGroupItem className={`${Style.listItem}`}>
          <Link href={`${dashboardPath}?id=${id}`}>
            <a className={`${Style.link} ${Style.orgLink} ${activeLink(router.pathname, dashboardPath, Style)}`}>
              <span className={LinkStyle.iconContainer}>
                <FontAwesomeIcon icon={faHome} />
              </span>
              <span className="ml-3">Dashboard</span>
            </a>
          </Link>
        </ListGroupItem>
      )}
      <ListGroupItem className={`${Style.listItem}`}>
        <Link href={`${aboutPath}?id=${id}`}>
          <a className={`${Style.link} ${Style.orgLink} ${activeLink(router.pathname, aboutPath, Style)}`}>
            <span className={LinkStyle.iconContainer}>
              <FontAwesomeIcon icon={faAddressCard} />
            </span>
            <span className="ml-3">About</span>
          </a>
        </Link>
      </ListGroupItem>
      {organization_feed &&
        (currentUser?.id ? (
          <ListGroupItem className={`${Style.listItem}`}>
            <Link href={`${timelinePath}?id=${id}`}>
              <a className={`${Style.link} ${Style.orgLink} ${activeLink(router.pathname, timelinePath, Style)}`}>
                <span className={LinkStyle.iconContainer}>
                  <FontAwesomeIcon icon={faStream} />
                </span>
                <span className="ml-3">Feed</span>
              </a>
            </Link>
          </ListGroupItem>
        ) : (
          <ListGroupItem className={`${Style.listItem}`}>
            <SignupModal
              title={`Create an account to view ${organization?.name}'s feed.`}
              message={`Never miss an update from ${organization?.name}. Sign up today!`}
            >
              <button className={`p-0 ${LinkStyle.transparentBtn}`} type="button">
                <a className={`${Style.link} ${Style.orgLink} ${activeLink(router.pathname, timelinePath, Style)}`}>
                  <span className={LinkStyle.iconContainer}>
                    <FontAwesomeIcon icon={faStream} />
                  </span>
                  <span className="ml-3">Feed</span>
                </a>
              </button>
            </SignupModal>
          </ListGroupItem>
        ))}
      {orgType !== "Closed" && (
        <>
          {currentUser?.id ? (
            <ListGroupItem className={`${Style.listItem}`}>
              <Link href={`${membersPath}?id=${id}`}>
                <a className={`${Style.link} ${Style.orgLink} ${activeLink(router.pathname, membersPath, Style)}`}>
                  <span className={LinkStyle.iconContainer}>
                    <FontAwesomeIcon icon={faUsers} />
                  </span>
                  <span className="ml-3">Members</span>
                </a>
              </Link>
            </ListGroupItem>
          ) : (
            <ListGroupItem className={`${Style.listItem}`}>
              <SignupModal
                title={`Create an account to view ${organization?.name}'s members.`}
                message={`Never miss an update from ${organization?.name}. Sign up today!`}
              >
                <button className={`p-0 ${LinkStyle.transparentBtn}`} type="button">
                  <a className={`${Style.link} ${Style.orgLink} ${activeLink(router.pathname, membersPath, Style)}`}>
                    <span className={LinkStyle.iconContainer}>
                      <FontAwesomeIcon icon={faUser} />
                    </span>
                    <span className="ml-3">Members</span>
                  </a>
                </button>
              </SignupModal>
            </ListGroupItem>
          )}

          {owner &&
            orgType !== "Invite" &&
            (currentUser?.id ? (
              <ListGroupItem className={`${Style.listItem}`}>
                <Link href={`${requestPath}?id=${id}`}>
                  <a className={`${Style.link} ${Style.orgLink} ${activeLink(router.pathname, requestPath, Style)}`}>
                    <span className={LinkStyle.iconContainer}>
                      <FontAwesomeIcon icon={faList} />
                    </span>
                    <span className="ml-3">Requests</span>
                  </a>
                </Link>
              </ListGroupItem>
            ) : (
              <ListGroupItem className={`${Style.listItem}`}>
                <SignupModal
                  title={`Create an account to view ${organization?.name}'s requests.`}
                  message={`Never miss an update from ${organization?.name}. Sign up today!`}
                >
                  <Link href="">
                    <a className={`${Style.link} ${Style.orgLink} ${activeLink(router.pathname, requestPath, Style)}`}>
                      <span className={LinkStyle.iconContainer}>
                        <FontAwesomeIcon icon={faList} />
                      </span>
                      <span className="ml-3">Requests</span>
                    </a>
                  </Link>
                </SignupModal>
              </ListGroupItem>
            ))}
        </>
      )}

      {currentUser?.id ? (
        <ListGroupItem className={`${Style.listItem}`}>
          <Link href={`${followerPath}?id=${id}`}>
            <a className={`${Style.link} ${Style.orgLink} ${activeLink(router.pathname, followerPath, Style)}`}>
              <span className={LinkStyle.iconContainer}>
                <FontAwesomeIcon icon={faUserFriends} />
              </span>
              <span className="ml-3">Followers</span>
            </a>
          </Link>
        </ListGroupItem>
      ) : (
        <ListGroupItem className={`${Style.listItem}`}>
          <SignupModal
            title={`Create an account to view ${organization?.name}'s followers.`}
            message={`Never miss an update from ${organization?.name}. Sign up today!`}
          >
            <button className={`p-0 ${LinkStyle.transparentBtn}`} type="button">
              <a className={`${Style.link} ${Style.orgLink} ${activeLink(router.pathname, followerPath, Style)}`}>
                <span className={LinkStyle.iconContainer}>
                  <FontAwesomeIcon icon={faUserFriends} />
                </span>
                <span className="ml-3">Followers</span>
              </a>
            </button>
          </SignupModal>
        </ListGroupItem>
      )}

      {currentUser?.id ? (
        <ListGroupItem className={`${Style.listItem}`}>
          <Link href={`${opportunitiesPath}?id=${id}`}>
            <a className={`${Style.link} ${Style.orgLink} ${activeLink(router.pathname, opportunitiesPath, Style)}`}>
              <span className={LinkStyle.iconContainer}>
                <FontAwesomeIcon icon={faClipboardList} />
              </span>
              <span className="ml-3">Opportunities</span>
            </a>
          </Link>
        </ListGroupItem>
      ) : (
        <ListGroupItem className={`${Style.listItem}`}>
          <SignupModal
            title={`Create an account to view ${organization?.name}'s opportunities.`}
            message={`Never miss an opportunity from ${organization?.name}. Sign up today!`}
          >
            <button className={`${LinkStyle.transparentBtn} p-0`} type="button">
              <a className={`${Style.link} ${Style.orgLink} ${activeLink(router.pathname, opportunitiesPath, Style)}`}>
                <span className={LinkStyle.iconContainer}>
                  <FontAwesomeIcon icon={faClipboardList} />
                </span>
                <span className="ml-3">Opportunities</span>
              </a>
            </button>
          </SignupModal>
        </ListGroupItem>
      )}
    </ListGroup>
  );
};

OrganizationLinks.propTypes = {
  margin: PropTypes.bool
};

export default withRouter(OrganizationLinks);
