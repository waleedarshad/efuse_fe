import React, { useEffect } from "react";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
import OrganizationHeaderCard from "../../Organizations/OrganizationDetail/OrganizationHeaderCard/OrganizationHeaderCard";
import FollowOrganization from "../../Organizations/FollowOrganization/FollowOrganization";
import { getOwnerAndCaptain } from "../../../helpers/OrganizationHelper";
import { getImage } from "../../../helpers/GeneralHelper";
import {
  getOrganizationById,
  getOrganizationBySlug,
  getOrganizationFromSSR
} from "../../../store/actions/organizationActions";
import HeaderWrapper from "../Internal/HeaderWrapper/HeaderWrapper";
import LandingNav from "../LandingNav/LandingNav";
import EFSubHeader from "../Internal/EFSubHeader/EFSubHeader";
import MockExternalUser from "../../MockExternalUser/MockExternalUser";
import Style from "./OrganizationLayout.module.scss";
import InternalStyle from "../Internal/Internal.module.scss";
import EFIsLoggedIn from "../../EFIsLoggedIn/EFIsLoggedIn";
import getOrganizationNavigationList from "../../../navigation/organization";

const OrganizationLayout = props => {
  const router = useRouter();
  const { id, org } = router.query;

  const dispatch = useDispatch();

  const authUser = useSelector(state => state.auth.currentUser);
  const organization = useSelector(state => state.organizations.organization);
  const mockExternalOrganization = useSelector(state => state.organizations.mockExternalOrganization);
  const showOrganizationTeams = useSelector(state => state.features.organization_teams);

  const { orgIdFromInviteCode, organizationFromSSR } = props;

  const { owner, captain } = getOwnerAndCaptain(authUser, organization);

  let isOwner = owner;
  let isCaptain = captain;
  if (mockExternalOrganization) {
    isOwner = false;
    isCaptain = false;
  }

  useEffect(() => {
    // always set the data from SSR when available
    if (organizationFromSSR) {
      dispatch(getOrganizationFromSSR(organizationFromSSR));
    }
    // continue to fetching data again client side. SSR request is public and does not include data like invite codes
    if (orgIdFromInviteCode) {
      // `orgIdFromInviteCode` is an organization ID passed from the InviteWrapper Component
      // We do this because org id is not present in invite code URL query params. example: efuse.gg/i/J35NX
      dispatch(getOrganizationById(orgIdFromInviteCode));
    } else if (org) {
      // for url slug route. ex: efuse.gg/org/some-slug-here
      dispatch(getOrganizationBySlug(org));
    } else if (id) {
      // for supporting legacy url slug route. ex: efuse.gg/organizations/show?id=...
      dispatch(getOrganizationById(id));
    }
  }, [id, org]);

  const actionButtons = [
    // eslint-disable-next-line react/jsx-key
    <div className={Style.externalViewButton}>
      <MockExternalUser
        currentUser={authUser}
        id={organization?.associatedUser?._id}
        newPortfolio
        type="organization"
        owner={owner || captain}
      />
    </div>
  ];

  return (
    <div className={Style.mainContainer}>
      <div id={InternalStyle.internalWrapper}>
        <EFIsLoggedIn altComponent={<LandingNav preventScrollFade />}>
          <HeaderWrapper />
        </EFIsLoggedIn>
        <div
          className={Style.imgLax}
          style={{
            backgroundImage: `url(${getImage(organization?.headerImage, "organization")})`
          }}
        />
        <div className={`container ${Style.organizationContent}`}>
          <OrganizationHeaderCard
            organization={organization}
            currentUser={authUser}
            followButton={!owner && <FollowOrganization theme="skyBlueBtn" organization={organization} />}
            isOwner={isOwner}
            captain={isCaptain}
          />

          <EFSubHeader
            positionUnderNav={false}
            navigationList={getOrganizationNavigationList(
              router,
              organization,
              isOwner || isCaptain,
              showOrganizationTeams
            )}
            includeButtonsInScroll
            actionButtons={actionButtons}
          />
          {props.children}
        </div>
      </div>
    </div>
  );
};
export default OrganizationLayout;
