import { Card } from "react-bootstrap";
import React, { Component } from "react";
import Style from "./PortfolioCard.module.scss";

class PortfolioCard extends Component {
  render() {
    return (
      <Card className={`customCard mb-4 ${Style.cardContainer}`}>
        <div>
          <Card.Header className={Style.cardHeader}>{this.props.title}</Card.Header>
          <Card.Body className={Style.bodyWrapper}>{this.props.children}</Card.Body>
        </div>
      </Card>
    );
  }
}

export default PortfolioCard;
