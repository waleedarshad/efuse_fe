import React from "react";
import { connect } from "react-redux";
import { withCdn } from "../../../../common/utils";
import EFComingSoonOverlay from "../../../EFComingSoonOverlay/EFComingSoonOverlay";
import PortfolioCard from "../PortfolioCard";
import Style from "./Badges.module.scss";

const badges = [
  {
    icon: "/static/images/badges/10k_badge.png",
    title: "Early Adopter",
    desc: "Being among the first 10,000 to join eFuse"
  },
  {
    icon: "/static/images/badges/apply_opp_badge.png",
    title: "Build Your Future",
    desc: "Apply for an opportunity"
  },
  {
    icon: "/static/images/badges/community_badge.png",
    title: "Community Matters",
    desc: "Follow 20 users"
  },
  {
    icon: "/static/images/badges/content_creator_badge.png",
    title: "Content Creator",
    desc: "Link 2 of the following: YouTube, Twitch, Instagram, or Twitter to your eFuse Profile"
  },
  {
    icon: "/static/images/badges/og_badge.png",
    title: "OG Badge",
    desc: "Become a verified influencer on the eFuse platform"
  }
];

const Badges = ({ showBadges }) => {
  const content = badges.map((badge, i) => (
    // eslint-disable-next-line react/no-array-index-key
    <div key={i} className={Style.badgeContainer}>
      <img className={Style.badgeIcon} src={withCdn(badge.icon)} alt="badge icon" />
      <div>
        <h4 className={Style.badgeTitle}>{badge.title}</h4>
        <p className={Style.badgeDesc}>{badge.desc}</p>
      </div>
    </div>
  ));

  return (
    <>
      {showBadges && (
        <PortfolioCard title="efuse badges">
          <EFComingSoonOverlay>{content}</EFComingSoonOverlay>
        </PortfolioCard>
      )}
    </>
  );
};

const mapStateToProps = state => ({
  showBadges: state.features.portfolio_badges
});

export default connect(mapStateToProps)(Badges);
