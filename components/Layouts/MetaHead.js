import Head from "next/head";

const MetaHead = ({ metaTitle }) => {
  return (
    <Head>
      <meta name="theme-color" content="#132940" />
      <title>{metaTitle}</title>
    </Head>
  );
};

MetaHead.defaultProps = {
  metaTitle: "eFuse"
};

export default MetaHead;
