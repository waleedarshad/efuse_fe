import { useSelector } from "react-redux";
import Internal from "../Internal/Internal";
import PublicInternal from "../PublicInternal/index";

const AuthAwareLayout = ({ metaTitle, containsSubheader, children }) => {
  const currentUser = useSelector(state => state.auth.currentUser);

  const isUserLoggedIn = currentUser?.id;

  if (isUserLoggedIn) {
    return (
      <Internal metaTitle={metaTitle} containsSubheader={containsSubheader}>
        {children}
      </Internal>
    );
  }

  return <PublicInternal metaTitle={metaTitle}>{children}</PublicInternal>;
};

export default AuthAwareLayout;
