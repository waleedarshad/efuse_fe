import { MockedProvider } from "@apollo/client/testing";
import { mountWithStore } from "../../../common/testUtils";
import Internal from "../Internal/Internal";
import PublicInternal from "../PublicInternal/index";
import AuthAwareLayout from "./AuthAwareLayout";
import { GET_USER_BY_ID } from "../../../graphql/UserQuery";

describe("AuthAwareLayout", () => {
  const mocks = [
    {
      request: {
        query: GET_USER_BY_ID,
        variables: {
          id: "12345"
        }
      },
      result: {
        data: {
          getUserById: {}
        }
      }
    }
  ];
  it("uses internal layout when user is logged in", () => {
    const state = {
      auth: { currentUser: { id: "007" } },
      webview: {},
      errors: {},
      features: {},
      messages: {}
    };

    const { subject } = mountWithStore(
      <MockedProvider mocks={mocks} addTypename={false}>
        <AuthAwareLayout>notLoggedIn</AuthAwareLayout>
      </MockedProvider>,
      state
    );

    expect(subject.find(Internal)).toHaveLength(1);
    expect(subject.find(PublicInternal)).toHaveLength(0);
  });

  it("uses public layout when user is NOT logged in", () => {
    const state = {
      auth: {},
      errors: {},
      experiences: {},
      features: {}
    };

    const { subject } = mountWithStore(
      <MockedProvider mocks={mocks} addTypename={false}>
        <AuthAwareLayout>notLoggedIn</AuthAwareLayout>
      </MockedProvider>,
      state
    );

    expect(subject.find(Internal)).toHaveLength(0);
    expect(subject.find(PublicInternal)).toHaveLength(1);
  });
});
