import { Col } from "react-bootstrap";
import Internal from "../Internal/Internal";
import BioCard from "../../BioCard/BioCard";
import Style from "./ThreeColumnLayout.module.scss";

const ThreeColumnLayout = ({ metaTitle, isWindowView, leftChildren, setThirdToTopMobile, children, rightChildren }) => (
  <Internal metaTitle={metaTitle} isWindowView={isWindowView}>
    <Col md={3} className={Style.removePadding}>
      <BioCard hideOnMobile />
      {leftChildren}
    </Col>
    <Col md={6} className={`mb-4 ${setThirdToTopMobile && "order-2"} order-md-1`}>
      {children}
    </Col>
    <Col md={3} className={`${Style.removePadding} ${setThirdToTopMobile && "order-1"} order-md-2`}>
      <div className="stickySidebar mb-3">{rightChildren}</div>
    </Col>
  </Internal>
);

export default ThreeColumnLayout;
