import Link from "next/link";
import React, { useState, useEffect } from "react";
import Style from "./LandingNav.module.scss";
import DynamicModal from "../../DynamicModal/DynamicModal";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";
import FEATURE_FLAGS from "../../../common/featureFlags";
import FeatureFlag from "../../General/FeatureFlag/FeatureFlag";
import FeatureFlagVariant from "../../General/FeatureFlag/FeatureFlagVariant/FeatureFlagVariant";
import useDeviceDetect from "../../../hooks/useDeviceDetect";
import { LINKS, getOneLink } from "../../../helpers/AppsFlyerHelper";

const LandingNav = ({ preventScrollFade }) => {
  const [isTop, setIsTop] = useState(true);
  const { isMobile } = useDeviceDetect();

  useEffect(() => {
    document.addEventListener("scroll", scroll);

    return () => {
      document.removeEventListener("scroll", scroll);
    };
  });

  const scroll = () => {
    if (window.scrollY <= 0) {
      setIsTop(true);
    } else {
      setIsTop(false);
    }
  };

  const loginSignupButtons = (
    <>
      <DynamicModal
        flow="LoginSignup"
        openOnLoad={false}
        startView="login"
        displayCloseButton
        allowBackgroundClickClose={false}
      >
        <EFRectangleButton
          buttonType="button"
          colorTheme="primary"
          fontWeightTheme="bold"
          shadowTheme="medium"
          size="large"
          text="LOG IN"
          width="fullWidth"
          onClick={() => {
            analytics.track("NAVIGATION_BUTTON_CLICKED", {
              type: "login"
            });
          }}
        />
      </DynamicModal>
      <DynamicModal
        flow="LoginSignup"
        openOnLoad={false}
        startView="signup"
        customRedirect="/welcome"
        displayCloseButton
        allowBackgroundClickClose={false}
      >
        <EFRectangleButton
          buttonType="button"
          colorTheme="secondary"
          fontWeightTheme="bold"
          shadowTheme="medium"
          size="large"
          text="SIGN UP"
          width="fullWidth"
          onClick={() => {
            analytics.track("NAVIGATION_BUTTON_CLICKED", {
              type: "signup"
            });
          }}
        />
      </DynamicModal>
    </>
  );

  return (
    <div className={`${Style.header} ${!isTop && Style.notTop}  ${preventScrollFade && Style.notTop}`}>
      <div>
        <Link href="/">
          <img
            className={Style.efuse}
            alt="efuse logo"
            src="https://cdn.efuse.gg/uploads/static/global/efuseLogo.png"
          />
        </Link>
        <Link href="/">
          <img
            className={Style.efuseName}
            alt="efuse watermark"
            src="https://cdn.efuse.gg/uploads/static/global/efuseName.png"
          />
        </Link>
      </div>
      <div className={Style.topButtonContainer}>
        {isMobile ? (
          <FeatureFlag name={FEATURE_FLAGS.EXP_APP_DOWNLOAD_LANDING_PAGE} experiment>
            <FeatureFlagVariant flagState>
              <EFRectangleButton
                buttonType="button"
                colorTheme="secondary"
                fontWeightTheme="bold"
                shadowTheme="medium"
                size="large"
                onClick={() => {
                  analytics.track("APP_DOWNLOAD_LINK_CLICKED", { location: "Top Navigation" }, () => {
                    window.location.href = getOneLink(LINKS.DOWNLOAD_APP);
                  });
                }}
                text="DOWNLOAD APP"
                width="fullWidth"
              />
            </FeatureFlagVariant>
            <FeatureFlagVariant flagState={false}>{loginSignupButtons}</FeatureFlagVariant>
          </FeatureFlag>
        ) : (
          loginSignupButtons
        )}
      </div>
    </div>
  );
};

export default LandingNav;
