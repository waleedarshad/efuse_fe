import React, { useState, useEffect } from "react";
import { useDropzone } from "react-dropzone";
import Style from "./ImageUploader.module.scss";
import ImageCropper from "../ImageCropper/ImageCropper";
import FileUploadButton from "./FileUploadButton/FileUploadButton";
import { sendNotification } from "../../helpers/FlashHelper";
import Preview from "./Preview/Preview";
import ShowImageRatioAndSize from "../ShowImageRatioAndSize/ShowImageRatioAndSize";
import { UploadTypes } from "../DirectUpload/DirectUpload";

const ImageUploader = ({
  text,
  onDrop,
  onRemoveImage,
  allowCross,
  name,
  value,
  theme,
  onCropChange,
  toggleCropperModal,
  aspectRatioWidth,
  aspectRatioHeight,
  showCropper,
  circlePreview,
  fullWidthPreview,
  dialogButtonOverlay,
  dialogButtonRightAlign,
  showImageDescription
}) => {
  const acceptedFileType = UploadTypes.image;
  const defaultFiles = value ? [{ preview: value, path: value }] : [];
  const [files, setFiles] = useState(defaultFiles);
  const [croppedFiles, setCroppedFiles] = useState([]);
  const placeholder = text;
  const { getRootProps, getInputProps, open } = useDropzone({
    multiple: false,
    accept: acceptedFileType,
    noDrag: true,
    onDrop: acceptedFiles => {
      setFiles(
        acceptedFiles.map(file => {
          if (showCropper) {
            toggleCropperModal(true, name);
          }
          setCroppedFiles([]);
          return Object.assign(file, {
            preview: URL.createObjectURL(file)
          });
        })
      );
      onDrop(acceptedFiles, name);
    },
    onDropRejected: () => {
      sendNotification("Please select an image file only", "danger", "File");
    }
  });

  const removeImage = () => {
    setFiles([]);
    setCroppedFiles([]);
    onRemoveImage();
  };
  useEffect(
    () => () => {
      files.forEach(file => URL.revokeObjectURL(file.preview));
    },
    [files]
  );

  const rootProps = getRootProps({
    onClick: event => event.stopPropagation(),
    onKeyDown: event => {
      if (event.keyCode === 32 || event.keyCode === 13) {
        event.stopPropagation();
      }
    }
  });

  const filesToBeReturned = files.length === 0 ? defaultFiles : files;
  const isBrowsedFile = filesToBeReturned[0] && filesToBeReturned[0].preview.includes("blob:");

  const uploadButton = (
    <FileUploadButton
      open={open}
      theme={theme}
      dialogButtonOverlay={dialogButtonOverlay}
      dialogButtonRightAlign={dialogButtonRightAlign}
    />
  );

  return (
    /* eslint-disable react/jsx-props-no-spreading */
    <div {...rootProps} className={`${Style.inputFile} ${Style[theme]}`}>
      <input {...getInputProps()} />
      {showCropper && isBrowsedFile && (
        <ImageCropper
          files={filesToBeReturned}
          onCropChange={onCropChange}
          name={name}
          toggleCropperModal={toggleCropperModal}
          setCroppedFiles={setCroppedFiles}
          aspectRatioWidth={aspectRatioWidth}
          aspectRatioHeight={aspectRatioHeight}
        />
      )}
      <div className={Style.imageUploaderWrapper}>
        <Preview
          placeholder={placeholder}
          files={croppedFiles && croppedFiles.length > 0 ? croppedFiles : filesToBeReturned}
          removeImage={removeImage}
          showCross={files.length > 0 || value}
          allowCross={allowCross}
          circlePreview={circlePreview}
          fullWidthPreview={fullWidthPreview}
          theme={theme}
        />

        {showImageDescription ? (
          <div className={Style.uploadButtonContainer}>
            {uploadButton}
            <ShowImageRatioAndSize width={aspectRatioWidth} height={aspectRatioHeight} />
          </div>
        ) : (
          uploadButton
        )}
      </div>
    </div>
    /* eslint-enable react/jsx-props-no-spreading */
  );
};

export default ImageUploader;
