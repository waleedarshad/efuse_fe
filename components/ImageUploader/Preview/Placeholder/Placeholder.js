import React from "react";
import Style from "./Placeholder.module.scss";

const Placeholder = ({ placeholder }) => <p className={Style.placeholder}>{placeholder}</p>;

export default Placeholder;
