import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/pro-solid-svg-icons";
import Style from "./Preview.module.scss";
import Placeholder from "./Placeholder/Placeholder";

const Preview = ({ files, placeholder, removeImage, showCross, allowCross, circlePreview, fullWidthPreview, theme }) =>
  files.length > 0 ? (
    files.map((file, index) => (
      <div className={Style.previewBox} key={index}>
        <div
          className={`${Style.thumbnailWrapper} ${circlePreview && Style.circleThumbnail} ${fullWidthPreview &&
            Style.fullWidthPreview}`}
        >
          <div className={Style.thumbnailFrame}>
            {showCross && allowCross && (
              <FontAwesomeIcon
                icon={faTimes}
                title="Remove Picture"
                className={Style.removeBtn}
                onClick={removeImage}
              />
            )}
            <img className={Style.thumbnail} src={file.preview} alt={`${file.path} - ${file.size} bytes`} />
          </div>
        </div>
      </div>
    ))
  ) : (
    <>{theme !== "lightButton" && <Placeholder placeholder={placeholder} />}</>
  );

export default Preview;
