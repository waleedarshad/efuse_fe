import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUpload } from "@fortawesome/pro-solid-svg-icons";
import Style from "./FileUploadButton.module.scss";
import EFButton from "../../Buttons/EFButton/EFButton";

const FileUploadButton = ({ open, theme, dialogButtonOverlay, dialogButtonRightAlign }) => (
  <EFButton
    className={`${Style.dialogButton} ${Style[theme]} ${dialogButtonOverlay &&
      Style.dialogButtonOverlay} ${dialogButtonRightAlign && Style.dialogButtonRightAlign}`}
    onClick={open}
    width="fullWidth"
  >
    Select a File
    {(theme === "lightButton" || theme === "noShadowButton") && (
      <FontAwesomeIcon icon={faUpload} className={Style.uploadIcon} />
    )}
  </EFButton>
);

export default FileUploadButton;
