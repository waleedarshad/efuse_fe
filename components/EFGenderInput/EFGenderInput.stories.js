import React from "react";
import EFGenderInput from "./EFGenderInput";

export default {
  title: "Inputs/EFGenderInput",
  component: EFGenderInput,
  argTypes: {
    disabled: {
      control: {
        type: "boolean"
      }
    },
    theme: {
      control: {
        type: "text"
      }
    },
    required: {
      control: {
        type: "boolean"
      }
    },
    errorMessage: {
      control: {
        type: "text"
      }
    }
  }
};

const Story = args => <EFGenderInput {...args} />;

export const Default = Story.bind({});

Default.args = {
  disabled: false,
  theme: "whiteShadow",
  required: true,
  errorMessage: "Content required"
};
