import React from "react";

import SelectBox from "../SelectBox/SelectBox";
import { formattedGender } from "../Settings/Recruiting/dropdownData";

const EFGenderInput = ({ disabled, theme, value, onChange, errorMessage }) => {
  return (
    <SelectBox
      disabled={disabled}
      name="gender"
      options={formattedGender}
      validated={false}
      theme={theme}
      value={value}
      onChange={onChange}
      errorMessage={errorMessage}
    />
  );
};

EFGenderInput.defaultProps = {
  disabled: false,
  theme: "whiteShadow"
};

export default EFGenderInput;
