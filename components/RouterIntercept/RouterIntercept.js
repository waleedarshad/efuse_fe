import React, { useEffect } from "react";

import Router from "next/router";

// use this component to intercept the nextjs link (or router)
const RouterIntercept = ({ children, mountedFunction }) => {
  useEffect(() => {
    Router.router.events.on("routeChangeStart", mountedFunction);
    return () => {
      Router.router.events.off("routeChangeStart", mountedFunction);
    };
  });
  return <>{children}</>;
};

export default RouterIntercept;
