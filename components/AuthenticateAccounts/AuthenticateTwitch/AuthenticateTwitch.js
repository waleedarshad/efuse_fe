import OauthButton from "../../Settings/ExternalAccounts/OauthButton/OauthButton";

import Style from "../AuthenticateAccounts.module.scss";
import authenticateAccount from "../../hoc/authenticateAccount";

const AuthenticateTwitch = ({ user, children, toggleAccount }) => {
  return (
    <>
      {children ? (
        <div onClick={() => toggleAccount("twitch", user.twitchVerified)}>{children}</div>
      ) : (
        <OauthButton
          onClick={() => toggleAccount("twitch", user.twitchVerified)}
          styleClass={Style.twitchAuth}
          img="twitch"
          isVerified={user.twitchVerified}
          iconClass={Style.unLink}
        />
      )}
    </>
  );
};

export default authenticateAccount(AuthenticateTwitch);
