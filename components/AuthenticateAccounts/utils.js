import EFAlert from "../EFAlert/EFAlert";

export const unlinkConfirmation = (service, onConfirm) => {
  const title = `Unlink ${service.toUpperCase()} Account?`;
  const message = "Are You Sure?";
  const confirmLabel = "Yes";
  const cancelLabel = "No";

  EFAlert(title, message, confirmLabel, cancelLabel, onConfirm);
};
