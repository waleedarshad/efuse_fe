import OauthButton from "../../Settings/ExternalAccounts/OauthButton/OauthButton";
import authenticateAccount from "../../hoc/authenticateAccount";
import Style from "../AuthenticateAccounts.module.scss";

const AuthenticateBattlenet = ({ user, toggleAccount, children }) => {
  return (
    <>
      {children ? (
        <div onClick={() => toggleAccount("bnet", user.battlenetVerified)}>{children}</div>
      ) : (
        <OauthButton
          onClick={() => toggleAccount("bnet", user.battlenetVerified)}
          styleClass={Style.battlenetBtn}
          img="battlenet"
          isVerified={user.battlenetVerified}
          iconClass={Style.unLink}
        />
      )}
    </>
  );
};

export default authenticateAccount(AuthenticateBattlenet);
