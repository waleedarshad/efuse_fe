import { Fragment, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { EXTERNAL_AUTH_SERVICES } from "../../../common/externalAuthServices";
import {
  deleteLinkForValorant,
  getValorantAuthStatusForLoggedUser
} from "../../../store/actions/externalAuth/externalAuthActions";
import FeatureFlag from "../../General/FeatureFlag/FeatureFlag";
import FeatureFlagVariant from "../../General/FeatureFlag/FeatureFlagVariant/FeatureFlagVariant";
import authenticateAccount from "../../hoc/authenticateAccount";
import OauthButton from "../../Settings/ExternalAccounts/OauthButton/OauthButton";
import Style from "../AuthenticateAccounts.module.scss";
import { unlinkConfirmation } from "../utils";

const AuthenticateValorant = ({ startOAuth, children }) => {
  const isUserVerifiedWithRiot = useSelector(state => state.auth.external.riot);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getValorantAuthStatusForLoggedUser());
  }, []);

  const onButtonClick = () => {
    if (isUserVerifiedWithRiot) {
      unlinkConfirmation(EXTERNAL_AUTH_SERVICES.RIOT, () => dispatch(deleteLinkForValorant()));
    } else {
      startOAuth(EXTERNAL_AUTH_SERVICES.RIOT);
    }
  };

  return (
    <FeatureFlag name="valorant_oauth_btn">
      <FeatureFlagVariant flagState>
        {children ? (
          <Fragment onClick={onButtonClick}>{children}</Fragment>
        ) : (
          <OauthButton
            onClick={onButtonClick}
            styleClass={Style.valorantAuth}
            img="valorant"
            isVerified={isUserVerifiedWithRiot}
            iconClass={Style.unLink}
          />
        )}
      </FeatureFlagVariant>
    </FeatureFlag>
  );
};

export default authenticateAccount(AuthenticateValorant);
