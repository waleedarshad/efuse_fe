import OauthButton from "../../Settings/ExternalAccounts/OauthButton/OauthButton";
import authenticateAccount from "../../hoc/authenticateAccount";
import Style from "../AuthenticateAccounts.module.scss";
import FeatureFlag from "../../General/FeatureFlag/FeatureFlag";
import FeatureFlagVariant from "../../General/FeatureFlag/FeatureFlagVariant/FeatureFlagVariant";

const AuthenticateYoutube = ({ user, toggleAccount, children }) => {
  return (
    <FeatureFlag name="youtube_oauth_btn">
      <FeatureFlagVariant flagState>
        {children ? (
          <div onClick={() => toggleAccount("google", user.googleVerified)}>{children}</div>
        ) : (
          <OauthButton
            onClick={() => toggleAccount("google", user.googleVerified)}
            styleClass={Style.blackButton}
            img="youTube"
            isVerified={user.googleVerified}
            iconClass={Style.unLink}
          />
        )}
      </FeatureFlagVariant>
    </FeatureFlag>
  );
};

export default authenticateAccount(AuthenticateYoutube);
