import OauthButton from "../../Settings/ExternalAccounts/OauthButton/OauthButton";
import authenticateAccount from "../../hoc/authenticateAccount";
import Style from "../AuthenticateAccounts.module.scss";

const AuthenticateSteam = ({ user, toggleAccount, children }) => {
  return (
    <>
      {children ? (
        <div onClick={() => toggleAccount("steam", user.steamVerified)}>{children}</div>
      ) : (
        <OauthButton
          onClick={() => toggleAccount("steam", user.steamVerified)}
          styleClass={Style.steamAuth}
          img="steam"
          isVerified={user.steamVerified}
          iconClass={Style.unLink}
        />
      )}
    </>
  );
};

export default authenticateAccount(AuthenticateSteam);
