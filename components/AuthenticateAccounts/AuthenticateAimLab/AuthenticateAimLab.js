import { Fragment, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { EXTERNAL_AUTH_SERVICES } from "../../../common/externalAuthServices";
import {
  deleteLinkForAimlab,
  getAimlabAuthStatusForLoggedUser
} from "../../../store/actions/externalAuth/externalAuthActions";
import FeatureFlag from "../../General/FeatureFlag/FeatureFlag";
import FeatureFlagVariant from "../../General/FeatureFlag/FeatureFlagVariant/FeatureFlagVariant";
import authenticateAccount from "../../hoc/authenticateAccount";
import OauthButton from "../../Settings/ExternalAccounts/OauthButton/OauthButton";
import Style from "../AuthenticateAccounts.module.scss";
import { unlinkConfirmation } from "../utils";

const AuthenticateAimLab = ({ startOAuth, children }) => {
  const isUserVerifiedWithStateSpace = useSelector(state => state.auth.external.statespace);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getAimlabAuthStatusForLoggedUser());
  }, []);

  const onButtonClick = () => {
    if (isUserVerifiedWithStateSpace) {
      unlinkConfirmation(EXTERNAL_AUTH_SERVICES.STATESPACE, () => dispatch(deleteLinkForAimlab()));
    } else {
      startOAuth(EXTERNAL_AUTH_SERVICES.STATESPACE);
    }
  };

  return (
    <FeatureFlag name="aimlab_oauth_btn">
      <FeatureFlagVariant flagState>
        {children ? (
          <Fragment onClick={onButtonClick}>{children}</Fragment>
        ) : (
          <OauthButton
            onClick={onButtonClick}
            styleClass={Style.blackButton}
            img="aimlab"
            isVerified={isUserVerifiedWithStateSpace}
            iconClass={Style.unLink}
          />
        )}
      </FeatureFlagVariant>
    </FeatureFlag>
  );
};

export default authenticateAccount(AuthenticateAimLab);
