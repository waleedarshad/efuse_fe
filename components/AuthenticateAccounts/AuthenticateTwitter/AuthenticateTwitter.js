import OauthButton from "../../Settings/ExternalAccounts/OauthButton/OauthButton";
import Style from "../AuthenticateAccounts.module.scss";
import authenticateAccount from "../../hoc/authenticateAccount";

const AuthenticateTwitter = ({ user, children, toggleAccount }) => {
  return (
    <>
      {children ? (
        <div onClick={() => toggleAccount("twitter", user.twitterVerified)}>{children}</div>
      ) : (
        <OauthButton
          onClick={() => toggleAccount("twitter", user.twitterVerified)}
          styleClass={Style.twitterBtn}
          img="twitter"
          isVerified={user.twitterVerified}
          iconClass={Style.checkMark}
        />
      )}
    </>
  );
};

export default authenticateAccount(AuthenticateTwitter);
