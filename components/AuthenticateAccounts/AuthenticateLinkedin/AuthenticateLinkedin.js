import OauthButton from "../../Settings/ExternalAccounts/OauthButton/OauthButton";
import authenticateAccount from "../../hoc/authenticateAccount";
import Style from "../AuthenticateAccounts.module.scss";

const AuthenticateLinkedin = ({ user, toggleAccount, children }) => {
  return (
    <>
      {children ? (
        <div onClick={() => toggleAccount("linkedin", user.linkedinVerified)}>{children}</div>
      ) : (
        <OauthButton
          onClick={() => toggleAccount("linkedin", user.linkedinVerified)}
          styleClass={Style.linkedinBtn}
          img="linkedIn"
          isVerified={user.linkedinVerified}
          iconClass={Style.unLink}
        />
      )}
    </>
  );
};

export default authenticateAccount(AuthenticateLinkedin);
