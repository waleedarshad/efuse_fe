import { connect } from "react-redux";
import OauthButton from "../../Settings/ExternalAccounts/OauthButton/OauthButton";
import Style from "../AuthenticateAccounts.module.scss";
import authenticateAccount from "../../hoc/authenticateAccount";

import { sendNotification } from "../../../helpers/FlashHelper";
import { facebookAuth } from "../../../store/actions/userActions";

const AuthenticateFacebook = ({ user, children, toggleAccount, facebookAuth, authNotification }) => {
  const loginFacebook = () => {
    window.FB.login(
      response => {
        responseFacebook(response.authResponse);
      },
      {
        scope: "public_profile, email, manage_pages, publish_pages, pages_show_list"
      }
    );
  };

  const responseFacebook = response => {
    if (response && response.accessToken) {
      facebookAuth(response.accessToken, response.userID, status => {
        authNotification(status, "facebook");
      });
    } else {
      sendNotification("Can not be verified, please try again.", "danger", "FACEBOOK");
    }
  };

  const performAction = () => {
    user.facebookVerified ? toggleAccount("facebook", user.facebookVerified) : loginFacebook();
  };

  return (
    <>
      {children ? (
        <div onClick={() => performAction()}>{children}</div>
      ) : (
        <OauthButton
          onClick={() => performAction()}
          styleClass={Style.facebookAuth}
          img="facebook"
          isVerified={user.facebookVerified}
          iconClass={Style.checkMark}
        />
      )}
    </>
  );
};

export default connect(null, { facebookAuth })(authenticateAccount(AuthenticateFacebook));
