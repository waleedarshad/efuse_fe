import OauthButton from "../../Settings/ExternalAccounts/OauthButton/OauthButton";

import Style from "../AuthenticateAccounts.module.scss";
import authenticateAccount from "../../hoc/authenticateAccount";

const AuthenticateDiscord = ({ user, children, toggleAccount }) => {
  return (
    <>
      {children ? (
        <div onClick={() => toggleAccount("discord", user.discordVerified)}>{children}</div>
      ) : (
        <OauthButton
          onClick={() => toggleAccount("discord", user.discordVerified)}
          styleClass={Style.discordAuth}
          img="discord"
          isVerified={user.discordVerified}
          iconClass={Style.unLink}
        />
      )}
    </>
  );
};

export default authenticateAccount(AuthenticateDiscord);
