import OauthButton from "../../Settings/ExternalAccounts/OauthButton/OauthButton";
import authenticateAccount from "../../hoc/authenticateAccount";
import Style from "../AuthenticateAccounts.module.scss";

const AuthenticateXboxLive = ({ user, toggleAccount, children }) => {
  return (
    <>
      {children ? (
        <div onClick={() => toggleAccount("xboxlive", user.xboxVerified)}>{children}</div>
      ) : (
        <OauthButton
          onClick={() => toggleAccount("xboxlive", user.xboxVerified)}
          styleClass={Style.xboxBtn}
          img="xboxLive"
          isVerified={user.xboxVerified}
          iconClass={Style.unLink}
        />
      )}
    </>
  );
};

export default authenticateAccount(AuthenticateXboxLive);
