import OauthButton from "../../Settings/ExternalAccounts/OauthButton/OauthButton";
import authenticateAccount from "../../hoc/authenticateAccount";
import Style from "../AuthenticateAccounts.module.scss";
import FeatureFlag from "../../General/FeatureFlag/FeatureFlag";
import FeatureFlagVariant from "../../General/FeatureFlag/FeatureFlagVariant/FeatureFlagVariant";

const AuthenticateSnapchat = ({ user, toggleAccount, children }) => {
  return (
    <FeatureFlag name="enable_snapchat_oauth_btn">
      <FeatureFlagVariant flagState={true}>
        {children ? (
          <div onClick={() => toggleAccount("snapchat", user.snapchatVerified)}>{children}</div>
        ) : (
          <OauthButton
            onClick={() => toggleAccount("snapchat", user.snapchatVerified)}
            styleClass={Style.snapchatAuth}
            img="snapchat"
            isVerified={user.snapchatVerified}
            iconClass={Style.unLink}
          />
        )}
      </FeatureFlagVariant>
    </FeatureFlag>
  );
};

export default authenticateAccount(AuthenticateSnapchat);
