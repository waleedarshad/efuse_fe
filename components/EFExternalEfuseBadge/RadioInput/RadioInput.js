import Style from "./RadioInput.module.scss";

const RadioInput = ({ title, onClick, checked }) => {
  return (
    <label className={Style.radioButtonLabel}>
      <input onChange={onClick} checked={checked} type="radio" />
      {title}
    </label>
  );
};

export default RadioInput;
