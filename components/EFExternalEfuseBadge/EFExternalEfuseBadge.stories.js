import React from "react";
import EFExternalEfuseBadge from "./EFExternalEfuseBadge";

export default {
  title: "Badges/EFExternalEfuseBadge",
  component: EFExternalEfuseBadge,
  argTypes: {
    id: {
      control: {
        type: "text"
      }
    },
    linkUrl: {
      control: {
        type: "text"
      }
    },
    imageUrl: {
      control: {
        type: "text"
      }
    },
    altText: {
      control: {
        type: "text"
      }
    }
  }
};

const Story = args => <EFExternalEfuseBadge {...args} />;

export const OrganizationBadge = Story.bind({});
OrganizationBadge.args = {
  id: "5f73271f51dc9525a873a43c",
  linkUrl: "http://efuse.gg/organizations/show?id=5f73271f51dc9525a873a43c",
  imageUrl: "https://cdn.efuse.gg/uploads/static/badges/efuse_verified_organization.png",
  badgeType: "organization",
  altText: "eFuse Organization Portfolio Badge"
};

export const PortfolioBadge = Story.bind({});
PortfolioBadge.args = {
  id: "5ecf54d2343c90783cffb06e",
  linkUrl: "https://efuse.gg/u/saud",
  imageUrl: "https://cdn.efuse.gg/uploads/static/badges/efuse_verified_portfolio.png",
  badgeType: "portfolio",
  altText: "eFuse Portfolio Badge"
};
