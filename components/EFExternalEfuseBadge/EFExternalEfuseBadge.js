import React, { useState } from "react";
import upperCase from "lodash/upperCase";

import RadioInput from "./RadioInput/RadioInput";
import Style from "./EFExternalEfuseBadge.module.scss";

const EFExternalEfuseBadge = ({ imageUrl, linkUrl, id, badgeType, altText }) => {
  const [size, setSize] = useState("225px");

  const sizeChange = newSize => {
    analytics.track(`${upperCase(badgeType)}_BADGE_SIZE_CHANGE`, { newSize });
    setSize(newSize);
  };
  return (
    <>
      <p>
        {badgeType === "organization"
          ? "Copy this HTML snippet below to add an official eFuse badge on your organization's website and blog. This badge will link back to your eFuse organization"
          : "Copy this html snippet below to add an official eFuse badge on your personal website or blog. This badge will link back to your eFuse portfolio."}
      </p>
      <p>HTML Snippet:</p>
      <RadioInput
        title="Small"
        onClick={() => {
          sizeChange("150px");
        }}
        checked={size === "150px"}
      />
      <RadioInput
        title="Medium"
        onClick={() => {
          sizeChange("225px");
        }}
        checked={size === "225px"}
      />
      <RadioInput
        title="Large"
        onClick={() => {
          sizeChange("300px");
        }}
        checked={size === "300px"}
      />
      <pre className={Style.htmlCodeSnippet}>
        {id
          ? `<a href="${linkUrl}" target="_new"><img style="width: ${size}; height: auto;" src="${imageUrl}" alt=${altText}></img></a>`
          : "loading snippet!"}
      </pre>
      <p>Result:</p>
      <img src={imageUrl} style={{ width: size }} />
    </>
  );
};

export default EFExternalEfuseBadge;
