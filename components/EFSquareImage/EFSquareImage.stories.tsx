import React from "react";
import EFSquareImage from "./EFSquareImage";

export default {
  title: "Shared/EFSquareImage",
  component: EFSquareImage
};

const Story = args => (
  <div style={{ width: "280px", margin: "3rem" }}>
    <EFSquareImage {...args} />
  </div>
);

export const Config = Story.bind({});

Config.args = {
  src: "https://i.ytimg.com/vi/0E44DClsX5Q/maxresdefault.jpg"
};
