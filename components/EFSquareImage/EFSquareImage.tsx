import React from "react";
import styled from "styled-components";
import EFImage from "../EFImage/EFImage";
import colors from "../../styles/sharedStyledComponents/colors";

interface Props {
  alt?: string;
  src: string;
  border?: boolean;
  size?: number;
  spaceRight?: boolean;
  className?: string;
}

const EFSquareImage = ({ alt, src, border = false, size = 20, spaceRight = false, className = "" }: Props) => {
  return (
    <Image
      alt={alt}
      src={src}
      height={size}
      width={size}
      border={border}
      borderRadius={size / 5}
      spaceRight={spaceRight}
      className={className}
    />
  );
};

const Image = styled(EFImage)`
  border-radius: ${props => Math.floor(props.borderRadius)}px;
  border: ${props => (props.border ? `1px solid ${colors.outerBorder};` : "none")};
  object-fit: cover;
  ${props => (props.spaceRight ? "margin-right: 10px" : "")}
`;

export default EFSquareImage;
