import { Container } from "react-bootstrap";

const OauthLayout = props => <Container className="mt-5">{props.children}</Container>;

export default OauthLayout;
