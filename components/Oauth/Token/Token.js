import { useState } from "react";
import { useRouter } from "next/router";
import { Row, Col } from "react-bootstrap";
import Cookies from "js-cookie";
import isEmpty from "lodash/isEmpty";

import LoginForm from "./LoginForm/LoginForm";
import { redirect } from "../../../helpers/OauthHelper";
import OauthLayout from "../OauthLayout/OauthLayout";
import MissingParameters from "../MissingParameters/MissingParameters";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";

const Token = () => {
  const [forceRender, setForceRender] = useState(false);
  let tokenObject = {};
  const tokenCookie = Cookies.get("efuse_temp_token");
  const router = useRouter();
  const { client_id, redirect_uri, scope } = router.query;
  if (tokenCookie) {
    tokenObject = JSON.parse(tokenCookie);
  }

  const handleContinue = () => redirect("authenticate_scope", client_id, scope, redirect_uri);

  const handleNewLogIn = () => {
    Cookies.remove("efuse_temp_token");
    setForceRender(true);
  };

  let pageContent = "";
  if (isEmpty(redirect_uri) || isEmpty(scope) || isEmpty(client_id)) {
    pageContent = <MissingParameters />;
  } else if (!isEmpty(tokenObject)) {
    pageContent = (
      <Row>
        <Col>
          <EFRectangleButton text={`Continue as ${tokenObject.email}`} onClick={handleContinue} />
        </Col>
        <Col>
          <EFRectangleButton text="Log in again" onClick={handleNewLogIn} />
        </Col>
      </Row>
    );
  } else {
    pageContent = <LoginForm client_id={client_id} redirect_uri={redirect_uri} scope={scope} />;
  }
  return <OauthLayout>{pageContent}</OauthLayout>;
};

export default Token;
