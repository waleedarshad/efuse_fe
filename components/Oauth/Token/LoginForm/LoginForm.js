import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Form, Row, FormControl } from "react-bootstrap";

import { login } from "../../../../store/actions/oauthActions";
import OauthError from "../../OauthError/OauthError";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";

const LoginForm = ({ client_id, scope, redirect_uri }) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [formValidated, setFormValidated] = useState(false);
  const disabled = useSelector(state => state.oauth.disabled);
  const dispatch = useDispatch();

  const handleEmailChange = event => {
    setEmail(event.target.value);
  };

  const handlePasswordChange = event => {
    setPassword(event.target.value);
  };

  const handleFormSubmit = event => {
    event.preventDefault();
    if (!formValidated) {
      setFormValidated(true);
    }
    if (event.target.checkValidity() && client_id && redirect_uri && scope) {
      dispatch(login({ username: email, password, clientId: client_id, redirect_uri }, scope));
    }
  };

  return (
    <>
      <OauthError />
      <Row>
        <Form onSubmit={handleFormSubmit} noValidate role="form" validated={formValidated}>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control
              size="lg"
              name="email"
              type="email"
              placeholder="Enter email"
              onChange={handleEmailChange}
              value={email}
              required
            />
            <FormControl.Feedback type="invalid">Email is required</FormControl.Feedback>
          </Form.Group>

          <Form.Group controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control
              size="lg"
              name="password"
              type="password"
              placeholder="Password"
              onChange={handlePasswordChange}
              value={password}
              required
            />
            <FormControl.Feedback type="invalid">Password is required</FormControl.Feedback>
          </Form.Group>
          <EFRectangleButton text="Submit" buttonType="submit" disabled={disabled} />
        </Form>
      </Row>
    </>
  );
};

export default LoginForm;
