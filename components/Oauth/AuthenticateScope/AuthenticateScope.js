import { useDispatch, useSelector } from "react-redux";
import { Row, Col } from "react-bootstrap";
import isEmpty from "lodash/isEmpty";

import scopesObject from "./scopes.json";
import { authorizeScope } from "../../../store/actions/oauthActions";
import OauthError from "../OauthError/OauthError.js";
import OauthLayout from "../OauthLayout/OauthLayout.js";
import MissingParameters from "../MissingParameters/MissingParameters.js";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";

const AuthenticateScope = props => {
  const dispatch = useDispatch();
  const disabled = useSelector(state => state.oauth.disabled);
  const { redirect_uri, scope, tokenObject, client_id } = props;
  const { clientName, email, token } = tokenObject;
  const missingParams = isEmpty(redirect_uri) || isEmpty(scope) || isEmpty(client_id);
  const scopes = scope.split(" ");
  const scopesContent = (
    <ul>
      {scopes.map((scope, scopeIndex) => {
        if (scopesObject[scope]) {
          return scopesObject[scope].map((scopeContent, index) => <li key={index}>{scopeContent}</li>);
        }
        return [<li key={scopeIndex}>You are not authorized to access this scope</li>];
      })}
    </ul>
  );

  const handleAuthorize = event => {
    event.preventDefault();
    dispatch(authorizeScope({ token, scope, redirect_uri }, client_id));
  };

  const handleCancel = () => {
    event.preventDefault();
    window.location = redirect_uri;
  };

  return (
    <OauthLayout>
      {missingParams ? (
        <MissingParameters />
      ) : (
        <>
          <OauthError />
          <Row>
            <Col>Client Name: {clientName}</Col>
            <Col>Signed in as: {email}</Col>
            <Col>Scopes {scopesContent}</Col>
          </Row>
          <Row>
            <Col>
              <EFRectangleButton text="Cancel" onClick={handleCancel} />
            </Col>
            <Col>
              <EFRectangleButton text="Authorize" onClick={handleAuthorize} disabled={disabled} />
            </Col>
          </Row>
        </>
      )}
    </OauthLayout>
  );
};

export default AuthenticateScope;
