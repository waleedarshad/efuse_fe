import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Alert, Row } from "react-bootstrap";

import { clearError } from "../../../store/actions/oauthActions";

const OauthError = () => {
  const timeout = null;
  const oauthError = useSelector(state => state.oauth.error);
  const dispatch = useDispatch();
  useEffect(() => {
    if (oauthError) {
      setTimeout(() => dispatch(clearError()), 5000);
    } else if (timeout) {
      clearTimeout(timeout);
    }
  }, [oauthError]);
  return (
    <>
      {oauthError && (
        <Row>
          <Alert variant="danger">
            <Alert.Heading>You got an error!</Alert.Heading>
            <p>{oauthError.message}</p>
          </Alert>
        </Row>
      )}
    </>
  );
};

export default OauthError;
