import { Row, Col } from "react-bootstrap";

const MissingParameters = () => (
  <Row>
    <Col>
      Make sure required params are added
      <ul>
        <li>client_id</li>
        <li>scope</li>
        <li>redirect_uri</li>
      </ul>
    </Col>
  </Row>
);

export default MissingParameters;
