import { useSelector, useDispatch } from "react-redux";
import Modal from "../Modal/Modal";
import { shareLinkModal } from "../../store/actions/shareAction";
import ShareLinkContent from "../ShareLinkContent/ShareLinkContent";

export const SHARE_LINK_KINDS = {
  ORGANIZATION: "organization",
  LEARNING_ARTICLE: "learning-article",
  OPPORTUNITY: "opportunity",
  BROADCAST_EVENT: "broadcast-event",
  PORTFOLIO: "portfolio",
  NEWS_ARTICLE: "news-article"
};

/**
 * ShareLinkModal presents a link with multiple buttons to share to different social platforms
 *
 * Note: This modal is instantiated in `_app.js` and should not need instantiated anywhere else.
 * To use the modal dispatch the `shareLinkModal` action
 */
const ShareLinkModal = () => {
  const dispatch = useDispatch();

  // Pull shareLinkInfo state from redux
  const { show, title, link, kind } = useSelector(state => state.share.shareLinkInfo);

  // Component Functions
  const hideDialog = () => {
    dispatch(shareLinkModal(false));
  };

  return (
    <Modal
      displayCloseButton
      openOnLoad={show}
      forceClose={!show}
      title={title}
      onCloseModal={hideDialog}
      component={<ShareLinkContent link={link} shareLinkKind={kind} />}
      highStackOrder="level1"
    />
  );
};

export default ShareLinkModal;
