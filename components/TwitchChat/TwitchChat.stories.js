import React from "react";
import TwitchChat from "./TwitchChat";

export default {
  title: "Twitch/TwitchChat",
  component: TwitchChat,
  argTypes: {
    channel: {
      control: {
        type: "select",
        options: ["nickmercs", "shroud", "ninja", "pokimane"]
      }
    },
    darkTheme: {
      control: {
        type: "boolean"
      }
    }
  }
};

const Story = args => <TwitchChat {...args} />;

export const Dark = Story.bind({});
Dark.args = {
  channel: "nickmercs",
  darkTheme: true
};

export const Light = Story.bind({});
Light.args = {
  channel: "nickmercs",
  darkTheme: false
};
