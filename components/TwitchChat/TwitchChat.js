import React from "react";

const TwitchChat = ({ channel, darkTheme }) => {
  return (
    <iframe
      title={`Twitch Chat ${channel}`}
      frameBorder="0"
      scrolling="no"
      id="chat_embed"
      src={`https://www.twitch.tv/embed/${channel}/chat?${
        darkTheme ? "darkpopout" : ""
      }&parent=localhost&parent=efuse.gg&parent=efuse.wtf`}
      height="100%"
      width="100%"
    />
  );
};

export default TwitchChat;
