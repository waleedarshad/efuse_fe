import React from "react";

import directUpload from "../hoc/directUpload";
import UploadDisplay from "../UploadDisplay/UploadDisplay";
import DirectUpload, { UploadTypes } from "../DirectUpload/DirectUpload";

const UploadVideoButton = props => {
  const {
    uploadProgress,
    uploadStarted,
    file,
    onUploadProgress,
    onUploadError,
    onUploadStart,
    handleFinishedUpload,
    cancelUpload,
    directory,
    placeholder,
    style,
    theme,
    dialogBtn
  } = props;
  return (
    <div>
      <>
        <UploadDisplay
          uploadProgress={uploadProgress}
          uploadStarted={uploadStarted}
          file={file}
          cancelUpload={cancelUpload}
        />
        <DirectUpload
          onProgress={onUploadProgress}
          onError={onUploadError}
          preprocess={onUploadStart}
          onFinish={handleFinishedUpload}
          directory={directory}
          placeholder={placeholder}
          style={style}
          theme={theme}
          dialogBtn={dialogBtn}
          accept={UploadTypes.video}
        />
      </>
    </div>
  );
};

UploadVideoButton.defaultProps = {
  directory: "uploads/media/",
  placeholder: "Photo/Video",
  style: {}
};

export default directUpload(UploadVideoButton);
