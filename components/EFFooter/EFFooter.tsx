import styled from "styled-components";
import { mediaQueries } from "../../styles/sharedStyledComponents/breakpoints";
import Link from "next/link";

const socials = [
  {
    name: "Facebook",
    href: "https://www.facebook.com/eFuseOfficial/",
    icon: "https://cdn.efuse.gg/static/images/clippy_landing_pages/facebook.png"
  },
  {
    name: "Instagram",
    href: "https://www.instagram.com/efuseofficial/",
    icon: "https://cdn.efuse.gg/static/images/clippy_landing_pages/instagram.png"
  },
  {
    name: "Twitter",
    href: "https://twitter.com/efuseofficial",
    icon: "https://cdn.efuse.gg/static/images/clippy_landing_pages/twitter.png"
  },
  {
    name: "Linkedin",
    href: "https://www.linkedin.com/company/efuseofficial/",
    icon: "https://cdn.efuse.gg/static/images/clippy_landing_pages/linkedin.png"
  },
  {
    name: "TikTok",
    href: "https://www.tiktok.com/@efuseofficial",
    icon: "https://cdn.efuse.gg/static/images/clippy_landing_pages/tiktok.png"
  },
  {
    name: "Youtube",
    href: "https://www.youtube.com/channel/UCeUQ6DGmB_nV57eVEJePXAQ",
    icon: "https://cdn.efuse.gg/static/images/clippy_landing_pages/youtube.png"
  }
];

const links = isClippy => [
  {
    label: "Privacy Policy",
    href: "https://efuse.gg/privacy"
  },
  {
    label: "About Us",
    href: "https://efuse.gg/org/efuse"
  },
  {
    label: "Feedback",
    href: "https://efuse-public.nolt.io"
  },
  {
    label: "Terms of Service",
    href: "https://efuse.gg/terms"
  },
  // TODO this option is temporary until the clippy FAQ page is created, remove after
  {
    label: isClippy ? "Your Data" : "Careers",
    href: isClippy ? "https://efuse.gg/privacy/data" : "https://efuse.breezy.hr"
  },
  {
    label: "Help",
    href: "https://efuse.gg/news/efuse-help"
  }
];

// TODO isClippy prop is temporary until we have a Clippy FAQ page, remove after
const EFFooter = ({ className = "", isClippy = false }) => {
  return (
    <FooterContainer className={className}>
      <SocialsContainer>
        <FollowUsText>Follow our socials @eFuseOfficial</FollowUsText>
        <SocialIconsContainer>
          {socials.map(item => (
            <SocialLink key={item.name} href={item.href}>
              <img src={item.icon} alt={`${item.name} icon`} />
            </SocialLink>
          ))}
        </SocialIconsContainer>
        <CopywriteText>&copy; 2021 eFuse, Inc. All rights reserved.</CopywriteText>
      </SocialsContainer>
      <FooterLinksContainer>
        <SiteLinks>
          {links(isClippy).map(item => (
            <SiteLink key={item.label} href={item.href}>
              {item.label}
            </SiteLink>
          ))}
        </SiteLinks>
        <LogoContainer>
          <Link href="/">
            <a href="/">
              <Logo src="https://cdn.efuse.gg/uploads/landing-pages/efuse_logo_dark.png" alt="efuse logo" />
            </a>
          </Link>
        </LogoContainer>
      </FooterLinksContainer>
    </FooterContainer>
  );
};

const FooterContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding-top: 20px;
  padding-bottom: 20px;
  margin: 0 140px;
  height: 200px;

  ${mediaQueries("lg")`
  margin: 0 100px;
  `};

  ${mediaQueries("md")`
  margin: 0 50px;
  `};

  ${mediaQueries("sm")`
  margin: 0 40px;
  flex-direction: column
  `};
`;

const SocialsContainer = styled.div`
  flex: 1;
  order: 1;

  ${mediaQueries("sm")`
  order: 0;
  `};
`;

const FollowUsText = styled.div`
  font-family: Poppins, sans-serif;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 20px;
  text-align: right;
  color: #10141f;

  ${mediaQueries("sm")`
  text-align: center;
  `};
`;

const SocialIconsContainer = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;

  ${mediaQueries("sm")`
  justify-content: center;
  `};
`;

const SocialLink = styled.a`
  color: rgba(10, 12, 18, 0.6);
  margin-left: 20px;

  ${mediaQueries("sm")`
  margin: 0 10px;
  `};
`;

const CopywriteText = styled.div`
  font-family: Poppins, sans-serif;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 20px;
  text-align: right;
  color: #10141f;
  padding-top: 40px;

  ${mediaQueries("sm")`
  padding-top: 20px;
  text-align: center;
  `};
`;

const FooterLinksContainer = styled.div``;

const SiteLinks = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-row-gap: 15px;
  grid-column-gap: 60px;
  flex-wrap: wrap;
  max-width: 500px;

  ${mediaQueries("md")`
  grid-template-columns: repeat(3, 1fr);
  max-width: 400px;
  `};
  ${mediaQueries("sm")`
  margin: 20px auto;
  margin-left: 10%;
  `};
`;

const SiteLink = styled.a`
  font-family: Poppins, sans-serif;
  font-style: normal;
  font-weight: 600;
  font-size: 14px;
  line-height: 12px;
  color: rgba(10, 12, 18, 0.6);

  ${mediaQueries("md")`
  font-size: 12px;
  `};
`;

const LogoContainer = styled.div`
  margin-top: 15px;
`;

const Logo = styled.img`
  width: 135px;
  height: auto;
  display: block;
  ${mediaQueries("sm")`
  margin: auto;
  `};
`;

export default EFFooter;
