import { useSelector } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBolt } from "@fortawesome/pro-duotone-svg-icons";
import { Badge, OverlayTrigger, Tooltip } from "react-bootstrap";

import Style from "./BoltIcon.module.scss";

const BoltIcon = ({ streak }) => {
  const enableStreaksIcon = useSelector(state => state.features.streaks_daily_icon);
  return (
    <>
      {enableStreaksIcon && streak?.isActive && streak?.streakDuration > 1 && (
        <OverlayTrigger
          placement="top"
          overlay={
            <Tooltip id="tooltip-hype" style={{ zIndex: 9999 }}>
              Daily Active Streak
            </Tooltip>
          }
        >
          <span className={Style.boltWrapper}>
            <FontAwesomeIcon icon={faBolt} className={Style.boltIcon} />
            <Badge variant="primary" className={Style.boltBadge}>
              {streak?.streakDuration}
            </Badge>
          </span>
        </OverlayTrigger>
      )}
    </>
  );
};

export default BoltIcon;
