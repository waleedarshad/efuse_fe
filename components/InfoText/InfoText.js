import { OverlayTrigger, Tooltip } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faInfoCircle } from "@fortawesome/pro-solid-svg-icons";

import PropTypes from "prop-types";

import Style from "./InfoText.module.scss";

const InfoText = ({ label, instructions, theme }) => (
  <span className={`${Style.infoText} ${Style[theme]}`}>
    {label}{" "}
    <OverlayTrigger
      placement="top"
      overlay={
        <Tooltip id="tooltip-top" style={{ zIndex: 9999 }}>
          {instructions}
        </Tooltip>
      }
    >
      <FontAwesomeIcon icon={faInfoCircle} />
    </OverlayTrigger>
  </span>
);

InfoText.propTypes = {
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.element]).isRequired,
  instructions: PropTypes.string.isRequired,
  theme: PropTypes.string
};

InfoText.defaultProps = {
  theme: "external"
};

export default InfoText;
