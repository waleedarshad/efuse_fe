import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { faMoon, faSun } from "@fortawesome/pro-solid-svg-icons";

import { changeToDarkTheme, changeToLightTheme } from "../../store/actions/themeActions";
import EFCircleIconButtonTooltip from "../Buttons/EFCircleIconButtonTooltip/EFCircleIconButtonTooltip";

const EFToggleTheme = () => {
  const dispatch = useDispatch();
  const currentThemeType = useSelector(state => state.theme.currentThemeType);

  return (
    <EFCircleIconButtonTooltip
      colorTheme="transparent"
      fontColorTheme="light"
      icon={currentThemeType === "light" ? faMoon : faSun}
      shadowTheme="none"
      size="medium"
      tooltipPlacement="bottom"
      tooltipContent={currentThemeType === "light" ? "Switch to Dark Theme" : "Switch to Light Theme"}
      onClick={() => (currentThemeType === "light" ? dispatch(changeToDarkTheme()) : dispatch(changeToLightTheme()))}
    />
  );
};

export default EFToggleTheme;
