const WindowPortal = (externalURL, windowTitle) => {
  window.open(externalURL, windowTitle, "width=600,height=400,left=200,top=200");
};

export default WindowPortal;
