import { faComments } from "@fortawesome/pro-solid-svg-icons";
import Router from "next/router";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { isUserLoggedIn } from "../../helpers/AuthHelper";
import { checkIfFollower } from "../../store/actions/followerActions";
import { startChatWith } from "../../store/actions/messageActions";
import EFCircleIconButton from "../Buttons/EFCircleIconButton/EFCircleIconButton";
import EFCircleIconButtonTooltip from "../Buttons/EFCircleIconButtonTooltip/EFCircleIconButtonTooltip";
import SignupModal from "../SignupModal/SignupModal";

const MessageButton = ({ userName, userId }) => {
  const dispatch = useDispatch();

  const currentUser = useSelector(state => state.auth.currentUser);
  const isFollowed = useSelector(state => state.followers.isFollowed);
  const followsYou = useSelector(state => state.followers.followsYou);
  const disable = useSelector(state => state.followers.disable);
  const userLoggedIn = isUserLoggedIn();

  useEffect(() => {
    if (userLoggedIn && userId && currentUser?.id) {
      dispatch(checkIfFollower(userId));
    }
  }, []);

  const onClick = () => {
    dispatch(startChatWith(userId, userName));
    Router.push("/messages");
  };

  let content = null;

  const showMessageButton = isFollowed && followsYou;

  if (!userId || (currentUser && currentUser.id === userId)) {
    content = "";
  } else if (isUserLoggedIn()) {
    content = showMessageButton ? (
      <EFCircleIconButton onClick={() => onClick()} disabled={disable} icon={faComments} />
    ) : (
      <EFCircleIconButtonTooltip
        disabled
        icon={faComments}
        tooltipContent={`You must be mutual followers to message ${userName}`}
        tooltipPlacement="top"
      />
    );
  } else {
    content = (
      <SignupModal
        title={`Create an account to message ${userName}.`}
        message={`Connect with ${userName} and other gamers now. Sign up today!`}
      >
        <EFCircleIconButton disabled={disable} icon={faComments} />
      </SignupModal>
    );
  }

  return <>{content}</>;
};

export default MessageButton;
