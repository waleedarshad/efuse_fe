import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faStar } from "@fortawesome/pro-solid-svg-icons";
import Style from "./FeedAd.module.scss";

const FeedAd = props => {
  const { adID, setID, width, height } = props;
  const imgUrl = `https://servedbyadbutler.com/go2/;ID=${adID};setID=${setID}`;
  const imgHref = `https://servedbyadbutler.com/adserve/;ID=${adID};setID=${setID};type=img;click=CLICK_MACRO_PLACEHOLDER`;
  return (
    <div className={Style.adContainer}>
      <div className={Style.adHeader}>
        <FontAwesomeIcon icon={faStar} className={Style.starIcon} />
        <p className={Style.headerText}>Promoted</p>
      </div>
      <a className={Style.hrefTag} href={imgUrl} target="_blank">
        <img className={Style.adImage} src={imgHref} width={width} height={height} />
      </a>
    </div>
  );
};

export default FeedAd;
