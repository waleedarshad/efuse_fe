import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit } from "@fortawesome/pro-regular-svg-icons";

import Style from "./BracketMatch.module.scss";
import BracketMatchModal from "../BracketMatchModal/BracketMatchModal";

const BracketMatch = ({ teams, editMatch, match, tournamentId, roundNumber, totalTeams }) => {
  const teamOneSet = match?.teams[0]?.team?.name;
  const teamTwoSet = match?.teams[1]?.team?.name;
  const teamOneStats = match?.teamOneScore ? match.teamOneScore : 0;
  const teamTwoStats = match?.teamTwoScore ? match.teamTwoScore : 0;

  const checkTeamWinner = teamId => {
    const winner = match?.winner?._id;
    if (!winner) {
      return "noResult";
    }
    if (teamId === winner) {
      return "winner";
    }
    return "loser";
  };

  return (
    <div className={Style.match}>
      <div className={Style.positionEditButton}>
        {((editMatch && teamOneSet && teamTwoSet) || (editMatch && roundNumber === 1)) && (
          <BracketMatchModal
            match={match}
            tournamentId={tournamentId}
            roundNumber={roundNumber}
            totalTeams={totalTeams}
          >
            <div className={Style.editMatchOverlay}>
              <FontAwesomeIcon icon={faEdit} className={Style.editIcon} />
              Edit Match
            </div>
          </BracketMatchModal>
        )}
      </div>

      {teams?.map((team, index) => {
        return (
          <div
            className={`${Style.team} ${checkTeamWinner(team?.team?._id) === "winner" &&
              Style.teamWinner} ${checkTeamWinner(team?.team?._id) === "loser" && Style.teamLoser} ${!team?.team
              ?.name &&
              roundNumber !== 1 &&
              Style.emptyTeam}`}
            key={team?._id}
          >
            <span className={Style.teamName}>{team?.team?.name}</span>
            {team?.team?.name && <span className={Style.score}>{index === 0 ? teamOneStats : teamTwoStats}</span>}
          </div>
        );
      })}
    </div>
  );
};

export default BracketMatch;
