import React from "react";
import Modal from "../../Modal/Modal";
import UpdateMatch from "../UpdateMatch/UpdateMatch";

const BracketMatchModal = ({ children, match, tournamentId, roundNumber, totalTeams }) => {
  const component = (
    <UpdateMatch match={match} tournamentId={tournamentId} roundNumber={roundNumber} totalTeams={totalTeams} />
  );
  return (
    <Modal
      displayCloseButton={true}
      allowBackgroundClickClose={false}
      openOnLoad={false}
      component={component}
      textAlignCenter={false}
      customMaxWidth="800px"
      title="Edit Match"
    >
      {children}
    </Modal>
  );
};

export default BracketMatchModal;
