import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { updateMatch, updateMatchScreenshot } from "../../../store/actions/erenaActions";
import Style from "./UpdateMatch.module.scss";
import EFCard from "../../Cards/EFCard/EFCard";
import SelectBox from "../../SelectBox/SelectBox";
import directUpload from "../../hoc/directUpload";
import EFCircleIconButtonTooltip from "../../Buttons/EFCircleIconButtonTooltip/EFCircleIconButtonTooltip";
import UploadMatchImage from "./UploadMatchImage/UploadMatchImage";
import { faTrophy } from "@fortawesome/pro-regular-svg-icons";
import EFImage from "../../EFImage/EFImage";

const UpdateMatch = ({ match, tournamentId, roundNumber, totalTeams }) => {
  const dispatch = useDispatch();

  useEffect(() => {
    setTeamOneScore(match?.teamOneScore ? match.teamOneScore : 0);
    setTeamTwoScore(match?.teamTwoScore ? match.teamTwoScore : 0);
    setTeamOneName(match?.teams[0]?.team?.name && match.teams[0].team._id);
    setTeamTwoName(match?.teams[1]?.team?.name && match.teams[1].team._id);
  }, [match]);

  const [team1Score, setTeamOneScore] = useState(0);
  const [team2Score, setTeamTwoScore] = useState(0);
  const [team1Name, setTeamOneName] = useState("Team 1");
  const [team2Name, setTeamTwoName] = useState("Team 2");

  let teamOneOptions = [];
  let teamTwoOptions = [];

  if (!match?.teams[0]?.team?.name) {
    teamOneOptions.push({ label: "Select a Team...", value: "" });
  }

  if (!match?.teams[1]?.team?.name) {
    teamTwoOptions.push({ label: "Select a Team...", value: "" });
  }

  const teamOptions = totalTeams.map(team => {
    return { label: team?.name, value: team?._id };
  });

  teamOneOptions = [...teamOneOptions, ...teamOptions];
  teamTwoOptions = [...teamTwoOptions, ...teamOptions];

  const onChange = (e, index) => {
    if (index === 0) {
      setTeamOneScore(e.target.value);
    } else {
      setTeamTwoScore(e.target.value);
    }
  };

  //update a teamId in a match
  const updateTeamOnSelect = (e, index) => {
    e.preventDefault();
    if (index === 0) {
      setTeamOneName(e.target.value);
    } else {
      setTeamTwoName(e.target.value);
    }
    const data = {
      addTeam: e.target.value,
      addTeamIndex: index
    };
    dispatch(updateMatch(tournamentId, match?._id, data, "Successfully set team"));
  };

  //set the score for a team in a match
  const updateMatchScoreOnBlur = (score, index) => {
    let data = {};
    //check if first or second team
    if (index === 0) {
      data = {
        teamOneScore: score
      };
    } else {
      data = {
        teamTwoScore: score
      };
    }
    dispatch(updateMatch(tournamentId, match?._id, data, "Successfully set score"));
  };

  //set the winner for the match
  const updateMatchOnClick = teamId => {
    const data = {
      winner: teamId
    };
    dispatch(updateMatch(tournamentId, match?._id, data, "Successfully set winner"));
  };

  const onFileUpload = (file, index) => {
    let data = {};
    if (index === 0) {
      data = {
        teamOneImage: file
      };
    } else {
      data = {
        teamTwoImage: file
      };
    }

    dispatch(updateMatchScreenshot(tournamentId, match?._id, data));
  };

  return (
    <>
      {match?.teams?.map((team, index) => {
        return (
          <div className={Style.marginItem} key={team?.team?._id}>
            <EFCard widthTheme="fullWidth">
              <div className={Style.teamContainer}>
                {team?.team?.name && roundNumber !== 1 && <span className={Style.teamName}>{team.team.name}</span>}
                {roundNumber === 1 && (
                  <div className={Style.selectBoxWrapper}>
                    <SelectBox
                      disabled={false}
                      name={index === 0 ? "team1" : "team2"}
                      options={index === 0 ? teamOneOptions : teamTwoOptions}
                      size="md"
                      value={index === 0 ? team1Name : team2Name}
                      theme="whiteShadow"
                      validated={false}
                      errorMessage="Owner is required."
                      onChange={e => updateTeamOnSelect(e, index)}
                    />
                  </div>
                )}

                <div className={Style.inputFieldContainer}>
                  <input
                    className={Style.inputField}
                    onChange={e => onChange(e, index)}
                    type="number"
                    value={index === 0 ? team1Score : team2Score}
                    onBlur={e => updateMatchScoreOnBlur(e.target.value, index)}
                  ></input>
                </div>

                <UploadMatchImage
                  team={team}
                  onFileUpload={e => onFileUpload(e, index)}
                  hasImage={
                    index === 0
                      ? match?.teamOneImage?.filename !== "no_image.jpg"
                      : match?.teamTwoImage?.filename !== "no_image.jpg"
                  }
                />

                <EFCircleIconButtonTooltip
                  tooltipContent="Select as Winner"
                  icon={faTrophy}
                  size="small"
                  shadowTheme="small"
                  disabled={!team?.team?._id}
                  colorTheme={match?.winner?._id && match?.winner?._id === team?.team?._id ? "primary" : "light"}
                  onClick={() => updateMatchOnClick(team?.team?._id)}
                />
              </div>

              <div className={Style.updateImageWrapper}>
                {index === 0 && match?.teamOneImage && match?.teamOneImage?.filename !== "no_image.jpg" && (
                  <div className={Style.imageWrapper}>
                    <EFImage src={match.teamOneImage.url} className={Style.matchImage} alt="Team 1 image" />
                  </div>
                )}
                {index === 1 && match?.teamTwoImage && match?.teamTwoImage?.filename !== "no_image.jpg" && (
                  <div className={Style.imageWrapper}>
                    <EFImage src={match.teamTwoImage.url} className={Style.matchImage} alt="Team 2 image" />
                  </div>
                )}
              </div>
            </EFCard>
            {index === 0 && (
              <div className={Style.vsContainer}>
                <i>VS</i>
              </div>
            )}
          </div>
        );
      })}
    </>
  );
};

export default directUpload(UpdateMatch);
