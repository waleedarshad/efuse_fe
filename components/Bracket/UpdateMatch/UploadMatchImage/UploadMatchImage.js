import React from "react";
import { faCamera } from "@fortawesome/pro-solid-svg-icons";

import Style from "./UploadMatchImage.module.scss";
import DirectUpload from "../../../DirectUpload/DirectUpload";
import directUpload from "../../../hoc/directUpload";
import EFCircleIconButtonTooltip from "../../../Buttons/EFCircleIconButtonTooltip/EFCircleIconButtonTooltip";

const UploadMatchImage = ({ team, hasImage, onUploadProgress, onUploadError, onUploadStart, handleFinishedUpload }) => {
  return (
    <DirectUpload
      onProgress={onUploadProgress}
      onError={onUploadError}
      preprocess={onUploadStart}
      onFinish={handleFinishedUpload}
      directory="uploads/matches/"
      customDesign
    >
      <div className={Style.cameraButtonWrapper}>
        <EFCircleIconButtonTooltip
          tooltipContent="Upload an Image for the Match"
          icon={faCamera}
          size="small"
          shadowTheme="small"
          colorTheme={hasImage ? "primary" : "light"}
          disabled={!team?.team?._id}
        />
      </div>
    </DirectUpload>
  );
};

export default directUpload(UploadMatchImage);
