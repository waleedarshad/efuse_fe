import React from "react";
import ScrollContainer from "react-indiana-drag-scroll";
import Style from "./Bracket.module.scss";
import BracketMatch from "./BracketMatch/BracketMatch";

const Bracket = ({ primaryColor, secondaryColor, bracket, editMatch, tournamentId, totalTeams }) => {
  return (
    <ScrollContainer className={Style.bracketWrapper}>
      <section className={Style.bracketContainer}>
        <div className={Style.headerColor} style={{ backgroundColor: primaryColor }} />
        <div
          className={Style.bracketBackground}
          style={{
            backgroundImage: `${`linear-gradient(${secondaryColor}, rgba(255, 255, 255, 0))`}`
          }}
        />
        {bracket?.rounds?.map(round => {
          return (
            <div className={Style.roundWrapper} key={round?._id}>
              <div className={Style.roundTitle}>
                {round?.round?.roundTitle}
                {/* <p className={Style.date}>October 23</p> */}
              </div>
              <div className={Style.round}>
                {round?.round?.matches?.map(match => {
                  return (
                    <BracketMatch
                      teams={match?.match?.teams}
                      editMatch={editMatch}
                      key={match?._id}
                      match={match?.match}
                      tournamentId={tournamentId}
                      roundNumber={round?.round?.roundNumber}
                      totalTeams={totalTeams}
                    />
                  );
                })}
              </div>
            </div>
          );
        })}
      </section>
    </ScrollContainer>
  );
};

Bracket.defaultProps = {
  primaryColor: Style.primaryLeaderboardColor,
  secondaryColor: Style.secondaryLeaderboardColor
};

export default Bracket;
