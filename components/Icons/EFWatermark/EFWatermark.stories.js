import React from "react";
import EFWatermark from "./EFWatermark";

export default {
  title: "Icons/EFWatermark",
  component: EFWatermark,
  argTypes: {
    type: {
      control: {
        type: "select",
        options: ["light", "dark"]
      }
    },
    size: {
      control: {
        type: "select",
        options: ["extraSmall", "small", "medium", "large", "extraLarge"]
      }
    }
  }
};

const Story = args => <EFWatermark {...args} />;

export const Dark = Story.bind({});
Dark.args = {
  type: "dark",
  size: "large"
};

export const Light = Story.bind({});
Light.args = {
  type: "light",
  size: "large"
};
