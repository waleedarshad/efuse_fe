import React from "react";
import Style from "./EFWatermark.module.scss";
import Link from "next/link";

const EFWatermark = ({ type, size }) => {
  return (
    <Link href="/lounge/[type]" as="/lounge/featured">
      <img
        className={`${Style.watermark} ${Style[size]}`}
        src={
          type === "light"
            ? "https://cdn.efuse.gg/uploads/static/global/efuseWatermarkLight.png"
            : "https://cdn.efuse.gg/uploads/static/global/efuseLogoDark.png"
        }
        alt="efuse watermark"
      />
    </Link>
  );
};

EFWatermark.defaultProps = {
  type: "light",
  size: "large"
};

export default EFWatermark;
