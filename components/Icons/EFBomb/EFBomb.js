import React from "react";
import Style from "./EFBomb.module.scss";

const EFBomb = ({ darkBomb, size }) => {
  return (
    <img
      src={
        darkBomb
          ? "https://cdn.efuse.gg/uploads/static/global/efuseBombLogoDark.png"
          : "https://cdn.efuse.gg/uploads/static/global/efuseBombLogoLightNoMargin.png"
      }
      className={size && Style[size]}
      alt="eFuse logo"
    />
  );
};

EFBomb.defaultProps = {
  darkBomb: false,
  size: "small"
};

export default EFBomb;
