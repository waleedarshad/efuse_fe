import React from "react";
import EFBomb from "./EFBomb";

export default {
  title: "Icons/EFBomb",
  component: EFBomb,
  argTypes: {
    darkBomb: {
      control: {
        type: "boolean"
      }
    },
    size: {
      control: {
        type: "select",
        options: ["small", "medium", "large"]
      }
    }
  }
};

const Story = args => <EFBomb {...args} />;

export const Dark = Story.bind({});
Dark.args = {
  darkBomb: true,
  size: "medium"
};

export const Light = Story.bind({});
Light.args = {
  darkBomb: false,
  size: "medium"
};
