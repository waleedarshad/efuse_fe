import moment from "moment";
import PropTypes from "prop-types";

import Style from "./DatepickerMonthElement.module.scss";
import { buildYearOptions } from "../../helpers/GeneralHelper";

const DatepickerMonthElement = ({ month, onMonthSelect, onYearSelect, includeCurrentYear }) => (
  <div style={{ display: "flex", justifyContent: "center" }}>
    <div>
      <select
        className={`${Style.pickerSelect}`}
        value={month.month()}
        onChange={e => onMonthSelect(month, e.target.value)}
      >
        {moment.months().map((label, value) => (
          <option value={value} key={value}>
            {label}
          </option>
        ))}
      </select>
    </div>
    <div>
      <select
        className={`${Style.pickerSelect}`}
        value={month.year()}
        onChange={e => onYearSelect(month, e.target.value)}
      >
        {buildYearOptions(includeCurrentYear)}
      </select>
    </div>
  </div>
);

DatepickerMonthElement.propTypes = {
  includeCurrentYear: PropTypes.bool
};

DatepickerMonthElement.defaultProps = {
  includeCurrentYear: false
};

export default DatepickerMonthElement;
