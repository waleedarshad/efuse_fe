import React, { Component } from "react";
import PropTypes from "prop-types";

import Button from "./MoreButton/MoreButton";

const ClampContent = {
  display: "-webkit-box",
  overflow: "hidden",
  WebkitBoxOrient: "vertical",
  WebkitLineClamp: 3
};

const WrapperStyle = { display: "flex", flexDirection: "column" };

class ShowMore extends Component {
  divRef = React.createRef();

  timeOut = [];

  state = {
    hasExtraContent: false,
    displayLess: false,
    displayMore: true
  };

  componentDidMount() {
    window.addEventListener("resize", this.onResize);
    this.calculateHeight();
  }

  componentDidUpdate() {
    this.calculateHeight();
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.onResize);
    this.timeOut.forEach(timeout => clearTimeout(timeout));
  }

  onResize = () => {
    this.calculateHeight();
  };

  calculateHeight = () => {
    const element = this.divRef.current;
    let timeOut = null;
    if (element) {
      timeOut = setTimeout(() => {
        this.setState({
          hasExtraContent: element.offsetHeight < element.scrollHeight
        });
      }, 500);
    }
    this.timeOut.push(timeOut);
  };

  displayAll = () => {
    this.setState({ displayLess: true, displayMore: false });
  };

  displayLess = () => {
    this.setState({ displayLess: false, displayMore: true });
  };

  render() {
    const {
      lineClamp,
      showMoreContent,
      showLessContent,
      showMoreButtonClass,
      showLessButtonClass,
      wrapperClass
    } = this.props;
    const clampContent = { ...ClampContent };
    clampContent.WebkitLineClamp = lineClamp;
    const { hasExtraContent, displayLess, displayMore } = this.state;
    return (
      <div style={WrapperStyle} className={wrapperClass}>
        <div style={displayMore ? clampContent : null} ref={this.divRef}>
          {this.props.children}
        </div>
        {hasExtraContent && displayMore && (
          <Button customClass={showMoreButtonClass} content={showMoreContent} onClick={this.displayAll} />
        )}
        {displayLess && (
          <Button customClass={showLessButtonClass} content={showLessContent} onClick={this.displayLess} />
        )}
      </div>
    );
  }
}

ShowMore.propTypes = {
  showMoreContent: PropTypes.string,
  showLessContent: PropTypes.string,
  lineClamp: PropTypes.number,
  showMoreButtonClass: PropTypes.string,
  showLessButtonClass: PropTypes.string,
  wrapperClass: PropTypes.string
};

ShowMore.defaultProps = {
  showMoreContent: "Show More",
  showLessContent: "Show Less",
  lineClamp: 3
};

export default ShowMore;
