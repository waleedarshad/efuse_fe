import React from "react";

const ButtonStyle = {
  boxShadow: "none",
  border: "none",
  background: "transparent",
  fontSize: "12px",
  margin: "0px",
  padding: "0px",
  zIndex: 3,
  fontWeight: 600,
  alignSelf: "flex-end",
  outline: "none"
};

const MoreButton = ({ content, onClick, customClass }) => {
  return (
    <button style={ButtonStyle} onClick={onClick} type="button" className={customClass}>
      {content}
    </button>
  );
};

export default MoreButton;
