import React from "react";
import { Controller } from "react-hook-form";
import Switch from "../Switch/Switch";
import InputLabelWithHelpTooltip from "../InputLabelWithHelpTooltip/InputLabelWithHelpTooltip";
import InputLabel from "../InputLabel/InputLabel";
import Style from "./formControls.module.scss";

const EFInputControl = ({ label, tooltip, name, control }) => {
  const render = ({ field }) => {
    return (
      <div className={Style.switchWrapper}>
        <Switch name={name} value={!!field.value} checked={!!field.value} onChange={() => field.onChange(!field.value)} />
      </div>
    );
  };
  return (
    <div className={Style.flexWrapper}>
      {tooltip && label && <InputLabelWithHelpTooltip tooltipText={tooltip} labelText={label} theme="darkColor" />}
      {label && !tooltip && <InputLabel theme="darkColor">{label}</InputLabel>}
      <Controller name={name} control={control} defaultValue={false} render={render} />
    </div>
  );
};

export default EFInputControl;
