import React from "react";
import { Controller } from "react-hook-form";
import CustomUrl from "../FormFields/CustomUrl/CustomUrl";
import { validateSlug } from "../../helpers/FormHelper";

const EFCustomURLControl = ({ name, errors, control, required, longName, slugType, pathType, disabled }) => {
  const validateUrlSlug = async urlSlug => {
    const { valid, error } = await validateSlug(urlSlug, slugType);
    return valid || error || "URL invalid.";
  };

  const validationRules = {
    required: { value: required, message: "URL is required" },
    validate: { validateUrlSlug }
  };

  const render = ({ field }) => (
    <CustomUrl
      onChange={field.onChange}
      shortName={field.value}
      longName={!disabled && longName}
      disabled={disabled}
      pathType={pathType}
      name={field.name}
      onBlur={field.onBlur}
      invalidUrlMessage={errors[field.name]?.message}
      isUrlValid={!errors[field.name]}
    />
  );

  return (
    <>
      <Controller
        name={name}
        control={control}
        defaultValue={disabled ? "" : longName}
        rules={!disabled && validationRules}
        render={render}
      />
    </>
  );
};

export default EFCustomURLControl;
