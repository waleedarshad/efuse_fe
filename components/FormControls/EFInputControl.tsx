import React from "react";
import { Controller, FieldErrors, Validate } from "react-hook-form";
import { Control } from "react-hook-form/dist/types/form";
import { FieldPathValue } from "react-hook-form/dist/types/utils";
import InputField from "../InputField/InputField";
import InputLabel from "../InputLabel/InputLabel";
import InputLabelWithHelpTooltip from "../InputLabelWithHelpTooltip/InputLabelWithHelpTooltip";

interface EFInputControlProps {
  label?: string;
  tooltip?: string;
  errors: FieldErrors;
  name: string;
  control: Control<any>;
  required?: boolean;
  placeholder?: string;
  maxLength?: number;
  type?: "text" | "number" | "textarea";
  prepend?: string;
  step?: number;
  valueAsNumber?: boolean;
  minValue?: number;
  validate?: Validate<FieldPathValue<any, any>>;
  defaultValue?: string | number;
  disabled?: boolean;
  className?: string;
}

const EFInputControl = ({
  label,
  tooltip,
  errors,
  name,
  control,
  required = false,
  placeholder,
  maxLength,
  type,
  prepend,
  step,
  validate,
  valueAsNumber = false,
  defaultValue = "",
  minValue,
  disabled = false,
  className
}: EFInputControlProps) => {
  const tryParse = event => {
    const isNumber = new RegExp(/^([0-9]+)$/g).test(event.target.value);

    return isNumber ? Number.parseInt(event.target.value, 10) : "";
  };

  const render = ({ field }) => {
    return (
      <InputField
        name={field.name}
        placeholder={placeholder}
        theme="whiteShadow"
        size="lg"
        onChange={event => field.onChange(valueAsNumber ? tryParse(event) : event)}
        value={field.value}
        maxLength={maxLength}
        onBlur={field.onBlur}
        isValid={!errors[field.name]}
        isInvalid={errors[field.name]}
        errorMessage={errors[field.name]?.message}
        type={type}
        prepend={prepend}
        step={step}
        min={minValue}
        className={className}
        disabled={disabled}
      />
    );
  };
  return (
    <>
      {tooltip && label && <InputLabelWithHelpTooltip tooltipText={tooltip} labelText={label} optional={!required} />}
      {label && !tooltip && (
        <InputLabel optional={!required} theme="darkColor">
          {label}
        </InputLabel>
      )}
      <Controller
        name={name}
        control={control}
        defaultValue={defaultValue}
        rules={{
          required: { value: required, message: `${label || "Field"} is required` },
          validate,
          min: { value: minValue, message: `${label || "Value"} must be at least ${minValue}` }
        }}
        render={render}
      />
    </>
  );
};

export default EFInputControl;
