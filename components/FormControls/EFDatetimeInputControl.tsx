import React from "react";
import { Controller, FieldErrors } from "react-hook-form";
import Flatpickr from "react-flatpickr";
import FormControl from "react-bootstrap/FormControl";
import { Control } from "react-hook-form/dist/types/form";
import InputLabel from "../InputLabel/InputLabel";
import Style from "./formControls.module.scss";
import InfoText from "../InfoText/InfoText";

interface EFDatetimeInputControlProps {
  label?: string;
  instructions?: string;
  errors: FieldErrors;
  name: string;
  control: Control;
  required?: boolean;
  minDate?: string;
}
const EFDatetimeInputControl = ({
  label,
  errors,
  instructions,
  name,
  control,
  required = false,
  minDate
}: EFDatetimeInputControlProps) => {
  const render = ({ field }) => {
    return (
      <Flatpickr
        data-enable-time
        name={field.name}
        value={field.value}
        className={Style.flatpickr}
        placeholder="Select a date and time"
        options={{
          minDate,
          dateFormat: "F j, Y @ h:i K"
        }}
        onChange={e => field.onChange(e[0]?.toISOString && e[0].toISOString())}
      />
    );
  };
  return (
    <>
      {instructions ? (
        <InfoText
          theme="internal"
          label={<InputLabel theme="darkColor">{label}</InputLabel>}
          instructions={instructions}
        />
      ) : (
        <InputLabel theme="darkColor">{label}</InputLabel>
      )}
      <br />
      <Controller
        name={name}
        control={control}
        defaultValue=""
        rules={{ required: { value: required, message: `${label} is required` } }}
        render={render}
      />
      {errors[name] && (
        <FormControl.Feedback type="invalid" style={{ display: "block" }}>
          {errors[name].message}
        </FormControl.Feedback>
      )}
    </>
  );
};

export default EFDatetimeInputControl;
