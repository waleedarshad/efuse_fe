import React from "react";
import { Controller } from "react-hook-form";
import FormControl from "react-bootstrap/FormControl";
import InputLabel from "../InputLabel/InputLabel";
import EFSelectGamesField from "../EFSelectGamesField/EFSelectGamesField";

const EFSelectGamesModalControl = ({ label, errors, name, control, required, selectMultiple }) => {
  const render = ({ field }) => {
    const onChange = selectMultiple ? field.onChange : games => field.onChange(games[0]);
    return <EFSelectGamesField onChange={onChange} value={field.value} selectMultiple={selectMultiple} />;
  };
  return (
    <>
      <InputLabel theme="darkColor" optional={!required}>
        {label}
      </InputLabel>
      <Controller
        name={name}
        control={control}
        defaultValue=""
        rules={{ required: { value: required, message: `${label} is required` } }}
        render={render}
      />
      {errors[name] && (
        <FormControl.Feedback type="invalid" style={{ display: "block" }}>
          {errors[name].message}
        </FormControl.Feedback>
      )}
    </>
  );
};

export default EFSelectGamesModalControl;
