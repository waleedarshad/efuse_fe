import React from "react";
import { Controller } from "react-hook-form";
import { Control } from "react-hook-form/dist/types/form";
import styled from "styled-components";
import InputField from "../InputField/InputField";

interface EFCheckboxControlProps {
  name: string;
  control: Control<any>;
  defaultValue?: boolean;
  label: string;
}

const Wrapper = styled.div`
  display: flex;

  input {
    display: inline-block;
    width: 18px !important;
    height: 20px !important;
    font-style: normal;
    font-weight: 900;
    font-size: 20px;
    line-height: 20px;
    border-color: #0a0c12 !important;

    :hover {
      cursor: pointer;
    }
  }

  p {
    margin: 0 0 0 13px;
    color: #0a0c12;
    font-family: Open Sans, sans-serif;
    font-style: normal;
    font-weight: normal;
    font-size: 14px;
    line-height: 20px;
  }
`;

const EFCheckboxControl = ({ name, label, control, defaultValue = false }: EFCheckboxControlProps) => {
  const render = ({ field }) => {
    return (
      <Wrapper>
        <InputField
          name={field.name}
          onChange={event => field.onChange(event)}
          value={field.value}
          onBlur={field.onBlur}
          type="checkbox"
          defaultChecked={defaultValue}
        />
        <p>{label}</p>
      </Wrapper>
    );
  };
  return <Controller name={name} control={control} defaultValue={defaultValue} render={render} />;
};

export default EFCheckboxControl;
