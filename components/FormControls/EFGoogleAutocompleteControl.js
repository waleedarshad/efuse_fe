import React from "react";
import { Controller } from "react-hook-form";
import GoogleAutoComplete from "../GoogleAutocomplete/GoogleAutocomplete";
import InputLabel from "../InputLabel/InputLabel";

const EFGoogleAutocompleteControl = ({ name, control }) => {
  const render = ({ field }) => {
    const onSuggestSelect = suggest => field.onChange(suggest?.label || suggest?.description || "");
    return (
      <GoogleAutoComplete
        onSuggest={onSuggestSelect}
        placeholder="Search Locations..."
        className="geoselect-internal"
        initialValue={field.value}
      />
    );
  };
  return (
    <>
      <InputLabel theme="darkColor" mobileMarginTop optional>
        Location
      </InputLabel>
      <Controller name={name} control={control} defaultValue="" render={render} />
    </>
  );
};

export default EFGoogleAutocompleteControl;
