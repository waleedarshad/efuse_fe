import React, { useRef } from "react";
import FormControl from "react-bootstrap/FormControl";
import InputLabel from "../../InputLabel/InputLabel";
import DisplayMedia from "../../DirectUpload/DisplayMedia/DisplayMedia";
import UploadImage from "../../DirectUpload/UploadImage/UploadImage";
import WithUploadContext from "../../DirectUpload/UploadContext";
import UploadWithValidationControl from "./UploadWithValidationControl";
import FileUploadButton from "../../ImageUploader/FileUploadButton/FileUploadButton";

const EFImageUploadControl = ({ control, label, name, directory, errors, buttonTheme = "lightButton" }) => {
  const uploadProviderRef = useRef(null);

  const render = ({ field }) => {
    return (
      <>
        <DisplayMedia
          file={field.value ? { url: field.value, edit: true } : {}}
          clearUploadedFile={() => {
            uploadProviderRef.current.cancelUpload();
            field.onChange("");
          }}
        />
        <UploadImage
          onFileUpload={file => field.onChange(file?.url)}
          file={field.value}
          isMediaUploaded={!!field.value}
          directory={directory}
        >
          <FileUploadButton theme={buttonTheme} />
        </UploadImage>
      </>
    );
  };

  return (
    <WithUploadContext ref={uploadProviderRef}>
      <InputLabel theme="internal">{label}</InputLabel>
      <UploadWithValidationControl control={control} render={render} name={name} />
      {errors[name] && (
        <FormControl.Feedback type="invalid" style={{ display: "block" }}>
          {errors[name].message}
        </FormControl.Feedback>
      )}
    </WithUploadContext>
  );
};

export default EFImageUploadControl;
