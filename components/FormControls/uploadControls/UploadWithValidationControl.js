import React, { useContext } from "react";
import { Controller } from "react-hook-form";
import { UploadContext } from "../../DirectUpload/UploadContext";

const UploadWithValidationControl = ({ control, name, render }) => {
  const { isUploading } = useContext(UploadContext);
  const validationRules = {
    validate: () => !isUploading || "Please wait for your file to finish loading."
  };

  return <Controller control={control} name={name} defaultValue="" render={render} rules={validationRules} />;
};

export default UploadWithValidationControl;
