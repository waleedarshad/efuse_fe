import React from "react";
import { Controller } from "react-hook-form";
import FormControl from "react-bootstrap/FormControl";
import InputLabel from "../../InputLabel/InputLabel";
import InfoText from "../../InfoText/InfoText";
import ImageUploader from "../../ImageUploader/ImageUploader";
import { rescueNil } from "../../../helpers/GeneralHelper";

const EFCroppableImageControl = ({
  label,
  errors,
  instructions,
  name,
  control,
  required,
  validate,
  onPreviewChange,
  toggleCropperModal
}) => {
  const render = ({ field }) => {
    const onCropChange = croppedImage => {
      const { blob, preview } = croppedImage;
      const croppedFile = new File([blob], blob.name, {
        type: blob.type,
        preview
      });
      field.onChange(croppedFile);
      onPreviewChange(preview);
    };

    return (
      <ImageUploader
        text={label}
        name={field.name}
        onDrop={selectedFile => field.onChange(selectedFile[0])}
        value={rescueNil(field.value, "url")}
        theme="lightButton"
        showCropper
        aspectRatioWidth={600}
        aspectRatioHeight={400}
        onCropChange={onCropChange}
        toggleCropperModal={toggleCropperModal}
        showImageDescription
      />
    );
  };
  return (
    <>
      {instructions ? (
        <InfoText
          theme="internal"
          label={<InputLabel theme="darkColor">{label}</InputLabel>}
          instructions={instructions}
        />
      ) : (
        <InputLabel theme="darkColor">{label}</InputLabel>
      )}
      <br />
      <Controller
        name={name}
        control={control}
        defaultValue=""
        rules={{ required: { value: required, message: `${label} is required` }, validate }}
        render={render}
      />
      {errors[name] && (
        <FormControl.Feedback type="invalid" style={{ display: "block" }}>
          {errors[name].message}
        </FormControl.Feedback>
      )}
    </>
  );
};

export default EFCroppableImageControl;
