import React from "react";
import WithUploadContext from "../../DirectUpload/UploadContext";
import UploadWithValidationControl from "./UploadWithValidationControl";
import UploadImage from "../../DirectUpload/UploadImage/UploadImage";

const EFImageUploadButtonControl = ({ control, name, children, directory }) => {
  const render = ({ field }) => {
    return (
      <UploadImage
        onFileUpload={file => field.onChange(file)}
        file={field.value?.url}
        isMediaUploaded={false}
        directory={directory}
      >
        {children}
      </UploadImage>
    );
  };

  return (
    <WithUploadContext>
      <UploadWithValidationControl control={control} name={name} defaultValue="" render={render} />
    </WithUploadContext>
  );
};

export default EFImageUploadButtonControl;
