import React from "react";
import { Controller } from "react-hook-form";
import EFHtmlInput from "../EFHtmlInput/EFHtmlInput";
import InputLabel from "../InputLabel/InputLabel";

const EFHtmlInputControl = ({ label, errors, name, control, required }) => {
  const render = ({ field }) => {
    return (
      <EFHtmlInput
        value={field.value}
        onChange={field.onChange}
        onBlur={field.onBlur}
        displayError={errors[field.name]}
        errorMessage={errors[field.name]?.message}
      />
    );
  };
  return (
    <>
      <InputLabel theme="darkColor">{label}</InputLabel>
      <Controller
        rules={{ required: { value: required, message: `${label} is required` } }}
        name={name}
        control={control}
        defaultValue=""
        render={render}
      />
    </>
  );
};

export default EFHtmlInputControl;
