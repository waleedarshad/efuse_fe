import React from "react";
import { Controller } from "react-hook-form";
import FormControl from "react-bootstrap/FormControl";
import InputLabel from "../InputLabel/InputLabel";
import EFGameSelect from "../EFGameSelect/EFGameSelect";

const EFSelectGamesControl = ({ label, errors, name, control, required, selectMultiple }) => {
  const render = ({ field }) => {
    const onChange = games => {
      const value = selectMultiple ? games.map(game => game?._id) : games?.[0]?._id;
      field.onChange(value);
    };

    return (
      <EFGameSelect
        onSelect={onChange}
        selectMultiple={selectMultiple}
        preselectedGameIds={field.value}
        gamesToRemove="aim-lab"
      />
    );
  };
  return (
    <>
      <InputLabel theme="darkColor" optional={!required}>
        {label}
      </InputLabel>
      <Controller
        name={name}
        control={control}
        defaultValue=""
        rules={{ required: { value: required, message: `${label} is required` } }}
        render={render}
      />
      {errors[name] && (
        <FormControl.Feedback type="invalid" style={{ display: "block" }}>
          {errors[name].message}
        </FormControl.Feedback>
      )}
    </>
  );
};

export default EFSelectGamesControl;
