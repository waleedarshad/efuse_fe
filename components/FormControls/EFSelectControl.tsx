import React from "react";
import { Controller, FieldErrors } from "react-hook-form";
import { Control } from "react-hook-form/dist/types/form";
import SelectBox from "../SelectBox/SelectBox";
import InputLabel from "../InputLabel/InputLabel";

interface EFSelectControlProps {
  label?: string;
  errors: FieldErrors;
  name: string;
  control: Control<any>;
  required?: boolean;
  options: {
    label: string;
    value: string;
  }[];
}

const EFSelectControl = ({ label, name, control, errors, required, options }: EFSelectControlProps) => {
  const render = ({ field }) => {
    return (
      <SelectBox
        name={field.name}
        options={options}
        size="md"
        validated={false}
        value={field.value}
        theme="whiteShadow"
        onBlur={field.onBlur}
        onChange={field.onChange}
        isValid={!errors[field.name]}
        isInvalid={errors[field.name]}
        errorMessage={errors[field.name]?.message}
        style={{ height: "42px" }}
      />
    );
  };
  return (
    <>
      {label && <InputLabel theme="darkColor">{label}</InputLabel>}
      <Controller
        name={name}
        control={control}
        defaultValue=""
        rules={{ required: { value: required, message: `${label || "Field"} is required` } }}
        render={render}
      />
    </>
  );
};

export default EFSelectControl;
