import React from "react";
import { Col } from "react-bootstrap";
import { Controller } from "react-hook-form";
import Style from "./RequiredFieldsList.module.scss";
import Switch from "../../Switch/Switch";
import InfoText from "../../InfoText/InfoText";

const EFRequiredFieldsList = ({ requiredFields, control, name }) => {
  return (
    <>
      {requiredFields.map(requiredField => (
        <Col sm={12} key={requiredField?._id}>
          <div className={Style.switchWrapper}>
            <span className={Style.switchText}>
              {requiredField?.name}
              {requiredField?.description && <InfoText instructions={requiredField?.description} label="" />}
            </span>
            <div className={Style.switch}>
              <Controller
                render={({ field }) => (
                  <Switch name={field.name} checked={field.value} value={field.value} onChange={field.onChange} />
                )}
                name={`${name}.${requiredField?._id}`}
                control={control}
                defaultValue={false}
                type="checkbox"
              />
            </div>
          </div>
        </Col>
      ))}
    </>
  );
};

export default EFRequiredFieldsList;
