import React from "react";
import { Controller } from "react-hook-form";
import InputColor from "react-input-color";
import InputLabelWithHelpTooltip from "../InputLabelWithHelpTooltip/InputLabelWithHelpTooltip";
import InputLabel from "../InputLabel/InputLabel";
import Style from "./formControls.module.scss";

const EFColorControl = ({ label, tooltip, name, control }) => {
  const render = ({ field }) => {
    return (
      <InputColor
        initialValue={field.value}
        onChange={color => field.onChange(color?.hex)}
        placement="right"
        className={Style.colorInput}
      />
    );
  };
  return (
    <>
      {tooltip && label && <InputLabelWithHelpTooltip tooltipText={tooltip} labelText={label} theme="darkColor" />}
      {label && !tooltip && <InputLabel theme="darkColor">{label}</InputLabel>}
      <Controller name={name} control={control} defaultValue="" render={render} />
    </>
  );
};

export default EFColorControl;
