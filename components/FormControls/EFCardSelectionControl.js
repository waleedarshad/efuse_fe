import React from "react";
import { Controller } from "react-hook-form";
import FormControl from "react-bootstrap/FormControl";
import InputLabel from "../InputLabel/InputLabel";
import EFCardSelection from "../EFCardSelection/EFCardSelection";

const EFCardSelectionControl = ({ label, name, control, errors, required, size, cards }) => {
  const render = ({ field }) => {
    return (
      <EFCardSelection cards={cards} onChange={field.onChange} value={field.value} size={size} name={field.name} />
    );
  };
  return (
    <>
      {label && <InputLabel theme="darkColor">{label}</InputLabel>}
      <Controller
        name={name}
        control={control}
        defaultValue=""
        rules={{ required: { value: required, message: `${label || "Field"} is required` } }}
        render={render}
      />
      {errors[name] && (
        <FormControl.Feedback type="invalid" style={{ display: "block" }}>
          {errors[name].message}
        </FormControl.Feedback>
      )}
    </>
  );
};

export default EFCardSelectionControl;
