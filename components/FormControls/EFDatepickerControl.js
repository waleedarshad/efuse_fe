import React from "react";
import { Controller } from "react-hook-form";
import InputLabel from "../InputLabel/InputLabel";
import InfoText from "../InfoText/InfoText";
import DatePickerInput from "../DatePickerInput/DatePickerInput";

const EFDatepickerControl = ({ label, errors, instructions, name, control, required, validate }) => {
  const render = ({ field }) => {
    return (
      <DatePickerInput
        date={field.value || ""}
        dateHandler={date => field.onChange(date.format())}
        numberOfMonths={1}
        invalid={errors[field.name]}
        errorMessage={errors[field.name]?.message}
      />
    );
  };
  return (
    <>
      {instructions ? (
        <InfoText
          theme="internal"
          label={<InputLabel theme="darkColor">{label}</InputLabel>}
          instructions={instructions}
        />
      ) : (
        <InputLabel theme="darkColor">{label}</InputLabel>
      )}
      <br />
      <Controller
        name={name}
        control={control}
        defaultValue=""
        rules={{ required: { value: required, message: `${label} is required` }, validate }}
        render={render}
      />
    </>
  );
};

export default EFDatepickerControl;
