import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import Cookies from "js-cookie";
import Style from "./DemoButton.module.scss";
import COOKIES from "../../common/cookies";
import EFRectangleButton from "../Buttons/EFRectangleButton/EFRectangleButton";

const DemoButton = () => {
  const publicText = "Public (Launched)";
  const employeeText = "Internal (Testing)";
  const demoCookie = Cookies.get(COOKIES.DEMO_STATE);
  const currentUser = useSelector(state => state.auth.currentUser);
  const [buttonText, setButtonText] = useState("");
  const [buttonBackground, setButtonBackground] = useState(Style.demoButtonInital);

  const handleButtonClick = () => {
    Cookies.set(COOKIES.DEMO_STATE, returnCookieState());
    setButtonBackground(returnButtonBackground);
    setButtonText(returnButtonText);
    window.location.reload();
  };

  const returnCookieState = () => {
    return demoCookie === COOKIES.SHOW_LATEST_FEATURES ? COOKIES.SHOW_PUBLIC_SITE : COOKIES.SHOW_LATEST_FEATURES;
  };

  const returnButtonText = () => {
    return demoCookie === COOKIES.SHOW_LATEST_FEATURES ? employeeText : publicText;
  };

  const returnButtonBackground = () => {
    return demoCookie === COOKIES.SHOW_LATEST_FEATURES ? Style.demoButtonEmployee : Style.demoButtonPublic;
  };

  useEffect(() => {
    if (demoCookie === undefined) {
      Cookies.set(COOKIES.DEMO_STATE, COOKIES.SHOW_LATEST_FEATURES);
    }
    setButtonBackground(returnButtonBackground);
    setButtonText(returnButtonText);
  });

  if (!currentUser?._id || !currentUser?.roles?.includes("employee")) {
    return <></>;
  }

  return (
    <div className={Style.demoButton}>
      <EFRectangleButton text={buttonText} className={`${buttonBackground}`} onClick={handleButtonClick} />
    </div>
  );
};

export default DemoButton;
