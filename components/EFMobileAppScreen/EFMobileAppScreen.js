import React, { useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faStar } from "@fortawesome/pro-solid-svg-icons";

import Style from "./EFMobileAppScreen.module.scss";
import EFRectangleButton from "../Buttons/EFRectangleButton/EFRectangleButton";

const EFMobileAppScreen = () => {
  const TEXT = {
    hero: "Continue your Journey on the App",
    subtitle: "For the best experience, download the eFuse mobile app today!",
    buttonLabel: "DOWNLOAD APP NOW"
  };

  const DOWNLOAD_LINK = "https://efuse.onelink.me/OZoN/download";

  useEffect(() => {
    analytics.track("APP_DOWNLOAD_BANNER_SHOW", TEXT);
  }, []);

  return (
    <div className={Style.backgroundImage}>
      <div className={Style.centeredSection}>
        <div className={Style.heroText}>{TEXT.hero}</div>
        <div className={Style.subText}>{TEXT.subtitle}</div>
        <div className={Style.starSection}>
          <FontAwesomeIcon icon={faStar} className={Style.starIcon} />
          <FontAwesomeIcon icon={faStar} className={Style.starIcon} />
          <FontAwesomeIcon icon={faStar} className={Style.starIcon} />
          <FontAwesomeIcon icon={faStar} className={Style.starIcon} />
          <FontAwesomeIcon icon={faStar} className={Style.starIcon} />
          <span className={Style.reviewCountText}>15k</span>
        </div>
      </div>
      <div className={Style.buttonContainer}>
        <img
          src="https://cdn.efuse.gg/static/images/app_icon.png"
          className={Style.appIconImage}
          alt="efuse app icon"
        />
        <div className={Style.downloadButton}>
          <EFRectangleButton
            onClick={() => {
              analytics.track("APP_DOWNLOAD_BANNER_CLICKED", TEXT, () => {
                document.location.href = DOWNLOAD_LINK;
              });
            }}
            text={TEXT.buttonLabel}
            width="fullWidth"
            colorTheme="white"
          />
        </div>
      </div>
    </div>
  );
};

export default EFMobileAppScreen;
