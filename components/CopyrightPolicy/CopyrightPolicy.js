import React from "react";
import Style from "./CopyrightPolicy.module.scss";

const CopyrightPolicy = () => {
  return (
    <pre style={{ whiteSpace: "pre-wrap" }}>
      <h1 className={Style.header}>eFuse’s Copyright Policy</h1>
      <p className={Style.subHeader}>
        <strong>What types of copyright complaints does eFuse respond to?</strong>
      </p>
      <p>
        eFuse responds to copyright&nbsp;complaints submitted under the Digital Millennium Copyright Act (“DMCA”).
        Section 512 of the DMCA outlines the statutory requirements necessary for formally reporting copyright
        infringement, as well as providing instructions on how an affected party can appeal a removal by submitting a
        compliant counter-notice.
      </p>
      <p>
        eFuse will respond to reports of alleged copyright infringement, such as allegations concerning the unauthorized
        use of a copyrighted image as a profile or header photo, allegations concerning the unauthorized use of a
        copyrighted video or image uploaded to our website or links that include allegedly infringing materials. Note
        that not all unauthorized uses of copyrighted materials are infringements.
      </p>
      <p className={Style.subHeader}>
        <strong>Am I a copyright holder? How do I know?</strong>
      </p>
      <p>
        If you are unsure whether you hold rights to a particular work, please consult an attorney or another adviser as
        eFuse cannot provide legal advice. There are plenty of resources to learn more about copyright law including
        &nbsp;<a href="http://copyright.gov/">http://copyright.gov</a>,{" "}
        <a href="https://lumendatabase.org/">https://lumendatabase.org/</a>, and{" "}
        <a href="http://www.eff.org/issues/bloggers/legal/liability/IP">
          http://www.eff.org/issues/bloggers/legal/liability/IP
        </a>
        , to name a few.
      </p>
      <p className={Style.subHeader}>
        <strong>What to consider before submitting a copyright complaint</strong>
      </p>
      <p>
        Before submitting a copyright complaint to us, please consider whether or not the use could be considered fair
        use.
      </p>
      <p>
        If you have considered fair use, and you still wish to continue with a copyright complaint, you may want to
        first reach out to the user in question to see if you can resolve the matter directly with the user. You can
        reply to the user’s message or send the user a Direct Message and ask for them to remove your copyrighted
        content without having to contact eFuse.&nbsp;
      </p>
      <p>
        Prior to submitting a formal complaint with eFuse, please be aware that under 17 U.S.C. § 512(f), you may be
        liable for any damages, including costs and attorneys’ fees incurred by us or our users, if you knowingly
        materially misrepresent that material or activity is infringing. If you are unsure whether the material you are
        reporting is in fact infringing, you may wish to contact an attorney before filing a notification with us.
      </p>
      <p>
        <strong>Note</strong>
        <strong>:&nbsp;</strong>In general, the photographer and NOT the subject of a photograph is the actual rights
        holder of the resulting photograph. If you’re unsure whether or not you own the copyrights to a work, or if
        you’re infringing upon someone else’s work, please consult an attorney or another advisor.
      </p>

      <p className={Style.subHeader}>
        <strong>What information do you need to process a copyright complaint?</strong>
      </p>
      <p>
        To submit a notice of claimed copyright infringement, you will need to provide us with the following
        information:
      </p>
      <ol>
        <li>
          <p>
            A physical or electronic signature (typing your full name will suffice) of the copyright owner or a person
            authorized to act on their behalf;
          </p>
        </li>
        <li>
          <p>
            Identification of the copyrighted work claimed to have been infringed (e.g., a link to your original work or
            clear description of the materials allegedly being infringed upon);
          </p>
        </li>
        <li>
          <p>
            Identification of the infringing material and information reasonably sufficient to permit eFuse to locate
            the material on our website or services;
          </p>
        </li>
        <li>
          <p>Your contact information, including your address, telephone number, and an email address;</p>
        </li>
        <li>
          <p>
            A statement that you have a good faith belief that the use of the material in the manner asserted is not
            authorized by the copyright owner, its agent, or the law; and
          </p>
        </li>
        <li>
          <p>
            A statement that the information in the complaint is accurate, and, under penalty of perjury, that you are
            authorized to act on behalf of the copyright owner.
          </p>
        </li>
      </ol>
      <p>
        If you are reporting the content of a post, please give us a direct link to the post. Or please specify if the
        alleged infringement is in the header, avatar, etc. A LINK TO A PROFILE PAGE IS INSUFFICIENT FOR eFUSE TO
        IDENTIFY INFRINGING MATERIALS.
      </p>
      <p className={Style.subHeader}>
        <strong>How do I file a copyright complaint?</strong>
      </p>
      <p>You can report alleged copyright infringement by filling out a Copyright Complaint.</p>
      <p>
        Filing a DMCA complaint is the start of a pre-defined legal process. Your complaint will be reviewed for
        accuracy, validity, and completeness. If your complaint has satisfied these requirements, we will take action on
        your request - which includes forwarding a full copy of your notice (including your name, address, phone and
        email address) to the user(s) who posted the allegedly infringing material in question.
      </p>
      <p>
        If you are concerned about your contact information being forwarded, you may wish to use an agent to report for
        you.
      </p>
      <p>
        Please be aware that under 17 U.S.C. § 512(f), you may be liable for any damages, including costs and attorneys’
        fees incurred by us or our users, if you knowingly materially misrepresent that material or activity is
        infringing. If you are unsure whether the material you are reporting is in fact infringing, you may wish to
        contact an attorney before filing a copyright complaint.
      </p>
      <p className={Style.subHeader}>
        <strong>How are claims processed?</strong>
      </p>
      <p>
        We process copyright complaints in the order in which they are received. Once you&rsquo;ve submitted your
        complaint, we will email you a confirmation. If you do not receive a confirmation that means we did not receive
        your complaint and you should re-submit your complaint. However, please note, submitting duplicate copyright
        complaints will result in a delay in processing.
      </p>
      <p>
        If we decide to remove or disable access to the material, we will notify the affected user(s) and provide them
        with a full copy of the reporter’s complaint (including the provided contact information) along with
        instructions on how to file a counter-notice.
      </p>
      <p className={Style.subHeader}>
        <strong>What information gets forwarded to the reported user(s)?&nbsp;</strong>
      </p>
      <p>
        If we remove or disable access to the materials reported in a copyright complaint, the reported user(s) will
        receive a copy of the complaint, including the reporter’s full name, email, street address, and any other
        information included in the complaint.&nbsp;
      </p>
      <p>
        If you are uncomfortable sharing your contact information with the reported user(s), you may wish to consider
        appointing an agent to submit your DMCA notice on your behalf. Your agent will be required to submit the DMCA
        notice with valid contact information, and identify you as the content owner that they are representing.&nbsp;
      </p>
      <p className={Style.subHeader}>
        <strong>What happens next?</strong>
      </p>
      <p>
        eFuse’s response to copyright complaints may include the removal or restriction of access to allegedly
        infringing material. If we remove or restrict access to user content in response to a copyright complaint, eFuse
        will make a good faith effort to contact the affected account holder with information concerning the removal or
        restriction of access, including a full copy of the complaint, along with instructions for filing a
        counter-notice.
      </p>
      <p>
        If you’ve not yet received a copy of the copyright complaint regarding the content removed from your account,
        please respond to the support ticket we sent you.
      </p>
      <p className={Style.subHeader}>
        <strong>My content was removed from EFuse</strong>
      </p>
      <p className={Style.subHeader}>
        <strong>Why did I receive a copyright complaint?&nbsp;</strong>
      </p>
      <p>
        If you receive a copyright complaint, it means that access to the content described in the complaint has been
        restricted. Please take the time to read through our correspondence to you, which includes information on the
        complaint we received as well as instructions on how to file a counter-notice. Please ensure that you are
        monitoring the email address associated with your eFuse account.
      </p>
      <p>
        <strong>Tip:</strong> Removing the material reported in a copyright complaint will not resolve that complaint.
      </p>
      <p className={Style.subHeader}>
        <strong>What if I want to contest the takedown?&nbsp;</strong>
      </p>
      <p>
        If you believe that the materials reported in the copyright&nbsp;complaints were misidentified or removed in
        error, you may send us a counter-notification(s). A counter-notice is a request for eFuse to reinstate the
        removed material, and it has legal consequences. Alternatively, you may be able to seek a retraction of the
        copyright complaint from the reporter.
      </p>
      <p className={Style.subHeader}>
        <strong>How do I seek a retraction?</strong>
      </p>
      <p>
        The DMCA complaint you received includes the contact information of the reporter. You may want to reach out and
        ask them to retract their notice. &nbsp;The reporter can send retractions to copyright@eFuse.com , and should
        include: (1) identification of the material that was disabled, and (2) a statement that the reporter would like
        to retract their DMCA notice. This is the fastest and most efficient means of resolving an unresolved copyright
        complaint. A retraction is at the sole discretion of the original reporter.
      </p>
      <p className={Style.subHeader}>
        <strong>When should I file a counter-notice?</strong>
      </p>
      <p>
        A counter-notice is a request for eFuse to reinstate the removed material, and is the start of a legal process
        that has legal consequences. &nbsp;For example, submitting a counter notice indicates that you consent to the
        jurisdiction of a U.S. Federal court and that you consent to the disclosure of your personal information to the
        reporter.
      </p>
      <p>
        With these considerations in mind, you may file a counter-notice if you believe that this material was
        misidentified, or you have a good faith belief that the material should not have been removed. &nbsp;If you’re
        unsure whether or not you should file a counter-notice, you may want to consult with an attorney.
      </p>
      <p>
        <strong>Tip:</strong> Re-posting material removed in response to a copyright complaint may result in permanent
        account suspension. If you believe the content was removed in error, please file a counter-notice rather than
        re-posting the material.&nbsp;
      </p>
      <p className={Style.subHeader}>
        <strong>What information do you need to process a counter-notice?</strong>
      </p>
      <p>To submit a counter-notice, you will need to provide us with the following information:</p>
      <ol>
        <li>
          <p>A physical or electronic signature (typing your full name will suffice);</p>
        </li>
        <li>
          <p>
            Identification of the material that has been removed or to which access has been disabled and the location
            at which the material appeared before it was removed or access to it was disabled (the description from the
            copyright notice will suffice);
          </p>
        </li>
        <li>
          <p>
            A statement under penalty of perjury that you have a good faith belief that the material was removed or
            disabled as a result of mistake or misidentification of the material to be removed or disabled; and
          </p>
        </li>
        <li>
          <p>
            Your name, address, and telephone number, and a statement that you consent to the jurisdiction of the
            Federal District Court for the judicial district in which the address is located, or if your address is
            outside of the United States, for any judicial district in which eFuse may be found, and that you will
            accept service of process from the person who provided notification under subsection (c)(1)(C) or an agent
            of such person.
          </p>
        </li>
      </ol>
      <p>
        To submit a counter-notice, please respond to our original email notification of the removal and include the
        required information in the body of your reply as we discard all attachments for security reasons.
      </p>
      <p className={Style.subHeader}>
        <strong>What happens after I submit a counter-notice?</strong>
      </p>
      <p>
        Upon receipt of a valid counter-notice, we will promptly forward a copy to the person who filed the original
        notice. This means that the contact information that is submitted in your counter-notice will be shared to the
        person who filed the original notice.&nbsp;
      </p>
      <p>
        If the copyright owner disagrees that the content was removed in error or misidentification, they may pursue
        legal action against you. &nbsp;If we do not receive notice within 10 business days that the original reporter
        believes that the takedown was correct or is seeking a court order to prevent further infringement of the
        material at issue, we may replace or cease disabling access to the material that was removed.
      </p>
      <p>We cannot offer any legal advice. Should you have questions, please consult an attorney.</p>
      <p className={Style.subHeader}>
        <strong>Filing a copyright complaint or counter-notice is serious business!</strong>
      </p>
      <p>
        Please think twice before submitting a claim or counter-notice, especially if you are unsure whether you are the
        actual rights holder or authorized to act on a rights holder’s behalf. There are legal and financial
        consequences for fraudulent and/or bad faith submissions. Please be sure that you are the actual rights holder,
        or that you have a good faith belief that the material was removed in error, and that you understand the
        repercussions of submitting a false claim.
      </p>
      <p className={Style.subHeader}>
        <strong>What happens if my account receives multiple copyright complaints?&nbsp;</strong>
      </p>
      <p>
        If multiple copyright complaints are received eFuse may lock accounts or take other actions to warn repeat
        violators. These warnings may vary across eFuse’s services. &nbsp;Under appropriate circumstances we may suspend
        user accounts. However, we may take retractions and counter-notices into account when applying our repeat
        infringer policy.
      </p>
    </pre>
  );
};

export default CopyrightPolicy;
