import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft, faChevronRight } from "@fortawesome/pro-solid-svg-icons";
import { forwardRef, useEffect, useImperativeHandle, useState } from "react";
import ReactPaginate from "react-paginate";
import Style from "./EFPaginationBar.module.scss";

// pass a ref prop to access the goToPage(pageNumber) function
const EFPaginationBar = ({ totalPages, onPageChange, marginPages, pageRange }, ref) => {
  const [currentPage, setCurrentPage] = useState(0);

  useImperativeHandle(ref, () => ({
    goToPageNumber: num => {
      setCurrentPage(num - 1);
    }
  }));

  useEffect(() => {
    onPageChange(currentPage + 1);
  }, [currentPage]);

  return (
    <ReactPaginate
      forcePage={currentPage}
      previousLabel={<FontAwesomeIcon icon={faChevronLeft} className={Style.chevron} />}
      nextLabel={<FontAwesomeIcon icon={faChevronRight} className={Style.chevron} />}
      breakLabel="..."
      breakClassName="break-me"
      pageCount={totalPages}
      marginPagesDisplayed={marginPages}
      pageRangeDisplayed={pageRange}
      onPageChange={page => setCurrentPage(page.selected)}
      disabledClassName={Style.paginationDisabled}
      containerClassName={`pagination ${Style.paginationContainer}`}
      subContainerClassName="pages pagination"
      activeClassName={Style.paginationActive}
    />
  );
};

export default forwardRef(EFPaginationBar);
