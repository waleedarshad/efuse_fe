import React from "react";
import EFPaginationBar from "./EFPaginationBar";

export default {
  title: "Shared/PaginationBar",
  component: EFPaginationBar,
  argTypes: {
    totalPages: {
      control: {
        type: "number"
      }
    },
    marginPages: {
      control: {
        type: "number"
      }
    },
    pageRange: {
      control: {
        type: "number"
      }
    }
  }
};

// eslint-disable-next-line react/jsx-props-no-spreading
const Story = args => <EFPaginationBar {...args} />;

export const Bar = Story.bind({});
Bar.args = {
  totalPages: 20,
  marginPages: 1,
  pageRange: 2
};
