import { faPlus } from "@fortawesome/pro-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import EFCard from "../Cards/EFCard/EFCard";
import Style from "./EFCreateCard.module.scss";

interface EFCreateCardProps {
  text: string;
  onClick?: () => void;
  size?: "small" | "medium";
}

const EFCreateCard: React.FC<EFCreateCardProps> = ({ text, onClick = () => {}, size = "medium" }) => {
  return (
    <div className={size ? Style[size] : ""}>
      <EFCard
        widthTheme="fullWidth"
        heightTheme="fullHeight"
        shadow="none"
        hoverEffect="default"
        noBorder
        onClick={onClick}
      >
        <div className={Style.body}>
          <div className={Style.icon}>
            <FontAwesomeIcon icon={faPlus} />
          </div>
          <div className={Style.text} aria-hidden>
            {text}
          </div>
        </div>
      </EFCard>
    </div>
  );
};

export default EFCreateCard;
