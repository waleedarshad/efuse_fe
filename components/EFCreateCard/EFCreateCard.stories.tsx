import React from "react";
import EFCreateCard from "./EFCreateCard";

export default {
  title: "Cards/EFCreateCard",
  component: EFCreateCard
};

const Story = args => <EFCreateCard {...args} />;

export const Default = Story.bind({});

Default.args = {
  text: "Create Orgg"
};
