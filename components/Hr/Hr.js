import Style from "./Hr.module.scss";

const Hr = ({ customClass }) => <hr className={`${Style.hr} ${customClass}`} />;

export default Hr;
