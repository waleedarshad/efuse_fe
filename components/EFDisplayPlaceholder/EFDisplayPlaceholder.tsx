import styled from "styled-components";
import React from "react";
import colors from "../../styles/sharedStyledComponents/colors";

const EFDisplayPlaceholder = ({ text }) => (
  <PlaceholderContainer>
    <PlaceholderText>{text}</PlaceholderText>
  </PlaceholderContainer>
);

const PlaceholderContainer = styled.div`
  height: 500px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const PlaceholderText = styled.div`
  font-weight: 500;
  font-size: 18px;
  color: ${colors.textStrong};
`;

export default EFDisplayPlaceholder;
