import React from "react";
import EFOrganizationInviteSection from "./EFOrganizationInviteSection";

export default {
  title: "EFOrganizationInviteSection/EFOrganizationInviteSection",
  component: EFOrganizationInviteSection,
  argTypes: {
    inviteCode: {
      control: {
        type: "text"
      }
    },
    numberOfSignups: {
      control: {
        type: "number"
      }
    },
    titleText: {
      control: {
        type: "text"
      }
    },
    bodyText: {
      control: {
        type: "text"
      }
    },
    buttonText: {
      control: {
        type: "text"
      }
    },
    buttonColorTheme: {
      control: {
        type: "text"
      }
    }
  }
};

// eslint-disable-next-line react/jsx-props-no-spreading
const Story = args => <EFOrganizationInviteSection {...args} />;

export const InviteSection = Story.bind({});
InviteSection.args = {
  inviteCode: "XZL2V",
  numberOfSignups: 2,
  titleText: "Please follow",
  bodyText: "Please send invites to make people follow. I need followers to start a cult",
  buttonText: "Invite",
  buttonColorTheme: "primary"
};
