import React from "react";
import { useDispatch } from "react-redux";

import { shareLinkModal } from "../../store/actions/shareAction";
import { SHARE_LINK_KINDS } from "../ShareLinkModal/ShareLinkModal";
import EFCard from "../Cards/EFCard/EFCard";
import EFRectangleButton from "../Buttons/EFRectangleButton/EFRectangleButton";
import Style from "./EFOrganizationInviteSection.module.scss";

const EFOrganizationInviteSection = ({
  inviteCodes,
  numberOfSignups,
  titleText,
  bodyText,
  buttonText,
  buttonColorTheme
}) => {
  const dispatch = useDispatch();
  return (
    <div className={Style.inviteWrapper}>
      <EFCard>
        <div className={Style.contentWrapper}>
          <div className={Style.titleDescriptionContainer}>
            <div className={Style.title}>{titleText}</div>
            <div className={Style.description}>{bodyText}</div>
            <div className={Style.signupCount}>{numberOfSignups} sign-ups using this invite code.</div>
          </div>
          <div className={Style.inviteButtonContainer}>
            <EFRectangleButton
              text={buttonText}
              colorTheme={buttonColorTheme}
              onClick={() => {
                dispatch(
                  shareLinkModal(
                    true,
                    "Share Organization Invite Link",
                    `https://efuse.gg/i/${inviteCodes}`,
                    SHARE_LINK_KINDS.ORGANIZATION
                  )
                );
              }}
            />
          </div>
        </div>
      </EFCard>
    </div>
  );
};

export default EFOrganizationInviteSection;
