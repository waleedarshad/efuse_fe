import Style from "./CharacterCounter.module.scss";
/** 
  @category Components
  @desc Charater Counter component that return remaing length of charaters from its maximum value and style it
  @param {number} maxLength length 
  @param {number} currentLength of the charaters
*/

const CharacterCounter = ({ maxLength = 0, currentLength = 0 }) => {
  const count = maxLength - currentLength;
  return <span className={count < 0 ? Style.danger : Style.normal}>{count}</span>;
};

export default CharacterCounter;
