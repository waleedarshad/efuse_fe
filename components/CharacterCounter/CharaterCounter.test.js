import React from "react";
import { mount } from "enzyme";
import CharacterCounter from "./CharacterCounter";

describe("Charater Counter", () => {
  it("render Charater Counter with default value zero", () => {
    const subject = mount(<CharacterCounter />);
    expect(
      subject
        .find("span")
        .at(0)
        .text()
    ).toEqual("0");
  });

  it("expect counter to be negative", () => {
    const subject = mount(<CharacterCounter maxLength={5} currentLength={10} />);
    const counter = parseInt(subject.find("span").text(), 10);
    expect(counter).toBeLessThan(0);
  });

  it("expect counter to be greater than zero", () => {
    const subject = mount(<CharacterCounter maxLength={15} currentLength={10} />);
    const counter = parseInt(subject.find("span").text(), 10);
    expect(counter).toBeGreaterThan(0);
  });

  it("expect styling to be danger due to counter is less than zero", () => {
    const subject = mount(<CharacterCounter maxLength={5} currentLength={10} />);
    expect(subject.find("span").prop("className")).toEqual("danger");
  });
  it("expect styling to be normal as counter is greater than zero", () => {
    const subject = mount(<CharacterCounter maxLength={15} currentLength={10} />);
    expect(subject.find("span").prop("className")).toEqual("normal");
  });
});
