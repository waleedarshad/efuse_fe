import React, { useEffect } from "react";
import isEmpty from "lodash/isEmpty";
import { useSelector, useDispatch } from "react-redux";
import PropTypes from "prop-types";
import SignupModal from "../SignupModal/SignupModal";

import EFRectangleButton from "../Buttons/EFRectangleButton/EFRectangleButton";
import { checkIfFollower, follow, unfollow } from "../../store/actions/followerActions";
import EFIsLoggedIn from "../EFIsLoggedIn/EFIsLoggedIn";

const Follow = ({
  id,
  bypass,
  fromBio,
  renderAs,
  renderFollower,
  isFollowedFromHash,
  notAFriend,
  unfollowOveride,
  followOveride
}) => {
  const currentUser = useSelector(state => state.auth.currentUser);
  const user = useSelector(state => state.user.currentUser);
  const isFollowed = useSelector(state => state.followers.isFollowed);
  const disable = useSelector(state => state.followers.disable);
  const dispatch = useDispatch();
  let content = null;

  useEffect(() => {
    if (id && !bypass && currentUser?.id) {
      dispatch(checkIfFollower(id));
    }
  }, []);

  const followUser = () => {
    let isCurrentUser = currentUser.id === id;
    if (fromBio) isCurrentUser = fromBio;
    dispatch(follow(id, isCurrentUser, renderAs));
  };

  const unfollowUser = () => {
    let isCurrentUser = currentUser.id === id;
    if (fromBio) isCurrentUser = fromBio;
    dispatch(unfollow(id, isCurrentUser, currentUser.id, renderAs));
  };

  if (!bypass && (id === undefined || (currentUser && currentUser.id === id))) {
    content = "";
  } else if (renderFollower === false || (isFollowed && isEmpty(renderAs)) || isFollowedFromHash) {
    content = <EFRectangleButton onClick={unfollowOveride || unfollowUser} disabled={disable} text="Following" />;
  } else if (!isFollowed || notAFriend) {
    content = (
      <EFIsLoggedIn
        altComponent={
          <SignupModal
            title={`Create an account to follow ${user.name}.`}
            message={`Never miss an update from ${user.name}. Sign up today!`}
          >
            <EFRectangleButton disabled={disable} text="Follow" />
          </SignupModal>
        }
      >
        <EFRectangleButton onClick={followOveride || followUser} disabled={disable} text="Follow" />
      </EFIsLoggedIn>
    );
  }
  return <>{content}</>;
};

export default Follow;

Follow.propTypes = {
  id: PropTypes.string,
  bypass: PropTypes.bool,
  renderFollower: PropTypes.bool
};
