import React from "react";
import Style from "./EFCloutCard.module.scss";
import EFCloutCardIcons from "../EFCloutCardIcons/EFCloutCardIcons";
import EFCloutCardContent from "../EFCloutCardContent/EFCloutCardContent";
import EFCloutCardButtons from "../EFCloutCardButtons/EFCloutCardButtons";
import EFCloutProvider from "../EFCloutProvider/EFCloutProvider";

const EFCloutCard = ({ socialType, accountData, owner }) => {
  return (
    <div className={Style.mainContainer}>
      <EFCloutProvider socialType={socialType} accountData={accountData} owner={owner}>
        <EFCloutCardIcons />
        <EFCloutCardContent />
        <EFCloutCardButtons />
      </EFCloutProvider>
    </div>
  );
};

export default EFCloutCard;
