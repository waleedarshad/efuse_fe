import { mount } from "enzyme";
import React from "react";
import { faTwitch } from "@fortawesome/free-brands-svg-icons";
import EFCloutCard from "./EFCloutCard";
import EFCloutCardIcons from "../EFCloutCardIcons/EFCloutCardIcons";
import EFCloutCardContent from "../EFCloutCardContent/EFCloutCardContent";
import EFCloutCardButtons from "../EFCloutCardButtons/EFCloutCardButtons";
import { UNLINK_TWITTER_ACCOUNT_FAILURE, UNLINK_TWITTER_ACCOUNT_SUCCESS } from "../../../store/actions/types";

jest.mock("react-redux", () => ({
  useDispatch: () => jest.fn()
}));

describe("EFCloutCard", () => {
  let subject;

  const socialType = {
    socialType: "twitter",
    icon: faTwitch,
    socialTitle: "twitter",
    linkText: "Twitter",
    supporterTitle: "Followers",
    buttonText: "View",
    accountUrl: "https://www.twitter.com/",
    actionTypes: {
      unlinkSuccess: UNLINK_TWITTER_ACCOUNT_SUCCESS,
      unlinkFailure: UNLINK_TWITTER_ACCOUNT_FAILURE
    }
  };

  const accountData = {
    accountLinked: true,
    accountInfo: {
      accountName: "coolUsername",
      supporterCount: 123
    }
  };

  const owner = {
    owner: true,
    ownerKind: "organization",
    ownerId: "555"
  };

  beforeEach(() => {
    subject = mount(<EFCloutCard socialType={socialType} accountData={accountData} owner={owner} />);
  });

  it("renders the correct icon component", () => {
    expect(subject.find(EFCloutCardIcons)).toHaveLength(1);
  });

  it("renders the correct content", () => {
    expect(subject.find(EFCloutCardContent)).toHaveLength(1);
  });

  it("renders the correct buttons component", () => {
    expect(subject.find(EFCloutCardButtons)).toHaveLength(1);
  });
});
