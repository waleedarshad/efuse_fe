import React, { useContext } from "react";
import Style from "./EFCloutCardContent.module.scss";
import { EFCloutContext } from "../EFCloutProvider/EFCloutProvider";

const EFCloutCardContent = () => {
  const context = useContext(EFCloutContext);

  const usernameSymbol = context.accountNameSymbol ? context.accountNameSymbol : "";

  const text = context.accountLinked
    ? `${usernameSymbol}${context.accountInfo?.accountName}`
    : `Link your ${context.linkText} to show off your clout!`;

  return (
    <>
      <div>
        <p className={Style.titleText}>{context.socialTitle}</p>
        <p className={Style.linkText}>{text}</p>
      </div>
      {context.accountLinked && (
        <div className={Style.supporterContainer}>
          <p className={Style.titleText}>{context.supporterTitle}</p>
          <p className={Style.linkText}>{context.accountInfo?.supporterCount}</p>
        </div>
      )}
    </>
  );
};

export default EFCloutCardContent;
