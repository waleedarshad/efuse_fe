import { mount } from "enzyme";
import React from "react";
import EFCloutCardContent from "./EFCloutCardContent";
import { EFCloutContext } from "../EFCloutProvider/EFCloutProvider";

describe("EFCloutCardContent", () => {
  it("renders the correct social title", () => {
    const subject = mount(
      <EFCloutContext.Provider value={{ socialTitle: "Discord", linkText: "Discord" }}>
        <EFCloutCardContent />
      </EFCloutContext.Provider>
    );

    expect(
      subject
        .find("p")
        .at(0)
        .text()
    ).toEqual("Discord");
  });

  it("renders the correct link text by default", () => {
    const subject = mount(
      <EFCloutContext.Provider value={{ socialTitle: "Discord", linkText: "Discord" }}>
        <EFCloutCardContent />
      </EFCloutContext.Provider>
    );

    expect(
      subject
        .find("p")
        .at(1)
        .text()
    ).toEqual("Link your Discord to show off your clout!");
  });

  it("renders the correct link text when account is linked", () => {
    const subject = mount(
      <EFCloutContext.Provider
        value={{
          socialTitle: "Discord",
          linkText: "Discord",
          accountLinked: true,
          accountInfo: { accountName: "myCoolUserName" }
        }}
      >
        <EFCloutCardContent />
      </EFCloutContext.Provider>
    );

    expect(
      subject
        .find("p")
        .at(1)
        .text()
    ).toEqual("myCoolUserName");
  });
});
