import { mount } from "enzyme";
import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEnvelope } from "@fortawesome/pro-solid-svg-icons";

import EFCloutCardIcons from "./EFCloutCardIcons";
import { EFCloutContext } from "../EFCloutProvider/EFCloutProvider";

describe("EFCloutCardIcons", () => {
  const subject = mount(
    <EFCloutContext.Provider value={{ icon: faEnvelope, socialType: "instagram" }}>
      <EFCloutCardIcons />
    </EFCloutContext.Provider>
  );

  it("renders the correct icons", () => {
    expect(
      subject
        .find(FontAwesomeIcon)
        .at(0)
        .prop("icon")
    ).toEqual(faEnvelope);
    expect(
      subject
        .find(FontAwesomeIcon)
        .at(0)
        .prop("className")
    ).toEqual("topIcon instagram");
    expect(
      subject
        .find(FontAwesomeIcon)
        .at(1)
        .prop("icon")
    ).toEqual(faEnvelope);
  });
});
