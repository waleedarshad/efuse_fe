import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useContext } from "react";
import Style from "./EFCloutCardIcons.module.scss";
import { EFCloutContext } from "../EFCloutProvider/EFCloutProvider";

const EFCloutCardIcons = () => {
  const context = useContext(EFCloutContext);

  return (
    <>
      <div>
        <FontAwesomeIcon icon={context.icon} className={`${Style.topIcon} ${Style[context.socialType]}`} />
      </div>
      <div className={Style.backgroundIconContainer}>
        <FontAwesomeIcon icon={context.icon} className={Style.backgroundIcon} />
      </div>
    </>
  );
};

export default EFCloutCardIcons;
