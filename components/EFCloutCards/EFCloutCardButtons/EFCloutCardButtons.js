import React, { useContext, useEffect } from "react";
import { useDispatch } from "react-redux";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";
import Style from "./EFCloutCardButtons.module.scss";
import { EFCloutContext } from "../EFCloutProvider/EFCloutProvider";
import EFCloutUnlinkButton from "./EFCloutUnlinkButton/EFCloutUnlinkButton";
import {
  setSocialMediaAccountInfoFromSocket,
  socialMediaLinkingFailure,
  startAuthenticationFlow
} from "../../../store/actions/socialLinkingActions/socialLinkingActions";
import { getSocket } from "../../../websockets";

const EFCloutCardButtons = () => {
  const dispatch = useDispatch();
  const socket = getSocket();
  const context = useContext(EFCloutContext);

  const buttonText = context.accountLinked ? context.buttonText : "Link";

  const isOwnerAndAccountIsLinked = context.owner && context.accountLinked;

  const navigateToAccountPage = () => {
    analytics.track(`EXTERNAL_ACCOUNT_VIEW_${context.socialType.toUpperCase()}`, {
      location: context.analyticsLocation,
      type: context.ownerKind
    });
    window.open(context.accountUrl);
  };

  const openAuthenticationWindow = () => {
    analytics.track(`EXTERNAL_ACCOUNT_OPEN_AUTH_WINDOW_${context.socialType.toUpperCase()}`, {
      location: context.analyticsLocation,
      type: context.ownerKind
    });

    startAuthenticationFlow(context);
  };

  useEffect(() => {
    socket.on(context.socketTitle, payload => {
      if (payload.status === "success") {
        dispatch(setSocialMediaAccountInfoFromSocket(payload, context));
      } else {
        dispatch(socialMediaLinkingFailure(context));
      }
    });

    return () => {
      socket.close();
    };
  }, []);

  return (
    <div>
      {isOwnerAndAccountIsLinked && <EFCloutUnlinkButton />}
      <div className={Style.linkButtonContainer}>
        <EFRectangleButton
          buttonType="button"
          colorTheme="light"
          shadowTheme="medium"
          size="medium"
          fontWeightTheme="bold"
          fontCaseTheme="upperCase"
          text={buttonText}
          width="fullWidth"
          onClick={context.accountLinked ? () => navigateToAccountPage() : () => openAuthenticationWindow()}
        />
      </div>
    </div>
  );
};

export default EFCloutCardButtons;
