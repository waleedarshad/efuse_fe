import React from "react";
import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import userEvent from "@testing-library/user-event";
import EFCloutCardButtons from "./EFCloutCardButtons";
import { startAuthenticationFlow } from "../../../store/actions/socialLinkingActions/socialLinkingActions";
import { EFCloutContext } from "../EFCloutProvider/EFCloutProvider";
import SOCIAL_LINKING from "../../../store/actions/socialLinkingActions/types";

jest.mock("../../../store/actions/socialLinkingActions/socialLinkingActions", () => ({
  startAuthenticationFlow: jest.fn()
}));

jest.mock("react-redux", () => ({
  useDispatch: () => jest.fn()
}));

window.open = jest.fn();

describe("EFCloutCardButtons", () => {
  describe("non-owner non-linked account", () => {
    const context = {
      socialType: "twitter",
      analyticsLocation: "organization",
      ownerKind: "organization",
      ownerId: "123",
      actionTypes: {
        linkSuccess: SOCIAL_LINKING.TWITTER.LINKED.SUCCESS,
        linkFailure: SOCIAL_LINKING.TWITTER.LINKED.FAILURE,
        accountInfoSuccess: SOCIAL_LINKING.TWITTER.ACCOUNT_INFO.SUCCESS,
        accountInfoFailure: SOCIAL_LINKING.TWITTER.ACCOUNT_INFO.FAILURE,
        unlinkSuccess: SOCIAL_LINKING.TWITTER.UNLINK.SUCCESS,
        unlinkFailure: SOCIAL_LINKING.TWITTER.UNLINK.FAILURE
      }
    };

    beforeEach(() => {
      render(
        <EFCloutContext.Provider value={context}>
          <EFCloutCardButtons />
        </EFCloutContext.Provider>
      );
    });

    it("renders a button with the correct text when account is not linked", () => {
      expect(screen.getByRole("button", { name: "Link" })).toBeInTheDocument();
    });

    it("opens the correct authentication window on click when account is not linked", () => {
      userEvent.click(screen.getByRole("button"));

      expect(startAuthenticationFlow).toHaveBeenCalledWith(context);
      expect(window.analytics.track).toHaveBeenCalledWith("EXTERNAL_ACCOUNT_OPEN_AUTH_WINDOW_TWITTER", {
        location: "organization",
        type: "organization"
      });
    });

    it("does not render an unlink button", () => {
      expect(screen.queryByRole("button", { name: "Unlink" })).not.toBeInTheDocument();
    });
  });

  describe("non-owner linked account", () => {
    const context = {
      socialType: "twitter",
      accountLinked: true,
      buttonText: "Click Me",
      accountUrl: "www.twitter.com/myAwesomeUsername",
      accountInfo: {
        accountName: "myAwesomeUsername"
      },
      ownerKind: "organization",
      analyticsLocation: "organization"
    };

    beforeEach(() => {
      render(
        <EFCloutContext.Provider value={context}>
          <EFCloutCardButtons />
        </EFCloutContext.Provider>
      );
    });

    it("renders correct button text when account is linked", () => {
      expect(screen.getByRole("button", { name: "Click Me" })).toBeInTheDocument();
    });

    it("navigates the correct url when button is clicked", () => {
      userEvent.click(screen.getByRole("button"));

      expect(window.open).toHaveBeenCalledWith("www.twitter.com/myAwesomeUsername");
      expect(window.analytics.track).toHaveBeenCalledWith("EXTERNAL_ACCOUNT_VIEW_TWITTER", {
        location: "organization",
        type: "organization"
      });
    });
  });
});
