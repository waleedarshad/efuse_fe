import React, { useContext } from "react";
import { useDispatch } from "react-redux";
import { EFCloutContext } from "../../EFCloutProvider/EFCloutProvider";
import Style from "./EFCloutUnlinkButton.module.scss";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";
import EFAlert from "../../../EFAlert/EFAlert";
import { unlinkOrganizationSocialMediaAccount } from "../../../../store/actions/socialLinkingActions/socialLinkingActions";

const EFCloutUnlinkButton = () => {
  const dispatch = useDispatch();
  const context = useContext(EFCloutContext);

  const toggleUnlinkModal = () => {
    const title = `You are about to unlink your ${context.linkText} account.`;

    const onConfirm = () => {
      dispatch(unlinkOrganizationSocialMediaAccount(context));
    };

    EFAlert(title, "Are you sure?", "Yes", "No", onConfirm);
  };

  return (
    <div className={Style.unlinkButtonContainer}>
      <EFRectangleButton
        buttonType="button"
        colorTheme="light"
        shadowTheme="medium"
        size="medium"
        fontWeightTheme="bold"
        fontCaseTheme="upperCase"
        text="Unlink"
        width="fullWidth"
        onClick={() => toggleUnlinkModal()}
      />
    </div>
  );
};

export default EFCloutUnlinkButton;
