import React, { createContext } from "react";

export const EFCloutContext = createContext();

const EFCloutProvider = ({ children, socialType, accountData, owner }) => {
  const context = {
    ...socialType,
    ...accountData,
    ...owner
  };

  return <EFCloutContext.Provider value={context}>{children}</EFCloutContext.Provider>;
};

export default EFCloutProvider;
