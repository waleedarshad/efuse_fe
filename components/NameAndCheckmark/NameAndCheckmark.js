import React from "react";
import Style from "./NameAndCheckmark.module.scss";
import VerifiedIcon from "../VerifiedIcon/VerifiedIcon";
import Link from "next/link";

const NameAndCheckmark = ({ name, verified, username }) => {
  return (
    <div className={Style.wrapper}>
      <Link href="/u/[u]" as={`/u/${username}`}>
        <p className={Style.name}>{name}</p>
      </Link>
      {verified && <VerifiedIcon />}
    </div>
  );
};

export default NameAndCheckmark;
