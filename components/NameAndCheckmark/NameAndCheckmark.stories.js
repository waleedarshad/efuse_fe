import React from "react";
import NameAndCheckmark from "./NameAndCheckmark";

export default {
  title: "Users/NameAndCheckmark",
  component: NameAndCheckmark,
  argTypes: {
    name: {
      control: {
        type: "text"
      }
    },
    verified: {
      control: {
        type: "boolean"
      }
    }
  }
};

const Story = args => <NameAndCheckmark {...args} />;

export const Verified = Story.bind({});
Verified.args = {
  name: "Austin May",
  verified: true
};

export const UnVerified = Story.bind({});
UnVerified.args = {
  name: "Austin May",
  verified: false
};
