import React from "react";
import capitalize from "lodash/capitalize";
import isEmpty from "lodash/isEmpty";
import { connect } from "react-redux";
import Style from "./MockExternalUser.module.scss";
import { mockExternalOrganizationView } from "../../store/actions/organizationActions";
import { mockExternalUserView } from "../../store/actions/portfolioActions";
import { dispatchNotification } from "../../store/actions/notificationActions";
import { sendNotification } from "../../helpers/FlashHelper";
import { toggleMockExternalErenaEvent } from "../../store/actions/erenaActions";

const MockExternalUser = props => {
  const notify = (visitor, type, customMessage) => {
    let message = visitor
      ? `You have stopped viewing your ${type} as a visitor`
      : `You are now viewing your ${type} as a visitor`;

    if (customMessage) message = customMessage;

    sendNotification(message, "success", `Changing ${capitalize(type)} View`);
  };

  const organizationMock = type => {
    const { mockExternalOrganization } = props;
    props.mockExternalOrganizationView(!mockExternalOrganization);
    notify(mockExternalOrganization, type);
  };
  const profileMock = type => {
    const { mockExternalUser } = props;
    props.mockExternalUserView(!mockExternalUser);
    notify(mockExternalUser, type);
  };

  const erenaMock = type => {
    props.toggleMockExternalErenaEvent();
    notify(props.mockExternalErenaEvent, type);
  };

  const mockBy = type => {
    switch (type) {
      case "profile":
        profileMock(type);
        break;
      case "organization":
        organizationMock(type);
        break;
      case "erena":
        erenaMock(type);
        break;
      default:
    }
  };
  const {
    currentUser,
    mockingPortfolioButton,
    mockExternalUser,
    id,
    type,
    owner,
    mockExternalOrganization,
    mockExternalErenaEvent,
    customStyle
  } = props;
  const showExternalButton = !isEmpty(currentUser) && (currentUser?.id === id || owner);

  return (
    <>
      {mockingPortfolioButton && showExternalButton && (
        <div onClick={() => mockBy(type)} className={`${Style.divStyle} ${customStyle}`}>
          <p className={Style.pStyle}>
            {mockExternalUser || mockExternalOrganization || mockExternalErenaEvent
              ? "Stop Viewing as an External Visitor"
              : "View as an External Visitor"}
          </p>
        </div>
      )}
    </>
  );
};

const mapStateToProps = state => ({
  mockExternalOrganization: state.organizations.mockExternalOrganization,
  mockExternalUser: state.portfolio.mockExternalUser,
  mockingPortfolioButton: state.features.mocking_portfolio_button,
  mockExternalErenaEvent: state.erena.mockExternalErenaEvent
});

export default connect(mapStateToProps, {
  mockExternalUserView,
  mockExternalOrganizationView,
  dispatchNotification,
  toggleMockExternalErenaEvent
})(MockExternalUser);
