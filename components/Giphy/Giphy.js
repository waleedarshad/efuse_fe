import { useRef, useState } from "react";
import dynamic from "next/dynamic";
import getConfig from "next/config";

import { OverlayTrigger, Overlay, Popover, Tooltip } from "react-bootstrap";
import Style from "./Giphy.module.scss";
import FeatureFlag from "../General/FeatureFlag/FeatureFlag";
import FeatureFlagVariant from "../General/FeatureFlag/FeatureFlagVariant/FeatureFlagVariant";
import { withCdn } from "../../common/utils";
import EFButton from "../Buttons/EFButton/EFButton";

const ReactGiphySearchBox = dynamic(import("react-giphy-searchbox"), {
  ssr: false
});
const { publicRuntimeConfig } = getConfig();
const { giphyApiKey } = publicRuntimeConfig;

const Giphy = ({ onGiphySelect, placement }) => {
  const ref = useRef(null);
  const [show, setShow] = useState(false);
  const [target, setTarget] = useState(null);

  const closePopover = event => {
    setShow(false);
  };

  const showPopover = event => {
    setShow(!show);
    setTarget(event.target);
  };
  const closeOnSelection = giphy => {
    onGiphySelect(giphy);
    closePopover();
  };
  return (
    <FeatureFlag name="enable_giphy">
      <FeatureFlagVariant flagState={true}>
        <div ref={ref} className={Style.container}>
          <Overlay
            target={target}
            show={show}
            placement={placement || "top"}
            rootClose
            rootCloseEvent="mousedown"
            onHide={closePopover}
          >
            <Popover id="popover-positioned-top" className={Style.popover}>
              <Popover.Content>
                <ReactGiphySearchBox
                  apiKey={giphyApiKey}
                  onSelect={closeOnSelection}
                  masonryConfig={[
                    { columns: 2, imageWidth: 110, gutter: 5 },
                    { mq: "700px", columns: 3, imageWidth: 110, gutter: 5 }
                  ]}
                  wrapperClassName={Style.giphyWrapper}
                  listWrapperClassName={Style.listWrapper}
                  searchFormClassName={Style.formWrapper}
                />
              </Popover.Content>
            </Popover>
          </Overlay>
          <OverlayTrigger key="top" placement="top" overlay={<Tooltip id="tooltip-top">Post a Gif</Tooltip>}>
            <EFButton colorTheme="transparent" shadowTheme="none" className={Style.button} onClick={showPopover}>
              <img src={withCdn("/static/images/gif.svg")} className={Style.gifIcon} alt="GIF" />
            </EFButton>
          </OverlayTrigger>
        </div>
      </FeatureFlagVariant>
    </FeatureFlag>
  );
};

export default Giphy;
