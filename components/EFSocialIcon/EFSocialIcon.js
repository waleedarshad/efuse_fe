import React from "react";
import Style from "./EFSocialIcon.module.scss";

const EFSocialIcon = ({ imageUrl, linkUrl, imageAltText }) => {
  return (
    <a className={Style.socialLink} href={linkUrl} target="_blank" rel="noreferrer">
      <img className={Style.socialIcon} src={imageUrl} alt={imageAltText} />
    </a>
  );
};

export default EFSocialIcon;
