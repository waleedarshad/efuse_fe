import React from "react";
import { TabContainer, TabContent, TabPane, Nav } from "react-bootstrap";
import EFBadge from "../EFBadge/EFBadge";
import Style from "./EFTabs.module.scss";

export interface TabData {
  tabTitle: string;
  tabKey: string;
  content: React.ReactNode;
  onTabSelect?: (tabKey: string) => void;
  badge?: number;
  actionButton?: React.ReactNode;
}

interface EFTabsProps {
  tabVariant?: "tabs" | "pills";
  tabs: TabData[];
  tabSize?: "small" | "large";
  addTabBorder?: boolean;
  defaultActiveKey?: string;
}

const EFTabs: React.FC<EFTabsProps> = ({
  tabVariant = "pills",
  tabs,
  tabSize = "large",
  addTabBorder = false,
  defaultActiveKey
}) => {
  const tabsData = tabs.map(tab => (!tab.tabKey ? { ...tab, tabKey: tab.tabTitle } : tab));

  return (
    <TabContainer defaultActiveKey={defaultActiveKey || tabsData[0].tabKey}>
      <Nav variant={tabVariant} className={Style.tabsContainer}>
        {tabs.map(tab => {
          return (
            <Nav.Item key={tab.tabKey} className={Style.tabWrapper}>
              <Nav.Link
                onSelect={() => tab?.onTabSelect(tab.tabKey)}
                eventKey={tab.tabKey}
                className={`${Style.tab} ${Style[tabSize]} ${addTabBorder ? Style.tabBorder : ""}`}
              >
                {tab.tabTitle}
              </Nav.Link>
              {tab.badge && (
                <span className={Style.badge}>
                  <EFBadge text={tab.badge} size="medium" backgroundColor="red" />
                </span>
              )}
            </Nav.Item>
          );
        })}
      </Nav>
      <TabContent className={Style.tabContent}>
        {tabs.map(tab => {
          return (
            <TabPane key={tab.tabKey} eventKey={tab.tabKey}>
              {tab.content}
            </TabPane>
          );
        })}
      </TabContent>
    </TabContainer>
  );
};

export default EFTabs;
