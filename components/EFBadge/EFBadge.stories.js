import React from "react";
import EFBadge from "./EFBadge";

export default {
  title: "Shared/EFBadge",
  component: EFBadge
};

// eslint-disable-next-line react/jsx-props-no-spreading
const Story = args => <EFBadge {...args} />;

export const Default = Story.bind({});
Default.args = {
  text: 10
};
