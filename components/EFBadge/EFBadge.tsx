import React from "react";
import Style from "./EFBadge.module.scss";

interface EFBadgeProps {
  text: string | Number;
  size: "small" | "medium";
  backgroundColor: "blue" | "red";
}

const EFBadge: React.FC<EFBadgeProps> = ({ text = "", size = "small", backgroundColor = "blue" }) => {
  if (text === "") {
    return <></>;
  }

  return <span className={`${Style.badge} ${Style[size]} ${Style[backgroundColor]} `}>{text}</span>;
};

export default EFBadge;
