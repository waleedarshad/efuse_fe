import { shallow } from "enzyme";
import EFBadge from "./EFBadge";

describe("EFBadge", () => {
  it("renders passed content", () => {
    const subject = shallow(<EFBadge text="new" />);

    expect(subject.text()).toContain("new");
  });
});
