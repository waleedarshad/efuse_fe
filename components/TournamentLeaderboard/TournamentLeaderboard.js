import React, { Component } from "react";
import axios from "axios";
import { connect } from "react-redux";
import Style from "./TournamentLeaderboard.module.scss";
import Accordion from "../Accordion/Accordion";
import KillsHeader from "../Accordion/TournamentKillsTemplate/KillsHeader";

class TournamentLeaderboard extends Component {
  state = {
    teams: [],
    players: [],
    mobileArrayDisplay: "teams",
    remainingAllTeams: "allTeams"
  };

  constructor(props) {
    super(props);
    this.getTeams = this.getTeams.bind(this);
    this.getPlayers = this.getPlayers.bind(this);
  }

  componentDidMount() {
    this.getTeams();
    this.getPlayers();
    setInterval(this.getTeams, 15000);
    setInterval(this.getPlayers, 15000);
  }

  componentWillUnMount() {
    clearInterval(this.getTeams);
    clearInterval(this.getPlayers);
  }

  getTeams() {
    const { tournament } = this.props;
    if (tournament?.tournamentId) {
      axios
        .get(`/tournament/${tournament.tournamentId}/leaderboard/teams`)
        .then(response => {
          this.setState({
            teams: response.data
          });
        })
        .catch(error => {
          console.log(error);
        });
    }
  }

  getPlayers() {
    const { tournament } = this.props;
    if (tournament?.tournamentId) {
      axios
        .get(`/tournament/${tournament.tournamentId}/leaderboard/players`)
        .then(response => {
          this.setState({
            players: response.data
          });
        })
        .catch(error => {
          console.log(error);
        });
    }
  }

  changeArrayState(newState) {
    this.setState({
      mobileArrayDisplay: newState
    });
  }

  changeRemainingAllTeams(newState) {
    this.setState({
      remainingAllTeams: newState
    });
  }

  render() {
    const { teams, players, mobileArrayDisplay, remainingAllTeams } = this.state;
    const { twitchView, tournament } = this.props;

    const teamsArray = teams.map((team, index) => {
      let displayTab = true;
      if (twitchView && index >= 3) {
        displayTab = false;
      }
      if (remainingAllTeams == "remaining" && !team.isActive && !twitchView) {
        displayTab = false;
      }
      return {
        data: team,
        display: displayTab,
        twitchView
      };
    });

    const playersArray = players.map((player, index) => {
      let displayTab = true;
      if (twitchView && index >= 3) {
        displayTab = false;
      }
      if (remainingAllTeams == "remaining" && !player.isActive && !twitchView) {
        displayTab = false;
      }
      return {
        data: player,
        display: displayTab,
        twitchView
      };
    });

    return (
      <div className={`${twitchView && Style.twitchContainer}`}>
        <div className={Style.tournamentLeaderboardContainer}>
          <div
            style={{
              backgroundColor: `${tournament?.primaryColor ? tournament.primaryColor : "#2e87ff"}`
            }}
            className={`${Style.mainHeader} ${twitchView && Style.twitchHeader}`}
          >
            <img
              className={`${Style.keemparkLogoLeaderboard} ${twitchView && Style.twitchKeemparkLogo}`}
              src={tournament?.brandLogoUrl || "https://cdn.efuse.gg/uploads/static/global/erenaLogo.png"}
            />
            <div className={`${Style.mainHeaderTitle} ${twitchView && Style.twitchHeaderTitle}`}>
              {tournament?.brandName || "eRena"} Leaderboard
            </div>
          </div>
          <div
            style={{
              backgroundColor: `${tournament?.secondaryColor ? tournament.secondaryColor : "rgb(33, 99, 185)"}`
            }}
            className={Style.subHeader}
          >
            <img
              className={`${Style.warzoneLeaderboardLogo} ${twitchView && Style.twitchWarzoneLogo}`}
              src={tournament?.tournamentLogoUrl}
            />
            <div className={`${Style.subHeaderTitle} ${twitchView && Style.twitchSubHeaderTitle}`}>
              {tournament?.tournamentName || "Fandom Warzone"}
            </div>
          </div>
          <div className={`${Style.bodyContainer} ${twitchView && Style.twitchBodyContainer}`}>
            <div
              style={{
                backgroundImage: `${
                  tournament?.primaryColor
                    ? `linear-gradient(${tournament.primaryColor}, rgb(29, 31, 39))`
                    : "linear-gradient(#2e87ff, rgb(29, 31, 39))"
                }`
              }}
              className={Style.backgroundGradient}
            />
            <div className={Style.buttonWrapper}>
              <div className={Style.selectButtonsContainer}>
                <p
                  className={`${Style.button} ${remainingAllTeams == "remaining" && Style.buttonActiveState}`}
                  onClick={() => this.changeRemainingAllTeams("remaining")}
                >
                  Remaining
                </p>
                <p
                  className={`${Style.button} ${remainingAllTeams == "allTeams" && Style.buttonActiveState}`}
                  onClick={() => this.changeRemainingAllTeams("allTeams")}
                >
                  All Teams
                </p>
              </div>
            </div>
            <div className={Style.titleGrid}>
              <div
                className={`${Style.titleContainer} ${mobileArrayDisplay == "teams" &&
                  Style.titleContainerActive} ${twitchView &&
                  mobileArrayDisplay == "teams" &&
                  Style.titleContainerActiveTwitch}`}
                onClick={() => this.changeArrayState("teams")}
              >
                <p className={Style.title}>Team Standings</p>
                <hr className={Style.divider} />
              </div>
              <div
                className={`${Style.titleContainer} ${mobileArrayDisplay == "players" &&
                  Style.titleContainerActive} ${twitchView &&
                  mobileArrayDisplay == "players" &&
                  Style.titleContainerActiveTwitch}`}
                onClick={() => this.changeArrayState("players")}
              >
                <p className={Style.title}>Players Rank</p>
                <hr className={Style.divider} />
              </div>
            </div>
            <div className={Style.leaderboardGrid}>
              <div
                className={`${Style.leftBox} ${!(mobileArrayDisplay == "teams") &&
                  Style.mobileTeamsList} ${twitchView && !(mobileArrayDisplay == "teams") && Style.twitchTeamsList}`}
              >
                <div className={Style.subTitle}>
                  <p>Team</p>
                  <p>Players</p>
                  <p>Score</p>
                </div>
                <div className={Style.teamList}>
                  <Accordion
                    openOnMount={false}
                    tabArray={teamsArray}
                    marginTop="8px"
                    customTemplate="TournamentKillsTemplate"
                  />
                </div>
              </div>
              <div
                className={`${Style.rightBox} ${!(mobileArrayDisplay == "players") &&
                  Style.mobileTeamsList} ${twitchView && !(mobileArrayDisplay == "players") && Style.twitchTeamsList}`}
              >
                <div className={Style.subTitle}>
                  <p>Team</p>
                  <p>Players</p>
                  <p>Score</p>
                </div>
                <div className={Style.teamList}>
                  {playersArray.map((tab, index) => {
                    if (tab.display != false) {
                      return (
                        <KillsHeader
                          teamName={tab.data.name}
                          rank={tab.data.rank}
                          isActiveTournament={tab.data.isActive}
                          points={tab.data.stats?.value}
                          pointsName={tab.data.stats?.name}
                          twitchView={tab.twitchView}
                          removeAccordion
                          key={index}
                        />
                      );
                    }
                    return <></>;
                  })}
                </div>
              </div>
            </div>
          </div>
          {twitchView && (
            <div className={Style.leaderboardButtonContainer}>
              <button
                className={Style.leaderboardButton}
                role="button"
                // onClick={this.goToLeaderboard}
              >
                View full leaderboard here
              </button>
            </div>
          )}
          <div className={Style.footer}>
            <a href="https://efuse.gg" target="_blank">
              <div className={Style.poweredBy}>Powered by</div>
              <img
                className={Style.efuseLogoLeaderboard}
                src="https://cdn.efuse.gg/uploads/static/global/efuseLogo.png"
              />
              <img
                className={Style.efuseNameLeaderboard}
                src="https://cdn.efuse.gg/uploads/static/global/efuseName.png"
              />
            </a>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  tournament: state.erena.tournament
});

export default connect(mapStateToProps, {})(TournamentLeaderboard);
