import React from "react";
import Link from "next/link";

import Style from "./UserCard.module.scss";
import { rescueNil, getImage } from "../../../helpers/GeneralHelper";
import EFAvatar from "../../EFAvatar/EFAvatar";
import VerifiedIcon from "../../VerifiedIcon/VerifiedIcon";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";

const UserCard = ({ user }) => {
  const profilePicture = rescueNil(user, "profilePicture");
  const fullName = user.name;
  const displayUsername = `@${user?.username}`;
  const verified = rescueNil(user, "verified");
  return (
    <div className={Style.card}>
      <div className={Style.header}>
        <EFAvatar
          borderTheme="primary"
          displayOnlineButton={false}
          profilePicture={getImage(profilePicture, "avatar")}
          size="large"
          href={`/u/${user?.username}?promptSignup=true`}
        />
      </div>

      <div className={Style.body}>
        <div>
          <b>{fullName}</b> {verified && <VerifiedIcon />}
        </div>
        <div className={Style.username}>{displayUsername}</div>
      </div>

      <div className={Style.bottom}>{user.bio}</div>
      <Link href={`/u/${user?.username}?promptSignup=true`}>
        <EFRectangleButton width="fullWidth" colorTheme="light" text="VIEW PORTFOLIO" />
      </Link>
    </div>
  );
};

export default UserCard;
