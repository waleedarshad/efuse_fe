import React from "react";
import map from "lodash/map";
import { faArrowLeft, faArrowRight } from "@fortawesome/pro-regular-svg-icons";

import Style from "./EFUserCarousel.module.scss";
import HorizontalScroll from "../HorizontalScroll/HorizontalScroll";
import UserCard from "./UserCard/UserCard";

const EFUserCarousel = ({ users }) => {
  return (
    <HorizontalScroll customButtonMargin={Style.buttonMarginTop} iconLeft={faArrowLeft} iconRight={faArrowRight}>
      <div className={Style.cardList}>
        {map(users, (user, index) => {
          return <UserCard user={user} key={index} />;
        })}
      </div>
    </HorizontalScroll>
  );
};

export default EFUserCarousel;
