import React from "react";
import EFUserCarousel from "./EFUserCarousel";

export default {
  title: "Users/EFUserCarousel",
  component: EFUserCarousel
};

// eslint-disable-next-line react/jsx-props-no-spreading
const Story = args => <EFUserCarousel {...args} />;

export const UserCarousel = Story.bind({});
UserCarousel.args = {
  users: [
    {
      username: "vantsome",
      verified: true,
      firstName: "Fakhir",
      lastName: "Shad",
      bio: "This is a test bio"
    },
    {
      username: "MSB",
      verified: true,
      firstName: "Saud",
      lastName: "Butt",
      bio: "This is how a user should write his bio. Take notes people. Short and futile :)"
    }
  ]
};
