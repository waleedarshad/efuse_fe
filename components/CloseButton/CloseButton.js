import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/pro-solid-svg-icons";
import PropTypes from "prop-types";

import Style from "./CloseButton.module.scss";
import EFRectangleButton from "../Buttons/EFRectangleButton/EFRectangleButton";

const CloseButton = ({ onClick, customClass, buttonSize }) => (
  <div className={!customClass && Style.closeButtonWrapper}>
    <EFRectangleButton
      text={<FontAwesomeIcon icon={faTimes} />}
      onClick={onClick}
      className={`${Style.closeButton} ${customClass}`}
      colorTheme="transparent"
      size={buttonSize}
      shadowTheme="none"
    />
  </div>
);

CloseButton.propTypes = {
  onClick: PropTypes.func.isRequired,
  customClass: PropTypes.string,
  buttonSize: PropTypes.string
};

CloseButton.defaultProps = {
  customClass: "",
  buttonSize: "small"
};

export default CloseButton;
