import React from "react";
import { faArrowLeft, faArrowRight } from "@fortawesome/pro-regular-svg-icons";

import HorizontalScroll from "../HorizontalScroll/HorizontalScroll";
import Style from "./EFTips.module.scss";

const EFTips = ({ tips, HeadingText, scrollValue, scrollIconLeft, scrollIconRight }) => {
  return (
    <>
      <div className={Style.tips}>{HeadingText}</div>
      <HorizontalScroll
        customStyle={Style.tipsContainer}
        customButtonMargin={Style.buttonMargin}
        iconLeft={scrollIconLeft}
        iconRight={scrollIconRight}
        scrollValue={scrollValue}
      >
        {tips?.map((tip, index) => {
          return (
            <p className={Style.tipText} key={index}>
              {tip}
            </p>
          );
        })}
      </HorizontalScroll>
    </>
  );
};

EFTips.defaultProps = {
  HeadingText: "TIPS",
  scrollValue: "565",
  scrollIconLeft: faArrowLeft,
  scrollIconRight: faArrowRight
};

export default EFTips;
