import React from "react";
import EFTips from "./EFTips";

export default {
  title: "Shared/EFTips",
  component: EFTips,
  argTypes: {
    tips: {
      control: {
        type: "array"
      }
    },
    HeadingText: {
      control: {
        type: "text"
      }
    }
  }
};

const Story = args => (
  <div className="container">
    <div className="row" style={{ width: "70%", margin: "auto" }}>
      <div className="col text-center">
        {/* eslint-disable-next-line react/jsx-props-no-spreading */}
        <EFTips {...args} />
      </div>
    </div>
  </div>
);

export const Basic = Story.bind({});
Basic.args = {
  tips: [
    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
    "When an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
    "It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
  ],
  HeadingText: "Efuse dummy tips"
};
