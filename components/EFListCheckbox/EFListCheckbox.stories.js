import React from "react";
import EFListCheckbox from "./EFListCheckbox";

export default {
  title: "Inputs/EFListCheckbox",
  component: EFListCheckbox,
  argTypes: {
    inputs: {
      control: {
        type: "array"
      }
    },
    onSelect: {
      control: {
        type: "function"
      }
    }
  }
};

// eslint-disable-next-line react/jsx-props-no-spreading
const Story = args => <EFListCheckbox {...args} />;

export const Default = Story.bind({});
Default.args = {
  inputs: [
    {
      imageUrl: "https://i.ytimg.com/vi/0E44DClsX5Q/maxresdefault.jpg",
      title: "New York Excelsior",
      _id: "1",
      isChecked: true
    },
    {
      imageUrl: "https://i.ytimg.com/vi/0E44DClsX5Q/maxresdefault.jpg",
      title: "New York Excelsior",
      _id: "2",
      isChecked: false
    },
    {
      imageUrl: "https://i.ytimg.com/vi/0E44DClsX5Q/maxresdefault.jpg",
      title: "New York Excelsior",
      _id: "3",
      isChecked: false,
      disabled: true
    }
  ],
  onSelect: () => {}
};
