import React from "react";

import EFNewCheckbox from "../EFNewCheckbox/EFNewCheckbox";
import Style from "./EFListCheckbox.module.scss";

interface EFListCheckboxProps {
  inputs: any;
  onSelect: (any) => void;
}

const EFListCheckbox: React.FC<EFListCheckboxProps> = ({ inputs, onSelect }) => {
  return inputs.map(input => {
    return (
      <div className={Style.checkboxWrapper} key={input._id}>
        <EFNewCheckbox
          imageUrl={input.imageUrl}
          title={input.title}
          disabled={input.disabled}
          id={input._id}
          isChecked={input.isChecked}
          onChange={() => onSelect && onSelect(input)}
        />
      </div>
    );
  });
};

export default EFListCheckbox;
