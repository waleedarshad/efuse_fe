import React from "react";
import { useRouter } from "next/router";
import { useSelector } from "react-redux";
import Link from "next/link";
import { faGamepadAlt } from "@fortawesome/pro-regular-svg-icons";
import { Col, Row } from "react-bootstrap";

import EFSubHeader from "../../Layouts/Internal/EFSubHeader/EFSubHeader";
import { getEsportsNavigationList } from "../../../navigation/esports";
import AuthAwareLayout from "../../Layouts/AuthAwareLayout/AuthAwareLayout";
import Style from "./EsportsLandingWrapper.module.scss";
import { formatMonth } from "../../../helpers/GeneralHelper";
import EFListCard from "../../Cards/EFListCard/EFListCard";
import EFSmallListCardCarousel from "../../EFCarousel/EFSmallListCardCarousel";
import EFQuickLinksWidget from "../../Cards/EFQuickLinksWidget/EFQuickLinksWidget";
import HighlightVideo from "../HighlightVideo/HighlightVideo";
import { getIndexName } from "../../../helpers/AlgoliaHelper";
import EFAlgoliaPersonalizedInstantSearch from "../../Algolia/EFAlgoliaPersonalizedInstantSearch";
import EFPersonalizedNewsList from "../../Algolia/News/EFPersonalizedNewsList";
import EventOpportunities from "../EventOpportunities/EventOpportunities";
import EFComingSoonOverlay from "../../EFComingSoonOverlay/EFComingSoonOverlay";

const EsportsLandingWrapper = ({ pageProps }) => {
  const router = useRouter();

  const algoliaIndex = getIndexName("NEWS");

  const showErenaLink = useSelector(state => state.features.web_erena_in_discover_navigation);
  const eRenaLandingIsActive = useSelector(state => state.features.web_erena_in_discover_navigation);
  const eSportsIsActive = useSelector(state => state.features.web_esports_page);

  const upcomingErenaTournaments = pageProps?.liveAndScheduledTournaments?.docs;

  const activeErenaTournaments = upcomingErenaTournaments?.filter(
    upcomingErenaTournament => upcomingErenaTournament.status === "active"
  );

  const getHighlightVideo = () => {
    if (activeErenaTournaments?.length && activeErenaTournaments[0]?.twitchChannel) {
      return activeErenaTournaments[0];
    }
    return pageProps?.erenaFeaturedVideos[0];
  };

  const newsArticleCategories = pageProps?.newsCategories;

  const tournamentList = upcomingErenaTournaments?.map((tournament, index) => {
    const formattedStartDateTime = formatMonth(tournament?.startDatetime);

    return (
      <div key={index}>
        <EFListCard
          backgroundImage={tournament?.backgroundImageUrl}
          liveChip={tournament?.status === "Active"}
          categoryChip="eRena"
          title={tournament?.tournamentName}
          date={formattedStartDateTime}
          owner={tournament?.owner?.name}
          slug={tournament?.slug}
          href={`/e/${tournament?.slug}`}
          size="carousel-large"
        />
      </div>
    );
  });

  return (
    <AuthAwareLayout metaTitle="eFuse | Esports" containsSubheader>
      <EFSubHeader
        headerIcon={faGamepadAlt}
        navigationList={getEsportsNavigationList(router.pathname, showErenaLink)}
      />
      <EFComingSoonOverlay size="large" hideOverlay={eSportsIsActive}>
        <div className={Style.carouselSection}>
          {tournamentList?.length > 0 && (
            <div>
              <div className={Style.introSection}>
                <p className={Style.mainHeader}>Upcoming eRena Events</p>
                {eRenaLandingIsActive && (
                  <Link href="/erena">
                    <p className={Style.basicLink}>View All</p>
                  </Link>
                )}
              </div>
              <div className={Style.carouselContainer}>
                <EFSmallListCardCarousel>{tournamentList}</EFSmallListCardCarousel>
              </div>
            </div>
          )}
          <Row>
            <Col>
              <HighlightVideo video={getHighlightVideo()} />
            </Col>
          </Row>
          <Row className={Style.contentSection}>
            <Col md={3}>
              <EFQuickLinksWidget />
            </Col>
            <Col md={7}>
              <p className={Style.sectionHeader}>Esports News</p>
              <EFAlgoliaPersonalizedInstantSearch indexName={algoliaIndex} hitsPerPage={5}>
                <EFPersonalizedNewsList indexName={algoliaIndex} categoryList={newsArticleCategories} />
              </EFAlgoliaPersonalizedInstantSearch>
              <div className={Style.opportunitiesWrapper}>
                <p className={Style.sectionHeader}>Opportunities</p>
                <EventOpportunities />
              </div>
            </Col>
            <Col md={2} />
          </Row>
        </div>
      </EFComingSoonOverlay>
    </AuthAwareLayout>
  );
};

export default EsportsLandingWrapper;
