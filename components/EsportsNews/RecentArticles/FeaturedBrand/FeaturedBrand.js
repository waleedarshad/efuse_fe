import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

import Style from "./FeaturedBrand.module.scss";
import { getNewsArticlesByCategory } from "../../../../store/actions/newsActions";

const FeaturedBrand = ({ title, categoryId, articleLength, direction, showImage }) => {
  const dispatch = useDispatch();

  const articles = useSelector(state => state.news.articles);

  useEffect(() => {
    dispatch(getNewsArticlesByCategory(1, 2, { category: categoryId }));
  }, []);

  return (
    <div>
      <h5 className={Style.brandTitle}>{title}</h5>
      <div className={Style.articleContainer} style={{ flexDirection: direction }}>
        {articles.map((article, index) => {
          if (index > articleLength - 1) {
            return <></>;
          }
          return (
            <div className={Style.article} key={index}>
              {showImage && (
                <div className={Style.articlePhoto} style={{ backgroundImage: `url(${article.image.url})` }} />
              )}
              <span className={Style.articleTitle}>
                <a className={Style.articleUrl} href={article?.url}>
                  {article.title}
                </a>
              </span>
              <span className={Style.articleDescription}>{article.summary}</span>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default FeaturedBrand;
