import React from "react";
import { Nav } from "react-bootstrap";

import Style from "./NavLinks.module.scss";

const NavLinks = ({ route }) => {
  return (
    <div className={Style.learningBtnNav}>
      <a href="/news">
        <Nav.Link href="/news" className={`${Style.link} ${route === "/news" && Style.active}`}>
          All
        </Nav.Link>
      </a>
      <a href="/news/owned">
        <Nav.Link href="/news/owned" className={`${Style.link} ${route === "/news/owned" && Style.active}`}>
          My Articles
        </Nav.Link>
      </a>
    </div>
  );
};

export default NavLinks;
