import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useRouter } from "next/router";

import Style from "./RecentArticles.module.scss";
import ArticleListing from "../ArticleListing/ArticleListing";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";
import EmptyComponent from "../../EmptyComponent/EmptyComponent";
import Modal from "../../Modal/Modal";
import NewArticleModal from "../NewArticleModal/NewArticleModal";
import NavLinks from "./NavLinks/NavLinks";
import NewsCategoryFilters from "./Filters/NewsCategoryFilters";

const RecentArticles = ({ articles, loadMore, hasMore, categoryList }) => {
  const router = useRouter();
  const [ifMobile, setIfMobile] = useState(window.matchMedia("(max-width: 520px)").matches);

  const allowArticleCreation = useSelector(state => state.features.allow_learning_article_creation);

  useEffect(() => {
    window.addEventListener("resize", mediaQueryResponse);
  }, []);

  const mediaQueryResponse = () => {
    if (window.matchMedia("(max-width: 520px)").matches) {
      setIfMobile(true);
    } else {
      setIfMobile(false);
    }
  };

  const { categorySlug } = router.query;
  const currentFilter = categorySlug;

  const skeletonArticles = [];
  for (let index = 0; index < 20; index++) {
    skeletonArticles.push({});
  }

  return (
    <div className={Style.gridDisplay}>
      <div className={Style.middle}>
        {allowArticleCreation && <NavLinks route={router.route} />}
        {articles && articles.length === 0 ? (
          <EmptyComponent
            style={{ width: "100%" }}
            text="There are no articles for this category. Please check back later!"
          />
        ) : (
          <ArticleListing articles={articles} loadMore={loadMore} hasMore={hasMore} isMobile={ifMobile} expandedGrid />
        )}
      </div>
      <div>
        <div className="stickySidebar">
          {allowArticleCreation && (
            <Modal
              displayCloseButton
              title="Create News Article"
              textAlignCenter={false}
              onOpenModal={() => {}}
              component={<NewArticleModal />}
            >
              <div className={Style.create}>
                <EFRectangleButton text="Create News Article" shadowTheme="none" width="fullWidth" />
              </div>
            </Modal>
          )}

          <NewsCategoryFilters currentFilter={currentFilter} categoryList={categoryList} />
        </div>
      </div>
    </div>
  );
};

export default RecentArticles;
