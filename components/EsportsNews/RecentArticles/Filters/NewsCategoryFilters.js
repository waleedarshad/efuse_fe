import React from "react";
import Link from "next/link";
import Style from "./Filters.module.scss";
import DisplayFilter from "./DisplayFilter/DisplayFilter";

const NewsCategoryFilters = ({ currentFilter, categoryList }) => {
  return (
    <div className={Style.filters}>
      <p className={Style.filterTitle}>News Categories</p>
      <Link href={{ pathname: "/news" }}>
        <button
          className={`${Style.filterButton} ${Style.recentFilter} ${!currentFilter && Style.active}`}
          type="button"
        >
          <span className={Style.btnText}>Featured Content</span>
        </button>
      </Link>
      {categoryList?.map((category, index) => {
        return (
          <DisplayFilter
            key={index}
            pathname={category?.slug}
            filterName={category?.name}
            currentFilter={currentFilter}
          />
        );
      })}
    </div>
  );
};

export default NewsCategoryFilters;
