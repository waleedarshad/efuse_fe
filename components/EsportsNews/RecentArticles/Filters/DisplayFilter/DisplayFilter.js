import React from "react";
import Link from "next/link";

import Style from "../Filters.module.scss";

const DisplayFilter = ({ pathname, filterName, currentFilter }) => {
  const onClick = () => {
    analytics.track("NEWS_ARTICLE_FILTERS_APPLIED", { category: filterName });
  };
  return (
    <Link href={{ pathname: `/news/${pathname}` }}>
      <button
        className={`${Style.filterButton} ${Style.recentFilter} ${currentFilter === pathname && Style.active}`}
        type="button"
        onClick={() => onClick()}
        data-cy={`filter-${filterName}`}
      >
        <span className={Style.btnText}>{filterName}</span>
      </button>
    </Link>
  );
};

export default DisplayFilter;
