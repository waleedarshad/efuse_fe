import { NextSeo } from "next-seo";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";
import { Col } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { faBinoculars } from "@fortawesome/pro-solid-svg-icons";
import { withCdn } from "../../common/utils";
import { getNewsArticlesNew, getOwnedArticles } from "../../store/actions/newsActions";
import HighlightFive from "../HighlightFive/HighlightFive";
import ArticleListing from "./ArticleListing/ArticleListing";
import Style from "./NewsHomePage.module.scss";
import RecentArticles from "./RecentArticles/RecentArticles";
import { getDiscoverNavigationList } from "../../navigation/discover";
import EFSubHeader from "../Layouts/Internal/EFSubHeader/EFSubHeader";
import AuthAwareLayout from "../Layouts/AuthAwareLayout/AuthAwareLayout";

const NewsHomePage = ({ query, pageProps }) => {
  const currentUser = useSelector(state => state.auth.currentUser);
  const articles = useSelector(state => state.news.articles);
  const pagination = useSelector(state => state.news.pagination);
  const learningSectionGrid = useSelector(state => state.features.learning_section_grid);
  const showErenaLink = useSelector(state => state.features.web_erena_in_discover_navigation);

  const userLoggedIn = currentUser?.id;
  const router = useRouter();
  const dispatch = useDispatch();

  const [userLoaded, setUserLoaded] = useState(false);

  const loadMore = () => {
    const nextPage = pagination ? pagination.page + 1 : 1;
    dispatch(getNewsArticlesNew(!userLoggedIn, nextPage, 9, query));
  };

  const loadMoreOwned = () => {
    const nextPage = pagination ? pagination.page + 1 : 1;
    dispatch(getOwnedArticles(nextPage));
  };

  const loadArticles = () => {
    const { categorySlug } = router.query;

    if (!userLoaded && currentUser) {
      if (categorySlug) {
        // category page
        analytics.page("News", { categorySlug });
        dispatch(getNewsArticlesNew(!userLoggedIn, 1, 9, { categorySlug }));
      } else if (router.route === "/news/owned") {
        // my articles page
        analytics.page("News", { categorySlug: "Featured Content" });
        dispatch(getOwnedArticles(1));
      } else {
        // all articles page
        analytics.page("News", { categorySlug: "Featured Content" });
        dispatch(getNewsArticlesNew(!userLoggedIn, 1));
      }
      setUserLoaded(true);
    }
  };

  useEffect(() => {
    loadArticles();
  });

  const hasMore = pagination && pagination.hasNextPage;
  const isOwnedPage = router.route === "/news/owned";

  const headerArticles = [];
  const listOfArticles = [];

  if (articles) {
    articles.forEach((article, index) => {
      if (index < 5) {
        headerArticles.push({
          name: article?.author.name,
          date: article?.createdAt,
          image: article?.image?.url,
          title: article?.title,
          url: article?.url
        });
      } else {
        listOfArticles.push(article);
      }
    });
  }
  const mainContent = learningSectionGrid ? (
    <>
      <NextSeo
        title="eFuse News | efuse.gg"
        description="View popular articles about gaming and esports. eFuse is a web and mobile application that provides validated opportunities and candidates for the esports and Video Game industry."
        openGraph={{
          type: "website",
          url: "https://efuse.gg/news",
          title: "eFuse News | efuse.gg",
          site_name: "eFuse.gg",
          description:
            "View popular articles about gaming and esports. eFuse is a web and mobile application that provides validated opportunities and candidates for the esports and Video Game industry.",
          images: [{ url: withCdn("/static/images/fb_scrape.png") }]
        }}
        twitter={{
          handle: "@eFuseOfficial",
          site: "https://efuse.gg/news",
          cardType: "summary_large_image"
        }}
      />
      <div className={Style.title}>
        <div className={Style.efuseLogoContianer}>
          <Link href="/">
            <img
              className={Style.efuseName}
              alt="efuse watermark"
              src="https://cdn.efuse.gg/uploads/static/global/efuseLogoDark.png"
            />
          </Link>
        </div>
        <h1 style={{ color: "black", padding: "15px" }}>News</h1>
      </div>
      <HighlightFive listOfFive={headerArticles} />
      <RecentArticles
        articles={listOfArticles}
        loadMore={isOwnedPage ? loadMoreOwned : loadMore}
        hasMore={hasMore}
        categoryList={pageProps?.categories}
      />
    </>
  ) : (
    <Col md={12}>
      {listOfArticles && (
        <ArticleListing articles={listOfArticles} loadMore={isOwnedPage ? loadMoreOwned : loadMore} hasMore={hasMore} />
      )}
    </Col>
  );

  return (
    <AuthAwareLayout metaTitle="eFuse | News" containsSubheader>
      <EFSubHeader
        headerIcon={faBinoculars}
        navigationList={getDiscoverNavigationList(router.pathname, showErenaLink)}
      />
      {mainContent}
    </AuthAwareLayout>
  );
};

export default NewsHomePage;
