import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Form } from "react-bootstrap";

import Style from "./Modals.module.scss";
import { updateNewsArticle, getCategories } from "../../../../store/actions/newsActions";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";

const ChangeCategoryModal = ({ currentCategoryId, articleId, closeModal }) => {
  const dispatch = useDispatch();

  const [categoryId, setCategoryId] = useState(currentCategoryId);

  const categories = useSelector(state => state.news.categories);

  useEffect(() => {
    dispatch(getCategories());
  }, []);

  return (
    <>
      <div className={Style.authorContainer}>
        {categories.map(category => {
          return (
            <div key={category._id} className="mb-3">
              <Form.Check
                type="radio"
                id={category._id}
                label={category.name}
                checked={category._id === categoryId}
                onChange={() => setCategoryId(category._id)}
              />
            </div>
          );
        })}
      </div>
      <EFRectangleButton
        shadowTheme="none"
        text="Update Category"
        onClick={() => {
          dispatch(
            updateNewsArticle(articleId, {
              category: categoryId
            })
          );
          closeModal();
        }}
      />
    </>
  );
};

export default ChangeCategoryModal;
