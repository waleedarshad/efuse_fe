import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";

import Style from "./Modals.module.scss";
import { updateNewsArticle } from "../../../../store/actions/newsActions";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";
import targetingQuestionsJson from "../../../TraitsMotivationsSelection/targeting.json";
import TraitsMotivationsSelection from "../../../TraitsMotivationsSelection/TraitsMotivationsSelection";

const ChangeTraitsMotivationsModal = ({ initialTraits, initialMotivations, closeModal, articleId }) => {
  const dispatch = useDispatch();

  const [questions, setQuestions] = useState(null);

  const setTraitData = (traits, motivations, questions) => {
    const list = questions.map(question => {
      if (question.id === "traits") {
        question.options = question.options.map(item => {
          item.selected = traits.includes(item.value) ? true : item.selected;
          return item;
        });
      }
      if (question.id === "motivations") {
        question.options = question.options.map(item => {
          item.selected = motivations.includes(item.value) ? true : item.selected;
          return item;
        });
      }
      return question;
    });
    return list;
  };

  useEffect(() => {
    setQuestions(setTraitData(initialTraits, initialMotivations, targetingQuestionsJson));
  }, []);

  const traitOptionClicked = (questionArrayName, value, type) => {
    const list = questions.map(question => {
      if (type === question.id) {
        question.options = question.options.map(item => {
          if (question.type === "single-answer") item.selected = false;

          item.selected = item.value === value ? !item.selected : item.selected;

          return item;
        });
      }
      return question;
    });
    setQuestions(list);
  };

  const getTraitData = () => {
    const traits = [];
    const motivations = [];
    const traitsQuestion = questions.find(question => question.id === "traits");
    const motivationsQuestion = questions.find(question => question.id === "motivations");

    traitsQuestion.options.map(option => {
      if (option.selected) traits.push(option.value);
    });

    motivationsQuestion.options.map(option => {
      if (option.selected) motivations.push(option.value);
    });

    return { traits, motivations };
  };

  return (
    <>
      <div className={Style.traitsMotivationsContainer}>
        {questions && (
          <TraitsMotivationsSelection
            questionArrayName="questions"
            questions={questions}
            optionClicked={traitOptionClicked}
          />
        )}
        <div>
          eFuse will use the information provided above to better recommend your learning article to relevant users on
          eFuse.
        </div>
      </div>
      <EFRectangleButton
        shadowTheme="none"
        text="Update Tags"
        onClick={() => {
          const { traits, motivations } = getTraitData();
          dispatch(
            updateNewsArticle(articleId, {
              traits,
              motivations
            })
          );
          closeModal();
        }}
      />
    </>
  );
};

export default ChangeTraitsMotivationsModal;
