import React, { useState } from "react";
import { useDispatch } from "react-redux";

import Style from "./Modals.module.scss";
import { updateNewsArticle } from "../../../../store/actions/newsActions";
import HelpToolTip from "../../../HelpToolTip/HelpToolTip";
import UploadVideoButton from "../../../UploadVideoButton";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";

const ChangeMainVideoModal = ({ currentVideo, closeModal, articleId }) => {
  const dispatch = useDispatch();

  const [video, setVideo] = useState(currentVideo);

  const onClearVideo = () => {
    setVideo(null);
  };

  const onVideoFileUpload = file => {
    if (file.url.length === 0) {
      setVideo(null);
    } else {
      setVideo(file);
    }
  };

  const updateMainVideo = () => {
    const articleData = new FormData();
    articleData.append("video", JSON.stringify(video));
    dispatch(updateNewsArticle(articleId, articleData));
    closeModal();
  };

  return (
    <>
      <div className={Style.summaryCardContainer}>
        <div className={Style.labelContainer} style={{ paddingBottom: "25px" }}>
          <label className={Style.label}>Main Video</label>
          <HelpToolTip text="This allows you to upload a video directly to eFuse. If you are using vimeo or youtube, use the + button on the article editor." />
        </div>
        {video ? (
          <UploadVideoButton
            onFileUpload={onVideoFileUpload}
            onClearMedia={onClearVideo}
            file={video}
            uploadStarted={video !== null}
            directory="uploads/learning-articles/"
            placeholder="UPLOAD VIDEO"
            style={{
              borderRadius: "0",
              backgroundColor: "#214579",
              marginBottom: ".5rem",
              paddingRight: "40px",
              paddingLeft: "40px"
            }}
          />
        ) : (
          <UploadVideoButton
            onFileUpload={onVideoFileUpload}
            onClearMedia={onClearVideo}
            directory="uploads/learning-articles/"
            placeholder="UPLOAD VIDEO"
            style={{
              borderRadius: "0",
              backgroundColor: "#214579",
              marginBottom: ".5rem",
              paddingRight: "40px",
              paddingLeft: "40px"
            }}
          />
        )}
      </div>
      <EFRectangleButton shadowTheme="none" text="Update Main Video" onClick={() => updateMainVideo()} />
    </>
  );
};

export default ChangeMainVideoModal;
