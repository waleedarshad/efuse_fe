import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Form } from "react-bootstrap";
import Style from "./Modals.module.scss";
import { updateNewsArticle } from "../../../../store/actions/newsActions";
import { getPostableOrganizations } from "../../../../store/actions/userActions";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";

const ChangeAuthorModal = ({ currentAuthorId, authorType, closeModal, articleId }) => {
  const dispatch = useDispatch();
  const currentUser = useSelector(state => state.auth.currentUser);
  const currentUserOrgs = useSelector(state => state.user.postableOrganizations);

  const [authorId, setAuthorId] = useState(null);
  const [localAuthorType, setLocalAuthorType] = useState(authorType);

  useEffect(() => {
    dispatch(getPostableOrganizations(1, 999));
    setAuthorId(currentAuthorId);
  }, []);

  return (
    <>
      <div className={Style.authorContainer}>
        <div key={currentUser.id} className="mb-3">
          <Form.Check
            type="radio"
            id={currentUser.id}
            label={currentUser.name}
            checked={currentUser.id === authorId}
            onChange={() => {
              setAuthorId(currentUser.id);
              setLocalAuthorType("users");
            }}
          />
        </div>
        {currentUserOrgs.map(org => {
          return (
            <div key={org._id} className="mb-3">
              <Form.Check
                type="radio"
                id={org._id}
                label={org.name}
                checked={org._id === authorId}
                onChange={() => {
                  setAuthorId(org._id);
                  setLocalAuthorType("organizations");
                }}
              />
            </div>
          );
        })}
      </div>
      <EFRectangleButton
        shadowTheme="none"
        text="Update Author"
        onClick={() => {
          dispatch(
            updateNewsArticle(articleId, {
              author: authorId,
              authorType: localAuthorType
            })
          );
          closeModal();
        }}
      />
    </>
  );
};
export default ChangeAuthorModal;
