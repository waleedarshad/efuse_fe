import React, { useState } from "react";
import { useDispatch } from "react-redux";
import CharacterCounter from "react-character-counter";

import Style from "./Modals.module.scss";
import { updateNewsArticle } from "../../../../store/actions/newsActions";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";
import HelpToolTip from "../../../HelpToolTip/HelpToolTip";
import InputField from "../../../InputField/InputField";
import { toggleCropModal } from "../../../../store/actions/settingsActions";
import ImageUploader from "../../../ImageUploader/ImageUploader";

const ChangeSummaryCardModal = ({ currentSummary, currentImage, articleId, closeModal }) => {
  const dispatch = useDispatch();

  const [summary, setSummary] = useState(currentSummary);
  const [image, setImage] = useState(currentImage.url);

  const onDrop = selectedFile => {
    setImage(selectedFile);
  };

  const onCropChange = croppedImage => {
    const { blob } = croppedImage;
    const croppedFile = new File([blob], blob.name, { type: blob.type });
    setImage(croppedFile);
  };

  const updateSummaryCard = () => {
    const articleData = new FormData();
    articleData.append("image", image);
    articleData.append("summary", summary);
    dispatch(updateNewsArticle(articleId, articleData));
    closeModal();
  };

  return (
    <>
      <div className={Style.summaryCardContainer}>
        <div className={Style.labelContainer}>
          <label className={Style.label}>Image</label>
          <HelpToolTip text="Upload a square image that represents your learning article content." />
        </div>
        <ImageUploader
          text="no image uploaded"
          name="image"
          onDrop={onDrop}
          value={image}
          theme="internal"
          onCropChange={onCropChange}
          toggleCropperModal={(val, name) => {
            dispatch(toggleCropModal(val, name));
          }}
          aspectRatioWidth={180}
          aspectRatioHeight={180}
          showCropper
          showImageDescription
        />
        <div className={Style.labelContainer}>
          <label className={Style.label}>Summary</label>
          <HelpToolTip text="Write a short, meaningful article summary that describes the content of your article." />
        </div>
        <CharacterCounter value={summary} maxLength={70}>
          <InputField
            name="title"
            placeholder="Briefly summarize your article..."
            size="lg"
            theme="internal"
            onChange={e => setSummary(e.target.value)}
            required
            errorMessage="Summary is required."
            value={summary}
            maxLength={70}
          />
        </CharacterCounter>
      </div>
      <EFRectangleButton shadowTheme="none" text="Update Summary Card" onClick={() => updateSummaryCard()} />
    </>
  );
};

export default ChangeSummaryCardModal;
