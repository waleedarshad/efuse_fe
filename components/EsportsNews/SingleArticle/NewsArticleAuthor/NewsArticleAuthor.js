import React from "react";
import Link from "next/link";

import EFAvatar from "../../../EFAvatar/EFAvatar";
import { EFHtmlParser } from "../../../EFHtmlParser/EFHtmlParser";
import { getImage } from "../../../../helpers/GeneralHelper";
import Style from "../SingleArticle.module.scss";

const NewsArticleAuthor = ({ article, postedByOrganization, ownerUrl }) => {
  const authorPicture = getImage(
    article.authorType === "users" ? article.author.profilePicture : article.author.profileImage,
    "avatar"
  );
  return (
    <div className={Style.aboutAuthorContainer}>
      <Link href={postedByOrganization ? ownerUrl : "/u/[u]"} as={ownerUrl}>
        <a className={Style.authorPicture}>
          <EFAvatar displayOnlineButton={false} profilePicture={authorPicture} size="large" />
        </a>
      </Link>
      <div>
        <div className={Style.writtedBy}>WRITTEN BY</div>
        <Link href={postedByOrganization ? ownerUrl : "/u/[u]"} as={ownerUrl}>
          <a className={Style.authorLinkBio}>{article.author.name}</a>
        </Link>
        <div className={Style.aboutBio}>
          {article.authorType === "users"
            ? article.author.bio
            : article.author.about && <EFHtmlParser>{article.author.about}</EFHtmlParser>}
        </div>
      </div>
    </div>
  );
};

export default NewsArticleAuthor;
