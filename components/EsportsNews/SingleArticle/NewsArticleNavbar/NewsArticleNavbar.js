import { Component } from "react";
import { Col } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft } from "@fortawesome/pro-solid-svg-icons";

import Style from "./NewsArticleNavbar.module.scss";

class NewsArticleNavbar extends Component {
  render() {
    const { category, darkMode } = this.props;

    return (
      <Col md={12}>
        <div>
          <a href="/news" className={Style.backArrowLinkMobile}>
            <FontAwesomeIcon icon={faChevronLeft} className={Style.backArrowIcon} />
            <span>eFuse News</span>
          </a>
        </div>
        <div className={Style.navbar}>
          <div className={Style.logoCategoryContainer}>
            <img
              src={
                darkMode
                  ? "https://cdn.efuse.gg/uploads/static/global/efuseBombLogoLightNoMargin.png"
                  : "https://cdn.efuse.gg/uploads/static/global/efuseBombLogoDark.png"
              }
              className={Style.logo}
            />
            <div className={Style.categoryNavItem}>{category.name}</div>
          </div>
          <a href="/news" className={Style.backArrowLink}>
            <FontAwesomeIcon icon={faChevronLeft} className={Style.backArrowIcon} />
            <span>eFuse News</span>
          </a>
        </div>
      </Col>
    );
  }
}

export default NewsArticleNavbar;
