import React from "react";

import Recommendation from "../../../Recommendation/Recommendation";
import EFIsLoggedIn from "../../../EFIsLoggedIn/EFIsLoggedIn";
import Style from "./NewsArticleRecommendations.module.scss";

const NewsArticleRecommendations = () => {
  return (
    <EFIsLoggedIn>
      <div className={Style.recommendedArticlesWrapper}>
        <div className={Style.recommendedArticles}>Recommended Articles</div>
        <Recommendation type="learning" hideContainer />
      </div>
    </EFIsLoggedIn>
  );
};

export default NewsArticleRecommendations;
