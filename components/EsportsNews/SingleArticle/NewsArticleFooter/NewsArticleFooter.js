import React from "react";
import { Container, Col, Row } from "react-bootstrap";
import Link from "next/link";

import EFIsLoggedIn from "../../../EFIsLoggedIn/EFIsLoggedIn";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";
import Style from "./NewsArticleFooter.module.scss";

const NewsArticleFooter = () => {
  return (
    <div className={Style.footer}>
      <Container>
        <Row>
          <Col md={4} className="mt-3">
            <h3>About eFuse</h3>
            <p>A web and mobile application that serves as the professional hub for esports and video games.</p>
            <EFRectangleButton internalHref="/" text="Learn More" shadowTheme="none" />
          </Col>
          <Col md={4} />
          <Col md={4} className="mt-3">
            <EFIsLoggedIn
              altComponent={
                <>
                  <h3>Sign up today</h3>
                  <p>
                    By joining eFuse, you&apos;ll be able to create your gaming portfolio and interact with other gamers
                    and professionals in the industry.
                  </p>

                  <EFRectangleButton internalHref="/signup" text="Sign up" shadowTheme="none" />
                </>
              }
            />
          </Col>
        </Row>
        <Row className={Style.footerLinkRow}>
          <Col md={6} className="mb-3">
            <Link href="/">
              <img
                className={Style.efuse}
                alt="efuse logo"
                src="https://cdn.efuse.gg/uploads/static/global/efuseLogo.png"
              />
            </Link>
            <Link href="/">
              <img
                className={Style.efuseName}
                alt="efuse watermark"
                src="https://cdn.efuse.gg/uploads/static/global/efuseName.png"
              />
            </Link>
          </Col>
          <Col md={6} className={Style.footerLinkColumn}>
            <a href="/privacy">Privacy Policy</a>
            <a href="/terms">Terms of Service</a>
            <a href="https://efuse.careers">Careers</a>
            <a href="https://efuse.gg/news/efuse-help">Help</a>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default NewsArticleFooter;
