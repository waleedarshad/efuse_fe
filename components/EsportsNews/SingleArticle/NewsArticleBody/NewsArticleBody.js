import React from "react";
import { Col } from "react-bootstrap";

import VideoPlayer from "../../../VideoPlayer/VideoPlayer";
import { EFHtmlParser, ValidationParserTypes } from "../../../EFHtmlParser/EFHtmlParser";
import Style from "./NewsArticleBody.module.scss";

const NewsArticleBody = ({ article, isOwner }) => {
  return (
    <Col md={{ span: 8, offset: 2 }}>
      <div>
        {article?.video?.url && (
          <div className={Style.videoWrapper}>
            <VideoPlayer url={article.video.url} />
          </div>
        )}

        <div className={isOwner ? Style.show : Style.hide}>
          <textarea id="entry" />
        </div>
        <div className={isOwner ? Style.hide : Style.show}>
          <div className={`${Style.body} arx-content`}>
            <EFHtmlParser validationType={ValidationParserTypes.LEARNING_ARTICLE}>{article?.body || ""}</EFHtmlParser>
          </div>
        </div>
      </div>
    </Col>
  );
};

export default NewsArticleBody;
