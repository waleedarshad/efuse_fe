import { Component } from "react";
import { Col } from "react-bootstrap";
import Style from "./NewsArticleTitle.module.scss";

class NewsArticleTitle extends Component {
  render() {
    const { title, isOwner, onChange, onBlur, darkMode } = this.props;

    return (
      <Col md={{ span: 10, offset: 1 }}>
        <div>
          {isOwner ? (
            <textarea
              className={`${Style.titleInput} ${darkMode ? Style.dark : "light"}`}
              value={title}
              // rows={1}
              onChange={e => {
                onChange(e.target.value);
              }}
              onBlur={e => {
                onBlur(e.target.value);
              }}
            />
          ) : (
            <h1 className={Style.title} data-cy="news_article_title">
              {title}
            </h1>
          )}
        </div>
      </Col>
    );
  }
}

export default NewsArticleTitle;
