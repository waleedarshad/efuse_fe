import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Container, Col } from "react-bootstrap";
import { NextSeo } from "next-seo";
import { Fade } from "react-reveal";
import config from "react-reveal/globals";
import Style from "./SingleArticle.module.scss";
import { updateNewsArticle, setArticleSSR } from "../../../store/actions/newsActions";
import { getPostableOrganizations } from "../../../store/actions/userActions";
import NewsArticleNavbar from "./NewsArticleNavbar/NewsArticleNavbar";
import NewsArticleTitle from "./NewsArticleTitle/NewsArticleTitle";
import NewsArticleInfoBar from "./NewsArticleInfoBar/NewsArticleInfoBar";
import { shareLinkModal } from "../../../store/actions/shareAction";
import { SHARE_LINK_KINDS } from "../../ShareLinkModal/ShareLinkModal";
import sanitizeMetaDescription from "../../../store/utils/sanitizeMetaDescription";
import Error from "../../../pages/_error";
import NewsArticleBody from "./NewsArticleBody/NewsArticleBody";
import NewsArticleAuthor from "./NewsArticleAuthor/NewsArticleAuthor";
import NewsArticleFooter from "./NewsArticleFooter/NewsArticleFooter";
import NewsArticleRecommendations from "./NewsArticleRecommendations/NewsArticleRecommendations";
import {
  initArticleEditor,
  onYouTubeIframeAPIReady,
  isArticleOrganizationAdmin,
  isArticleOwner,
  calculateReadingTime
} from "../../../helpers/NewsHelper";

config({ ssrFadeout: true });

const SingleArticle = ({ pageProps }) => {
  const dispatch = useDispatch();

  const [title, setTitle] = useState(pageProps.article.title);
  const [body, setBody] = useState(pageProps.article.body);
  const [darkMode, setDarkMode] = useState(false);
  const [userLoaded, setUserLoaded] = useState(false);

  const currentUser = useSelector(state => state.auth.currentUser);
  const article = useSelector(state => state.news.currentArticle);
  const currentUserOrgs = useSelector(state => state.user.postableOrganizations);

  useEffect(() => {
    dispatch(setArticleSSR(pageProps.article));

    analytics.page("News Portal Article Page", {
      slug: pageProps.article?.url
    });

    (async () => {
      // Load in Article WYSIWIG Editor
      // eslint-disable-next-line global-require
      const { ArticleEditor } = await require("../../../static/plugins/article-editor/article-editor");
      initArticleEditor(ArticleEditor, setBody, body, updateArticle);
    })().catch(error => {
      console.error(error);
    });

    const sevenAM = 7;
    const eightPM = 20;
    const d = new Date();
    if (d.getHours() < sevenAM || d.getHours() > eightPM) setDarkMode(true);
  }, []);

  useEffect(() => {
    // Setting onStateChange function for embedded youtube video player
    // Allowing only one video to play at a time

    const observer = new MutationObserver((mutations, me) => {
      // `mutations` is an array of mutations that occurred
      // `me` is the MutationObserver instance
      const iframes = document.querySelectorAll("iframe");

      if (iframes) {
        onYouTubeIframeAPIReady(iframes);
        me.disconnect(); // stop observing
      }
    });

    // start observing
    observer.observe(document, {
      childList: true,
      subtree: true
    });
  }, []);

  const updateArticle = text => {
    dispatch(updateNewsArticle(pageProps.article._id, { body: text }));
  };

  if (!userLoaded && currentUser?.id) {
    dispatch(getPostableOrganizations());
    setUserLoaded(true);
  }

  if (pageProps && !pageProps.article) {
    return <Error statusCode="404" />;
  }

  // this is the article from SSR - only used for NextSeo component for link scraping.
  const SSRArticle = pageProps.article;

  // Determines if current user is author or admin - enabled editing permissions
  let isOwner = isArticleOwner(currentUser, article?.author?._id);

  // Check if user is owner or captain of organization
  if (isArticleOrganizationAdmin(currentUserOrgs, article?.author?._id)) {
    isOwner = true;
  }

  // article games
  const preselectedGames = article?.associatedGames;

  const postedByOrganization = article?.authorType === "organizations";
  const ownerUrl = postedByOrganization
    ? `/organizations/show?id=${article?.author?._id}`
    : `/u/${article?.author?.username}`;

  return (
    <div className={darkMode ? `${Style.dark} dark` : `${Style.light} light`}>
      <NextSeo
        title={`${SSRArticle.title} | eFuse News`}
        description={sanitizeMetaDescription(`${SSRArticle.summary}`)}
        openGraph={{
          type: "website",
          url: `https://efuse.gg${SSRArticle.url}`,
          title: `${SSRArticle.title} | eFuse News`,
          site_name: "eFuse.gg",
          description: SSRArticle.summary,
          images: [{ url: SSRArticle.image.url }]
        }}
        twitter={{
          handle: "@eFuseOfficial",
          site: `https://efuse.gg${SSRArticle.url}`,
          cardType: "summary_large_image"
        }}
      />
      {article && (
        <Container>
          <Fade top distance="50px">
            <NewsArticleNavbar category={article.category} darkMode={darkMode} />
          </Fade>
          <Fade bottom distance="100px">
            <div>
              <NewsArticleTitle
                title={title}
                isOwner={isOwner}
                darkMode={darkMode}
                onChange={newTitle => setTitle(newTitle)}
                onBlur={newTitle =>
                  dispatch(
                    updateNewsArticle(article._id, {
                      title: newTitle
                    })
                  )
                }
              />
              <NewsArticleInfoBar
                articleId={article._id}
                createdAt={article.createdAt}
                authorType={article.authorType}
                author={article.author}
                image={article.image}
                traits={article.traits}
                motivations={article.motivations}
                video={article.video}
                category={article.category}
                summary={article.summary}
                status={article.status}
                url={article.url}
                readingTime={calculateReadingTime(body)}
                isOwner={isOwner}
                darkMode={darkMode}
                onDarkModeChange={isDarkMode => setDarkMode(isDarkMode)}
                onStatusChange={newStatus => {
                  dispatch(updateNewsArticle(article._id, { status: newStatus }));

                  if (newStatus === "Published") {
                    dispatch(
                      shareLinkModal(
                        true,
                        "Share News Article",
                        `https://efuse.gg${SSRArticle.url}`,
                        SHARE_LINK_KINDS.LEARNING_ARTICLE
                      )
                    );
                  }
                }}
                preselectedGames={preselectedGames}
              />
              <NewsArticleBody isOwner={isOwner} article={article} />

              <Col md={{ span: 10, offset: 1 }}>
                <NewsArticleAuthor postedByOrganization={postedByOrganization} ownerUrl={ownerUrl} article={article} />

                <NewsArticleRecommendations />
              </Col>
            </div>
          </Fade>
        </Container>
      )}
      <NewsArticleFooter />
    </div>
  );
};

export default SingleArticle;
