import React from "react";
import { Col } from "react-bootstrap";
import DarkModeToggle from "react-dark-mode-toggle";
import { faShare } from "@fortawesome/pro-solid-svg-icons";

import ShareLinkButton from "../../../ShareLinkButton";
import Style from "./NewsArticleInfoBar.module.scss";
import { SHARE_LINK_KINDS } from "../../../ShareLinkModal/ShareLinkModal";
import AuthorInfo from "./AuthorInfo/AuthorInfo";
import NewsDropdown from "./NewsDropdown/NewsDropdown";

const NewsArticleInfoBar = ({
  articleId,
  createdAt,
  authorType,
  author,
  category,
  status,
  image,
  video,
  summary,
  onStatusChange,
  url,
  onDarkModeChange,
  darkMode,
  readingTime,
  isOwner,
  traits,
  motivations,
  preselectedGames
}) => {
  return (
    <Col md={{ span: 10, offset: 1 }}>
      <div className={`${Style.infoBar} ${darkMode ? Style.dark : "light"}`}>
        <AuthorInfo author={author} authorType={authorType} createdAt={createdAt} />
        <div className={Style.rightInfoBarContainer}>
          <DarkModeToggle onChange={isDarkMode => onDarkModeChange(isDarkMode)} checked={darkMode} size="70px" />
          <div className={Style.readTime}>{readingTime} min read</div>
          {status !== "Draft" && (
            <ShareLinkButton
              title="Share Article"
              buttonText="Share"
              shareLinkKind={SHARE_LINK_KINDS.LEARNING_ARTICLE}
              style={{ marginTop: "0px", width: "96px" }}
              url={`https://efuse.gg${url}`}
              icon={faShare}
            />
          )}
          {isOwner && (
            <>
              <NewsDropdown
                articleId={articleId}
                summary={summary}
                category={category}
                image={image}
                video={video}
                onStatusChange={onStatusChange}
                traits={traits}
                motivations={motivations}
                status={status}
                author={author}
                authorType={authorType}
                preselectedGames={preselectedGames}
              />
            </>
          )}
        </div>
      </div>
    </Col>
  );
};

export default NewsArticleInfoBar;
