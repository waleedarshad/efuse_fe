import React from "react";
import { Dropdown } from "react-bootstrap";

import Modal from "../../../../Modal/Modal";

const DropdownItem = ({ modalTitle, component, itemTitle }) => {
  return (
    <Modal displayCloseButton title={modalTitle} onOpenModal={() => {}} component={component}>
      <Dropdown.Item>{itemTitle}</Dropdown.Item>
    </Modal>
  );
};

export default DropdownItem;
