import { shallow } from "enzyme";
import { Dropdown, DropdownButton } from "react-bootstrap";
import NewsDropdown from "./NewsDropdown";
import DropdownItem from "./DropdownItem";
import FEATURE_FLAGS from "../../../../../common/featureFlags";
import FeatureFlag from "../../../../General/FeatureFlag/FeatureFlag";

describe("NewsDropdown", () => {
  const onStatusChange = () => jest.fn();
  let subject;

  const author = {
    _id: "author-id-123",
    goldSubscriber: false,
    name: "Test Duder",
    online: true,
    pathway: "12345",
    profilePicture: {},
    roles: [],
    showOnline: true,
    showOnlineMobile: true,
    username: "testduder",
    verified: true
  };
  const image = {
    contentType: "image/jpg",
    filename: "no_image.jpg",
    url: "https://some/url/test"
  };
  const category = {
    _id: "category-id",
    createdAt: "",
    image,
    isActive: true,
    name: "Health and Wellness",
    slug: "health-and-wellness",
    user: "test"
  };

  const dropdown = (
    <NewsDropdown
      articleId="article-id-123"
      summary=""
      category={category}
      image={image}
      video={undefined}
      onStatusChange={onStatusChange}
      traits={[]}
      motivations={[]}
      status="Draft"
      author={author}
    />
  );

  beforeEach(() => {
    subject = shallow(dropdown);
  });

  it("renders with the correct feature flag for article games", () => {
    expect(subject.find(FeatureFlag).prop("name")).toEqual(FEATURE_FLAGS.FEATURE_WEB_ADD_GAMES_TO_NEW_ARTICLES);
  });

  it("renders a dropdownbutton for draft dropdown", () => {
    expect(subject.find(DropdownButton)).toHaveLength(1);
  });

  it("renders each option in dropdownbutton", () => {
    expect(
      subject
        .find(DropdownButton)
        .find(Dropdown.Item)
        .at(0)
        .text()
    ).toEqual("Draft");
    expect(
      subject
        .find(DropdownButton)
        .find(Dropdown.Item)
        .at(1)
        .text()
    ).toEqual("Published");
    expect(
      subject
        .find(DropdownButton)
        .find(Dropdown.Item)
        .at(2)
        .text()
    ).toEqual("Unlisted");
  });

  it("renders a dropdown for three dots dropdown", () => {
    expect(subject.find(Dropdown)).toHaveLength(1);
  });

  it("renders each option in dropdown", () => {
    expect(
      subject
        .find(DropdownItem)
        .at(0)
        .props().itemTitle
    ).toEqual("Add main video");
    expect(
      subject
        .find(DropdownItem)
        .at(1)
        .props().itemTitle
    ).toEqual("Change author");
    expect(
      subject
        .find(DropdownItem)
        .at(2)
        .props().itemTitle
    ).toEqual("Change category");
    expect(
      subject
        .find(DropdownItem)
        .at(3)
        .props().itemTitle
    ).toEqual("Edit summary card");
    expect(
      subject
        .find(DropdownItem)
        .at(4)
        .props().itemTitle
    ).toEqual("Edit tags");
    expect(
      subject
        .find(DropdownItem)
        .at(5)
        .props().itemTitle
    ).toEqual("Update related game(s)");
  });
});
