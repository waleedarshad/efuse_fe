/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable react/display-name */
import React, { forwardRef } from "react";
import { Dropdown, DropdownButton } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEllipsisV } from "@fortawesome/pro-solid-svg-icons";

import ChangeAuthorModal from "../../Modals/ChangeAuthorModal";
import ChangeCategoryModal from "../../Modals/ChangeCategoryModal";
import ChangeSummaryCardModal from "../../Modals/ChangeSummaryCardModal";
import ChangeMainVideoModal from "../../Modals/ChangeMainVideoModal";
import ChangeTraitsMotivationsModal from "../../Modals/ChangeTraitsMotivationsModal";
import DropdownItem from "./DropdownItem";
import Style from "../NewsArticleInfoBar.module.scss";
import FEATURE_FLAGS from "../../../../../common/featureFlags";
import FeatureFlag from "../../../../General/FeatureFlag/FeatureFlag";
import FeatureFlagVariant from "../../../../General/FeatureFlag/FeatureFlagVariant/FeatureFlagVariant";
import ChangeArticleGamesModal from "../../../NewArticleModal/ChangeArticleGamesModal";

const NewsDropdown = ({
  articleId,
  summary,
  category,
  image,
  video,
  onStatusChange,
  traits,
  motivations,
  status,
  author,
  authorType,
  preselectedGames
}) => {
  const CustomToggle = forwardRef(({ children, onClick }, ref) => (
    <a
      href=""
      ref={ref}
      onClick={e => {
        e.preventDefault();
        onClick(e);
      }}
    >
      {children}
      <FontAwesomeIcon icon={faEllipsisV} className={Style.moreDropdownIcon} />
    </a>
  ));

  return (
    <>
      <DropdownButton title={status}>
        <Dropdown.Header>Article Status</Dropdown.Header>
        <Dropdown.Item eventKey="1" active={status === "Draft"} onClick={() => onStatusChange("Draft")}>
          Draft
        </Dropdown.Item>
        <Dropdown.Item eventKey="2" active={status === "Published"} onClick={() => onStatusChange("Published")}>
          Published
        </Dropdown.Item>
        <Dropdown.Item eventKey="3" active={status === "Unlisted"} onClick={() => onStatusChange("Unlisted")}>
          Unlisted
        </Dropdown.Item>
      </DropdownButton>
      <Dropdown>
        <Dropdown.Toggle as={CustomToggle} />
        <Dropdown.Menu size="sm" title="">
          <Dropdown.Header>Options</Dropdown.Header>
          <DropdownItem
            modalTitle={video ? "Change Main Video" : "Add Main Video"}
            component={<ChangeMainVideoModal articleId={articleId} currentVideo={video} />}
            itemTitle={`${video ? "Change" : "Add"} main video`}
          />
          <DropdownItem
            modalTitle="Change Author"
            component={<ChangeAuthorModal currentAuthorId={author._id} authorType={authorType} articleId={articleId} />}
            itemTitle="Change author"
          />
          <DropdownItem
            modalTitle="Change Category"
            component={<ChangeCategoryModal articleId={articleId} currentCategoryId={category._id} />}
            itemTitle="Change category"
          />
          <DropdownItem
            modalTitle="Change Summary Card"
            component={<ChangeSummaryCardModal articleId={articleId} currentSummary={summary} currentImage={image} />}
            itemTitle="Edit summary card"
          />
          <DropdownItem
            modalTitle="Article Tags"
            component={
              <ChangeTraitsMotivationsModal
                articleId={articleId}
                initialTraits={traits || []}
                initialMotivations={motivations || []}
              />
            }
            itemTitle="Edit tags"
          />
          <FeatureFlag name={FEATURE_FLAGS.FEATURE_WEB_ADD_GAMES_TO_NEW_ARTICLES}>
            <FeatureFlagVariant flagState>
              <DropdownItem
                modalTitle=""
                component={
                  <ChangeArticleGamesModal
                    articleId={articleId}
                    preselectedGames={preselectedGames}
                    title="Select Game(s)"
                  />
                }
                itemTitle="Update related game(s)"
              />
            </FeatureFlagVariant>
          </FeatureFlag>
        </Dropdown.Menu>
      </Dropdown>
    </>
  );
};

export default NewsDropdown;
