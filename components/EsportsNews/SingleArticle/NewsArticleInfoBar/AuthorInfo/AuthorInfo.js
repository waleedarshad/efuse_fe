import React from "react";
import Link from "next/link";

import EFAvatar from "../../../../EFAvatar/EFAvatar";
import { getImage, formatDate } from "../../../../../helpers/GeneralHelper";
import Style from "../NewsArticleInfoBar.module.scss";

const AuthorInfo = ({ author, createdAt, authorType }) => {
  const postedByOrganization = authorType === "organizations";
  const ownerUrl = postedByOrganization ? `/organizations/show?id=${author._id}` : `/u/${author.username}`;
  return (
    <div className={Style.authorContainer}>
      <div className={Style.authorDateContainer}>
        <Link href={postedByOrganization ? ownerUrl : "/u/[u]"} as={ownerUrl}>
          <div className="mr-3">
            <EFAvatar
              displayOnlineButton={false}
              profilePicture={getImage(authorType === "users" ? author.profilePicture : author.profileImage, "avatar")}
              size="small"
            />
          </div>
        </Link>
        <div>
          <Link href={postedByOrganization ? ownerUrl : "/u/[u]"} as={ownerUrl}>
            <span className={Style.authorLink}>{author.name}</span>
          </Link>
          <div className={Style.dateText}>{formatDate(createdAt) || ""}</div>
        </div>
      </div>
    </div>
  );
};

export default AuthorInfo;
