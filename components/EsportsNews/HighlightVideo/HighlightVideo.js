import React from "react";

import EFCard from "../../Cards/EFCard/EFCard";
import EFResponsiveChip from "../../EFResponsiveChip/EFResponsiveChip";
import TwitchPlayerIframe from "../../TwitchPlayerIframe/TwitchPlayerIframe";
import VideoPlayer from "../../VideoPlayer/VideoPlayer";
import Style from "./HighlightVideo.module.scss";

const HighlightVideo = ({ video }) => {
  const getPlayer = () => {
    if (video?.twitchChannel) {
      return <TwitchPlayerIframe twitchChannel={video?.twitchChannel} autoplay={false} muted />;
    }
    if (video?.youtubeLink) {
      return <VideoPlayer url={video?.youtubeLink} autoplay={false} height={370} muted />;
    }
    return <></>;
  };

  return (
    <div className={Style.highlightVideoSection}>
      <EFCard shadow="none" widthTheme="fullWidth">
        <div className={Style.cardContent}>
          <p className={Style.title}>Featured Event</p>
          <div className={Style.videoContainer}>
            {video?.twitchChannel && (
              <>
                <span className={`${Style.chip} ${Style.liveChip}`}>
                  <EFResponsiveChip badgeType="LIVE" />
                </span>
                <span className={`${Style.chip} ${Style.erenaChip}`}>
                  <EFResponsiveChip badgeType="eRena" />
                </span>
              </>
            )}
            {getPlayer()}
          </div>
        </div>
      </EFCard>
    </div>
  );
};

export default HighlightVideo;
