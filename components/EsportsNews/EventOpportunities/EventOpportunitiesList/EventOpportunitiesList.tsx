import React from "react";
import { connectInfiniteHits } from "react-instantsearch-dom";

import { triggerDebouncedSegmentEvent } from "../../../../helpers/AlgoliaHelper";
import EFShowMoreButton from "../../../Buttons/EFShowMoreButton/EFShowMoreButton";
import EFListCard from "../../../Cards/EFListCard/EFListCard";
import Style from "./EventOpportunitiesList.module.scss";

const EventOpportunitiesList = connectInfiniteHits(({ hits, hasMore, refineNext, indexName }) => {
  triggerDebouncedSegmentEvent(hits, "NEWS_CARD_LIST_VIEW", indexName);
  return (
    <div className={Style.opportunitiesWrapper}>
      {hits.map(hit => {
        return (
          <div className={Style.cardWrapper} key={hit.objectID}>
            <EFListCard
              backgroundImage={hit?.image?.url}
              liveChip={false}
              categoryChip="Event"
              title={hit?.title}
              date={hit?.createdAt}
              owner={hit?.author?.name}
              backgroundImageAltText={hit?.title}
              href={`/o/${hit?.shortName}`}
              timeAgo
              size="large"
              onClick={() => {}}
            />
          </div>
        );
      })}
      {hasMore && (
        <div className={Style.showMoreButton}>
          <EFShowMoreButton onClick={refineNext} text="Show more" />
        </div>
      )}
    </div>
  );
});

export default EventOpportunitiesList;
