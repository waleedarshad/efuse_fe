import React from "react";
import { useSelector } from "react-redux";
import { InstantSearch, Configure } from "react-instantsearch-dom";

import { returnAlgoliaSearchClient, getIndexName } from "../../../helpers/AlgoliaHelper";

import EventOpportunitiesList from "./EventOpportunitiesList/EventOpportunitiesList";

const EventOpportunities = () => {
  const currentUser = useSelector(state => state.auth.currentUser);

  const algoliaIndex = getIndexName("OPPORTUNITIES");

  return (
    <InstantSearch searchClient={returnAlgoliaSearchClient()} indexName={algoliaIndex}>
      <Configure
        userToken={currentUser?._id}
        hitsPerPage={5}
        distinct={false}
        analytics={false}
        filters="opportunityType:Event"
      />
      <EventOpportunitiesList indexName={algoliaIndex} />
    </InstantSearch>
  );
};

export default EventOpportunities;
