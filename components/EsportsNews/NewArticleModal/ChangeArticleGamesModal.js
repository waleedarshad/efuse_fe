import React, { useState } from "react";
import { useDispatch } from "react-redux";
import InputLabel from "../../InputLabel/InputLabel";
import EFGameSelect from "../../EFGameSelect/EFGameSelect";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";
import Style from "../SingleArticle/NewsArticleInfoBar/NewsArticleInfoBar.module.scss";
import { updateNewsArticleGames } from "../../../store/actions/newsActions";

const ChangeArticleGamesModal = ({ articleId, preselectedGames, title, closeModal }) => {
  const dispatch = useDispatch();
  const [selectedGames, setSelectedGames] = useState([]);
  // this is because EFGameSelect needs an object with id value to check if selected
  const preselectedGamesAsObjects = preselectedGames?.map(id => ({
    _id: id
  }));

  const saveArticleGamesSubmit = () => {
    const newGames = selectedGames.map(game => game._id);

    dispatch(updateNewsArticleGames(articleId, newGames));
  };

  return (
    <>
      <InputLabel className="" theme="darkColor">
        {title}
      </InputLabel>
      <EFGameSelect onSelect={games => setSelectedGames(games)} preselectedGameIds={preselectedGamesAsObjects} />
      <div className={Style.buttonWrapper}>
        <EFRectangleButton
          colorTheme="primary"
          text="Save"
          buttonType="submit"
          onClick={() => {
            saveArticleGamesSubmit();
            closeModal();
          }}
        />
      </div>
    </>
  );
};

export default ChangeArticleGamesModal;
