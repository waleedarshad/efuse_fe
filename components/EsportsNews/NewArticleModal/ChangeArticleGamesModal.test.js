import { MockedProvider } from "@apollo/client/testing";
import React from "react";
import { mountWithStore } from "../../../common/testUtils";
import EFGameSelect from "../../EFGameSelect/EFGameSelect";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";
import InputLabel from "../../InputLabel/InputLabel";
import ChangeArticleGamesModal from "./ChangeArticleGamesModal";

jest.mock("../../../store/actions/newsActions", () => ({
  updateNewsArticleGames: () => ({
    type: "mockedUpdateNewsArticleGames"
  })
}));

describe("ChangeArticleGamesModal Rendering", () => {
  const preselectedGames = ["123", "456"];
  const preselectedGameIdsAsObjects = [{ _id: "123" }, { _id: "456" }];
  let state;

  beforeEach(() => {
    state = {};
  });

  it("renders with the correct title, preselectedGameIds and button", () => {
    const { subject } = mountWithStore(
      <MockedProvider>
        <ChangeArticleGamesModal preselectedGames={preselectedGames} title="Select Game(s)" />
      </MockedProvider>,
      state
    );

    expect(subject.find(InputLabel).prop("children")).toEqual("Select Game(s)");

    expect(subject.find(EFGameSelect).prop("preselectedGameIds")).toEqual(preselectedGameIdsAsObjects);

    expect(subject.find(EFRectangleButton).prop("text")).toEqual("Save");
  });
});
