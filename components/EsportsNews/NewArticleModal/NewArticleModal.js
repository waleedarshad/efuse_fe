import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useRouter } from "next/router";
import { Form, Spinner } from "react-bootstrap";
import CharacterCounter from "react-character-counter";

import FEATURE_FLAGS from "../../../common/featureFlags";
import FeatureFlag from "../../General/FeatureFlag/FeatureFlag";
import InputField from "../../InputField/InputField";
import SelectBox from "../../SelectBox/SelectBox";
import { getCategories, createNewsArticle } from "../../../store/actions/newsActions";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";
import { getPostableOrganizations } from "../../../store/actions/userActions";
import InputWrapper from "./InputWrapper/InputWrapper";
import { validateSlug } from "../../../helpers/FormHelper";
import Style from "./NewArticleModal.module.scss";
import EFGameSelect from "../../EFGameSelect/EFGameSelect";
import InputLabel from "../../InputLabel/InputLabel";
import FeatureFlagVariant from "../../General/FeatureFlag/FeatureFlagVariant/FeatureFlagVariant";

const NewArticleModal = () => {
  const dispatch = useDispatch();
  const router = useRouter();
  const [title, setTitle] = useState("");
  const [slug, setSlug] = useState("");
  const [category, setCategory] = useState("");
  const [author, setAuthor] = useState("");
  const [validated, setValidated] = useState(false);
  const [isUrlValid, setIsUrlValid] = useState(true);
  const [invalidUrlMessage, setInvalidUrlMessage] = useState("");
  const [selectedGames, setSelectedGames] = useState([]);

  const categories = useSelector(state => state.news.categories);
  const currentUserOrgs = useSelector(state => state.user.postableOrganizations);
  const submittingNewArticle = useSelector(state => state.news.submittingNewArticle);
  const currentUser = useSelector(state => state.auth.currentUser);

  let formatedCategories = [];
  categories.map(articleCategory => {
    return formatedCategories.push({ value: articleCategory._id, label: articleCategory.name });
  });

  formatedCategories = [{ label: "Select one", value: "" }, ...formatedCategories];

  const selectedCategory = categories.filter(c => c._id === category);

  let categorySlug = "category";

  if (selectedCategory.length === 1) categorySlug = selectedCategory[0].slug;

  let formatedAuthors = [];

  if (currentUserOrgs) {
    currentUserOrgs.map(org => {
      return formatedAuthors.push({ value: org._id, label: org.name });
    });
  }

  if (currentUser?.id) {
    formatedAuthors = [
      { label: "Create article as...", value: "" },
      {
        label: currentUser.name,
        value: currentUser.id
      },
      ...formatedAuthors
    ];
  }

  useEffect(() => {
    dispatch(getCategories());
    dispatch(getPostableOrganizations());
  }, []);

  const onSubmit = e => {
    e.preventDefault();
    setValidated(true);
    const formValid = e.currentTarget.checkValidity();
    const authorType = currentUserOrgs.find(org => {
      return org._id === author;
    })
      ? "organizations"
      : "users";
    if (formValid) {
      const articleData = new FormData();
      articleData.append("slug", slug);
      articleData.append("category", category);
      articleData.append("summary", "");
      articleData.append(
        "body",
        "<h2>New News Article</h2><p>Start typing here to create your article. You can use the " +
          " button above to add images, headers, lists, videos or other components.&nbsp;</p><p>Your changes will be saved as you go, so you can start and stop anytime.</p><p>When you're ready, publish your article for the world to see.</p>"
      );
      articleData.append("title", title);
      articleData.append("author", author);
      articleData.append("authorType", authorType);

      selectedGames?.map(game => articleData.append("games[]", game._id));

      dispatch(createNewsArticle(articleData, router));
    }
  };

  const validateArticleSlug = async event => {
    const { valid, error } = await validateSlug(event.target.value, "learningarticles");

    setIsUrlValid(valid);
    setInvalidUrlMessage(error);
  };

  return (
    <Form noValidate validated={validated} role="form" onSubmit={onSubmit}>
      <InputWrapper
        inputLabel="Title"
        tooltipText="Write a short, meaningful title that summarizes the content of your article."
      >
        <CharacterCounter value={title} maxLength={70}>
          <InputField
            disabled={submittingNewArticle}
            name="title"
            placeholder="How to get a job in esports?"
            size="lg"
            theme="whiteShadow"
            onChange={e => {
              setTitle(e.target.value);
            }}
            required
            errorMessage="Title is required."
            value={title}
            maxLength={70}
          />
        </CharacterCounter>
      </InputWrapper>
      <InputWrapper inputLabel="Author" tooltipText="Select one of the available authors of your article.">
        <SelectBox
          disabled={submittingNewArticle}
          name="author"
          options={formatedAuthors}
          validated={validated}
          theme="whiteShadow"
          value={author}
          required
          onChange={e => setAuthor(e.target.value)}
          errorMessage="Author is required."
        />
      </InputWrapper>
      <InputWrapper
        inputLabel="Category"
        tooltipText="Select one of the available categories that best matches the topic of your article."
      >
        <SelectBox
          disabled={submittingNewArticle}
          name="category"
          options={formatedCategories}
          validated={validated}
          theme="whiteShadow"
          value={category}
          required
          onChange={e => setCategory(e.target.value)}
          errorMessage="Category is required."
        />
      </InputWrapper>
      <InputWrapper
        inputLabel="URL"
        tooltipText="Write a meaningful url slug with a few words as possible. Please use hyphens to separate words. Ex: /esports-in-the-news"
      >
        <InputField
          disabled={submittingNewArticle}
          prepend={`https://efuse.gg/news/${categorySlug}/`}
          name="slug"
          placeholder="url-slug"
          size="lg"
          theme="whiteShadow"
          onChange={e => setSlug(e.target.value)}
          required
          errorMessage="3-24 character alphanumeric and hyphen characters only."
          value={slug}
          maxLength={70}
          pattern="^[0-9a-zA-Z-]*$"
          style={{
            textTransform: "lowercase"
          }}
          onBlur={validateArticleSlug}
        />
        {!isUrlValid && <div className={Style.invalidUrl}>{invalidUrlMessage}</div>}
      </InputWrapper>
      <FeatureFlag name={FEATURE_FLAGS.FEATURE_WEB_ADD_GAMES_TO_NEW_ARTICLES}>
        <FeatureFlagVariant flagState>
          <InputLabel className="" theme="darkColor" optional>
            What games are you discussing in this article?
          </InputLabel>
          <EFGameSelect onSelect={games => setSelectedGames(games)} />
        </FeatureFlagVariant>
      </FeatureFlag>
      <EFRectangleButton
        disabled={submittingNewArticle || !isUrlValid}
        buttonType="submit"
        text={
          <>
            {submittingNewArticle && <Spinner animation="border" variant="light" size="sm" />}
            {submittingNewArticle ? " Submitting..." : "Create News Article"}
          </>
        }
        shadowTheme="none"
      />
    </Form>
  );
};

export default NewArticleModal;
