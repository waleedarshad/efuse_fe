import mockAxios from "axios";
import React from "react";
import NewArticleModal from "./NewArticleModal";
import FEATURE_FLAGS from "../../../common/featureFlags";
import FeatureFlag from "../../General/FeatureFlag/FeatureFlag";

import { mountWithStore } from "../../../common/testUtils";

describe("NewArticleModal Rendering Fields", () => {
  let subject;
  const state = {
    auth: {
      currentUser: {}
    },
    features: {
      article_games: "article_games"
    },
    news: {
      categories: [],
      submittingNewArticle: false
    },
    user: {
      postableOrganizations: []
    }
  };

  const categoriesData = [
    {
      __typename: "some category",
      _id: "538df5e8fefd5feff8",
      name: "Health Things",
      slug: "health-things-slug"
    }
  ];

  beforeEach(() => {
    mockAxios.get.mockResolvedValue({ data: categoriesData });

    const wrapper = mountWithStore(<NewArticleModal />, state);

    subject = wrapper.subject;
  });

  it("renders with the correct feature flag for article games", () => {
    expect(subject.find(FeatureFlag).prop("name")).toEqual(FEATURE_FLAGS.FEATURE_WEB_ADD_GAMES_TO_NEW_ARTICLES);
  });

  it("renders the input field for title", () => {
    expect(
      subject
        .find("input")
        .at(0)
        .props().placeholder
    ).toEqual("How to get a job in esports?");
  });

  it("renders the select box for author", () => {
    expect(
      subject
        .find("select")
        .at(0)
        .props().name
    ).toBe("author");
  });

  it("renders the select box for category", () => {
    expect(
      subject
        .find("select")
        .at(1)
        .props().name
    ).toBe("category");
  });

  it("renders the input field for slug value", () => {
    expect(
      subject
        .find("input")
        .at(1)
        .props().placeholder
    ).toEqual("url-slug");
  });

  it("renders all the labels correctly", () => {
    expect(subject.text().includes("Title")).toBe(true);
    expect(subject.text().includes("Author")).toBe(true);
    expect(subject.text().includes("Category")).toBe(true);
    expect(subject.text().includes("URL")).toBe(true);
  });

  it("renders the news artical component button", () => {
    expect(subject.find("button").text()).toContain("Create News Article");
  });
});
