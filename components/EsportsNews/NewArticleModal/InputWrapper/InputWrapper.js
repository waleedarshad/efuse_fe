import React from "react";
import { Col } from "react-bootstrap";

import InputRow from "../../../InputRow/InputRow";
import InputLabelWithHelpTooltip from "../../../InputLabelWithHelpTooltip/InputLabelWithHelpTooltip";

const InputWrapper = ({ children, inputLabel, tooltipText }) => {
  return (
    <InputRow>
      <Col sm={12}>
        <InputLabelWithHelpTooltip labelText={inputLabel} tooltipText={tooltipText} />
        {children}
      </Col>
    </InputRow>
  );
};

export default InputWrapper;
