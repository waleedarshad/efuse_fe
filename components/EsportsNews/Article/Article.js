import { Card } from "react-bootstrap";
import PropTypes from "prop-types";
import TimeAgo from "react-timeago";

import Style from "./Article.module.scss";
import { rescueNil } from "../../../helpers/GeneralHelper";
import { withCdn } from "../../../common/utils";

const Article = ({ filterHandler, title, showContent, noRadius, article, applyHover, isActive, size }) => {
  const displayableImage = article ? article.image.url : "";
  const displayableTitle = article ? article.title : title;

  return (
    <Card
      onClick={filterHandler}
      className={`${Style.articleWrapper} ${size === "small" && Style.small} ${noRadius &&
        Style.noRadius} text-white ${isActive && Style.active} ${applyHover && Style.articleHover} ${size == "large" &&
        Style.largeSize}`}
    >
      {displayableImage ? (
        <div
          className={`${Style.imageMain} ${size === "large" && Style.imageMainLarge} ${size === "small" &&
            Style.imageMainSmall}`}
          style={{ backgroundImage: `url(${displayableImage})` }}
        />
      ) : (
        <Card.Img className={Style.placeholder} src={withCdn("/static/images/placeholder.png")} alt="Card image" />
      )}
      {showContent && (
        <Card.Body className={`${Style.overlay}`}>
          <Card.Title className={Style.title}>
            {article ? (
              <a
                className={Style.titleLink}
                href="#"
                onClick={() => {
                  analytics.track(
                    "DISCOVER_ARTICLE_CLICKED",
                    {
                      type: "FEATURED_TOP",
                      article
                    },
                    {},
                    () => {
                      window.open(rescueNil(article, "website"), "_news");
                    }
                  );
                }}
              >
                {displayableTitle}
              </a>
            ) : (
              <span className={`${Style.titleLink} ${Style.titleNoLink}`}>{displayableTitle}</span>
            )}
          </Card.Title>
          {article && (
            <div className={Style.textWrapper}>
              <Card.Text className={Style.author}>{article.author.name}</Card.Text>
              <Card.Text className={Style.text}>
                <TimeAgo date={article.publishedAt} minPeriod={30} />
              </Card.Text>
            </div>
          )}
        </Card.Body>
      )}
    </Card>
  );
};

Article.propTypes = {
  sm: PropTypes.bool,
  showContent: PropTypes.bool,
  noRadius: PropTypes.bool,
  article: PropTypes.object,
  applyHover: PropTypes.bool,
  filterHandler: PropTypes.func,
  image: PropTypes.string,
  title: PropTypes.string,
  isActive: PropTypes.bool
};

Article.defaultProps = {
  sm: false,
  showContent: true,
  noRadius: false,
  applyHover: false,
  isActive: false
};

export default Article;
