import InfiniteScroll from "react-infinite-scroller";
import isEmpty from "lodash/isEmpty";

import AnimatedLogo from "../../AnimatedLogo";
import WideCard from "../../Cards/WideCard/WideCard";
import Style from "./ArticleListing.module.scss";

const ArticleListing = ({ articles, loadMore, hasMore }) => {
  if (isEmpty(articles)) {
    return <></>;
  }

  const newsItems = articles.map(article => {
    const postedByOrganization = article?.authorType === "organizations";
    const ownerUrl = postedByOrganization
      ? `/organizations/show?id=${article?.author?._id}`
      : `/u/${article?.author?.username}`;
    return (
      <div className={Style.marginBottom} key={article?._id}>
        <WideCard
          displayImage
          imageUrl={article?.image?.url}
          title={article?.title}
          href={article?.url}
          name={article?.author.name}
          nameRedirect={ownerUrl}
          label={article?.status}
          displayLabel={article.status === "Draft"}
          summary={article?.summary}
        />
      </div>
    );
  });

  return (
    <InfiniteScroll
      pageStart={1}
      loadMore={loadMore}
      hasMore={hasMore}
      loader={<AnimatedLogo key={0} theme="inline" />}
    >
      {newsItems}
    </InfiniteScroll>
  );
};

export default ArticleListing;
