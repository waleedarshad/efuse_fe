import { Col, Row } from "react-bootstrap";

import TimeAgo from "react-timeago";
import Link from "next/link";
import Style from "./ArticleListItem.module.scss";
import { EFHtmlParser } from "../../../EFHtmlParser/EFHtmlParser";

const ArticleListItem = ({ article }) => {
  return (
    <Link href={`/learning/${article.slug}`} passHref className={Style.showCursor}>
      <Row className={`mb-4 ${Style.showCursor}`}>
        <Col sm={4}>
          <div style={{ backgroundImage: `url('${article.image.url}')` }} className={Style.leftImage} />
        </Col>
        <Col sm={8} className={Style.contentWrapper}>
          <h5 className={Style.title}>{article.title}</h5>
          {article.author && <h6 className={Style.author}>{article.author.name}</h6>}
          <p className={Style.description}>
            <EFHtmlParser>{article.summary}</EFHtmlParser>
          </p>
          <TimeAgo className={Style.time} date={article.publishedAt} minPeriod={30} />
        </Col>
      </Row>
    </Link>
  );
};

export default ArticleListItem;
