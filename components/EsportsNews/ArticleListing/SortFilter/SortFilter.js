import React from "react";
import { Col } from "react-bootstrap";

import InputRow from "../../../InputRow/InputRow";
import SelectBox from "../../../SelectBox/SelectBox";

const SortFilter = () => {
  return (
    <InputRow>
      <Col sm={4}>
        <SelectBox
          validated={false}
          options={[{ value: "", label: "Popular News" }]}
          size="lg"
          theme="borderLess"
          block="true"
        />
      </Col>
    </InputRow>
  );
};

export default SortFilter;
