import EFAlert from "./EFAlert";

export default {
  title: "Shared/EFAlert",
  component: EFAlert,
  argTypes: {
    title: {
      type: "text"
    },
    message: {
      type: "text"
    },
    confirmLabel: {
      type: "text"
    },
    cancelLabel: {
      type: "text"
    }
  }
};

const Story = args => <div>{EFAlert("Confirm", "Are you sure", "Yes", "No")}</div>;

export const Alert = Story.bind({});
Alert.args = {
  title: "Confir",
  message: "Are you sure",
  confirmLabel: "Yes",
  cancelLabel: "No",
  onConfirm: () => {},
  onCancel: () => {}
};
