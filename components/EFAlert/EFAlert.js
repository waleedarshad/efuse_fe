import { confirmAlert } from "react-confirm-alert";

const EFAlert = (title, message, confirmLabel, cancelLabel, onConfirm, onCancel) => {
  confirmAlert({
    title,
    message,
    buttons: [
      {
        label: confirmLabel,
        onClick: () => onConfirm()
      },
      {
        label: cancelLabel,
        onClick: () => onCancel && onCancel()
      }
    ]
  });
};
export default EFAlert;
