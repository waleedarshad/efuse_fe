import Geosuggest from "react-geosuggest";
import PropTypes from "prop-types";
import { FormControl } from "react-bootstrap";

const GoogleAutoComplete = ({
  onSuggest,
  placeholder,
  required,
  errorMsg,
  error,
  className,
  types,
  placeDetailFields,
  initialValue
}) => (
  <>
    <Geosuggest
      onSuggestSelect={onSuggest}
      placeholder={placeholder}
      required={required}
      className={className}
      types={types}
      placeDetailFields={placeDetailFields}
      initialValue={initialValue}
    />
    {error && (
      <FormControl.Feedback type="invalid" style={{ display: "block" }}>
        {errorMsg}
      </FormControl.Feedback>
    )}
  </>
);

GoogleAutoComplete.propTypes = {
  onSuggest: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
  required: PropTypes.bool,
  className: PropTypes.string,
  types: PropTypes.array,
  placeDetailFields: PropTypes.array,
  errorMsg: PropTypes.string,
  error: PropTypes.bool,
  initialValue: PropTypes.string
};

GoogleAutoComplete.defaultProps = {
  placeholder: "Enter Location",
  required: false,
  types: ["(cities)"],
  placeDetailFields: ["geometry"],
  error: false
};

export default GoogleAutoComplete;
