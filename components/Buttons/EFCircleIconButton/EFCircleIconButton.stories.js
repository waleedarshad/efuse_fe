import React from "react";
import { faComments } from "@fortawesome/pro-solid-svg-icons";
import EFCircleIconButton from "./EFCircleIconButton";

export default {
  title: "Buttons/EFCircleIconButton",
  component: EFCircleIconButton,
  argTypes: {
    buttonType: {
      control: {
        type: "select",
        options: ["button", "submit", "reset"]
      }
    },
    icon: {
      control: {
        type: "select",
        options: ["comments", "share", "eye", "handshake"]
      }
    },
    colorTheme: {
      control: {
        type: "select",
        options: ["light", "primary", "secondary", "dark"]
      }
    },
    disabled: {
      control: {
        type: "boolean"
      }
    },
    shadowTheme: {
      control: {
        type: "select",
        options: ["none", "small", "medium", "large"]
      }
    },
    size: {
      control: {
        type: "select",
        options: ["small", "medium", "large"]
      }
    },
    fontWeightTheme: {
      control: {
        type: "select",
        options: ["normal", "bold", "extraBold"]
      }
    }
  }
};

const Story = args => <EFCircleIconButton {...args} />;

export const Light = Story.bind({});
Light.args = {
  buttonType: "button",
  icon: faComments,
  colorTheme: "light",
  shadowTheme: "medium",
  size: "large"
};

export const Dark = Story.bind({});
Dark.args = {
  buttonType: "button",
  icon: faComments,
  colorTheme: "dark",
  shadowTheme: "medium",
  size: "large"
};

export const Primary = Story.bind({});
Primary.args = {
  buttonType: "button",
  icon: faComments,
  colorTheme: "primary",
  shadowTheme: "medium",
  size: "large"
};

export const Secondary = Story.bind({});
Secondary.args = {
  buttonType: "button",
  icon: faComments,
  colorTheme: "secondary",
  shadowTheme: "medium",
  size: "large"
};

export const Transparent = Story.bind({});
Transparent.args = {
  buttonType: "button",
  icon: faComments,
  colorTheme: "transparent",
  shadowTheme: "none",
  size: "large"
};
