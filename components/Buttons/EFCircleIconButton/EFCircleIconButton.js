import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import EFButton from "../EFButton/EFButton";
import Style from "./EFCircleIconButton.module.scss";

const EFCircleIconButton = ({
  onClick,
  buttonType,
  disabled,
  internalHref,
  internalAs,
  externalHref,
  externalTarget,
  icon,
  colorTheme,
  shadowTheme,
  size,
  fontColorTheme,
  alignContent,
  width,
  text
}) => {
  return (
    <div className={`${Style.circleButton} ${Style[size]}`}>
      <EFButton
        buttonType={buttonType}
        disabled={disabled}
        internalHref={internalHref}
        internalAs={internalAs}
        externalHref={externalHref}
        externalTarget={externalTarget}
        onClick={onClick}
        colorTheme={colorTheme}
        shadowTheme={shadowTheme}
        fontColorTheme={fontColorTheme}
        alignContent={alignContent}
        width={width}
      >
        <div className={Style.contentWrapper}>
          <FontAwesomeIcon icon={icon} className={Style.iconStyle} title={icon.iconName} />
          {text && <p className={Style.text}>{text}</p>}
        </div>
      </EFButton>
    </div>
  );
};

EFCircleIconButton.defaultProps = {
  colorTheme: "light",
  shadowTheme: "medium",
  size: "large",
  text: ""
};

export default EFCircleIconButton;
