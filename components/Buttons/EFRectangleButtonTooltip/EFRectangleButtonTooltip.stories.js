import React from "react";
import EFRectangleButtonTooltip from "./EFRectangleButtonTooltip";

export default {
  title: "Buttons/EFRectangleButtonTooltip",
  component: EFRectangleButtonTooltip,
  argTypes: {
    tooltipPlacement: {
      control: {
        type: "select",
        options: ["top", "bottom", "left", "right"]
      }
    },
    tooltipText: {
      control: {
        type: "text"
      }
    }
  }
};

const Story = args => <EFRectangleButtonTooltip {...args} />;

export const Basic = Story.bind({});
Basic.args = {
  buttonType: "button",
  text: "Click Me!",
  colorTheme: "light",
  shadowTheme: "medium",
  size: "large",
  tooltipText: "Click Me!",
  tooltipPlacement: "bottom"
};
