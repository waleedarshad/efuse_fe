import React from "react";
import EFRectangleButton from "../EFRectangleButton/EFRectangleButton";
import EFTooltip from "../../tooltip/EFTooltip/EFTooltip";

const EFRectangleButtonTooltip = ({
  onClick,
  buttonType,
  disabled,
  internalHref,
  internalAs,
  externalHref,
  externalTarget,
  icon,
  colorTheme,
  fontColorTheme,
  fontWeightTheme,
  shadowTheme,
  size,
  tooltipText,
  tooltipPlacement,
  tooltipDelay,
  text,
  width,
  alignContent
}) => {
  return (
    <div style={{ display: "inline-block" }}>
      <EFTooltip tooltipPlacement={tooltipPlacement} delay={tooltipDelay} tooltipContent={tooltipText}>
        <div>
          <EFRectangleButton
            text={text}
            onClick={onClick}
            buttonType={buttonType}
            disabled={disabled}
            internalHref={internalHref}
            internalAs={internalAs}
            externalHref={externalHref}
            externalTarget={externalTarget}
            icon={icon}
            colorTheme={colorTheme}
            fontColorTheme={fontColorTheme}
            fontWeightTheme={fontWeightTheme}
            shadowTheme={shadowTheme}
            size={size}
            width={width}
            alignContent={alignContent}
          />
        </div>
      </EFTooltip>
    </div>
  );
};

EFRectangleButtonTooltip.defaultProps = {
  tooltipPlacement: "bottom",
  tooltipText: "text"
};

export default EFRectangleButtonTooltip;
