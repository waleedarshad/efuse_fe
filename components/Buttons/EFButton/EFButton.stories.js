import React from "react";
import EFButton from "./EFButton";

export default {
  title: "Buttons/EFButton",
  component: EFButton,
  argTypes: {
    buttonType: {
      control: {
        type: "select",
        options: ["button", "submit", "reset"]
      }
    },
    content: {
      control: {
        type: "text"
      }
    },
    disabled: {
      control: {
        type: "boolean"
      }
    },
    internalHref: {
      control: {
        type: "text"
      }
    },
    internalAs: {
      control: {
        type: "text"
      }
    },
    externalHref: {
      control: {
        type: "text"
      }
    },
    externalTarget: {
      control: {
        type: "text"
      }
    },
    colorTheme: {
      control: {
        type: "select",
        options: ["light", "primary", "secondary", "transparent", "dark"]
      }
    },
    shadowTheme: {
      control: {
        type: "select",
        options: ["none", "small", "medium", "large"]
      }
    },
    fontWeightTheme: {
      control: {
        type: "select",
        options: ["normal", "bold", "extraBold"]
      }
    },
    fontCaseTheme: {
      control: {
        type: "select",
        options: ["normalCase", "upperCase"]
      }
    },
    alignContent: {
      control: {
        type: "select",
        options: ["center", "left", "right"]
      }
    },
    width: {
      control: {
        type: "select",
        options: ["fullWidth", "autoWidth", "fitContent"]
      }
    }
  }
};

// eslint-disable-next-line react/jsx-props-no-spreading
const Story = args => <EFButton {...args} />;

export const Basic = Story.bind({});
Basic.args = {
  buttonType: "button",
  children: "Click Me!",
  internalHref: "",
  internalAs: "",
  externalHref: "",
  externalTarget: ""
};

export const ElementAsContent = Story.bind({});
ElementAsContent.args = {
  buttonType: "button",
  children: <div style={{ color: "red" }}>Click Me!</div>,
  internalHref: "",
  internalAs: "",
  externalHref: "",
  externalTarget: ""
};

export const External = Story.bind({});
External.args = {
  buttonType: "button",
  children: "Click Me!",
  internalHref: "",
  internalAs: "",
  externalHref: "https://www.efuse.gg",
  externalTarget: "_blank"
};

export const Internal = Story.bind({});
Internal.args = {
  buttonType: "button",
  children: "Click Me!",
  internalHref: "/lounge/[type]",
  internalAs: "/lounge/featured",
  externalHref: "",
  externalTarget: ""
};

export const Light = Story.bind({});
Light.args = {
  buttonType: "button",
  children: "Click Me!",
  colorTheme: "light",
  shadowTheme: "medium"
};

export const Dark = Story.bind({});
Dark.args = {
  buttonType: "button",
  children: "Click Me!",
  colorTheme: "dark",
  shadowTheme: "medium"
};

export const Primary = Story.bind({});
Primary.args = {
  buttonType: "button",
  children: "Click Me!",
  colorTheme: "primary",
  shadowTheme: "medium"
};

export const Secondary = Story.bind({});
Secondary.args = {
  buttonType: "button",
  children: "Click Me!",
  colorTheme: "secondary",
  shadowTheme: "medium"
};

export const Transparent = Story.bind({});
Transparent.args = {
  buttonType: "button",
  children: "Click Me!",
  colorTheme: "transparent",
  shadowTheme: "none"
};

export const NoShadow = Story.bind({});
NoShadow.args = {
  buttonType: "button",
  children: "Click Me!",
  shadowTheme: "none"
};

export const SmallShadow = Story.bind({});
SmallShadow.args = {
  buttonType: "button",
  children: "Click Me!",
  shadowTheme: "small"
};

export const MediumShadow = Story.bind({});
MediumShadow.args = {
  buttonType: "button",
  children: "Click Me!",
  shadowTheme: "medium"
};

export const LargeShadow = Story.bind({});
LargeShadow.args = {
  buttonType: "button",
  children: "Click Me!",
  shadowTheme: "large"
};

export const FullWidthAndAlignedToLeft = Story.bind({});
FullWidthAndAlignedToLeft.args = {
  buttonType: "button",
  children: "Click Me!",
  colorTheme: "secondary",
  shadowTheme: "medium",
  width: "fullWidth",
  alignContent: "left"
};
