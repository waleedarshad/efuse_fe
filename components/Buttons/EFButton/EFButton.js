import Link from "next/link";
import PropTypes from "prop-types";
import React from "react";
import ButtonFontThemes from "./ButtonFontThemes.module.scss";
import ButtonFontColorThemes from "./ButtonFontColorThemes.module.scss";
import ButtonShadows from "./ButtonShadows.module.scss";
import ButtonThemes from "./ButtonThemes.module.scss";
import EFRipple from "../../EFRipple/EFRipple";

const EFButton = ({
  onClick,
  buttonType,
  className,
  children,
  disabled,
  internalHref,
  internalAs,
  externalHref,
  externalTarget,
  externalRel,
  colorTheme,
  shadowTheme,
  fontWeightTheme,
  fontColorTheme,
  fontCaseTheme,
  alignContent,
  width
}) => {
  let renderButton = <></>;
  const buttonComponent = (
    <button
      // eslint-disable-next-line react/button-has-type
      type={buttonType}
      onClick={onClick}
      disabled={disabled}
      className={`${ButtonThemes[colorTheme]} ${ButtonShadows[shadowTheme]} ${ButtonFontThemes[fontWeightTheme]} ${ButtonFontColorThemes[fontColorTheme]} ${ButtonFontThemes[fontCaseTheme]} ${ButtonThemes[width]} ${className}`}
    >
      <div className={ButtonThemes[alignContent]}>{children}</div>
      {!disabled && <EFRipple />}
    </button>
  );
  if (externalHref && !disabled) {
    renderButton = (
      <a href={externalHref} target={externalTarget} rel={externalRel}>
        {buttonComponent}
      </a>
    );
  } else if (internalHref && !disabled) {
    renderButton = (
      <Link href={internalHref} as={internalAs || internalHref}>
        {buttonComponent}
      </Link>
    );
  } else {
    renderButton = buttonComponent;
  }

  return renderButton;
};

EFButton.propTypes = {
  buttonType: PropTypes.oneOf(["submit", "button", "reset"]),
  alignContent: PropTypes.oneOf(["center", "left", "right"]),
  width: PropTypes.oneOf(["fullWidth", "autoWidth", "fitContent"]),
  fontCaseTheme: PropTypes.oneOf(["normalCase", "upperCase"]),
  shadowTheme: PropTypes.oneOf(["none", "small", "medium", "large"])
};

EFButton.defaultProps = {
  buttonType: "button",
  externalTarget: "_blank",
  externalRel: "",
  fontCaseTheme: "normalCase",
  alignContent: "center",
  width: "autoWidth",
  shadowTheme: "medium"
};

export default EFButton;
