import React from "react";
import EFPillButton from "./EFPillButton";
import { faComments } from "@fortawesome/pro-solid-svg-icons";

export default {
  title: "Buttons/EFPillButton",
  component: EFPillButton,
  argTypes: {
    size: {
      control: {
        type: "select",
        options: ["auto", "small", "medium", "large", "extraLarge"]
      }
    },
    width: {
      control: {
        type: "select",
        options: ["autoWidth", "fullWidth", "fitContent"]
      }
    }
  }
};

// eslint-disable-next-line react/jsx-props-no-spreading
const Story = args => <EFPillButton {...args} />;

export const Basic = Story.bind({});
Basic.args = {
  text: "Click Me!",
  buttonType: "button",
  colorTheme: "light",
  shadowTheme: "none",
  fontWeightTheme: "bold",
  fontCaseTheme: "normal"
};

export const Light = Story.bind({});
Light.args = {
  text: "Click Me!",
  buttonType: "button",
  icon: faComments,
  colorTheme: "light",
  shadowTheme: "medium",
  fontWeightTheme: "bold",
  fontCaseTheme: "normal"
};

export const Dark = Story.bind({});
Dark.args = {
  text: "Click Me!",
  buttonType: "button",
  icon: faComments,
  colorTheme: "dark",
  shadowTheme: "medium",
  fontWeightTheme: "bold",
  fontCaseTheme: "normal"
};

export const Primary = Story.bind({});
Primary.args = {
  text: "Click Me!",
  buttonType: "button",
  icon: faComments,
  colorTheme: "primary",
  shadowTheme: "medium",
  fontWeightTheme: "bold",
  fontCaseTheme: "normal"
};

export const Secondary = Story.bind({});
Secondary.args = {
  text: "Click Me!",
  buttonType: "button",
  icon: faComments,
  colorTheme: "secondary",
  shadowTheme: "medium",
  fontWeightTheme: "bold",
  fontCaseTheme: "normal"
};

export const Auto = Story.bind({});
Auto.args = {
  text: "Auto",
  buttonType: "button",
  icon: faComments,
  colorTheme: "primary",
  shadowTheme: "small",
  fontWeightTheme: "bold",
  fontCaseTheme: "normal",
  size: "auto"
};

export const Small = Story.bind({});
Small.args = {
  text: "Small",
  buttonType: "button",
  icon: faComments,
  colorTheme: "primary",
  shadowTheme: "small",
  fontWeightTheme: "bold",
  fontCaseTheme: "normal",
  size: "small"
};

export const Medium = Story.bind({});
Medium.args = {
  text: "Medium",
  buttonType: "button",
  icon: faComments,
  colorTheme: "primary",
  shadowTheme: "small",
  fontWeightTheme: "bold",
  fontCaseTheme: "normal",
  size: "medium"
};

export const Large = Story.bind({});
Large.args = {
  text: "Large",
  buttonType: "button",
  icon: faComments,
  colorTheme: "primary",
  shadowTheme: "small",
  fontWeightTheme: "bold",
  fontCaseTheme: "normal",
  size: "large"
};

export const ExtraLarge = Story.bind({});
ExtraLarge.args = {
  text: "ExtraLarge",
  buttonType: "button",
  icon: faComments,
  colorTheme: "primary",
  shadowTheme: "small",
  fontWeightTheme: "bold",
  fontCaseTheme: "normal",
  size: "extraLarge"
};
