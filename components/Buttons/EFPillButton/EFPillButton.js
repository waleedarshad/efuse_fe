import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import PropTypes from "prop-types";
import React from "react";
import EFButton from "../EFButton/EFButton";
import Style from "./EFPillButton.module.scss";

const EFPillButton = ({
  text,
  icon,
  onClick,
  buttonType,
  disabled,
  internalHref,
  internalAs,
  externalHref,
  externalTarget,
  externalRel,
  colorTheme,
  shadowTheme,
  fontWeightTheme,
  fontCaseTheme,
  width,
  alignContent,
  size,
  className
}) => {
  let content = text;
  if (icon && text) {
    content = (
      <>
        <FontAwesomeIcon icon={icon} className={Style.iconStyle} />
        {text}
      </>
    );
  }
  return (
    <div className={`${Style.pillButton} ${Style[size]}`}>
      <EFButton
        buttonType={buttonType}
        disabled={disabled}
        internalHref={internalHref}
        internalAs={internalAs}
        externalHref={externalHref}
        externalTarget={externalTarget}
        externalRel={externalRel}
        onClick={onClick}
        colorTheme={colorTheme}
        shadowTheme={shadowTheme}
        fontWeightTheme={fontWeightTheme}
        fontCaseTheme={fontCaseTheme}
        alignContent={alignContent}
        width={width}
        className={className}
      >
        {content}
      </EFButton>
    </div>
  );
};

EFPillButton.propTypes = {
  colorTheme: PropTypes.string,
  shadowTheme: PropTypes.string,
  fontWeightTheme: PropTypes.string,
  fontCaseTheme: PropTypes.string,
  size: PropTypes.oneOf(["auto", "small", "medium", "large", "extraLarge"]),
  width: PropTypes.oneOf(["autoWidth", "fullWidth", "fitContent"]),
  disabled: PropTypes.bool,
  alignContent: PropTypes.oneOf(["left", "right", "center"]),
  onClick: PropTypes.func
};

EFPillButton.defaultProps = {
  colorTheme: "primary",
  shadowTheme: "medium",
  fontWeightTheme: "bold",
  fontCaseTheme: "normalCase",
  size: "auto",
  width: "autoWidth",
  disabled: false,
  alignContent: "left",
  onClick: () => {}
};

export default EFPillButton;
