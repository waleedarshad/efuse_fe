import { faTwitch } from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import EFRectangleButton from "../EFRectangleButton/EFRectangleButton";
import Style from "./TwitchButton.module.scss";

const TwitchButton = ({ href, isLive }) => {
  return (
    <EFRectangleButton
      colorTheme="light"
      text={
        <>
          {isLive ? (
            <span className={Style.live}>LIVE</span>
          ) : (
            <FontAwesomeIcon icon={faTwitch} className={Style.twitchIcon} />
          )}
          <span className={`${Style.twitchBtnText} ${Style.btnText}`}>Twitch</span>
        </>
      }
      externalHref={href}
      externalTarget="_blank"
      className={Style.portfolioBtn}
    />
  );
};

export default TwitchButton;
