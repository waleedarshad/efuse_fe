import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronDown } from "@fortawesome/pro-solid-svg-icons";
import React from "react";
import Style from "./EFShowMoreButton.module.scss";

const EFShowMoreButton = ({ onClick, text }: { onClick: () => void; text: string }) => {
  return (
    <button className={Style.showMore} type="button" onClick={onClick}>
      {text} <FontAwesomeIcon icon={faChevronDown} />
    </button>
  );
};

export default EFShowMoreButton;
