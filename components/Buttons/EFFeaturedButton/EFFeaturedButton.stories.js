import React from "react";
import { faGamepadAlt } from "@fortawesome/pro-light-svg-icons";
import EFFeaturedButton from "./EFFeaturedButton";

export default {
  title: "Buttons/EFFeaturedButton",
  component: EFFeaturedButton,
  argTypes: {
    icon: {
      control: {
        type: "array"
      }
    },
    text: {
      control: {
        type: "text"
      }
    },
    subText: {
      control: {
        type: "text"
      }
    },
    internalHref: {
      control: {
        type: "text"
      }
    },
    externalHref: {
      control: {
        type: "text"
      }
    },

    colorTheme: {
      control: {
        type: "select",
        options: ["light", "primary", "secondary", "transparent", "dark"]
      }
    },
    shadowTheme: {
      control: {
        type: "select",
        options: ["none", "small", "medium", "large"]
      }
    },
    alignContent: {
      control: {
        type: "select",
        options: ["center", "left", "right"]
      }
    }
  }
};

// eslint-disable-next-line react/jsx-props-no-spreading
const Story = args => <EFFeaturedButton {...args} />;

export const Basic = Story.bind({});
Basic.args = {
  icon: faGamepadAlt,
  subText: "Opportunity",
  internalHref: "/opportunities/create"
};
