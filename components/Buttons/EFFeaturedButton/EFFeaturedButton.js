import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import EFButton from "../EFButton/EFButton";
import Style from "./EFFeaturedButton.module.scss";

const EFFeaturedButton = ({
  icon,
  text,
  subText,
  onClick,
  internalHref,
  externalHref,
  shadowTheme,
  colorTheme,
  alignContent,
  width
}) => {
  const content = (
    <>
      <FontAwesomeIcon className={Style.createIcon} icon={icon} />
      <div className={Style.textWrapper}>
        <p className={Style.text}>{text}</p>
        <p className={Style.subText}>{subText}</p>
      </div>
    </>
  );
  return (
    <div className={Style.createBtn}>
      <EFButton
        onClick={onClick}
        internalHref={internalHref}
        externalHref={externalHref}
        colorTheme={colorTheme}
        shadowTheme={shadowTheme}
        alignContent={alignContent}
        width={width}
      >
        {content}
      </EFButton>
    </div>
  );
};

EFFeaturedButton.defaultProps = {
  text: "CREATE",
  alignContent: "left",
  shadowTheme: "medium",
  colorTheme: "light"
};

export default EFFeaturedButton;
