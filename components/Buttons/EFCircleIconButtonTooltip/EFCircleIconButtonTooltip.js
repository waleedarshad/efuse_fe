import React from "react";
import EFCircleIconButton from "../EFCircleIconButton/EFCircleIconButton";
import EFTooltip from "../../tooltip/EFTooltip/EFTooltip";

const EFCircleIconButtonTooltip = ({
  onClick,
  buttonType,
  disabled,
  internalHref,
  internalAs,
  externalHref,
  externalTarget,
  icon,
  colorTheme,
  shadowTheme,
  size,
  tooltipContent,
  tooltipPlacement,
  fontColorTheme,
  tooltipDelay,
  alignContent,
  width
}) => {
  return (
    <div style={{ display: "inline-block" }}>
      <EFTooltip tooltipPlacement={tooltipPlacement} delay={tooltipDelay} tooltipContent={tooltipContent}>
        <div>
          <EFCircleIconButton
            onClick={onClick}
            buttonType={buttonType}
            disabled={disabled}
            internalHref={internalHref}
            internalAs={internalAs}
            externalHref={externalHref}
            externalTarget={externalTarget}
            icon={icon}
            colorTheme={colorTheme}
            shadowTheme={shadowTheme}
            size={size}
            fontColorTheme={fontColorTheme}
            alignContent={alignContent}
            width={width}
          />
        </div>
      </EFTooltip>
    </div>
  );
};

EFCircleIconButtonTooltip.defaultProps = {
  tooltipPlacement: "bottom",
  tooltipText: "",
  tooltipDelay: { show: 0, hide: 0 }
};

export default EFCircleIconButtonTooltip;
