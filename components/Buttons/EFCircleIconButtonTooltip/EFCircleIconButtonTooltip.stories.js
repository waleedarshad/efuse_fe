import React from "react";
import { faComments } from "@fortawesome/pro-solid-svg-icons";
import EFCircleIconButtonTooltip from "./EFCircleIconButtonTooltip";

export default {
  title: "Buttons/EFCircleIconButtonTooltip",
  component: EFCircleIconButtonTooltip,
  argTypes: {
    tooltipPlacement: {
      control: {
        type: "select",
        options: ["top", "bottom", "left", "right"]
      }
    },
    tooltipText: {
      control: {
        type: "text"
      }
    }
  }
};

const Story = args => <EFCircleIconButtonTooltip {...args} />;

export const Basic = Story.bind({});
Basic.args = {
  buttonType: "button",
  icon: faComments,
  colorTheme: "light",
  shadowTheme: "medium",
  size: "large",
  tooltipText: "Click Me!",
  tooltipPlacement: "bottom"
};
