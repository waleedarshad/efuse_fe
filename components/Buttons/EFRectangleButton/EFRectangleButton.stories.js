import React from "react";
import { faComments } from "@fortawesome/pro-solid-svg-icons";
import EFRectangleButton from "./EFRectangleButton";

export default {
  title: "Buttons/EFRectangleButton",
  component: EFRectangleButton,
  argTypes: {
    buttonType: {
      control: {
        type: "select",
        options: ["button", "submit", "reset"]
      }
    },
    text: {
      control: {
        type: "text"
      }
    },
    icon: {
      control: {
        type: "select",
        options: ["comments", "share", "eye", "handshake"]
      }
    },
    disabled: {
      control: {
        type: "boolean"
      }
    },
    colorTheme: {
      control: {
        type: "select",
        options: ["light", "primary", "secondary", "dark"]
      }
    },
    shadowTheme: {
      control: {
        type: "select",
        options: ["none", "small", "medium", "large"]
      }
    },
    size: {
      control: {
        type: "select",
        options: ["auto", "small", "medium", "large", "extraLarge"]
      }
    },
    width: {
      control: {
        type: "select",
        options: ["fitContent", "autoWidth", "fullWidth"]
      }
    },
    fontWeightTheme: {
      control: {
        type: "select",
        options: ["normal", "bold", "extraBold"]
      }
    }
  }
};

const Story = args => <EFRectangleButton {...args} />;

export const TextAndIcon = Story.bind({});
TextAndIcon.args = {
  text: "Click Me!",
  icon: faComments,
  buttonType: "button",
  colorTheme: "light",
  shadowTheme: "medium",
  fontWeightTheme: "bold",
  size: "large",
  width: "autoWidth"
};

export const Light = Story.bind({});
Light.args = {
  text: "Click Me!",
  buttonType: "button",
  colorTheme: "light",
  shadowTheme: "medium",
  fontWeightTheme: "bold",
  size: "large",
  width: "autoWidth"
};

export const Dark = Story.bind({});
Dark.args = {
  text: "Click Me!",
  buttonType: "button",
  colorTheme: "dark",
  shadowTheme: "medium",
  fontWeightTheme: "bold",
  size: "large",
  width: "autoWidth"
};

export const Primary = Story.bind({});
Primary.args = {
  text: "Click Me!",
  buttonType: "button",
  colorTheme: "primary",
  shadowTheme: "medium",
  fontWeightTheme: "bold",
  size: "large",
  width: "autoWidth"
};

export const Secondary = Story.bind({});
Secondary.args = {
  text: "Click Me!",
  buttonType: "button",
  colorTheme: "secondary",
  shadowTheme: "medium",
  fontWeightTheme: "bold",
  size: "large",
  width: "autoWidth"
};

export const Transparent = Story.bind({});
Transparent.args = {
  text: "Click Me!",
  buttonType: "button",
  colorTheme: "transparent",
  shadowTheme: "none",
  fontWeightTheme: "bold",
  size: "large",
  width: "autoWidth"
};
