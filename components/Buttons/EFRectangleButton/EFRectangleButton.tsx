/* eslint-disable react/require-default-props */
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import { IconProp } from "@fortawesome/fontawesome-svg-core";
import EFButton from "../EFButton/EFButton";
import Style from "./EFRectangleButton.module.scss";

interface EFRectangleButtonProps {
  text?: string | React.ReactNode;
  icon?: IconProp;
  onClick?: (event: any) => void;
  buttonType?: "submit" | "button" | "reset";
  disabled?: boolean;
  internalHref?: string;
  internalAs?: string;
  externalHref?: string;
  externalTarget?: string;
  externalRel?: string;
  colorTheme?: "light" | "primary" | "secondary" | "dark" | "transparent" | "white" | "clippy";
  fontColorTheme?: "dangerous" | "light" | "primary";
  shadowTheme?: "none" | "small" | "medium" | "large";
  fontWeightTheme?: "normal" | "bold" | "extraBold" | "normalCase" | "upperCase" | "semibold";
  fontCaseTheme?: "normalCase" | "upperCase";
  className?: string;
  size?: "auto" | "extraSmall" | "small" | "medium" | "large" | "extraLarge";
  width?: "fullWidth" | "autoWidth" | "fitContent";
  alignContent?: "center" | "left" | "right";
}

const EFRectangleButton = ({
  text,
  icon,
  onClick,
  buttonType,
  disabled,
  internalHref,
  internalAs,
  externalHref,
  externalTarget,
  externalRel,
  colorTheme = "primary",
  fontColorTheme,
  shadowTheme = "medium",
  fontWeightTheme = "bold",
  fontCaseTheme = "normalCase",
  className,
  size = "large",
  width = "fitContent",
  alignContent
}: EFRectangleButtonProps) => {
  let content = text;
  if (icon && text) {
    content = (
      <>
        <FontAwesomeIcon icon={icon} className={Style.iconStyle} />
        {text}
      </>
    );
  }
  return (
    <div className={`${Style.rectangleButton} ${Style[size]} ${icon && Style.noLetterSpacing}`}>
      <EFButton
        buttonType={buttonType}
        disabled={disabled}
        internalHref={internalHref}
        internalAs={internalAs}
        externalHref={externalHref}
        externalTarget={externalTarget}
        externalRel={externalRel}
        onClick={onClick}
        className={className}
        colorTheme={colorTheme}
        fontColorTheme={fontColorTheme}
        fontWeightTheme={fontWeightTheme}
        fontCaseTheme={fontCaseTheme}
        shadowTheme={shadowTheme}
        width={width}
        alignContent={alignContent}
      >
        {content}
      </EFButton>
    </div>
  );
};

export default EFRectangleButton;
