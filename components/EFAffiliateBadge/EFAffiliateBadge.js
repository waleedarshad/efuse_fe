import React from "react";
import { faBadgeCheck } from "@fortawesome/pro-solid-svg-icons";
import EFPostBadge from "../EFPostBadge/EFPostBadge";
import FeatureFlag from "../General/FeatureFlag/FeatureFlag";
import FeatureFlagVariant from "../General/FeatureFlag/FeatureFlagVariant/FeatureFlagVariant";
import FEATURE_FLAGS from "../../common/featureFlags";

const EFAffiliateBadge = ({ text, size }) => {
  return (
    <FeatureFlag name={FEATURE_FLAGS.FEATURE_BOTH_AFFILIATE_BUTTON}>
      <FeatureFlagVariant flagState={true}>
        <EFPostBadge icon={faBadgeCheck} colorTheme="grey" size={size} text={text} />
      </FeatureFlagVariant>
    </FeatureFlag>
  );
};

EFAffiliateBadge.defaultProps = {
  size: "small"
};

export default EFAffiliateBadge;
