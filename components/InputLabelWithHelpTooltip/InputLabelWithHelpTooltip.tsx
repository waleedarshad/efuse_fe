import React from "react";

import HelpToolTip from "../HelpToolTip/HelpToolTip";
import InputLabel from "../InputLabel/InputLabel";
import Style from "./InputLabelWithHelpTooltip.module.scss";

interface InputLabelWithHelpTooltipProps {
  labelTheme?: string;
  labelText?: string;
  tooltipText?: string;
  tooltipPlacement?: string;
  optional?: boolean;
  required?: boolean;
  mobileMarginTop?: boolean;
}

const InputLabelWithHelpTooltip = ({
  labelTheme = "darkColor",
  labelText,
  tooltipText,
  tooltipPlacement = "top",
  optional,
  required,
  mobileMarginTop
}: InputLabelWithHelpTooltipProps) => {
  return (
    <div className={Style.labelContainer}>
      <InputLabel theme={labelTheme} optional={optional} required={required} mobileMarginTop={mobileMarginTop}>
        {labelText}
        <span className="ml-2">
          <HelpToolTip text={tooltipText} placement={tooltipPlacement} />
        </span>
      </InputLabel>
    </div>
  );
};

export default InputLabelWithHelpTooltip;
