import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Badge } from "react-bootstrap";
import {
  notificationsBoxToggle,
  getNotificationsCount,
  markAllAsRead,
  clearAll
} from "../../store/actions/notificationsActions";
import Style from "./BellNotifications.module.scss";
import NotificationsBox from "./NotificationsBox/NotificationsBox";
import useIsFirstRender from "../../hooks/useIsFirstRender/useIsFirstRender";

const BellNotifications = ({ children }) => {
  const dispatch = useDispatch();
  const isFirstRender = useIsFirstRender();
  const [boxShowing, setBoxStatus] = useState(false);
  const showNotificationBox = useSelector(state => state.notifications.show);
  const count = useSelector(state => state.notifications.count);

  const displayCount = count > 99 ? "99+" : count;

  useEffect(() => {
    dispatch(getNotificationsCount());
  }, []);

  useEffect(() => {
    if (!isFirstRender) {
      dispatch(clearAll());
      dispatch(notificationsBoxToggle(boxShowing));
      dispatch(getNotificationsCount());
    }
  }, [boxShowing]);

  const showBox = stateShowBox => {
    analytics.track(`NOTIFICATION_WINDOW_${!stateShowBox ? "OPEN" : "CLOSE"}`);
    setBoxStatus(!stateShowBox);
  };

  const readAll = e => {
    e.preventDefault();
    dispatch(markAllAsRead());
  };

  return (
    <div className={Style.notificationWrapper} onClick={() => showBox(boxShowing)}>
      {children}
      {count > 0 && (
        <Badge pill className={Style.badge}>
          {displayCount}
        </Badge>
      )}
      {showNotificationBox && <NotificationsBox onClick={e => readAll(e)} count={count} />}
    </div>
  );
};

export default BellNotifications;
