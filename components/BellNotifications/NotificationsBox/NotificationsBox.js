import React, { useEffect } from "react";
import { Card, ListGroup, ListGroupItem } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import InfiniteScroll from "react-infinite-scroller";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/pro-solid-svg-icons";
import Style from "./NotificationsBox.module.scss";
import ListStyle from "../../List/List.module.scss";
import { notificationsBoxToggle, getNotifications, markAsRead } from "../../../store/actions/notificationsActions";
import NotificationItem from "./NotificationItem/NotificationItem";
import AnimatedLogo from "../../AnimatedLogo";

const NotificationsBox = ({ onClick, count }) => {
  const dispatch = useDispatch();
  const notifications = useSelector(state => state.notifications.notifications);
  const pagination = useSelector(state => state.notifications.pagination);
  const createRef = React.createRef();

  useEffect(() => {
    dispatch(getNotifications());
    return () => {
      document.removeEventListener("mousedown", closeNotifications);
    };
  }, []);

  const closeNotifications = event => {
    if (createRef.current && !createRef.current.contains(event.target)) {
      dispatch(notificationsBoxToggle(false));
    }
  };

  const loadMore = () => {
    const nextPage = pagination ? pagination.page + 1 : 1;
    dispatch(getNotifications(nextPage));
  };

  const notificationItems = notifications.map(notification => (
    <ListGroupItem className={`${ListStyle.notification} ${Style.lastItem}`} key={notification._id}>
      <NotificationItem markAsRead={() => dispatch(markAsRead)} Style={Style} notification={notification} />
    </ListGroupItem>
  ));

  const noNotifcationsMessage = (
    <ListGroupItem className={`${ListStyle.notification} ${Style.lastItem}`}>No notifications yet!</ListGroupItem>
  );

  return (
    <Card className={`customCard pb-0 ${Style.notificationBox}`} ref={createRef}>
      <Card.Header>
        <div className={Style.header}>
          <h5 className={Style.headerTitle}>Notifications</h5>
          <div className={`${Style.cross}`}>
            <FontAwesomeIcon icon={faTimes} />
          </div>
        </div>
        <div className={`${Style.readAll} text-left `} onClick={onClick} aria-hidden="true">
          <p className={`${count > 0 ? Style.bold : ""}`}>MARK AS READ</p>
        </div>
      </Card.Header>
      <ListGroup>
        {notifications.length > 0 ? (
          <div className={Style.infiniteScrollWrapper}>
            <InfiniteScroll
              pageStart={1}
              loadMore={loadMore}
              hasMore={pagination?.hasNextPage}
              loader={<AnimatedLogo theme="inline" />}
              useWindow={false}
            >
              {notificationItems}
            </InfiniteScroll>
          </div>
        ) : (
          noNotifcationsMessage
        )}
      </ListGroup>
    </Card>
  );
};

export default NotificationsBox;
