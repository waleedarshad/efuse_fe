import { Media } from "react-bootstrap";
import TimeAgo from "react-timeago";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faFire,
  faCommentAlt,
  faQuoteRight,
  faGamepadAlt,
  faUserFriends,
  faComments
} from "@fortawesome/pro-light-svg-icons";
import { useRouter } from "next/router";
import { useDispatch } from "react-redux";

import EFAvatar from "../../../EFAvatar/EFAvatar";
import { buildUrl, buildProfileUrl, buildContent, getAvatar } from "../../../../helpers/NotificationsHelper";
import { EFHtmlParser } from "../../../EFHtmlParser/EFHtmlParser";
import { notificationsBoxToggle } from "../../../../store/actions/notificationsActions";

const NotificationItem = ({ notification, Style, markAsRead }) => {
  const router = useRouter();
  const dispatch = useDispatch();

  const url = buildUrl(notification);
  const profilePicture = getAvatar(notification);
  const content = buildContent(notification);
  const profileUrl = buildProfileUrl(notification);

  let iconName = "";
  let iconClass = "";
  let iconBackground = "";

  switch (notification.notifyableType) {
    case "hypes":
    case "likes":
      iconName = faFire;
      iconClass = Style.fire;
      iconBackground = Style.fireIcon;
      break;
    case "comments":
      iconName = faCommentAlt;
      iconClass = Style.comment;
      iconBackground = Style.commentIcon;
      break;
    case "homeFeeds":
      iconName = faQuoteRight;
      iconClass = Style.quote;
      iconBackground = Style.quoteIcon;
      break;
    case "opportunities":
    case "erenatournaments":
      iconName = faGamepadAlt;
      iconClass = Style.gamepad;
      iconBackground = Style.gamepadIcon;
      break;
    case "friends":
      iconName = faUserFriends;
      iconClass = Style.friends;
      iconBackground = Style.friendIcon;
      break;
    case "messages":
      iconName = faComments;
      iconClass = Style.friends;
      iconBackground = Style.friendIcon;
      break;
    default:
      break;
  }

  return (
    <div
      className={Style.notificationItemContainer}
      role="button"
      onClick={() => {
        analytics.track("NOTIFICATION_ITEM_CLICKED", { notification });
        dispatch(notificationsBoxToggle(false));
        markAsRead(notification._id);
        router.push(url);
      }}
      aria-hidden="true"
    >
      <Media>
        <a href={profileUrl}>
          <div className={Style.avatarIcon}>
            <EFAvatar displayOnlineButton={false} profilePicture={profilePicture} size="small" />
            <div className={`${Style.icon} ${iconName}`}>
              <div className={iconBackground}>
                <FontAwesomeIcon icon={iconName} className={iconClass} size="2x" />
              </div>
            </div>
          </div>
        </a>

        <Media.Body>
          <div className={Style.notificationContent}>
            <div>
              <div className={Style.content}>
                <EFHtmlParser>{content}</EFHtmlParser>
              </div>
              <div className={Style.timeAgo}>
                <TimeAgo date={notification.createdAt} minPeriod={30} />
              </div>
            </div>
          </div>
        </Media.Body>
      </Media>
    </div>
  );
};

export default NotificationItem;
