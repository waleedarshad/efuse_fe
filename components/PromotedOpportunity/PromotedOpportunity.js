import React, { Component } from "react";
import { connect } from "react-redux";
import Router from "next/router";
import { getPromotedOpportunities } from "../../store/actions/opportunityActions";
import ImageCard from "../Cards/ImageCard/ImageCard";
import { getFullOpportunityUrl } from "../../helpers/UrlHelper";

class PromotedOpportunity extends Component {
  componentDidMount() {
    this.props.getPromotedOpportunities(true);
  }

  render() {
    const { promotedOpportunities, type, displayNumber, marginTop, marginRight } = this.props;

    if (promotedOpportunities?.length === 0) {
      return <></>;
    }
    if (type === "ImageCard") {
      return (
        <>
          {promotedOpportunities?.length > 0 &&
            promotedOpportunities.map((opp, index) => {
              const associatedOrganization = opp?.associatedOrganization;
              const associatedUser = opp?.associatedUser;
              if (!displayNumber || index < displayNumber) {
                return (
                  <div key={index} style={{ marginTop, marginRight }}>
                    <ImageCard
                      userImage={
                        associatedOrganization
                          ? associatedOrganization?.profileImage?.url
                          : associatedUser?.profilePicture?.url
                      }
                      backgroundImage={opp?.image?.url}
                      type={opp?.opportunityType}
                      onUserClick={() => {
                        analytics.track("PROMOTED_OPPORTUNITY_ORG_CLICKED", { associatedOrganization }, {}, () => {
                          Router.push(`/organizations/show?id=${associatedOrganization._id}`);
                        });
                      }}
                      onBackgroundClick={() => {
                        analytics.track("PROMOTED_OPPORTUNITY_ITEM_CLICKED", { opp }, {}, () => {
                          Router.push(getFullOpportunityUrl(opp));
                        });
                      }}
                    />
                  </div>
                );
              }
            })}
        </>
      );
    }
  }
}

const mapStateToProps = state => ({
  promotedOpportunities: state.opportunities.promotedOpportunities
});

export default connect(mapStateToProps, {
  getPromotedOpportunities
})(PromotedOpportunity);
