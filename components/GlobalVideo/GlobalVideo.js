import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { connect } from "react-redux";
import Draggable from "react-draggable";
import Link from "next/link";
import { faTimes } from "@fortawesome/pro-solid-svg-icons";

import { removeVideo } from "../../store/actions/globalVideoActions";
import Style from "./GlobalVideo.module.scss";
import TwitchPlayerScript from "../TwitchPlayerScript/TwitchPlayerScript";

class GlobalVideo extends Component {
  render() {
    const { globalVideo } = this.props;
    if (!globalVideo?.displayName) {
      return <></>;
    }
    return (
      <Draggable defaultPosition={{ x: 0, y: 0 }} bounds="parent">
        <div className={Style.globalVideoWrapper}>
          <Link href={`/stream/${globalVideo?.displayName}`}>
            <div className={Style.hoverOverlay} />
          </Link>
          <div className={Style.header}>
            <img className={Style.userImage} src={globalVideo?.profileImageUrl} />
            <p className={Style.username}>{globalVideo?.displayName}</p>
            <FontAwesomeIcon className={Style.closeIcon} icon={faTimes} onClick={() => this.props.removeVideo()} />
          </div>
          <div className={`${Style.bottomRightVideo} embed-responsive embed-responsive-16by9`}>
            <TwitchPlayerScript twitchChannel={globalVideo?.displayName} theme="dark" />
          </div>
        </div>
      </Draggable>
    );
  }
}

const mapStateToProps = state => ({
  globalVideo: state.globalVideo.video
});

export default connect(mapStateToProps, { removeVideo })(GlobalVideo);
