import React, { Component } from "react";

const EMBED_URL = "https://embed.twitch.tv/embed/v1.js";

export class TwitchVideoPlayer extends Component {
  state = {
    loaded: false
  };

  componentDidUpdate() {
    if (document?.getElementById("twitch-embed") && !this.state.loaded) {
      let embed;
      const script = document.createElement("script");
      script.setAttribute("src", EMBED_URL);
      script.addEventListener("load", () => {
        embed = new window.Twitch.Embed(this.props.targetID, { ...this.props });
      });
      document.body.appendChild(script);
      this.setState({
        ...this.state,
        loaded: true
      });
    }
  }

  componentWillUnmount() {
    document.removeEventListener("load");
  }

  render() {
    return <div id="twitch-embed" className={this.props.style} />;
  }
}

TwitchVideoPlayer.defaultProps = {
  targetID: "twitch-embed",
  width: "100%",
  height: "100%",
  theme: "dark",
  layout: "video-with-chat"
};

export default TwitchVideoPlayer;
