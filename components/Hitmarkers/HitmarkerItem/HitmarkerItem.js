const HitmarkerItem = ({ hitmarker }) => {
  const { title, url, location } = hitmarker;
  return (
    <div>
      <ul>
        <li>Title: {title}</li>
        <li>
          URL:{" "}
          <a href={url} target="_blank">
            {url}
          </a>
        </li>
        <li>Location: {location}</li>
      </ul>
    </div>
  );
};

export default HitmarkerItem;
