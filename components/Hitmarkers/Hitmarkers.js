import React, { Component } from "react";
import { connect } from "react-redux";

import InfiniteScroll from "react-infinite-scroller";
import { getHitmarkers } from "../../store/actions/hitmarkerActions";
import AnimatedLogo from "../AnimatedLogo";
import HitmarkerItem from "./HitmarkerItem/HitmarkerItem";

class Hitmarkers extends Component {
  componentDidMount() {
    this.props.getHitmarkers();
  }

  loadMore = page => {
    const { pagination } = this.props;
    const nextPage = pagination ? pagination.page + 1 : 1;
    this.props.getHitmarkers(nextPage);
  };

  render() {
    const { pagination } = this.props;
    const hasMore = pagination && pagination.hasNextPage;
    const hitmarkerItems = this.props.hitmarkers.map((hitmarker, index) => (
      <HitmarkerItem hitmarker={hitmarker} key={index} />
    ));
    return (
      <InfiniteScroll
        pageStart={1}
        loadMore={this.loadMore}
        hasMore={hasMore}
        loader={<AnimatedLogo key={0} theme="inline" />}
      >
        {hitmarkerItems}
      </InfiniteScroll>
    );
  }
}

const mapStateToProps = state => ({
  hitmarkers: state.hitmarker.hitmarkers,
  pagination: state.hitmarker.pagination
});

export default connect(mapStateToProps, { getHitmarkers })(Hitmarkers);
