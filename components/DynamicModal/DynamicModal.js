import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import Modal from "../Modal/Modal";
import LoginSignupFlow from "./LoginSignupFlow/LoginSignupFlow";
import OnboardingFlow from "./OnboardingFlow/OnboardingFlow";
import AddCreditCardFlow from "./AddCreditCardFlow/AddCreditCardFlow";
import OpportunityFlow from "./OpportunityFlow/OpportunityFlow";
import { getUserExperiences } from "../../store/actions/experiencesActions";
import { isEmptyObject } from "../../helpers/GeneralHelper";

/**
 @category Components
 @desc A modal that utilizes our global modal component and controls our flows (signup, onboarding)
 @param {Boolean} displayCloseButton Display the close icon to close the modal
 @param {Boolean} allowBackgroundClickClose Clicking outside the modal will close the modal
 @param {String} startView The starting view of the flow you would like to display
 @param {String} flow The flow you would like to display
 @param {String} customMaxWidth A maxWidth value to change the default max width of the modal (ex: 800px)
 @param {String} customRedirect A custom redirect url for when a user uses the LoginSignupFlow
*/

const DynamicModal = ({
  displayCloseButton,
  allowBackgroundClickClose,
  children,
  customMaxWidth,
  customRedirect,
  inviteCode,
  startView,
  onCloseModal,
  openOnLoad,
  opportunity,
  flow,
  organization,
  signupModalTitle
}) => {
  const dispatch = useDispatch();

  const auth = useSelector(state => state.auth);
  const experiences = useSelector(state => state.experiences.experiences);

  const [userLoggedIn, setUserLoggedIn] = useState("");
  const [localFlow, setLocalFlow] = useState("");
  const [initialView, setInitialView] = useState("");
  const [forceCloseModal, setForceCloseModal] = useState(false);
  const [loadedFlow, setLoadedFlow] = useState(false);
  const [localOpenOnLoad, setLocalOpenOnLoad] = useState(false);
  const [isComponentRendered, setIsComponentRendered] = useState(false);

  useEffect(() => {
    switch (flow) {
      case "Onboarding":
        dispatch(getUserExperiences());
        setLocalOpenOnLoad(false);
        break;
      case "LoginSignup":
        if (openOnLoad && isEmptyObject(auth?.currentUser)) {
          setLocalOpenOnLoad(true);
        } else {
          setLocalOpenOnLoad(false);
        }
        break;
      default:
        setLocalOpenOnLoad(openOnLoad);
        break;
    }
  }, [openOnLoad]);

  if (auth?.currentUser?.id !== userLoggedIn) {
    setUserLoggedIn(auth?.currentUser?.id);
  }

  if (flow !== localFlow && !loadedFlow) {
    setLocalFlow(flow);
    setInitialView(startView);
    setLoadedFlow(true);
  }

  const closeModal = () => {
    setForceCloseModal(true);
  };

  const switchFlow = (newFlow, newStartView) => {
    setLocalFlow(newFlow);
    setInitialView(newStartView);
  };

  let component = <></>;

  if (localFlow === "AddCreditCard") {
    component = <AddCreditCardFlow view={initialView} switchFlow={switchFlow} closeModal={closeModal} />;
  }

  if (localFlow === "Opportunity") {
    component = (
      <OpportunityFlow view={initialView} opportunity={opportunity} switchFlow={switchFlow} closeModal={closeModal} />
    );
  }

  if (localFlow === "Onboarding") {
    let found = false;
    let foundExperience;
    // Check if the BE has returned the onboarding experience, if not, don't show the modal
    for (let i = 0; i < experiences?.length; i++) {
      if (experiences[i]?.name === "onboardingFlow") {
        if (experiences[i]?.display) {
          found = true;
          foundExperience = experiences[i];
        } else {
          found = false;
        }
      }
    }

    if (found && !isComponentRendered) {
      setLocalOpenOnLoad(true && openOnLoad);
      setIsComponentRendered(true);
    }

    component = (
      <OnboardingFlow
        view={initialView}
        switchFlow={switchFlow}
        closeDynamicModal={closeModal}
        experience={foundExperience}
      />
    );
  }

  // Only set openOnLoad to true if the current user isn't logged in
  if (localFlow === "LoginSignup") {
    component = (
      <LoginSignupFlow
        view={initialView}
        inviteCode={inviteCode}
        switchFlow={switchFlow}
        closeModal={closeModal}
        customRedirect={customRedirect}
        signupLocationForTracking="site_wide_modal"
        organization={organization}
        signupModalTitle={signupModalTitle}
      />
    );
  }

  return (
    <Modal
      displayCloseButton={displayCloseButton}
      allowBackgroundClickClose={allowBackgroundClickClose}
      openOnLoad={localOpenOnLoad}
      component={component}
      customMaxWidth={customMaxWidth}
      forceClose={forceCloseModal}
      onCloseModal={onCloseModal}
      onOpenModal={() => {
        setForceCloseModal(false);
      }}
    >
      {children}
    </Modal>
  );
};

export default DynamicModal;
