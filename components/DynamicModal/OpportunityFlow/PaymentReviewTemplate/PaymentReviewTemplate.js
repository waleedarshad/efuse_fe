import React, { Component } from "react";
import { connect } from "react-redux";
import { InputGroup, FormControl } from "react-bootstrap";
import Style from "./PaymentReviewTemplate.module.scss";
import { apply } from "../../../../store/actions/applicantActions";
import { getPaymentMethods } from "../../../../store/actions/userActions";
import { validatePromocode, clearPromocodeError } from "../../../../store/actions/orderActions";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";

class PaymentReviewTemplate extends Component {
  state = {
    selectedCard: null,
    selectedCardError: null,
    promocodeText: ""
  };

  componentDidMount() {
    const { getPaymentMethods } = this.props;
    getPaymentMethods();
  }

  onCardSelectionChange(newCardSelection) {
    this.setState({
      ...this.state,
      selectedCard: newCardSelection,
      selectedCardError: null
    });
  }

  onSubmit = event => {
    event.preventDefault();
    const { closeModal, apply, answers, promocode } = this.props;
    const { selectedCard } = this.state;
    if (selectedCard) {
      apply(this.props.opportunity._id, answers, selectedCard, promocode ? promocode._id : null);
      closeModal();
    } else {
      this.setState({
        ...this.state,
        selectedCardError: "Payment method required"
      });
    }
  };

  render() {
    const {
      opportunity,
      paymentMethods,
      switchView,
      answers,
      validatePromocode,
      promocodeError,
      promocode,
      clearPromocodeError
    } = this.props;
    const { selectedCard, selectedCardError, promocodeText } = this.state;

    return (
      <div>
        <h3 className={Style.mainTitle}>Payment Required</h3>
        <div className={Style.costContainer}>
          <h4 className={Style.applicationCostText}>${opportunity.applicationCost.toFixed(2)}</h4>
          <p className={Style.applicationCostLabel}>Application Cost</p>
        </div>
        <div className={Style.paymentMethodOutsideContainer}>
          <div className={Style.paymentMethodHeader}>
            <span className={Style.formLabel}>Payment method</span>
            <EFRectangleButton
              text="Add Card"
              colorTheme="light"
              onClick={() => {
                switchView("AddCreditCard");
              }}
            />
          </div>
          <div className={Style.paymentMethodContainer}>
            {!paymentMethods && <div className={Style.paymentMethodMessage}>Loading....</div>}
            {paymentMethods?.length == 0 && <div className={Style.paymentMethodMessage}>No payment methods</div>}
            {paymentMethods?.length > 0 &&
              paymentMethods.map(method => {
                return (
                  <label>
                    <input
                      name="paymentMethods"
                      type="radio"
                      selected={selectedCard == method.id}
                      onChange={() => this.onCardSelectionChange(method.id)}
                    />
                    {method.card.brand[0].toUpperCase() + method.card.brand.slice(1)} ending in {method.card.last4}
                  </label>
                );
              })}
          </div>
          {selectedCardError && <div className={Style.error}>{selectedCardError}</div>}
        </div>
        <label className={Style.formLabel} />
        <div className={Style.promocodeContainer}>
          <label className={Style.formLabel}>Promo code</label>
          {promocode ? (
            <div className={Style.activePromocode}>
              <div style={{ fontWeight: "bold" }}>{`${promocode.code} (${
                promocode.discountType == "VALUE"
                  ? `$${(Math.floor(promocode.amount * 100) / 100).toFixed(2)}`
                  : `${promocode.amount * 100}%`
              } off)`}</div>
              <div>{promocode.userMessage}</div>
            </div>
          ) : (
            <>
              <InputGroup className="mb-3">
                <FormControl
                  placeholder=""
                  aria-label="Promo code"
                  aria-describedby="basic-addon1"
                  value={promocodeText}
                  style={{ textTransform: "uppercase" }}
                  onChange={e => {
                    this.setState({
                      ...this.state,
                      promocodeText: e.target.value
                    });
                    if (promocodeError) {
                      clearPromocodeError();
                    }
                  }}
                />
                <InputGroup.Append>
                  <EFRectangleButton
                    text="Submit"
                    colorTheme="light"
                    onClick={() => {
                      validatePromocode(promocodeText, "PAID_OPPORTUNITY");
                    }}
                  />
                </InputGroup.Append>
              </InputGroup>
              <div className={Style.invalidFormInput}>{promocodeError || ""}</div>
            </>
          )}
        </div>
        {opportunity?.candidateQuestions?.length > 0 && (
          <div className="mr-2">
            <EFRectangleButton text="Back" onClick={() => switchView("Questions")} />
          </div>
        )}

        <EFRectangleButton text="Submit Payment" onClick={e => this.onSubmit(e)} disabled={!selectedCard} />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  paymentMethods: state.user.paymentMethods,
  promocode: state.orders.promocode,
  promocodeError: state.orders.promocodeError
});

export default connect(mapStateToProps, {
  getPaymentMethods,
  apply,
  validatePromocode,
  clearPromocodeError
})(PaymentReviewTemplate);
