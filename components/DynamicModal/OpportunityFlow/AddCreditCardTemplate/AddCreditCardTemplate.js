import React, { Component } from "react";
import { connect } from "react-redux";
import { getSetupIntent } from "../../../../store/actions/userActions";
import CreditCardForm from "../../../Payments/CreditCardForm/CreditCardForm";

class AddCreditCardTemplate extends Component {
  componentDidMount() {
    this.props.getSetupIntent();
  }

  render() {
    const { switchSignup, setupIntent, closeModal, switchView } = this.props;
    if (setupIntent) {
      return (
        <div>
          <CreditCardForm
            setupIntent={setupIntent}
            closeModal={closeModal}
            onCancel={() => switchView("PaymentReview")}
            onSubmit={() => switchView("PaymentReview")}
          />
        </div>
      );
    }
    return <div>Loading...</div>;
  }
}

const mapStateToProps = state => ({
  setupIntent: state.user.setupIntent
});

export default connect(mapStateToProps, {
  getSetupIntent
})(AddCreditCardTemplate);
