import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "next/router";
import QuestionsTemplate from "./QuestionsTemplate/QuestionsTemplate";
import PaymentReviewTemplate from "./PaymentReviewTemplate/PaymentReviewTemplate";
import AddCreditCardTemplate from "./AddCreditCardTemplate/AddCreditCardTemplate";

class OpportunityFlow extends Component {
  state = {
    view: "",
    currentUser: "",
    startViewSet: false,
    answers: []
  };

  constructor(props) {
    super(props);
    this.switchView = this.switchView.bind(this);
    this.clearState = this.clearState.bind(this);
  }

  static getDerivedStateFromProps(props, state) {
    if (props?.view != state?.view && !state.startViewSet) {
      state.view = props.view;
      state.startViewSet = true;
    }
    return state;
  }

  updateQuestionsState(i, updatedText) {
    this.setState({
      answers: {
        ...this.state.answers,
        [i]: {
          index: i,
          question: updatedText
        }
      }
    });
  }

  switchView = value => {
    this.setState({
      view: value
    });
  };

  clearState() {
    this.setState({
      view: "login"
    });
  }

  render() {
    const { closeModal, opportunity, switchFlow } = this.props;
    const { view, answers } = this.state;

    return (
      <>
        {view == "Questions" && (
          <QuestionsTemplate
            switchView={this.switchView}
            closeModal={closeModal}
            opportunity={opportunity}
            switchFlow={switchFlow}
            updateQuestionsState={(i, updatedText) => this.updateQuestionsState(i, updatedText)}
            answers={answers}
          />
        )}
        {view == "PaymentReview" && (
          <PaymentReviewTemplate
            switchView={this.switchView}
            closeModal={closeModal}
            opportunity={opportunity}
            switchFlow={switchFlow}
            answers={answers}
          />
        )}
        {view == "AddCreditCard" && (
          <AddCreditCardTemplate
            switchView={this.switchView}
            closeModal={closeModal}
            opportunity={opportunity}
            switchFlow={switchFlow}
          />
        )}
      </>
    );
  }
}

const mapStateToProps = state => ({
  thirdPartySignup: state.features.third_party_signup
});

export default connect(mapStateToProps, {})(withRouter(OpportunityFlow));
