import React, { Component } from "react";

import { connect } from "react-redux";
import AddApplicantQuestion from "../../../Opportunities/OpportunityWrapper/AddApplicantQuestion/AddApplicantQuestion";
import { apply } from "../../../../store/actions/applicantActions";

class QuestionsTemplate extends Component {
  render() {
    const { switchView, closeModal, opportunity, answers, updateQuestionsState } = this.props;
    return (
      <div>
        <AddApplicantQuestion
          opportunity={opportunity}
          answers={answers}
          questions={opportunity?.candidateQuestions}
          onCancel={() => closeModal()}
          updateQuestionsState={updateQuestionsState}
          onSubmit={answers => {
            if (opportunity?.applicationCost > 0) {
              switchView("PaymentReview");
            } else {
              this.props.apply(opportunity?.opportunityType, opportunity._id, answers);
            }
          }}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({});

export default connect(mapStateToProps, { apply })(QuestionsTemplate);
