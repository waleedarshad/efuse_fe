import React, { Component } from "react";

import { connect } from "react-redux";
import { withRouter } from "next/router";
import AddCreditCardTemplate from "./AddCreditCardTemplate/AddCreditCardTemplate";

class AddCreditCardFlow extends Component {
  state = {
    view: "",
    currentUser: "",
    startViewSet: false
  };

  constructor(props) {
    super(props);
    this.switchSignup = this.switchSignup.bind(this);
    this.clearState = this.clearState.bind(this);
  }

  static getDerivedStateFromProps(props, state) {
    if (props?.view != state?.view && !state.startViewSet) {
      state.view = props.view;
      state.startViewSet = true;
    }
    return state;
  }

  switchSignup = value => {
    this.setState({
      view: value
    });
  };

  clearState() {
    this.setState({
      view: "login"
    });
  }

  render() {
    const { closeModal } = this.props;
    const { view } = this.state;

    return (
      <>
        {view == "AddCreditCard" && <AddCreditCardTemplate switchSignup={this.switchSignup} closeModal={closeModal} />}
      </>
    );
  }
}

const mapStateToProps = state => ({
  thirdPartySignup: state.features.third_party_signup
});

export default connect(mapStateToProps, {})(withRouter(AddCreditCardFlow));
