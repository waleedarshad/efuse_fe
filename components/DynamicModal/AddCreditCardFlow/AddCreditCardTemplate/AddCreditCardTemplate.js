import React, { Component } from "react";
import { connect } from "react-redux";
import { getSetupIntent } from "../../../../store/actions/userActions";
import CreditCardForm from "../../../Payments/CreditCardForm/CreditCardForm";

class AddCreditCardTemplate extends Component {
  componentDidMount() {
    this.props.getSetupIntent();
  }

  render() {
    const { switchSignup, setupIntent, closeModal } = this.props;
    console.log("setupIntent", setupIntent);

    if (setupIntent) {
      return (
        <div>
          <CreditCardForm setupIntent={setupIntent} closeModal={closeModal} onSubmit={closeModal} />
        </div>
      );
    }
    return <div>Loading...</div>;
  }
}

const mapStateToProps = state => ({
  setupIntent: state.user.setupIntent
});

export default connect(mapStateToProps, {
  getSetupIntent
})(AddCreditCardTemplate);
