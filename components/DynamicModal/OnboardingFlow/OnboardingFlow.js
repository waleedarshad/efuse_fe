import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import ProgressBar from "react-bootstrap/ProgressBar";

import ProfileCustomizationTemplate from "./ProfileCustomizationTemplate/ProfileCustomizationTemplate";
import ExternalAccountsTemplate from "./ExternalAccountsTemplate/ExternalAccountsTemplate";
import Style from "./OnboardingFlow.module.scss";
import { getCurrentUser } from "../../../store/actions/common/userAuthActions";
import { incrementGlobalView, incrementSubSkip, incrementSubView } from "../../../store/actions/experiencesActions";
import SelectGamesTemplate from "./SelectGamesTemplate/SelectGamesTemplate";

/**
 @category Components
 @desc The onboarding flow for a user after they sign in to eFuse
 @param {User} user The current logged in user
 @param {String} view The initial view you would like to display on this onboarding flow
*/

const OnboardingFlow = ({ view, experience, closeDynamicModal }) => {
  const dispatch = useDispatch();
  const [localView, setLocalView] = useState(view);

  const user = useSelector(state => state.user.currentUser);

  useEffect(() => {
    dispatch(getCurrentUser());
    // Tracking this event so we can build funnel for signup process
    analytics.track("USER_ONBOARDING_GAMES");
  }, []);

  const closeModalAnalytics = (trackAnalytics, isInactive, wasSkipped) => {
    if (isInactive) return;
    if (trackAnalytics) trackAnalytics;

    if (wasSkipped) {
      dispatch(incrementSubSkip(experience?.experienceId, localView));
    } else {
      dispatch(incrementSubView(experience?.experienceId, localView));
    }

    dispatch(incrementGlobalView(experience?.experienceId));
    closeDynamicModal();
  };

  const switchSignup = (value, trackAnalytics, wasSkipped) => {
    if (trackAnalytics) trackAnalytics;

    if (wasSkipped) {
      dispatch(incrementSubSkip(experience?.experienceId, localView));
    } else {
      dispatch(incrementSubView(experience?.experienceId, localView));
    }

    // Tracking this event so we can build funnel for signup process
    analytics && analytics.track(`USER_ONBOARDING_${value.toUpperCase()}`);

    setLocalView(value);
  };

  let progressCount = 0;

  if (localView === "selectGames") {
    progressCount = 25;
  }

  if (localView === "profileCustomization") {
    progressCount = 50;
  }

  if (localView === "externalAccounts") {
    progressCount = 75;
  }

  return (
    <>
      {localView === "selectGames" && <SelectGamesTemplate switchSignup={switchSignup} />}
      {localView === "profileCustomization" && <ProfileCustomizationTemplate switchSignup={switchSignup} user={user} />}
      {localView === "externalAccounts" && (
        <ExternalAccountsTemplate switchSignup={switchSignup} user={user} closeModalAnalytics={closeModalAnalytics} />
      )}

      <ProgressBar className={Style.progressBar} now={progressCount} variant="blue" />
    </>
  );
};

export default OnboardingFlow;
