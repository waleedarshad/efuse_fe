import React from "react";

import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";
import Style from "../OnboardingFlow.module.scss";

const HeaderSection = ({ title, allowSkip, onSkipClick }) => {
  return (
    <div className={Style.header}>
      <h2 className={Style.title}>{title}</h2>
      {allowSkip && (
        <div className={Style.skip}>
          <EFRectangleButton colorTheme="transparent" shadowTheme="none" text="Skip" onClick={onSkipClick} />
        </div>
      )}
    </div>
  );
};

HeaderSection.defaultProps = {
  allowSkip: true
};

export default HeaderSection;
