import React, { useState } from "react";
import { useMutation } from "@apollo/client";
import HeaderSection from "../HeaderSection/HeaderSection";
import EFGameSelect from "../../../EFGameSelect/EFGameSelect";
import NextButton from "../NextButton/NextButton";
import { ADD_GAMES_TO_USER } from "../../../../graphql/UserGamesQuery";

const SelectGamesTemplate = ({ switchSignup }) => {
  const [selectedGames, setSelectedGames] = useState([]);
  const [submitGamesGQL] = useMutation(ADD_GAMES_TO_USER);

  const userSubmission = () => {
    const selectedGameIds = selectedGames.map(game => game._id);
    const selectedGameTitles = selectedGames.map(game => game.title);
    analytics.track("USER_GAMES_SELECTED", { location: "onboarding", selectedGameIds, selectedGameTitles });
    submitGamesGQL({ variables: { addGamesToUserGameIds: selectedGameIds } });
    switchSignup("profileCustomization", null, false);
  };

  return (
    <>
      <HeaderSection
        title="Follow the games you love"
        onSkipClick={() =>
          switchSignup("profileCustomization", analytics.track("ONBOARDING_SELECT_GAMES_SKIP_CLICKED"), true)
        }
      />
      <hr />
      <EFGameSelect onSelect={games => setSelectedGames(games)} />
      <NextButton onClick={userSubmission} disabled={selectedGames.length === 0} />
    </>
  );
};

export default SelectGamesTemplate;
