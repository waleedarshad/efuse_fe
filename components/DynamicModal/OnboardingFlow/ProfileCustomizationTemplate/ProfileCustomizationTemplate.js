import React, { Component } from "react";
import { connect } from "react-redux";
import isEmpty from "lodash/isEmpty";
import Style from "./ProfileCustomizationTemplate.module.scss";
import { updateCurrentUser, removePic } from "../../../../store/actions/userActions";
import { toggleCropModal } from "../../../../store/actions/settingsActions";
import InputField from "../../../InputField/InputField";
import ImageUploader from "../../../ImageUploader/ImageUploader";
import { withCdn } from "../../../../common/utils";
import HeaderSection from "../HeaderSection/HeaderSection";
import NextButton from "../NextButton/NextButton";
import InputLabel from "../../../InputLabel/InputLabel";

class ProfileCustomizationTemplate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      validated: false,
      hasUpdated: false,
      headerImageCropped: null,
      user: {
        profilePicture: {},
        headerImage: {},
        bio: "",
        name: ""
      }
    };
  }

  static getDerivedStateFromProps(props, state) {
    if (
      props?.user?._id == props.currentUser?._id &&
      !state.hasUpdated &&
      (props?.user?.bio !== state?.user?.bio ||
        props?.user?.headerImage?.url !== state?.user?.headerImage?.url ||
        props?.user?.profilePicture?.url !== state?.user?.profilePicture?.url)
    ) {
      state.user.profilePicture = props.user.profilePicture;
      state.user.headerImage = props.user.headerImage;
      state.user.bio = props.user.bio;
      state.user.name = props.user.username;
      state.hasUpdated = true;
    }
    return state;
  }

  componentDidMount() {
    analytics.page("Onboarding Profile Customization");
  }

  onChange = event => {
    this.setState({
      user: {
        ...this.state.user,
        [event.target.name]: event.target.value
      }
    });
  };

  onDrop = (selectedFile, name) => {
    this.setState(
      {
        user: {
          ...this.state.user,
          [name]: selectedFile[0]
        }
      },
      () => {}
    );
  };

  onCropChange = (croppedImage, name) => {
    const { blob } = croppedImage;
    const croppedFile = new File([blob], blob.name, { type: blob.type });
    this.setState({
      user: { ...this.state.user, [name]: croppedFile }
    });
  };

  userSubmission = isInactive => {
    if (isInactive) return;
    const userData = new FormData();
    const { user } = this.state;
    Object.keys(user).forEach(key => {
      if (user[key] !== undefined) {
        userData.append(key, user[key]);
        if (user[key]?.size > 0) {
          key === "profilePicture" && analytics.track("ADD_USER_PROFILE_PICTURE");
          key === "headerImage" && analytics.track("ADD_USER_HEADER_IMAGE");
        }
        if (!isEmpty(user[key])) {
          key === "bio" && analytics.track("ADD_USER_BIO");
        }
      }
    });
    this.props.updateCurrentUser(userData);
    this.props.switchSignup("externalAccounts", null, false);
  };

  toggleCropperModal = (val, name) => {
    this.props.toggleCropModal(val, name);
  };

  render() {
    const { switchSignup, user } = this.props;
    const userState = this.state.user;
    const hasDefaultHeaderImage = user?.headerImage?.url.includes("/static/images/no_image.jpg");
    const hasDefaultProfilePicture =
      user?.profilePicture?.url === "https://cdn.efuse.gg/uploads/static/global/mindblown_white.png";
    const hasDefaultEmptyBio = user?.bio === "" || user?.bio === undefined;

    const hasUploadedHeaderImage = userState?.headerImage?.name !== undefined;
    const hasUploadedProfilePicture = userState?.profilePicture?.name !== undefined;
    const hasBio = userState?.bio !== "" && userState?.bio !== undefined;
    const hasName = userState?.name !== "" && userState?.bio !== undefined;

    // check if the user has added any information to the state OR the user has previous information

    const nextButtonInactive =
      !hasName ||
      (!hasBio &&
        !hasUploadedProfilePicture &&
        !hasUploadedHeaderImage &&
        hasDefaultHeaderImage &&
        hasDefaultProfilePicture &&
        hasDefaultEmptyBio);
    return (
      <>
        <HeaderSection
          title="Customize Portfolio"
          onSkipClick={() =>
            switchSignup("externalAccounts", analytics.track("ONBOARDING_PROFILE_CUSTOMIZATION_SKIP_CLICKED"), true)
          }
        />
        <hr />
        <div className={Style.profileCustomizationWrapper}>
          <ImageUploader
            text="Header Image"
            name="headerImage"
            onDrop={this.onDrop}
            allowCross={false}
            theme="internal"
            value={userState?.headerImage?.url ? userState?.headerImage?.url : withCdn("/static/images/no_image.jpg")}
            onCropChange={this.onCropChange}
            toggleCropperModal={this.toggleCropperModal}
            aspectRatioWidth={180}
            aspectRatioHeight={50}
            showCropper
            fullWidthPreview
            dialogButtonRightAlign
          />
          <div className={Style.profileWrapper}>
            <ImageUploader
              text="Profile Picture"
              name="profilePicture"
              onDrop={this.onDrop}
              allowCross={false}
              theme="internal"
              value={
                userState?.profilePicture?.url
                  ? userState?.profilePicture?.url
                  : "https://cdn.efuse.gg/uploads/static/global/mindblown_white.png"
              }
              onCropChange={this.onCropChange}
              toggleCropperModal={this.toggleCropperModal}
              aspectRatioWidth={160}
              aspectRatioHeight={160}
              showCropper
              circlePreview
              dialogButtonOverlay
            />
          </div>
          <div className={Style.bioWrapper}>
            <div className="mb-3">
              <InputLabel theme="internal">Display Name</InputLabel>
              <InputField
                name="name"
                placeholder="Enter a display name"
                value={userState.name}
                onChange={this.onChange}
                theme="internal"
              />
            </div>

            <div>
              <InputLabel theme="internal">Bio</InputLabel>
              <InputField
                as="textarea"
                name="bio"
                placeholder="Describe a little bit about yourself..."
                value={userState?.bio}
                onChange={this.onChange}
                rows={4}
                maxLength={250}
                theme="internal"
              />
            </div>
          </div>
          <NextButton disabled={nextButtonInactive} onClick={() => this.userSubmission(nextButtonInactive)} />
        </div>
      </>
    );
  }
}

const mapStateToProps = state => ({
  currentUser: state.auth.currentUser
});

export default connect(mapStateToProps, {
  updateCurrentUser,
  toggleCropModal,
  removePic
})(ProfileCustomizationTemplate);
