import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import isEmpty from "lodash/isEmpty";

import { setUserpathway } from "../../../../store/actions/onboardingActions";
import HeaderSection from "../HeaderSection/HeaderSection";
import NextButton from "../NextButton/NextButton";
import FlowStyle from "../OnboardingFlow.module.scss";
import EFPathwaysSelection from "../../../EFPathwaysSelection/EFPathwaysSelection";

const Pathways = ({ switchSignup }) => {
  const dispatch = useDispatch();
  const availablePathways = useSelector(state => state.onboarding.pathways);
  const user = useSelector(state => state.auth.currentUser);

  const [selectedPathwayID, setSelectedPathwayID] = useState("");
  useEffect(() => {
    if (user?.pathway) {
      switchSignup("profileCustomization", null, false);
    } else analytics.track("PATHWAY_SELECTED", { location: "Onboarding Flow" });
  }, []);

  const onCardSelect = selectedCardId => {
    const selectedCard = availablePathways.find(card => card._id === selectedCardId);
    setSelectedPathwayID(selectedCardId);
  };

  const userSubmission = () => {
    analytics.track("ONBOARDING_PATHWAYS_SUBMIT_CLICKED");
    dispatch(setUserpathway({ pathwayId: selectedPathwayID }));
    switchSignup("profileCustomization", null, false);
  };

  return (
    <>
      <HeaderSection
        title="Select a Pathway"
        allowSkip={false}
        onSkipClick={() =>
          switchSignup("profileCustomization", analytics.track("ONBOARDING_PATHWAYS_SKIP_CLICKED"), true)
        }
      />
      <p className={FlowStyle.infoText}>By selecting a pathway we can guide you to achieving your goals</p>
      <hr />
      <EFPathwaysSelection onCardSelect={onCardSelect} selectedPathwayID={selectedPathwayID} />
      <NextButton onClick={userSubmission} disabled={isEmpty(selectedPathwayID)} />
    </>
  );
};

export default Pathways;
