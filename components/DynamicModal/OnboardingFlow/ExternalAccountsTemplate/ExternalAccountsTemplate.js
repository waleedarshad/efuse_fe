import React, { Component } from "react";

import ExternalAccounts from "../../../Settings/ExternalAccounts/index";
import HeaderSection from "../HeaderSection/HeaderSection";
import NextButton from "../NextButton/NextButton";
import FlowStyle from "../OnboardingFlow.module.scss";
import Style from "./ExternalAccountsWrapper.module.scss";

class ExternalAccountsTemplate extends Component {
  switchSignup(view, isInactive, skipped) {
    if (isInactive) return;
    this.props.switchSignup(view, isInactive, skipped);
  }

  render() {
    const { switchSignup, user, closeModalAnalytics } = this.props;
    const nextButtonInactive =
      !user?.twitterVerified &&
      !user?.discordVerified &&
      !user?.facebookVerified &&
      !user?.twitchVerified &&
      !user?.googleVerified;
    return (
      <>
        <HeaderSection title="Link External Accounts" onSkipClick={() => closeModalAnalytics()} />
        <p className={FlowStyle.infoText}>
          Linking your external accounts can unlock additional features for you on eFuse. Build your brands. Grow your
          following. Access new features.
        </p>
        <hr />
        <div className={Style.externalAccountsWrapper}>
          <ExternalAccounts
            onboardingFlow
            onlyShowLinks
            displaySteam={false}
            displayBattlenet={false}
            displayLinkedin={false}
            displayTwitter
            displayDiscord
            displayFacebook
            displayTwitch
            displayGoogle
            displayAimLab={false}
            displayValorant={false}
          />
        </div>
        <NextButton disabled={nextButtonInactive} onClick={() => closeModalAnalytics()} />
      </>
    );
  }
}

export default ExternalAccountsTemplate;
