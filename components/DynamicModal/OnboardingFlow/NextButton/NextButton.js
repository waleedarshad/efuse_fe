import React from "react";

import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";
import Style from "./NextButton.module.scss";

const NextButton = ({ disabled, onClick }) => {
  return (
    <div className={Style.nextButton}>
      <EFRectangleButton disabled={disabled} onClick={onClick} text="Next" width="fullWidth" />
    </div>
  );
};

export default NextButton;
