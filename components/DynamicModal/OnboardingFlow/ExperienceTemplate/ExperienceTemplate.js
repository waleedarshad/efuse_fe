import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck, faPlus } from "@fortawesome/pro-solid-svg-icons";

import Style from "./ExperienceTemplate.module.scss";
import AddEducationExperience from "../../../User/Portfolio/EducationExperience/AddExperience/AddExperience";
import AddWorkExperience from "../../../User/Portfolio/BusinessExperience/AddExperience/AddExperience";
import NextButton from "../NextButton/NextButton";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";
import FlowStyle from "../OnboardingFlow.module.scss";

const EducationExperienceTemplate = ({ user, switchSignup }) => {
  const [display, setDisplay] = useState("");

  useEffect(() => {
    analytics.page("Onboarding Experience");
  }, []);

  const changeDisplay = (displayValue, haveNoExperience) => {
    if (haveNoExperience) {
      if (displayValue === "education") {
        analytics.track("ONBOARDING_EDUCATION_EXPERIENCE_CLICKED");
      }
      if (displayValue === "work") {
        analytics.track("ONBOARDING_WORK_EXPERIENCE_CLICKED");
      }
      setDisplay(displayValue);
    }
  };

  const switchView = (view, isInactive) => {
    if (isInactive) return;
    switchSignup(view);
  };

  const educationExperience = user?.educationExperience?.length;
  const businessExperience = user?.businessExperience?.length;
  const nextButtonInactive = !(educationExperience > 0) && !(businessExperience > 0);

  return (
    <>
      <div className={FlowStyle.header}>
        {display !== "" && (
          <>
            <h2 className={FlowStyle.title}>Add {display} Experience</h2>
            <div className={Style.back}>
              <EFRectangleButton
                colorTheme="transparent"
                shadowTheme="none"
                text="Back"
                onClick={() => setDisplay("")}
              />
            </div>
          </>
        )}
        {display === "" && (
          <>
            <h2 className={FlowStyle.title}>Add Experience</h2>
            <div className={FlowStyle.skip}>
              <EFRectangleButton
                colorTheme="transparent"
                shadowTheme="none"
                text="Skip"
                onClick={() =>
                  switchSignup("externalAccounts", analytics.track("ONBOARDING_EXPERIENCE_SKIP_CLICKED"), true)
                }
              />
            </div>
          </>
        )}
      </div>
      <p className={FlowStyle.infoText}>
        Adding your experience increases your chances of earning opportunities. Building a good portfolio allows you to
        establish credibility in the industry by showcasing your talent, abilities and experience.
      </p>
      <hr />
      <div className={`${Style.buttons} ${display !== "" && Style.hideButtons}`}>
        <div
          className={`${Style.button} ${educationExperience > 0 && Style.buttonComplete}`}
          onClick={() => changeDisplay("education", educationExperience === 0)}
        >
          <FontAwesomeIcon icon={educationExperience > 0 ? faCheck : faPlus} className={Style.plusIcon} />
          <span className={Style.buttonText}>Add Education Experience</span>
        </div>
        <div
          className={`${Style.button} ${businessExperience > 0 && Style.buttonComplete}`}
          onClick={() => changeDisplay("work", businessExperience === 0)}
        >
          <FontAwesomeIcon icon={businessExperience > 0 ? faCheck : faPlus} className={Style.plusIcon} />
          <span className={Style.buttonText}>Add Work Experience</span>
        </div>
      </div>
      <div className={`${Style.educationWrapper} ${display === "education" && Style.showEducationView}`}>
        <AddEducationExperience
          resetOnboardingState={() => setDisplay("")}
          removeCancelOption
          onboardingAnalyticsOptions
        />
      </div>
      <div className={`${Style.workWrapper} ${display === "work" && Style.showWorkView}`}>
        <AddWorkExperience resetOnboardingState={() => setDisplay("")} removeCancelOption onboardingAnalyticsOptions />
      </div>
      {display === "" && (
        <NextButton
          disabled={nextButtonInactive}
          onClick={() => switchView("externalAccounts", nextButtonInactive, false)}
        />
      )}
    </>
  );
};

export default EducationExperienceTemplate;
