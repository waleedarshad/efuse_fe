import React, { Component } from "react";

import FlowStyle from "../OnboardingFlow.module.scss";
import Style from "./WhoTofollowTemplate.module.scss";
import WhoToFollow from "./WhoToFollow/WhoToFollow";
import HeaderSection from "../HeaderSection/HeaderSection";
import NextButton from "../NextButton/NextButton";

class WhoToFollowTemplate extends Component {
  render() {
    const { switchSignup, closeModalAnalytics, user } = this.props;
    const requiredFollowers = 5;
    const remainingFollowers = Math.max(requiredFollowers - this.props.followeesCount, 0);
    const nextButtonInactive = remainingFollowers !== 0;
    return (
      <>
        <HeaderSection
          title="Who to Follow"
          onSkipClick={() => closeModalAnalytics(analytics.track("WHO_TO_FOLLOW_SKIP_CLICKED"), false, true)}
        />
        {remainingFollowers !== 0 && (
          <p className={FlowStyle.infoText}>Follow {remainingFollowers} more users to get updates!</p>
        )}
        <hr />
        <div className={Style.externalAccountsWrapper}>
          <WhoToFollow />
        </div>
        <NextButton
          disabled={nextButtonInactive}
          onClick={() => closeModalAnalytics(null, nextButtonInactive, false)}
        />
      </>
    );
  }
}

export default WhoToFollowTemplate;
