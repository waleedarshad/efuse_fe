import React from "react";
import { connect } from "react-redux";
import Style from "./WhoToFollow.module.scss";
import { getFollowerRecommendations } from "../../../../../store/actions/recommendationsActions";
import EFFollow from "../../../../EFFollow/EFFollow";
import { getFolloweeIds, unfollowThenRefresh, followThenRefresh } from "../../../../../store/actions/followerActions";
import VerifiedIcon from "../../../../VerifiedIcon/VerifiedIcon";

class WhoToFollow extends React.Component {
  state = {
    done: false
  };

  componentDidMount() {
    this.props.getFollowerRecommendations();
  }

  componentDidUpdate(props) {
    if (props.currentUser && !this.state.done) {
      props.getFolloweeIds(props.currentUser.id);
      this.setState({ done: true });
    }
  }

  render() {
    return (
      <div className={`${Style.whoToFollow}`}>
        {this.props.recommendations.map((user, index) => {
          const following = this.props.followeeIds.filter(followeeId => followeeId == user._id).length > 0;
          return (
            <div key={index}>
              <div className={Style.profileCard}>
                <div
                  className={Style.headerImage}
                  style={{
                    background: `url(${user?.headerImage?.url}) no-repeat center center`,
                    backgroundSize: "cover"
                  }}
                />
                <div className={Style.profilePictureContainer}>
                  <img width="80" src={user?.profilePicture?.url} />
                </div>
                <div className={Style.name}>
                  {user.name}
                  {user.verified && <VerifiedIcon />}
                </div>
                <div className={Style.username}>@{user.username}</div>
                <div className={Style.bio}>{user.bio ? user.bio : "I am a new user to eFuse.gg"}</div>

                <EFFollow
                  renderAs="followees"
                  id={user._id}
                  isFollowed={following}
                  followOveride={() => this.props.followThenRefresh(user._id, this.props.currentUser.id)}
                  unfollowOveride={() => this.props.unfollowThenRefresh(user._id, this.props.currentUser.id)}
                />
              </div>
            </div>
          );
        })}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  recommendations: state.recommendations.followers,
  currentUser: state.auth.currentUser,
  followeeIds: state.followers.followeeIds
});

export default connect(mapStateToProps, {
  getFollowerRecommendations,
  getFolloweeIds,
  unfollowThenRefresh,
  followThenRefresh
})(WhoToFollow);
