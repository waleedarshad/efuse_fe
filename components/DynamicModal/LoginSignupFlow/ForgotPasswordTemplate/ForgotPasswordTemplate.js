import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import Form from "react-bootstrap/Form";
import { useRouter } from "next/router";

import Style from "./ForgotPasswordTemplate.module.scss";
import { forgotPassword } from "../../../../store/actions/userActions";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";

const ForgotPasswordTemplate = ({ clearState }) => {
  const dispatch = useDispatch();
  const router = useRouter();

  const [email, setEmail] = useState("");
  const [validated, setValidated] = useState(false);

  const onSubmit = event => {
    event.preventDefault();
    setValidated(true);
    if (event.target.checkValidity() && email !== "") {
      dispatch(forgotPassword({ email }, router));
      clearState();
    }
  };

  useEffect(() => {
    analytics.page("Forgot Password");
  }, []);

  return (
    <Form className={Style.formContainer} noValidate validated={validated} onSubmit={onSubmit}>
      <Form.Label className={Style.formLabel}>Email Address</Form.Label>
      <Form.Control
        className={Style.formField}
        size="lg"
        type="email"
        placeholder="Email Address"
        required
        name="email"
        value={email}
        onChange={e => {
          setEmail(e.target.value);
          setValidated(false);
        }}
      />
      <Form.Control.Feedback type="invalid">Email is required.</Form.Control.Feedback>
      <EFRectangleButton
        text="Forgot Password"
        width="fullWidth"
        className={Style.formButton}
        fontCaseTheme="upperCase"
        fontWeightTheme="extraBold"
        buttonType="submit"
      />
    </Form>
  );
};

export default ForgotPasswordTemplate;
