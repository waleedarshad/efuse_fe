import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { useSelector } from "react-redux";
import ForgotPasswordTemplate from "./ForgotPasswordTemplate/ForgotPasswordTemplate";
import ResetPasswordTemplate from "./ResetPasswordTemplate/ResetPasswordTemplate";
import Style from "./LoginSignupFlow.module.scss";
import FeatureFlag from "../../General/FeatureFlag/FeatureFlag";
import FeatureFlagVariant from "../../General/FeatureFlag/FeatureFlagVariant/FeatureFlagVariant";
import EFDynamicLink from "../../EFDynamicLink/EFDynamicLink";
import SignupTemplate from "./SignupTemplate/SignupTemplate";
import LoginForm from "./LoginForm/LoginForm";
import EFAuthenticateMessage from "../../EFAuthenticateMessage/EFAuthenticateMessage";

const LoginSignupFlow = ({
  view,
  customRedirect,
  signupModalTitle,
  signupLocationForTracking,
  inviteCode,
  organization
}) => {
  const router = useRouter();
  const thirdPartySignup = useSelector(state => state.features.third_party_signup);

  const [localView, setLocalView] = useState("");
  const [startViewSet, setStartViewSet] = useState(false);

  useEffect(() => {
    if (view !== localView && !startViewSet) {
      setLocalView(view);
      setStartViewSet(true);
    }
  }, [localView]);

  const switchSignup = value => {
    setLocalView(value);
  };

  return (
    <FeatureFlag name="signup_login_enable">
      <FeatureFlagVariant flagState>
        {(localView === "signup" || localView === "invite") && (
          <SignupTemplate
            inviteCode={inviteCode}
            customRedirect={customRedirect}
            signupModalTitle={signupModalTitle}
            localView={localView}
            signupLocationForTracking={signupLocationForTracking}
            onClick={() => setLocalView("login")}
            redirectUrl={customRedirect || router.asPath}
            switchSignup={switchSignup}
          />
        )}
        {localView === "complete" && <EFAuthenticateMessage signup />}
        {localView === "login" && (
          <LoginForm
            hasThirdPartySignup={thirdPartySignup || router.query.third_party_signup}
            customRedirect={customRedirect || router.asPath}
            organization={organization}
            onAccountClick={() => setLocalView("signup")}
            onPasswordClick={() => setLocalView("forgot_password")}
          />
        )}
        {localView === "forgot_password" && (
          <>
            <div className={Style.modalTitle}>Forgot Password</div>
            <ForgotPasswordTemplate clearState={() => setLocalView("login")} />
            <EFDynamicLink text="Already have an account?" linkText="Log In" onClick={() => setLocalView("login")} />
            <EFDynamicLink text="Don't have an account?" linkText="Sign Up" onClick={() => setLocalView("signup")} />
          </>
        )}
        {localView === "reset_password" && (
          <>
            <div className={Style.modalTitle}>Reset Password</div>
            <ResetPasswordTemplate clearState={() => setLocalView("login")} token={router.query?.token} />
          </>
        )}
      </FeatureFlagVariant>
      <FeatureFlagVariant flagState={false}>
        <div>
          <br />
          <p>Login and Signup are currently disabled. Please check back soon.</p>
          <p>We are sorry for the inconvenience.</p>
        </div>
      </FeatureFlagVariant>
    </FeatureFlag>
  );
};

LoginSignupFlow.defaultProps = {
  signupModalTitle: "Join for Free Today!"
};

export default LoginSignupFlow;
