import React, { useEffect, useState } from "react";
import Form from "react-bootstrap/Form";
import { useRouter } from "next/router";
import { connect } from "react-redux";
import getConfig from "next/config";
import { useGoogleReCaptcha } from "react-google-recaptcha-v3";
import Style from "./LoginTemplate.module.scss";
import { loginUser } from "../../../../store/actions/authActions";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";

const LoginTemplate = ({ login, customRedirect }) => {
  const { publicRuntimeConfig } = getConfig();
  const { environment } = publicRuntimeConfig;

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const router = useRouter();
  const { executeRecaptcha } = useGoogleReCaptcha();

  const getToken = () => {
    if (environment === "production") {
      return executeRecaptcha("login");
    }
    return Promise.resolve("dev");
  };

  useEffect(() => {
    analytics.page("Login");
  }, []);

  const onSubmit = e => {
    e.preventDefault();
    if (e?.target?.checkValidity()) {
      getToken().then(captchaToken => {
        const user = {
          email,
          password,
          captchaToken
        };
        login(user, customRedirect || router.asPath);
      });
    }
  };

  return (
    <>
      <Form className={Style.formContainer} onSubmit={onSubmit}>
        <Form.Label className={Style.formLabel}>Email</Form.Label>
        <Form.Control
          size="lg"
          type="email"
          placeholder="Email Address"
          required
          name="email"
          value={email}
          onChange={e => setEmail(e.target.value)}
          data-cy="login_email_input"
        />
        <Form.Label className={Style.formLabel}>Password</Form.Label>
        <Form.Control
          size="lg"
          type="password"
          placeholder="Password"
          required
          name="password"
          value={password}
          onChange={e => setPassword(e.target.value)}
          data-cy="login_password_input"
        />
        <EFRectangleButton
          text="Log in"
          width="fullWidth"
          buttonType="submit"
          fontCaseTheme="upperCase"
          className={Style.formButton}
        />
      </Form>
    </>
  );
};

export default connect(null, { login: loginUser })(LoginTemplate);
