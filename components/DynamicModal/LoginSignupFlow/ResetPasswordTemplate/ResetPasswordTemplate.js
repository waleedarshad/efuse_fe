import React, { Component } from "react";
import { FormControl } from "react-bootstrap";
import { connect } from "react-redux";
import Router from "next/router";
import Form from "react-bootstrap/Form";

import { resetPassword } from "../../../../store/actions/userActions";
import { setErrors } from "../../../../store/actions/errorActions";
import Style from "./ResetPasswordTemplate.module.scss";
import PasswordStrength from "../../../PasswordStrength/PasswordStrength";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";

class ResetPasswordTemplate extends Component {
  confirmPasswordRef = React.createRef();

  constructor(props) {
    super(props);

    this.state = {
      password: "",
      confirmPassword: "",
      validated: false,
      missMatch: false,
      validPassword: true
    };
  }

  componentDidMount() {
    analytics.page("Reset Password");
  }

  onChange = event => {
    const confirmPassword = event.target.value;
    const { password } = this.state;
    const missMatch = this.matchPassword(password, confirmPassword);
    this.setState({ confirmPassword, missMatch });
  };

  matchPassword = (password, confirmPassword) => {
    let missMatch = false;
    if (password !== confirmPassword) {
      missMatch = true;
      this.confirmPasswordRef.current.setCustomValidity("invalid");
    } else {
      this.confirmPasswordRef.current.setCustomValidity("");
    }
    return missMatch;
  };

  onSubmit = event => {
    event.preventDefault();
    const statesToBeUpdated = {};
    const { validated, password, confirmPassword, validPassword } = this.state;
    if (!validated) {
      statesToBeUpdated.validated = true;
    }
    if (password.length === 0) {
      statesToBeUpdated.validPassword = false;
    }
    const missMatch = this.matchPassword(password, confirmPassword);
    statesToBeUpdated.missMatch = missMatch;
    if (event.currentTarget.checkValidity() && validPassword && !missMatch) {
      const data = {
        password: this.state.password
      };
      this.props.resetPassword(data, this.props.token, Router);
      statesToBeUpdated.missMatch = false;
    }
    this.setState({ ...statesToBeUpdated });
  };

  passwordChange = (password, isValid) => {
    this.setState({
      validPassword: isValid,
      password
    });
  };

  render() {
    const { confirmPassword, validated, missMatch, validPassword } = this.state;

    return (
      <Form className={Style.formContainer} noValidate validated={validated} role="form" onSubmit={this.onSubmit}>
        <div className={Style.password}>
          <Form.Label className={Style.formLabel}>New Password</Form.Label>
          <PasswordStrength
            placeholder="Password"
            changeCallback={this.passwordChange}
            validPassword={validPassword}
            dark
          />
        </div>
        <Form.Label className={Style.formLabel}>Confirm Password</Form.Label>
        <Form.Control
          className={Style.formField}
          size="lg"
          type="password"
          placeholder="Confirm Password"
          required
          errorMessage={missMatch ? "The passwords do not match" : "Confirm password is required"}
          name="confirmPassword"
          value={confirmPassword}
          onChange={this.onChange}
          isInvalid={missMatch}
          ref={this.confirmPasswordRef}
        />
        <FormControl.Feedback type="invalid">
          {missMatch ? "The passwords do not match" : "Confirm password is required"}
        </FormControl.Feedback>
        <div className="mt-5">
          <EFRectangleButton
            text="Reset Password"
            colorTheme="primary"
            width="fullWidth"
            buttonType="submit"
            size="large"
          />
        </div>
      </Form>
    );
  }
}

export default connect(null, { resetPassword, setErrors })(ResetPasswordTemplate);
