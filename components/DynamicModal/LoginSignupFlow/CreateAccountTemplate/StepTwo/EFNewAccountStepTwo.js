import React, { useState } from "react";
import { Col, Row, FormControl } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft } from "@fortawesome/pro-solid-svg-icons";
import { useRouter } from "next/router";
import InputMask from "react-input-mask";
import moment from "moment";
import Style from "../StepsGlobal.module.scss";
import InputWithLabel from "../../../../InputWithLabel/InputWithLabel";

// TODO: update divs to buttons

const EFNewAccountStepTwo = ({
  onChange,
  hasName,
  backStepOne,
  underAge,
  setUnderAge,
  validateBadWords,
  badWordInputs,
  agreedToTermsAndPP,
  onDate,
  newUser,
  setUser
}) => {
  const router = useRouter();

  const [dateInvalid, setDateInvalid] = useState(false);
  const [dateRangeInvalid, setDateRangeInvalid] = useState(false);

  const formattedDate = newUser.dateOfBirth && newUser.dateOfBirth._i;

  const validateDate = date => {
    if (underAge) {
      setUnderAge(false);
    }
    if (date) {
      if (date.includes("D") || date.includes("M") || date.includes("Y") || !moment(date).isValid()) {
        setDateInvalid(true);
        setDateRangeInvalid(false);
        onDate(null);
      } else if (moment(date).isAfter("1900-01-01")) {
        setDateInvalid(false);
        setDateRangeInvalid(false);
        const momentObj = moment(date);
        onDate(momentObj);
      } else {
        setDateInvalid(false);
        setDateRangeInvalid(true);
      }
      if (moment(date, "DD/MM/YYYY").year() < 1900) {
        setDateRangeInvalid(true);
        onDate(null);
      }
    }

    if (date === "MM/DD/YYYY") {
      setDateInvalid(false);
    }
  };

  const errorDisplay = (inputState, label, name) => {
    const message = !inputState ? `${label} is required` : "Sexual or offensive words are not accepted";
    if (!inputState || badWordInputs.includes(name)) {
      return (
        <FormControl.Feedback
          type="invalid"
          style={{
            display: "block",
            marginTop: "-12px",
            paddingBottom: "15px"
          }}
        >
          {message}
        </FormControl.Feedback>
      );
    }
    return <></>;
  };

  // Allowing only letters and spaces
  const validateInput = e => {
    let value = e.target.value.replace(/[^A-z\s]/g, "");
    setUser({ ...newUser, [e.target.name]: value });
  };

  return (
    <div className={Style.formContainer}>
      <div className={Style.backButton} onClick={backStepOne}>
        <FontAwesomeIcon icon={faChevronLeft} />
      </div>
      <Row>
        <Col>
          <InputWithLabel
            dark
            label="Name"
            placeholder="Name"
            name="name"
            value={newUser.name}
            onChange={validateInput}
            onBlur={validateBadWords}
          />
          {errorDisplay(hasName, "Name", "name")}
        </Col>
      </Row>
      <div className={Style.padding}>
        <InputMask
          mask="99/99/9999"
          maskPlaceholder="MM/DD/YYYY"
          alwaysShowMask
          onChange={event => validateDate(event.target.value)}
          onBlur={event => validateDate(event.target.value)}
          value={formattedDate}
        >
          <InputWithLabel dark label="Date of Birth (MM/DD/YYYY)" placeholder="MM/DD/YYYY" name="dob" />
        </InputMask>
        {dateInvalid && (
          <FormControl.Feedback type="invalid" style={{ display: "block" }}>
            Date is not valid
          </FormControl.Feedback>
        )}
        {dateRangeInvalid && (
          <FormControl.Feedback type="invalid" style={{ display: "block" }}>
            Date year must be greater than 1900
          </FormControl.Feedback>
        )}

        {underAge && (
          <FormControl.Feedback type="invalid" style={{ display: "block" }}>
            You must be at least 13 years or older
          </FormControl.Feedback>
        )}
      </div>

      <div className={Style.checkboxWrapper}>
        {router.query.institutionSlug && (
          <label className={Style.checkboxContainer}>
            <input type="checkbox" name="useAlias" onChange={onChange} checked={newUser.useAlias} />
            <span>Alias my name to protect my identity (Recommended)</span>
          </label>
        )}

        <label className={Style.checkboxContainer}>
          <input type="checkbox" name="weeklyNewsLetter" onChange={onChange} checked={newUser.weeklyNewsLetter} />
          <span>Subscribe to eFuse Promotional Materials</span>
        </label>
        <label className={Style.checkboxContainer}>
          <input
            type="checkbox"
            name="agreedToPrivacy"
            onChange={onChange}
            checked={newUser.agreedToPrivacy}
            data-cy="privacy_policy_checkbox"
          />
          <span>
            I agree to the{" "}
            <a target="_blank" rel="noreferrer" href="https://efuse.gg/privacy" className={Style.marginRight}>
              Privacy Policy
            </a>
            and
            <a target="_blank" rel="noreferrer" href="https://efuse.gg/terms" className={Style.marginLeft}>
              Terms of Service
            </a>
          </span>
        </label>
        {!agreedToTermsAndPP && (
          <FormControl.Feedback type="invalid" style={{ display: "block" }}>
            Please agree to the Terms of Service and Privacy Policy
          </FormControl.Feedback>
        )}
      </div>
    </div>
  );
};

export default EFNewAccountStepTwo;
