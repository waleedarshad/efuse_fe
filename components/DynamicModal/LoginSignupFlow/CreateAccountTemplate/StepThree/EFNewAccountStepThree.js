import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft } from "@fortawesome/pro-solid-svg-icons";

import EFPathwaysSelection from "../../../../EFPathwaysSelection/EFPathwaysSelection";
import EFRectangleButton from "../../../../Buttons/EFRectangleButton/EFRectangleButton";
import Style from "../StepsGlobal.module.scss";

const EFNewAccountStepThree = ({ selectedPathwayID, setSelectedPathwayID, onSubmit, setStep }) => {
  const onCardSelect = selectedCardId => {
    setSelectedPathwayID(selectedCardId);
  };

  return (
    <>
      <div className={Style.backButton} onClick={() => setStep(2)}>
        <FontAwesomeIcon icon={faChevronLeft} />
      </div>
      <div>
        <div className={Style.title}>Choose your Journey</div>
        <p className={Style.subTitle}>Pick the option that best describes you.</p>
      </div>
      <EFPathwaysSelection onCardSelect={onCardSelect} selectedPathwayID={selectedPathwayID} />
      <EFRectangleButton
        colorTheme="secondary"
        disabled={!selectedPathwayID}
        text="Complete"
        width="fullWidth"
        onClick={() => {
          analytics.track("PATHWAY_SELECTED", { location: "Signup form" });
          onSubmit();
        }}
      />
    </>
  );
};

export default EFNewAccountStepThree;
