import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Form } from "react-bootstrap";
import toLower from "lodash/toLower";
import { useRouter } from "next/router";
import Cookies from "js-cookie";
import getConfig from "next/config";
import { useGoogleReCaptcha } from "react-google-recaptcha-v3";
import Style from "./CreateAccountTemplate.module.scss";
import { getBrowserInfo, defaultDobYear, getAge } from "../../../../helpers/GeneralHelper";
import {
  validateEmailInput,
  validateUsernameInput,
  clearSignupValidationErrors
} from "../../../../store/actions/authActions";
import { createUser, toggleInProgress } from "../../../../store/actions/userActions";
import EFNewAccountStepOne from "./StepOne/EFNewAccountStepOne";
import badWords from "../../../../static/data/badWords.json";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";
import EFNewAccountStepTwo from "./StepTwo/EFNewAccountStepTwo";
import EFNewAccountStepThree from "./StepThree/EFNewAccountStepThree";

const { publicRuntimeConfig } = getConfig();
const { environment } = publicRuntimeConfig;

const CreateAccountTemplate = ({
  signupLocationForTracking,
  redirectUrl,
  organizationId,
  switchSignup,
  setCurrentStep
}) => {
  const router = useRouter();
  const dispatch = useDispatch();
  const { executeRecaptcha } = useGoogleReCaptcha();

  const signupFormValidationErrors = useSelector(state => state.auth.signupFormValidationErrors);
  const availablePathways = useSelector(state => state.onboarding.pathways);
  let enableNextButton;

  const initialUser = {
    email: "",
    name: "",
    dateOfBirth: "",
    username: "",
    password: "",
    signupToken: "",
    userSignupDevice: "web",
    agreedToPrivacy: false,
    referral: "",
    userAgent: "",
    weeklyNewsLetter: true,
    useAlias: false,
    pathway: ""
  };
  const userAgent = getBrowserInfo();

  const [localUser, setLocalUser] = useState(initialUser);
  const [validated, setValidated] = useState(false);
  const [underAge, setUnderAge] = useState(false);
  const [hasName, setHasName] = useState(true);
  const [agreedToTermsAndPP, setAgreedToTermsAndPP] = useState(true);
  const [validPassword, setValidPassword] = useState(true);
  const [step, setStep] = useState(1);
  const [badWordInputs, setBadWordInputs] = useState([]);
  const [selectedPathwayID, setSelectedPathwayID] = useState("");

  useEffect(() => {
    analytics.page("Signup");
    analytics.track("SIGNUP_STEP_ONE");

    dispatch(clearSignupValidationErrors());
  }, []);

  useEffect(() => {
    setCurrentStep(step);
  }, [step]);

  const getToken = (action = "signup") => {
    if (environment === "production") {
      return executeRecaptcha(action);
    }
    return Promise.resolve("dev");
  };

  const updateFieldOnChange = event => {
    const newVal = event.target.type !== "checkbox" ? event.target.value : event.target.checked;
    const newUser = { ...localUser, [event.target.name === "efuseUsername" ? "username" : event.target.name]: newVal };

    setLocalUser(newUser);

    if (event.target.name === "name") {
      !event.target.value || event.target.value === "" ? setHasName(false) : setHasName(true);
    }

    if (event.target.name === "agreedToPrivacy") {
      !event.target.checked ? setAgreedToTermsAndPP(false) : setAgreedToTermsAndPP(true);
    }
  };

  const validateBadWords = event => {
    const { name, value } = event.target;
    let updatedInputs = [];
    if (badWords.includes(toLower(value))) {
      updatedInputs = [...badWordInputs, name];
      setBadWordInputs(updatedInputs);
      event.target.setCustomValidity("invalid");
    } else {
      updatedInputs = [...badWordInputs.filter(a => a !== name)];
      setBadWordInputs(updatedInputs);
      event.target.setCustomValidity("");
    }
  };

  const updateBirthdateOnChange = date => {
    const newUser = {
      ...localUser,
      dateOfBirth: defaultDobYear(date)
    };

    setLocalUser(newUser);
  };

  const validateEmailAndVerifyAvailable = event => {
    const email = event.target.value;
    dispatch(validateEmailInput(email));
  };

  const validateUsernameAndVerifyAvailable = event => {
    const username = event.target.value;
    dispatch(validateUsernameInput(username));
  };

  const validatePasswordStrength = (password, isValid) => {
    const newUser = {
      ...localUser,
      password
    };

    setValidPassword(isValid);
    setLocalUser(newUser);
  };

  const goBackToStepOne = () => {
    enableNextButton = true;
    setStep(1);
  };

  const goToStepTwo = event => {
    let stepOneHasAProblem = false;
    event.preventDefault();
    event.stopPropagation();

    setAgreedToTermsAndPP(true);
    setValidated(true);

    // Validation check - must enter a password and meet requirements
    if (!localUser.password || localUser.password === "" || !validPassword) {
      setValidPassword(false);
      stepOneHasAProblem = true;
      analytics.track("SIGNUP_FIELD_ERROR", { errorType: "Password" });
    }

    // Validation check - must have unique username
    // revalidate username input because it's possible to bypass the onBlur with a password manager
    if (signupFormValidationErrors.username) {
      stepOneHasAProblem = true;
      analytics.track("SIGNUP_FIELD_ERROR", { errorType: "Username" });
    }

    // Validation check - must have unique email
    if (signupFormValidationErrors.email) {
      stepOneHasAProblem = true;
      analytics.track("SIGNUP_FIELD_ERROR", { errorType: "Email" });
    }

    // Validation check - all fields must pass requirements
    if (event.target.checkValidity() === false) {
      stepOneHasAProblem = true;
      analytics.track("SIGNUP_FIELD_ERROR", { errorType: "Validation failed step one" });
    }

    if (!stepOneHasAProblem) {
      setStep(2);
      analytics.track("SIGNUP_STEP_TWO");
    } else {
      analytics.track("SIGNUP_FIELD_ERROR", { errorType: "Step one error" });
    }
  };

  const goToStepThree = event => {
    let formHasAProblem = false;
    event.preventDefault();
    event.stopPropagation();

    setAgreedToTermsAndPP(true);
    setValidated(true);
    setUnderAge(false);

    if (!localUser.agreedToPrivacy) {
      setAgreedToTermsAndPP(false);
      formHasAProblem = true;
      analytics.track("SIGNUP_FIELD_ERROR", { errorType: "TOS checkbox" });
    }

    if (localUser.name === "") {
      setHasName(false);
      formHasAProblem = true;
      analytics.track("SIGNUP_FIELD_ERROR", { errorType: "Name" });
    }

    if (getAge(localUser.dateOfBirth) < 13) {
      setUnderAge(true);
      formHasAProblem = true;
      analytics.track("SIGNUP_FIELD_ERROR", { errorType: "Under age" });
    }

    if (badWordInputs.length > 0) {
      formHasAProblem = true;
      analytics.track("SIGNUP_FIELD_ERROR", { errorType: "Bad words" });
    }

    if (event.target.checkValidity() === false) {
      formHasAProblem = true;
      analytics.track("SIGNUP_FIELD_ERROR", { errorType: "Validation failed step two" });
    }

    if (!formHasAProblem) {
      setStep(3);
      analytics.track("SIGNUP_STEP_THREE");
    } else {
      analytics.track("SIGNUP_STEP_TWO_ERROR");
    }
  };

  const handleFinalSubmit = () => {
    const referral = Cookies.get("i") || "self";
    const referralUrl = Cookies.get("referralUrl") || "";
    const signupUrl = window.location.href;

    const getFinalUserForSubmission = captchaToken => {
      const newUser = {
        ...localUser,
        referral,
        userAgent,
        referralUrl,
        signupUrl,
        captchaToken,
        pathway: selectedPathwayID
      };

      // This is required for signup through an org.
      // This field added to the user on the request body sent will trigger
      // an async job to add the new user as a member of the org.
      if (organizationId) {
        newUser.organization = organizationId;
      }

      return newUser;
    };

    const selectedPathway = availablePathways.filter(pathway => pathway._id === selectedPathwayID)[0];
    let customRedirectUrl = redirectUrl;

    // give customRedirectUrl (URL param) preference over the pathway redirect
    // if no customRedirectUrl is set and the user selected the organization pathway, redirect to the organization create page
    if (selectedPathway?.webRedirectUrl === "/organizations/new" && customRedirectUrl === "/welcome") {
      customRedirectUrl = selectedPathway.webRedirectUrl;
    }

    getToken().then(captchaToken => {
      const newUser = getFinalUserForSubmission(captchaToken);
      setLocalUser(newUser);

      dispatch(toggleInProgress(true));
      dispatch(
        createUser(
          { user: newUser },
          "/auth/register",
          router,
          customRedirectUrl,
          error => {
            dispatch(toggleInProgress(false));
            analytics.track("SIGNUP_ERROR_FAILED_CREATION", error);
            switchSignup("signup");
          },
          signupLocationForTracking
        )
      );

      Cookies.remove("i");
      Cookies.remove("referralUrl");

      switchSignup("complete");
    });
  };

  enableNextButton =
    signupFormValidationErrors.email === "" &&
    signupFormValidationErrors.username === "" &&
    validPassword &&
    localUser.password;

  const disableSubmitButton = localUser.name && localUser.dateOfBirth && localUser.agreedToPrivacy;

  return (
    <>
      {step === 1 && (
        <Form className={Style.formContainer} onSubmit={e => goToStepTwo(e)} noValidate validated={validated}>
          <EFNewAccountStepOne
            onChange={updateFieldOnChange}
            passwordChange={validatePasswordStrength}
            validPassword={validPassword}
            validateEmail={validateEmailAndVerifyAvailable}
            validateUsername={validateUsernameAndVerifyAvailable}
            newUser={localUser}
          />
          <EFRectangleButton
            colorTheme="secondary"
            text="Next"
            width="fullWidth"
            disabled={!enableNextButton}
            buttonType="submit"
          />
        </Form>
      )}
      {step === 2 && (
        <Form className={Style.formContainer} onSubmit={e => goToStepThree(e)} noValidate validated={validated}>
          <EFNewAccountStepTwo
            onChange={updateFieldOnChange}
            onDate={updateBirthdateOnChange}
            hasName={hasName}
            underAge={underAge}
            setUnderAge={setUnderAge}
            backStepOne={goBackToStepOne}
            validateBadWords={validateBadWords}
            badWordInputs={badWordInputs}
            agreedToTermsAndPP={agreedToTermsAndPP}
            newUser={localUser}
            setUser={setLocalUser}
          />
          <EFRectangleButton
            colorTheme="secondary"
            text="Next"
            width="fullWidth"
            disabled={!disableSubmitButton}
            buttonType="submit"
          />
        </Form>
      )}
      {step === 3 && (
        <EFNewAccountStepThree
          selectedPathwayID={selectedPathwayID}
          setSelectedPathwayID={setSelectedPathwayID}
          onSubmit={handleFinalSubmit}
          setStep={setStep}
        />
      )}
    </>
  );
};

export const getServerSideProps = req => {
  return {
    namespacesRequired: ["public"],
    signupToken: req.query.token
  };
};

export default CreateAccountTemplate;
