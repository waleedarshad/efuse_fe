import { useSelector } from "react-redux";
import { Form } from "react-bootstrap";
import toLower from "lodash/toLower";
import React from "react";
import PasswordStrength from "../../../../PasswordStrength/PasswordStrength";
import InputWithLabel from "../../../../InputWithLabel/InputWithLabel";
import Style from "../StepsGlobal.module.scss";

const EFNewAccountStepOne = ({
  onChange,
  passwordChange,
  validPassword,
  validateEmail,
  validateUsername,
  emailRef,
  usernameRef,
  newUser
}) => {
  const signupFormValidationErrors = useSelector(state => state.auth.signupFormValidationErrors);

  return (
    <div className={Style.formContainer}>
      <div className={Style.label}>
        <InputWithLabel
          dark
          label="Email"
          placeholder="example@email.com"
          name="email"
          value={newUser.email}
          onChange={onChange}
          onBlur={validateEmail}
          inputRef={emailRef}
          data-cy="signup_email_input"
          onFocus={() => analytics.track("SIGNUP_STEP_ONE_INPUT", { inputName: "email" })}
        />
        <div className={Style.inputError}>
          {signupFormValidationErrors.email ? signupFormValidationErrors.email : ""}
        </div>
      </div>
      <div className={Style.label}>
        <InputWithLabel
          dark
          label="Username"
          placeholder="Ex. chocoTaco"
          name="efuseUsername"
          value={newUser.username}
          onChange={onChange}
          onBlur={validateUsername}
          inputRef={usernameRef}
          hotjarWhiteList
          data-cy="signup_username_input"
          onFocus={() => analytics.track("SIGNUP_STEP_ONE_INPUT", { inputName: "username" })}
        />
        <div className={Style.inputError}>
          {signupFormValidationErrors.username ? signupFormValidationErrors.username : ""}
        </div>
      </div>
      <div className={Style.password}>
        <Form.Label className={`${Style.formLabel}`}>Password</Form.Label>
        <PasswordStrength
          changeCallback={passwordChange}
          validPassword={validPassword}
          firstName={toLower(newUser.firstName)}
          lastName={toLower(newUser.lastName)}
          defaultValue={newUser.password}
          dark
        />
      </div>
    </div>
  );
};

export default EFNewAccountStepOne;
