import React from "react";
import Style from "./RecaptchaBranding.module.scss";

const RecaptchaBranding = () => {
  return (
    <p className={Style.recaptchaBranding}>
      This site is protected by reCAPTCHA and the Google{" "}
      <a href="https://policies.google.com/privacy">Privacy Policy</a> and{" "}
      <a href="https://policies.google.com/terms">Terms of Service</a> apply
    </p>
  );
};

export default RecaptchaBranding;
