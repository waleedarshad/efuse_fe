import React, { useContext } from "react";
import { Form } from "react-bootstrap";
import EFRectangleButton from "../../../../Buttons/EFRectangleButton/EFRectangleButton";
import { EFWizardContext } from "../../../../EFWizard/EFWizard";
import InputWithLabel from "../../../../InputWithLabel/InputWithLabel";
import PasswordInputWithIndicator from "../../../../PasswordStrengthV2/PasswordInputWithIndicator/PasswordInputWithIndicator";
import Style from "../EFCreateAccountStepStyling.module.scss";

const EFCreateAccountStepOne = ({
  user,
  onPasswordChange,
  onEmailChange,
  onUsernameChange,
  nextButtonDisabled,
  onEmailBlur,
  onUsernameBlur,
  inputErrors
}) => {
  const context = useContext(EFWizardContext);

  return (
    <div className={Style.formContainer}>
      <div className={Style.label}>
        <InputWithLabel
          dark
          label="Email"
          placeholder="example@email.com"
          name="email"
          value={user.email}
          onChange={onEmailChange}
          onBlur={onEmailBlur}
        />
        <div className={Style.inputError}>{inputErrors?.email ? inputErrors.email : ""}</div>
      </div>
      <div className={Style.label}>
        <InputWithLabel
          dark
          label="Username"
          placeholder="Ex. chocoTaco"
          name="eFuseUsername"
          value={user.username}
          onChange={onUsernameChange}
          onBlur={onUsernameBlur}
          hotjarWhiteList
        />
        <div className={Style.inputError}>{inputErrors?.username ? inputErrors.username : ""}</div>
      </div>
      <div className={Style.password}>
        <Form.Label className={`${Style.formLabel}`}>Password</Form.Label>
        <PasswordInputWithIndicator onPasswordChange={onPasswordChange} dark />
      </div>
      <EFRectangleButton
        colorTheme="secondary"
        text="Next"
        width="fullWidth"
        buttonType="submit"
        onClick={context.goNextStep}
        disabled={nextButtonDisabled}
      />
    </div>
  );
};

export default EFCreateAccountStepOne;
