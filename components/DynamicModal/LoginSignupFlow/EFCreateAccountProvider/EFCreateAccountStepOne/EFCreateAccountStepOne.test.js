import { mount } from "enzyme";
import React from "react";
import EFCreateAccountStepOne from "./EFCreateAccountStepOne";
import InputWithLabel from "../../../../InputWithLabel/InputWithLabel";
import { EFWizardContext } from "../../../../EFWizard/EFWizard";
import PasswordInputWithIndicator from "../../../../PasswordStrengthV2/PasswordInputWithIndicator/PasswordInputWithIndicator";
import EFRectangleButton from "../../../../Buttons/EFRectangleButton/EFRectangleButton";

const mockOnPasswordChange = jest.fn();
const mockOnEmailChange = jest.fn();
const mockOnUsernameChange = jest.fn();
const mockGoNextStep = jest.fn();

describe("EFCreateAccountStepOne", () => {
  const user = {
    email: "coolEmail@email.com",
    username: "prettyCoolUsername"
  };

  const contextProps = {
    goNextStep: mockGoNextStep
  };

  it("renders the correct email input", () => {
    const subject = mount(
      <EFWizardContext.Provider value={contextProps}>
        <EFCreateAccountStepOne
          user={user}
          onPasswordChange={mockOnPasswordChange}
          onEmailChange={mockOnEmailChange}
          onUsernameChange={mockOnUsernameChange}
          nextButtonDisabled
        />
      </EFWizardContext.Provider>
    );

    expect(
      subject
        .find(InputWithLabel)
        .at(0)
        .prop("value")
    ).toEqual("coolEmail@email.com");
  });

  it("renders the correct username input", () => {
    const subject = mount(
      <EFWizardContext.Provider value={contextProps}>
        <EFCreateAccountStepOne
          user={user}
          onPasswordChange={mockOnPasswordChange}
          onEmailChange={mockOnEmailChange}
          onUsernameChange={mockOnUsernameChange}
          nextButtonDisabled
        />
      </EFWizardContext.Provider>
    );

    expect(
      subject
        .find(InputWithLabel)
        .at(1)
        .prop("value")
    ).toEqual("prettyCoolUsername");
  });

  it("renders the correct password input", () => {
    const subject = mount(
      <EFWizardContext.Provider value={contextProps}>
        <EFCreateAccountStepOne
          user={user}
          onPasswordChange={mockOnPasswordChange}
          onEmailChange={mockOnEmailChange}
          onUsernameChange={mockOnUsernameChange}
          nextButtonDisabled
        />
      </EFWizardContext.Provider>
    );

    expect(subject.find(PasswordInputWithIndicator)).toHaveLength(1);
  });

  it("has a disabled button if inputs are not valid", () => {
    const subject = mount(
      <EFWizardContext.Provider value={contextProps}>
        <EFCreateAccountStepOne
          user={user}
          onPasswordChange={mockOnPasswordChange}
          onEmailChange={mockOnEmailChange}
          onUsernameChange={mockOnUsernameChange}
          nextButtonDisabled
        />
      </EFWizardContext.Provider>
    );

    expect(subject.find(EFRectangleButton).prop("disabled")).toBe(true);
  });

  it("has an enabled button if all fields are valid", () => {
    const subject = mount(
      <EFWizardContext.Provider value={contextProps}>
        <EFCreateAccountStepOne
          user={user}
          onPasswordChange={mockOnPasswordChange}
          onEmailChange={mockOnEmailChange}
          onUsernameChange={mockOnUsernameChange}
          nextButtonDisabled={false}
        />
      </EFWizardContext.Provider>
    );

    expect(subject.find(EFRectangleButton).prop("disabled")).toBe(false);
  });

  it("displays the correct email error message for an invalid email", () => {
    const inputErrors = { email: "Please enter a valid email address" };

    const subject = mount(
      <EFWizardContext.Provider value={contextProps}>
        <EFCreateAccountStepOne
          user={user}
          onPasswordChange={mockOnPasswordChange}
          onEmailChange={mockOnEmailChange}
          onUsernameChange={mockOnUsernameChange}
          nextButtonDisabled={false}
          inputErrors={inputErrors}
        />
      </EFWizardContext.Provider>
    );

    expect(
      subject
        .find(".inputError")
        .at(0)
        .text()
    ).toEqual("Please enter a valid email address");
  });

  it("displays a blank div if no email error", () => {
    const inputErrors = { email: "" };

    const subject = mount(
      <EFWizardContext.Provider value={contextProps}>
        <EFCreateAccountStepOne
          user={user}
          onPasswordChange={mockOnPasswordChange}
          onEmailChange={mockOnEmailChange}
          onUsernameChange={mockOnUsernameChange}
          nextButtonDisabled={false}
          inputErrors={inputErrors}
        />
      </EFWizardContext.Provider>
    );

    expect(
      subject
        .find(".inputError")
        .at(0)
        .text()
    ).toEqual("");
  });

  it("displays the correct username error message for an invalid username", () => {
    const inputErrors = { username: "Username has already been taken" };

    const subject = mount(
      <EFWizardContext.Provider value={contextProps}>
        <EFCreateAccountStepOne
          user={user}
          onPasswordChange={mockOnPasswordChange}
          onEmailChange={mockOnEmailChange}
          onUsernameChange={mockOnUsernameChange}
          nextButtonDisabled={false}
          inputErrors={inputErrors}
        />
      </EFWizardContext.Provider>
    );

    expect(
      subject
        .find(".inputError")
        .at(1)
        .text()
    ).toEqual("Username has already been taken");
  });

  it("displays a blank div if no username error", () => {
    const inputErrors = { username: "" };

    const subject = mount(
      <EFWizardContext.Provider value={contextProps}>
        <EFCreateAccountStepOne
          user={user}
          onPasswordChange={mockOnPasswordChange}
          onEmailChange={mockOnEmailChange}
          onUsernameChange={mockOnUsernameChange}
          nextButtonDisabled={false}
          inputErrors={inputErrors}
        />
      </EFWizardContext.Provider>
    );

    expect(
      subject
        .find(".inputError")
        .at(1)
        .text()
    ).toEqual("");
  });
});
