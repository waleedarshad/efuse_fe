import EFRectangleButton from "../../../../Buttons/EFRectangleButton/EFRectangleButton";
import React, { useContext } from "react";
import { EFWizardContext } from "../../../../EFWizard/EFWizard";

const EFCreateAccountStepTwo = () => {
  const { goPrevStep } = useContext(EFWizardContext);

  return (
    <div>
      <h3>This is Step 2.</h3>
      <EFRectangleButton
        colorTheme="secondary"
        text="Back"
        width="autoWidth"
        buttonType="submit"
        onClick={goPrevStep}
      />
    </div>
  );
};

export default EFCreateAccountStepTwo;
