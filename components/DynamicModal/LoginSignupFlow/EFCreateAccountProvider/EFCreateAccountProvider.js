import React, { useState } from "react";
import { Form } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import EFWizard from "../../../EFWizard/EFWizard";
import EFCreateAccountStepOne from "./EFCreateAccountStepOne/EFCreateAccountStepOne";
import EFCreateAccountStepTwo from "./EFCreateAccountStepTwo/EFCreateAccountStepTwo";
import { validateEmailInput, validateUsernameInput } from "../../../../store/actions/authActions";
import Style from "./EFCreateAccountProvider.module.scss";

const EFCreateAccountProvider = () => {
  const dispatch = useDispatch();

  const { signupFormValidationErrors } = useSelector(state => state.auth);

  const initialUserState = {
    email: "",
    username: "",
    password: "",
    passwordIsValid: false
  };

  const [newUser, setNewUser] = useState(initialUserState);

  const validatePasswordStrength = (password, isValid) => {
    const user = {
      ...newUser,
      password,
      passwordIsValid: isValid
    };

    setNewUser(user);
  };

  const onEmailChange = event => {
    const user = {
      ...newUser,
      email: event.target.value
    };

    setNewUser(user);
  };

  const onUsernameChange = event => {
    const user = {
      ...newUser,
      username: event.target.value
    };

    setNewUser(user);
  };

  const formHasValidEmail = signupFormValidationErrors.email === "";

  const nextButtonIsEnabled = formHasValidEmail && newUser?.username !== "" && newUser?.passwordIsValid;

  return (
    <Form className={Style.formContainer}>
      <EFWizard>
        <EFCreateAccountStepOne
          user={newUser}
          onPasswordChange={validatePasswordStrength}
          onEmailChange={onEmailChange}
          onUsernameChange={onUsernameChange}
          nextButtonDisabled={!nextButtonIsEnabled}
          onEmailBlur={event => {
            dispatch(validateEmailInput(event.target.value));
          }}
          onUsernameBlur={event => {
            dispatch(validateUsernameInput(event.target.value));
          }}
          inputErrors={signupFormValidationErrors}
        />
        <EFCreateAccountStepTwo />
      </EFWizard>
    </Form>
  );
};

export default EFCreateAccountProvider;
