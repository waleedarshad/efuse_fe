import React, { useState } from "react";

import CreateAccountTemplate from "../CreateAccountTemplate/CreateAccountTemplate";
import EFDynamicLink from "../../../EFDynamicLink/EFDynamicLink";
import Style from "../LoginSignupFlow.module.scss";
import { getImage } from "../../../../helpers/GeneralHelper";
import {
  getJoinTextFromInviteCode,
  getNameFromInviteCode,
  getProfileImageFromInviteCode,
  getRouteFromInviteCode
} from "../../../../helpers/InviteCodeHelper";
import InviteHeader from "../InviteHeader/InviteHeader";

const SignupTemplate = ({
  inviteCode,
  signupModalTitle,
  localView,
  signupLocationForTracking,
  onClick,
  redirectUrl,
  switchSignup
}) => {
  const [currentStep, setCurrentStep] = useState(null);

  return (
    <>
      {inviteCode && (
        <InviteHeader
          profilePictureUrl={getImage(getProfileImageFromInviteCode(inviteCode), "avatar")}
          inviteCodeRoute={getRouteFromInviteCode(inviteCode)}
          inviteCodeName={getNameFromInviteCode(inviteCode)}
          inviteCodeJoinText={getJoinTextFromInviteCode(inviteCode)}
          organizationDescription={inviteCode?.organization?.description}
        />
      )}
      {!inviteCode && currentStep !== 3 && localView === "signup" && (
        <div className={Style.modalTitle}>{signupModalTitle}</div>
      )}
      <CreateAccountTemplate
        signupLocationForTracking={signupLocationForTracking}
        redirectUrl={redirectUrl}
        organizationId={inviteCode?.organization?._id}
        switchSignup={switchSignup}
        setCurrentStep={setCurrentStep}
      />
      <EFDynamicLink text="Already have an account?" linkText="Log In" onClick={onClick} />
    </>
  );
};

export default SignupTemplate;
