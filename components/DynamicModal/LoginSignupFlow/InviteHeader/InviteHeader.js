import Link from "next/link";
import React from "react";
import EFAvatar from "../../../EFAvatar/EFAvatar";
import Style from "./InviteHeader.module.scss";

const InviteHeader = ({
  profilePictureUrl,
  inviteCodeRoute,
  inviteCodeName,
  inviteCodeJoinText,
  organizationDescription
}) => {
  return (
    <>
      <div className={Style.topContainer}>
        <div>
          <EFAvatar displayOnlineButton={false} profilePicture={profilePictureUrl} size="extraLarge" />
        </div>
        <div>
          <h3 className={Style.header}>
            <Link href={inviteCodeRoute}>{inviteCodeName}</Link> invited you to join {inviteCodeJoinText}
          </h3>
        </div>
      </div>
      <div className={Style.descriptionContainer}>
        <p>{organizationDescription}</p>
      </div>
    </>
  );
};

export default InviteHeader;
