import React from "react";
import ExternalSignupServices from "../../../ExternalAuth/ExternalSignupServices/ExternalSignupServices";
import LoginTemplate from "../LoginTemplate/LoginTemplate";
import EFDynamicLink from "../../../EFDynamicLink/EFDynamicLink";
import EFAvatar from "../../../EFAvatar/EFAvatar";
import Style from "./LoginForm.module.scss";
import RecaptchaBranding from "../RecaptchaBranding/RecaptchaBranding";

const LoginForm = ({ organization, hasThirdPartySignup, customRedirect, onAccountClick, onPasswordClick }) => {
  const eFuseLogo = (
    <img
      alt="eFuse logo"
      className={Style.efuseLogo}
      src="https://cdn.efuse.gg/uploads/static/global/efuseLogoDark.png"
    />
  );

  return (
    <>
      <div className={Style.modalTitle}>
        {organization && (
          <div className={Style.loginLogo}>
            <EFAvatar displayOnlineButton={false} profilePicture={organization?.profileImage?.url} size="extraLarge" />
          </div>
        )}
        Log in to
        {organization ? <h3 className={Style.header}>{organization?.name}</h3> : eFuseLogo}
      </div>
      {!organization && hasThirdPartySignup && <ExternalSignupServices isLogin />}
      <LoginTemplate customRedirect={customRedirect} />
      <EFDynamicLink text="Don't have an account?" linkText="Sign Up" onClick={onAccountClick} />
      <EFDynamicLink text="Forgot Password?" linkText="Reset" onClick={onPasswordClick} />
      <RecaptchaBranding />
    </>
  );
};

export default LoginForm;
