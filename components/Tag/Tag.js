import PropTypes from "prop-types";

import Style from "./Tag.module.scss";
import CloseButton from "../CloseButton/CloseButton";

const Tag = ({ title, onClose, close }) => {
  return (
    <span className={Style.tagWrapper}>
      <span>{title}</span>
      {close && <CloseButton onClick={onClose} buttonSize="extraSmall" />}
    </span>
  );
};

Tag.propTypes = {
  title: PropTypes.string.isRequired,
  close: PropTypes.bool,
  onClose: PropTypes.func
};

Tag.defaultProps = {
  close: true
};

export default Tag;
