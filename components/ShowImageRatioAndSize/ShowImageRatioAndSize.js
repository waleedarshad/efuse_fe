import React from "react";

import Style from "./ShowImageRatioAndSize.module.scss";

const ShowImageRatioAndSize = ({ width, height }) => {
  const calculateRatio = (num1, num2) => {
    let ratioNum1 = num1;
    let ratioNum2 = num2;

    for (let num = ratioNum2; num > 1; num--) {
      if (ratioNum1 % num === 0 && ratioNum2 % num === 0) {
        ratioNum1 /= num;
        ratioNum2 /= num;
      }
    }
    const ratio = `${ratioNum1}:${ratioNum2}`;
    return ratio;
  };

  return (
    <div className={Style.imageDescription}>
      <p>
        {width}px x {height}px ({calculateRatio(width, height)} ratio)
      </p>
    </div>
  );
};

export default ShowImageRatioAndSize;
