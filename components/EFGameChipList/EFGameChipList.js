import uniqueId from "lodash/uniqueId";
import React from "react";
import getFoundGamesHook from "../../hooks/getHooks/getFoundGamesHook";
import EFGamesChip from "../EFGamesChip/EFGamesChip";
import Style from "./EFGameChipList.module.scss";

const EFGameChipList = ({ games }) => {
  const foundGames = getFoundGamesHook(games);

  return (
    <div className={Style.gamesChipWrapper}>
      {foundGames?.map(game => {
        return (
          <div key={uniqueId()} className={Style.addChipMargin}>
            <EFGamesChip.Info image={game?.gameImage?.url} title={game?.title} />
          </div>
        );
      })}
    </div>
  );
};

export default EFGameChipList;
