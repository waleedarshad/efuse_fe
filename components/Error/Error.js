import React from "react";
import { useSelector } from "react-redux";
import { Alert } from "react-bootstrap";
import ErrorList from "./ErrorList/ErrorList";
import { isEmptyObject } from "../../helpers/GeneralHelper";

const Error = () => {
  const errors = useSelector(state => state.errors.errors);

  return !isEmptyObject(errors) ? (
    <Alert variant="danger">
      <Alert.Heading>You got an error!</Alert.Heading>
      <ErrorList errors={errors} />
    </Alert>
  ) : (
    <></>
  );
};

export default Error;
