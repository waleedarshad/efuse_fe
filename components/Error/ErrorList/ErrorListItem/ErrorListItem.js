import startCase from "lodash/startCase";

const ErrorListItem = ({ error, fieldName }) => (
  <li>
    <strong>{startCase(fieldName)}</strong>
    {` ${error}`}
  </li>
);

export default ErrorListItem;
