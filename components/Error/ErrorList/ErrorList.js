import Style from "./ErrorList.module.scss";
import ErrorListItem from "./ErrorListItem/ErrorListItem";

const ErrorList = ({ errors }) => {
  const errorListItems = Object.keys(errors).map((errorKey, index) => (
    <ErrorListItem key={index} error={errors[errorKey]} fieldName={errorKey} />
  ));
  return <ul className={Style.errorList}>{errorListItems}</ul>;
};

export default ErrorList;
