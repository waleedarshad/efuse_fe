import React, { useState, useEffect } from "react";
import { Col, Form } from "react-bootstrap";
import { useDispatch } from "react-redux";

import InputLabel from "../../../InputLabel/InputLabel";
import SelectBox from "../../../SelectBox/SelectBox";
import InputRow from "../../../InputRow/InputRow";
import InputField from "../../../InputField/InputField";
import HelpToolTip from "../../../HelpToolTip/HelpToolTip";
import { updateTournament } from "../../../../store/actions/erenaActions";
import Style from "./UpdateTournament.module.scss";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";
import EFHtmlInput from "../../../EFHtmlInput/EFHtmlInput";

const UpdateTournament = ({ tournament, currentUserOrgs, currentUser }) => {
  const validated = false;
  const dispatch = useDispatch();
  const [creatorLabel, setCreatorLabel] = useState("");
  const [creatorValue, setCreatorValue] = useState("");
  const [erenaEvent, setErenaEvent] = useState({
    owner: "",
    tournamentName: "",
    tournamentDescription: "",
    rulesMarkdown: "",
    twitchChannel: ""
  });

  const getOwnerType = owner => {
    const ownerType = currentUserOrgs.find(org => {
      return org._id === owner;
    })
      ? "organizations"
      : "users";
    return ownerType;
  };

  useEffect(() => {
    if (tournament) {
      setErenaEvent({
        ...erenaEvent,
        owner: tournament.owner,
        tournamentName: tournament.tournamentName,
        tournamentDescription: tournament.tournamentDescription,
        rulesMarkdown: tournament.rulesMarkdown,
        twitchChannel: tournament.twitchChannel
      });
      currentUserOrgs.forEach(org => {
        if (org._id === tournament.owner) {
          setCreatorLabel(org.name);
          setCreatorValue(org._id);
        } else if (org._id === currentUser?.id) {
          setCreatorLabel(currentUser?.name);
          setCreatorValue(tournament.owner);
        }
      });
    }
  }, [tournament]);

  let formatedCreators = [];

  if (currentUserOrgs) {
    currentUserOrgs.map(org => {
      return formatedCreators.push({ value: org._id, label: org.name });
    });
  }

  if (currentUser?.id) {
    formatedCreators = [
      { label: creatorLabel, value: creatorValue },
      {
        label: currentUser?.name,
        value: currentUser.id
      },
      ...formatedCreators
    ];
  }

  const onChange = e => {
    setErenaEvent({
      ...erenaEvent,
      [e.target.name]: e.target.value
    });
  };

  const onEditorStateChange = (editorState, type) => {
    setErenaEvent({
      ...erenaEvent,
      [type]: editorState
    });
  };

  const onSubmit = event => {
    event.preventDefault();

    dispatch(
      updateTournament(tournament, {
        owner: erenaEvent.owner,
        ownerType: getOwnerType(erenaEvent.owner),
        tournamentName: erenaEvent.tournamentName,
        tournamentDescription: erenaEvent.tournamentDescription,
        slug: erenaEvent.slug,
        rulesMarkdown: erenaEvent.rulesMarkdown,
        twitchChannel: erenaEvent.twitchChannel
      })
    );
  };

  return (
    <Form noValidate validated={false} role="form" onSubmit={onSubmit}>
      <InputRow>
        <Col sm={12}>
          <div className={Style.labelContainer}>
            <InputLabel theme="darkColor">
              Owner
              <span className={Style.requiredAsterisk}>*</span>
            </InputLabel>
            <HelpToolTip text="Select the name of the owner for this tournament" />
          </div>
          <SelectBox
            disabled={false}
            name="owner"
            validated={validated}
            options={formatedCreators}
            size="md"
            value={erenaEvent.owner}
            theme="whiteShadow"
            required
            onChange={e => onChange(e)}
            errorMessage="Owner is required."
          />
        </Col>
      </InputRow>
      <InputRow>
        <Col sm={12}>
          <div className={Style.labelContainer}>
            <InputLabel theme="darkColor">
              Event Name
              <span className={Style.requiredAsterisk}>*</span>
            </InputLabel>
            <HelpToolTip text="Enter the name of event" />
          </div>
          <InputField
            disabled={false}
            name="tournamentName"
            size="lg"
            theme="whiteShadow"
            onChange={e => onChange(e)}
            required
            placeholder="Add an event name"
            value={erenaEvent.tournamentName}
            maxLength={70}
          />
        </Col>
      </InputRow>
      <InputRow>
        <Col sm={12}>
          <div className={Style.labelContainer}>
            <InputLabel theme="darkColor">
              Description
              <span className={Style.requiredAsterisk}>*</span>
            </InputLabel>
            <HelpToolTip text="Enter details about the tournament" />
          </div>
          <EFHtmlInput
            value={erenaEvent.tournamentDescription || ""}
            onChange={e => onEditorStateChange(e, "tournamentDescription")}
          />
        </Col>
      </InputRow>
      <InputRow>
        <Col sm={12}>
          <div className={Style.labelContainer}>
            <InputLabel theme="darkColor">Rules</InputLabel>
            <HelpToolTip text="Enter the rules for the tournament" />
          </div>
          <EFHtmlInput value={erenaEvent.rulesMarkdown || ""} onChange={e => onEditorStateChange(e, "rulesMarkdown")} />
        </Col>
      </InputRow>
      <InputRow>
        <Col sm={12}>
          <div className={Style.labelContainer}>
            {/* [PAVEL] THIS YOUTUBE FUNCTIONALITY IS TEMPORARY AND HACKY */}
            <InputLabel theme="darkColor">Twitch Channel / Youtube Live Video URL</InputLabel>
            <HelpToolTip text="If Twitch, this should only be the channel name, not the URL. Youtube should be the full live video URL." />
          </div>
          <InputField
            disabled={false}
            name="twitchChannel"
            size="lg"
            theme="whiteShadow"
            onChange={onChange}
            required
            placeholder="Add a Twitch channel"
            value={erenaEvent.twitchChannel}
          />
        </Col>
      </InputRow>
      <div className={Style.buttonContainer}>
        <EFRectangleButton buttonType="submit" colorTheme="primary" text="Submit" />
      </div>
    </Form>
  );
};
export default UpdateTournament;
