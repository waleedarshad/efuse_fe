import React, { useEffect } from "react";
import { withRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
import EFSubHeader from "../../Layouts/Internal/EFSubHeader/EFSubHeader";
import SettingsContainer from "../../Settings/Recruiting/SettingsContainer/SettingsContainer";
import Internal from "../../Layouts/Internal/Internal";
import UpdateTournament from "./UpdateTournament/UpdateTournament";
import { erenaNavigationList } from "../../../navigation/erena";
import { getPostableOrganizations } from "../../../store/actions/userActions";
import { setTournamentSSR } from "../../../store/actions/erenaActions";
import InternalNavigation from "../InternalNavigation/InternalNavigation";
import TournamentDesign from "../ErenaDetails/TournamentDesign/TournamentDesign";
import Style from "./TournamentSettings.module.scss";
import { withCdn } from "../../../common/utils";
import SelectTournamentOpportunity from "../SelectTournamentOpportunity/SelectTournamentOpportunity";

const TournamentSetting = ({ pageProps, router }) => {
  const dispatch = useDispatch();
  const tournament = useSelector(state => state.erena.tournament);
  const currentUser = useSelector(state => state.auth.currentUser);
  const currentUserOrgs = useSelector(state => state.user.postableOrganizations);

  useEffect(() => {
    dispatch(setTournamentSSR(pageProps.event));
    dispatch(getPostableOrganizations(1));
  }, [pageProps.event]);

  let actionButtons = [];

  let isOwner = false;

  const orgOwner = currentUserOrgs.find(org => {
    return org?.id === tournament?.owner;
  });

  if (tournament) {
    // Check if current user is owner/admin of tournament
    if (currentUser?.id === tournament?.owner || orgOwner) {
      isOwner = true;
    }
    //set action buttons for nav
    actionButtons = InternalNavigation(tournament);
  }

  return (
    <Internal isAuthenticated containsSubheader>
      <EFSubHeader
        headerImage={withCdn("/static/images/eRenaLogo.png")}
        navigationList={
          isOwner && erenaNavigationList(router?.query, router?.pathname, tournament?.bracketType === "bracket")
        }
        actionButtons={actionButtons}
      />
      <div className={Style.tournamentSettingsWrapper}>
        <SelectTournamentOpportunity tournament={tournament} ownerId={tournament?.owner} />

        <SettingsContainer title="Tournament Details">
          <UpdateTournament tournament={tournament} currentUserOrgs={currentUserOrgs} currentUser={currentUser} />
        </SettingsContainer>
        <SettingsContainer title="Tournament Design">
          <TournamentDesign />
        </SettingsContainer>
      </div>
    </Internal>
  );
};

export default withRouter(TournamentSetting);
