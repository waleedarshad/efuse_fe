const AdImages = tournament => {
  const ads = [];
  tournament?.imageUrl1 ? ads.push({ image: tournament.imageUrl1, url: tournament?.imageLink1 }) : ads.push({});
  tournament?.imageUrl2 ? ads.push({ image: tournament.imageUrl2, url: tournament?.imageLink2 }) : ads.push({});
  tournament?.imageUrl3 ? ads.push({ image: tournament.imageUrl3, url: tournament?.imageLink3 }) : ads.push({});
  tournament?.imageUrl4 ? ads.push({ image: tournament.imageUrl4, url: tournament?.imageLink4 }) : ads.push({});
  tournament?.imageUrl5 ? ads.push({ image: tournament.imageUrl5, url: tournament?.imageLink5 }) : ads.push({});
  tournament?.imageUrl6 ? ads.push({ image: tournament.imageUrl6, url: tournament?.imageLink6 }) : ads.push({});
  return ads;
};

export default AdImages;
