import React, { Component } from "react";
import { withRouter } from "next/router";
import { connect } from "react-redux";
import Internal from "../../Layouts/Internal/Internal";
import Style from "./TournamentManagement.module.scss";
import CreateTeams from "./CreateTeams/CreateTeams";
import TeamScoring from "./TeamScoring/TeamScoring";
import { setTournamentSSR } from "../../../store/actions/erenaActions";
import EmptyComponent from "../../EmptyComponent/EmptyComponent";
import { getPostableOrganizations } from "../../../store/actions/userActions";
import Bracket from "../../Bracket/Bracket";
import InternalNavigation from "../InternalNavigation/InternalNavigation";
import { erenaNavigationList } from "../../../navigation/erena";
import EFSubHeader from "../../Layouts/Internal/EFSubHeader/EFSubHeader";
import { withCdn } from "../../../common/utils";

class TournamentManagement extends Component {
  componentDidMount() {
    analytics.page("eRena Manage Teams");
    const { event } = this.props.pageProps;
    if (event?._id) {
      this.props.setTournamentSSR(event);
    }
    // check if current user
    this.props.getPostableOrganizations(1);
  }

  render() {
    const { router, tournament, currentUser, currentUserOrgs } = this.props;
    const scoringParameter = router.query.scoring;
    const bracketParameter = router.query.bracket;
    const teamsPage = router.query.teams;

    let isOwner = false;
    let actionButtons;

    const orgOwner = currentUserOrgs.find(org => {
      return org?.id === tournament?.owner;
    });

    if (tournament) {
      // Check if current user is owner/admin of tournament
      if (currentUser?.id === tournament?.owner || orgOwner) {
        isOwner = true;
      }
      //set action buttons for nav
      actionButtons = InternalNavigation(tournament);
    }

    // white_label or efuse
    return (
      <Internal metaTitle="eFuse | eRena Team Management" containsSubheader>
        <EFSubHeader
          headerImage={withCdn("/static/images/eRenaLogo.png")}
          navigationList={
            isOwner && erenaNavigationList(router?.query, router?.pathname, tournament?.bracketType === "bracket")
          }
          actionButtons={actionButtons}
        />
        {isOwner ? (
          <>
            {scoringParameter && (
              <div className={Style.scoringContainer}>
                <TeamScoring tournament={tournament} />
              </div>
            )}
            {bracketParameter && (
              <Bracket
                bracket={tournament?.bracket}
                editMatch
                tournamentId={tournament?.tournamentId}
                primaryColor={tournament?.primaryColor}
                secondaryColor={tournament?.secondaryColor}
                totalTeams={tournament?.teamStats}
              />
            )}
            {teamsPage && (
              <div className={Style.teamsContainer}>
                <CreateTeams isBracket={tournament?.bracketType === "bracket"} />
              </div>
            )}
          </>
        ) : (
          <EmptyComponent text="You do not have authority to edit or view these teams." />
        )}
      </Internal>
    );
  }
}

const mapStateToProps = state => ({
  currentUser: state.auth.currentUser,
  tournament: state.erena.tournament,
  currentUserOrgs: state.user.postableOrganizations
});

export default connect(mapStateToProps, {
  setTournamentSSR,
  getPostableOrganizations
})(withRouter(TournamentManagement));
