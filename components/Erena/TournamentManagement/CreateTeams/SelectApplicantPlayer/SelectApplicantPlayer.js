import React, { useState } from "react";
import { Form } from "react-bootstrap";
import Select from "react-select";
import Style from "./SelectApplicantPlayer.module.scss";
import EFRectangleButton from "../../../../Buttons/EFRectangleButton/EFRectangleButton";

const SelectApplicantPlayer = ({ applicants, submitFunction }) => {
  const submit = e => {
    e.preventDefault();
    submitFunction(selectedApplicants);
  };

  const applicantsList = applicants?.map(applicant => {
    return {
      label: `@${applicant?.username} (${applicant?.name})`,
      value: applicant?._id,
      ...applicant
    };
  });

  const [selectedApplicants, setApplicant] = useState([]);

  const chooseApplicant = e => {
    setApplicant(e.map(value => value));
  };

  return (
    <Form onSubmit={submit} noValidate>
      <Select value={selectedApplicants} onChange={chooseApplicant} options={applicantsList} isMulti />
      <div className={Style.buttonWrapper}>
        <EFRectangleButton colorTheme="primary" text="Submit" buttonType="submit" disabled={false} />
      </div>
    </Form>
  );
};

export default SelectApplicantPlayer;
