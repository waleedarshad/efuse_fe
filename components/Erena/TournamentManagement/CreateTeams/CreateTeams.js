import React, { Component } from "react";
import moment from "moment";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus } from "@fortawesome/pro-solid-svg-icons";
import { connect } from "react-redux";
import Style from "./CreateTeams.module.scss";
import InputRow from "../../../InputRow/InputRow";
import {
  createTeam,
  deleteTeam,
  createPlayer,
  deletePlayer,
  updateTeam,
  updatePlayer
} from "../../../../store/actions/erenaActions";
import EmptyComponent from "../../../EmptyComponent/EmptyComponent";
import CreateTeamItem from "./CreateTeamItem/CreateTeamItem";
import { getApplicants } from "../../../../store/actions/applicantActions";
import PageTitleDetails from "../../PageTitleDetails/PageTitleDetails";

class CreateTeams extends Component {
  state = {
    validated: false,
    statsLoaded: false,
    opportunityLoaded: false,
    teams: [],
    selectedOpportunty: ""
  };

  componentDidMount() {
    this.setStats();
  }

  componentDidUpdate() {
    this.setStats();
  }

  static getDerivedStateFromProps(props, state) {
    if (props?.tournament?.teamStats && props?.tournament?.teamStats?.length !== state?.teams?.length) {
      state.teams = props?.tournament?.teamStats;
    }
    return state;
  }

  setStats = () => {
    const { statsLoaded, opportunityLoaded } = this.state;
    const { tournament } = this.props;

    if (tournament?.opportunity?._id && !opportunityLoaded) {
      this.props.getApplicants(tournament?.opportunity?._id, 1, true, {}, "accepted");
      this.setState({
        opportunityLoaded: true
      });
    }

    // check if stats are loaded, if tournament exists and if teams exist in state
    if (!statsLoaded && tournament?.tournamentId) {
      this.setState({
        statsLoaded: true,
        teams: tournament?.teamStats
      });
    }
  };

  findTeam = teamId => {
    const { teams } = this.state;
    return teams.find(x => x._id === teamId);
  };

  findTeamIndex = teamId => {
    const { teams } = this.state;
    return teams.findIndex(x => x._id === teamId);
  };

  findPlayerIndex = (teamIndex, playerId) => {
    const { teams } = this.state;
    const players = teams[teamIndex]?.players;
    return players.findIndex(x => x._id === playerId);
  };

  onTeamChange = (event, teamId) => {
    const { teams } = this.state;
    let team = this.findTeam(teamId);
    let teamIndex = this.findTeamIndex(teamId);
    team = {
      ...team,
      name: event.target.value
    };
    teams[teamIndex] = team;
    this.setState({ teams });
  };

  onTeamBlur = (event, teamId) => {
    const { tournament } = this.props;
    const data = {
      name: event.target.value,
      type: "EFUSE"
    };
    this.props.updateTeam(tournament?.tournamentId, teamId, data);
  };

  removeTeam = teamId => {
    const { tournament } = this.props;
    const teamIndex = this.findTeamIndex(teamId);
    this.props.deleteTeam(tournament, teamId, teamIndex);
  };

  addTeam = () => {
    const { tournament } = this.props;
    this.props.createTeam(tournament);
  };

  addPlayer = teamId => {
    const { tournament } = this.props;
    const teamIndex = this.findTeamIndex(teamId);
    let data = {
      name: "Player Name",
      type: "EFUSE"
    };
    this.props.createPlayer(tournament, teamId, teamIndex, data);
  };

  addEfusePlayer = (teamId, user) => {
    const { tournament } = this.props;
    const teamIndex = this.findTeamIndex(teamId);
    let data = {
      name: user?.username,
      type: "EFUSE",
      userId: user?.value
    };
    this.props.createPlayer(tournament, teamId, teamIndex, data);
  };

  removePlayer = (teamId, playerId) => {
    const { tournament } = this.props;
    const teamIndex = this.findTeamIndex(teamId);
    const playerIndex = this.findPlayerIndex(teamIndex, playerId);
    this.props.deletePlayer(tournament, teamId, playerId, playerIndex, teamIndex);
  };

  onPlayerChange = (event, teamId, playerId) => {
    const { teams } = this.state;
    const team = this.findTeam(teamId);
    const teamIndex = this.findTeamIndex(teamId);
    const playerIndex = this.findPlayerIndex(teamIndex, playerId);

    let player = team.players[playerIndex];
    player = {
      ...player,
      name: event.target.value
    };

    teams[teamIndex].players[playerIndex] = player;
    this.setState({ teams });
  };

  onPlayerBlur = (event, teamId, playerId) => {
    const { tournament } = this.props;
    const data = {
      name: event.target.value,
      type: "EFUSE",
      status: "Active"
    };
    this.props.updatePlayer(tournament, teamId, playerId, data);
  };

  render() {
    const { isBracket, applicants } = this.props;
    const { teams } = this.state;

    //sort the teams by date created so they aren't updated when scores are changed
    const teamsSorted = teams?.slice()?.sort((a, b) => {
      return moment(a.createdAt) - moment(b.createdAt);
    });

    return (
      <>
        <PageTitleDetails
          title="Build Your Teams and Players"
          tooltipText="There are three options to add new players seen below."
        />

        <div className={Style.headerContainer}>
          {!isBracket && (
            <div className={Style.addTeam} onClick={this.addTeam}>
              <FontAwesomeIcon className={Style.plus} icon={faPlus} />
              Add Team
            </div>
          )}
        </div>
        {teamsSorted?.length > 0 ? (
          <InputRow>
            {teamsSorted.map((team, teamIndex) => {
              return (
                <CreateTeamItem
                  teamIndex={teamIndex}
                  team={team}
                  applicants={applicants}
                  onTeamChange={this.onTeamChange}
                  onTeamBlur={this.onTeamBlur}
                  removeTeam={this.removeTeam}
                  addPlayer={this.addPlayer}
                  onPlayerChange={this.onPlayerChange}
                  onPlayerBlur={this.onPlayerBlur}
                  removePlayer={this.removePlayer}
                  isBracket={isBracket}
                  addEfusePlayer={this.addEfusePlayer}
                  key={teamIndex}
                />
              );
            })}
          </InputRow>
        ) : (
          <>
            <br />
            <EmptyComponent text="You have not created any teams. Click the button above to begin adding teams." />
          </>
        )}
      </>
    );
  }
}

const mapStateToProps = state => ({
  tournament: state.erena.tournament,
  applicants: state.applicants.applicants,
  pagination: state.applicants.pagination
});

export default connect(mapStateToProps, {
  createTeam,
  deleteTeam,
  createPlayer,
  deletePlayer,
  updateTeam,
  updatePlayer,
  getApplicants
})(CreateTeams);
