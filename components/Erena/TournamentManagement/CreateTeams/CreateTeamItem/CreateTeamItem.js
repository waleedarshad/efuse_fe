import React from "react";
import { Col } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrash } from "@fortawesome/pro-solid-svg-icons";
import { faTwitch } from "@fortawesome/free-brands-svg-icons";
import AddPlayerButtons from "./AddPlayerButtons/AddPlayerButtons";
import Style from "./CreateTeamItem.module.scss";
import ConfirmAlert from "../../../../ConfirmAlert/ConfirmAlert";
import { withCdn } from "../../../../../common/utils";

const CreateTeamItem = ({
  team,
  teamIndex,
  onTeamChange,
  onTeamBlur,
  removeTeam,
  addPlayer,
  onPlayerBlur,
  onPlayerChange,
  removePlayer,
  isBracket,
  addEfusePlayer,
  applicants
}) => {
  return (
    <Col sm={6} className={Style.teamInput} key={teamIndex}>
      <div className={Style.teamCard}>
        <div className={Style.inputHeader}>
          <div className={Style.fullWidth}>
            <p className={Style.teamCardTitle}>Team</p>
            <input
              className={Style.teamNameInputField}
              value={team?.name}
              maxLength={50}
              onChange={event => onTeamChange(event, team?._id)}
              onBlur={event => onTeamBlur(event, team?._id)}
            />
          </div>
          {!isBracket && (
            <ConfirmAlert
              title="Delete this Team?"
              message="Are you sure?"
              onYes={() => removeTeam(team?._id, teamIndex)}
            >
              <FontAwesomeIcon icon={faTrash} className={`${Style.deleteIcon} ${Style.postitionAbsolute}`} />
            </ConfirmAlert>
          )}
        </div>
        <div className={Style.playerContainer}>
          <div className={Style.playerTitleWrapper}>
            <p className={Style.teamCardTitle}>Players</p>
            <AddPlayerButtons
              addPlayer={addPlayer}
              applicants={applicants}
              addEfusePlayer={addEfusePlayer}
              team={team}
            />
          </div>

          {team?.players &&
            team?.players.map((player, playerIndex) => {
              return (
                <div key={playerIndex}>
                  <div className={Style.inputPlayerHeader}>
                    <input
                      className={Style.playersInputField}
                      value={player?.name}
                      maxLength={50}
                      onChange={event => onPlayerChange(event, team?._id, player?._id)}
                      onBlur={event => onPlayerBlur(event, team?._id, player?._id)}
                    />
                    <div className={Style.alignItems}>
                      {player?.user && (
                        <a target="_blank" href={`/u/${player?.user?.username}`}>
                          <img src={withCdn("/static/images/eFuse_Secondary_Logo.svg")} className={Style.efuseLogo} />
                        </a>
                      )}
                      {player?.user?.twitchUserName && (
                        <a target="_blank" href={`/stream/${player.user.twitchUserName}`}>
                          <FontAwesomeIcon className={Style.twitchIcon} icon={faTwitch} />
                        </a>
                      )}
                      <ConfirmAlert
                        title="Delete this Player?"
                        message="Are you sure?"
                        onYes={() => removePlayer(team?._id, player?._id)}
                      >
                        <FontAwesomeIcon icon={faTrash} className={Style.deleteIcon} />
                      </ConfirmAlert>
                    </div>
                  </div>
                </div>
              );
            })}
        </div>
      </div>
    </Col>
  );
};

export default CreateTeamItem;
