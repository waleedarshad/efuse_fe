import React, { useState, useEffect } from "react";
import moment from "moment";
import { connect, useDispatch } from "react-redux";
import Style from "./TeamScoring.module.scss";
import EmptyComponent from "../../../EmptyComponent/EmptyComponent";
import { updatePlayer } from "../../../../store/actions/erenaActions";

const TeamScoring = ({ tournament }) => {
  const dispatch = useDispatch();
  const [teams, setTeams] = useState([]);

  useEffect(() => {
    //sort the teams by date created so they aren't updated when scores are changed
    const teamsSorted = tournament?.teamStats.slice().sort((a, b) => {
      return moment(a.createdAt) - moment(b.createdAt);
    });

    setTeams(teamsSorted);
  }, [tournament.teamStats?.length]);

  const setPlayers = (event, teamIndex, playerIndex) => {
    const teamsArray = [...teams];
    teamsArray[teamIndex].players[playerIndex].stats.value = event.target.value;
    setTeams(teamsArray);
  };

  const onBlur = (event, player, teamId) => {
    const data = {
      ...player,
      total_kills: event.target.value,
      status: "Active"
    };
    dispatch(updatePlayer(tournament, teamId, player?._id, data));
  };

  if (teams?.length === 0) {
    return <EmptyComponent text="You have not created any teams. Navigate to the Teams tab to begin adding teams." />;
  }
  return (
    <div className={Style.teamsWrapper}>
      {teams &&
        teams.map((team, teamIndex) => {
          return (
            <div className={Style.teamContainer} key={team?._id}>
              <div className={Style.teamNameWrapper}>
                <p className={Style.teamName}>{team?.name}</p>
                {/* <p className={Style.teamScore}>
                  Total Score: <b>{team?.stats?.value}</b>
                </p> */}
              </div>
              <div className={Style.playersWrapper}>
                {team?.players?.map((player, playerIndex) => {
                  return (
                    <div className={Style.playerContainer} key={player?._id}>
                      <p className={Style.playerName}>{player?.name}</p>
                      <input
                        type="number"
                        className={Style.playerInputField}
                        value={player?.stats?.value}
                        onChange={event => setPlayers(event, teamIndex, playerIndex)}
                        onBlur={event => onBlur(event, player, team?._id)}
                      />
                    </div>
                  );
                })}
              </div>
            </div>
          );
        })}
    </div>
  );
};

export default connect(null, {
  updatePlayer
})(TeamScoring);
