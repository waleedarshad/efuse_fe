import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { updateTournament } from "../../../store/actions/erenaActions";
import { getAdminOpportunities } from "../../../store/actions/opportunityActions";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";
import EFCard from "../../Cards/EFCard/EFCard";
import OpportunitySmallListItem from "../../Opportunities/OpportunityWrapper/OpportunitySmallListing/OpportunitySmallListItem/OpportunitySmallListItem";
import SelectBox from "../../SelectBox/SelectBox";
import PageTitleDetails from "../PageTitleDetails/PageTitleDetails";
import Style from "./SelectTournamentOpportunity.module.scss";

const SelectTournamentOpportunity = ({ tournament, ownerId }) => {
  const dispatch = useDispatch();
  const opportunities = useSelector(state => state.opportunities.opportunities);

  useEffect(() => {
    dispatch(getAdminOpportunities(1, { opportunityType: "Event" }, ownerId));
  }, [tournament?.opportunity?._id, opportunities?.length]);

  const onChange = e => {
    dispatch(updateTournament(tournament, { opportunity: e.target.value }));
  };

  let opportunityList = opportunities?.map(opp => {
    return { label: `${opp?.title} | ${opp?.opportunityType}`, value: opp?._id };
  });

  opportunityList = [{ label: "Select Opportunity", value: "" }, ...opportunityList];

  return (
    <>
      <PageTitleDetails
        title="Select an Opportunity"
        tooltipText="Selecting an opportunity will provide you the ability to choose players from the list of opportunity applicants."
        selectOpportuntyTitle
      />
      <div className={`${Style.selectOpportunityWrapper} ${!tournament?.opportunity?._id && Style.addMarginBottom}`}>
        <div className={Style.selectBoxWrapper}>
          <SelectBox
            name="selectedOpportunty"
            options={opportunityList}
            size="md"
            value={tournament?.opportunity?._id || ""}
            theme="whiteShadow"
            onChange={onChange}
          />
        </div>
        <div className={Style.createOpportunityWrapper}>
          <EFRectangleButton
            text="Create Opportunity"
            colorTheme="primary"
            size="medium"
            shadowTheme="small"
            internalHref="/opportunities/create"
          />
        </div>
      </div>
      {tournament?.opportunity?._id && (
        <div className={Style.selectedCardWrapper}>
          <EFCard widthTheme="fullWidth" overflowTheme="visible">
            <OpportunitySmallListItem key={tournament?.opportunity?._id} opportunity={tournament?.opportunity} />
          </EFCard>
        </div>
      )}
    </>
  );
};

export default SelectTournamentOpportunity;
