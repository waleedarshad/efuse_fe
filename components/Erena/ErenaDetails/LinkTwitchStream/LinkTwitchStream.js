import React, { Component } from "react";
import { connect } from "react-redux";
import { Form, Col } from "react-bootstrap";
import isEmpty from "lodash/isEmpty";
import InputRow from "../../../InputRow/InputRow";
import InputField from "../../../InputField/InputField";
import InputLabel from "../../../InputLabel/InputLabel";
import { updateTournament } from "../../../../store/actions/erenaActions";
import Style from "./LinkTwitchStream.module.scss";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";
class LinkTwitchStream extends Component {
  state = {
    validated: false,
    twitchChannel: ""
  };

  onChange = event => {
    this.setState({
      ...this.state,
      [event.target.name]: event.target.value
    });
  };

  onSubmit = event => {
    const { updateTournament, tournament, closeModal } = this.props;
    event.preventDefault();
    const { validated, twitchChannel } = this.state;
    if (!validated) {
      this.setState({ validated: true });
    }
    if (event.currentTarget.checkValidity()) {
      analytics.track("ERENA_MANAGE_STREAM_LINKED", {
        twitchChannel
      });
      updateTournament(tournament, { twitchChannel });
      closeModal();
    }
  };

  render() {
    const { validated, twitchChannel } = this.state;

    let nextButtonInactive = false;
    if (twitchChannel == "" || isEmpty(twitchChannel)) {
      nextButtonInactive = true;
    }
    return (
      <Form noValidate validated={validated} onSubmit={this.onSubmit}>
        <InputRow>
          <Col sm={12}>
            <InputLabel theme="internal">Twitch Channel</InputLabel>
            <InputField
              name="twitchChannel"
              placeholder=""
              theme="internal"
              size="lg"
              onChange={this.onChange}
              value={twitchChannel}
              maxLength={250}
            />
          </Col>
        </InputRow>
        <div className={Style.buttonContainer}>
          <EFRectangleButton buttonType="submit" disabled={nextButtonInactive} colorTheme="primary" text="Submit" />
        </div>
      </Form>
    );
  }
}

const mapStateToProps = state => ({
  tournament: state.erena.tournament
});

export default connect(mapStateToProps, { updateTournament })(LinkTwitchStream);
