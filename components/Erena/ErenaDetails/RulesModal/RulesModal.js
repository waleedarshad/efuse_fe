import React from "react";
import Markdown from "markdown-to-jsx";
import Style from "./RulesModal.module.scss";

const RulesModal = ({ rules }) => {
  return (
    <div className={Style.rulesContainer}>
      <Markdown>{rules}</Markdown>
    </div>
  );
};

export default RulesModal;
