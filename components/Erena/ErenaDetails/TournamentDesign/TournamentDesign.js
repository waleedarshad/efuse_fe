import React, { Component } from "react";
import { connect } from "react-redux";
import { Form, Col } from "react-bootstrap";
import InputColor from "react-input-color";
import InputRow from "../../../InputRow/InputRow";
import InputLabel from "../../../InputLabel/InputLabel";
import Style from "./TournamentDesign.module.scss";
import { rescueNil } from "../../../../helpers/GeneralHelper";
import { updateTournament } from "../../../../store/actions/erenaActions";
import ImageUploader from "../../../ImageUploader/ImageUploader";
import { toggleCropModal } from "../../../../store/actions/settingsActions";
import Switch from "../../../Switch/Switch";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";

class TournamentDesign extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hasRendered: false,
      validated: false,
      primaryColor: "#2e87ff",
      secondaryColor: "#0f0f0f",
      brandLogoUrl: null,
      backgroundImageUrl: null,
      hideLeaderboard: false,
      hideButtonRendered: false
    };
  }

  static getDerivedStateFromProps(props, state) {
    if (props?.tournament?.brandLogoUrl && props?.tournament?.brandLogoUrl !== state?.brandLogoUrl) {
      state.brandLogoUrl = { url: props.tournament.brandLogoUrl };
    }
    if (props?.tournament?.backgroundImageUrl && props?.tournament?.backgroundImageUrl !== state?.backgroundImageUrl) {
      state.backgroundImageUrl = { url: props.tournament.backgroundImageUrl };
    }
    if (
      props?.tournament?.hideLeaderboard &&
      !state.hideButtonRendered &&
      props?.tournament?.hideLeaderboard !== state?.hideLeaderboard
    ) {
      state.hideLeaderboard = props.tournament.hideLeaderboard;
      state.hideButtonRendered = true;
    }
    return state;
  }

  onChange = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  onSubmit = event => {
    event.preventDefault();
    const { updateTournament, tournament } = this.props;
    const { validated, primaryColor, secondaryColor, brandLogoUrl, backgroundImageUrl, hideLeaderboard } = this.state;
    if (!validated) {
      this.setState({ validated: true });
    }
    if (event.currentTarget.checkValidity()) {
      analytics.track("ERENA_MANAGE_TOURNAMENT_DESIGN");
      const updateData = new FormData();
      updateData.append("primaryColor", primaryColor.hex || primaryColor);
      updateData.append("secondaryColor", secondaryColor.hex || secondaryColor);
      updateData.append("hideLeaderboard", hideLeaderboard);

      if (brandLogoUrl?.path) {
        updateData.append("brandLogoUrl", brandLogoUrl);
      }

      if (backgroundImageUrl?.name) {
        updateData.append("backgroundImageUrl", backgroundImageUrl);
      }

      updateTournament(tournament, updateData);
    }
  };

  onDrop = (selectedFile, name) => {
    this.setState({
      [name]: selectedFile[0]
    });
  };

  setPrimaryColor = primaryColor => {
    this.setState({
      primaryColor
    });
  };

  setSecondaryColor = secondaryColor => {
    this.setState({
      secondaryColor
    });
  };

  onCropChange = (croppedImage, name) => {
    const { blob, preview } = croppedImage;
    const croppedFile = new File([blob], blob.name, {
      type: blob.type,
      preview
    });
    this.setState({
      [name]: croppedFile
    });
  };

  switchToggle = () => {
    const { hideLeaderboard } = this.state;
    this.setState({
      hideLeaderboard: !hideLeaderboard
    });
  };

  render() {
    const { tournament } = this.props;
    const { validated, brandLogoUrl, backgroundImageUrl, hideLeaderboard } = this.state;
    const nextButtonInactive = false;

    return (
      <Form noValidate validated={validated} onSubmit={this.onSubmit}>
        <InputRow>
          <Col sm={12}>
            <div className={Style.flexWrapper}>
              <InputLabel theme="internal">
                Hide {tournament?.bracketType === "bracket" ? "Bracket" : "Leaderboard"}
              </InputLabel>
              <div className={Style.switchWrapper}>
                <Switch
                  name="hideLeaderboard"
                  value={hideLeaderboard}
                  checked={hideLeaderboard}
                  onChange={this.switchToggle}
                />
              </div>
            </div>
          </Col>
        </InputRow>
        <InputRow>
          <Col sm={6}>
            <InputLabel theme="internal">Primary Color</InputLabel>
            <br />
            <InputColor
              initialValue={tournament?.primaryColor ? tournament.primaryColor : "#2e87ff"}
              onChange={color => this.setPrimaryColor(color)}
              placement="right"
              className={Style.inputColor}
            />
          </Col>
          <Col sm={6}>
            <InputLabel theme="internal">Secondary Color</InputLabel>
            <br />
            <InputColor
              initialValue={tournament?.secondaryColor ? tournament.secondaryColor : "#2163b9"}
              onChange={color => this.setSecondaryColor(color)}
              placement="right"
              className={Style.inputColor}
            />
          </Col>
        </InputRow>
        <InputRow>
          <Col sm={12}>
            <InputLabel theme="internal">Logo Image</InputLabel>
            <br />
            <ImageUploader
              text="Add Tournament Logo"
              name="brandLogoUrl"
              onDrop={this.onDrop}
              theme="lightButton"
              value={rescueNil(brandLogoUrl, "url")}
              showCropper={false}
            />
          </Col>
        </InputRow>
        <InputRow>
          <Col sm={12}>
            <InputLabel theme="internal">Background Image</InputLabel>
            <br />
            <ImageUploader
              text="Add Background Image"
              name="backgroundImageUrl"
              onDrop={this.onDrop}
              value={rescueNil(backgroundImageUrl, "url")}
              theme="lightButton"
              showCropper
              aspectRatioWidth={1920}
              aspectRatioHeight={1080}
              onCropChange={this.onCropChange}
              toggleCropperModal={this.props.toggleCropModal}
              showImageDescription
            />
          </Col>
        </InputRow>
        <div className={Style.buttonContainer}>
          <EFRectangleButton buttonType="submit" disabled={nextButtonInactive} colorTheme="primary" text="Submit" />
        </div>
      </Form>
    );
  }
}

const mapStateToProps = state => ({
  tournament: state.erena.tournament
});

export default connect(mapStateToProps, { updateTournament, toggleCropModal })(TournamentDesign);
