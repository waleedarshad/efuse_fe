import React, { Component } from "react";
import { connect } from "react-redux";
import { Form, Col } from "react-bootstrap";
import { faTrash } from "@fortawesome/pro-solid-svg-icons";

import InputRow from "../../../InputRow/InputRow";
import InputField from "../../../InputField/InputField";
import InputLabel from "../../../InputLabel/InputLabel";
import { rescueNil } from "../../../../helpers/GeneralHelper";
import { editErenaAd, removeErenaAd } from "../../../../store/actions/erenaActions";
import ImageUploader from "../../../ImageUploader/ImageUploader";
import Style from "./CreateAd.module.scss";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";
import { toggleCropModal } from "../../../../store/actions/settingsActions";

class ManageTournament extends Component {
  state = {
    validated: false,
    adImage: "",
    adLink: ""
  };

  componentDidMount() {
    const { editAdUrl, editAdImage } = this.props;
    if (editAdUrl && editAdImage) {
      this.setState({
        ...this.state,
        adLink: editAdUrl,
        adImage: { url: editAdImage }
      });
    }
  }

  onChange = event => {
    this.setState({
      ...this.state,
      [event.target.name]: event.target.value
    });
  };

  removeAd = event => {
    event.preventDefault();
    const { tournament, adIndex, removeErenaAd } = this.props;
    removeErenaAd(tournament, adIndex + 1);
    analytics.track("ERENA_ADS_DELETE");
    this.props.closeModal && this.props.closeModal();
  };

  onSubmit = event => {
    event.preventDefault();
    const { editErenaAd, tournament, adIndex } = this.props;
    const { validated, adImage, adLink } = this.state;
    if (!validated) {
      this.setState({ validated: true });
    }
    if (event.currentTarget.checkValidity()) {
      const adFormData = new FormData();
      adFormData.append("link", adLink);
      adFormData.append("index", adIndex + 1);

      if (adImage?.name) {
        adFormData.append("image", adImage);
      }
      analytics.track("ERENA_ADS_EDIT", {
        adImage,
        adLink
      });
      this.props.closeModal && this.props.closeModal();
      editErenaAd(tournament, adFormData);
    }
  };

  onDrop = selectedFile => {
    this.setState({
      ...this.state,
      adImage: selectedFile[0]
    });
  };

  onCropChange = croppedImage => {
    const { blob } = croppedImage;
    const croppedFile = new File([blob], blob.name, { type: blob.type });
    this.setState({
      ...this.state,
      adImage: croppedFile
    });
  };

  toggleCropperModal = (val, name) => {
    this.props.toggleCropModal(val, name);
  };

  render() {
    const { editMode } = this.props;
    const { validated, adImage, adLink } = this.state;

    let nextButtonInactive = false;
    if (adLink === "" || adImage === "") {
      nextButtonInactive = true;
    }

    return (
      <Form noValidate validated={validated} onSubmit={this.onSubmit}>
        <InputRow>
          <Col sm={12}>
            <InputLabel theme="darkColor">Reference Link</InputLabel>
            <InputField
              name="adLink"
              placeholder="https://efuse.gg"
              theme="whiteShadow"
              size="lg"
              onChange={this.onChange}
              value={adLink}
              maxLength={250}
            />
          </Col>
        </InputRow>
        <InputRow>
          <Col sm={12}>
            <InputLabel theme="darkColor">Promo Image</InputLabel>
            <ImageUploader
              text="Upload Image"
              name="adImage"
              onDrop={this.onDrop}
              theme="lightButton"
              value={rescueNil(adImage, "url")}
              showCropper={false}
              onCropChange={this.onCropChange}
              toggleCropperModal={this.toggleCropperModal}
              aspectRatioWidth={16}
              aspectRatioHeight={9}
              showCropper
            />
          </Col>
        </InputRow>
        <div className={Style.buttonContainer}>
          {editMode && (
            <EFRectangleButton
              buttonType="submit"
              onClick={e => this.removeAd(e)}
              disabled={nextButtonInactive}
              colorTheme="transparent"
              shadowTheme="none"
              text="Delete"
              icon={faTrash}
            />
          )}
          <EFRectangleButton buttonType="submit" disabled={nextButtonInactive} colorTheme="primary" text="Submit" />
        </div>
      </Form>
    );
  }
}

const mapStateToProps = state => ({
  tournament: state.erena.tournament
});

export default connect(mapStateToProps, { editErenaAd, removeErenaAd, toggleCropModal })(ManageTournament);
