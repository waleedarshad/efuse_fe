import React, { useEffect, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faVideo } from "@fortawesome/pro-solid-svg-icons";

import TwitchVideoPlayerCustomChat from "../../TwitchVideoPlayerCustomChat/TwitchVideoPlayerCustomChat";
import Style from "../Erena.module.scss";
import TournamentLeaderboard from "../../TournamentLeaderboard/TournamentLeaderboard";
import ImageCards from "../ImageCards/ImageCards";
import DynamicModal from "../../DynamicModal/DynamicModal";
import AddItem from "../../AddItem/AddItem";
import LinkTwitchStream from "../ErenaDetails/LinkTwitchStream/LinkTwitchStream";
import FeatureFlag from "../../General/FeatureFlag/FeatureFlag";
import FeatureFlagVariant from "../../General/FeatureFlag/FeatureFlagVariant/FeatureFlagVariant";
import Bracket from "../../Bracket/Bracket";
import MockExternalUser from "../../MockExternalUser/MockExternalUser";
import TwitchMultipleStreams from "../../TwitchMultipleStreams/TwitchMultipleStreams";
import ErenaCallToAction from "../../ErenaV2/ErenaCallToAction/ErenaCallToAction";
import OpportunitySummaryCard from "../../OpportunitiesV2/OpportunityCards/OpportunitySummaryCard/OpportunitySummaryCard";
import VideoPlayer from "../../VideoPlayer/VideoPlayer";

const TournamentEventComponent = ({
  currentUser,
  tournament,
  eventsLogin,
  eventsStreamButton,
  leaderboardRoute,
  isOwner,
  mockExternalErenaEvent,
  eventSSR,
  ads,
  router,
  opportunity,
  exp_web_erena_teep_opportunity
}) => {
  useEffect(() => {
    setStreams(getPlayersStreams());
  }, [tournament?.teamStats]);

  const [streams, setStreams] = useState([]);

  const getPlayersStreams = () => {
    let streams = [];
    tournament?.teamStats?.forEach(team => {
      const playersList = [];
      team?.players?.forEach(player => {
        if (player?.user?.twitchUserName) {
          playersList.push({
            name: player?.name,
            channel: player?.user?.twitchUserName,
            type: "twitch"
          });
        }
      });
      if (playersList?.length > 0) {
        streams.push({ name: team?.name, players: [...playersList] });
      }
    });
    return streams;
  };

  return (
    <>
      <div className={Style.mockExternalUserWrapper}>
        <MockExternalUser currentUser={currentUser} type="erena" erenaEvent owner={isOwner} />
      </div>

      {/* erena background image */}
      {tournament?.backgroundImageUrl ? (
        <img className={Style.backgroundImage} src={tournament.backgroundImageUrl} alt="erena background" />
      ) : tournament.brandLogoUrl ? (
        <img className={Style.backgroundLogo} src={tournament?.brandLogoUrl} alt="erena logo" />
      ) : (
        <></>
      )}

      {eventsLogin && (
        <DynamicModal
          flow="LoginSignup"
          openOnLoad
          startView="login"
          displayCloseButton={false}
          allowBackgroundClickClose={false}
        />
      )}

      {eventsStreamButton && leaderboardRoute && (
        <a href={router.query.e} className={Style.link}>
          <p className={Style.viewStream}>
            View Stream
            <FontAwesomeIcon className={Style.icon} icon={faVideo} />
          </p>
        </a>
      )}

      <div className={Style.container}>
        {/* twitch channel */}
        {!leaderboardRoute && (
          <>
            {tournament?.twitchChannel ? (
              <div className={Style.twitchWrapper}>
                {streams?.length > 0 ? (
                  <TwitchMultipleStreams
                    mainStream={{
                      players: [{ name: tournament.twitchChannel, channel: tournament.twitchChannel, type: "twitch" }]
                    }}
                    streams={streams}
                    streamTitle={tournament.tournamentName}
                    startingChannel={tournament.twitchChannel}
                    startingName={tournament.twitchChannel}
                  />
                ) : (
                  <TwitchVideoPlayerCustomChat channel={tournament.twitchChannel} title={tournament.tournamentName} />
                )}
              </div>
            ) : isOwner && !mockExternalErenaEvent ? (
              <AddItem text="Twitch Channel" modalTitle="Manage Twitch Channel" modalComponent={<LinkTwitchStream />}>
                <div className={Style.twitchWrapper} />
              </AddItem>
            ) : (
              <></>
            )}
          </>
        )}

        {opportunity?._id && exp_web_erena_teep_opportunity && (
          <div className={Style.marginTop}>
            <OpportunitySummaryCard opportunity={opportunity} />
          </div>
        )}

        {!currentUser?.id && (
          <div className={Style.marginTop}>
            <ErenaCallToAction />
          </div>
        )}

        {/* image cards */}
        <div className={Style.marginTop}>
          <ImageCards gridLayout="double" ads={ads} isOwner={isOwner} mockExternalErenaEvent={mockExternalErenaEvent} />
        </div>

        {/* leaderboard or bracket */}
        {!tournament?.hideLeaderboard && (
          <>
            {tournament?.bracketType === "pointRace" ? (
              <div className={Style.leaderboardWrapper}>
                <FeatureFlag name="erena_leaderboard">
                  <FeatureFlagVariant flagState={true}>
                    <TournamentLeaderboard twitchView={false} tournamentId={tournament.tournamentId} />
                  </FeatureFlagVariant>
                </FeatureFlag>
              </div>
            ) : (
              <div className={Style.marginTop}>
                <Bracket
                  bracket={tournament?.bracket}
                  primaryColor={tournament?.primaryColor}
                  secondaryColor={tournament?.secondaryColor}
                />
              </div>
            )}
          </>
        )}

        {/* challonge leaderboard */}
        {eventSSR?.challongeLeaderboard && (
          <div className={Style.borderRadius}>
            <iframe
              title="challonge leaderboard"
              src={eventSSR?.challongeLeaderboard}
              width="100%"
              height="500"
              frameBorder="0"
              scrolling="auto"
              allowtransparency="true"
            />
          </div>
        )}
      </div>
    </>
  );
};

export default TournamentEventComponent;
