import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { ProgressBar } from "react-bootstrap";
import { withRouter } from "next/router";
import { faChevronLeft } from "@fortawesome/pro-solid-svg-icons";
import { getPostableOrganizations } from "../../../store/actions/userActions";
import GeneralErenaTemplate from "./GeneralErenaTemplate/GeneralErenaTemplate";
import Style from "./CreateErenaEventFlow.module.scss";
import EFCircleIconButton from "../../Buttons/EFCircleIconButton/EFCircleIconButton";
import TournamentErenaTemplate from "./TournamentErenaTemplate/TournamentErenaTemplate";
import { createErenaEvent } from "../../../store/actions/erenaActions";
import { getOpportunityRequiredFields } from "../../../store/actions/opportunityActions";
import RequirementsErenaTemplate from "./RequirementsErenaTemplate/RequirementsErenaTemplate";

const CreateErenaEventFlow = ({ router }) => {
  const dispatch = useDispatch();

  const [step, setStep] = useState(0);
  const [erenaTournament, setErenaTournament] = useState();

  const currentUser = useSelector(state => state.auth.currentUser);
  const currentUserOrgs = useSelector(state => state.user.postableOrganizations);
  const requiredFields = useSelector(state => state.opportunities.requiredFormFields);

  const next = () => setStep(current => current + 1);
  const back = () => setStep(current => current - 1);

  const submitPage = data => {
    setErenaTournament(oldData => ({ ...oldData, ...data }));
    next();
  };

  const submitFinalPage = finalPageData => {
    const data = { ...erenaTournament, ...finalPageData };
    data.tournamentId = data.slug;
    data.type = "EFUSE";
    data.ownerType = currentUserOrgs.find(org => org._id === data.owner) ? "organizations" : "users";
    data.requirements = Object.keys(data?.requirements).filter(key => data?.requirements[key]);

    dispatch(createErenaEvent(data, router));
  };

  const erenaCreateViews = {
    GENERAL: {
      heading: "Create eRena Event",
      content: (
        <GeneralErenaTemplate onSubmit={submitPage} currentUser={currentUser} currentUserOrgs={currentUserOrgs} />
      )
    },
    TOURNAMENT: {
      heading: "Add Tournament Details",
      content: <TournamentErenaTemplate onSubmit={submitPage} />
    },
    REQUIREMENTS: {
      heading: "Add Additional Requirements",
      content: <RequirementsErenaTemplate onSubmit={submitFinalPage} requiredFields={requiredFields} />
    }
  };

  const flow = [erenaCreateViews.GENERAL, erenaCreateViews.TOURNAMENT, erenaCreateViews.REQUIREMENTS];
  const progressPercent = (step / flow.length) * 100;

  useEffect(() => {
    dispatch(getPostableOrganizations(1));
    dispatch(getOpportunityRequiredFields());
  }, []);

  return (
    <>
      <div className={Style.erenaTitle}>
        {step !== 0 && <EFCircleIconButton icon={faChevronLeft} size="small" onClick={back} />}
        <span className={Style.modalTitle}>{flow[step]?.heading}</span>
      </div>
      <hr />
      {flow[step]?.content}
      <ProgressBar className={Style.progressBar} now={progressPercent} variant="blue" />
    </>
  );
};

export default withRouter(CreateErenaEventFlow);
