import React from "react";
import { Form, Col } from "react-bootstrap";
import { useForm } from "react-hook-form";
import InputRow from "../../../InputRow/InputRow";
import Style from "../GeneralErenaTemplate/GeneralErenaTemplate.module.scss";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";
import EFRequiredFieldsList from "../../../FormControls/RequiredFieldsList/EFRequiredFieldsList";

const RequirementsErenaTemplate = ({ onSubmit, requiredFields }) => {
  const { control, handleSubmit } = useForm();

  return (
    <Form className={Style.leftAlign}>
      <InputRow>
        <Col sm={12}>
          <EFRequiredFieldsList requiredFields={requiredFields} control={control} name="requirements" />
        </Col>
      </InputRow>

      <div className={Style.alignRight}>
        <EFRectangleButton colorTheme="primary" text="Create eRena Event" onClick={handleSubmit(onSubmit)} />
      </div>
    </Form>
  );
};

export default RequirementsErenaTemplate;
