import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { NextSeo } from "next-seo";
import Internal from "../../Layouts/Internal/Internal";

import EmptyComponent from "../../EmptyComponent/EmptyComponent";
import Modal from "../../Modal/Modal";
import CreateErenaEventFlow from "../CreateErenaEventFlow/CreateErenaEventFlow";

const CreateEventPage = () => {
  const allowCreation = useSelector(state => state.features.allow_erena_creation);

  useEffect(() => {
    analytics.page("Create Event Page");
  }, []);

  return (
    <>
      <NextSeo title="eRena powered by eFuse | Duke it out on a Global Stage" />
      <Internal isAuthenticated noCollapse>
        <Modal
          displayCloseButton={false}
          allowBackgroundClickClose={false}
          openOnLoad={allowCreation}
          component={<CreateErenaEventFlow />}
        />
        {!allowCreation && (
          <EmptyComponent
            style={{ display: "block", width: "100%" }}
            text="You do not have permission to create eRena events."
          />
        )}
      </Internal>
    </>
  );
};

export default CreateEventPage;
