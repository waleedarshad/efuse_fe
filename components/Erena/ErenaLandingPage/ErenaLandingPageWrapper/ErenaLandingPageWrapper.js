import { useDispatch, useSelector } from "react-redux";
import React, { useEffect } from "react";
import { useRouter } from "next/router";
import { useQuery } from "@apollo/client";
import Internal from "../../../Layouts/Internal/Internal";
import PublicInternal from "../../../Layouts/PublicInternal";
import Style from "./ErenaLandingPageWrapper.module.scss";
import { withCdn } from "../../../../common/utils";
import EFSubHeader from "../../../Layouts/Internal/EFSubHeader/EFSubHeader";
import { erenaLandingNavigationList } from "../../../../navigation/erena";
import FEATURE_FLAGS from "../../../../common/featureFlags";
import { getFlagForFeature } from "../../../../store/selectors/featureFlagSelectors";
import GamesList from "../GamesList/GamesList";
import FeatureFlag from "../../../General/FeatureFlag/FeatureFlag";
import FeatureFlagVariant from "../../../General/FeatureFlag/FeatureFlagVariant/FeatureFlagVariant";
import { getScheduledAndActiveErenaTournaments } from "../../../../store/actions/erenaV2/erenaActionsV2";
import EventCardsList from "../EventCardsList/EventCardsList";
import ERenaFeaturedEventSection from "../ERenaFeaturedEventSection/ERenaFeaturedEventSection";

import ERenaFeaturedVideoSection from "../ERenaFeaturedVideoSection/ERenaFeaturedVideoSection";

import GET_ERENA_FEATURED_EVENT from "../../../../graphql/ERenaFeaturedEventQuery";

let page = 1;
const ErenaLandingPageWrapper = ({ pageProps }) => {
  const dispatch = useDispatch();
  const router = useRouter();

  const currentUser = useSelector(state => state.auth.currentUser);
  const showCompete = useSelector(state => getFlagForFeature(state, FEATURE_FLAGS.WEB_ERENA_NAVIGATION_COMPETE));
  const showWatch = useSelector(state => getFlagForFeature(state, FEATURE_FLAGS.WEB_ERENA_NAVIGATION_WATCH));
  const showPipeline = useSelector(state => getFlagForFeature(state, FEATURE_FLAGS.WEB_ERENA_NAVIGATION_PIPELINE));
  const showNews = useSelector(state => getFlagForFeature(state, FEATURE_FLAGS.WEB_ERENA_NAVIGATION_NEWS));
  const showErenaLanding = useSelector(state => state.features.web_erena_in_discover_navigation);
  const tournaments = useSelector(state => state.erenaV2.tournaments);
  const pagination = useSelector(state => state.erenaV2.pagination);

  const userIsLoggedIn = currentUser?.id;
  const doodleBackground = withCdn("/static/images/efusedoodles.png");

  const { loading, error, data } = useQuery(GET_ERENA_FEATURED_EVENT);

  let event = null;

  if (loading || error || !data?.getFeaturedEvent?.twitchChannel) {
    event = null;
  } else {
    event = data?.getFeaturedEvent;
  }

  useEffect(() => {
    dispatch(getScheduledAndActiveErenaTournaments(page));
  }, []);

  const loadMore = () => {
    if (pagination?.hasNextPage) {
      page += 1;
    }
    dispatch(getScheduledAndActiveErenaTournaments(page));
  };

  const mainContent = (
    <>
      <EFSubHeader
        headerImage="https://cdn.efuse.gg/static/images/Erena_Logo.png"
        navigationList={erenaLandingNavigationList(showCompete, showWatch, showPipeline, showNews, router.pathname)}
        customImageStyle={Style.customImageStyle}
      />
      {showErenaLanding && (
        <>
          <div style={{ width: "100%", position: "relative" }} className={Style.notch}>
            <div
              className={`${Style.gradientBackground} ${!event && Style.adjustHeader}`}
              style={{
                backgroundImage: `linear-gradient(to bottom, rgba(255, 255, 255, 0), #ffffff),
      url(${doodleBackground})`
              }}
            >
              <div className={Style.mainContentWrapper}>
                <ERenaFeaturedEventSection event={event} />
              </div>
              <div className={Style.pageIntro}>
                <h3>The Home for Competitive Gaming</h3>
                <p>With eRena, it has never been easier to watch, manage, or compete in an esports tournament.</p>
              </div>
            </div>
            <div className={Style.featuredVideo}>
              <ERenaFeaturedVideoSection />
            </div>
          </div>
          <div className={Style.listFilteringSection}>
            <FeatureFlag name={FEATURE_FLAGS.WEB_ERENA_GAMES_FILTER}>
              <FeatureFlagVariant flagState>
                <div className={Style.gamesWrapper}>
                  <GamesList />
                </div>
              </FeatureFlagVariant>
            </FeatureFlag>
            <EventCardsList events={tournaments} pagination={pagination} loadMore={loadMore} games={pageProps?.games} />
          </div>
        </>
      )}
      {!showErenaLanding && <div className={Style.comingSoon}>Coming Soon!</div>}
    </>
  );

  return userIsLoggedIn ? (
    <Internal metaTitle="eFuse | eRena" maxWidth noPadding containsSubheaderExact>
      {mainContent}
    </Internal>
  ) : (
    <PublicInternal maxWidth noPadding>
      {mainContent}
    </PublicInternal>
  );
};

export default ErenaLandingPageWrapper;
