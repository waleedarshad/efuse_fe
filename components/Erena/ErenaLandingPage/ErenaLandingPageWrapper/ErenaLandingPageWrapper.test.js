import React from "react";
import { MockedProvider } from "@apollo/client/testing";
import { mountWithStore } from "../../../../common/testUtils";
import ErenaLandingPageWrapper from "./ErenaLandingPageWrapper";
import Internal from "../../../Layouts/Internal/Internal";
import PublicInternal from "../../../Layouts/PublicInternal";
import GET_ERENA_FEATURED_EVENT from "../../../../graphql/ERenaFeaturedEventQuery";

jest.mock("../../../../store/actions/erenaV2/erenaActionsV2", () => ({
  getScheduledAndActiveErenaTournaments: () => jest.fn()
}));

describe("ErenaLandingPageWrapper", () => {
  const mocks = [
    {
      request: {
        query: GET_ERENA_FEATURED_EVENT
      },
      result: {
        data: {
          getFeaturedEvent: {}
        }
      }
    }
  ];

  it("displays the correct header if the viewing user is logged in", () => {
    const state = {
      auth: {
        currentUser: {
          id: "12345"
        }
      },
      webview: {
        status: ""
      },
      messages: {
        unreadMessageCount: 0
      },
      errors: {
        errors: "error"
      },
      features: {
        allow_opportunity_creation: true
      },
      erenaV2: {
        tournaments: [],
        pagination: {}
      }
    };

    const { subject } = mountWithStore(
      <MockedProvider mocks={mocks} addTypename={false}>
        <ErenaLandingPageWrapper />
      </MockedProvider>,
      state
    );

    expect(subject.find(Internal).prop("metaTitle")).toEqual("eFuse | eRena");
  });

  it("displays the correct header if the viewing user is not logged in", () => {
    const state = {
      auth: {
        currentUser: {}
      },
      webview: {
        status: ""
      },
      messages: {
        unreadMessageCount: 0
      },
      errors: {
        errors: "error"
      },
      features: {
        allow_opportunity_creation: true
      },
      experiences: {
        experiences: ""
      },
      erenaV2: {
        tournaments: [],
        pagination: {}
      }
    };

    const { subject } = mountWithStore(
      <MockedProvider mocks={mocks} addTypename={false}>
        <ErenaLandingPageWrapper />
      </MockedProvider>,
      state
    );

    expect(subject.find(PublicInternal)).toHaveLength(1);
  });
});
