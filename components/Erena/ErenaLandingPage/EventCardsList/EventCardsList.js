import React from "react";
import InfiniteScroll from "react-infinite-scroller";
import uniqueId from "lodash/uniqueId";
import AnimatedLogo from "../../../AnimatedLogo";
import EFEventCard from "../../../Cards/EFEventCard/EFEventCard";
import { withCdn } from "../../../../common/utils";
import Style from "./EventCardsList.module.scss";

const EventCardsList = ({ events, pagination, loadMore, games }) => {
  const eventCardsList = events.map(event => {
    let subTitleHref = "";

    if (event?.ownerType === "users") {
      subTitleHref = `/u/${event?.owner}`;
    } else if (event?.ownerType === "organizations") {
      subTitleHref = `/o/${event?.owner}`;
    }

    const game = games?.find(g => g._id === event?.gameSelection);

    return (
      <EFEventCard
        key={uniqueId()}
        badge="eRena"
        videoUrl=""
        imageUrl={event?.backgroundImageUrl || withCdn("/static/images/no_image.jpg")}
        title={event?.tournamentName}
        description={event?.tournamentDescription}
        titleHref={`/e/${event?.slug || event?.tournamentId}`}
        mediaHref={`/e/${event?.slug || event?.tournamentId}`}
        subTitle={event?.owner?.name}
        subTitleHref={subTitleHref}
        date={event?.startDatetime}
        game={game ? game?.title : ""}
        bracketType={event?.bracketType}
        isLive={event?.status === "Active"}
      />
    );
  });

  const hasMore = pagination && pagination.hasNextPage;

  return (
    <div className={Style.eventsScrollWrapper}>
      <InfiniteScroll
        pageStart={1}
        loadMore={loadMore}
        hasMore={hasMore}
        loader={<AnimatedLogo key={0} theme="inline" />}
      >
        <div className={Style.listWrapper}>{eventCardsList}</div>
      </InfiniteScroll>
    </div>
  );
};

export default EventCardsList;
