import React from "react";
import { useQuery } from "@apollo/client";
import GET_ERENA_FEATURED_VIDEO from "../../../../graphql/ERenaFeaturedVideoQuery";
import Error from "../../../Error/Error";
import CustomCarousel from "../../../Carousels/CustomCarousel/CustomCarousel";

const ERenaFeaturedVideoSection = () => {
  const { loading, error, data } = useQuery(GET_ERENA_FEATURED_VIDEO);
  if (loading) return [];
  if (error) return <Error />;
  const erenaFeaturedVideos = data.getERenaFeaturedVideos;

  return (
    <CustomCarousel
      sideCardHighlight
      itemsList={erenaFeaturedVideos.map(video => {
        return { ...video, videoUrl: video.youtubeLink, redirectLink: video.youtubeLink };
      })}
    />
  );
};

export default ERenaFeaturedVideoSection;
