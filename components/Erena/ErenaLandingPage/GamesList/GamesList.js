import React, { useState, useEffect } from "react";

import EFGameCardList from "../../../EFGameCardList/EFGameCardList";
import getGamesHook from "../../../../hooks/getHooks/getGamesHook";

const GamesList = () => {
  const gamesList = getGamesHook();

  const [games, setGames] = useState([]);

  useEffect(() => {
    setGames(gamesList?.map(game => ({ ...game })));
  }, [gamesList]);

  const onGameClick = game => {
    const gameIndex = games.findIndex(x => x.id === game?.id);

    const updatedGamesList = games.map(game => {
      return { ...game, active: false };
    });

    updatedGamesList[gameIndex] = { ...game, active: !game?.active };

    setGames(updatedGamesList);
  };

  return <EFGameCardList games={games} size="medium" onGameClick={onGameClick} horizontalScroll />;
};

export default GamesList;
