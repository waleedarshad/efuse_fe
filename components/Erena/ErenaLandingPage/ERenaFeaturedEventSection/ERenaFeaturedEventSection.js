import React from "react";
import { Row, Col } from "react-bootstrap";

import TwitchPlayerIframe from "../../../TwitchPlayerIframe/TwitchPlayerIframe";
import EFSidebarCard from "../../../Cards/EFSidebarCard/EFSidebarCard";
import Style from "./ERenaFeaturedEventSection.module.scss";
import FeatureFlag from "../../../General/FeatureFlag/FeatureFlag";
import FEATURE_FLAGS from "../../../../common/featureFlags";
import FeatureFlagVariant from "../../../General/FeatureFlag/FeatureFlagVariant/FeatureFlagVariant";

const ERenaFeaturedEventSection = ({ event }) => {
  return (
    <Row className={Style.featuredStreamSection}>
      <Col md={6} className={Style.featuredVideoSection}>
        <div className={Style.featuredVideo}>
          <TwitchPlayerIframe twitchChannel={event?.twitchChannel} roundedCorners fullHeight />
        </div>
        <FeatureFlag name={FEATURE_FLAGS.TEMP_VIDEO_DETAIL_BAR_WEB}>
          <FeatureFlagVariant flagState>
            <div className={Style.videoDetailBar} />
          </FeatureFlagVariant>
        </FeatureFlag>
      </Col>
      <Col md={3} className={Style.featuredSidebar}>
        <EFSidebarCard
          backgroundImageUrl={event?.backgroundImageUrl}
          logoImageUrl={event?.brandLogoUrl}
          title={event?.tournamentName}
          description={event?.tournamentDescription}
          buttonText="VIEW EVENT"
          buttonInternalHref={`/e/${event?.slug}`}
          badge="eRena"
        />
      </Col>
    </Row>
  );
};

export default ERenaFeaturedEventSection;
