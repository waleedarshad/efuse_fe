import React, { useEffect } from "react";
import { connect } from "react-redux";
import { withRouter } from "next/router";
import getConfig from "next/config";
import Internal from "../Layouts/Internal/Internal";
import Style from "./Erena.module.scss";
import LandingNav from "../Layouts/LandingNav/LandingNav";
import EFSubHeader from "../Layouts/Internal/EFSubHeader/EFSubHeader";
import EmptyComponent from "../EmptyComponent/EmptyComponent";
import { getPostableOrganizations } from "../../store/actions/userActions";
import { getCurrentUser } from "../../store/actions/common/userAuthActions";
import { getOpportunityById } from "../../store/actions/opportunityActions";
import { setTournamentSSR } from "../../store/actions/erenaActions";
import adImages from "./AdImages/AdImages";
import TournamentEventComponent from "./TournamentEventComponent/TournamentEventComponent";
import InternalNavigation from "./InternalNavigation/InternalNavigation";
import ErenaSeo from "./ErenaSeo/ErenaSeo";
import { erenaNavigationList } from "../../navigation/erena";

const { publicRuntimeConfig } = getConfig();
const { environment } = publicRuntimeConfig;

const Erena = ({
  router,
  setTournamentSSR,
  getPostableOrganizations,
  pageProps,
  currentUser,
  currentUserOrgs,
  tournament,
  eventsLogin,
  eventsStreamButton,
  mockExternalErenaEvent,
  opportunity,
  getOpportunityById,
  exp_web_erena_teep_opportunity
}) => {
  useEffect(() => {
    analytics.page("eRena");
    if (environment === "production") {
      //use for development -> 6065479e31a966001340947c
      getOpportunityById("60fad32ffa3bde003c1771cf");
    }
  }, []);

  useEffect(() => {
    setTournamentSSR(pageProps.event);
    if (currentUser?.id) {
      getPostableOrganizations(1);
    }
  }, [currentUser?.id, currentUserOrgs?.length, tournament?._id]);

  const isTournamentOwner = () => {
    return currentUser?.id === tournament?.owner;
  };

  const orgOwner = () => {
    return currentUserOrgs.find(org => org?._id === tournament?.owner);
  };

  const eventSSR = pageProps.event;
  let innerComponent;
  let actionButtons;

  const leaderboardRoute = router.query.leaderboard;
  const userLoggedIn = currentUser?.id;

  let isOwner = false;
  if (userLoggedIn) {
    isOwner = isTournamentOwner() || orgOwner();
  }

  let ads = [];

  innerComponent = (
    <EmptyComponent style={{ display: "block", width: "100%" }} text="No eRena event found for this url." />
  );

  if (tournament) {
    // adImages
    ads = adImages(tournament);

    //set buttons for navigation
    actionButtons = InternalNavigation(tournament);

    innerComponent = (
      <TournamentEventComponent
        currentUser={currentUser}
        tournament={tournament}
        eventsLogin={eventsLogin}
        eventsStreamButton={eventsStreamButton}
        leaderboardRoute={leaderboardRoute}
        isOwner={isOwner}
        mockExternalErenaEvent={mockExternalErenaEvent}
        eventSSR={eventSSR}
        ads={ads}
        router={router}
        opportunity={opportunity}
        exp_web_erena_teep_opportunity={exp_web_erena_teep_opportunity}
      />
    );
  }

  return (
    <>
      <ErenaSeo eventSSR={eventSSR} />
      {!userLoggedIn ? (
        <>
          <LandingNav preventScrollFade />
          {tournament ? (
            <EFSubHeader
              headerImage="https://cdn.efuse.gg/static/images/Erena_Logo.png"
              actionButtons={actionButtons}
              customImageStyle={Style.customImageStyle}
            />
          ) : (
            <></>
          )}
          <div className={Style.internalWrapper}>
            <div className={Style.zIndex}>{innerComponent}</div>
          </div>
        </>
      ) : (
        <Internal metaTitle="eFuse | eRena" isAuthenticated containsSubheader>
          <EFSubHeader
            headerImage="https://cdn.efuse.gg/static/images/Erena_Logo.png"
            navigationList={
              isOwner && erenaNavigationList(router?.query, router?.pathname, tournament?.bracketType === "bracket")
            }
            actionButtons={actionButtons}
            customImageStyle={Style.customImageStyle}
          />
          <div className={Style.zIndex}>{innerComponent}</div>
        </Internal>
      )}
    </>
  );
};

const mapStateToProps = state => ({
  isWindowView: state.messages.isWindowView,
  currentUser: state.auth.currentUser,
  eventsLogin: state.features.events_login,
  exp_web_erena_teep_opportunity: state.features.exp_web_erena_teep_opportunity,
  eventsStreamButton: state.features.events_stream_button,
  tournament: state.erena.tournament,
  currentUserOrgs: state.user.postableOrganizations,
  mockExternalErenaEvent: state.erena.mockExternalErenaEvent,
  opportunity: state.opportunities.opportunity
});

export default connect(mapStateToProps, {
  setTournamentSSR,
  getPostableOrganizations,
  getCurrentUser,
  getOpportunityById
})(withRouter(Erena));
