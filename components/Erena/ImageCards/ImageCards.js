import React from "react";

import Style from "./ImageCards.module.scss";
import AddItem from "../../AddItem/AddItem";
import CreateAd from "../ErenaDetails/CreateAd/CreateAd";
import DisplayAd from "./DisplayAd/DisplayAd";

const ImageCards = ({ gridLayout, ads, isOwner, mockExternalErenaEvent }) => {
  return (
    <div
      className={`${gridLayout === "double" && Style.doubleGridContainer} ${gridLayout === "triple" &&
        Style.tripleGridContainer} ${gridLayout === "single" && Style.singleGridContainer}`}
    >
      {ads.map((ad, index) => {
        if (!mockExternalErenaEvent && (!ad?.image || !ad?.url) && isOwner) {
          return (
            <AddItem
              text="Create Promo"
              modalTitle="Create Promo"
              key={index}
              modalComponent={<CreateAd adIndex={index} />}
            >
              <div className={Style.ad} />
            </AddItem>
          );
        }

        if ((!ad?.image || !ad?.url) && !isOwner) {
          return <></>;
        }

        const innerComponent = ad?.image && (
          <DisplayAd ad={ad} index={index} isOwner={isOwner} mockExternalErenaEvent={mockExternalErenaEvent} />
        );

        return isOwner ? (
          innerComponent
        ) : (
          <a href={ad.url} target="_blank" key={index} rel="noreferrer">
            {innerComponent}
          </a>
        );
      })}
    </div>
  );
};

ImageCards.defaultProps = {
  gridLayout: "double"
};

export default ImageCards;
