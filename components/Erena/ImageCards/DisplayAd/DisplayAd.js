import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPencilAlt } from "@fortawesome/pro-solid-svg-icons";

import AddItem from "../../../AddItem/AddItem";
import CreateAd from "../../ErenaDetails/CreateAd/CreateAd";
import Style from "../ImageCards.module.scss";

const DisplayAd = ({ ad, index, mockExternalErenaEvent, isOwner }) => {
  return (
    <div className={Style.ad}>
      <img className={Style.adImage} src={ad.image} alt="Event ad" />
      {!mockExternalErenaEvent && isOwner && (
        <AddItem
          text="Edit Promo"
          modalTitle="Edit Promo"
          editMode
          modalComponent={<CreateAd adIndex={index} editAdImage={ad.image} editAdUrl={ad.url} editMode />}
        >
          <div className={Style.editButton}>
            <FontAwesomeIcon icon={faPencilAlt} className={Style.icon} />
            Edit
          </div>
        </AddItem>
      )}
    </div>
  );
};

export default DisplayAd;
