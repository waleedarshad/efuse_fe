import React from "react";

import { NextSeo } from "next-seo";

const ErenaSeo = ({ eventSSR }) =>
  eventSSR?.tournamentName ? (
    <NextSeo
      title={`${eventSSR?.tournamentName} | eRena`}
      description={eventSSR?.tournamentDescription}
      openGraph={{
        type: "website",
        url: `https://efuse.gg/e/${eventSSR.tournamentId}`,
        title: `${eventSSR?.tournamentName} | eRena`,
        site_name: "eFuse.gg",
        description: `${eventSSR?.tournamentDescription}`,
        images: [
          {
            url: eventSSR?.brandLogoUrl
          }
        ]
      }}
      twitter={{
        handle: "@eFuseOfficial",
        site: `https://efuse.gg/e/${eventSSR.tournamentId}`,
        cardType: "summary_large_image"
      }}
    />
  ) : (
    <NextSeo
      title="eRena powered by eFuse | Duke it out on a Global Stage"
      description="The eRena, powered by eFuse, is where cultures collide and gaming skills are put to the test with BIG bucks on the line. Athletes, pro gamers, streamers, musicians, and yes...LUCKY FANS will all go head-to-head as they fight for bragging rights and their fair share of the prize pool. Do YOU have what it takes?"
      openGraph={{
        type: "website",
        url: "https://efuse.gg/",
        title: "eRena powered by eFuse | Duke it out on a Global Stage",
        site_name: "eFuse.gg",
        description:
          "The eRena, powered by eFuse, is where cultures collide and gaming skills are put to the test with BIG bucks on the line. Athletes, pro gamers, streamers, musicians, and yes...LUCKY FANS will all go head-to-head as they fight for bragging rights and their fair share of the prize pool. Do YOU have what it takes?",
        images: [{ url: "https://efuse.gg/static/images/fb_scrape.png" }]
      }}
      twitter={{
        handle: "@eFuseOfficial",
        site: "https://efuse.gg/static/images/fb_scrape.png",
        cardType: "summary_large_image"
      }}
    />
  );
export default ErenaSeo;
