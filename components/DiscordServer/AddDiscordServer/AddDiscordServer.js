import React, { Component } from "react";
import { connect } from "react-redux";
import { Form, Col } from "react-bootstrap";
import InputRow from "../../InputRow/InputRow";
import InputLabel from "../../InputLabel/InputLabel";
import InputField from "../../InputField/InputField";
import ActionButtonGroup from "../../Layouts/Internal/ActionButtonGroup/ActionButtonGroup";
import { dispatchNotification } from "../../../store/actions/notificationActions";
import { setDiscordServer } from "../../../store/actions/userActions";
import { setDiscordServerOrg } from "../../../store/actions/organizationActions";

import Style from "../DiscordServer.module.scss";
import EFPrimaryModal from "../../Modals/EFPrimaryModal/EFPrimaryModal";

class AddDiscordServer extends Component {
  state = {
    validated: false,
    valuesSet: false,
    discordServer: ""
  };

  onChange = event => {
    this.setState({
      discordServer: event.target.value
    });
  };

  onSubmit = formEvent => {
    formEvent.preventDefault();
    const { validated, discordServer } = this.state;
    if (!validated) {
      this.setState({ validated: true });
    }
    if (formEvent.currentTarget.checkValidity() && discordServer) {
      if (this.props.isOrganization) {
        this.props.setDiscordServerOrg(this.props.orgId, {
          discordServer
        });
      } else {
        this.props.setDiscordServer({ discordServer });
      }
      this.props.onHide();
    }
  };

  render() {
    const { show, onHide, inProgress } = this.props;
    const { validated, discordServer } = this.state;
    return (
      <EFPrimaryModal
        title="Add Discord Server"
        widthTheme="medium"
        isOpen={show}
        onClose={onHide}
        allowBackgroundClickClose={false}
      >
        <div className={Style.imageContainer}>
          <div className={Style.imageBody}>
            <p className={Style.title}>Step 1</p>
            <p className={Style.desc}>Click the down arrow on your server to open more options.</p>
            <img className={Style.step1} src="https://cdn.efuse.gg/uploads/static/discord/discord_step1.png" />
          </div>
          <div className={Style.imageBody}>
            <p className={Style.title}>Step 2</p>
            <p className={Style.desc}>Select the 'Server Settings' option.</p>
            <img className={Style.step2} src="https://cdn.efuse.gg/uploads/static/discord/discord_step2.png" />
          </div>
          <div className={Style.imageBody}>
            <p className={Style.title}>Step 3</p>
            <p className={Style.desc}>
              Navigate to the 'Widget' on the left side of your screen. 'Enable Server Widget' by clicking the toggle
              button. Select an 'Invite Channel' if you would like users to join your Discord (if you wouldn't like
              users to join your discord, ignore this option). Lastly, copy the 'Server ID' and paste it below.
            </p>
            <img className={Style.step3} src="https://cdn.efuse.gg/uploads/static/discord/discord_step3.png" />
          </div>
        </div>
        <br />
        <Form noValidate validated={validated} onSubmit={this.onSubmit}>
          <InputRow>
            <Col sm={12}>
              <InputLabel theme="darkColor">Add Discord Server ID</InputLabel>
              <InputField
                name="discordServer"
                placeholder="ex: 674659136004292657"
                theme="whiteShadow"
                size="lg"
                onChange={this.onChange}
                value={discordServer}
                required
                errorMessage="Discord Server ID is required"
              />
            </Col>
          </InputRow>
          <ActionButtonGroup onCancel={onHide} disabled={inProgress} />
        </Form>
      </EFPrimaryModal>
    );
  }
}
const mapStateToProps = state => ({
  inProgress: state.portfolio.inProgress
});

export default connect(mapStateToProps, {
  setDiscordServer,
  setDiscordServerOrg
})(AddDiscordServer, dispatchNotification);
