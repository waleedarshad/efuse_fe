import React, { Component } from "react";
import { connect } from "react-redux";
import { Card } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrash, faPlus } from "@fortawesome/pro-solid-svg-icons";

import AddButton from "../User/Portfolio/AddButton/AddButton";
import Style from "./DiscordServer.module.scss";
import { togglePortfolioModal } from "../../store/actions/portfolioActions";
import { removeDiscordServerOrg } from "../../store/actions/organizationActions";
import AddDiscordServer from "./AddDiscordServer/AddDiscordServer";
import { removeDiscordServer } from "../../store/actions/userActions";
import ConfirmAlert from "../ConfirmAlert/ConfirmAlert";
import DiscordIframe from "../DiscordIframe/DiscordIframe";

class DiscordServer extends Component {
  loadModal = () => {
    this.props.togglePortfolioModal(true, "discord_server");
  };

  onHide = () => {
    this.props.togglePortfolioModal(false);
  };

  onRemove = () => {
    if (this.props.isOrganization) {
      this.props.removeDiscordServerOrg(this.props.orgId);
    } else {
      this.props.removeDiscordServer();
    }
  };

  render() {
    const {
      user,
      currentUser,
      modal,
      modalSection,
      discordServer,
      portfolio_discord_server,
      organization_discord_server,
      isOrganization,
      userProfile,
      isOrgOwner,
      orgId
    } = this.props;
    const isOwner = currentUser && currentUser.id === user._id;
    return (
      <>
        {(userProfile && portfolio_discord_server) || (isOrganization && organization_discord_server) ? (
          <>
            {((isOwner && userProfile) || (isOrgOwner && isOrganization)) && (
              <>
                <Card className={`${Style.container} customCard`}>
                  <Card.Body>
                    <div className={Style.addButtonWrapper}>
                      <AddButton
                        desc={`${discordServer ? "Edit" : "Add"} Discord Server`}
                        icon={faPlus}
                        onClick={this.loadModal}
                      />
                    </div>
                  </Card.Body>
                </Card>
                <br />
              </>
            )}
            {discordServer && (
              <>
                {((isOwner && userProfile) || (isOrgOwner && isOrganization)) && (
                  <ConfirmAlert
                    title="Remove Discord Server?"
                    message="Are you sure you would like to remove your discord server?"
                    onYes={this.onRemove}
                  >
                    <FontAwesomeIcon className={Style.trash} icon={faTrash} />
                  </ConfirmAlert>
                )}
                <DiscordIframe discordServer={discordServer} />
              </>
            )}
            {modal && modalSection === "discord_server" && (
              <AddDiscordServer show={modal} onHide={this.onHide} orgId={orgId} isOrganization={isOrganization} />
            )}
          </>
        ) : (
          <></>
        )}
      </>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user.currentUser,
  modal: state.portfolio.modal,
  modalSection: state.portfolio.modalSection,
  currentUser: state.auth.currentUser,
  portfolio_discord_server: state.features.portfolio_discord_server,
  organization_discord_server: state.features.organization_discord_server
});

export default connect(mapStateToProps, {
  togglePortfolioModal,
  removeDiscordServer,
  removeDiscordServerOrg
})(DiscordServer);
