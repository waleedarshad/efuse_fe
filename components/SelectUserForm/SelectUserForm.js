import { connect } from "react-redux";
import { Form, FormControl } from "react-bootstrap";
import AsyncSelect from "react-select/lib/Async";
import React, { useState } from "react";

import { getImage } from "../../helpers/GeneralHelper";
import { searchForUsers } from "../../helpers/SearchHelper";
import { sendInvite } from "../../store/actions/organizationActions";
import { withCdn } from "../../common/utils";
import EFRectangleButton from "../Buttons/EFRectangleButton/EFRectangleButton";
import Style from "./SelectUserForm.module.scss";

const SelectUserForm = ({ placeholder, submitFunction, colorTheme, shadowTheme, excludedUsers }) => {
  const [validated, setValidated] = useState(false);
  const [inviteesEmptyError, setEmptyError] = useState(false);
  const [invitees, setInvitees] = useState([]);

  const onChange = selectedOptions => {
    setEmptyError(!selectedOptions.length > 0);
    setInvitees(selectedOptions.map(opt => opt));
  };

  const loadOptions = (input, callback) => {
    const inviteeIds = invitees.map(invitee => invitee.value);

    searchForUsers(input).then(response => {
      const options = response.data.users.docs
        .map(user => ({
          value: user._id,
          label: (
            <div className="optionWithImage">
              <img src={getImage(user.profilePicture, "avatar")} alt="Profile" />{" "}
              <span>
                <strong>{user.name}</strong>{" "}
              </span>
              {user.verified && (
                <span>
                  <img
                    className={Style.userImage}
                    src={withCdn("/static/images/efuse_verify.png")}
                    alt="eFuse Verify"
                  />
                </span>
              )}
              <span>@{user.username}</span>
            </div>
          ),
          avatar: user.profilePicture,
          url: user.url,
          username: user.username,
          type: user.type
        }))
        .filter(u => !inviteeIds.includes(u.value) && !excludedUsers?.includes(u.value));

      callback(options);
    });
  };

  const onSubmit = event => {
    event.preventDefault();
    setEmptyError(invitees.length === 0);
    setValidated(true);

    if (event.currentTarget.checkValidity() && invitees.length > 0) {
      submitFunction(invitees);
      setInvitees([]);
    }
  };

  return (
    <Form onSubmit={e => onSubmit(e)} noValidate validated={validated}>
      <AsyncSelect
        cacheOptions
        isMulti
        onChange={onChange}
        loadOptions={loadOptions}
        className="nav-home nav-search"
        value={invitees}
        placeholder={placeholder}
        id="dynamic-id-search-users"
        key={invitees?.length}
      />
      {inviteesEmptyError && (
        <FormControl.Feedback type="invalid" className={Style.formFeedback}>
          Select one or more to invite
        </FormControl.Feedback>
      )}

      <div className={Style.buttonWrapper}>
        <EFRectangleButton shadowTheme={shadowTheme} buttonType="submit" text="Submit" colorTheme={colorTheme} />
      </div>
    </Form>
  );
};

SelectUserForm.defaultProps = {
  colorTheme: "primary"
};
// TODO: Wire up add user to work for orgs instead of institutions
export default connect(null, { sendInvite })(SelectUserForm);
