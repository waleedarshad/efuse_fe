import getConfig from "next/config";
import dynamic from "next/dynamic";
import PropTypes from "prop-types";
import { useState } from "react";
import { Overlay, Popover } from "react-bootstrap";
import Style from "./EFGiphySearch.module.scss";

const ReactGiphySearchBox = dynamic(import("react-giphy-searchbox"), {
  ssr: false
});

const { publicRuntimeConfig } = getConfig();
const { giphyApiKey } = publicRuntimeConfig;

const EFGifSearch = ({ renderCustomTrigger, onGifSelected, disabled, placement }) => {
  const [show, setShow] = useState(false);
  const [target, setTarget] = useState(null);

  const closePopover = () => {
    setShow(false);
  };

  const showPopover = event => {
    setShow(!show);
    setTarget(event.target);
  };

  const closeOnSelection = giphy => {
    onGifSelected(giphy);
    closePopover();
  };

  const onTriggerClick = event => {
    if (!disabled) {
      showPopover(event);
    }
  };

  return (
    <>
      <Overlay
        target={target}
        show={show}
        placement={placement}
        rootClose
        rootCloseEvent="mousedown"
        onHide={closePopover}
      >
        <Popover className={Style.popover}>
          <Popover.Content>
            <ReactGiphySearchBox
              apiKey={giphyApiKey}
              onSelect={closeOnSelection}
              masonryConfig={[
                { columns: 2, imageWidth: 110, gutter: 5 },
                { mq: "700px", columns: 3, imageWidth: 110, gutter: 5 }
              ]}
              wrapperClassName={Style.giphyWrapper}
              listWrapperClassName={Style.listWrapper}
              searchFormClassName={Style.formWrapper}
            />
          </Popover.Content>
        </Popover>
      </Overlay>

      {renderCustomTrigger(onTriggerClick)}
    </>
  );
};

EFGifSearch.propTypes = {
  placement: PropTypes.oneOf(["bottom", "top"])
};

EFGifSearch.defaultProps = {
  placement: "bottom"
};
export default EFGifSearch;
