import { Col } from "react-bootstrap";

import Style from "./Friend.module.scss";
import EFAvatar from "../EFAvatar/EFAvatar";
import { rescueNil, getImage, formatRoles } from "../../helpers/GeneralHelper";
import EFFollow from "../EFFollow/EFFollow";
import UserLink from "../UserLink/UserLink";

const Friend = ({ user, renderAs, hasKey, currentUser, followOveride, unfollowOveride }) => {
  const id = user.id || user._id;
  const profilePicture = rescueNil(user, "profilePicture", {});
  return (
    <Col md={3} sm={4}>
      <div className={Style.friendWrapper}>
        <EFAvatar displayOnlineButton={false} profilePicture={getImage(profilePicture, "avatar")} size="medium" />
        <h5 className={Style.name}>
          <UserLink user={user} />{" "}
        </h5>
        <p className={Style.roles}>{formatRoles(user)}</p>
        {id !== currentUser.id && (
          <EFFollow
            id={id}
            currentUser={currentUser}
            renderAs={renderAs}
            isFollowed={hasKey}
            followOveride={followOveride}
            unfollowOveride={unfollowOveride}
          />
        )}
      </div>
    </Col>
  );
};

export default Friend;
