import FormControl from "react-bootstrap/FormControl";
import TextMask from "react-text-mask";
import PropTypes from "prop-types";

import Style from "../InputField/InputField.module.scss";

const MaskedInput = props => {
  const { errorMessage, theme, ...rest } = props;
  return (
    <>
      <TextMask {...rest} className={`${Style.inputField} ${Style[theme]} form-control form-control-lg`} />
      {errorMessage && <FormControl.Feedback type="invalid">{errorMessage}</FormControl.Feedback>}
    </>
  );
};

MaskedInput.propTypes = {
  mask: PropTypes.array.isRequired,
  guide: PropTypes.bool,
  placeholderChar: PropTypes.string,
  errorMessage: PropTypes.string,
  theme: PropTypes.string
};

MaskedInput.defaultProps = {
  guide: false,
  placeholderChar: "\u2000",
  theme: "external"
};

export default MaskedInput;
