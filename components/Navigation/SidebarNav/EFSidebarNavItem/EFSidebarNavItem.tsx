import Link from "next/link";
import React from "react";
import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import EFRoundAvatar from "../../../Avatar/EFRoundAvatar/EFRoundAvatar";
import { SmallHeader } from "../../../../styles/fontStyles";
import colors from "../../../../styles/colors";
import { NavItem } from "./NavItem";

interface EFSidebarNavItemProps {
  item: NavItem;
  pathName: string;
}

const EFSidebarNavItem: React.FC<EFSidebarNavItemProps> = ({ item, pathName }) => {
  const { icon, image, customIcon, href, imageAltText, name } = item;
  const selected = href === pathName;

  return (
    <Link href={href} passHref>
      <NavItemRow className="ef-sidebar-nav-item__nav-item-row" selected={selected}>
        <BorderDiv selected={selected} />
        {icon && (
          <IconContainer>
            <FontAwesomeIcon icon={icon} />
          </IconContainer>
        )}
        {image && (
          <IconContainer>
            <EFRoundAvatar src={image} altText={imageAltText} />
          </IconContainer>
        )}
        {customIcon && (
          <IconContainer>
            <CustomIcon selected={selected} viewBox="10 10 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
              {customIcon}
            </CustomIcon>
          </IconContainer>
        )}
        <NavigationLabel href={href} pathName={pathName}>
          {name}
        </NavigationLabel>
      </NavItemRow>
    </Link>
  );
};
const BorderDiv = styled.div`
  width: 4px;
  height: 40px;
  background-color: ${colors.navigationBlue};
  border-radius: 0 3px 3px 0;
  position: absolute;
  top: -4.5px;
  left: 0;
  display: ${props => (props.selected ? "block" : "none")};
`;

const NavigationLabel = styled.span`
  ${SmallHeader};
  margin-left: 10px;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  cursor: pointer;
`;

const IconContainer = styled.div`
  width: 30px;
`;

const CustomIcon = styled.svg`
  width: 24px;
  height: 24px;
  padding-bottom: 2px;

  path {
    fill: ${props => (props.selected ? colors.navigationBlue : colors.navigationText)};
  }
`;

const NavItemRow = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  cursor: pointer;
  padding: 0 20px;
  margin: 9px 0;
  color: ${props => (props.selected ? colors.navigationBlue : colors.navigationText)};
  &:hover ${NavigationLabel}, &:hover ${IconContainer} {
    color: ${colors.navigationBlue};
  }
  &:hover ${CustomIcon} {
    path {
      fill: ${colors.navigationBlue};
    }
  }
  min-height: 30px;
`;

export default EFSidebarNavItem;
