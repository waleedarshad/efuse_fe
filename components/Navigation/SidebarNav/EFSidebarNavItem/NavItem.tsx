import { IconDefinition } from "@fortawesome/pro-solid-svg-icons";
import { ReactNode } from "react";

export interface NavItem {
  name: string;
  href: string;
  icon?: IconDefinition;
  customIcon?: ReactNode;
  children?: NavItem[];
  image?: string;
  imageAltText?: string;
  featureFlag?: string;
}
