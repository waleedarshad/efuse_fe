import React from "react";
import styled from "styled-components";
import uniqueId from "lodash/uniqueId";

import { NavSection } from "../EFSidebarNavSection/NavSection";
import EFSidebarNavSection from "../EFSidebarNavSection/EFSidebarNavSection";

const EFSidebarContainer = ({ navSections }: { navSections: NavSection[] }) => {
  return (
    <SidebarWrapper>
      {navSections?.map(section => {
        return <EFSidebarNavSection key={uniqueId()} navSection={section} />;
      })}
    </SidebarWrapper>
  );
};

const SidebarWrapper = styled.div`
  width: 240px;
  margin: 0px;
`;

export default EFSidebarContainer;
