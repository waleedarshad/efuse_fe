import { NavItem } from "../EFSidebarNavItem/NavItem";

export interface NavSection {
  title?: string;
  navItems: NavItem[];
}
