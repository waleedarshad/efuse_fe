import React from "react";
import styled from "styled-components";
import { useRouter } from "next/router";
import uniqueId from "lodash/uniqueId";

import EFSidebarNavItem from "../EFSidebarNavItem/EFSidebarNavItem";
import { NavSection } from "./NavSection";

const EFSidebarNavSection = ({ navSection }: { navSection: NavSection }) => {
  const router = useRouter();
  const title = navSection?.title;
  const items = navSection?.navItems;

  const navigationItems = items?.map(item => {
    return <EFSidebarNavItem key={uniqueId()} item={item} pathName={router?.asPath} />;
  });

  return (
    <>
      {items?.length > 0 && (
        <>
          {title && <NavTitle>{title}</NavTitle>}
          <div>{navigationItems}</div>
        </>
      )}
    </>
  );
};

const NavTitle = styled.div`
  font-family: Poppins, sans-serif;
  font-size: 14px;
  font-style: normal;
  font-weight: 500;
  line-height: 20px;
  letter-spacing: 0;
  text-align: left;
  margin-top: 10px;
  margin-bottom: 10px;
  padding-left: 20px;
`;

export default EFSidebarNavSection;
