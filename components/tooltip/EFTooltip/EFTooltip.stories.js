import React from "react";
import EFTooltip from "./EFTooltip";

export default {
  title: "Shared/EFTooltip",
  component: EFTooltip,
  argTypes: {
    tooltipText: {
      control: {
        type: "text"
      }
    },
    tooltipPlacement: {
      control: {
        type: "select",
        options: ["top", "bottom", "left", "right"]
      }
    },
    tooltipDelay: {
      control: {
        type: "object"
      }
    }
  },
  decorators: [
    Story => (
      <div style={{ marginTop: "2em", marginLeft: "8em" }}>
        <Story />
      </div>
    )
  ]
};

const Story = args => <EFTooltip {...args} />;

export const Basic = Story.bind({});
Basic.args = {
  children: <span style={{ backgroundColor: "blue", color: "white" }}>Hover Me!</span>,
  tooltipContent: "Tooltip Hover!",
  tooltipPlacement: "bottom"
};

export const DelayHover = Story.bind({});
DelayHover.args = {
  children: <span style={{ backgroundColor: "blue", color: "white" }}>Hover Me!</span>,
  tooltipContent: "Tooltip Hover!",
  tooltipPlacement: "bottom",
  tooltipDelay: { show: 500, hide: 500 }
};

export const Top = Story.bind({});
Top.args = {
  children: <span style={{ backgroundColor: "blue", color: "white" }}>Hover Me!</span>,
  tooltipContent: "Tooltip Hover!",
  tooltipPlacement: "top"
};

export const Right = Story.bind({});
Right.args = {
  children: <span style={{ backgroundColor: "blue", color: "white" }}>Hover Me!</span>,
  tooltipContent: "Tooltip Hover!",
  tooltipPlacement: "right"
};

export const Left = Story.bind({});
Left.args = {
  children: <span style={{ backgroundColor: "blue", color: "white" }}>Hover Me!</span>,
  tooltipContent: "Tooltip Hover!",
  tooltipPlacement: "left"
};

export const Bottom = Story.bind({});
Bottom.args = {
  children: <span style={{ backgroundColor: "blue", color: "white" }}>Hover Me!</span>,
  tooltipContent: "Tooltip Hover!",
  tooltipPlacement: "bottom"
};
