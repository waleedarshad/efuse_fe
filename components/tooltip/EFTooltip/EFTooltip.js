import React from "react";
import { OverlayTrigger, Tooltip } from "react-bootstrap";

const EFTooltip = ({ children, tooltipContent, tooltipPlacement, delay }) => {
  return (
    <OverlayTrigger
      placement={tooltipPlacement}
      delay={delay}
      overlay={<Tooltip style={{ zIndex: 9999 }}>{tooltipContent}</Tooltip>}
      trigger={["hover", "focus"]}
    >
      {children}
    </OverlayTrigger>
  );
};

EFTooltip.defaultProps = {
  tooltipPlacement: "bottom",
  delay: { show: 0, hide: 0 }
};

export default EFTooltip;
