import React from "react";
import EFFormattedTooltip from "./EFFormattedTooltip";

export default {
  title: "Shared/EFFormattedTooltip",
  component: EFFormattedTooltip,
  argTypes: {
    tooltipContent: {
      control: {
        type: "text"
      }
    },
    tooltipPlacement: {
      control: {
        type: "select",
        options: ["top", "bottom", "left", "right"]
      }
    },
    delay: {
      control: {
        type: "object"
      }
    },
    trigger: {
      control: {
        type: "text"
      }
    }
  }
};

const Story = args => <EFFormattedTooltip {...args} />;

export const Basic = Story.bind({});
Basic.args = {
  children: <span style={{ backgroundColor: "blue", color: "white" }}>Click Me!</span>,
  tooltipContent: "Tooltip Click!",
  tooltipPlacement: "bottom"
};

export const DelayHover = Story.bind({});
DelayHover.args = {
  children: <span style={{ backgroundColor: "blue", color: "white" }}>Click Me!</span>,
  tooltipContent: "Tooltip Click!",
  tooltipPlacement: "bottom"
};

export const Top = Story.bind({});
Top.args = {
  children: <span style={{ backgroundColor: "blue", color: "white" }}>Click Me!</span>,
  tooltipContent: "Tooltip Click!",
  tooltipPlacement: "top"
};

export const Right = Story.bind({});
Right.args = {
  children: <span style={{ backgroundColor: "blue", color: "white" }}>Click Me!</span>,
  tooltipContent: "Tooltip Click!",
  tooltipPlacement: "right"
};

export const Left = Story.bind({});
Left.args = {
  children: <span style={{ backgroundColor: "blue", color: "white" }}>Click Me!</span>,
  tooltipContent: "Tooltip Click!",
  tooltipPlacement: "left"
};

export const Bottom = Story.bind({});
Bottom.args = {
  children: <span style={{ backgroundColor: "blue", color: "white" }}>Click Me!</span>,
  tooltipContent: "Tooltip Click!",
  tooltipPlacement: "bottom"
};
