import React, { useState } from "react";
import { Tooltip } from "react-bootstrap";
import { faTimes } from "@fortawesome/pro-solid-svg-icons";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { EFHtmlParser } from "../../EFHtmlParser/EFHtmlParser";
import EFBaseToolTip from "../EFBaseTooltip/EFBaseTooltip";
import Style from "./EFFormattedTooltip.module.scss";
import EFButton from "../../Buttons/EFButton/EFButton";

const EFFormattedTooltip = ({ children, tooltipContent, tooltipPlacement, delay, trigger }) => {
  const [show, toggle] = useState(false);
  return (
    <EFBaseToolTip
      tooltipPlacement={tooltipPlacement}
      delay={delay}
      show={show}
      onToggle={() => toggle(!show)}
      overlay={
        <Tooltip className={Style.tooltipBox} style={{ zIndex: 9999 }}>
          <div className={Style.content}>
            <div className={Style.icon}>
              <EFButton onClick={() => toggle(!show)} colorTheme="transparent">
                <FontAwesomeIcon className={Style.close} icon={faTimes} />
              </EFButton>
            </div>
            <EFHtmlParser>{tooltipContent}</EFHtmlParser>
          </div>
        </Tooltip>
      }
      trigger={trigger}
    >
      {children}
    </EFBaseToolTip>
  );
};

EFFormattedTooltip.defaultProps = {
  tooltipPlacement: "bottom",
  delay: { show: 0, hide: 0 },
  trigger: "click"
};

export default EFFormattedTooltip;
