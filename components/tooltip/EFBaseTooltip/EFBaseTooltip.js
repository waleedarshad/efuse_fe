import React from "react";
import { OverlayTrigger } from "react-bootstrap";
import propTypes from "prop-types";

const EFBaseToolTip = ({ children, tooltipPlacement, delay, overlay, trigger, show, onToggle }) => {
  return (
    <OverlayTrigger
      show={show}
      placement={tooltipPlacement}
      delay={delay}
      overlay={overlay}
      trigger={trigger}
      onToggle={onToggle}
    >
      {children}
    </OverlayTrigger>
  );
};

EFBaseToolTip.propTypes = {
  trigger: propTypes.oneOf(["hover", "click"])
};

EFBaseToolTip.defaultProps = {
  trigger: "hover"
};

export default EFBaseToolTip;
