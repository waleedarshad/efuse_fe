import debounce from "lodash/debounce";
import { Component } from "react";
import AsyncSelect from "react-select/lib/Async";
import Router from "next/router";
import { GLOBAL_SEARCH_EVENTS } from "../../common/analyticEvents";
import { getImage } from "../../helpers/GeneralHelper";
import { search } from "../../helpers/SearchHelper";
import { withCdn } from "../../common/utils";
import Style from "./SearchUsers.module.scss";

const INPUT_DEBOUNCE = 200;

class SearchUsers extends Component {
  constructor(props) {
    super(props);
    this.state = { selectedOption: null, searchedText: "" };
  }

  loadOptions = (input, callback) => {
    this.setState({ searchedText: input });
    search(input).then(response => {
      const options = response.data.users.docs.map(user => ({
        value: user._id,
        label: (
          <div className="optionWithImage">
            <img src={getImage(user.profilePicture, "avatar")} alt="Profile" />{" "}
            <span>
              <strong>{user.name}</strong>{" "}
            </span>
            {user.verified && (
              <span>
                <img
                  style={{ width: "16px", height: "auto" }}
                  src={withCdn("/static/images/efuse_verify.png")}
                  alt="eFuse Verify"
                />
              </span>
            )}
            <span>@{user.username}</span>
          </div>
        ),
        avatar: user.profilePicture,
        url: user.url,
        username: user.username,
        type: user.type
      }));

      callback(options);
    });
  };

  onChange = selectedOption => {
    const { url, value } = selectedOption;
    analytics.track(GLOBAL_SEARCH_EVENTS.COMPLETE, { selectedUser: value, term: this.state.searchedText });

    this.setState({ selectedOption });

    Router.push(url);
  };

  onFocus = () => {
    analytics.track(GLOBAL_SEARCH_EVENTS.STARTED);
  };

  render() {
    const { selectedOption } = this.state;
    const { placeholder } = this.props;

    return (
      <AsyncSelect
        cacheOptions
        className={`nav-home nav-search ${Style.searchWrapper}`}
        id="dynamic-id-search-users"
        loadOptions={debounce(this.loadOptions, INPUT_DEBOUNCE)}
        noOptionsMessage={() => null}
        onChange={this.onChange}
        onFocus={this.onFocus}
        placeholder={placeholder}
        value={selectedOption}
      />
    );
  }
}

export default SearchUsers;
