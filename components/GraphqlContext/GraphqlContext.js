import { createContext } from "react";
import { useQuery } from "@apollo/client";

import Error from "../../pages/_error";

export const GraphqlContext = createContext();

export const GraphqlContextProvider = ({ query, children, options }) => {
  const { loading, error, data } = useQuery(query, options);

  let renderedComponent = children;

  if (!loading && error) {
    console.error(error);
    renderedComponent = <Error />;
  }

  const contextProps = {
    loading,
    error,
    data
  };

  return <GraphqlContext.Provider value={contextProps}>{renderedComponent}</GraphqlContext.Provider>;
};

GraphqlContextProvider.defaultProps = {
  options: undefined
};
