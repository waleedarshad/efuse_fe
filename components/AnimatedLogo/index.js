import PropTypes from "prop-types";
import { withCdn } from "../../common/utils";
import Style from "./AnimatedLogo.module.scss";

const AnimatedLogo = ({ theme }) => (
  <div className={Style[theme]}>
    <img src={withCdn("/static/images/loader.svg")} alt="Loading" className={Style.logo} />
  </div>
);

AnimatedLogo.propTypes = {
  theme: PropTypes.string.isRequired
};

export default AnimatedLogo;
