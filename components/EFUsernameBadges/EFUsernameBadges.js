import React from "react";
import EFAffiliateBadge from "../EFAffiliateBadge/EFAffiliateBadge";
import EFPartnerBadge from "../EFPartnerBadge/EFPartnerBadge";
import EFStreakBadge from "../EFStreakBadge/EFStreakBadge";
import Style from "./EFUsernameBadges.module.scss";

const EFUsernameBadges = ({
  username,
  showStreakBadge,
  streakValue,
  showAffiliateBadge,
  affiliateBadgeText,
  showPartnerBadge,
  partnerBadgeText
}) => {
  return (
    <span className={Style.wrapper}>
      <span className={Style.username}>{username}</span>
      {showStreakBadge && (
        <div className={Style.marginRight}>
          <EFStreakBadge streak={streakValue} />
        </div>
      )}
      {showAffiliateBadge && affiliateBadgeText && !partnerBadgeText && <EFAffiliateBadge text={affiliateBadgeText} />}
      {showPartnerBadge && partnerBadgeText && <EFPartnerBadge text={partnerBadgeText} />}
    </span>
  );
};

export default EFUsernameBadges;
