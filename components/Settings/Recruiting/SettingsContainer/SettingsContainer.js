import { Card } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck, faTimes } from "@fortawesome/pro-solid-svg-icons";
import Style from "./SettingsContainer.module.scss";

const SettingsContainer = ({ children, title, progressBadgeValue }) => (
  <Card className={`${Style.settingsContainer}`}>
    <Card.Header className={Style.settingsContainerHeader}>
      <span>{title}</span>
      {progressBadgeValue === true && (
        <div className={Style.payoutAllowedStatusMessage}>
          <FontAwesomeIcon icon={faCheck} className={Style.payoutIcon} />
          Eligible for Pipeline
        </div>
      )}
      {progressBadgeValue === false && (
        <div className={Style.payoutNotAllowedStatusMessage}>
          <FontAwesomeIcon icon={faTimes} className={Style.payoutIcon} />
          Missing Required Fields
        </div>
      )}
    </Card.Header>
    <Card.Body>{children}</Card.Body>
  </Card>
);
export default SettingsContainer;
