import { yearsOptions } from "../../../helpers/GeneralHelper";
import countriesData from "../../../static/data/countries.json";
import stateData from "../../../static/data/states.json";

const selectOption = { label: "Select one", value: "" };

export const UNITED_STATES = "United States";

export const formattedCollegeSizes = [
  selectOption,
  { value: "small", label: "Small (Less than 5,000 Students)" },
  { value: "medium", label: "Medium (5,000-15,000 Students)" },
  { value: "large", label: "Large (More than 15,000 Students)" }
];

export const formattedCollegeRegions = [
  selectOption,
  { value: "West", label: "West" },
  { value: "Southwest", label: "Southwest" },
  { value: "Southeast", label: "Southeast" },
  { value: "Northeast", label: "Northeast" },
  { value: "Midwest", label: "Midwest" }
];

export const formattedGraduationYears = yearsOptions(100, 8, "Select one", true);

export const hoursPlayed = ["0-1000", "1000-2500", "2500-5000", "5000+"];

export const requiredFieldsUSA = [
  "name",
  "graduationClass",
  "addressLine1",
  "city",
  "state",
  "zipCode",
  "country",
  "gradingScale",
  "highSchoolGPA",
  "highSchool",
  "primaryGame",
  "primaryGameUsername",
  "primaryGameHoursPlayed"
];

export const requiredFieldsInternational = [
  "name",
  "graduationClass",
  "addressLine1",
  "city",
  "state",
  "zipCode",
  "country",
  "gradingScale",
  "highSchoolGPA",
  "highSchool",
  "primaryGame",
  "primaryGameUsername",
  "primaryGameHoursPlayed"
];

export const requiredFieldsPretty = {
  name: "full name",
  graduationClass: "graduation class",
  addressLine1: "address",
  city: "city",
  state: "state",
  zipCode: "zip code",
  country: "country",
  gradingScale: "high school grading scale",
  highSchoolGPA: "high school GPA",
  highSchool: "high school",
  primaryGame: "primary game",
  primaryGameUsername: "linked stats for primary game",
  primaryGameHoursPlayed: "hours played for primary game"
};

export const highSchoolClubTeam = [
  { label: "Yes", value: "Yes" },
  { label: "No", value: "No" }
];

export const formattedGender = [
  selectOption,
  { label: "Man", value: "Man" },
  { label: "Woman", value: "Woman" },
  { label: "Non-Binary", value: "Non-Binary" },
  {
    label: "Prefer not to Disclose",
    value: "Prefer not to Disclose"
  }
];

export const formattedCountries = [selectOption, ...countriesData.map(it => ({ value: it, label: it }))];
export const formattedStates = [selectOption, ...stateData.map(state => ({ value: state, label: state }))];
