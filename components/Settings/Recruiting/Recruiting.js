import React, { useEffect, useState } from "react";
import { shallowEqual, useDispatch, useSelector } from "react-redux";
import ProgressBar from "react-bootstrap/ProgressBar";
import { Image } from "react-bootstrap";
import SettingsLayout from "../../Layouts/SettingsLayout";
import { getCurrentUser } from "../../../store/actions/common/userAuthActions";
import { getRecruitmentProfile, updateRecruitmentProfile } from "../../../store/actions/recruitingActions";
import SettingsContainer from "./SettingsContainer/SettingsContainer";
import { requiredFieldsUSA, requiredFieldsInternational, requiredFieldsPretty, UNITED_STATES } from "./dropdownData";
import Style from "./Recruiting.module.scss";
import RecruitInfo from "../../Recruiting/JoinThePipeline/RecruitInfo/RecruitInfo";
import AcademicInfo from "../../Recruiting/JoinThePipeline/AcademicInfo/AcademicInfo";
import GamingInfo from "../../Recruiting/JoinThePipeline/GamingInfo/GamingInfo";
import GameStats from "../../Recruiting/JoinThePipeline/GameStats/GameStats";
import HonorsAndEvents from "../../Recruiting/JoinThePipeline/HonorsAndEvents/HonorsAndEvents";
import PipelineSocialLinks from "../../Recruiting/JoinThePipeline/PipelineSocialLinks/PipelineSocialLinks";
import ClipsAndVideos from "../../Recruiting/JoinThePipeline/ClipsAndVideos/ClipsAndVideos";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";
import {
  getAimlabAuthStatusForLoggedUser,
  getValorantAuthStatusForLoggedUser
} from "../../../store/actions/externalAuth/externalAuthActions";

const RecruitingPage = () => {
  const dispatch = useDispatch();

  const existingProfile = useSelector(state => state.recruiting.profile);

  const [recruitingProfile, setRecruitingProfile] = useState(existingProfile);

  useEffect(() => {
    dispatch(getRecruitmentProfile());
    dispatch(getCurrentUser());
    dispatch(getValorantAuthStatusForLoggedUser());
    dispatch(getAimlabAuthStatusForLoggedUser());
    analytics.page("Recruiting Settings");
  }, []);

  useEffect(() => {
    setRecruitingProfile(existingProfile);
  }, [existingProfile]);

  const onChange = e => {
    setRecruitingProfile({
      ...recruitingProfile,
      [e.target.name]: e.target.value
    });
  };

  const onSubmit = () => {
    dispatch(updateRecruitmentProfile(recruitingProfile));
  };

  const initialQuestionnaireComplete = recruitingProfile?.graduationClass && recruitingProfile?.country;

  const requiredFields = existingProfile?.country === UNITED_STATES ? requiredFieldsUSA : requiredFieldsInternational;
  const percentComplete = Math.floor(
    (requiredFields.filter(it => existingProfile && existingProfile[it]).length / requiredFields.length) * 100
  );
  const missingFields = requiredFields.filter(it => !existingProfile || !existingProfile[it]);
  const isComplete = percentComplete === 100;

  const unsavedChanges = !shallowEqual(existingProfile, recruitingProfile);

  return (
    <SettingsLayout title="Recruiting Settings" customRightLayout>
      {initialQuestionnaireComplete ? (
        <>
          <SettingsContainer title="Welcome to the Pipeline!" progressBadgeValue={isComplete}>
            <div className={Style.welcomeMessage}>
              {isComplete ? (
                <b>
                  Your portfolio is now available to collegiate coaches. Soon, you will have access to exclusive
                  Pipeline Leaderboards. In the mean time, check out organizations, opportunities, and the lounge!
                  <br /> <br />
                  Return to this settings page any time to update your information.
                </b>
              ) : (
                <>
                  In order to be seen by college coaches, <b>you must fill out all of the required information </b>
                  on the Recruit Settings Page. <br /> <br />
                  The following missing fields are required:
                  {missingFields.map((field, index) => (
                    <b key={field}>
                      {` ${requiredFieldsPretty[field] || field}`}
                      {index < missingFields.length - 1 ? "," : "."}
                    </b>
                  ))}
                  <br />
                  Return to this settings page any time to update your information.
                </>
              )}

              {unsavedChanges && (
                <>
                  <br />
                  <br />
                  <b className={Style.unsavedChanges}>You have unsaved changes. Hit save to update your progress.</b>
                </>
              )}
            </div>
            <ProgressBar
              className={Style.progressBar}
              now={percentComplete}
              label={`${percentComplete}%`}
              variant="blue"
            />
          </SettingsContainer>

          <SettingsContainer title="Demographic Information">
            <RecruitInfo recruitingProfile={recruitingProfile} onChange={onChange} />
          </SettingsContainer>

          <SettingsContainer title="Academic Information">
            <AcademicInfo recruitingProfile={recruitingProfile} onChange={onChange} />
          </SettingsContainer>

          <SettingsContainer title="Gaming Information">
            <GamingInfo recruitingProfile={recruitingProfile} setRecruitingProfile={setRecruitingProfile} />
          </SettingsContainer>

          <SettingsContainer title="Game Stats">
            <GameStats recruitingProfile={recruitingProfile} onChange={onChange} />
          </SettingsContainer>

          <SettingsContainer title="Honors and Events">
            <HonorsAndEvents />
          </SettingsContainer>

          <SettingsContainer title="Social Platforms">
            <PipelineSocialLinks />
          </SettingsContainer>

          <SettingsContainer title="Clips and Videos">
            <ClipsAndVideos />
          </SettingsContainer>

          <EFRectangleButton text="Save" className={Style.saveButton} onClick={onSubmit} disabled={!unsavedChanges} />
        </>
      ) : (
        <>
          <div className={Style.welcomeContent}>
            <div>
              <p className={Style.welcome}>WELCOME TO</p>

              <Image
                className={Style.pipelineLogo}
                src="https://cdn.efuse.gg/uploads/pipeline/homepage/pipeline_logo.png"
              />

              <p className={Style.welcomeText}>
                <span>Where do you stack up?</span>
                <br />
                <span>Earn your way to the top of the rankings by increasing your skills, impact, & experience.</span>
              </p>
              <EFRectangleButton colorTheme="secondary" text="TAKE ME TO THE PIPELINE" internalHref="/pipeline" />
            </div>
          </div>
        </>
      )}
    </SettingsLayout>
  );
};

export default RecruitingPage;
