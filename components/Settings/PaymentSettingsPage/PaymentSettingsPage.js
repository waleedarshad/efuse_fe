import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import styled from "styled-components";

import SettingsCardLayout from "../SettingsCardLayout/SettingsCardLayout";
import SettingsLayout from "../../Layouts/SettingsLayout";
import { getCurrentUser } from "../../../store/actions/common/userAuthActions";
import PayoutAccountContainer from "./PayoutAccountContainer/PayoutAccountContainer";
import PaymentMethodsContainer from "./PaymentMethodsContainer/PaymentMethodsContainer";

const Payments = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getCurrentUser());
    analytics.page("Settings Payments");
  }, []);

  return (
    <SettingsLayout title="Payments">
      <SettingsCardLayout>
        <PaymentMethodsContainer />
      </SettingsCardLayout>
      <Spacer />
      <SettingsCardLayout>
        <PayoutAccountContainer />
      </SettingsCardLayout>
    </SettingsLayout>
  );
};

export default Payments;

const Spacer = styled.div`
  height: 10px;
`;
