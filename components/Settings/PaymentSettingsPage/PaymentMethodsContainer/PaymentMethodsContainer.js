import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Cards from "react-credit-cards";
import "react-credit-cards/es/styles-compiled.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/pro-solid-svg-icons";
import { useRouter } from "next/router";
import styled from "styled-components";

import {
  getPaymentMethods,
  removePaymentMethod,
  getPayoutAccount,
  verifyPayoutAccount
} from "../../../../store/actions/userActions";
import ConfirmAlert from "../../../ConfirmAlert/ConfirmAlert";
import DynamicModal from "../../../DynamicModal/DynamicModal";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";

const PaymentMethodsContainer = () => {
  const dispatch = useDispatch();
  const router = useRouter();
  const user = useSelector(state => state.auth.currentUser);
  const paymentMethods = useSelector(state => state.user.paymentMethods);

  useEffect(() => {
    if (user?._id) {
      dispatch(getPaymentMethods());

      if (router.query.code) {
        verifyPayoutAccount(router.query.code);
        // Next.js shallow routing allows us to silently remove the code URL param without reloading page
        router.push("/settings/payments", undefined, {
          shallow: true
        });
      } else {
        dispatch(getPayoutAccount());
      }
    }
  }, [user]);

  const removeCard = cardId => {
    dispatch(removePaymentMethod(cardId));
  };

  return (
    <>
      <PaymentMethodTitleContainer>
        <h4>Payment Methods</h4>
        <DynamicModal
          flow="AddCreditCard"
          openOnLoad={false}
          startView="AddCreditCard"
          displayCloseButton
          allowBackgroundClickClose={false}
        >
          <EFRectangleButton text="Add New Card" />
        </DynamicModal>
      </PaymentMethodTitleContainer>
      <p>
        Your credit and debit cards are stored securely on your eFuse account. You can add or removed saved cards at any
        time. These payment methods can be used later for applying to paid opportunities.
      </p>
      <CreditCardContainer>
        {!paymentMethods && <PaymentMethodMessage>Loading....</PaymentMethodMessage>}
        {paymentMethods?.length === 0 && <PaymentMethodMessage>No payment methods</PaymentMethodMessage>}
        {paymentMethods?.length > 0 &&
          paymentMethods.map(method => {
            return (
              <SingleCardContainer key={method.card_id}>
                <Cards
                  expiry={`0${method.card.exp_month}/${method.card.exp_year}`}
                  name={method.billing_details.name || " "}
                  number={`************${method.card.last4}`}
                  preview
                  issuer={method.brand}
                />

                <ConfirmAlert
                  title="Confirmation"
                  message="Are you sure you want to remove this card from your eFuse account?"
                  onYes={() => removeCard(method.id)}
                >
                  <RemoveButton>
                    <FontAwesomeIcon icon={faTimes} />
                  </RemoveButton>
                </ConfirmAlert>
              </SingleCardContainer>
            );
          })}
      </CreditCardContainer>
    </>
  );
};

export default PaymentMethodsContainer;

const PaymentMethodTitleContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 10px;
`;

const CreditCardContainer = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: center;
  margin: 15px 0;
  background-color: #f7fafc;
  margin-bottom: 10px;

  & > * {
    margin: 20px;
  }
`;

const PaymentMethodMessage = styled.div`
  margin: 100px auto;
  color: #848484;
`;

const SingleCardContainer = styled.div`
  position: relative;
  display: block;
`;

const RemoveButton = styled.div`
  background-color: red;
  border-radius: 200px;
  margin: auto;
  padding: 2px 10px;
  color: white;
  position: absolute;
  top: -10px;
  right: -10px;
  text-align: center;
  display: block;
  font-size: 1.1em;
  opacity: 0.8;
  height: 35px;
  width: 35px;
  line-height: 33px;

  &:hover {
    opacity: 1;
    cursor: pointer;
  }
`;
