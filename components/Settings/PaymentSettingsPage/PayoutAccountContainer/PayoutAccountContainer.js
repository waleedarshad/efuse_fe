import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useRouter } from "next/router";
import getConfig from "next/config";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck, faTimes } from "@fortawesome/pro-solid-svg-icons";
import styled from "styled-components";

import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";
import { getPayoutAccount, verifyPayoutAccount } from "../../../../store/actions/userActions";
import StripePayoutAccount from "../StripePayoutAccount/StripePayoutAccount";

const { publicRuntimeConfig } = getConfig();
const { stripeClientId } = publicRuntimeConfig;

const PayoutAccountContainer = () => {
  const dispatch = useDispatch();
  const router = useRouter();
  const user = useSelector(state => state.auth.currentUser);
  const payoutAccount = useSelector(state => state.user.payoutAccount);
  const dob = user?.dateOfBirth ? new Date(user.dateOfBirth) : null;

  let stripePayoutAccountUrl = `https://connect.stripe.com/express/oauth/authorize?client_id=${stripeClientId}`;
  stripePayoutAccountUrl += user?.email ? `&stripe_user[email]=${user.email}` : "";
  stripePayoutAccountUrl += user?.name ? `&stripe_user[first_name]=${user.name}` : "";
  stripePayoutAccountUrl += user?.lastName ? `&stripe_user[last_name]=${user.lastName}` : "";
  stripePayoutAccountUrl += dob
    ? `&stripe_user[dob_day]=${dob.getDate()}&stripe_user[dob_month]=${dob.getMonth() +
        1}&stripe_user[dob_year]=${dob.getFullYear()}`
    : "";
  stripePayoutAccountUrl += "&tos_acceptance[service_agreement]=recipient";

  useEffect(() => {
    if (user?._id) {
      if (router.query.code) {
        dispatch(verifyPayoutAccount(router.query.code));
        // Next.js shallow routing allows us to silently remove the code URL param without reloading page
        router.push("/settings/payments", undefined, {
          shallow: true
        });
      } else {
        dispatch(getPayoutAccount());
      }
    }
  }, [user]);

  return (
    <>
      <PaymentMethodTitleContainer>
        <h4>Payouts</h4>

        {!payoutAccount && <EFRectangleButton text="Set up Payout Account" externalHref={stripePayoutAccountUrl} />}
        {payoutAccount && payoutAccount.payout_settings.enabled && (
          <PayoutAllowedStatusMessage>
            <PayoutIcon>
              <FontAwesomeIcon icon={faCheck} />
            </PayoutIcon>
            You are eligible to receive payouts
          </PayoutAllowedStatusMessage>
        )}
        {payoutAccount && !payoutAccount.payout_settings.enabled && (
          <PayoutNotAllowedStatusMessage>
            <PayoutIcon>
              <FontAwesomeIcon icon={faTimes} />
            </PayoutIcon>
            You are not eligible to receive payouts
          </PayoutNotAllowedStatusMessage>
        )}
      </PaymentMethodTitleContainer>
      <p>
        Set up a payout account to accept and transfer tournament and scholarship winnings to your bank account. eFuse
        uses Stripe, a secure payment platform, to store and transfer funds to your bank account.
      </p>
      <CreditCardContainer>
        {!payoutAccount && <PaymentMethodMessage>Payout account has not been set up.</PaymentMethodMessage>}
        {payoutAccount && <StripePayoutAccount payoutAccount={payoutAccount} />}
      </CreditCardContainer>
    </>
  );
};

export default PayoutAccountContainer;

const CreditCardContainer = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: center;
  margin: 15px 0;
  background-color: #f7fafc;
  margin-bottom: 10px;

  & > * {
    margin: 20px;
  }
`;

const PaymentMethodMessage = styled.div`
  margin: 100px auto;
  color: #848484;
`;
const PayoutIcon = styled.span`
  margin-right: 10px;
`;

const PayoutAllowedStatusMessage = styled.div`
  background-color: #e0efda;
  color: #4d864d;
  border-radius: 3px;
  padding: 8px 12px;
`;
const PayoutNotAllowedStatusMessage = styled.div`
  background-color: #fee5e2;
  color: #cd4d57;
  border-radius: 3px;
  padding: 8px 12px;
`;

const PaymentMethodTitleContainer = styled.div`
  margin-bottom: 10px;
  margin-top: 15px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;
