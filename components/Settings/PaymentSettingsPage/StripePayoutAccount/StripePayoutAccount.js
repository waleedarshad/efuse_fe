import React from "react";
import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCreditCard, faUniversity } from "@fortawesome/pro-solid-svg-icons";

import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";

const StripePayoutAccount = ({ payoutAccount }) => {
  const availableBalance = (payoutAccount.balance.available[0].amount / 100).toFixed(2);
  const currency = payoutAccount.balance.available[0].currency.toUpperCase();
  const pendingBalance = (payoutAccount.balance.pending[0].amount / 100).toFixed(2);
  const hasPendingBalance = payoutAccount.balance.pending[0].amount > 0;
  const { interval } = payoutAccount.payout_settings.schedule;
  const delay = payoutAccount.payout_settings.schedule.delay_days;
  const hasPayoutMethod = payoutAccount.payout_settings.external_account.length > 0;
  let payoutMethod;

  if (hasPayoutMethod) [payoutMethod] = payoutAccount.payout_settings.external_account;

  return (
    <PayoutPreviewContainer>
      <BalanceMessage>
        Your balance is ${availableBalance} {currency}
      </BalanceMessage>
      {hasPendingBalance && (
        <PayoutFrequencyMessage>
          ${pendingBalance} {currency} pending
        </PayoutFrequencyMessage>
      )}
      <PayoutFrequencyMessage>
        You’ll receive payouts {interval} on a {delay} day rolling basis.
      </PayoutFrequencyMessage>
      <PayoutMethodMessage>
        {hasPayoutMethod ? (
          <>
            <PayoutIcon>
              <FontAwesomeIcon icon={payoutMethod.object === "card" ? faCreditCard : faUniversity} />
            </PayoutIcon>
            {payoutMethod.object === "card" ? payoutMethod.brand : payoutMethod.bank_name} x{payoutMethod.last4}
          </>
        ) : (
          "No bank or debit card found"
        )}
      </PayoutMethodMessage>
      <br />
      <EFRectangleButton text="View Payouts" externalHref={payoutAccount.link} />
    </PayoutPreviewContainer>
  );
};

export default StripePayoutAccount;

const BalanceMessage = styled.div`
  color: rgb(26, 31, 54);
  font-size: 20px;
`;

const PayoutPreviewContainer = styled.div`
  padding: 50px 0;
  text-align: center;
  color: #848484;
`;

const PayoutFrequencyMessage = styled.div`
  color: rgb(60, 66, 87);
  font-size: 14px;
`;

const PayoutMethodMessage = styled.div`
  display: inline-block;
  color: rgb(60, 66, 87);
  border-radius: 3px;
  background-color: #e3e8ee;
  padding: 8px 12px;
  margin: 20px auto;
`;

const PayoutIcon = styled.span`
  margin-right: 10px;
`;
