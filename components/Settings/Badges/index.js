import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import SettingsLayout from "../../Layouts/SettingsLayout";
import { getCurrentUser } from "../../../store/actions/common/userAuthActions";
import EFExternalEfuseBadge from "../../EFExternalEfuseBadge/EFExternalEfuseBadge";

const Badges = () => {
  const dispatch = useDispatch();
  const user = useSelector(state => state.user.currentUser);

  useEffect(() => {
    dispatch(getCurrentUser());
    analytics.page("Settings Badge");
  }, []);

  let imageUrl = "";
  let linkUrl = "";
  if (user._id) {
    imageUrl = `https://cdn.efuse.gg/uploads/static/badges/efuse_${user.verified ? "verified_" : ""}portfolio.png`;
    linkUrl = `https://efuse.gg/u/${user.username}`;
  }

  return (
    <SettingsLayout title="Portfolio Badge">
      <EFExternalEfuseBadge
        id={user._id}
        imageUrl={imageUrl}
        linkUrl={linkUrl}
        badgeType="portfolio"
        altText="eFuse Portfolio Badge"
      />
    </SettingsLayout>
  );
};

export default Badges;
