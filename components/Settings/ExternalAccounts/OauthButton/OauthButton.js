import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUnlink } from "@fortawesome/pro-solid-svg-icons";
import PropTypes from "prop-types";
import { withCdn } from "../../../../common/utils";
import Styles from "./OauthButton.module.scss";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";

const OauthButton = ({ onClick, styleClass, img, isVerified, iconClass, isSignup }) => (
  <EFRectangleButton
    onClick={onClick}
    className={`${Styles.ButtonContent} ${styleClass} ${isSignup && Styles.isSignup}`}
    text={
      <>
        {img && <img src={withCdn(`/static/images/external_accounts/${img}.png`)} alt={`${img} logo`} />}
        {isVerified && <FontAwesomeIcon className={iconClass} icon={faUnlink} title="Unlink" />}{" "}
      </>
    }
    width="fullWidth"
    size="extraLarge"
  />
);

OauthButton.propTypes = {
  onClick: PropTypes.func.isRequired,
  styleClass: PropTypes.string.isRequired,
  img: PropTypes.string.isRequired,
  iconClass: PropTypes.string.isRequired
};

export default OauthButton;
