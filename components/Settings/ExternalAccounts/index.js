import React, { useEffect } from "react";
import intersection from "lodash/intersection";
import { connect } from "react-redux";
import { withRouter } from "next/router";
import {
  removeDiscordWebhook,
  removeFacebookPage,
  updateUserSimple,
  alterUserFields
} from "../../../store/actions/userActions";
import { getCurrentUser } from "../../../store/actions/common/userAuthActions";
import SettingsLayout from "../../Layouts/SettingsLayout";
import ExternalAccountLinks from "./ExternalAccountLinks/ExternalAccountLinks";
import { sendNotification } from "../../../helpers/FlashHelper";
import DiscordHook from "./Hooks/DiscordHook/DiscordHook";
import FacebookPageHook from "./Hooks/FacebookPageHook/FacebookPageHook";
import {
  getAimlabAuthStatusForLoggedUser,
  getValorantAuthStatusForLoggedUser
} from "../../../store/actions/externalAuth/externalAuthActions";

const SERVICES = [
  "facebook",
  "twitch",
  "discord",
  "steam",
  "linkedin",
  "battlenet",
  "twitter",
  "xboxlive",
  "snapchat",
  "google",
  "statespace",
  "riot"
];

const ExternalAccounts = props => {
  useEffect(() => {
    props.getCurrentUser();
    manualAuthCallback();
  }, []);

  const manualAuthCallback = () => {
    window.onmessage = e => {
      const common = intersection(Object.keys(e.data), SERVICES);
      if (common.length === 1) {
        authNotification(e.data.status, common[0]);
      }
    };
  };

  const trackService = service => {
    const { onboardingFlow, portfolioView } = props;
    if (onboardingFlow) {
      analytics.track(`EXTERNAL_ACCOUNT_LINKED_${service.toUpperCase()}`, {
        location: "onboarding"
      });
    }

    if (portfolioView) {
      analytics.track(`EXTERNAL_ACCOUNT_LINKED_${service.toUpperCase()}`, {
        location: "portfolio"
      });
    }

    if (!(portfolioView || onboardingFlow)) {
      analytics.track(`EXTERNAL_ACCOUNT_LINKED_${service.toUpperCase()}`);
    }
  };

  const updatedUserAndFlash = (serviceParam, title) => {
    let service = serviceParam;

    if (service === "xboxlive") {
      service = "xbox";
    }

    props.alterUserFields({ [`${service}Verified`]: true });

    if (service === "riot") {
      props.alterUserFields({ [service]: true });
      props.getValorantAuthStatusForLoggedUser();
    }

    if (service === "statespace") {
      props.alterUserFields({ [service]: true });
      props.getAimlabAuthStatusForLoggedUser();
    }

    if (props.autoUpdate) setTimeout(() => props.getCurrentUser(), 2000);

    sendNotification("Successfully verified!", "success", title);
  };

  const authNotification = (status, service) => {
    const title = service.toLocaleUpperCase();
    if (status === "success") {
      trackService(status, service);
      updatedUserAndFlash(service, title);
    } else {
      sendNotification("Can not be verified, please try again.", "danger", title);
    }
  };

  const {
    onlyShowLinks,
    displaySteam,
    displayBattlenet,
    displayLinkedin,
    displayTwitter,
    displayDiscord,
    displayFacebook,
    displayTwitch,
    displayGoogle,
    displayAimLab,
    displayValorant,
    children,
    avoidGrid,
    displaySnapchat,
    autoUpdate,
    xboxLiveAuthBtn,
    removeDiscordWebhook: onRemoveDiscordWebhook,
    removeFacebookPage: onRemoveFacebookPage
  } = props;

  const ExternalLinks = ({ xboxLiveAuthBtn: xboxBtn }) => (
    <ExternalAccountLinks
      displaySteam={displaySteam}
      displayBattlenet={displayBattlenet}
      displayLinkedin={displayLinkedin}
      displayTwitter={displayTwitter}
      displayDiscord={displayDiscord}
      displayFacebook={displayFacebook}
      displayTwitch={displayTwitch}
      displayGoogle={displayGoogle}
      displaySnapchat={displaySnapchat}
      avoidGrid={avoidGrid}
      autoUpdate={autoUpdate}
      authNotification={authNotification}
      xboxLiveAuthBtn={xboxBtn}
      displayAimLab={displayAimLab}
      displayValorant={displayValorant}
    >
      {children}
    </ExternalAccountLinks>
  );

  // used to not render the full settings view
  if (onlyShowLinks) {
    return <ExternalLinks />;
  }

  return (
    <SettingsLayout title="External Accounts">
      <ExternalLinks xboxLiveAuthBtn={xboxLiveAuthBtn} />
      <DiscordHook user={props.user} onRemoveDiscordWebhook={onRemoveDiscordWebhook} />
      <FacebookPageHook user={props.user} onRemoveFacebookPage={onRemoveFacebookPage} />
    </SettingsLayout>
  );
};

const mapStateToProps = state => ({
  user: state.user.currentUser,
  modal: state.portfolio.modal,
  modalSection: state.portfolio.modalSection,
  xboxLiveAuthBtn: state.features.xbox_live_auth_btn
});

export default connect(mapStateToProps, {
  getCurrentUser,
  updateUserSimple,
  removeDiscordWebhook,
  removeFacebookPage,
  alterUserFields,
  getAimlabAuthStatusForLoggedUser,
  getValorantAuthStatusForLoggedUser
})(withRouter(ExternalAccounts));
