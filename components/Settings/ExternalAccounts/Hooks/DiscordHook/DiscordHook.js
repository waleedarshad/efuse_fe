import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus, faTrash } from "@fortawesome/pro-solid-svg-icons";
import Styles from "../../index.module.scss";
import InfoMessage from "../../../../InfoMessage/InfoMessage";
import AddDiscordWebhook from "../../../../GlobalModals/AddDiscordWebhook/AddDiscordWebhook";
import ConfirmAlert from "../../../../ConfirmAlert/ConfirmAlert";

const DiscordHook = ({ user, onRemoveDiscordWebhook }) =>
  user?.discordVerified ? (
    <div className={Styles.externalCrossPost}>
      <p className={Styles.title}>{user?.discordWebhooks?.length > 0 ? "Discord Webhooks" : "Add a Discord Webhook"}</p>
      <InfoMessage text="This will allow you to share a post to your discord server." />
      {!user?.discordWebhooks?.length && (
        <AddDiscordWebhook>
          <div className={Styles.addCrossPost}>
            <FontAwesomeIcon className={Styles.add} icon={faPlus} />
          </div>
        </AddDiscordWebhook>
      )}
      {user?.discordWebhooks &&
        user?.discordWebhooks.map(value => (
          <div className={Styles.webhookList}>
            <p className={Styles.name}>{value.name}</p>
            <hr />
            <p className={Styles.url}>
              Webhook: <b>{value.url}</b>
            </p>
            <ConfirmAlert
              title="Remove Discord Webhook?"
              message="Are you sure you would like to remove your discord webhook?"
              onYes={() => onRemoveDiscordWebhook(value._id)}
            >
              <FontAwesomeIcon className={Styles.trash} icon={faTrash} />
            </ConfirmAlert>
          </div>
        ))}
    </div>
  ) : (
    <></>
  );

export default DiscordHook;
