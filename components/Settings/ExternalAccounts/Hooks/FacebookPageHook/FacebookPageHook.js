import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus, faTrash } from "@fortawesome/pro-solid-svg-icons";
import Styles from "../../index.module.scss";
import InfoMessage from "../../../../InfoMessage/InfoMessage";

import ConfirmAlert from "../../../../ConfirmAlert/ConfirmAlert";
import AddFacebookPage from "../../../../GlobalModals/AddFacebookPage/AddFacebookPage";

const FacebookPageHook = ({ user, onRemoveFacebookPage }) =>
  user?.facebookVerified ? (
    <div className={Styles.externalCrossPost}>
      <p className={Styles.title}>Facebook Pages</p>
      <InfoMessage
        text="A Facebook page must be added in order to share a post to
          Facebook."
      />
      {user?.facebookPages?.length < 1 && (
        <AddFacebookPage>
          <div className={Styles.addCrossPost}>
            <FontAwesomeIcon className={Styles.add} icon={faPlus} />
          </div>
        </AddFacebookPage>
      )}
      {user?.facebookPages &&
        user?.facebookPages.map(value => (
          <div className={Styles.webhookList}>
            <p className={Styles.name}>{value.pageName}</p>
            <ConfirmAlert
              title="Remove Facebook Page?"
              message="Are you sure you would like to remove your facebook page?"
              onYes={() => onRemoveFacebookPage(value._id)}
            >
              <FontAwesomeIcon className={Styles.trash} icon={faTrash} />
            </ConfirmAlert>
          </div>
        ))}
    </div>
  ) : (
    <></>
  );

export default FacebookPageHook;
