import React from "react";
import Styles from "../index.module.scss";
import AuthenticateDiscord from "../../../AuthenticateAccounts/AuthenticateDiscord/AuthenticateDiscord";
import AuthenticateTwitch from "../../../AuthenticateAccounts/AuthenticateTwitch/AuthenticateTwitch";
import AuthenticateSteam from "../../../AuthenticateAccounts/AuthenticateSteam/AuthenticateSteam";
import AuthenticateTwitter from "../../../AuthenticateAccounts/AuthenticateTwitter/AuthenticateTwitter";
import AuthenticateBattlenet from "../../../AuthenticateAccounts/AuthenticateBettlenet/AuthenticateBattlenet";
import AuthenticateLinkedin from "../../../AuthenticateAccounts/AuthenticateLinkedin/AuthenticateLinkedin";
import AuthenticateSnapchat from "../../../AuthenticateAccounts/AuthenticateSnapchat/AuthenticateSnapchat";
import AuthenticateXboxLive from "../../../AuthenticateAccounts/AuthenticateXboxLive/AuthenticateXboxLive";
import AuthenticateYoutube from "../../../AuthenticateAccounts/AuthenticateYoutube/AuthenticateYoutube";
import AuthenticateFacebook from "../../../AuthenticateAccounts/AuthenticateFacebook/AuthenticateFacebook";
import AuthenticateAimLab from "../../../AuthenticateAccounts/AuthenticateAimLab/AuthenticateAimLab";
import AuthenticateValorant from "../../../AuthenticateAccounts/AuthenticateValorant/AuthenticateValorant";

const ExternalAccountLinks = props => {
  const {
    displaySteam,
    displayBattlenet,
    displayLinkedin,
    displayTwitter,
    displayDiscord,
    displayFacebook,
    displayTwitch,
    displayGoogle,
    children,
    avoidGrid,
    xboxLiveAuthBtn,
    displaySnapchat,
    authNotification,
    displayAimLab,
    displayValorant
  } = props;

  return (
    <div className={!avoidGrid ? Styles.externalAccountsGrid : ""}>
      {displayDiscord && <AuthenticateDiscord>{children}</AuthenticateDiscord>}
      {displayTwitch && <AuthenticateTwitch>{children}</AuthenticateTwitch>}
      {displaySteam && <AuthenticateSteam>{children}</AuthenticateSteam>}
      {displayLinkedin && <AuthenticateLinkedin>{children}</AuthenticateLinkedin>}
      {displayBattlenet && <AuthenticateBattlenet>{children}</AuthenticateBattlenet>}
      {displayTwitter && <AuthenticateTwitter>{children}</AuthenticateTwitter>}

      {displayFacebook && <AuthenticateFacebook authNotification={authNotification}>{children}</AuthenticateFacebook>}

      {xboxLiveAuthBtn && <AuthenticateXboxLive>{children}</AuthenticateXboxLive>}
      {displaySnapchat && <AuthenticateSnapchat>{children}</AuthenticateSnapchat>}
      {displayGoogle && <AuthenticateYoutube>{children}</AuthenticateYoutube>}
      {displayAimLab && <AuthenticateAimLab>{children}</AuthenticateAimLab>}
      {displayValorant && <AuthenticateValorant>{children}</AuthenticateValorant>}
    </div>
  );
};

ExternalAccountLinks.defaultProps = {
  displaySteam: true,
  displayBattlenet: true,
  displayLinkedin: true,
  displayTwitter: true,
  displayDiscord: true,
  displayFacebook: true,
  displayTwitch: true,
  displayGoogle: true,
  displaySnapchat: true,
  xboxLiveAuthBtn: false,
  displayAimLab: true,
  displayValorant: true
};

export default ExternalAccountLinks;
