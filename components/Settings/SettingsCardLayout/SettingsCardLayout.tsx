import React, { ReactNode } from "react";
import styled from "styled-components";
import EFCard from "../../Cards/EFCard/EFCard";

const CardWrapper = styled.div`
  padding: 20px;
`;

const Title = styled.h2`
  margin: 0;
  font-family: Poppins, sans-serif;
  font-style: normal;
  font-weight: 600;
  font-size: 14px;
  line-height: 20px;
  color: #0a0c12;
  padding-bottom: 5px;
`;

const Content = styled.div`
  width: 100%;
`;

interface SettingsCardLayoutProps {
  title: string;
  children: ReactNode;
}

const SettingsCardLayout = ({ title, children }: SettingsCardLayoutProps) => {
  return (
    <EFCard widthTheme="fullWidth" shadow="none" overflowTheme="visible">
      <CardWrapper>
        <Title>{title}</Title>
        <Content>{children}</Content>
      </CardWrapper>
    </EFCard>
  );
};

export default SettingsCardLayout;
