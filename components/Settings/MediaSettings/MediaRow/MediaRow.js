import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import Style from "./MediaRow.module.scss";

const MediaRow = ({ icon, label, children }) => {
  return (
    <div className={`row pt-3 ${Style.wrapper} align-items-center`}>
      <div className="col-sm-5">
        <div className="row">
          <div className="col-2">
            <FontAwesomeIcon icon={icon} size="lg" className={Style.icon} />
          </div>
          <div className={`col-10 ${Style.label}`}>{label}</div>
        </div>
      </div>
      <div className={`col-sm-7 ${Style.children}`}>{children}</div>
    </div>
  );
};

export default MediaRow;
