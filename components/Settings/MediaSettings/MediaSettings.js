import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { faCameraMovie, faPlayCircle, faVolumeSlash } from "@fortawesome/pro-solid-svg-icons";

import SettingsLayout from "../../Layouts/SettingsLayout/index";
import SelectBox from "../../SelectBox/SelectBox";
import Switch from "../../Switch/Switch";
import MediaRow from "./MediaRow/MediaRow";
import { getMediaSettings, updateMediaSettings } from "../../../store/actions/userSettingActions";

const MEDIA_PROFILES = [
  { label: "Standard Definition", value: "sd" },
  { label: "High Definition", value: "hd" },
  { label: "Ultra High Definition", value: "uhd" }
];

const MediaSettings = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getMediaSettings());
  }, []);

  const [mediaState, setMediaState] = useState({ profile: "hd", autoplay: true, mute: true });
  const userMediaSettings = useSelector(state => state.user.mediaSettings);

  useEffect(() => {
    if (userMediaSettings) {
      setMediaState({
        profile: userMediaSettings.profile,
        autoplay: userMediaSettings.autoplay,
        mute: userMediaSettings.mute
      });
    }
  }, [userMediaSettings]);

  const onChange = event => {
    const { target } = event;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const settingsToBeUpdated = { ...mediaState, [target.name]: value };

    setMediaState(settingsToBeUpdated);
    dispatch(updateMediaSettings(settingsToBeUpdated));
  };

  return (
    <SettingsLayout title="Media Settings">
      <MediaRow icon={faCameraMovie} label="Desired Media Quality">
        <SelectBox
          disabled={false}
          name="profile"
          options={MEDIA_PROFILES}
          validated={false}
          theme="whiteShadow"
          value={mediaState.profile}
          required
          onChange={onChange}
        />
      </MediaRow>
      <MediaRow icon={faPlayCircle} label="Autoplay Video">
        <Switch name="autoplay" value={mediaState.autoplay} checked={mediaState.autoplay} onChange={onChange} />
      </MediaRow>
      <MediaRow icon={faVolumeSlash} label="Mute Video">
        <Switch name="mute" value={mediaState.mute} checked={mediaState.mute} onChange={onChange} />
      </MediaRow>
    </SettingsLayout>
  );
};

export default MediaSettings;
