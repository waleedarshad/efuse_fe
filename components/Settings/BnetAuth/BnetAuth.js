import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "next/router";
import { Row, Col } from "react-bootstrap";
import SettingsLayout from "../../Layouts/SettingsLayout";
import { setUserFromBnet } from "../../../store/actions/userActions";

class BnetAuth extends Component {
  componentDidMount() {
    this.verifyBnetAccount();
    analytics.page("Settings Battle Net");
  }

  verifyBnetAccount = () => {
    const { token } = this.props.router.query;
    if (token === undefined) {
      this.callback("failure");
    } else {
      this.props.setUserFromBnet("success", this.callback);
    }
  };

  callback = response => {
    window.opener.parent.postMessage({ battlenet: true, status: response }, "*");
    window.close();
  };

  render() {
    return (
      <SettingsLayout title="External Account Settings">
        <Row>
          <Col sm={12}>
            <h4>Verifying Battlenet Account ...</h4>
          </Col>
        </Row>
      </SettingsLayout>
    );
  }
}

export default connect(null, { setUserFromBnet })(withRouter(BnetAuth));
