import { useEffect } from "react";
import { useRouter } from "next/router";

const YoutubeAuth = () => {
  const router = useRouter();
  const { query } = router;

  const callback = (status = "success") => {
    if (window.opener) {
      window.opener?.parent?.postMessage({ google: true, status }, "*");
    }
    window.close();
  };

  useEffect(() => {
    if (query.token) {
      return callback("success");
    }
    callback("failure");
  }, []);
  return <div>Authenticating...</div>;
};

export default YoutubeAuth;
