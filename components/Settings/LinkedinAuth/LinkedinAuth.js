import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "next/router";
import { Row, Col } from "react-bootstrap";

import SettingsLayout from "../../Layouts/SettingsLayout";
import { setUserFromLinkedin } from "../../../store/actions/userActions";

class LinkedinAuth extends Component {
  componentDidMount() {
    this.verifyLinkedinAccount();
    analytics.page("Settings Linkedin");
  }

  verifyLinkedinAccount = () => {
    const { token } = this.props.router.query;
    if (token === undefined) {
      this.callback("failure");
    } else {
      this.props.setUserFromLinkedin("success", this.callback);
    }
  };

  callback = response => {
    window.opener.parent.postMessage({ linkedin: true, status: response }, "*");
    window.close();
  };

  render() {
    return (
      <SettingsLayout title="External Account Settings">
        <Row>
          <Col sm={12}>
            <h4>Verifying Linkedin Account ...</h4>
          </Col>
        </Row>
      </SettingsLayout>
    );
  }
}

export default connect(null, { setUserFromLinkedin })(withRouter(LinkedinAuth));
