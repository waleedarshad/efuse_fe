import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";
import EFAlert from "../../../EFAlert/EFAlert";
import Style from "./AliasName.module.scss";

const AliasName = ({ user, onAlias }) => {
  const alertMessage =
    "Aliasing your name will help protect your real identity while using eFuse. We will replace your real name with an anonymous name. You can add your real name back at any time. Would you like to continue?";

  return (
    <div className={Style.nameContainer}>
      <div className={Style.aliasButton}>
        {!user.name?.includes("Anonymous") && (
          <EFRectangleButton
            text="Alias my name to protect my identity"
            colorTheme="transparent"
            shadowTheme="none"
            size="small"
            fontWeightTheme="normal"
            onClick={() => EFAlert(null, alertMessage, "Yes", "No", onAlias)}
          />
        )}
      </div>
    </div>
  );
};

export default AliasName;
