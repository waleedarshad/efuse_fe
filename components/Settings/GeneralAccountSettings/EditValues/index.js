import React from "react";
import Form from "react-bootstrap/Form";
import { Col } from "react-bootstrap";
import CharacterCounter from "react-character-counter";

import { rescueNil } from "../../../../helpers/GeneralHelper";
import InputRow from "../../../InputRow/InputRow";
import InputField from "../../../InputField/InputField";
import InputLabel from "../../../InputLabel/InputLabel";
import Style from "./index.module.scss";
import DatePickerInput from "../../../DatePickerInput/DatePickerInput";
import DatepickerMonthElement from "../../../DatepickerMonthElement/DatepickerMonthElement";
import GoogleAutoComplete from "../../../GoogleAutocomplete/GoogleAutocomplete";
import ImageUploader from "../../../ImageUploader/ImageUploader";
import EFGenderInput from "../../../EFGenderInput/EFGenderInput";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";
import DeleteAccount from "../DeleteAccount/DeleteAccount";
import ChangePassword from "./ChangePassword/ChangePassword";

const EditValues = props => {
  const { user, onDrop, onCropChange, toggleCropperModal, onRemoveProfileImage, onRemoveHeaderImage } = props;

  return (
    <Form noValidate validated={props.validated} role="form" onSubmit={props.onSubmit}>
      <InputRow>
        <Col sm={6}>
          <InputLabel theme="darkColor">Display Name</InputLabel>
          <InputField
            size="lg"
            type="text"
            placeholder="Display Name"
            name="name"
            value={user.name}
            onChange={props.onChange}
            required
            errorMessage="Display name is required"
            theme="whiteShadow"
          />
        </Col>
        <Col sm={6}>
          <InputLabel theme="darkColor">Username</InputLabel>
          <InputField
            size="lg"
            type="text"
            placeholder="Username"
            name="username"
            value={user.username}
            onChange={props.onUserNameChange}
            required
            errorMessage="Username is required"
            theme="whiteShadow"
          />
        </Col>
      </InputRow>
      <InputRow>
        <Col sm={6}>
          <InputLabel theme="darkColor">Email Address</InputLabel>
          <InputField
            size="lg"
            type="email"
            placeholder="Email Address"
            name="email"
            value={user.email}
            onChange={props.onChange}
            required
            errorMessage=""
            onBlur={props.onBlur}
            theme="whiteShadow"
          />
        </Col>
        <Col sm={6}>
          <InputLabel theme="darkColor">Date Of Birth</InputLabel>
          <DatePickerInput
            date={user.dateOfBirth}
            direction="down"
            dateHandler={props.onDateChange}
            isOutsideRange={() => false}
            renderMonthElement={DatepickerMonthElement}
            numberOfMonths={1}
            invalid={props.underAge}
            errorMessage="You must be 13 years or older to use eFuse"
          />
        </Col>
      </InputRow>
      <InputRow>
        <Col sm={6}>
          <InputLabel theme="darkColor">Location</InputLabel>
          <GoogleAutoComplete
            onSuggest={props.onSuggestSelect}
            placeholder="Search City"
            className="geoselect-internal"
            initialValue={user.address}
          />
        </Col>
        <Col sm={6}>
          <InputLabel theme="darkColor">I identify my gender as…</InputLabel>
          <EFGenderInput value={user.gender} onChange={props.onChange} />
        </Col>
      </InputRow>
      <InputRow>
        <Col>
          <CharacterCounter value={user.bio ? user.bio : "0"} maxLength={250}>
            <InputField
              as="textarea"
              name="bio"
              placeholder="Bio"
              value={user.bio}
              onChange={props.onChange}
              rows={4}
              maxLength={250}
              theme="whiteShadow"
            />
          </CharacterCounter>
        </Col>
      </InputRow>
      <InputRow>
        <Col sm={6} className="m-auto">
          <InputLabel theme="darkColor">Profile Picture</InputLabel>
          <ImageUploader
            text="Upload a Profile Picture"
            name="profilePicture"
            onDrop={onDrop}
            onRemoveImage={onRemoveProfileImage}
            allowCross
            theme="lightButton"
            value={rescueNil(user.profilePicture, "url")}
            onCropChange={onCropChange}
            toggleCropperModal={toggleCropperModal}
            aspectRatioWidth={160}
            aspectRatioHeight={160}
            showCropper
            showImageDescription
          />
        </Col>
        <Col sm={6} className="m-auto">
          <InputLabel theme="darkColor">Header Image</InputLabel>
          <ImageUploader
            text="Upload a Header Image"
            name="headerImage"
            onDrop={onDrop}
            onRemoveImage={onRemoveHeaderImage}
            allowCross
            theme="lightButton"
            value={rescueNil(user.headerImage, "url")}
            onCropChange={onCropChange}
            toggleCropperModal={toggleCropperModal}
            aspectRatioWidth={180}
            aspectRatioHeight={50}
            showCropper
            showImageDescription
          />
        </Col>
      </InputRow>
      <div className={Style.buttonsWrapper}>
        <div>
          <div className={`${Style.settingButton} mr-3`}>
            <DeleteAccount />
          </div>
          <div className={Style.settingButton}>
            <ChangePassword />
          </div>
        </div>
        <div className={Style.settingButton}>
          <EFRectangleButton text="Save" buttonType="submit" />
        </div>
      </div>
    </Form>
  );
};

export default EditValues;
