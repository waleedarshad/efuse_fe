import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { Col, Form } from "react-bootstrap";

import EFRectangleButton from "../../../../Buttons/EFRectangleButton/EFRectangleButton";
import InputField from "../../../../InputField/InputField";
import InputLabel from "../../../../InputLabel/InputLabel";
import InputRow from "../../../../InputRow/InputRow";
import PasswordStrength from "../../../../PasswordStrength/PasswordStrength";
import { updatePassword } from "../../../../../store/actions/userActions";
import Style from "../index.module.scss";

const ChangePasswordTemplate = ({ closeModal }) => {
  const dispatch = useDispatch();

  const [currentPassword, setCurrentPassword] = useState("");
  const [newPassword, setNewPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [validPassword, setValidPassword] = useState(true);
  const [validated, setValidated] = useState(false);
  const [passwordMisMatch, setPasswordMisMatch] = useState(false);

  const handlePasswordChange = (password: string, isValid: boolean) => {
    setValidPassword(isValid);
    setNewPassword(password);
  };

  const matchPassword = (password, password2) => {
    if (password !== password2) {
      setPasswordMisMatch(true);
      return true;
    }
    setPasswordMisMatch(false);
    return false;
  };

  const onSubmit = event => {
    const mismatch = matchPassword(newPassword, confirmPassword);
    setValidated(true);
    if (event.currentTarget.checkValidity() && validPassword && !mismatch) {
      dispatch(updatePassword({ currentPassword, newPassword }, closeModal));
    }
  };

  const isButtonEnabled = validPassword && currentPassword && confirmPassword;

  return (
    <Form noValidate validated={validated} role="form">
      <InputRow>
        <Col>
          <InputLabel theme="darkColor">Current Password</InputLabel>
          <InputField
            size="lg"
            type="password"
            placeholder="Current Password"
            name="name"
            value={currentPassword}
            onChange={e => setCurrentPassword(e.target.value)}
            required
            errorMessage="Current password is required"
            theme="whiteShadow"
          />
        </Col>
      </InputRow>
      <InputRow>
        <Col className={Style.passwordInput}>
          <InputLabel theme="darkColor">New Password</InputLabel>
          <PasswordStrength
            placeholder="Password"
            changeCallback={handlePasswordChange}
            validPassword={validPassword}
            dark
          />
        </Col>
      </InputRow>
      <InputRow>
        <Col>
          <InputLabel theme="darkColor">Confirm Password</InputLabel>
          <InputField
            size="lg"
            type="password"
            placeholder="Confirm Password"
            name="confirmPassword"
            value={confirmPassword}
            onChange={e => {
              setPasswordMisMatch(false);
              setConfirmPassword(e.target.value);
            }}
            required
            errorMessage={passwordMisMatch ? "The passwords do not match" : "Confirm password is required"}
            theme="whiteShadow"
            isInvalid={passwordMisMatch}
          />
        </Col>
      </InputRow>
      <EFRectangleButton text="Change Password" onClick={event => onSubmit(event)} disabled={!isButtonEnabled} />
    </Form>
  );
};
export default ChangePasswordTemplate;
