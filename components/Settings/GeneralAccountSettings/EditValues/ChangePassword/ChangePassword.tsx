import React, { useState } from "react";
import { faEdit } from "@fortawesome/pro-solid-svg-icons";

import EFRectangleButton from "../../../../Buttons/EFRectangleButton/EFRectangleButton";
import EFPrimaryModal from "../../../../Modals/EFPrimaryModal/EFPrimaryModal";
import ChangePasswordTemplate from "./ChangePasswordTemplate";

const ChangePassword = () => {
  const [isModalOpen, toggleModal] = useState(false);

  const onclose = () => {
    toggleModal(false);
  };

  return (
    <>
      <EFPrimaryModal title="Change Password" isOpen={isModalOpen} onClose={onclose}>
        <ChangePasswordTemplate closeModal={onclose} />
      </EFPrimaryModal>

      <EFRectangleButton icon={faEdit} text="Change Password" onClick={() => toggleModal(true)} />
    </>
  );
};

export default ChangePassword;
