import { useState } from "react";
import { useDispatch } from "react-redux";
import { faTrash } from "@fortawesome/pro-solid-svg-icons";

import EFPrimaryModal from "../../../Modals/EFPrimaryModal/EFPrimaryModal";
import InputField from "../../../InputField/InputField";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";
import { deleteAccount } from "../../../../store/actions/userActions";

const DeleteAccount = () => {
  const dispatch = useDispatch();

  const [isSubmitting, setIsSubmitting] = useState(false);
  const [open, setOpen] = useState(false);
  const [password, setPassword] = useState("");

  const closeHandler = () => {
    setOpen(false);
    setPassword("");
  };

  const deleteAccountHandler = () => {
    setIsSubmitting(true);

    dispatch(deleteAccount(password, () => setIsSubmitting(false)));
  };

  return (
    <>
      <EFRectangleButton
        onClick={() => setOpen(true)}
        type="button"
        icon={faTrash}
        text="Delete Account"
        colorTheme="secondary"
      />
      {open && (
        <EFPrimaryModal isOpen={open} onClose={closeHandler} displayCloseButton allowBackgroundClickClose={false}>
          <div>
            <h4>Are you sure you want to delete your account?</h4>
            <p>Please enter your password to delete your account</p>
            <div className="text-center mb-2">
              <InputField
                theme="whiteShadow"
                value={password}
                placeholder="Enter Password"
                type="password"
                name="password"
                onChange={event => setPassword(event.target.value)}
              />
            </div>
            <small>
              <i>Note: This action can not be undone</i>
            </small>
            <div className="d-flex flex-row justify-content-end">
              <div className="p-2">
                <EFRectangleButton
                  onClick={closeHandler}
                  colorTheme="light"
                  shadowTheme="medium"
                  text="CANCEL"
                  disabled={isSubmitting}
                />
              </div>
              <div className="p-2">
                <EFRectangleButton
                  onClick={deleteAccountHandler}
                  colorTheme="secondary"
                  shadowTheme="medium"
                  text="DELETE"
                  disabled={isSubmitting}
                />
              </div>
            </div>
          </div>
        </EFPrimaryModal>
      )}
    </>
  );
};

export default DeleteAccount;
