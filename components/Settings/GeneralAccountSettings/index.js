import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import SettingsLayout from "../../Layouts/SettingsLayout";
import { toggleCropModal } from "../../../store/actions/settingsActions";
import { updateCurrentUser, removePic, getAliasedFullName } from "../../../store/actions/userActions";
import { getCurrentUser } from "../../../store/actions/common/userAuthActions";
import { defaultDobYear, getAge } from "../../../helpers/GeneralHelper";
import EditValues from "./EditValues";
import AliasName from "./AliasName/AliasName";
import IMAGES from "../../../static/data/images.json";

const GeneralAccountSettings = () => {
  const dispatch = useDispatch();
  const user = useSelector(state => state.user.currentUser);

  const [validated, setValidate] = useState(false);
  const [underAge, setUnderAge] = useState(false);

  const [localUser, setLocalUser] = useState({
    name: "",
    username: "",
    email: "",
    address: "",
    zipCode: "",
    dateOfBirth: "",
    gender: "",
    bio: "",
    profilePicture: {},
    headerImage: {},
    publicTraitsMotivations: false
  });

  useEffect(() => {
    dispatch(getCurrentUser());
    analytics.page("Settings General Account");
    analytics.track("SETTINGS_GENERAL_ACCOUNT_VIEW");
  }, []);

  useEffect(() => {
    const data = {
      name: user.name,
      username: user.username,
      email: user.email,
      address: user.address,
      zipCode: user.zipCode,
      dateOfBirth: user.dateOfBirth,
      gender: user.gender,
      bio: user.bio,
      profilePicture: user.profilePicture,
      headerImage: user.headerImage,
      publicTraitsMotivations: false
    };
    setLocalUser(data);
  }, [JSON.stringify(user)]);

  const onChange = event => {
    setLocalUser({
      ...localUser,
      [event.target.name]: event.target.value
    });
  };

  const onUserNameChange = event => {
    const maxChars = 15;

    setLocalUser({
      ...localUser,
      [event.target.name]: event.target.value.substr(0, maxChars)
    });
  };

  const onDate = date => {
    if (underAge) {
      setUnderAge(false);
    }

    setLocalUser({
      ...localUser,
      dateOfBirth: defaultDobYear(date)
    });
  };

  const onSuggestSelect = suggest => {
    setLocalUser({
      ...localUser,
      address: suggest === undefined ? "" : suggest.label
    });
  };

  const onSubmit = event => {
    event.preventDefault();
    setValidate(true);

    if (event.currentTarget.checkValidity()) {
      if (getAge(localUser.dateOfBirth) < 13) {
        setUnderAge(true);
      } else {
        analytics.track("SETTINGS_GENERAL_ACCOUNT_SAVE");
        userSubmission();
      }
    }
  };

  const onDrop = (selectedFile, name) => {
    setLocalUser({
      ...localUser,
      [name]: selectedFile[0]
    });
  };

  const onRemoveProfileImage = () => {
    setLocalUser({
      ...localUser,
      profilePicture: IMAGES.placeholder.mindblown_white
    });

    dispatch(removePic("profile"));
  };

  const onRemoveHeaderImage = () => {
    setLocalUser({
      ...localUser,
      headerImage: IMAGES.placeholder.mindblown_white
    });

    dispatch(removePic("header"));
  };

  const onCropChange = (croppedImage, name) => {
    const { blob } = croppedImage;
    const croppedFile = new File([blob], blob.name, { type: blob.type });

    setLocalUser({
      ...localUser,
      [name]: croppedFile
    });
  };

  const toggleCropperModal = (val, name) => {
    dispatch(toggleCropModal(val, name));
  };

  const userSubmission = () => {
    const userData = new FormData();

    Object.keys(localUser).forEach(key => {
      if (localUser[key] !== "") {
        userData.append(key, localUser[key]);
      }
    });

    dispatch(updateCurrentUser(userData));
  };

  const onAlias = () => {
    dispatch(getAliasedFullName());
  };

  return (
    <SettingsLayout title="General Account Settings">
      <AliasName user={user} onAlias={onAlias} />
      <EditValues
        user={localUser}
        onChange={onChange}
        onSubmit={onSubmit}
        onDrop={onDrop}
        onRemoveProfileImage={onRemoveProfileImage}
        onRemoveHeaderImage={onRemoveHeaderImage}
        onDateChange={onDate}
        onSuggestSelect={onSuggestSelect}
        validated={validated}
        onUserNameChange={onUserNameChange}
        onCropChange={onCropChange}
        toggleCropperModal={toggleCropperModal}
        underAge={underAge}
      />
    </SettingsLayout>
  );
};

export default GeneralAccountSettings;
