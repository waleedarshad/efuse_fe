import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "next/router";
import { Row, Col } from "react-bootstrap";
import SettingsLayout from "../../Layouts/SettingsLayout";
import { setUserFromSteam } from "../../../store/actions/userActions";

class SteamAuth extends Component {
  componentDidMount() {
    this.verifySteamAccount();
    analytics.page("Settings Steam");
  }

  verifySteamAccount = () => {
    const { token } = this.props.router.query;
    if (token === undefined) {
      this.callback("failure");
    } else {
      this.props.setUserFromSteam("success", this.callback);
    }
  };

  callback = response => {
    window.opener.parent.postMessage({ steam: true, status: response }, "*");
    window.close();
  };

  render() {
    return (
      <SettingsLayout title="External Account Settings">
        <Row>
          <Col sm={12}>
            <h4>Verifying Steam Account ...</h4>
          </Col>
        </Row>
      </SettingsLayout>
    );
  }
}

export default connect(null, { setUserFromSteam })(withRouter(SteamAuth));
