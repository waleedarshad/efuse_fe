import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { useRouter } from "next/router";
import { twitterAuth } from "../../../store/actions/userActions";

const TwitterAuth = () => {
  const router = useRouter();
  const dispatch = useDispatch();
  const { query } = router;

  const callback = (status = "success") => {
    if (window?.opener?.parent) {
      window.opener.parent.postMessage({ twitter: true, status }, "*");
    }
    window.close();
  };

  useEffect(() => {
    if (query.token) {
      return dispatch(twitterAuth(callback));
    }
    return callback("failure");
  }, []);
  return <div>Authenticating...</div>;
};

export default TwitterAuth;
