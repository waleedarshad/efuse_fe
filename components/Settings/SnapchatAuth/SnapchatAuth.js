import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { useRouter } from "next/router";

import { setSnapchatVerified } from "../../../store/actions/userActions";

const SnapchatAuth = () => {
  const router = useRouter();
  const dispatch = useDispatch();
  const { query } = router;

  const callback = (status = "success") => {
    if (window.opener) {
      window.opener?.parent?.postMessage({ snapchat: true, status }, "*");
    }
    window.close();
  };

  useEffect(() => {
    if (query.token) {
      return dispatch(setSnapchatVerified(callback));
    }
    callback("failure");
  }, []);
  return <div>Authenticating...</div>;
};

export default SnapchatAuth;
