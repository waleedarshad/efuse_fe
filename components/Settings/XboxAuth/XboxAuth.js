import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { useRouter } from "next/router";

import { xboxAuth } from "../../../store/actions/userActions";

const XboxAuth = () => {
  const router = useRouter();
  const dispatch = useDispatch();
  const { query } = router;

  const callback = (response, user) => {
    if (window.opener) {
      window.opener?.parent?.postMessage({ xboxlive: true, status: response, user }, "*");
    }
    window.close();
  };

  useEffect(async () => {
    if (query.code) {
      return dispatch(xboxAuth(query.code, callback));
    }
    return window.close();
  }, []);

  return <div>Authenticating...</div>;
};

export default XboxAuth;
