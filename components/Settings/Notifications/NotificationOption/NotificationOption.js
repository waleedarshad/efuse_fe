import React from "react";
import { Col, Row } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faFlame,
  faClipboardList,
  faTasks,
  faCommentDots,
  faCommentAlt,
  faUsers
} from "@fortawesome/pro-solid-svg-icons";
import NotificationSettingSwitch from "./NotificationSettingSwitch/NotificationSettingSwitch";

const iconLookup = option => {
  const notificationOptionData = {};
  switch (option) {
    case "newMessage":
      notificationOptionData.icon = faCommentDots;
      notificationOptionData.label = "New Message Notifications";
      return notificationOptionData;
    case "newHype":
      notificationOptionData.icon = faFlame;
      notificationOptionData.label = "New Hype Notifications";
      return notificationOptionData;
    case "newComment":
      notificationOptionData.icon = faCommentAlt;
      notificationOptionData.label = "New Comment Notifications";
      return notificationOptionData;
    case "newUserFollow":
      notificationOptionData.icon = faUsers;
      notificationOptionData.label = "New User Follow Notifications";
      return notificationOptionData;
    case "newApplicant":
      notificationOptionData.icon = faTasks;
      notificationOptionData.label = "New Application";
      return notificationOptionData;
    case "weeklySummary":
      notificationOptionData.icon = faClipboardList;
      notificationOptionData.label = "Weekly Notification Summary";
      return notificationOptionData;
    default:
      notificationOptionData.icon = faCommentDots;
      notificationOptionData.label = "Notification";
      return notificationOptionData;
  }
};

const NotificationOption = ({ notificationOption, category }) =>
  notificationOption !== "newShare" && (
    <Row className="pt-3 pb-3 mx-0">
      <Col sm={8}>
        <Row className="mx-0">
          <Col sm={1}>
            <FontAwesomeIcon icon={iconLookup(notificationOption).icon} size="lg" color="#008dff" />
          </Col>
          <Col style={{ color: "#84878c", fontSize: 15 }}>{iconLookup(notificationOption).label}</Col>
        </Row>
      </Col>
      <NotificationSettingSwitch type="email" category={category} notificationOption={notificationOption} />
      <NotificationSettingSwitch type="push" category={category} notificationOption={notificationOption} />
    </Row>
  );
export default NotificationOption;
