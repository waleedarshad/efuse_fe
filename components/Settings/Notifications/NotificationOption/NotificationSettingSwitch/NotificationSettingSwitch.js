import React from "react";
import { connect } from "react-redux";
import { Col } from "react-bootstrap";
import Switch from "../../../../Switch/Switch";
import { updateNotificationSettings } from "../../../../../store/actions/userActions";

const notificationSettingSwitch = ({
  notificationSettings,
  type,
  category,
  notificationOption,
  updateNotificationSettings
}) => {
  if (!notificationSettings[type][category]) {
    return <></>;
  }
  const isNotificationOn = notificationSettings[type][category][notificationOption];
  return (
    <Col sm={2} className="text-center">
      <Switch
        name={notificationOption}
        value={isNotificationOn}
        checked={isNotificationOn}
        onChange={() => {
          // Deep copy on the object to ensure update to redux only comes on successful rest call
          const newNotificationSettings = JSON.parse(JSON.stringify(notificationSettings));
          newNotificationSettings[type][category][notificationOption] = !isNotificationOn;
          updateNotificationSettings(newNotificationSettings);
        }}
        sliderAddedStyle={isNotificationOn ? "onSwitchStyle" : "offSwitchStyle"}
      />
    </Col>
  );
};

const mapStateToProps = state => ({
  notificationSettings: state.user.notificationSettings
});

export default connect(mapStateToProps, { updateNotificationSettings })(notificationSettingSwitch);
