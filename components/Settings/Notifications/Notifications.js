import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import SettingsLayout from "../../Layouts/SettingsLayout";
import { getCurrentNotificationSettings } from "../../../store/actions/userActions";
import { getCurrentUser } from "../../../store/actions/common/userAuthActions";
import Style from "./Notifications.module.scss";
import NotificationTitleRow from "./NotificationTitleRow/NotificationTitleRow";
import NotificationOption from "./NotificationOption/NotificationOption";
import EFCard from "../../Cards/EFCard/EFCard";

const NotificationSettings = () => {
  const dispatch = useDispatch();
  const { notificationSettings } = useSelector(state => state.user);

  useEffect(() => {
    dispatch(getCurrentUser());
    dispatch(getCurrentNotificationSettings());
    analytics.track("SETTINGS_NOTIFICATIONS_VIEW");
    analytics.page("Settings Notifications");
  }, []);

  const settings =
    notificationSettings &&
    Object.keys(notificationSettings.email).map(notificationCategory => (
      <div className={Style.notification} key={notificationCategory}>
        <NotificationTitleRow title={notificationCategory} />
        {Object.keys(notificationSettings.email[notificationCategory]).map(notificationOption => (
          <NotificationOption
            key={notificationOption}
            category={notificationCategory}
            notificationOption={notificationOption}
          />
        ))}
      </div>
    ));
  return (
    <SettingsLayout title="Notifications Settings">
      <EFCard widthTheme="fullWidth" shadow="none">
        {settings}
      </EFCard>
    </SettingsLayout>
  );
};

export default NotificationSettings;
