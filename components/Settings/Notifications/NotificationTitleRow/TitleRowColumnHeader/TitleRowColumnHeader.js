import React from "react";
import { Col } from "react-bootstrap";

const titleRowColumnHeader = props => {
  return (
    <Col sm={2} md={2} className="text-center" style={{ fontSize: 17, color: "#5f7399" }}>
      {props.notificationType}
    </Col>
  );
};

export default titleRowColumnHeader;
