import React from "react";
import { Col, Row } from "react-bootstrap";
import TitleRowColumnHeader from "./TitleRowColumnHeader/TitleRowColumnHeader";

const notificationTitleRow = props => {
  const notificationCategory = {
    chatNotifications: "Chat Notifications",
    postNotifications: "Post Notifications",
    followNotifications: "Follow Notifications",
    opportunityNotifications: "Opportunities Notifications",
    summaryNotifications: "Summary Notifications"
  };
  return (
    <Row className="py-3 px-3 ml-0 mr-0" style={{ backgroundColor: "#F2F5FA" }}>
      <Col sm={6} md={8}>
        <div style={{ fontSize: 17, color: "#5f7399" }}>{notificationCategory[props.title]}</div>
      </Col>
      <TitleRowColumnHeader notificationType="Email" />
      <TitleRowColumnHeader notificationType="Push" />
    </Row>
  );
};

export default notificationTitleRow;
