import React from "react";
import { useMutation, useQuery } from "@apollo/client";
import SettingsLayout from "../../Layouts/SettingsLayout";
import { GET_BLOCKED_USERS, UNBLOCK_USER } from "../../../graphql/BlockedUsersQuery";
import Style from "./BlockedUsers.module.scss";
import EFAvatar from "../../EFAvatar/EFAvatar";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";
import { sendNotification } from "../../../helpers/FlashHelper";

const BlockedUsers = () => {
  const { data } = useQuery(GET_BLOCKED_USERS);
  const blockedUsers = data?.blockedUsers;

  const [unblockUser] = useMutation(UNBLOCK_USER, {
    refetchQueries: [{ query: GET_BLOCKED_USERS }],
    awaitRefetchQueries: true
  });
  const unblock = (blockeeId, username) => {
    unblockUser({ variables: { blockeeId } }).then(() => {
      sendNotification(`Unblocked ${username}`, "success", "Success");
    });
  };

  return (
    <SettingsLayout title="Blocked Users">
      {blockedUsers?.length > 0 ? (
        <div className={Style.blockedUsersList}>
          {blockedUsers.map(blockedUser => (
            <div className={Style.blockedUser} key={blockedUser.username}>
              <EFAvatar profilePicture={blockedUser.profilePicture?.url} displayOnlineButton={false} size="small" />
              <div className={Style.name}>
                <div className={Style.username}>@{blockedUser.username}</div>
                <div>
                  {blockedUser.firstName} {blockedUser.lastName}
                </div>
              </div>
              <div className={Style.button}>
                <EFRectangleButton
                  className={Style.button}
                  onClick={() => unblock(blockedUser._id, blockedUser.username)}
                  colorTheme="primary"
                  text="Unblock"
                />
              </div>
            </div>
          ))}
        </div>
      ) : (
        <span>You have not blocked any users.</span>
      )}
    </SettingsLayout>
  );
};

export default BlockedUsers;
