import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "next/router";
import { Row, Col } from "react-bootstrap";
import SettingsLayout from "../../Layouts/SettingsLayout";
import { updateCurrentUser, discordAuth } from "../../../store/actions/userActions";
import { getCurrentUser } from "../../../store/actions/common/userAuthActions";
import { statesTobeUpdated } from "../../../helpers/UserHelper";

class DiscordAuth extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {}
    };
  }

  componentDidMount() {
    this.props.getCurrentUser();
    this.verifyDiscordAccount();
    analytics.page("Settings Discord");
  }

  static getDerivedStateFromProps(props, state) {
    return statesTobeUpdated(props, state, "user", Object.keys(state.user));
  }

  verifyDiscordAccount = () => {
    const { code } = this.props.router.query;
    if (code !== undefined) {
      this.props.discordAuth(code, response => {
        if (window?.opener?.parent) {
          window.opener.parent.postMessage({ discord: true, status: response }, "*");
        }
        window.close();
      });
    }
  };

  render() {
    return (
      <SettingsLayout title="External Account Settings">
        <Row>
          <Col sm={12}>
            <h4>Verifying Discord Account ...</h4>
          </Col>
        </Row>
      </SettingsLayout>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user.currentUser
});

export default connect(mapStateToProps, {
  getCurrentUser,
  updateCurrentUser,
  discordAuth
})(withRouter(DiscordAuth));
