import React from "react";
import uniqueId from "lodash/uniqueId";
import Style from "./PartnerInfo.module.scss";

const PartnerInfo = ({ canApplyData }) => {
  const partnerMapping = {
    Approved: "Congratulations! You are an eFuse Partner.",
    "Under Review": "Thanks for submitting to be an eFuse Partner. Your application is under review.",
    Deny: "Unfortunately, your account was not approved to be an eFuse Partner."
  };

  const partnerTextMapping = {
    "Bio is missing": "Add a bio to your account.",
    "No social or gaming account connected": "Connect a social or gaming account.",
    "Streak must be greater than or equal to 60": "Maintain a daily active streak of 60 or more."
  };

  return (
    <div className={Style.partnerText}>
      {canApplyData?.canApply || canApplyData?.partnerApplicant ? (
        <>
          {partnerMapping[canApplyData?.partnerApplicant?.status] ||
            "You are able to apply for eFuse Partner! Click the button below to submit your profile application."}
        </>
      ) : (
        <>
          Currently, you are unable to apply for eFuse partner. To apply for eFuse partner, you must meet the following
          requirements:
          <ul>
            {canApplyData?.reasons.map(reason => (
              <li key={uniqueId()}>{partnerTextMapping[reason]}</li>
            ))}
          </ul>
        </>
      )}
    </div>
  );
};

export default PartnerInfo;
