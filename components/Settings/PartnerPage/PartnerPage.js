import { useQuery, useMutation } from "@apollo/client";
import PartnerInfo from "./PartnerInfo/PartnerInfo";
import SettingsLayout from "../../Layouts/SettingsLayout/index";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";
import { CAN_APPLY_PARTNER_BADGE, APPLY_PARTNER_BADGE } from "../../../graphql/PartnerApplicantQuery";

const PartnerPage = () => {
  const { data } = useQuery(CAN_APPLY_PARTNER_BADGE);
  const [submitPartner] = useMutation(APPLY_PARTNER_BADGE, { refetchQueries: [{ query: CAN_APPLY_PARTNER_BADGE }] });

  const canApplyData = data?.CanApplyForPartnerBadge;

  return (
    <SettingsLayout title="eFuse Partner">
      <PartnerInfo canApplyData={canApplyData} />
      {canApplyData?.canApply && (
        <EFRectangleButton
          colorTheme="primary"
          shadowTheme="small"
          width="fullWidth"
          text="Apply for eFuse Partner"
          onClick={submitPartner}
        />
      )}
    </SettingsLayout>
  );
};

export default PartnerPage;
