import React, { useEffect } from "react";
import { useSelector } from "react-redux";

const EFApplyTheme = ({ children }) => {
  const currentTheme = useSelector(state => state.theme.theme);

  const updateCSSVariables = theme => {
    const variableKeys = Object.keys(theme);
    const variableValues = Object.values(theme);

    // Loop through each array key and set the CSS Variables
    variableKeys.forEach((variableKey, index) => {
      // Based on our snippet from MDN
      document.documentElement.style.setProperty(variableKey, variableValues[index]);
    });
  };

  // On Component Mount and Component Update
  useEffect(() => {
    updateCSSVariables(currentTheme);
  }, [currentTheme]);

  return <>{children}</>;
};

export default EFApplyTheme;
