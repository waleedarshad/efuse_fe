import React, { Component } from "react";
import { ProgressBar, Image } from "react-bootstrap";
import ReactPlayer from "react-player";
import { ChasingDots } from "better-react-spinkit";

import Style from "./UploadDisplay.module.scss";
import CloseButton from "../CloseButton/CloseButton";
import { withCdn } from "../../common/utils";

class UploadDisplay extends Component {
  state = {
    blurClass: Style.placeholderImage,
    imageLoaded: false
  };

  imageOnLoad = fileUrl => {
    if (fileUrl) this.setState({ blurClass: Style.removeBlur, imageLoaded: true });
  };

  render() {
    const { file, uploadStarted, uploadProgress, cancelUpload, removePadding, setHeight } = this.props;
    const fileUrl = file.url;
    const { contentType } = file;
    const { imageLoaded } = this.state;
    const isVideo = contentType.includes("video");
    const displayCloseBtn = imageLoaded || (isVideo && fileUrl);
    const spinnerConfig = { color: "#849bba", size: 100 };
    let content = "";
    if (contentType.includes("video")) {
      content = fileUrl ? (
        <ReactPlayer width="100%" height={setHeight || "auto"} url={fileUrl} controls className={`${Style.player}`} />
      ) : (
        <ChasingDots size={spinnerConfig.size} color={spinnerConfig.color} className={Style.spinner} />
      );
    } else {
      content = (
        <>
          {fileUrl && !imageLoaded && (
            <div className="text-center">
              <ChasingDots size={spinnerConfig.size} color={spinnerConfig.color} className={Style.spinner} />
            </div>
          )}
          <Image
            onLoad={() => this.imageOnLoad(fileUrl)}
            fluid
            src={fileUrl || withCdn("/static/images/placeholder.png")}
            className={this.state.blurClass}
          />
        </>
      );
    }
    return (
      <>
        {uploadStarted ? (
          <div className={`${Style.uploadWrapper} ${removePadding && Style.removePadding}`}>
            <span className={Style.imageWrapper}>
              {displayCloseBtn && <CloseButton customClass={Style.closeBtn} onClick={cancelUpload} />}
              {content}
            </span>
            <ProgressBar now={uploadProgress} label={`${uploadProgress}%`} className={Style.progressBar} />
          </div>
        ) : null}
      </>
    );
  }
}

export default UploadDisplay;
