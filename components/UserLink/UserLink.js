import PropTypes from "prop-types";
import { EFHtmlParser } from "../EFHtmlParser/EFHtmlParser";
import Style from "./UserLink.module.scss";

const UserLink = ({ user, displayName, profilePath }) => {
  const url = profilePath || `/users/posts?id=${user?._id || user?.id}`;

  return (
    <a href={url} className={Style.userLink}>
      <EFHtmlParser>{displayName || user?.name}</EFHtmlParser>
    </a>
  );
};

UserLink.propTypes = {
  user: PropTypes.objectOf(PropTypes.any).isRequired,
  displayName: PropTypes.string
};

UserLink.defaultProps = {
  displayName: ""
};

export default UserLink;
