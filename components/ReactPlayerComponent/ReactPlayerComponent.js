import React from "react";
import { useSelector } from "react-redux";
import ReactPlayer from "react-player";

import Style from "./ReactPlayerComponent.module.scss";
import { getFlagForFeature } from "../../store/selectors/featureFlagSelectors";
import FEATURE_FLAGS from "../../common/featureFlags";

const ReactPlayerComponent = ({ url, autoplay, muted, controls, playing }) => {
  /**
   * Global kill switch, so that we can disable autoplay for everyone when required.
   * Once set to true will take precedence over user's Media Settings or any default value
   */
  const disableAutoplay = useSelector(state =>
    getFlagForFeature(state, FEATURE_FLAGS.KILL_SWITCH_GLOBAL_DISABLE_AUTOPLAY)
  );

  const autoplayMedia = disableAutoplay ? false : autoplay;

  return (
    <div className={Style.videoWrapper}>
      <ReactPlayer
        className={Style.reactPlayer}
        width="100%"
        height="100%"
        url={url}
        controls={controls}
        playing={playing}
        muted={muted}
        config={{
          file: {
            attributes: {
              autoPlay: autoplayMedia,
              muted
            }
          }
        }}
      />
    </div>
  );
};

export default ReactPlayerComponent;
