import React, { ReactNode } from "react";
import Style from "./LeaguesDashboardLayout.module.scss";

interface LeaguesDashboardLayoutProps {
  header?: ReactNode;
  children: ReactNode;
  noScroll?: boolean;
  breadcrumbs?: ReactNode;
}

const LeaguesDashboardLayout = ({ noScroll = false, breadcrumbs, header, children }: LeaguesDashboardLayoutProps) => {
  return (
    <div className={`${Style.page} ${noScroll ? Style.noScroll : ""}`}>
      {breadcrumbs && <div>{breadcrumbs}</div>}
      {header && <div className={Style.header}>{header}</div>}
      <div className={Style.body}>{children}</div>
    </div>
  );
};

export default LeaguesDashboardLayout;
