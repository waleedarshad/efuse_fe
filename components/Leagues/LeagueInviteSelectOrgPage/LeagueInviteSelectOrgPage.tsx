import React, { useEffect, useState } from "react";
import { useMutation } from "@apollo/client";
import { useRouter } from "next/router";
import EFWizard from "../../EFWizard/EFWizard";
import SelectOrCreateOrg from "./SelectOrCreateOrg/SelectOrCreateOrg";
import OrganizationJoinedModal from "../Modals/OrganizationJoinedModal";
import OrganizationCreatedModal from "../Modals/OrganizationCreatedModal";
import { ACCEPT_INVITATION, AcceptInvitationResponse, AcceptInvitationVars } from "../../../graphql/leagues/Invitation";
import { sendNotification } from "../../../helpers/FlashHelper";

interface LeagueInviteSelectOrgPageProps {
  inviteCode: string;
  invitingOrg: {
    _id: string;
    name: string;
  };
  newOrgId: string;
}
const LeagueInviteSelectOrgPage = ({ inviteCode, invitingOrg, newOrgId }: LeagueInviteSelectOrgPageProps) => {
  const router = useRouter();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [joiningOrgId, setJoiningOrgId] = useState("");
  const [acceptInvitationMutation, { loading }] = useMutation<AcceptInvitationResponse, AcceptInvitationVars>(
    ACCEPT_INVITATION
  );

  const isComingFromOrgCreationFlow = !!newOrgId;

  useEffect(() => {
    if (isComingFromOrgCreationFlow) {
      acceptInvitation(newOrgId);
    }
  }, []);

  const displayFailedToJoinFlash = () =>
    sendNotification(`Failed to join ${invitingOrg.name}'s leagues`, "danger", "Failure");

  const acceptInvitation = (orgIdToJoin: string) => {
    acceptInvitationMutation({ variables: { inviteCode, joiningOrgId: orgIdToJoin } })
      .then(response => {
        if (response.data.acceptInvite) {
          setJoiningOrgId(orgIdToJoin);
          setIsModalOpen(true);
        } else {
          displayFailedToJoinFlash();
        }
      })
      .catch(() => {
        displayFailedToJoinFlash();
      });
  };

  const navigateToTeamCreation = () => {
    setIsModalOpen(false);
    router.push(`/leagues/teams/create/${invitingOrg._id}/${joiningOrgId}`);
  };

  return (
    <>
      <EFWizard>
        <SelectOrCreateOrg onNext={acceptInvitation} isLoading={loading} />
      </EFWizard>
      {isModalOpen && (
        <>
          <OrganizationCreatedModal modalIsOpen={isComingFromOrgCreationFlow} onClose={navigateToTeamCreation} />
          <OrganizationJoinedModal modalIsOpen={!isComingFromOrgCreationFlow} onClose={navigateToTeamCreation} />
        </>
      )}
    </>
  );
};

export default LeagueInviteSelectOrgPage;
