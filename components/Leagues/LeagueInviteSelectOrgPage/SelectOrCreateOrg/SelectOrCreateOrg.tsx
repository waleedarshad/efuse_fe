import React, { useState } from "react";
import { useRouter } from "next/router";
import { useQuery } from "@apollo/client";
import { useSelector } from "react-redux";
import EFCreateFlowLayout from "../../../EFCreateFlowLayout/EFCreateFlowLayout";
import EFDetailsCard from "../../../EFDetailsCard/EFDetailsCard";
import {
  GET_USER_ORGANIZATIONS,
  getUserOrganizationsData,
  getUserOrganizationsVars
} from "../../../../graphql/organizations/Organizations";
import EFCreateCard from "../../../EFCreateCard/EFCreateCard";
import Style from "./SelectOrCreateOrg.module.scss";
import { SubtitleTwo } from "../../Typography/Typography";
import SkeletonLoader from "../../../SkeletonLoader/SkeletonLoader";

interface SelectOrCreateOrgProps {
  onNext: (orgId: string) => void;
  isLoading: boolean;
}

const SelectOrCreateOrg = ({ onNext, isLoading }: SelectOrCreateOrgProps) => {
  const router = useRouter();
  const userId = useSelector(state => state.auth.currentUser?._id);
  const { data, loading } = useQuery<getUserOrganizationsData, getUserOrganizationsVars>(GET_USER_ORGANIZATIONS, {
    variables: { userId }
  });

  const [selectedOrgId, setSelectedOrgId] = useState("");

  const navigateToOrgCreation = () => {
    router.push(`/organizations/create/?redirect_url=${router.asPath}`);
  };

  const onCardClick = orgId => setSelectedOrgId(selectedOrgId === orgId ? "" : orgId);

  // Displaying skeleton effect like organization listing layout
  const displayLoader = [1, 2, 3, 4, 5, 6, 7, 8].map(() => {
    return (
      <div style={{ width: "185px" }}>
        <SkeletonLoader skeletons={1} postHeight={241} engagementHeight={0} />
      </div>
    );
  });

  return (
    <EFCreateFlowLayout
      titleText="Select an organization or create a new one!"
      isRightButtonDisabled={selectedOrgId === ""}
      onNext={() => onNext(selectedOrgId)}
      buttonText="Join"
      isLoading={isLoading}
    >
      <SubtitleTwo>Choose an existing organization or create a new one</SubtitleTwo>
      <div className={Style.selectOrgContainer}>
        {loading ? (
          displayLoader
        ) : (
          <>
            <EFCreateCard text="Create Org" onClick={navigateToOrgCreation} size="small" />
            {data?.getUserOrganizations.map(org => (
              <EFDetailsCard
                key={org._id}
                fitContent={false}
                image={org.profileImage?.url}
                line1={org.name}
                line3={`${org.teams.length} Team${org.teams.length === 1 ? "" : "s"}`}
                size="small"
                isSelected={selectedOrgId === org._id}
                onClick={() => onCardClick(org._id)}
              />
            ))}
          </>
        )}
      </div>
    </EFCreateFlowLayout>
  );
};

export default SelectOrCreateOrg;
