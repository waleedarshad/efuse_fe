import React from "react";
import { useRouter } from "next/router";
import { LeagueEvent, Team } from "../../../../graphql/interface/League";
import LeagueEventsContent from "../LeagueEvents/LeagueEventsContent";
import DisplayDescriptionAndRules from "../DisplayDescriptionAndRules/DisplayDescriptionAndRules";
import EFTabs from "../../../EFTabs/EFTabs";
import TeamsList from "../../LeagueDashboard/TeamsList/TeamsList";
import { Game } from "../../../../graphql/interface/Game";

interface LeagueTabsProps {
  leagueId: string;
  leagueGame: Game;
  events: LeagueEvent[];
  onEventCardClick?: (event: any) => void;
  deleteEvent?: (event: LeagueEvent, index: number) => void;
  rules: string;
  description: string;
  leagueteams: Team[];
  updateMethod?: (updates: any, closeModal: () => void) => void;
  isAdmin: boolean;
}

const LeagueTabs: React.FC<LeagueTabsProps> = ({
  leagueId,
  leagueGame,
  events,
  onEventCardClick,
  deleteEvent,
  rules,
  description,
  leagueteams,
  isAdmin = false
}) => {
  const router = useRouter();

  const onTabSelect = tabKey => {
    router.push(`/leagues/${leagueId}/?tab=${tabKey}`, undefined, { shallow: true });
  };

  const tabs = [
    {
      tabTitle: "Overview",
      content: <DisplayDescriptionAndRules info={description} fieldName="overview" />,
      tabKey: "overview",
      onTabSelect
    },
    {
      tabTitle: "Events",
      content: (
        <LeagueEventsContent
          events={events}
          leagueGame={leagueGame}
          onEventCardClick={onEventCardClick}
          deleteEvent={deleteEvent}
        />
      ),
      tabKey: "events",
      onTabSelect
    },
    {
      tabTitle: "Rules",
      content: <DisplayDescriptionAndRules info={rules} fieldName="rules" />,
      tabKey: "rules",
      onTabSelect
    },
    {
      tabTitle: "Teams",
      content: <TeamsList teams={leagueteams} hideInvite isAdmin={isAdmin} />,
      tabKey: "teams",
      onTabSelect
    }
  ];
  return <EFTabs tabs={tabs} defaultActiveKey={router.query.tab?.toString()} />;
};

export default LeagueTabs;
