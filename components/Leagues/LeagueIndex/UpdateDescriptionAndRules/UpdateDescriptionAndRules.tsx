import React, { useState, useEffect } from "react";
import { Form, Col } from "react-bootstrap";
import capitalize from "lodash/capitalize";
import { useRouter } from "next/router";
import styled from "styled-components";
import InputRow from "../../../InputRow/InputRow";
import EFPrimaryModal from "../../../Modals/EFPrimaryModal/EFPrimaryModal";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";
import EFHtmlInput from "../../../EFHtmlInput/EFHtmlInput";
import ActionButtonGroup from "../../../Layouts/Internal/ActionButtonGroup/ActionButtonGroup";
import { sendNotification } from "../../../../helpers/FlashHelper";

interface UpdateDetailsAndRulesProps {
  info: string;
  fieldName: string;
  updateFieldContent?: (content: string) => void;
  updateMethod?: (updates: any, closeModal: () => void) => void;
  showEditWithDisplay?: boolean;
  displayFieldName?: string;
}

const UpdateDescriptionAndRules: React.FC<UpdateDetailsAndRulesProps> = ({
  fieldName,
  info = "",
  updateFieldContent,
  updateMethod,
  showEditWithDisplay,
  displayFieldName
}) => {
  const [isModalOpen, toggleModal] = useState(false);
  const [fieldInput, setFieldInput] = useState("");

  const router = useRouter();

  const capitalizedFieldName = capitalize(displayFieldName ?? fieldName);

  useEffect(() => {
    setFieldInput(info);
  }, [info]);

  const afterSubmit = () => {
    toggleModal(false);
    sendNotification(`${capitalize(displayFieldName ?? fieldName)} updated successfully`, "success", "Success");
    updateFieldContent && updateFieldContent(fieldInput);
  };

  const onEditorChange = editorValue => {
    setFieldInput(editorValue);
  };

  const onSubmit = e => {
    e.preventDefault();

    if (updateMethod) {
      updateMethod({ [fieldName]: fieldInput }, afterSubmit);
    }
  };

  return (
    <>
      <DisplayButtonWrapper showEditWithDisplay={showEditWithDisplay}>
        <EFRectangleButton
          shadowTheme="none"
          text={`${fieldInput ? "Edit" : "Add"} ${capitalizedFieldName}`}
          onClick={() => toggleModal(true)}
        />
      </DisplayButtonWrapper>
      <EFPrimaryModal
        title={`${fieldInput ? "Update" : "Add"} League ${
          router.asPath.includes("event") ? "Event" : ""
        } ${capitalizedFieldName}`}
        isOpen={isModalOpen}
        onClose={() => toggleModal(false)}
        allowBackgroundClickClose
        displayCloseButton
        widthTheme="medium"
      >
        <Form noValidate onSubmit={onSubmit}>
          <InputRow>
            <Col sm={12}>
              <EFHtmlInput id={`UpdateDetailsAndRules-${fieldName}`} value={fieldInput} onChange={onEditorChange} />
            </Col>
          </InputRow>
          <ActionButtonGroup onCancel={() => toggleModal(false)} />
        </Form>
      </EFPrimaryModal>
    </>
  );
};

const DisplayButtonWrapper = styled.div`
  position: ${props => (props.showEditWithDisplay ? "absolute" : "unset")};
  top: -55px;
  right: 5px;
`;

export default UpdateDescriptionAndRules;
