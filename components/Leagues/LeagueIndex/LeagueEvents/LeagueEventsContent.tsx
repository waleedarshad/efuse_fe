import React from "react";
import styled from "styled-components";
import EFLeagueEventCard from "../../../Cards/EFLeagueEventCard/EFLeagueEventCard";
import colors from "../../../../styles/sharedStyledComponents/colors";

const LeagueEventsContent = ({ events, leagueGame, onEventCardClick, deleteEvent }) => {
  const upcomingEvents = events
    .filter(event => event.state === "pending" || event.state === "pendingPools" || event.state === "pendingTeams")
    .slice(0, 4);

  const currentEvents = events.filter(event => event.state === "active").slice(0, 4);

  const pastEvents = events.filter(event => event.state === "finished").slice(0, 4);

  const placeholderContent = (
    <PlaceholderContainer>
      <p>There are no events of this type at this time.</p>
    </PlaceholderContainer>
  );

  const getListOfEvents = list => (
    <EventList>
      {list.map((event, index) => {
        return (
          <EventItem key={event?._id}>
            <EFLeagueEventCard
              image={leagueGame?.gameImage?.url}
              title={event?.name}
              bracketType={event?.bracketType}
              numberOfTeams={event?.teams?.length}
              date={event?.startDate}
              onClick={() => onEventCardClick(event)}
              onDelete={() => deleteEvent(event, index)}
            />
          </EventItem>
        );
      })}
    </EventList>
  );

  return (
    <EventsListContainer>
      <EventTitle>Upcoming Events</EventTitle>
      {upcomingEvents?.length > 0 ? getListOfEvents(upcomingEvents) : placeholderContent}
      <EventTitle>Current Events</EventTitle>
      {currentEvents?.length > 0 ? getListOfEvents(currentEvents) : placeholderContent}
      <EventTitle>Past Events</EventTitle>
      {pastEvents?.length > 0 ? getListOfEvents(pastEvents) : placeholderContent}
    </EventsListContainer>
  );
};

const EventsListContainer = styled.div`
  margin-bottom: 20px;
`;

const EventList = styled.div`
  display: flex;
  grid-gap: 15px;
`;

const EventTitle = styled.p`
  font-weight: 600;
  font-size: 16px;
  color: ${colors.black};
  margin: 15px 0;
`;

const EventItem = styled.div`
  padding: 10px;
`;

const PlaceholderContainer = styled.div`
  height: 125px;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

export default LeagueEventsContent;
