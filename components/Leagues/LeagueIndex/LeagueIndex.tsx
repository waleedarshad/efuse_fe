import React, { useState } from "react";
import { useRouter } from "next/router";
import { useDispatch } from "react-redux";
import { faShieldAlt } from "@fortawesome/pro-solid-svg-icons";
import { useMutation } from "@apollo/client";
import { League, LeagueEvent } from "../../../graphql/interface/League";
import EFBannerSnippet from "../../Cards/EFBannerCard/EFBannerSnippet/EFBannerSnippet";
import EFBannerCard from "../../Cards/EFBannerCard/EFBannerCard";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";
import LeagueTabs from "./LeagueTabs/LeagueTabs";
import { DELETE_LEAGUE_EVENT } from "../../../graphql/leagues/LeagueEvents";
import { sendNotification } from "../../../helpers/FlashHelper";
import { setErrors } from "../../../store/actions/errorActions";
import EFBreadcrumb from "../../EFBreadcrumb/EFBreadcrumb";
import LeagueNavContainer from "../LeagueNavigation/LeagueSidebar/LeagueNavContainer";
import UpdateDescriptionAndRules from "./UpdateDescriptionAndRules/UpdateDescriptionAndRules";
import { UPDATE_LEAGUE, UpdateLeagueVars } from "../../../graphql/leagues/Leagues";
import { OrganizationUserRoles } from "../../../graphql/interface/Organization";
import LeaguesDashboardLayout from "../Layouts/LeaguesDashboardLayout/LeaguesDashboardLayout";

const LeagueIndex = ({
  league,
  events,
  userLeagueOrgRoles,
  userHasAdminPrivileges
}: {
  league: League;
  events: LeagueEvent[];
  userLeagueOrgRoles: OrganizationUserRoles;
  userHasAdminPrivileges: boolean;
}) => {
  const router = useRouter();
  const dispatch = useDispatch();
  const [deleteLeagueEvent] = useMutation(DELETE_LEAGUE_EVENT);
  const [stateEvents, setStateEvents] = useState(events);
  const [description, setDescription] = useState(league.description);
  const [rules, setRules] = useState(league.rules);

  const getNumberOfOrgs = () => {
    const orgs = new Set();
    // eslint-disable-next-line lodash/prefer-lodash-method
    league?.teams?.map(team => orgs.add(team.owner._id));
    return orgs.size;
  };

  const onCreateEventButtonClick = async () => {
    // TODO: update this to take the user to the individual league dashboard once that is done.
    await router.push(`/leagues/${league?._id}/events/create`);
  };

  const onEventCardClick = async event => {
    await router.push(`/leagues/event/${event?._id}`);
  };

  const deleteEvent = async (event: LeagueEvent, index: number) => {
    deleteLeagueEvent({
      variables: {
        eventId: event?._id
      }
    })
      .then(() => {
        sendNotification("Event removed successfully", "success", "Success");

        // remove the deleted league from state
        const updatedEvents = [...stateEvents];
        updatedEvents.splice(index, 1);
        setStateEvents(updatedEvents);
      })
      .catch(error => {
        dispatch(setErrors({ error: error.message }));
      });
  };

  const cardDetails = (
    <>
      <EFBannerSnippet title="Events" subtitle={`${events?.length || 0}`} />
      <EFBannerSnippet title="Organizations" subtitle={getNumberOfOrgs().toString()} />
      <EFBannerSnippet title="Teams" subtitle={`${league?.teams?.length || 0}`} />
    </>
  );

  const [updateLeague] = useMutation<any, UpdateLeagueVars>(UPDATE_LEAGUE);

  const updateMethod = (updates, afterSubmit) => {
    updateLeague({ variables: { leagueId: league?._id, updates } })
      .then(() => {
        afterSubmit();
      })
      .catch(error => dispatch(setErrors({ error: error.message })));
  };

  const getCardButton = () => {
    if (!userHasAdminPrivileges) {
      return null;
    }

    if (router.query.tab === "events") {
      return <EFRectangleButton text="Create Event" onClick={onCreateEventButtonClick} />;
    }

    if (router.query.tab === "rules") {
      return (
        <UpdateDescriptionAndRules
          info={rules}
          fieldName="rules"
          updateMethod={updateMethod}
          updateFieldContent={setRules}
        />
      );
    }

    if (router.query.tab === "teams") {
      return null;
    }

    return (
      <UpdateDescriptionAndRules
        info={description}
        fieldName="description"
        updateMethod={updateMethod}
        updateFieldContent={setDescription}
        displayFieldName="overview"
      />
    );
  };

  return (
    <LeagueNavContainer userLeagueOrgRoles={userLeagueOrgRoles}>
      <LeaguesDashboardLayout
        breadcrumbs={
          <EFBreadcrumb
            links={[
              { name: league.owner.name, href: `/leagues/dashboard/${league.owner._id}` },
              { name: league.name, href: `/leagues/${league._id}` }
            ]}
          />
        }
        header={
          <EFBannerCard
            backgroundImage={league?.imageUrl}
            cardTitle={league?.name}
            hasTitleAvatar={false}
            cardSubtitle={league?.owner?.name}
            subtitleIcon={faShieldAlt}
            cardDetails={cardDetails}
            cardButton={getCardButton()}
            cardShadow="none"
          />
        }
      >
        <LeagueTabs
          leagueId={league?._id}
          leagueGame={league?.game}
          events={stateEvents}
          onEventCardClick={onEventCardClick}
          deleteEvent={deleteEvent}
          description={description}
          rules={rules}
          leagueteams={league?.teams}
          updateMethod={updateMethod}
          isAdmin={userHasAdminPrivileges}
        />
      </LeaguesDashboardLayout>
    </LeagueNavContainer>
  );
};

export default LeagueIndex;
