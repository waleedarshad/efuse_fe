import React from "react";
import capitalize from "lodash/capitalize";
import { EFHtmlParser } from "../../../EFHtmlParser/EFHtmlParser";
import Style from "./DisplayDescriptionAndRules.module.scss";

interface DisplayDetailsAndRulesProps {
  info: string;
  fieldName: string;
}

const DisplayDescriptionAndRules: React.FC<DisplayDetailsAndRulesProps> = ({ info, fieldName }) => {
  return (
    <div className={Style.detailsAndRulesContainer}>
      {info ? (
        <EFHtmlParser>{info}</EFHtmlParser>
      ) : (
        <div className={Style.placeholderContainer}>
          <div>
            <p className={Style.placeholderText}>You currently do not have any {fieldName}</p>
            <p className={Style.placeholderSubtext}>
              Click Add {capitalize(fieldName)} to enter your {fieldName}
            </p>
          </div>
        </div>
      )}
    </div>
  );
};

export default DisplayDescriptionAndRules;
