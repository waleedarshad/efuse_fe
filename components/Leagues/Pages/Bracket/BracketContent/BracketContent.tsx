import React from "react";
import { LeagueBracketType, LeagueEvent, LeagueMatch } from "../../../../../graphql/interface/League";
import RoundRobinBracketContent from "./RoundRobinBracketContent";
import SingleElimBracketContent from "./SingleElimBracketContent";
import EFDisplayPlaceholder from "../../../../EFDisplayPlaceholder/EFDisplayPlaceholder";

interface Props {
  event: LeagueEvent;
  onMatchClick?: (match: LeagueMatch) => void;
}

const BracketContent = ({ event, onMatchClick = () => {} }: Props) => {
  switch (event.bracketType) {
    case LeagueBracketType.ROUND_ROBIN:
      return <RoundRobinBracketContent event={event} onMatchClick={onMatchClick} />;
    case LeagueBracketType.SINGLE_ELIM:
      return <SingleElimBracketContent event={event} onMatchClick={onMatchClick} />;
    default:
      return <EFDisplayPlaceholder text="Invalid Event" />;
  }
};

export default BracketContent;
