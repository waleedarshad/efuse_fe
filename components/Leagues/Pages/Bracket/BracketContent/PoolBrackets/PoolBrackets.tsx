import React from "react";
import EFMatchup from "../../../../../EFMatchup/EFMatchup";
import styled from "styled-components";
import { LeagueMatch } from "../../../../../../graphql/interface/League";
import Style from "./PoolBrackets.module.scss";
import colors from "../../../../../../styles/sharedStyledComponents/colors";

interface DisplayPoolBracketsProps {
  poolName: string;
  matches: LeagueMatch[];
  onMatchClick: (match: LeagueMatch) => void;
}

const PoolBrackets: React.FC<DisplayPoolBracketsProps> = ({ poolName, matches, onMatchClick }) => (
  <div>
    <p className={Style.poolName}>{poolName}</p>
    <div className={Style.brackets}>
      {matches?.length > 0 ? (
        <>
          {matches?.map(match => (
            <EFMatchup
              key={match._id}
              teams={match.teams}
              matchupIsFinal={false}
              onClick={() => onMatchClick(match)}
              matchId={match._id}
            />
          ))}
        </>
      ) : (
        <Placeholder>There are no matches for this round</Placeholder>
      )}
    </div>
  </div>
);

const Placeholder = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 50px;
  font-weight: 500;
  font-size: 18px;
  color: ${colors.textStrong};
`;

export default PoolBrackets;
