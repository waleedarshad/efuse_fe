import React, { useState } from "react";
import { TabContainer, TabContent, TabPane } from "react-bootstrap";
import { LeagueEvent, LeagueMatch } from "../../../../../graphql/interface/League";
import Style from "./Bracket.module.scss";
import HorizontalScroll from "../../../../HorizontalScroll/HorizontalScroll";
import RoundTab from "./RoundTab/RoundTab";
import { formatShortDateTime } from "../../../../../helpers/GeneralHelper";
import PoolBrackets from "./PoolBrackets/PoolBrackets";
import ScoreboardMenu from "./ScoreboardMenu/ScoreboardMenu";

interface BracketProps {
  event: LeagueEvent;
  onMatchClick?: (match: LeagueMatch) => void;
}

const RoundRobinBracketContent = ({ event, onMatchClick = () => {} }: BracketProps) => {
  // Its possible for pools to have different number of rounds. So we are trying to find the pool with the most number of rounds
  const getPoolWithMostNumberOfRounds = () => {
    let poolWithMostNumberOfRounds = event?.pools[0];
    event?.pools?.forEach(pool => {
      const poolsNumberOfRounds = pool?.bracket?.rounds?.length ?? 0;
      const highestNumberOfRounds = poolWithMostNumberOfRounds?.bracket?.rounds?.length ?? 0;

      poolWithMostNumberOfRounds = poolsNumberOfRounds > highestNumberOfRounds ? pool : poolWithMostNumberOfRounds;
    });

    return poolWithMostNumberOfRounds;
  };

  const poolWithMostNumberOfRounds = getPoolWithMostNumberOfRounds();
  const rounds = poolWithMostNumberOfRounds?.bracket?.rounds;
  const pools = event?.pools;

  const [activeTab, setActiveTab] = useState(rounds[0]?.name);

  const getSelectedRoundMatches = (poolRounds, currentRoundName) => {
    const selectedRound = poolRounds.filter(round => round.name === currentRoundName);

    return selectedRound[0]?.matches;
  };

  return (
    <TabContainer activeKey={activeTab} defaultActiveKey={activeTab}>
      <div className={Style.tabsContainer}>
        <HorizontalScroll customStyle={Style.tabs} customButtonMargin={Style.carouselButton} addFadingEffect>
          {rounds?.map(round => (
            <RoundTab
              key={round.name}
              title={round.name}
              subTitle={formatShortDateTime(round.startDate)}
              isActive={round.name === activeTab}
              onClick={k => setActiveTab(k)}
            />
          ))}
        </HorizontalScroll>
      </div>
      <TabContent>
        {pools?.map((pool, index) => (
          <TabPane eventKey={activeTab} key={pool._id}>
            <PoolBrackets
              poolName={pool.name}
              matches={getSelectedRoundMatches(pool.bracket?.rounds, activeTab)}
              onMatchClick={onMatchClick}
            />
            {pools.length - 1 !== index && <hr className={Style.breakLine} />}
          </TabPane>
        ))}
        <ScoreboardMenu eventId={event._id} />
      </TabContent>
    </TabContainer>
  );
};

export default RoundRobinBracketContent;
