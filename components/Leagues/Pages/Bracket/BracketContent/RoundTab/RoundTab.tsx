import React from "react";
import Style from "./RoundTab.module.scss";

interface RoundTabProps {
  title: string;
  subTitle: string;
  isActive: boolean;
  onClick: (key: string) => void;
}

const RoundTab: React.FC<RoundTabProps> = ({ title, subTitle, isActive, onClick }) => {
  return (
    <div
      className={`${Style.tabContainer} ${isActive ? Style.active : ""}`}
      onClick={() => onClick(title)}
      aria-hidden="true"
      role="button"
    >
      <p className={Style.title}>{title}</p>
      <span className={Style.subTitle}>{subTitle}</span>
    </div>
  );
};

export default RoundTab;
