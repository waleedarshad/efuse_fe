import { gql, useQuery } from "@apollo/client";

const FETCH_SCORES = gql`
  query getScoreboardsForEvent($eventId: String!) {
    getScoreboardsForEvent(eventId: $eventId) {
      scoreboard {
        team {
          _id
          name
          owner {
            ... on Organization {
              profileImage {
                url
              }
            }
          }
        }
        wins
        losses
      }
      pool {
        name
      }
    }
  }
`;

export const useGetScoreboard = eventId => {
  const { data, loading, error } = useQuery(FETCH_SCORES, { variables: { eventId }, fetchPolicy: "network-only" });
  return [data?.getScoreboardsForEvent, loading, error];
};
