import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styled from "styled-components";
import Collapsible from "react-collapsible";
import { faChevronDown, faChevronUp } from "@fortawesome/pro-regular-svg-icons";
import React, { useEffect, useState } from "react";
import EFSelectDropdown from "../../../../../Dropdowns/EFSelectDropdown/EFSelectDropdown";
import { useGetScoreboard } from "./queries";

const ScoreboardMenu = ({ eventId }) => {
  const [scoreboardData] = useGetScoreboard(eventId);
  const [selectedPoolName, setSelectedPoolName] = useState(null);
  const [scoreboard, setScoreboard] = useState(null);
  const [poolNames, setPoolNames] = useState([]);

  useEffect(() => {
    if (scoreboardData) {
      const localScoreboard = scoreboardData?.reduce((acc, scoreboardWithPool) => {
        const poolName = scoreboardWithPool.pool.name;
        const scores = scoreboardWithPool.scoreboard.map(leagueScoreboard => {
          const orgImage = leagueScoreboard.team.owner.profileImage.url;
          const teamName = leagueScoreboard.team.name;

          const { wins } = leagueScoreboard;
          const { losses } = leagueScoreboard;

          return {
            orgImage,
            teamName,
            wins,
            losses
          };
        });

        return { ...acc, [poolName]: scores };
      }, {});

      const localPoolNames = Object.keys(localScoreboard);

      setScoreboard(localScoreboard);
      setPoolNames(localPoolNames);

      if (localPoolNames.length === 1) {
        setSelectedPoolName(localPoolNames[0]);
      }
    }
  }, [scoreboardData]);

  if (!scoreboard) {
    return <></>;
  }

  return (
    <CollapsibleContainer>
      <Collapsible
        trigger={
          <TriggerContainer>
            <Trigger>Scoreboard</Trigger>
            <StyledChevron icon={faChevronUp} />
          </TriggerContainer>
        }
        triggerWhenOpen={
          <TriggerContainer>
            <Trigger>Scoreboard</Trigger>
            <StyledChevron icon={faChevronDown} />
          </TriggerContainer>
        }
        transitionTime={100}
      >
        <CollapsedMenuWrapper>
          {poolNames.length > 1 && (
            <Dropdown
              defaultValue={poolNames[0]}
              options={poolNames.map(poolName => ({
                value: poolName,
                label: (
                  <DropdownItem>
                    <p>{poolName}</p>
                  </DropdownItem>
                )
              }))}
              onSelect={setSelectedPoolName}
            />
          )}

          <Grid>
            <Cell position="left" header>
              Team
            </Cell>
            <Cell position="center" header>
              Win
            </Cell>
            <Cell position="right" header>
              Lose
            </Cell>

            {scoreboard[selectedPoolName]?.map(score => {
              return (
                <React.Fragment key={score.teamName}>
                  <Cell position="left">
                    <OrgImage src={score.orgImage} alt={`org image for ${score.teamName}`} />
                    {score.teamName}
                  </Cell>
                  <Cell position="center">{score.wins}</Cell>
                  <Cell position="right">{score.losses}</Cell>
                </React.Fragment>
              );
            })}
          </Grid>
        </CollapsedMenuWrapper>
      </Collapsible>
    </CollapsibleContainer>
  );
};

const CollapsibleContainer = styled.div`
  position: fixed;
  right: 30px;
  bottom: 0;
  background: #ffffff;
  border: 1px solid #c7d3ea;
  box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.15);
  border-radius: 8px 8px 0px 0px;
  width: 400px;
`;

const StyledChevron = styled(FontAwesomeIcon)`
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 20px;
  color: rgba(18, 21, 29, 0.6);
`;

const TriggerContainer = styled.div`
  padding: 18px 20px;
  cursor: pointer;
  display: flex;
  align-items: center;
  justify-content: space-between;
  transition: all 300ms;

  &:hover {
    opacity: 0.6;
  }
`;

const Trigger = styled.h2`
  padding: 0;
  margin: 0;
  font-family: Poppins, sans-serif;
  font-style: normal;
  font-weight: 600;
  font-size: 14px;
  line-height: 20px;
  color: #15151a;
`;

const Dropdown = styled(EFSelectDropdown)`
  svg {
    font-style: normal !important;
    font-weight: normal !important;
    font-size: 14px !important;
    line-height: 14px !important;
    color: #747d8b !important;
  }
`;

const DropdownItem = styled.div`
  padding: 8px 15px;

  p {
    margin: 0;
    padding: 0;
    font-family: Poppins, sans-serif;
    font-style: normal;
    font-weight: 600;
    font-size: 12px;
    line-height: 20px;
    color: rgba(10, 12, 18, 0.6);
  }
`;

const Grid = styled.div`
  display: grid;
  padding: 10px;
  grid-template-columns: 2fr 1fr 1fr;
`;

const CollapsedMenuWrapper = styled.div`
  height: 500px;
  overflow: scroll;
  padding: 0 20px;
`;

const Cell = styled.p`
  margin: 0;
  padding: 8px ${props => (props.position === "right" && !props.header ? "5px" : "0")} 8px 0;
  text-align: ${props => props.position};
  font-family: Poppins, sans-serif;
  font-style: normal;
  font-weight: ${props => (props.header ? "500" : "normal")};
  font-size: 12px;
  line-height: 20px;
  border-top: ${props => (props.header ? "none" : "1px solid rgba(199, 211, 234, 0.5)")};

  color: ${props => (props.header ? "#15151A" : "#12151D")};
`;

const OrgImage = styled.img`
  border: 1px solid #c7d3ea;
  border-radius: 4px;
  object-fit: cover;
  width: 20px;
  height: 20px;
  margin-right: 12px;
`;
export default ScoreboardMenu;
