import styled from "styled-components";
import { LeagueEvent, LeagueMatch } from "../../../../../graphql/interface/League";
import EFCard from "../../../../Cards/EFCard/EFCard";
import colors from "../../../../../styles/sharedStyledComponents/colors";
import { EFText, EFTextWeakest } from "../../../../../styles/sharedStyledComponents/sharedStyles";
import { formatDate } from "../../../../../helpers/GeneralHelper";
import HorizontalScroll from "../../../../HorizontalScroll/HorizontalScroll";

interface Props {
  event: LeagueEvent;
  onMatchClick?: (match: LeagueMatch) => void;
}

const SingleElimBracketContent = ({ event, onMatchClick = () => {} }: Props) => {
  // TODO: change when we add functionality for single elim to have multiple pools.
  const rounds = event?.pools?.[0]?.bracket?.rounds || [];

  return (
    <HorizontalScroll withLeftArrow={false} withRightArrow={false}>
      <EFCard shadow="none" overflowTheme="visible">
        <RoundsContainer>
          {rounds.map((round, index) => (
            <Round key={round.name}>
              <EFText>
                {index === rounds.length - 1 ? (
                  <>Championship</>
                ) : (
                  <>
                    Round {index + 1}
                    <RoundsTotal> of {rounds.length}</RoundsTotal>
                  </>
                )}
              </EFText>
              <EFTextWeakest>{formatDate(round.startDate)}</EFTextWeakest>
            </Round>
          ))}
        </RoundsContainer>
      </EFCard>
    </HorizontalScroll>
  );
};

const RoundsContainer = styled.div`
  display: flex;
`;

const Round = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  width: 190px;
  height: 48px;
  text-align: center;
  border-right: 1px solid ${colors.outerBorder};

  :last-child {
    border-right: none;
  }
`;

const RoundsTotal = styled.span`
  color: ${colors.textWeakest};
`;

export default SingleElimBracketContent;
