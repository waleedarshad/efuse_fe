import { gql } from "@apollo/client";
import { LeagueEvent } from "../../../../graphql/interface/League";

export interface GetEventBracketInfoVars {
  eventId: string;
}

export interface GetEventBracketInfoData {
  getLeagueEventById: LeagueEvent;
}

export const GET_EVENT_BRACKET_INFO = gql`
  query Query($eventId: String!) {
    getLeagueEventById(eventId: $eventId) {
      _id
      bracketType
      description
      gamesPerMatch
      rules
      league {
        _id
        imageUrl
        name
        owner {
          ... on Organization {
            _id
            name
            currentUserRole
            profileImage {
              url
            }
          }
        }
        game {
          title
        }
      }
      maxTeams
      name
      teams {
        _id
        name
        image {
          url
        }
        owner {
          ... on Organization {
            profileImage {
              url
            }
          }
        }
        members {
          _id
          user {
            name
            profilePicture {
              url
            }
          }
        }
      }
      startDate
      state
      timingMode
      pools {
        _id
        name
        teams {
          _id
          name
          image {
            url
          }
          owner {
            ... on Organization {
              profileImage {
                url
              }
            }
          }
        }
        bracket {
          _id
          name
          rounds {
            _id
            name
            createdAt
            startDate
            matches {
              finalScores {
                scores {
                  value
                  competitor {
                    ... on Team {
                      _id
                    }
                  }
                }
              }
              _id
              teams {
                _id
                name
                image {
                  url
                }
                owner {
                  ... on Organization {
                    _id
                    profileImage {
                      url
                    }
                    _id
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`;
