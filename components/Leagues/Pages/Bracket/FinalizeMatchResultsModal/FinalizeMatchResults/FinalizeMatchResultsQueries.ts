import { gql } from "@apollo/client";
import { LeagueGameScore, SubmittedScoreType } from "../../../../../../graphql/interface/League";

export interface GetScoresVars {
  matchId: string;
}

export interface GetScoresData {
  getLeagueGameScores: LeagueGameScore[];
}

export const GET_SCORES = gql`
  query Query($matchId: String!) {
    getLeagueGameScores(matchId: $matchId) {
      _id
      gameNumber
      image
      type
      scores {
        _id
        value
        competitor {
          ... on Team {
            _id
          }
        }
      }
      creator {
        __typename
        ... on Team {
          _id
        }
        ... on Organization {
          _id
        }
      }
    }
  }
`;

export interface CreateFinalizedScoreVars {
  submitLeagueGameScoreGameScoreBody: {
    creator: string;
    gameNumber: number;
    type: SubmittedScoreType;
    creatorType: "organizations" | "users";
    match: string;
  };
  submitLeagueGameScoreCompetitorScores: {
    value: number;
    competitor: string;
    competitorType: "team" | "teamMember";
  }[];
}

export const CREATE_FINALIZED_SCORE = gql`
  mutation SubmitLeagueGameScoreMutation(
    $submitLeagueGameScoreGameScoreBody: CreateLeagueGameScoreBody!
    $submitLeagueGameScoreCompetitorScores: [LeagueCompetitorScoreBody]!
  ) {
    submitLeagueGameScore(
      gameScoreBody: $submitLeagueGameScoreGameScoreBody
      competitorScores: $submitLeagueGameScoreCompetitorScores
    ) {
      _id
    }
  }
`;

export interface UpdateScoreVars {
  updateLeagueGameScoreGameScoreId: string;
  updateLeagueGameScoreGameScoreBody: {
    type: SubmittedScoreType;
    image?: string;
  };
  updateLeagueGameScoreCompetitorScores: {
    _id: string;
    value: number;
  }[];
}

export const UPDATE_SCORE = gql`
  mutation UpdateLeagueGameScoreMutation(
    $updateLeagueGameScoreGameScoreId: String!
    $updateLeagueGameScoreGameScoreBody: UpdateLeagueGameScoreBody!
    $updateLeagueGameScoreCompetitorScores: [UpdateCompetitorScoreBody]
  ) {
    updateLeagueGameScore(
      gameScoreId: $updateLeagueGameScoreGameScoreId
      gameScoreBody: $updateLeagueGameScoreGameScoreBody
      competitorScores: $updateLeagueGameScoreCompetitorScores
    ) {
      _id
      type
      scores {
        _id
        value
        competitor {
          ... on Team {
            _id
          }
        }
      }
    }
  }
`;
