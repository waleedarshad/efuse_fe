import { useMutation, useQuery } from "@apollo/client";
import React, { useEffect, useState } from "react";

import styled from "styled-components";
import {
  CREATE_FINALIZED_SCORE,
  CreateFinalizedScoreVars,
  GET_SCORES,
  GetScoresData,
  GetScoresVars,
  UPDATE_SCORE,
  UpdateScoreVars
} from "./FinalizeMatchResultsQueries";
import { computeGameWinner, computeOverallWinner, parseGameScoreData } from "./parseGameScoreDisplayData";
import EFScoringTable from "../../../../../EFScoringTable/EFScoringTable";
import CompareScores from "./CompareScores";
import EFRectangleButton from "../../../../../Buttons/EFRectangleButton/EFRectangleButton";
import { SubmittedScoreType, Team } from "../../../../../../graphql/interface/League";
import { sendNotification } from "../../../../../../helpers/FlashHelper";

interface Props {
  matchId: string;
  gamesPerMatch: number;
  leagueOwnerOrg: string;
  teams: Team[];
  isViewOnly?: boolean;
}

const FinalizeMatchResults = ({ matchId, gamesPerMatch, leagueOwnerOrg, teams, isViewOnly }: Props) => {
  const { data } = useQuery<GetScoresData, GetScoresVars>(GET_SCORES, { variables: { matchId } });
  const scores = data?.getLeagueGameScores;
  const gameScoreData = parseGameScoreData(gamesPerMatch, scores, teams);

  const [createFinalizedScore] = useMutation<any, CreateFinalizedScoreVars>(CREATE_FINALIZED_SCORE, {
    refetchQueries: [{ query: GET_SCORES, variables: { matchId } }]
  });

  const [updateScore] = useMutation<any, UpdateScoreVars>(UPDATE_SCORE);

  const [selectedGame, setSelectedGame] = useState<number | undefined>();
  const [games, setGames] = useState(gameScoreData);
  const [gamesUpdated, setGamesUpdated] = useState({});

  useEffect(() => {
    setGames(gameScoreData);
    setGamesUpdated({});
  }, [scores]);

  const onEditScore = (gameNumber, teamId, newValue) => {
    setGamesUpdated(prevState => ({ ...prevState, [gameNumber]: true }));
    setGames(prevState => {
      const newScores = {
        ...prevState[gameNumber].scores,
        [teamId]: { ...prevState[gameNumber].scores[teamId], score: newValue }
      };
      return {
        ...prevState,
        [gameNumber]: { ...prevState[gameNumber], scores: newScores, standingWinner: computeGameWinner(newScores) }
      };
    });
  };

  const saveGameScore = gameNumber => {
    const scoreData = gameScoreData?.[gameNumber];
    if (scoreData?.type === SubmittedScoreType.FINALIZED || scoreData?.type === SubmittedScoreType.CONFLICT) {
      return updateScore({
        variables: {
          updateLeagueGameScoreGameScoreId: scoreData._id,
          updateLeagueGameScoreGameScoreBody: { type: SubmittedScoreType.FINALIZED },
          updateLeagueGameScoreCompetitorScores: teams.map(team => ({
            _id: games?.[gameNumber]?.scores?.[team._id]?._id,
            value: games?.[gameNumber]?.scores?.[team._id]?.score || 0
          }))
        }
      });
    }
    return createFinalizedScore({
      variables: {
        submitLeagueGameScoreGameScoreBody: {
          creator: leagueOwnerOrg,
          gameNumber: parseInt(gameNumber, 10),
          type: SubmittedScoreType.FINALIZED,
          creatorType: "organizations",
          match: matchId
        },
        submitLeagueGameScoreCompetitorScores: teams.map(team => ({
          value: games?.[gameNumber]?.scores?.[team._id]?.score || 0,
          competitor: team._id,
          competitorType: "team"
        }))
      }
    });
  };

  const onSave = () => {
    Promise.all(Object.keys(gamesUpdated).map(gameNumber => saveGameScore(gameNumber))).then(() => {
      sendNotification("Scores saved.", "success", "Success");
    });
  };

  return data ? (
    <>
      <EFScoringTable
        teams={teams}
        standingWinner={computeOverallWinner(games, teams)}
        games={games}
        gamesPerMatch={gamesPerMatch}
        selectedGame={selectedGame}
        onSelectGame={setSelectedGame}
        onEditScore={onEditScore}
        isViewOnly={isViewOnly}
      />

      <hr />
      {selectedGame && <CompareScores gameNumber={selectedGame} teams={teams} submittedScores={scores} />}
      <ButtonsSection>
        <Button disabled={isViewOnly} colorTheme="primary" text="Save" onClick={onSave} />
      </ButtonsSection>
    </>
  ) : (
    <div>Loading Data...</div>
  );
};

const Button = styled(EFRectangleButton).attrs({ shadowTheme: "none" })`
  margin-left: 10px;
  width: 113px;
  margin-top: 20px;
`;

const ButtonsSection = styled.div`
  display: flex;
  flex-direction: row-reverse;
`;

export default FinalizeMatchResults;
