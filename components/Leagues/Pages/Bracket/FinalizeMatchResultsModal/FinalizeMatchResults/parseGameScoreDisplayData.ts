import maxBy from "lodash/maxBy";
import { SubmittedScoreType } from "../../../../../../graphql/interface/League";

export const computeGameWinner = teamScores => {
  return maxBy(Object.keys(teamScores), key => teamScores[key].score);
};

export const computeOverallWinner = (games, teams) => {
  if (games && teams) {
    // @ts-ignore
    return maxBy(teams, team => Object.values(games).filter(game => game.standingWinner === team._id)?.length)?._id;
  }

  return {};
};

export const parseGameScoreData = (gamesPerMatch, submittedScores, teams) => {
  if (gamesPerMatch && submittedScores && teams) {
    const games = {};

    const gameNumbers = [...Array(gamesPerMatch)].map((_, index) => index + 1);
    gameNumbers.forEach(gameNumber => {
      const teamScores = {};
      const submittedScoresForGame = submittedScores.filter(it => it.gameNumber === gameNumber);
      const submittedScoreToUse =
        submittedScoresForGame.find(it => it.type === SubmittedScoreType.FINALIZED) ||
        submittedScoresForGame.find(it => it.type === SubmittedScoreType.CONFLICT) ||
        submittedScoresForGame[0];

      teams?.forEach(team => {
        const score = submittedScoreToUse?.scores?.find(it => it.competitor?._id === team?._id);
        teamScores[team._id] = {
          score: score?.value,
          teamId: team?._id,
          _id: score?._id
        };
      });

      games[gameNumber] = {
        gameNumber,
        _id: submittedScoreToUse?._id,
        scores: teamScores,
        type: submittedScoreToUse?.type,
        standingWinner: computeGameWinner(teamScores)
      };
    });

    return games;
  }
  return {};
};
