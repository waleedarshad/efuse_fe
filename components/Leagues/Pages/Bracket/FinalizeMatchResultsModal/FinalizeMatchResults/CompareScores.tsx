import React from "react";
import styled from "styled-components";
import { EFHeading2 } from "../../../../../../styles/sharedStyledComponents/sharedStyles";
import { LeagueGameScore, SubmittedScoreType, Team } from "../../../../../../graphql/interface/League";
import TeamScoreSubmission from "./TeamScoreSubmission";

interface Props {
  gameNumber: number;
  teams: Team[];
  submittedScores: LeagueGameScore[];
}
const CompareScores = ({ gameNumber, teams, submittedScores }: Props) => {
  const findTeamSubmission = team =>
    submittedScores?.find(
      it => it.creator?._id === team?._id && it.gameNumber === gameNumber && it.type === SubmittedScoreType.SUBMITTED
    );
  return (
    <div>
      <EFHeading2>Game {gameNumber}</EFHeading2>
      <Container>
        <TeamScoreSubmission ownTeam={teams[0]} opposingTeam={teams[1]} submittedScore={findTeamSubmission(teams[0])} />
        <TeamScoreSubmission ownTeam={teams[1]} opposingTeam={teams[0]} submittedScore={findTeamSubmission(teams[1])} />
      </Container>
    </div>
  );
};

const Container = styled.div`
  display: flex;
  justify-content: space-between;
`;

export default CompareScores;
