import styled from "styled-components";
import React from "react";
import { LeagueGameScore, Team } from "../../../../../../graphql/interface/League";
import { EFText, textNormal } from "../../../../../../styles/sharedStyledComponents/sharedStyles";
import EFCard from "../../../../../Cards/EFCard/EFCard";
import EFSquareImage from "../../../../../EFSquareImage/EFSquareImage";
import colors from "../../../../../../styles/sharedStyledComponents/colors";
import EFImage from "../../../../../EFImage/EFImage";

interface Props {
  ownTeam: Team;
  opposingTeam: Team;
  submittedScore: LeagueGameScore;
}

const TeamScoreSubmission = ({ ownTeam, opposingTeam, submittedScore }: Props) => {
  const ownScore = submittedScore?.scores?.find(it => it.competitor?._id === ownTeam?._id);
  const opponentScore = submittedScore?.scores?.find(it => it.competitor?._id === opposingTeam?._id);

  return (
    <Submission>
      <EFText>{ownTeam?.name}</EFText>
      <VsScoreCard>
        <TeamScore score={ownScore} img={ownTeam?.owner?.profileImage?.url} />
        <VsScoreCardEntry>
          <VsLine />
          <VsText>VS</VsText>
          <VsLine />
        </VsScoreCardEntry>
        <TeamScore score={opponentScore} img={opposingTeam?.owner?.profileImage?.url} />
      </VsScoreCard>
      <Screenshot src={submittedScore?.image} alt="score screenshot" />
    </Submission>
  );
};

export default TeamScoreSubmission;

const TeamScore = ({ score, img }) => (
  <VsScoreCardEntry>
    {score ? (
      <>
        <EFSquareImage border src={img} size={40} />
        <ScoreBox>
          <EFText>{score.value}</EFText>
        </ScoreBox>
      </>
    ) : (
      <EFText>No Submission</EFText>
    )}
  </VsScoreCardEntry>
);

const Screenshot = styled(EFImage)`
  width: 100%;
  border-radius: 10px;
  margin-top: 8px;
`;

const VsText = styled.div`
  ${textNormal};
  color: ${colors.textWeak};
`;

const VsLine = styled.div`
  border-top: 1px solid ${colors.textWeak};
  height: 1px;
  width: 10px;
  margin: 4px;
`;

const Submission = styled.div`
  width: 45%;
`;

const VsScoreCard = styled(EFCard).attrs(() => ({ widthTheme: "fullWidth", shadow: "none" }))`
  display: flex;
  padding: 10px 20px;
  justify-content: space-between;
  align-items: center;
  height: 60px;
`;

const VsScoreCardEntry = styled.div`
  display: flex;
  align-items: center;
`;

const ScoreBox = styled(EFCard).attrs(() => ({ shadow: "none" }))`
  width: 60px;
  height: 40px;
  margin-left: 8px;
  display: flex;
  align-items: center;
  justify-content: center;
`;
