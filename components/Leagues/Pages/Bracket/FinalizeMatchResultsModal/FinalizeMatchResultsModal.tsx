import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircle } from "@fortawesome/pro-solid-svg-icons";
import React from "react";
import styled from "styled-components";
import moment from "moment";

import EFPrimaryModal from "../../../../Modals/EFPrimaryModal/EFPrimaryModal";

import FinalizeMatchResults from "./FinalizeMatchResults/FinalizeMatchResults";
import { Team } from "../../../../../graphql/interface/League";

interface FinalizeMatchResultsModalProps {
  isOpen: boolean;
  onClose: () => void;
  gameTitle: string;
  eventStartDate: Date;
  gamesPerMatch: number;
  matchId: string;
  teams: Team[];
  leagueOwnerOrg: string;
  isViewOnly?: boolean;
}

const FinalizeMatchResultsModal = ({
  isOpen,
  onClose,
  gameTitle,
  eventStartDate,
  matchId,
  gamesPerMatch,
  leagueOwnerOrg,
  teams,
  isViewOnly
}: FinalizeMatchResultsModalProps) => (
  <EFPrimaryModal
    title="Match Results"
    subtitle={
      <>
        {`Game ${gameTitle}`}
        <SubtitleDot>
          <FontAwesomeIcon icon={faCircle} />
        </SubtitleDot>
        {`Completed ${moment(eventStartDate).format("MMM.DD | h:mm A")}`}
      </>
    }
    isOpen={isOpen}
    onClose={onClose}
    allowBackgroundClickClose={false}
    widthTheme="large"
  >
    <FinalizeMatchResults
      matchId={matchId}
      gamesPerMatch={gamesPerMatch}
      leagueOwnerOrg={leagueOwnerOrg}
      teams={teams}
      isViewOnly={isViewOnly}
    />
  </EFPrimaryModal>
);

const SubtitleDot = styled.span`
  margin: 0 10px;
  font-size: 4px;
  vertical-align: middle;
`;

export default FinalizeMatchResultsModal;
