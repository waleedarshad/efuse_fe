import React, { useState } from "react";
import { LeagueEvent, LeagueMatch } from "../../../../graphql/interface/League";
import SubmitMatchResultsModal from "./SubmitMatchResultsModal/SubmitMatchResultsModal";
import BracketContent from "./BracketContent/BracketContent";

interface BracketTabProps {
  event: LeagueEvent;
}

const BracketTab = ({ event }: BracketTabProps) => {
  const [selectedMatch, setSelectedMatch] = useState<LeagueMatch>();

  return event ? (
    <>
      <BracketContent event={event} onMatchClick={setSelectedMatch} />
      <SubmitMatchResultsModal
        gameName={event.league.game.title}
        gamesPerMatch={event.gamesPerMatch}
        matchId={selectedMatch?._id}
        teams={selectedMatch?.teams}
        onClose={() => setSelectedMatch(null)}
        isOpen={!!selectedMatch}
        eventStartDate={event.startDate}
      />
    </>
  ) : (
    <></>
  );
};

export default BracketTab;
