import setWith from "lodash/setWith";

const transformScores = (rawAdminFinalizedScores = [], rawTeamSubmittedScores = []) => {
  const adminFinalizedScores = {};
  const submittedScoresByTeam = {};

  rawAdminFinalizedScores.forEach(gameScore => {
    const { gameNumber } = gameScore;

    gameScore.scores.forEach(score => {
      const teamId = score.competitor._id;
      const scoreValue = score.value;

      setWith(adminFinalizedScores, [gameNumber, teamId], scoreValue, Object);
    });
  });

  rawTeamSubmittedScores.forEach(gameScore => {
    const { gameNumber } = gameScore;
    const scoreCreatorId = gameScore.creator._id;
    const scoreImageUrl = gameScore.image;
    const gameScoreId = gameScore._id;

    setWith(submittedScoresByTeam, [scoreCreatorId, gameNumber, "scoreImageUrl"], scoreImageUrl, Object);
    setWith(submittedScoresByTeam, [scoreCreatorId, gameNumber, "gameScoreId"], gameScoreId, Object);

    gameScore.scores.forEach(score => {
      const teamId = score.competitor._id;
      const scoreValue = score.value;
      const scoreId = score._id;

      setWith(submittedScoresByTeam, [scoreCreatorId, gameNumber, teamId, "scoreValue"], scoreValue, Object);
      setWith(submittedScoresByTeam, [scoreCreatorId, gameNumber, teamId, "scoreId"], scoreId, Object);
    });
  });

  return { adminFinalizedScores, submittedScoresByTeam };
};

export default transformScores;
