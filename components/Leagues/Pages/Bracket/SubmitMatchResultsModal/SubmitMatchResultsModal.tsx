import React, { useState } from "react";
import styled from "styled-components";
import useGetScoringData from "./useGetScoringData";
import transformScores from "./transformScores";
import useGetMyTeamId from "./useGetMyTeamId";
import EFSpinnerOverlay from "../../../../Spinners/EFSpinnerOverlay";
import EFWizard from "../../../../EFWizard/EFWizard";
import MyTeamSubmissionView from "./MyTeamSubmissionView/MyTeamSubmissionView";
import EFModal from "../../../../Modals/EFModal/EFModal";
import OpponentTeamScoreView from "./OpponentTeamScoreView/OpponentTeamScoreView";
import SubmitScoreProvider from "./SubmitScoreContext";
import FinalizeMatchResultsModal from "../FinalizeMatchResultsModal/FinalizeMatchResultsModal";

const SubmitMatchResultsModal = ({ gameName, gamesPerMatch, matchId, teams, onClose, isOpen, eventStartDate }) => {
  if (!isOpen) {
    return <></>;
  }

  const [isDataReady, setIsDataReady] = useState(false);
  const { rawAdminFinalizedScores, rawTeamSubmittedScores, isGettingScores } = useGetScoringData(matchId);

  const { adminFinalizedScores, submittedScoresByTeam } = transformScores(
    rawAdminFinalizedScores,
    rawTeamSubmittedScores
  );

  const teamOrgOwners = teams.map(team => ({
    teamId: team._id,
    orgId: team.owner._id
  }));

  const { myTeams, isFetchingOrgRoles } = useGetMyTeamId(teamOrgOwners);

  if (myTeams.length === 0 && !isFetchingOrgRoles) {
    return (
      <FinalizeMatchResultsModal
        isOpen={isOpen}
        onClose={onClose}
        gameTitle={gameName}
        eventStartDate={eventStartDate}
        gamesPerMatch={gamesPerMatch}
        matchId={matchId}
        teams={teams}
        leagueOwnerOrg={null}
        isViewOnly
      />
    );
  }

  // score for the first team for now. In the future, we will allow the user to select the team when the user has roles for both
  const myTeamId = myTeams[0];
  const otherTeamId = teamOrgOwners.find(team => team.teamId !== myTeamId)?.teamId;

  const myTeamSubmittedScores = submittedScoresByTeam[myTeamId] || {};
  const otherTeamSubmittedScores = submittedScoresByTeam[otherTeamId] || {};

  const teamsInfo = teams.reduce((acc, team) => {
    return {
      ...acc,
      [team._id]: { imageUrl: team.owner.profileImage.url, orgOwnerId: team.owner._id, teamName: team.name }
    };
  }, {});

  if (!isDataReady && myTeamSubmittedScores && otherTeamSubmittedScores && teamsInfo) {
    setIsDataReady(true);
  }

  if (isFetchingOrgRoles) {
    return <></>;
  }

  return (
    <SubmitMatchModal
      allowBackgroundClickClose={false}
      displayCloseButton
      widthTheme="extraSmall"
      onClose={onClose}
      isOpen={isOpen}
    >
      <ModalContainer>
        <h1>Match Results</h1>
        <h2>{`Game ${gameName}`}</h2>
        <EFSpinnerOverlay isLoading={isGettingScores || isFetchingOrgRoles}>
          <SubmitScoreProvider>
            <EFWizard>
              <MyTeamSubmissionView
                teamsInfo={teamsInfo}
                gamesPerMatch={gamesPerMatch}
                adminFinalizedScores={adminFinalizedScores}
                myTeamSubmittedScores={myTeamSubmittedScores}
                myTeamId={myTeamId}
                otherTeamId={otherTeamId}
                matchId={matchId}
              />
              <OpponentTeamScoreView
                otherTeamSubmittedScores={otherTeamSubmittedScores}
                teamsInfo={teamsInfo}
                myTeamId={myTeamId}
                otherTeamId={otherTeamId}
              />
            </EFWizard>
          </SubmitScoreProvider>
        </EFSpinnerOverlay>
      </ModalContainer>
    </SubmitMatchModal>
  );
};

const SubmitMatchModal = styled(EFModal)`
  max-width: none;
  width: 380px;
`;

const ModalContainer = styled.div`
  padding: 30px;
  display: flex;
  flex-direction: column;
  min-height: 300px;

  h1 {
    padding: 0;
    margin: 0;
    font-family: Poppins, sans-serif;
    font-style: normal;
    font-weight: 500;
    font-size: 24px;
    line-height: 30px;
    color: #12151d;
  }

  h2 {
    margin: 0;
    padding: 0 0 10px 0;
    font-family: Poppins, sans-serif;
    font-style: normal;
    font-weight: normal;
    font-size: 12px;
    line-height: 20px;
    color: #12151d;
  }
`;

export default SubmitMatchResultsModal;
