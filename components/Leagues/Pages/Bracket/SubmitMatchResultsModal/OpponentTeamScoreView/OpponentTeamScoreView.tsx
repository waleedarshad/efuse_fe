import React, { useContext } from "react";
import isNil from "lodash/isNil";
import styled from "styled-components";
import { EFWizardContext } from "../../../../../EFWizard/EFWizard";
import { SubmitScoreContext } from "../SubmitScoreContext";
import EFRectangleButton from "../../../../../Buttons/EFRectangleButton/EFRectangleButton";
import EFVersusScores from "../../../../../EFVersusScores/EFVersusScores";
import ScoreImageUpload from "../MyTeamSubmissionView/ScoreImageUpload";

const OpponentTeamScoreView = ({ otherTeamSubmittedScores, teamsInfo, myTeamId, otherTeamId }) => {
  const { goPrevStep } = useContext(EFWizardContext);
  const { gameNumberSelected } = useContext(SubmitScoreContext);

  const otherTeamScore = otherTeamSubmittedScores?.[gameNumberSelected]?.[otherTeamId];
  const myTeamScore = otherTeamSubmittedScores?.[gameNumberSelected]?.[myTeamId];
  const image = otherTeamSubmittedScores?.[gameNumberSelected]?.scoreImageUrl;

  const noScoreProvided = isNil(otherTeamScore) && isNil(myTeamScore);

  return (
    <Container>
      {noScoreProvided && <Message>Opponent has not provided scores</Message>}
      {!noScoreProvided && (
        <>
          <EFVersusScores
            title={`${teamsInfo[otherTeamId].teamName} Submission`}
            subtitle={`Game ${gameNumberSelected}`}
            leftTeamData={{
              imageUrl: teamsInfo[otherTeamId].imageUrl,
              score: otherTeamScore.scoreValue,
              onScoreChange: () => {},
              isInputDisabled: true,
              teamName: teamsInfo[otherTeamId].teamName
            }}
            rightTeamData={{
              imageUrl: teamsInfo[myTeamId].imageUrl,
              score: myTeamScore.scoreValue,
              onScoreChange: () => {},
              isInputDisabled: true,
              teamName: teamsInfo[myTeamId].teamName
            }}
          />
          <ImageContainer>
            <ScoreImageUpload isDisabled imageUrl={image} />
          </ImageContainer>
        </>
      )}

      <EFRectangleButton shadowTheme="none" colorTheme="light" text="Go Back" onClick={goPrevStep} />
    </Container>
  );
};

export default OpponentTeamScoreView;

const Container = styled.div`
  display: flex;
  flex-direction: column;
`;

const Message = styled.p`
  padding: 100px 0;
  margin: 0 auto;
  font-family: Poppins, sans-serif;
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  line-height: 22px;

  color: #15151a;
`;

const ImageContainer = styled.div`
  padding: 10px 0 20px 0;
`;
