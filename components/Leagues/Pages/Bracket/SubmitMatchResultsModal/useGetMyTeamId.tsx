import intersection from "lodash/intersection";
import { useGetMyOrgRoles } from "./queries";
import { OrganizationUserRole } from "../../../../../graphql/interface/Organization";

const useGetMyTeamId = (teams: { teamId: string; orgId: string }[]) => {
  const [rawMyOrgRoles, isFetchingOrgRoles] = useGetMyOrgRoles();

  const myOrgRoles = rawMyOrgRoles?.reduce((acc, orgRole) => {
    const orgId = orgRole.organization._id;
    const { role } = orgRole;

    return { ...acc, [orgId]: [...(acc[orgId] || []), role] };
  }, {});

  const myTeams = teams.filter(team => {
    const hasCommonRoles = intersection(
      [OrganizationUserRole.CAPTAIN, OrganizationUserRole.OWNER],
      myOrgRoles?.[team.orgId] || []
    );

    return hasCommonRoles.length > 0;
  });

  return { myTeams: myTeams.map(teamObj => teamObj.teamId), isFetchingOrgRoles };
};

export default useGetMyTeamId;
