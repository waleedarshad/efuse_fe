import React, { createContext, useState } from "react";

export const SubmitScoreContext = createContext();

const SubmitScoreProvider = ({ children }) => {
  const [gameNumberSelected, setGameNumberSelected] = useState(0);

  const contextProps = {
    selectGameNumber: setGameNumberSelected,
    gameNumberSelected
  };

  return <SubmitScoreContext.Provider value={contextProps}>{children}</SubmitScoreContext.Provider>;
};

export default SubmitScoreProvider;
