import React, { useEffect, useState } from "react";
import range from "lodash/range";
import DropdownGameItem from "./DropdownGameItem";

const buildGamesDropdown = (
  gamesPerMatch,
  adminFinalizedScores,
  myTeamId,
  otherTeamId,
  myTeamImage,
  otherTeamImage,
  myTeamSubmittedScores
) => {
  const [dropdownOptions, setDropdownOptions] = useState([]);

  useEffect(() => {
    const localDropdownOptions = range(1, gamesPerMatch + 1).map(gameNumber => {
      if (adminFinalizedScores[gameNumber]) {
        const myTeamScore = adminFinalizedScores[gameNumber]?.[myTeamId];
        const otherTeamScore = adminFinalizedScores[gameNumber]?.[otherTeamId];

        return {
          label: (
            <DropdownGameItem
              gameNumber={gameNumber}
              leftTeamScore={myTeamScore}
              leftTeamImage={myTeamImage}
              rightTeamImage={otherTeamImage}
              rightTeamScore={otherTeamScore}
              isFinalizedScore
            />
          ),
          value: {
            gameNumber,
            status: "finalized"
          }
        };
      }
      if (myTeamSubmittedScores[gameNumber]) {
        const myTeamScore = myTeamSubmittedScores[gameNumber]?.[myTeamId].scoreValue;
        const myTeamScoreId = myTeamSubmittedScores[gameNumber]?.[myTeamId].scoreId;
        const otherTeamScore = myTeamSubmittedScores[gameNumber]?.[otherTeamId].scoreValue;
        const otherTeamScoreId = myTeamSubmittedScores[gameNumber]?.[otherTeamId].scoreId;
        const gameScoreId = myTeamSubmittedScores[gameNumber]?.gameScoreId;

        return {
          label: (
            <DropdownGameItem
              gameNumber={gameNumber}
              leftTeamScore={myTeamScore}
              leftTeamImage={myTeamImage}
              rightTeamImage={otherTeamImage}
              rightTeamScore={otherTeamScore}
            />
          ),
          value: {
            gameNumber,
            status: "submitted",
            gameScoreId,
            myTeamScoreId,
            otherTeamScoreId
          }
        };
      }
      return {
        label: <DropdownGameItem gameNumber={gameNumber} needsScoring />,
        value: {
          gameNumber,
          status: "new"
        }
      };
    });

    setDropdownOptions(localDropdownOptions);
  }, [myTeamSubmittedScores, adminFinalizedScores]);

  return dropdownOptions;
};

export default buildGamesDropdown;
