import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faImage } from "@fortawesome/pro-regular-svg-icons";
import React from "react";
import UploadImage from "../../../../../DirectUpload/UploadImage/UploadImage";
import WithUploadContext from "../../../../../DirectUpload/UploadContext";

const LEAGUES_UPLOAD_DIRECTORY = "uploads/leagues/scores/";

interface ScoreImageUploadPros {
  imageUrl: string;
  onImageUploaded?: (url: string) => void;
  isDisabled?: boolean;
}

const ScoreImageUpload = ({ imageUrl, onImageUploaded = () => {}, isDisabled = false }: ScoreImageUploadPros) => {
  const renderContent = () => {
    if (imageUrl) {
      return <img src={imageUrl} alt="uploaded media" />;
    }
    return (
      <InnerContainer>
        <StyledIcon icon={faImage} />
        <p>Upload image</p>
      </InnerContainer>
    );
  };

  return (
    <WithUploadContext>
      <UploadImage
        isDisabled={isDisabled}
        onFileUpload={file => onImageUploaded(file.url)}
        isMediaUploaded={!!imageUrl}
        directory={LEAGUES_UPLOAD_DIRECTORY}
      >
        <UploadContainer isImageUploaded={!!imageUrl}>{renderContent()}</UploadContainer>
      </UploadImage>
    </WithUploadContext>
  );
};

export default ScoreImageUpload;

const UploadContainer = styled.div`
  width: 100%;
  height: 180px;
  display: flex;
  justify-content: center;
  align-items: center;
  background: ${props =>
    !props.isImageUploaded &&
    "url(\"data:image/svg+xml,%3csvg width='100%25' height='100%25' xmlns='http://www.w3.org/2000/svg'%3e%3crect width='100%25' height='100%25' fill='none' rx='10' ry='10' stroke='%23C7D3EAFF' stroke-width='2' stroke-dasharray='10' stroke-dashoffset='0' stroke-linecap='square'/%3e%3c/svg%3e\")"};
  border-radius: 10px;
  overflow: hidden;

  border: ${props => props.isImageUploaded && "1px solid #c7d3ea"};

  img {
    height: 100%;
    width: 100%;
    object-fit: contain;
  }

  p {
    transition: all 200ms;
    font-family: Poppins, sans-serif;
    font-style: normal;
    font-weight: 500;
    font-size: 16px;
    line-height: 22px;
    color: #15151a;
    text-decoration-color: transparent;
  }

  &:hover {
    p {
      text-decoration-line: underline;
      text-decoration-color: #15151a;
    }
  }
`;

const InnerContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const StyledIcon = styled(FontAwesomeIcon)`
  font-style: normal;
  font-weight: normal;
  font-size: 28px;
  line-height: 22px;
  margin-bottom: 10px;

  color: #000000;
`;
