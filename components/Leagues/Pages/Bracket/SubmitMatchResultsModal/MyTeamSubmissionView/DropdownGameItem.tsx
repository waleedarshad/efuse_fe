import React from "react";
import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft, faChevronRight, faEquals } from "@fortawesome/pro-regular-svg-icons";
import EFImage from "../../../../../EFImage/EFImage";
import EFResponsiveChip from "../../../../../EFResponsiveChip/EFResponsiveChip";

const DropdownGameItem = ({
  gameNumber,
  needsScoring = false,
  leftTeamImage,
  leftTeamScore,
  rightTeamImage,
  rightTeamScore,
  isFinalizedScore = false
}) => {
  const renderLessThanOrGreaterThanSign = () => {
    let icon = faEquals;

    if (leftTeamScore > rightTeamScore) {
      icon = faChevronRight;
    } else if (leftTeamScore < rightTeamScore) {
      icon = faChevronLeft;
    }

    return <StyledLessMore isMyTeamWinning={leftTeamScore > rightTeamScore} icon={icon} />;
  };

  return (
    <Container>
      <GameStatusContainer>
        <GameNumber>Game {gameNumber}</GameNumber>
        {isFinalizedScore && <FinalizedChip badgeType="Finalized" size="small" />}
      </GameStatusContainer>
      {needsScoring ? (
        <NeedsScoring>Needs scoring</NeedsScoring>
      ) : (
        <ScoreSection>
          <TeamImage src={leftTeamImage} alt="team logo" />
          <Score>{leftTeamScore}</Score>
          {renderLessThanOrGreaterThanSign()}
          <Score>{rightTeamScore}</Score>
          <TeamImage src={rightTeamImage} alt="team logo" />
        </ScoreSection>
      )}
    </Container>
  );
};

const Container = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  justify-content: space-between;
  align-items: center;
  padding: 5px 10px;
`;

const ScoreSection = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;

  > * {
    margin-right: 8px;
  }

  > :last-child {
    margin-right: 0;
  }
`;

const TeamImage = styled(EFImage)`
  width: 20px;
  height: 20px;
  border: 1px solid #ced7e7;
  border-radius: 4px;
`;

const GameNumber = styled.h3`
  margin: 0;
  padding: 0;
  font-family: Poppins, sans-serif;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 20px;
  color: #15151a;
`;

const Score = styled.p`
  margin-top: 0;
  margin-bottom: 0;
  font-family: Poppins, sans-serif;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 20px;
  color: rgba(18, 21, 29, 0.6);
`;

const NeedsScoring = styled.p`
  margin: 0;
  padding: 0;
  font-family: Poppins, sans-serif;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 20px;
  color: rgba(18, 21, 29, 0.6);
`;

const StyledLessMore = styled(FontAwesomeIcon)`
  font-style: normal;
  font-weight: normal;
  font-size: 8px;
  line-height: 20px;

  color: ${props => (props.isMyTeamWinning ? "#30CC49" : "#ff0000")};
`;

const GameStatusContainer = styled.div`
  display: flex;
  align-items: center;
`;

const FinalizedChip = styled(EFResponsiveChip)`
  text-transform: none;
  background: linear-gradient(0deg, rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6)), #30cc49;
  border: 2px solid #30cc49;
  box-sizing: border-box;
  border-radius: 100px;
  margin-left: 10px;
`;

export default DropdownGameItem;
