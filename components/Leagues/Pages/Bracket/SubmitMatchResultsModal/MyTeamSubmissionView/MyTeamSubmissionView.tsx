import React, { useContext, useEffect, useRef, useState } from "react";
import styled from "styled-components";
import isNil from "lodash/isNil";
import { EFWizardContext } from "../../../../../EFWizard/EFWizard";
import EFSelectDropdown from "../../../../../Dropdowns/EFSelectDropdown/EFSelectDropdown";
import EFRectangleButton from "../../../../../Buttons/EFRectangleButton/EFRectangleButton";
import EFVersusScores from "../../../../../EFVersusScores/EFVersusScores";
import buildGamesDropdown from "./buildGamesDropdown";
import { useGetSubmitNewScoreMutation, useGetUpdateSubmittedScoreMutation } from "../queries";
import { sendNotification } from "../../../../../../helpers/FlashHelper";
import { SubmitScoreContext } from "../SubmitScoreContext";
import ScoreImageUpload from "./ScoreImageUpload";

const MyTeamSubmissionView = ({
  teamsInfo,
  gamesPerMatch,
  adminFinalizedScores,
  myTeamSubmittedScores,
  myTeamId,
  otherTeamId,
  matchId
}) => {
  const { goNextStep } = useContext(EFWizardContext);

  const [gameToExpand, setGameToExpand] = useState(null);
  const [myTeamScore, setMyTeamScore] = useState<string>("");
  const [otherTeamScore, setOtherTeamScore] = useState<string>("");
  const [image, setImage] = useState<string>("");

  const submitNewGameScore = useGetSubmitNewScoreMutation();
  const updateSubmittedGameScore = useGetUpdateSubmittedScoreMutation();
  const { selectGameNumber } = useContext(SubmitScoreContext);

  useEffect(() => {
    if (gameToExpand) {
      if (gameToExpand.status === "new") {
        clearForm();
      } else {
        setMyTeamScore(myTeamSubmittedScores[gameToExpand.gameNumber]?.[myTeamId].scoreValue);
        setOtherTeamScore(myTeamSubmittedScores[gameToExpand.gameNumber]?.[otherTeamId].scoreValue);
        setImage(myTeamSubmittedScores[gameToExpand.gameNumber]?.scoreImageUrl);
      }
    }
  }, [gameToExpand]);

  const onSubmit = () => {
    const myTeamScoreNumber = Number.parseInt(myTeamScore, 10);
    const otherTeamScoreNumber = Number.parseInt(otherTeamScore, 10);

    if (gameToExpand.status === "new") {
      submitNewGameScore({
        myTeamId,
        otherTeamId,
        myTeamScore: myTeamScoreNumber,
        otherTeamScore: otherTeamScoreNumber,
        gameNumber: gameToExpand.gameNumber,
        image,
        matchId
      })
        .then(() => sendNotification("Game score submitted", "success", "Bracket"))
        .catch(() => sendNotification("There was a problem submitting the score", "danger", "Bracket"));
    } else if (gameToExpand.status === "submitted") {
      updateSubmittedGameScore({
        gameScoreId: gameToExpand.gameScoreId,
        myTeamScoreId: gameToExpand.myTeamScoreId,
        otherTeamScoreId: gameToExpand.otherTeamScoreId,
        myTeamScore: myTeamScoreNumber,
        otherTeamScore: otherTeamScoreNumber,
        image,
        matchId
      })
        .then(() => sendNotification("Game score updated", "success", "Bracket"))
        .catch(() => sendNotification("There was a problem updating the score", "danger", "Bracket"));
    }

    findNextGameToExpand();
  };

  const clearForm = () => {
    setMyTeamScore("");
    setOtherTeamScore("");
    setImage("");
  };

  const myTeamImage = teamsInfo[myTeamId].imageUrl;
  const otherTeamImage = teamsInfo[otherTeamId].imageUrl;

  const gamesDropdownOptions = buildGamesDropdown(
    gamesPerMatch,
    adminFinalizedScores,
    myTeamId,
    otherTeamId,
    myTeamImage,
    otherTeamImage,
    myTeamSubmittedScores
  );

  useEffect(() => {
    if (gamesDropdownOptions) {
      findNextGameToExpand();
    }
  }, [gamesDropdownOptions]);

  const onSelectHandler = valueOfOptionClicked => {
    const gameOptionSelected = valueOfOptionClicked.gameNumber ? valueOfOptionClicked : null;

    setGameToExpand(gameOptionSelected);
    selectGameNumber(gameOptionSelected?.gameNumber);
  };

  const dropdownRef = useRef(null);

  const findNextGameToExpand = () => {
    const reversedOptions = [...gamesDropdownOptions].reverse();

    const firstGameNeedingScore = gamesDropdownOptions.find(option => option.value?.status === "new")?.value
      ?.gameNumber;
    const lastSubmittedGame = reversedOptions.find(option => option.value?.status === "submitted")?.value?.gameNumber;
    const lastFinalizedGame = reversedOptions.find(option => option.value?.status === "finalized")?.value?.gameNumber;

    const preSelectedGameNumber = firstGameNeedingScore || lastSubmittedGame || lastFinalizedGame;

    const optionSelector = option => option?.value?.gameNumber === preSelectedGameNumber;

    dropdownRef?.current?.selectOption(optionSelector);
  };

  return (
    <Container>
      <EFSelectDropdown ref={dropdownRef} disableCheckmark options={gamesDropdownOptions} onSelect={onSelectHandler} />
      {!isNil(gameToExpand) && (
        <>
          {gameToExpand.status === "new" && (
            <EFVersusScores
              title={`${teamsInfo[myTeamId].teamName} Submission`}
              subtitle={`Game ${gameToExpand.gameNumber}`}
              leftTeamData={{
                imageUrl: myTeamImage,
                onScoreChange: setMyTeamScore,
                teamName: teamsInfo[myTeamId].teamName,
                score: Number.parseInt(myTeamScore, 10)
              }}
              rightTeamData={{
                imageUrl: otherTeamImage,
                onScoreChange: setOtherTeamScore,
                teamName: teamsInfo[otherTeamId].teamName,
                score: Number.parseInt(otherTeamScore, 10)
              }}
            />
          )}

          {gameToExpand.status !== "new" && (
            <EFVersusScores
              title={`${teamsInfo[myTeamId].teamName} Submission`}
              subtitle={`Game ${gameToExpand.gameNumber}`}
              leftTeamData={{
                imageUrl: myTeamImage,
                onScoreChange: setMyTeamScore,
                score: Number.parseInt(myTeamScore, 10),
                isInputDisabled: gameToExpand.status === "finalized",
                teamName: teamsInfo[myTeamId].teamName
              }}
              rightTeamData={{
                imageUrl: otherTeamImage,
                onScoreChange: setOtherTeamScore,
                score: Number.parseInt(otherTeamScore, 10),
                isInputDisabled: gameToExpand.status === "finalized",
                teamName: teamsInfo[otherTeamId].teamName
              }}
            />
          )}

          <ScoreImageUpload
            isDisabled={gameToExpand.status === "finalized"}
            imageUrl={image || myTeamSubmittedScores[gameToExpand?.gameNumber]?.scoreImageUrl}
            onImageUploaded={setImage}
          />

          <ButtonsContainer>
            <EFRectangleButton shadowTheme="none" colorTheme="light" text="Opponent score" onClick={goNextStep} />
            <EFRectangleButton
              shadowTheme="none"
              colorTheme="primary"
              text={`${gameToExpand.status === "submitted" ? "Update" : "Submit"}`}
              onClick={onSubmit}
              disabled={gameToExpand.status === "finalized" || !myTeamScore || !otherTeamScore}
            />
          </ButtonsContainer>
        </>
      )}
    </Container>
  );
};

const Container = styled.div`
  > * {
    margin-bottom: 10px;
  }

  > :last-child {
    margin-bottom: 0;
  }
`;

const ButtonsContainer = styled.div`
  display: flex;
  justify-content: space-between;
  padding-top: 10px;
`;

export default MyTeamSubmissionView;
