import { gql, useMutation, useQuery } from "@apollo/client";
import { SubmittedScoreType } from "../../../../../graphql/interface/League";
import {
  UPDATE_SCORE,
  UpdateScoreVars
} from "../FinalizeMatchResultsModal/FinalizeMatchResults/FinalizeMatchResultsQueries";

const GET_LEAGUE_GAME_SCORES_FOR_MATCH = gql`
  query getLeagueGameScores($matchId: String!) {
    getLeagueGameScores(matchId: $matchId) {
      _id
      scores {
        _id
        value
        competitor {
          ... on Team {
            _id
          }
        }
      }
      gameNumber
      type
      creator {
        ... on Team {
          _id
        }
      }
      image
    }
  }
`;

// eslint-disable-next-line import/prefer-default-export
export const useGetLeagueMatchScores = matchId => {
  const { data, loading } = useQuery(GET_LEAGUE_GAME_SCORES_FOR_MATCH, { variables: { matchId } });

  return [data?.getLeagueGameScores, loading];
};

const GET_MY_ORG_ROLES = gql`
  query getUsersLeagueOrgsAndRoles {
    getUsersLeagueOrgsAndRoles {
      organizationRoles {
        organization {
          _id
        }
        role
      }
    }
  }
`;

export const useGetMyOrgRoles = () => {
  const { data, loading } = useQuery(GET_MY_ORG_ROLES);

  return [data?.getUsersLeagueOrgsAndRoles?.organizationRoles, loading];
};

export interface SubmitNewGameScoreBody {
  gameScoreBody: {
    creator: string;
    gameNumber: number;
    image: string;
    type: SubmittedScoreType.SUBMITTED;
    creatorType: "teams";
    match: string;
  };
  competitorScores: {
    value: number;
    competitor: string;
    competitorType: "team";
  }[];
}

const SUBMIT_GAME_SCORE = gql`
  mutation SubmitLeagueGameScoreMutation(
    $gameScoreBody: CreateLeagueGameScoreBody!
    $competitorScores: [LeagueCompetitorScoreBody]!
  ) {
    submitLeagueGameScore(gameScoreBody: $gameScoreBody, competitorScores: $competitorScores) {
      _id
    }
  }
`;

interface GameScore {
  myTeamId: string;
  otherTeamId: string;
  myTeamScore: number;
  otherTeamScore: number;
  gameNumber: number;
  image: string;
  matchId: string;
}

export const useGetSubmitNewScoreMutation = () => {
  const [submitGameScoreMutation] = useMutation(SUBMIT_GAME_SCORE);

  const submitNewGameScore = ({
    myTeamId,
    otherTeamId,
    myTeamScore,
    otherTeamScore,
    gameNumber,
    image,
    matchId
  }: GameScore) => {
    const submitNewGameScoreBody: SubmitNewGameScoreBody = {
      gameScoreBody: {
        creator: myTeamId,
        gameNumber,
        image,
        type: SubmittedScoreType.SUBMITTED,
        creatorType: "teams",
        match: matchId
      },
      competitorScores: [
        { value: myTeamScore, competitor: myTeamId, competitorType: "team" },
        { value: otherTeamScore, competitor: otherTeamId, competitorType: "team" }
      ]
    };

    return submitGameScoreMutation({
      variables: {
        gameScoreBody: submitNewGameScoreBody.gameScoreBody,
        competitorScores: submitNewGameScoreBody.competitorScores
      },
      refetchQueries: [
        {
          query: GET_LEAGUE_GAME_SCORES_FOR_MATCH,
          variables: { matchId }
        }
      ]
    });
  };

  return submitNewGameScore;
};

export const useGetUpdateSubmittedScoreMutation = () => {
  const [updateSubmittedScoreMutation] = useMutation<any, UpdateScoreVars>(UPDATE_SCORE);

  const updateSubmittedGameScore = ({
    gameScoreId,
    myTeamScoreId,
    otherTeamScoreId,
    myTeamScore,
    otherTeamScore,
    image,
    matchId
  }) => {
    return updateSubmittedScoreMutation({
      variables: {
        updateLeagueGameScoreGameScoreId: gameScoreId,
        updateLeagueGameScoreGameScoreBody: { type: SubmittedScoreType.SUBMITTED, image },
        updateLeagueGameScoreCompetitorScores: [
          {
            _id: myTeamScoreId,
            value: myTeamScore
          },
          { _id: otherTeamScoreId, value: otherTeamScore }
        ]
      },
      refetchQueries: [
        {
          query: GET_LEAGUE_GAME_SCORES_FOR_MATCH,
          variables: { matchId }
        }
      ]
    });
  };
  return updateSubmittedGameScore;
};
