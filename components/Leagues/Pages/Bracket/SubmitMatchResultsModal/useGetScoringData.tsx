import { useEffect, useState } from "react";
import { useGetLeagueMatchScores } from "./queries";
import { SubmittedScoreType } from "../../../../../graphql/interface/League";

const useGetScoringData = matchId => {
  const [rawAdminFinalizedScores, setRawAdminFinalizedScores] = useState([]);
  const [rawTeamSubmittedScores, setRawTeamSubmittedScores] = useState([]);

  const [allMatchScores, isGettingScores] = useGetLeagueMatchScores(matchId);

  useEffect(() => {
    const localFinalizedScores = [];
    const localSubmittedScores = [];

    if (allMatchScores) {
      allMatchScores.forEach(gameScore => {
        if (gameScore.type === SubmittedScoreType.FINALIZED) {
          localFinalizedScores.push(gameScore);
        } else if (gameScore.type === SubmittedScoreType.SUBMITTED) {
          localSubmittedScores.push(gameScore);
        }
      });

      setRawAdminFinalizedScores(localFinalizedScores);
      setRawTeamSubmittedScores(localSubmittedScores);
    }
  }, [allMatchScores]);

  return {
    rawAdminFinalizedScores,
    rawTeamSubmittedScores,
    isGettingScores
  };
};

export default useGetScoringData;
