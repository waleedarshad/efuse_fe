import React, { useState } from "react";
import { LeagueEvent, LeagueMatch } from "../../../../graphql/interface/League";
import FinalizeMatchResultsModal from "./FinalizeMatchResultsModal/FinalizeMatchResultsModal";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";
import BracketContent from "./BracketContent/BracketContent";

interface BracketProps {
  event: LeagueEvent;
}

const ManageBracketAdminTab = ({ event }: BracketProps) => {
  const [selectedMatch, setSelectedMatch] = useState<LeagueMatch>();

  let allPoolTeams = event?.pools?.map(pool => (pool.teams ? pool.teams : []));
  allPoolTeams = allPoolTeams && [].concat(...allPoolTeams);

  const unassignedTeams = event?.teams?.filter(
    team => !event?.pools?.some(pool => pool.teams.some(poolTeam => poolTeam._id === team._id))
  );

  const hasConfirmedTeams = unassignedTeams?.length > 0 || allPoolTeams?.length > 0;
  const hasConfirmedPools = allPoolTeams?.length > 0;

  return (
    <>
      {hasConfirmedPools && hasConfirmedTeams ? (
        <BracketContent event={event} onMatchClick={setSelectedMatch} />
      ) : (
        <div className="text-center">
          {!hasConfirmedTeams && (
            <>
              <p>Bracket management cannot be completed until your teams are confirmed.</p>
              <EFRectangleButton
                text="Manage Teams"
                colorTheme="secondary"
                internalHref={`/leagues/event/${event?._id}?tab=teams`}
              />
            </>
          )}
          {!hasConfirmedPools && hasConfirmedTeams && (
            <>
              <p>Bracket management cannot be completed until your pools are confirmed.</p>
              <EFRectangleButton
                text="Manage Pools"
                colorTheme="secondary"
                internalHref={`/leagues/event/${event?._id}?tab=pools`}
              />
            </>
          )}
        </div>
      )}
      <FinalizeMatchResultsModal
        isOpen={!!selectedMatch}
        onClose={() => setSelectedMatch(null)}
        leagueOwnerOrg={event?.league?.owner?._id}
        gameTitle={event?.league?.game?.title}
        eventStartDate={event?.startDate}
        gamesPerMatch={event?.gamesPerMatch}
        teams={selectedMatch?.teams}
        matchId={selectedMatch?._id}
      />
    </>
  );
};

export default ManageBracketAdminTab;
