import React from "react";
import { useQuery } from "@apollo/client";
import { LeagueEvent, LeagueEventState } from "../../../../graphql/interface/League";
import { TabData } from "../../../EFTabs/EFTabs";
import ManageBracketAdminTab from "./ManageBracketAdminTab";
import BracketTab from "./BracketTab";
import EFDisplayPlaceholder from "../../../EFDisplayPlaceholder/EFDisplayPlaceholder";
import { GET_EVENT_BRACKET_INFO, GetEventBracketInfoData, GetEventBracketInfoVars } from "./queries";

const useBracketTab = (event: LeagueEvent, userHasAdminPrivileges: boolean): TabData => {
  const { data } = useQuery<GetEventBracketInfoData, GetEventBracketInfoVars>(GET_EVENT_BRACKET_INFO, {
    variables: { eventId: event?._id }
  });
  const eventWithBracketData = data?.getLeagueEventById;

  const adminContent = <ManageBracketAdminTab event={eventWithBracketData} />;
  const defaultContent =
    event?.state === LeagueEventState.PENDING ? (
      <BracketTab event={eventWithBracketData} />
    ) : (
      <EFDisplayPlaceholder text="Brackets not available yet. Please check back later." />
    );
  return {
    tabKey: "bracket",
    tabTitle: "Bracket",
    content: userHasAdminPrivileges ? adminContent : defaultContent
  };
};

export default useBracketTab;
