import React, { useState } from "react";
import keyBy from "lodash/keyBy";
import styled from "styled-components";
import colors from "../../../../styles/sharedStyledComponents/colors";
import AvailableTeamsSection from "./AvailableTeamsSection/AvailableTeamsSection";
import JoinedTeamsSection from "./JoinedTeamsSection/JoinedTeamsSection";
import { Team } from "./types";
import { LeagueEvent } from "../../../../graphql/interface/League";

interface ManageTeamsTabProps {
  event: LeagueEvent;
  updateGlobalAddedTeamsIds: (teamIds: string[]) => void;
  userHasAdminPrivileges: boolean;
}

const ManageTeamsTab = ({ event, updateGlobalAddedTeamsIds, userHasAdminPrivileges }: ManageTeamsTabProps) => {
  const [addedTeamsIds, setAddedTeamsIds]: [string[], React.Dispatch<React.SetStateAction<string[]>>] = useState(
    event?.teams?.map(team => team._id) || []
  );

  const allTeams = keyBy(event.league.teams, "_id");

  const availableTeams: Team[] = Object.keys(allTeams)
    .filter(teamId => !addedTeamsIds.includes(teamId))
    .map(teamId => allTeams[teamId]);

  const joinedTeams = addedTeamsIds.map(teamId => allTeams[teamId]);

  const updateAddedTeamsIds = (teamIds: string[]) => {
    setAddedTeamsIds(teamIds);
    updateGlobalAddedTeamsIds(teamIds);
  };

  return (
    <PageWrapper>
      <TeamsTabContainer>
        <ColumnsContainer>
          <FirstColumn>
            <AvailableTeamsSection
              teams={availableTeams.map(team => ({
                name: team.name,
                imageSrc: team?.owner?.profileImage?.url,
                _id: team._id
              }))}
              onAddTeamsClick={newAddedTeams => updateAddedTeamsIds([...addedTeamsIds, ...newAddedTeams])}
              onRemoveAllTeams={() => updateAddedTeamsIds([])}
            />
          </FirstColumn>
          <Separator />
          <SecondColumn>
            <JoinedTeamsSection
              teams={joinedTeams}
              onRemoveTeam={removedTeamId =>
                updateAddedTeamsIds(addedTeamsIds.filter(teamId => teamId !== removedTeamId))
              }
              userHasAdminPrivileges={userHasAdminPrivileges}
            />
          </SecondColumn>
        </ColumnsContainer>
      </TeamsTabContainer>
    </PageWrapper>
  );
};

export default ManageTeamsTab;

const PageWrapper = styled.div`
  height: 100%;
  background: linear-gradient(0deg, ${colors.snow}, ${colors.snow});
  display: flex;
  flex-direction: column;
  padding: 20px 30px;
  position: relative;
`;

const TeamsTabContainer = styled.div`
  display: flex;
  flex-direction: column;
  height: 500px;
  min-height: 40vh;
`;

const ColumnsContainer = styled.div`
  display: flex;
  flex: 1;
`;

const FirstColumn = styled.div`
  width: 364px;
`;

const SecondColumn = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
`;

const Separator = styled.span`
  border: 1px solid #ced7e7;
  margin-right: 35px;
  margin-left: 35px;
`;
