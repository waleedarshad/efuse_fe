import React, { useState } from "react";
import { LeagueBracketType, LeagueEvent, LeagueEventState } from "../../../../graphql/interface/League";
import { TabData } from "../../../EFTabs/EFTabs";
import TeamsList from "../../LeagueDashboard/TeamsList/TeamsList";
import { confirmTeams } from "../../../../store/actions/leagues/leaguesActions";
import TeamsActionButton from "./TeamsActionButton";
import LeagueAdminTeamsContent from "./LeagueAdminTeamsContent";
import EFDisplayPlaceholder from "../../../EFDisplayPlaceholder/EFDisplayPlaceholder";

const useTeamsTab = (
  event: LeagueEvent,
  userHasAdminPrivileges: boolean,
  updateConfirmedTeamsCount: (numberOfTeams: number) => void
): TabData => {
  const [isConfirmationModalOpen, setIsConfirmationModalOpen] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const [addedTeamsIds, setAddedTeamsIds] = useState(event?.teams?.map(team => team._id) || []);

  const updateAddedTeams = teamIds => {
    updateConfirmedTeamsCount(teamIds.length);
    setAddedTeamsIds(teamIds);
  };

  const onConfirmTeamsClick = () => {
    if (addedTeamsIds.length < event.maxTeams || event.bracketType === LeagueBracketType.SINGLE_ELIM) {
      setIsConfirmationModalOpen(true);
    } else {
      confirmTeams(event, addedTeamsIds);
    }
  };

  const onConfirmModalClick = () => {
    setIsLoading(true);
    confirmTeams(event, addedTeamsIds);
  };

  const determineTabContent = () => {
    if (userHasAdminPrivileges) {
      return (
        <LeagueAdminTeamsContent
          event={event}
          addedTeamsIds={addedTeamsIds}
          isLoading={isLoading}
          userHasAdminPrivileges={userHasAdminPrivileges}
          isConfirmationModalOpen={isConfirmationModalOpen}
          updateAddedTeams={updateAddedTeams}
          onConfirmModalClick={onConfirmModalClick}
          updateIsConfirmationModalOpen={() => setIsConfirmationModalOpen(false)}
        />
      );
    }

    return (
      (event?.state !== LeagueEventState.PENDING_TEAMS && (
        <TeamsList teams={event?.teams} hideInvite isAdmin={false} />
      )) || <EFDisplayPlaceholder text="Pools not confirmed yet. Please check back later." />
    );
  };

  const teamTab: TabData = {
    tabTitle: "Teams",
    tabKey: "teams",
    content: determineTabContent(),
    actionButton: (
      <>
        {userHasAdminPrivileges && (
          <TeamsActionButton event={event} addedTeamsIds={addedTeamsIds} onConfirmTeamsClick={onConfirmTeamsClick} />
        )}
      </>
    )
  };

  return teamTab;
};

export default useTeamsTab;
