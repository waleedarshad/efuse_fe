import React from "react";
import { LeagueEvent } from "../../../../graphql/interface/League";
import TeamsConfirmationModal from "../../Modals/TeamsConfirmationModal";
import ManageTeamsTab from "./ManageTeamsTab";

interface LeagueAdminTeamsContentProps {
  event: LeagueEvent;
  addedTeamsIds: string[];
  isLoading: boolean;
  userHasAdminPrivileges: boolean;
  isConfirmationModalOpen: boolean;
  updateAddedTeams: (teamIds: any) => void;
  onConfirmModalClick: () => void;
  updateIsConfirmationModalOpen: () => void;
}

const LeagueAdminTeamsContent: React.FC<LeagueAdminTeamsContentProps> = ({
  event,
  addedTeamsIds,
  isLoading,
  userHasAdminPrivileges,
  isConfirmationModalOpen,
  updateAddedTeams,
  onConfirmModalClick,
  updateIsConfirmationModalOpen
}) => {
  return (
    <>
      <ManageTeamsTab
        event={event}
        updateGlobalAddedTeamsIds={updateAddedTeams}
        userHasAdminPrivileges={userHasAdminPrivileges}
      />
      <TeamsConfirmationModal
        bracketType={event.bracketType}
        gameImage={event.league.game.gameImage.url}
        modalIsOpen={isConfirmationModalOpen}
        onBackClicked={updateIsConfirmationModalOpen}
        onConfirmClick={onConfirmModalClick}
        confirmedTeams={addedTeamsIds.length}
        maxTeams={event.maxTeams}
        isLoading={isLoading}
      />
    </>
  );
};

export default LeagueAdminTeamsContent;
