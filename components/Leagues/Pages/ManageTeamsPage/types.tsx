import { LeagueEventState } from "../../../../graphql/interface/League";

export interface Team {
  _id: string;
  name: string;
  image: {
    url: string;
  };
  owner: {
    _id: string;
    profileImage: {
      url: string;
    };
  };
  members: {
    user: {
      name: string;
      profilePicture: {
        url: string;
      };
    };
  }[];
}

export interface Event {
  league: {
    imageUrl: string;
    owner: {
      _id: string;
      name: string;
      profileImage: {
        url: string;
      };
    };
    game: {
      title: string;
      gameImage: {
        url: string;
      };
    };
    teams: [Team];
  };
  _id: string;
  state: LeagueEventState;
  name: string;
  bracketType: string;
  startDate: string;
  rules: string;
  maxTeams: number;
  teams: Team[];
  pools: {
    _id: string;
  }[];
}
