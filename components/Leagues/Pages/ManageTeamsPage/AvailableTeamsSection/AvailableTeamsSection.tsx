import React, { useState } from "react";
import { faCheck, faTimes, faTrashAlt } from "@fortawesome/pro-regular-svg-icons";

import EFMultiCheckboxSelect from "../../../../EFMultiCheckboxSelect/EFMultiCheckboxSelect";
import { SectionTitle } from "../../../Typography/Typography";
import Style from "./AvailableTeamsSection.module.scss";
import EFRectangleButton from "../../../../Buttons/EFRectangleButton/EFRectangleButton";
import "simplebar-react/dist/simplebar.min.css";
import EFLightDropdown from "../../../../Dropdowns/EFLightDropdown/EFLightDropdown";
import EFMenuItem from "../../../../Dropdowns/EFMenuItem/EFMenuItem";
import { ITeamCardMenuItem } from "../../../../Cards/EFTeamCard/EFTeamCard";
import EFScrollbar from "../../../../EFScrollbar/EFScrollbar";

interface AvailableTeamsSectionProps {
  teams: { name: string; imageSrc: string; _id: string }[];
  onAddTeamsClick: (ids: string[]) => void;
  onRemoveAllTeams: () => void;
}

const AvailableTeamsSection = ({ teams, onAddTeamsClick, onRemoveAllTeams }: AvailableTeamsSectionProps) => {
  const [selectedTeamIds, setSelectedTeamIds] = useState([]);

  const menuItems: ITeamCardMenuItem[] = [
    {
      option: <EFMenuItem icon={faCheck} text="Select all" iconColorTheme="blue" textColorTheme="black" />,
      onClick: () => setSelectedTeamIds(teams.map(team => team._id))
    },
    {
      option: <EFMenuItem icon={faTimes} text="Clear selection" iconColorTheme="blue" textColorTheme="black" />,
      onClick: () => setSelectedTeamIds([])
    },
    {
      option: <EFMenuItem icon={faTrashAlt} text="Remove all teams" iconColorTheme="red" textColorTheme="red" />,
      onClick: onRemoveAllTeams
    }
  ];

  return (
    <div className={Style.container}>
      <div className={Style.header}>
        <SectionTitle>Available Teams</SectionTitle>
        <EFLightDropdown menuItems={menuItems} />
      </div>
      <EFScrollbar>
        <EFMultiCheckboxSelect
          inputs={teams.map(team => ({
            imageUrl: team.imageSrc,
            title: team.name,
            _id: team._id,
            isChecked: selectedTeamIds.includes(team._id)
          }))}
          onSelect={selection => setSelectedTeamIds(selection.map(t => t._id))}
        />
      </EFScrollbar>
      {selectedTeamIds.length > 0 && (
        <div className={Style.addTeamsButtonWrapper}>
          <EFRectangleButton
            colorTheme="secondary"
            text="Add Teams"
            onClick={() => {
              onAddTeamsClick(selectedTeamIds);
              setSelectedTeamIds([]);
            }}
          />
        </div>
      )}
    </div>
  );
};

export default AvailableTeamsSection;
