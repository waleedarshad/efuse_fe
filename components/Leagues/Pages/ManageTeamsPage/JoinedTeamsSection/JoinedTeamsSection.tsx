import React from "react";
import { faTrashAlt } from "@fortawesome/pro-regular-svg-icons";
import { SectionTitle } from "../../../Typography/Typography";
import Style from "./JoinedTeamsSection.module.scss";
import EFTeamCard, { ITeamCardMenuItem } from "../../../../Cards/EFTeamCard/EFTeamCard";
import EFMenuItem from "../../../../Dropdowns/EFMenuItem/EFMenuItem";
import EFScrollbar from "../../../../EFScrollbar/EFScrollbar";
import { Team } from "../types";

interface JoinedTeamsSectionProps {
  teams: Team[];
  onRemoveTeam: (teamId: string) => void;
  userHasAdminPrivileges: boolean;
}

const NoTeamsSelected = () => (
  <div className={Style.noTeams}>
    <h2>Select from Available Teams to add to event</h2>
    <h3>After you select teams this area will populate with their team card</h3>
  </div>
);

const JoinedTeamsSection = ({ teams, onRemoveTeam, userHasAdminPrivileges }: JoinedTeamsSectionProps) => {
  const getRemoveMenuItem = (teamId: string): ITeamCardMenuItem => ({
    option: <EFMenuItem icon={faTrashAlt} text="Remove" iconColorTheme="red" textColorTheme="red" />,
    onClick: () => onRemoveTeam(teamId)
  });

  return (
    <div className={Style.joinedTeamsSection}>
      {teams.length === 0 ? (
        <NoTeamsSelected />
      ) : (
        <>
          <div className={Style.header}>
            <SectionTitle>Selected Teams</SectionTitle>
          </div>
          <div className={Style.cardSection}>
            <EFScrollbar>
              <div className={Style.teamCardsContainer}>
                {teams.map(team => (
                  <EFTeamCard
                    key={team._id}
                    menuItems={[getRemoveMenuItem(team._id)]}
                    teamName={team.name}
                    teamImageUrl={team?.owner?.profileImage?.url}
                    teamMembers={team.members.map(member => ({
                      imageUrl: member.user.profilePicture.url,
                      name: member.user.name
                    }))}
                    hideInvite={true}
                    imageHref={`/leagues/dashboard/${team?.owner?._id}`}
                    noMembersMessage={"Currently no members are part of this team."}
                    isAdmin={userHasAdminPrivileges}
                  />
                ))}
              </div>
            </EFScrollbar>
          </div>
        </>
      )}
    </div>
  );
};

export default JoinedTeamsSection;
