import { gql } from "@apollo/client";
import { LeagueEvent } from "../../../../graphql/interface/League";

export interface GetEventData {
  getLeagueEventById: LeagueEvent;
}

export interface GetEventVars {
  eventId: string;
}

export const GET_EVENT = gql`
  query Query($eventId: String!) {
    getLeagueEventById(eventId: $eventId) {
      league {
        _id
        imageUrl
        name
        owner {
          ... on Organization {
            _id
            name
            currentUserRole
            profileImage {
              url
            }
            user {
              _id
            }
          }
        }
        game {
          title
          gameImage {
            url
          }
        }
        teams {
          _id
          name
          image {
            url
          }
          owner {
            ... on Organization {
              _id
              profileImage {
                url
              }
            }
          }
          members {
            user {
              name
              profilePicture {
                url
              }
            }
          }
        }
      }
      _id
      bracketType
      startDate
      rules
      name
      state
      teams {
        _id
        name
        image {
          url
        }
        owner {
          ... on Organization {
            _id
            profileImage {
              url
            }
          }
        }
        members {
          user {
            name
            profilePicture {
              url
            }
          }
        }
      }
      pools {
        _id
      }
      maxTeams
    }
  }
`;

export interface SetEventTeamsData {
  updateLeagueEvent: {
    _id: string;
    teams: { _id: string }[];
  };
}

export interface SetEventTeamsVars {
  eventId: string;
  event: {
    teams: string[];
    state: string;
  };
}

export const SET_EVENT_TEAMS = gql`
  mutation UpdateLeagueEventMutation($eventId: String!, $event: UpdateLeagueEventArg!) {
    updateLeagueEvent(eventId: $eventId, leagueEvent: $event) {
      _id
      teams {
        _id
      }
    }
  }
`;
