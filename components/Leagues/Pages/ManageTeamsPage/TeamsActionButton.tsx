import React from "react";
import styled from "styled-components";
import { LeagueBracketType, LeagueEvent } from "../../../../graphql/interface/League";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";
import EFTooltip from "../../../tooltip/EFTooltip/EFTooltip";

interface TeamsActionButtonProps {
  event: LeagueEvent;
  addedTeamsIds: string[];
  onConfirmTeamsClick: () => void;
}

const TeamsActionButton: React.FC<TeamsActionButtonProps> = ({ event, addedTeamsIds, onConfirmTeamsClick }) => {
  const isConfirmButtonDisabled = () => {
    let disabled =
      addedTeamsIds.length === 0 || addedTeamsIds.length % 2 !== 0 || addedTeamsIds.length > event.maxTeams;

    // disable button if teams is less than max for single elim
    if (event.bracketType === LeagueBracketType.SINGLE_ELIM) {
      disabled = addedTeamsIds.length !== event.maxTeams;
    }

    return disabled;
  };

  const buttonText = "Confirm Teams";
  const tooltipText =
    event.bracketType === LeagueBracketType.ROUND_ROBIN
      ? "You have an uneven number of teams added."
      : `You have ${addedTeamsIds.length}/${event.maxTeams} teams added.`;

  const buttonComponent = (
    <EFRectangleButton text={buttonText} onClick={onConfirmTeamsClick} disabled={isConfirmButtonDisabled()} />
  );

  const actionButton = (isConfirmButtonDisabled() && (
    <EFTooltip tooltipContent={tooltipText} tooltipPlacement="top">
      <TooltipWrapper>{buttonComponent}</TooltipWrapper>
    </EFTooltip>
  )) || <>{buttonComponent}</>;

  return actionButton;
};

export default TeamsActionButton;

const TooltipWrapper = styled.div`
  display: inline-block;
`;
