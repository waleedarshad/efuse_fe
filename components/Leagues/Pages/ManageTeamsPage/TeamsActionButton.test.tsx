import { fireEvent, render, screen, act } from "@testing-library/react";
import { mock } from "jest-mock-extended";
import { LeagueBracketType, LeagueEvent, Team } from "../../../../graphql/interface/League";
import TeamsActionButton from "./TeamsActionButton";

describe("TeamsActionButton", () => {
  it("renders the correct text for uneven teams on round robin", async () => {
    const team1 = mock<Team>();
    const addedTeamsIds = [team1._id];

    const event = mock<LeagueEvent>();
    event.bracketType = LeagueBracketType.ROUND_ROBIN;

    render(<TeamsActionButton event={event} addedTeamsIds={addedTeamsIds} onConfirmTeamsClick={() => {}} />);

    const button = screen.getByText("Confirm Teams");
    await act(async () => {
      fireEvent.mouseOver(button);
    });

    expect(screen.getByText("Confirm Teams"));
    expect(screen.getByText("You have an uneven number of teams added."));
  });

  it("renders the correct text for less than maxTeams on single elimination", async () => {
    const team1 = mock<Team>();
    const addedTeamsIds = [team1._id];

    const event = mock<LeagueEvent>();
    event.maxTeams = 2;
    event.bracketType = LeagueBracketType.SINGLE_ELIM;

    render(<TeamsActionButton event={event} addedTeamsIds={addedTeamsIds} onConfirmTeamsClick={() => {}} />);

    const button = screen.getByText("Confirm Teams");
    await act(async () => {
      fireEvent.mouseOver(button);
    });

    expect(screen.getByText("Confirm Teams"));
    expect(screen.getByText("You have 1/2 teams added."));
  });

  it("renders the correct text for correctly added teams", () => {
    const team1 = mock<Team>();
    const team2 = mock<Team>();
    const addedTeamsIds = [team1, team2].map(team => team._id);

    const event = mock<LeagueEvent>();
    event.maxTeams = 2;
    event.bracketType = LeagueBracketType.SINGLE_ELIM;

    render(<TeamsActionButton event={event} addedTeamsIds={addedTeamsIds} onConfirmTeamsClick={() => {}} />);

    expect(screen.getByText("Confirm Teams"));
  });
});
