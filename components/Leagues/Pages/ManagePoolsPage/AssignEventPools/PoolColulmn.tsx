import React, { useState } from "react";
import styled from "styled-components";
import { faCheck, faRepeatAlt, faTimes } from "@fortawesome/pro-regular-svg-icons";
import { faTrashAlt } from "@fortawesome/pro-light-svg-icons";
import EFMultiselectColumn from "../../../../EFMultiselectColumn/EFMultiselectColumn";
import EFRectangleButton from "../../../../Buttons/EFRectangleButton/EFRectangleButton";
import MoveTeamsModal from "../../../Modals/MoveTeamsModal";
import { LeagueEventPool } from "../../../../../graphql/interface/League";
import EFMenuItem from "../../../../Dropdowns/EFMenuItem/EFMenuItem";
import EFAlert from "../../../../EFAlert/EFAlert";
import { sendNotification } from "../../../../../helpers/FlashHelper";

interface PoolColumnProps {
  pool: LeagueEventPool;
  allPools: LeagueEventPool[];
  onMoveTeams: (teams: string[], fromPoolId: string, toPoolId: string) => Promise<void>;
  onAutoGenerate: () => Promise<void>;
  onClearPools: () => Promise<void>;
  allowEdit?: boolean;
}

const PoolColumn = ({ pool, allPools, onMoveTeams, onAutoGenerate, onClearPools, allowEdit }: PoolColumnProps) => {
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const [selectedTeams, setSelectedTeams] = useState([]);
  const [isLoading, setLoading] = useState(false);

  const callWithLoadingIndicator = call => {
    setLoading(true);
    call()
      .then(() => {
        setSelectedTeams([]);
        closeModal();
        sendNotification("Pools Updated", "success", "Success");
      })
      .catch(error => {
        sendNotification(error, "danger", "Something went wrong");
        setLoading(false);
      });
  };

  const closeModal = () => {
    setModalIsOpen(false);
    setLoading(false);
  };

  const setItems = selectedInputs => {
    setSelectedTeams(
      selectedInputs.map(input => ({ image: { url: input.imageUrl }, name: input.title, _id: input._id }))
    );
  };

  return (
    <>
      <EFMultiselectColumn
        title={pool.name}
        items={pool.teams.map(item => ({
          imageUrl: item.owner?.profileImage?.url,
          title: item.name,
          _id: item._id,
          isChecked: selectedTeams.some(it => it._id === item._id),
          disabled: !allowEdit
        }))}
        setSelectedItems={setItems}
        key={pool._id}
        button={
          selectedTeams.length > 0 && (
            <ButtonWrapper>
              <EFRectangleButton
                colorTheme="secondary"
                text="Move Teams"
                onClick={() => setModalIsOpen(true)}
                width="fullWidth"
              />
            </ButtonWrapper>
          )
        }
        menuItems={[
          {
            option: (
              <EFMenuItem icon={faRepeatAlt} text="Auto generate pools" iconColorTheme="blue" textColorTheme="black" />
            ),
            onClick: () => callWithLoadingIndicator(onAutoGenerate)
          },
          {
            option: <EFMenuItem icon={faCheck} text="Select all" iconColorTheme="blue" textColorTheme="black" />,
            onClick: () => {
              setSelectedTeams(pool?.teams);
            }
          },
          {
            option: <EFMenuItem icon={faTimes} text="Deselect all" iconColorTheme="blue" textColorTheme="black" />,
            onClick: () => {
              setSelectedTeams([]);
            }
          },
          {
            option: <EFMenuItem icon={faTrashAlt} text="Clear all pools" iconColorTheme="red" textColorTheme="red" />,
            onClick: () => {
              EFAlert(
                "Are you sure you want to clear all assigned teams?",
                "This will remove all progress.",
                "Yes",
                "No",
                () => callWithLoadingIndicator(onClearPools)
              );
            }
          }
        ]}
        enableHorizontalScroll
        allowEdit={allowEdit}
      />
      <MoveTeamsModal
        currentPoolId={pool._id}
        onMove={(teams, poolId) =>
          callWithLoadingIndicator(() =>
            onMoveTeams(
              teams.map(it => it._id),
              pool._id,
              poolId
            )
          )
        }
        pools={allPools}
        isOpen={modalIsOpen}
        teams={selectedTeams}
        onClose={closeModal}
        isLoading={isLoading}
      />
    </>
  );
};

const ButtonWrapper = styled.div`
  position: absolute;
  left: 50%;
  transform: translateX(-50%);
  bottom: -60px;
  width: 200px;
`;

export default PoolColumn;
