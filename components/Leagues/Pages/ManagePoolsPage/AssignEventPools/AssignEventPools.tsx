import React from "react";
import { useMutation } from "@apollo/client";
import { LeagueEventPool, Team } from "../../../../../graphql/interface/League";
import Styles from "./AssignEventPools.module.scss";

import HorizontalScroll from "../../../../HorizontalScroll/HorizontalScroll";
import PoolColumn from "./PoolColulmn";
import {
  CLEAR_TEAMS,
  ClearTeamsVars,
  GENERATE_POOLS,
  GeneratePoolsVars,
  MOVE_TEAMS,
  MoveTeamsVars
} from "../eventPoolsPageQueries";

interface AssignEventPoolsProps {
  unassignedTeams: Team[];
  pools: LeagueEventPool[];
  eventId: string;
  allowEdit?: boolean;
}

const AssignEventPools = ({ unassignedTeams, pools, eventId, allowEdit = true }: AssignEventPoolsProps) => {
  const UNASSIGNED_POOL_ID = "unassigned-pool";

  const [moveTeams] = useMutation<any, MoveTeamsVars>(MOVE_TEAMS);
  const [clearTeams] = useMutation<any, ClearTeamsVars>(CLEAR_TEAMS);
  const [generatePools] = useMutation<any, GeneratePoolsVars>(GENERATE_POOLS);

  const onMove = async (teams, fromPoolId, toPoolId) => {
    const fromPoolVars = fromPoolId === UNASSIGNED_POOL_ID ? {} : { fromPoolId };
    const toPoolVars = toPoolId === UNASSIGNED_POOL_ID ? {} : { toPoolId };
    await moveTeams({ variables: { teams, ...fromPoolVars, ...toPoolVars } });
  };

  const onClearPools = async () => {
    await clearTeams({ variables: { eventId } });
  };

  const onGeneratePools = async () => {
    await generatePools({ variables: { eventId } });
  };

  const allPools = allowEdit
    ? [{ _id: UNASSIGNED_POOL_ID, name: "Unassigned", teams: unassignedTeams }].concat(pools)
    : pools;

  return (
    <>
      <div className={Styles.container}>
        <HorizontalScroll customButtonMargin={Styles.buttonMargin}>
          {allPools.map(pool => (
            <PoolColumn
              key={pool._id}
              pool={pool}
              onMoveTeams={onMove}
              allPools={allPools}
              onAutoGenerate={onGeneratePools}
              onClearPools={onClearPools}
              allowEdit={allowEdit}
            />
          ))}
        </HorizontalScroll>
      </div>
    </>
  );
};

export default AssignEventPools;
