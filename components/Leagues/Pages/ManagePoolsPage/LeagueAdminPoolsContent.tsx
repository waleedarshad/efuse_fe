import React from "react";
import { LeagueEvent, Team } from "../../../../graphql/interface/League";
import EventPoolConfirmationModal from "../../Modals/EventPoolConfirmationModal";
import ManagePoolsTab from "./ManagePoolsTab";

interface LeagueAdminPoolsContentProps {
  event: LeagueEvent;
  modalIsOpen: boolean;
  unassignedTeams: Team[];
  gameImage: string;
  updateIsConfirmationModalOpen: () => void;
  onConfirm: () => Promise<void>;
}

const LeagueAdminPoolsContent: React.FC<LeagueAdminPoolsContentProps> = ({
  event,
  modalIsOpen,
  unassignedTeams,
  gameImage,
  updateIsConfirmationModalOpen,
  onConfirm
}) => {
  return (
    <>
      <ManagePoolsTab event={event} unassignedTeams={unassignedTeams} />
      <EventPoolConfirmationModal
        modalIsOpen={modalIsOpen}
        onClose={updateIsConfirmationModalOpen}
        gameImage={gameImage}
        onConfirm={onConfirm}
      />
    </>
  );
};

export default LeagueAdminPoolsContent;
