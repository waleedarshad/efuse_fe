import { render, screen } from "@testing-library/react";
import { mock } from "jest-mock-extended";
import { LeagueEvent, LeagueEventPool, Team } from "../../../../graphql/interface/League";
import PoolsActionButton from "./PoolsActionButton";

describe("PoolsActionButton", () => {
  it("renders the correct text", () => {
    const team1 = mock<Team>();
    const team2 = mock<Team>();

    const pool1 = mock<LeagueEventPool>();
    pool1.teams = [team1];

    const pool2 = mock<LeagueEventPool>();
    pool2.teams = [team2];

    const event = mock<LeagueEvent>();
    event.pools = [pool1, pool2];

    render(<PoolsActionButton event={event} unassignedTeamsLength={0} updateIsConfirmationModalOpen={() => {}} />);

    expect(screen.getByText("Confirm Pools"));
  });
});
