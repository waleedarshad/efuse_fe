import { gql } from "@apollo/client";
import { LeagueEvent, LeagueEventState } from "../../../../graphql/interface/League";

export interface GetEventData {
  getLeagueEventById: LeagueEvent;
}

export interface GetEventVars {
  eventId: string;
}

const teamFields = `
  _id
  name
  owner {
    ... on Organization {
      profileImage {
        url
      }
    }
  }
`;

export const GET_EVENT = gql`
  query Query($eventId: String!) {
    getLeagueEventById(eventId: $eventId) {
      _id
      bracketType
      startDate
      name
      state
      teams {
        ${teamFields}
      }
      maxTeams
      league {
        imageUrl
        owner {
          ... on Organization {
            name
            profileImage {
              url
            }
          }
        }
        game {
          title
          gameImage {
            url
          }
        }
      }
      pools {
        _id
        name
        teams {
          ${teamFields}
        }
      }
    }
  }
`;

export interface SetEventTeamsData {
  updateLeagueEvent: {
    teams: { _id: string }[];
  };
}

export interface SetEventStateVars {
  eventId: string;
  event: {
    state: LeagueEventState;
  };
}

export const SET_EVENT_STATE = gql`
  mutation UpdateLeagueEventMutation($eventId: String!, $event: UpdateLeagueEventArg!) {
    updateLeagueEvent(eventId: $eventId, leagueEvent: $event) {
      _id
    }
  }
`;

export interface GeneratePoolsVars {
  eventId: string;
}

export const GENERATE_POOLS = gql`
  mutation GenerateLeagueTeamPools($eventId: String!) {
    generateLeagueTeamPools(eventId: $eventId) {
      _id
      name
      teams {
        ${teamFields}
      }
    }
  }
`;

export interface MoveTeamsVars {
  fromPoolId?: string;
  toPoolId?: string;
  teams: string[];
}

export const MOVE_TEAMS = gql`
  mutation MoveTeamsToLeaguePool($fromPoolId: String, $toPoolId: String, $teams: [String]) {
    moveTeamsToLeaguePool(fromPoolId: $fromPoolId, toPoolId: $toPoolId, teams: $teams) {
      _id
      name
      teams {
        ${teamFields}
      }
    }
  }
`;
export interface ClearTeamsVars {
  eventId: string;
}

export const CLEAR_TEAMS = gql`
  mutation ClearAllTeamsInLeaguePools($eventId: String!) {
    clearAllTeamsInLeaguePools(eventId: $eventId) {
      _id
      name
      teams {
        ${teamFields}
      }
    }
  }
`;
