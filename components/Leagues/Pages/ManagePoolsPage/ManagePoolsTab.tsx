import React from "react";
import styled from "styled-components";
import AssignEventPools from "./AssignEventPools/AssignEventPools";
import { LeagueEvent, Team } from "../../../../graphql/interface/League";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";

interface ManagePoolsTabProps {
  event: LeagueEvent;
  unassignedTeams: Team[];
}

const ManagePoolsTab: React.FC<ManagePoolsTabProps> = ({ event, unassignedTeams }) => {
  const eventId = event?._id;

  let allPoolsTeam = event?.pools?.map(pool => (pool.teams ? pool.teams : []));
  allPoolsTeam = allPoolsTeam && [].concat(...allPoolsTeam);

  const hasConfirmedTeams = unassignedTeams?.length > 0 || allPoolsTeam?.length > 0;

  return (
    <>
      {hasConfirmedTeams ? (
        <PoolsContainer>
          {event && (
            <AssignEventPools
              eventId={eventId}
              unassignedTeams={unassignedTeams || []}
              pools={event?.pools || []}
              allowEdit
            />
          )}
        </PoolsContainer>
      ) : (
        <PlaceholderWrapper>
          <p>Pools management cannot be completed until your teams are confirmed.</p>
          <EFRectangleButton
            text="Manage Teams"
            colorTheme="secondary"
            internalHref={`/leagues/event/${eventId}?tab=teams`}
          />
        </PlaceholderWrapper>
      )}
    </>
  );
};

export default ManagePoolsTab;

const PoolsContainer = styled.div`
  display: flex;
  flex-direction: column;
  height: 500px;
`;

const PlaceholderWrapper = styled.div`
  classname: text-center;
`;
