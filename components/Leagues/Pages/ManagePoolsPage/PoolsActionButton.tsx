import React from "react";
import { LeagueEvent } from "../../../../graphql/interface/League";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";

interface PoolsActionButtonProps {
  event: LeagueEvent;
  unassignedTeamsLength: number;
  updateIsConfirmationModalOpen: () => void;
}

const PoolsActionButton: React.FC<PoolsActionButtonProps> = ({
  event,
  unassignedTeamsLength,
  updateIsConfirmationModalOpen
}) => {
  const poolsHaveEvenNumberOfTeams = event?.pools.every(pool => {
    return pool?.teams?.length % 2 === 0;
  });

  const isConfirmButtonDisabled = () => {
    const disabled = !(unassignedTeamsLength === 0) || !poolsHaveEvenNumberOfTeams;
    return disabled;
  };

  const buttonText = "Confirm Pools";
  const actionButton = (
    <EFRectangleButton text={buttonText} onClick={updateIsConfirmationModalOpen} disabled={isConfirmButtonDisabled()} />
  );

  return actionButton;
};

export default PoolsActionButton;
