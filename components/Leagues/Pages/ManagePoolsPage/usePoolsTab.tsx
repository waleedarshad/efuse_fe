import React, { useState } from "react";
import styled from "styled-components";
import { useQuery } from "@apollo/client";
import { confirmPools } from "../../../../store/actions/leagues/leaguesActions";
import { LeagueEventState } from "../../../../graphql/interface/League";
import { GetEventData, GetEventVars, GET_EVENT } from "./eventPoolsPageQueries";
import { TabData } from "../../../EFTabs/EFTabs";
import AssignEventPools from "./AssignEventPools/AssignEventPools";
import EFDisplayPlaceholder from "../../../EFDisplayPlaceholder/EFDisplayPlaceholder";
import LeagueAdminPoolsContent from "./LeagueAdminPoolsContent";
import PoolsActionButton from "./PoolsActionButton";

const usePoolsTab = (eventId: string, userHasAdminPrivileges: boolean): TabData => {
  const [isConfirmationModalOpen, setIsConfirmationModalOpen] = useState(false);

  const { data } = useQuery<GetEventData, GetEventVars>(GET_EVENT, {
    variables: { eventId },
    fetchPolicy: "network-only"
  });
  const event = data?.getLeagueEventById;

  const unassignedTeams = event?.teams?.filter(
    team => !event?.pools?.some(pool => pool.teams.some(poolTeam => poolTeam._id === team._id))
  );

  const determineTabContent = () => {
    if (userHasAdminPrivileges) {
      return (
        <LeagueAdminPoolsContent
          event={event}
          modalIsOpen={isConfirmationModalOpen}
          unassignedTeams={unassignedTeams}
          gameImage={event?.league?.game?.gameImage?.url}
          updateIsConfirmationModalOpen={() => setIsConfirmationModalOpen(false)}
          onConfirm={() => confirmPools(eventId)}
        />
      );
    }

    return event?.state === LeagueEventState.PENDING ? (
      <EventPoolsWrapper>
        <AssignEventPools
          eventId={event?._id}
          unassignedTeams={unassignedTeams || []}
          pools={event?.pools || []}
          allowEdit={false}
        />
      </EventPoolsWrapper>
    ) : (
      <EFDisplayPlaceholder text="Pools not confirmed yet. Please check back later." />
    );
  };

  const teamTab: TabData = {
    tabTitle: "Pools",
    tabKey: "pools",
    content: determineTabContent(),
    actionButton: (
      <>
        {userHasAdminPrivileges && (
          <PoolsActionButton
            event={event}
            unassignedTeamsLength={unassignedTeams?.length}
            updateIsConfirmationModalOpen={() => setIsConfirmationModalOpen(true)}
          />
        )}
      </>
    )
  };

  return teamTab;
};

export default usePoolsTab;

const EventPoolsWrapper = styled.div`
  height: 500px;

  label {
    opacity: 1;
  }
`;
