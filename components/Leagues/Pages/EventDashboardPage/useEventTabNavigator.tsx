import React, { ReactNode } from "react";
import { useRouter } from "next/router";
import capitalize from "lodash/capitalize";
import { TabData } from "../../../EFTabs/EFTabs";
import DisplayDescriptionAndRules from "../../LeagueIndex/DisplayDescriptionAndRules/DisplayDescriptionAndRules";
import usePoolsTab from "../ManagePoolsPage/usePoolsTab";
import { LeagueEvent } from "../../../../graphql/interface/League";
import useBracketTab from "../Bracket/useBracketTab";
import useTeamsTab from "../ManageTeamsPage/useTeamsTab";

const useEventTabNavigator = (
  event: LeagueEvent,
  userHasAdminPrivileges: boolean,
  updateConfirmedTeamsCount: (numberOfTeams: number) => void
): TabData[] => {
  const router = useRouter();

  const navigateToTabPage = async (tabKeySelected: string) => {
    await router.push(`/leagues/event/${event?._id}?tab=${tabKeySelected}`, undefined, { shallow: true });
  };

  const teamsTab = useTeamsTab(event, userHasAdminPrivileges, updateConfirmedTeamsCount);
  const poolsTab = usePoolsTab(event._id, userHasAdminPrivileges);
  const bracketTab = useBracketTab(event, userHasAdminPrivileges);

  const editingTabs = [teamsTab, bracketTab];
  if (event?.pools?.length > 1) {
    editingTabs.splice(1, 0, poolsTab);
  }

  const baseTabs: { tabKey: string; content: ReactNode; actionButton?: React.ReactNode }[] = [
    {
      tabKey: "overview",
      content: <DisplayDescriptionAndRules info={event?.description} fieldName="description" />
    },
    ...editingTabs,
    {
      tabKey: "rules",
      content: <DisplayDescriptionAndRules info={event?.rules} fieldName="rules" />
    }
  ];

  return baseTabs.map(tab => ({ ...tab, tabTitle: capitalize(tab.tabKey), onTabSelect: navigateToTabPage }));
};

export default useEventTabNavigator;
