import React, { useState } from "react";
import { useRouter } from "next/router";
import { useMutation } from "@apollo/client";
import EFCardEventHeader from "../../../Cards/EFEventHeaderCard/EFEventHeaderCard";
import LeaguesDashboardLayout from "../../Layouts/LeaguesDashboardLayout/LeaguesDashboardLayout";
import EFTabs from "../../../EFTabs/EFTabs";
import useEventTabNavigator from "./useEventTabNavigator";
import EFBreadcrumb from "../../../EFBreadcrumb/EFBreadcrumb";
import { LeagueEvent } from "../../../../graphql/interface/League";
import { EVENT_BRACKET_TYPE_MAP, EVENT_CHIP_STATE_MAP } from "../../constants";
import LeagueNavContainer from "../../LeagueNavigation/LeagueSidebar/LeagueNavContainer";
import { OrganizationUserRoles } from "../../../../graphql/interface/Organization";
import UpdateDescriptionAndRules from "../../LeagueIndex/UpdateDescriptionAndRules/UpdateDescriptionAndRules";
import { UPDATE_LEAGUE_EVENT, UpdateEventsVars } from "../../../../graphql/leagues/LeagueEvents";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";

interface EventDashboardPageProps {
  event: LeagueEvent;
  userLeagueOrgRoles: OrganizationUserRoles;
  userHasAdminPrivileges: boolean;
}

const EventDashboardPage = ({ event, userLeagueOrgRoles, userHasAdminPrivileges }: EventDashboardPageProps) => {
  const router = useRouter();
  const [localEvent, setLocalEvent] = useState(event);
  const [confirmedTeamsCount, setConfirmedTeamsCount] = useState(event?.teams?.length || 0);

  const updateConfirmedTeamsCount = (numberOfTeams: number) => setConfirmedTeamsCount(numberOfTeams);

  // @ts-ignore
  const currentTab = router.query.tab.toString();
  const tabs = useEventTabNavigator(localEvent, userHasAdminPrivileges, updateConfirmedTeamsCount);
  const [updateLeagueEvent] = useMutation<any, UpdateEventsVars>(UPDATE_LEAGUE_EVENT);

  const updateDetailsAndRules = async (updates, afterSubmit) => {
    await updateLeagueEvent({ variables: { eventId: event._id, updates } }).then(() => {
      afterSubmit();

      setLocalEvent(prevState => {
        return {
          ...prevState,
          description: updates.description ?? prevState.description,
          rules: updates.rules ?? prevState.rules
        };
      });
    });
  };

  const onManageButtonClick = async () => {
    await router.push(`/leagues/event/${event._id}/${currentTab}`);
  };

  const getActionButton = () => {
    if (!userHasAdminPrivileges) {
      return null;
    }

    // TODO: remove IF statement once we move all tabs to the new pattern
    if (currentTab === "teams" || currentTab === "bracket") {
      return tabs.find(it => it.tabKey === currentTab)?.actionButton;
    }

    if (currentTab == null || currentTab === "overview") {
      return (
        <UpdateDescriptionAndRules
          info={localEvent.description}
          fieldName="description"
          updateMethod={updateDetailsAndRules}
          displayFieldName="overview"
        />
      );
    }

    if (currentTab === "rules") {
      return (
        <UpdateDescriptionAndRules info={localEvent.rules} fieldName="rules" updateMethod={updateDetailsAndRules} />
      );
    }

    if (currentTab === "teams") {
      return tabs[1].actionButton;
    }

    if (currentTab === "pools") {
      return tabs[2].actionButton;
    }

    if (currentTab === "bracket") {
      const buttonText = `Confirm ${currentTab.charAt(0).toUpperCase() + currentTab.slice(1)}`;

      return <EFRectangleButton shadowTheme="none" onClick={onManageButtonClick} text={buttonText} />;
    }
  };

  const dashboardBreadCrumb = (
    <EFBreadcrumb
      links={[
        { name: event.league.owner.name, href: `/leagues/dashboard/${event.league.owner._id}` },
        { name: event.league.name, href: `/leagues/${event.league._id}` },
        { name: event.name, href: `/leagues/event/${event._id}` }
      ]}
    />
  );

  const dashboardHeader = (
    <EFCardEventHeader
      backgroundImage={event.league?.imageUrl}
      title={event.name}
      subTitle={EVENT_BRACKET_TYPE_MAP[event?.bracketType]}
      leagueEventStateChip={EVENT_CHIP_STATE_MAP[event?.state]}
      date={event.startDate}
      confirmedTeams={userHasAdminPrivileges ? confirmedTeamsCount : event.teams.length}
      maxTeams={event.maxTeams}
      game={event.league.game.title}
      organizer={event.league.owner.name}
      organizerImage={event.league.owner?.profileImage?.url}
      actionButton={getActionButton()}
    />
  );

  return (
    <LeagueNavContainer userLeagueOrgRoles={userLeagueOrgRoles}>
      <LeaguesDashboardLayout header={dashboardHeader} breadcrumbs={dashboardBreadCrumb}>
        <EFTabs defaultActiveKey={currentTab} tabs={tabs} />
      </LeaguesDashboardLayout>
    </LeagueNavContainer>
  );
};

export default EventDashboardPage;
