import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { useMutation, useQuery } from "@apollo/client";
import Router from "next/router";
import EFWizard from "../../EFWizard/EFWizard";
import LeagueInfo from "./LeagueInfo";
import GamesInfo from "./GamesInfo";
import {
  CREATE_LEAGUE,
  CreateLeagueData,
  CreateLeagueDataResponse,
  CreateLeagueVars,
  GET_LEAGUES_BY_ORG
} from "../../../graphql/leagues/Leagues";
import { GET_ORG_INFO_BY_ID } from "../../../graphql/organizations/Organizations";
import LeagueCreatedModal from "../Modals/LeagueCreatedModal";
import { sendNotification } from "../../../helpers/FlashHelper";

const CreateLeagueFlow = ({ orgId }) => {
  const [leagueInfo, setLeagueInfo] = useState({});
  const [createLeague, { loading }] = useMutation<CreateLeagueDataResponse, CreateLeagueVars>(CREATE_LEAGUE, {
    refetchQueries: [{ query: GET_LEAGUES_BY_ORG, variables: { organizationId: orgId } }]
  });
  const [successModalOpen, setSuccessModalOpen] = useState(false);
  const [createdLeague, setCreatedLeague] = useState<CreateLeagueData>();

  const orgInfoResults = useQuery(GET_ORG_INFO_BY_ID, { variables: { organizationId: orgId } });
  const organizationInfo = orgInfoResults.data?.getOrganizationById;

  const submitPage = data => {
    setLeagueInfo(info => ({ ...info, ...data }));
  };

  const submitLastPage = data => {
    const formData = { ...leagueInfo, ...data };
    const finalData = { ...formData, ownerType: "organizations", owner: orgId };

    createLeague({
      variables: {
        league: finalData
      }
    })
      .then(response => {
        setCreatedLeague(response?.data?.createLeague);
        setSuccessModalOpen(true);
      })
      .catch(() => {
        sendNotification("League Creation Failed!, please try again.", "danger", "Error");
      });
  };

  return (
    <>
      <EFWizard>
        <LeagueInfo organizationInfo={organizationInfo} form={useForm()} onSubmit={submitPage} />
        <GamesInfo form={useForm()} onSubmit={submitLastPage} isLoading={loading} />
      </EFWizard>
      <LeagueCreatedModal
        modalIsOpen={successModalOpen && createdLeague}
        onClose={() => Router.push(`/leagues/${createdLeague?._id}/events/create`)}
        gameImage={createdLeague?.game?.gameImage?.url}
      />
    </>
  );
};

export default CreateLeagueFlow;
