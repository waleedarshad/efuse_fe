import React, { useContext } from "react";
import EFCreateFlowLayout from "../../EFCreateFlowLayout/EFCreateFlowLayout";
import EFSelectGamesControl from "../../FormControls/EFSelectGamesControl";
import { EFWizardContext } from "../../EFWizard/EFWizard";

const GamesInfo = ({ form, onSubmit, isLoading }) => {
  const {
    control,
    handleSubmit,
    formState: { errors }
  } = form;
  const { goNextStep } = useContext(EFWizardContext);
  const onValid = data => {
    onSubmit(data);
    goNextStep();
  };

  return (
    <EFCreateFlowLayout
      titleText="You're almost done!"
      titleSubText="Don't give up now!!"
      onNext={handleSubmit(onValid)}
      isLoading={isLoading}
      isRightButtonDisabled={isLoading}
    >
      <EFSelectGamesControl
        label="Select the game you play"
        errors={errors}
        name="game"
        control={control}
        required
        selectMultiple={false}
      />
    </EFCreateFlowLayout>
  );
};

export default GamesInfo;
