import React, { useContext } from "react";
import { UseFormReturn } from "react-hook-form/dist/types";
import EFCreateFlowLayout from "../../EFCreateFlowLayout/EFCreateFlowLayout";
import EFInputControl from "../../FormControls/EFInputControl";
import EFHtmlInputControl from "../../FormControls/EFHtmlInputControl";
import EFImageUploadControl from "../../FormControls/uploadControls/EFImageUploadControl";
import Style from "./CreateLeagueFlow.module.scss";
import { EFWizardContext } from "../../EFWizard/EFWizard";

interface LeagueFormInfo {
  name: string;
  description: string;
  image: string;
}

interface LeagueInfoProps {
  organizationInfo: { name: string; description: string; _id: string };
  form: UseFormReturn;
  onSubmit: (data: LeagueFormInfo) => void;
}

const LeagueInfo = ({ organizationInfo, form, onSubmit }: LeagueInfoProps) => {
  const {
    control,
    handleSubmit,
    formState: { errors }
  } = form;
  const { goNextStep } = useContext(EFWizardContext);
  const onValid = data => {
    onSubmit(data);
    goNextStep();
  };

  return (
    <EFCreateFlowLayout
      titleText={`Welcome ${organizationInfo?.name ?? ""} to League Creation!`}
      titleSubText="Let's get started!"
      onNext={handleSubmit(onValid)}
    >
      <div>
        <div className={Style.inputMargin}>
          <EFInputControl
            control={control}
            errors={errors}
            required
            label="League Name"
            name="name"
            maxLength={70}
            placeholder="Enter league name"
          />
        </div>
        <div className={Style.inputMargin}>
          <EFImageUploadControl
            control={control}
            errors={errors}
            label="Image"
            name="imageUrl"
            directory="uploads/league"
            buttonTheme="noShadowButton"
          />
        </div>
        <div className={Style.inputMargin}>
          <EFHtmlInputControl control={control} errors={errors} required label="Description" name="description" />
        </div>
      </div>
    </EFCreateFlowLayout>
  );
};

export default LeagueInfo;
