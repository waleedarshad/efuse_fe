import React from "react";
import EFCreationModal from "../../Modals/EFCreationModal/EFCreationModal";

const LeagueCreatedModal = ({ modalIsOpen, onClose, gameImage }) => (
  <EFCreationModal
    bottomRightButton="Continue"
    imgSrc={gameImage}
    imageText="Event Created"
    onClose={onClose}
    isOpen={modalIsOpen}
    subText="Continue to step 3 and use the provided invite link to invite organizations to your new leagues!"
    title="Congratulations you’ve completed step 2 of 3"
  />
);

export default LeagueCreatedModal;
