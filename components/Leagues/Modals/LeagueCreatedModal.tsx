import React from "react";
import EFCreationModal from "../../Modals/EFCreationModal/EFCreationModal";

const LeagueCreatedModal = ({ modalIsOpen, onClose, gameImage }) => (
  <EFCreationModal
    bottomRightButton="Continue"
    imgSrc={gameImage}
    imageText="League Created"
    onClose={onClose}
    isOpen={modalIsOpen}
    subText="Continue to Step 2 and create your first event! After creating your event, invite organizations to battle it out in your league!"
    title="Congratulations! You've completed Step 1 of 3."
  />
);

export default LeagueCreatedModal;
