import React from "react";
import EFCreationModal from "../../Modals/EFCreationModal/EFCreationModal";

const OrganizationCreatedModal = ({ modalIsOpen, onClose }) => (
  <EFCreationModal
    bottomRightButton="Continue"
    imgSrc="https://www.callofduty.com/content/dam/atvi/callofduty/cod-touchui/blog/hero/bocw/BOCW-S4-Announcement-TOUT.jpg"
    imageText="Organization Created"
    onClose={onClose}
    isOpen={modalIsOpen}
    subText="Click continue to start building your teams and assigning them to leagues."
    title="Congratulations you’ve finished building your organization!"
  />
);

export default OrganizationCreatedModal;
