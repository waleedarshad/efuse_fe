import React from "react";
import EFCreationModal from "../../Modals/EFCreationModal/EFCreationModal";

interface EventPoolConfirmationModalProps {
  modalIsOpen: boolean;
  onClose: () => void;
  gameImage: string;
  onConfirm: () => void;
}

const EventPoolConfirmationModal = ({
  modalIsOpen,
  onClose,
  gameImage,
  onConfirm
}: EventPoolConfirmationModalProps) => (
  <EFCreationModal
    bottomRightButton="Confirm"
    bottomLeftButton="Back"
    onRightButtonClick={onConfirm}
    imgSrc={gameImage}
    imageText="Attention!"
    onClose={onClose}
    isOpen={modalIsOpen}
    rightButtonColorTheme="primary"
    subText="Before confirming, please double check you have placed the correct teams in each pool."
    title="Confirm Pools"
  />
);

export default EventPoolConfirmationModal;
