import React from "react";
import { Spinner } from "react-bootstrap";
import { LeagueBracketType } from "../../../graphql/interface/League";
import EFCreationModal from "../../Modals/EFCreationModal/EFCreationModal";

interface TeamsConfirmationModalProps {
  bracketType: string;
  modalIsOpen: boolean;
  onBackClicked: () => void;
  onConfirmClick: () => void;
  gameImage: string;
  confirmedTeams: number;
  maxTeams: number;
  isLoading?: boolean;
}

const TeamsConfirmationModal = ({
  bracketType,
  modalIsOpen,
  onBackClicked,
  onConfirmClick,
  gameImage,
  confirmedTeams,
  maxTeams,
  isLoading = false
}: TeamsConfirmationModalProps) => {
  const bottomRightButton = bracketType === LeagueBracketType.ROUND_ROBIN ? "Confirm" : "Continue";
  const bottomLeftButton = bracketType === LeagueBracketType.ROUND_ROBIN ? "Back" : "";
  const imageText = bracketType === LeagueBracketType.ROUND_ROBIN ? "Attention!" : "";
  const title =
    bracketType === LeagueBracketType.ROUND_ROBIN ? "You are about to confirm your teams" : "Teams Confirmed!";

  const subText =
    bracketType === LeagueBracketType.ROUND_ROBIN
      ? `You have ${confirmedTeams} out of the desired ${maxTeams} teams added. If you confirm teams now, your league will run with only ${confirmedTeams} teams. Is this correct?`
      : "Teams have been confirmed and will be randomly assigned in the bracket. You will be able to rearrange your matchups on the bracket view.";

  return (
    <EFCreationModal
      bottomRightButton={
        <>
          {isLoading && <Spinner className="mr-2" animation="border" variant="light" size="sm" />}
          {bottomRightButton}
        </>
      }
      rightButtonColorTheme="primary"
      rightButtonDisabled={isLoading}
      bottomLeftButton={bottomLeftButton}
      onLeftButtonClick={onBackClicked}
      imgSrc={gameImage}
      imageText={imageText}
      onClose={onConfirmClick}
      isOpen={modalIsOpen}
      title={title}
      subText={subText}
    />
  );
};

export default TeamsConfirmationModal;
