import React from "react";
import EFCreationModal from "../../Modals/EFCreationModal/EFCreationModal";

const OrganizationJoinedModal = ({ modalIsOpen, onClose }) => (
  <EFCreationModal
    bottomRightButton="Continue"
    imgSrc="https://www.callofduty.com/content/dam/atvi/callofduty/cod-touchui/blog/hero/bocw/BOCW-S4-Announcement-TOUT.jpg"
    imageText="Organization Joined"
    onClose={onClose}
    isOpen={modalIsOpen}
    subText="Click continue to start building your teams and assigning them to leagues."
    title="Congratulations your organization has joined!"
  />
);

export default OrganizationJoinedModal;
