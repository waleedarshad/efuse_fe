import React from "react";
import EFCreationModal from "../../Modals/EFCreationModal/EFCreationModal";

const TeamCreatedModal = ({ modalIsOpen, onCreateMore, gameImage, onFinished }) => (
  <EFCreationModal
    bottomRightButton="Create more"
    rightButtonColorTheme="secondary"
    bottomLeftButton="I'm finished"
    onLeftButtonClick={onFinished}
    imgSrc={gameImage}
    imageText="Team Created"
    onClose={onCreateMore}
    isOpen={modalIsOpen}
    subText="Would you like to create another team?"
    title="Congratulations you created a new team!"
  />
);

export default TeamCreatedModal;
