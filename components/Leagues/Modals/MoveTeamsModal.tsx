import React, { useState } from "react";
import styled from "styled-components";
import EFSelectDropdown from "../../Dropdowns/EFSelectDropdown/EFSelectDropdown";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";
import { LeagueEventPool, Team } from "../../../graphql/interface/League";
import EFSmallImageCard from "../../Cards/EFSmallImageCard/EFSmallImageCard";
import EFPrimaryModal from "../../Modals/EFPrimaryModal/EFPrimaryModal";
import EFSpinnerOverlay from "../../Spinners/EFSpinnerOverlay";

interface MoveTeamsModalProps {
  isOpen: boolean;
  teams: Team[];
  pools: LeagueEventPool[];
  onClose: () => void;
  onMove: (teams: Team[], poolId: string) => void;
  currentPoolId: string;
  isLoading: boolean;
}

const DEFAULT_SELECTION = "defaultSelection";

const MoveTeamsModal = ({ isOpen, teams, pools, onClose, onMove, currentPoolId, isLoading }: MoveTeamsModalProps) => {
  const poolOptions = [
    { label: "Select Pool to move chosen teams", value: DEFAULT_SELECTION },
    ...pools.filter(it => it._id !== currentPoolId).map(it => ({ label: it.name, value: it._id }))
  ];

  const [selectedPool, setSelectedPool] = useState("");

  return (
    <EFPrimaryModal
      title="Move Teams to Pool"
      isOpen={isOpen}
      onClose={onClose}
      allowBackgroundClickClose={false}
      widthTheme="medium"
    >
      <EFSpinnerOverlay isLoading={isLoading}>
        <SubHeading>Selected Teams</SubHeading>
        <TeamsSection>
          {teams.slice(0, 5).map(team => (
            <TeamCard key={team._id} image={team.image?.url} title={team.name} />
          ))}
          {teams.length > 5 && <MoreText>+{teams.length - 5} More</MoreText>}
        </TeamsSection>
        <EFSelectDropdown defaultValue={DEFAULT_SELECTION} options={poolOptions} onSelect={setSelectedPool} />
        <Buttons>
          <EFRectangleButton
            text="Done"
            onClick={() => onMove(teams, selectedPool)}
            disabled={!selectedPool || selectedPool === DEFAULT_SELECTION}
          />
          <EFRectangleButton colorTheme="transparent" shadowTheme="none" text="Close" onClick={onClose} />
        </Buttons>
      </EFSpinnerOverlay>
    </EFPrimaryModal>
  );
};

export default MoveTeamsModal;

const SubHeading = styled.div`
  font-family: Poppins, sans-serif;
  font-style: normal;
  font-weight: 500;
  font-size: 12px;
  line-height: 20px;
`;

const TeamCard = styled(EFSmallImageCard)`
  margin-right: 10px;
  margin-bottom: 10px;
  width: 25%;

  :last-child {
    margin-right: 0;
  }
`;

const TeamsSection = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

const MoreText = styled.div`
  font-family: Poppins, sans-serif;
  font-style: normal;
  font-weight: 500;
  font-size: 12px;
  line-height: 20px;
  height: 100%;
  width: 25%;
  margin-top: 5px;
  text-align: center;
  color: rgba(18, 21, 29, 0.6);
`;

const Buttons = styled.div`
  width: 100%;
  flex-direction: row-reverse;
  display: flex;
  margin: 10px 0;
`;
