export const LEAGUES_META_TITLE = "eFuse | Leagues";

export const EVENT_CHIP_STATE_MAP = {
  pending: "Under Construction",
  pendingTeams: "Under Construction",
  pendingPools: "Under Construction",
  active: "Battle Underway",
  finished: "Event Completed"
};

export const EVENT_BRACKET_TYPE_MAP = {
  roundRobin: "Round Robin",
  singleElim: "Single Elimination",
  doubleElim: "Double Elimination",
  swiss: "Swiss",
  pointRace: "Point Race"
};
