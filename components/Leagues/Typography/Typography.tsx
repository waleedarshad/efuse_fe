import Style from "./Typography.module.scss";

export const SubtitleTwo = ({ children }: { children: any }) => <h2 className={Style.subtitleTwo}>{children}</h2>;

export const SectionTitle = ({ children }: { children: any }) => <h2 className={Style.sectionTitle}>{children}</h2>;
