import { OrganizationUserRole, OrganizationUserRoles } from "../../../../../graphql/interface/Organization";
import { NavItem } from "../../../../Navigation/SidebarNav/EFSidebarNavItem/NavItem";

const useOrgNavBuilder = (userLeagueOrgRoles: OrganizationUserRoles) => {
  const ownerItems: NavItem[] = [];
  const memberItems: NavItem[] = [];

  userLeagueOrgRoles?.organizationRoles?.forEach(orgRole => {
    if (orgRole.role === OrganizationUserRole.OWNER || orgRole.role === OrganizationUserRole.CAPTAIN) {
      const item: NavItem = {
        name: orgRole?.organization?.name,
        href: `/leagues/dashboard/${orgRole?.organization?._id}`,
        image: orgRole?.organization?.profileImage?.url
      };
      ownerItems.push(item);
    } else {
      const item: NavItem = {
        name: orgRole?.organization?.name,
        href: `/leagues/dashboard/${orgRole?.organization?._id}`,
        image: orgRole?.organization?.profileImage?.url
      };
      memberItems.push(item);
    }
  });

  return {
    ownerItems,
    memberItems
  };
};

export default useOrgNavBuilder;
