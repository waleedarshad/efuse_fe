import React, { ReactNode } from "react";
import styled from "styled-components";
import useOrgNavBuilder from "./SidebarHelpers/useOrgNavBuilder";
import EFSidebarContainer from "../../../Navigation/SidebarNav/EFSidebarContainer/EFSidebarContainer";
import { NavSection } from "../../../Navigation/SidebarNav/EFSidebarNavSection/NavSection";
import { OrganizationUserRoles } from "../../../../graphql/interface/Organization";

const LeagueNavContainer = ({
  userLeagueOrgRoles,
  children
}: {
  userLeagueOrgRoles: OrganizationUserRoles;
  children: ReactNode;
}) => {
  const items = useOrgNavBuilder(userLeagueOrgRoles);

  // kept owner and member items separate so it would be organized with owned on top of the nav bar
  const navSections: NavSection[] = [
    {
      title: "Organizations",
      navItems: [...items.ownerItems, ...items.memberItems]
    }
  ];

  return (
    <NavWrapper>
      <EFSidebarContainer navSections={navSections} />
      <ContentWrapper>{children}</ContentWrapper>
    </NavWrapper>
  );
};

const NavWrapper = styled.div`
  width: 100%;
  display: inline-flex;
`;

const ContentWrapper = styled.div`
  width: 100%;
  overflow: hidden;
`;

export default LeagueNavContainer;
