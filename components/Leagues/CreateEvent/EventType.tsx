import { useState } from "react";
import EFCreateFlowLayout from "../../EFCreateFlowLayout/EFCreateFlowLayout";
import Style from "./CreateLeagueEventFlow.module.scss";
import EFSelectCard from "../../Cards/EFSelectCard/EFSelectCard";
import { LeagueBracketType } from "../../../graphql/interface/League";

interface EventTypeProps {
  gameImgSrc: string;
  gameTitle: string;
  updateBracketType: (type: LeagueBracketType) => void;
}

const EventType = ({ gameImgSrc, gameTitle, updateBracketType }: EventTypeProps) => {
  const [selectedEventType, setSelectedEventType] = useState("");

  const updateSelectedEventType = (field, value) => {
    updateBracketType(value ? field : "");
    setSelectedEventType(value ? field : "");
  };

  return (
    <EFCreateFlowLayout
      titleText="Now, let's get some info"
      backgroundImageSrc={gameImgSrc}
      isRightButtonDisabled={selectedEventType === ""}
    >
      <p className={Style.steps}>Step 2 of 3: Event Creation</p>
      <h1 className={Style.gameTitle}>{gameTitle}</h1>
      <p className={Style.eventType}>Choose Event Type</p>
      <ul className={Style.eventTypeList}>
        <li>
          <EFSelectCard
            text="A round robin is a tournament format in which each team participating plays each other team participating an equal number of times. ... The Champions Group Stage features a round robin in which each team plays each other team in their group in one Best of 2 match."
            title="Round Robin"
            isCardSelected={selectedEventType === LeagueBracketType.ROUND_ROBIN}
            onCardSelectedChange={value => updateSelectedEventType(LeagueBracketType.ROUND_ROBIN, value)}
          />
        </li>
        <li>
          <EFSelectCard
            text="This bracket is very straightforward. The teams that qualify for the bracket will be drawn into a place on the bracket, depending on their seed. The fixtures will then be played out and the winner will advance onto the next stage, while the loser will be eliminated from the tournament."
            title="Single Elimination"
            isCardSelected={selectedEventType === LeagueBracketType.SINGLE_ELIM}
            onCardSelectedChange={value => updateSelectedEventType(LeagueBracketType.SINGLE_ELIM, value)}
          />
        </li>
        <li>
          <EFSelectCard
            text="A double-elimination tournament is a type of elimination tournament competition in which a participant ceases to be eligible to win the tournament's championship upon having lost two games or matches. ... The minimum number is two less than twice the number of teams (e.g., 8 teams – 14 games)."
            title="Double Elimination"
            isDisabled
            isCardSelected={selectedEventType === LeagueBracketType.DOUBLE_ELIM}
            onCardSelectedChange={value => updateSelectedEventType(LeagueBracketType.DOUBLE_ELIM, value)}
          />
        </li>
        <li>
          <EFSelectCard
            text="A Swiss-system tournament is a non-eliminating tournament format that features a fixed number of rounds of competition, but considerably fewer than for a round-robin tournament; thus each competitor (team or individual) does not play all the other competitors."
            title="Swiss"
            isDisabled
            isCardSelected={selectedEventType === LeagueBracketType.SWISS}
            onCardSelectedChange={value => updateSelectedEventType(LeagueBracketType.SWISS, value)}
          />
        </li>
      </ul>
    </EFCreateFlowLayout>
  );
};

export default EventType;
