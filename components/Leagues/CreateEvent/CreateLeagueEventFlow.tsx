import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { useMutation } from "@apollo/client";
import Router from "next/router";
import EFWizard from "../../EFWizard/EFWizard";
import EventType from "./EventType";
import EventInfo from "./EventInfo";

import EventTiming from "./EventTiming";
import {
  CREATE_LEAGUE_EVENT,
  CreateLeagueEventResponse,
  CreateLeagueEventVars
} from "../../../graphql/leagues/LeagueEvents";
import LeagueEventCreatedModal from "../Modals/LeagueEventCreatedModal";
import { League, LeagueBracketType, LeagueEventState } from "../../../graphql/interface/League";

interface CreateLeagueEventFlowProps {
  league: League;
}

const CreateLeagueEventFlow = ({ league }: CreateLeagueEventFlowProps) => {
  const background = league?.game?.gameImage?.url;
  const gameTitle = league?.game?.title;
  const [eventInfo, setEventInfo] = useState({});
  const [bracketTypeState, setBracketTypeState] = useState(LeagueBracketType.ROUND_ROBIN);
  const [createdModalOpen, setCreatedModalOpen] = useState(false);
  const [createLeagueEvent, { loading }] = useMutation<CreateLeagueEventResponse, CreateLeagueEventVars>(
    CREATE_LEAGUE_EVENT
  );

  const updateBracketType = (type: LeagueBracketType) => {
    setBracketTypeState(type);
  };

  const submitPage = data => {
    setEventInfo(info => ({ ...info, ...data }));
  };

  const submitLastPage = data => {
    const hardcodedValues = {
      timingMode: "weekly",
      state: LeagueEventState.PENDING_TEAMS
    }; // TODO: un-hardcode when we add more values.
    const formData = { ...eventInfo, ...data };

    // Set gamesPerMarch value to 1 by default and convert it to a number
    formData.gamesPerMatch = formData.gamesPerMatch ? parseInt(formData.gamesPerMatch, 10) : 1;
    formData.maxTeams = formData.maxTeams ? parseInt(formData.maxTeams, 10) : 2;

    const finalData = { ...formData, bracketType: bracketTypeState, ...hardcodedValues, leagueId: league._id };

    // eslint-disable-next-line @typescript-eslint/no-floating-promises
    createLeagueEvent({ variables: { event: finalData } }).then(() => {
      setCreatedModalOpen(true);
    });
  };

  return (
    <>
      <EFWizard>
        <EventType gameImgSrc={background} gameTitle={gameTitle} updateBracketType={updateBracketType} />
        <EventInfo gameImgSrc={background} form={useForm()} onSubmit={submitPage} bracketType={bracketTypeState} />
        <EventTiming gameImgSrc={background} form={useForm()} onSubmit={submitLastPage} isLoading={loading} />
      </EFWizard>
      <LeagueEventCreatedModal
        modalIsOpen={createdModalOpen}
        onClose={() =>
          Router.push({
            pathname: `/leagues/org/${league?.owner._id}/invite`,
            query: { leagueId: league?._id }
          })
        }
        gameImage={background}
      />
    </>
  );
};

export default CreateLeagueEventFlow;
