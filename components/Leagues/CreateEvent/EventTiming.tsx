import { UseFormReturn } from "react-hook-form/dist/types";
import React, { useContext } from "react";
import EFCreateFlowLayout from "../../EFCreateFlowLayout/EFCreateFlowLayout";
import Style from "./CreateLeagueEventFlow.module.scss";
import EFDatetimeInputControl from "../../FormControls/EFDatetimeInputControl";
import InputLabel from "../../InputLabel/InputLabel";
import EFSelectCard from "../../Cards/EFSelectCard/EFSelectCard";
import { EFWizardContext } from "../../EFWizard/EFWizard";

interface EventTimingForm {
  timing: string;
}

interface EventTimingInfoProps {
  gameImgSrc: string;
  form: UseFormReturn;
  onSubmit: (data: EventTimingForm) => void;
  isLoading: boolean;
}

const EventTiming = ({ gameImgSrc, form, onSubmit, isLoading }: EventTimingInfoProps) => {
  const {
    handleSubmit,
    control,
    formState: { errors }
  } = form;
  const { goNextStep } = useContext(EFWizardContext);
  const onValid = data => {
    onSubmit(data);
    goNextStep();
  };

  return (
    <EFCreateFlowLayout
      titleText="Let's set your time"
      backgroundImageSrc={gameImgSrc}
      onNext={handleSubmit(onValid)}
      buttonText="Finish your event"
      isLoading={isLoading}
      isRightButtonDisabled={isLoading}
    >
      <p className={Style.steps}>Step 2 of 3: Event Creation</p>
      <div className={Style.inputMargin}>
        <InputLabel theme="darkColor">Timing Mode</InputLabel>
        <EFSelectCard
          text="Every round in this event will be scheduled at this day and time per week, until the number of rounds are complete."
          title="Recurring once per week"
          isCardSelected
          onCardSelectedChange={() => {}}
        />
      </div>
      <div className={Style.inputMargin}>
        <EFDatetimeInputControl
          control={control}
          errors={errors}
          name="startDate"
          label="Start Date & Time of Event"
          required
        />
      </div>
    </EFCreateFlowLayout>
  );
};

export default EventTiming;
