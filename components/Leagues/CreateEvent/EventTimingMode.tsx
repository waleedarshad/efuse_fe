import { useState } from "react";
import EFCreateFlowLayout from "../../EFCreateFlowLayout/EFCreateFlowLayout";
import Style from "./CreateLeagueEventFlow.module.scss";
import EFSelectCard from "../../Cards/EFSelectCard/EFSelectCard";

interface EventTimingProps {
  gameImgSrc: string;
}

const EventTimingMode = ({ gameImgSrc }: EventTimingProps) => {
  // pre-selecting recurring once per week as it is the only enabled option
  const [selectedEventTiming, setSelectedEventTiming] = useState("once-per-week");

  const updateSelectedEventTiming = (clickedCardName, isCardSelected) => {
    setSelectedEventTiming(isCardSelected ? clickedCardName : "");
  };

  return (
    <EFCreateFlowLayout titleText="Let's set your time" backgroundImageSrc={gameImgSrc}>
      <p className={Style.eventType}>Timing Mode</p>
      <ul className={Style.eventTypeList}>
        <li>
          <EFSelectCard
            text="Every round in this event will be scheduled at this day and time per week, until the number of rounds are complete."
            title="Recurring once per week"
            isCardSelected={selectedEventTiming === "once-per-week"}
            onCardSelectedChange={value => updateSelectedEventTiming("once-per-week", value)}
          />
        </li>
        <li>
          <EFSelectCard
            text="This will be coming soon…"
            title="I will manually schedule rounds and matches"
            isDisabled
            isCardSelected={selectedEventTiming === "manual-schedule"}
            onCardSelectedChange={value => updateSelectedEventTiming("manual-schedule", value)}
          />
        </li>
        <li>
          <EFSelectCard
            text="This will be coming soon…"
            title="Select your custom reoccurrence"
            isDisabled
            isCardSelected={selectedEventTiming === "custom"}
            onCardSelectedChange={value => updateSelectedEventTiming("custom", value)}
          />
        </li>
      </ul>
    </EFCreateFlowLayout>
  );
};

export default EventTimingMode;
