import { UseFormReturn } from "react-hook-form/dist/types";
import React, { useContext } from "react";
import EFCreateFlowLayout from "../../EFCreateFlowLayout/EFCreateFlowLayout";
import Style from "./CreateLeagueEventFlow.module.scss";
import EFInputControl from "../../FormControls/EFInputControl";
import EFHtmlInputControl from "../../FormControls/EFHtmlInputControl";
import { EFWizardContext } from "../../EFWizard/EFWizard";
import EFSelectControl from "../../FormControls/EFSelectControl";
import { LeagueBracketType } from "../../../graphql/interface/League";

interface EventFormInfo {
  name: string;
  description: string;
  maxTeams: number;
  numberOfPools: number;
  gamesPerMatch: number;
}

interface EventInfoProps {
  gameImgSrc: string;
  form: UseFormReturn;
  onSubmit: (data: EventFormInfo) => void;
  bracketType: string;
}

const EventInfo = ({ gameImgSrc, form, onSubmit, bracketType }: EventInfoProps) => {
  const {
    handleSubmit,
    control,
    formState: { errors }
  } = form;
  const { goNextStep } = useContext(EFWizardContext);
  const onValid = data => {
    onSubmit(data);
    goNextStep();
  };

  const gamesPerMatchOptions = [
    { label: "1", value: "1" },
    { label: "3", value: "3" },
    { label: "5", value: "5" },
    { label: "7", value: "7" },
    { label: "9", value: "9" }
  ];

  const singleElimTeamOptions = [
    { label: "2", value: "2" },
    { label: "4", value: "4" },
    { label: "8", value: "8" },
    { label: "16", value: "16" },
    { label: "32", value: "32" }
  ];

  return (
    <EFCreateFlowLayout titleText="Almost Done!" backgroundImageSrc={gameImgSrc} onNext={handleSubmit(onValid)}>
      <p className={Style.steps}>Step 2 of 3: Event Creation</p>
      <div className={Style.inputMargin}>
        <EFInputControl
          errors={errors}
          name="name"
          control={control}
          label="Event Name"
          placeholder="Enter event name"
          required
        />
      </div>
      <div className={Style.inputMargin}>
        <EFHtmlInputControl errors={errors} name="description" control={control} label="Description" required />
      </div>
      {(bracketType === LeagueBracketType.SINGLE_ELIM && (
        <div className={Style.inputMargin}>
          <EFSelectControl
            errors={errors}
            name="maxTeams"
            control={control}
            label="Number of Teams"
            options={singleElimTeamOptions}
          />
        </div>
      )) || (
        <div className={Style.inputMargin}>
          <EFInputControl
            errors={errors}
            name="maxTeams"
            control={control}
            label="Number of Teams"
            placeholder="Number of rounds will be number of teams minus one"
            required
            valueAsNumber
            type="number"
            step={1}
            minValue={1}
          />
        </div>
      )}
      {bracketType === LeagueBracketType.ROUND_ROBIN && (
        <div className={Style.inputMargin}>
          <EFInputControl
            errors={errors}
            name="numberOfPools"
            control={control}
            label="Number of Pools"
            placeholder="Enter number of pools"
            required
            type="number"
            valueAsNumber
            step={1}
            minValue={1}
          />
        </div>
      )}
      <div className={Style.inputMargin}>
        <EFSelectControl
          errors={errors}
          name="gamesPerMatch"
          control={control}
          label="Series Length"
          options={gamesPerMatchOptions}
        />
      </div>
    </EFCreateFlowLayout>
  );
};

export default EventInfo;
