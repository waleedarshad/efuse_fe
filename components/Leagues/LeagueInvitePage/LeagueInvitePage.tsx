import React from "react";
import { useRouter } from "next/router";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";
import EFDetailsCard from "../../EFDetailsCard/EFDetailsCard";
import Carousel from "../../Carousels/Carousel/Carousel";
import Style from "./LeagueInvitePage.module.scss";
import useWindowDimensions from "../../../hooks/useWindowDimensions";

interface LeagueInvitePageProps {
  inviteCode: string;
  invitingOrgLeagues: [
    {
      name: string;
      imageUrl: string;
      game: {
        title: string;
      };
      teams: [{ _id: string }];
      events: [{ _id: string }];
    }
  ];
  invitingOrg: {
    name: string;
  };
}

const LeagueInvitePage = ({ inviteCode, invitingOrgLeagues, invitingOrg }: LeagueInvitePageProps) => {
  const router = useRouter();
  const { width } = useWindowDimensions();
  const media767AndBelow = width <= 767;

  const navigateToSelectOrCreateOrg = () => {
    router.push(`/leagues/join/${inviteCode}/organization`);
  };

  return (
    <div className={Style.background}>
      <div className={Style.backgroundBlur}>
        <h1 className={Style.inviteTextHeader}>{`${invitingOrg.name} has invited you to join their Leagues!`}</h1>
        <div className={Style.buttonContainer}>
          <EFRectangleButton
            onClick={navigateToSelectOrCreateOrg}
            text="LET'S GO!"
            colorTheme="secondary"
            size="extraLarge"
          />
        </div>
        <div className={Style.leagueContainer}>
          <Carousel
            settings={{
              slidesToShow: media767AndBelow ? 1 : 4,
              slidesToScroll: media767AndBelow ? 1 : 4,
              infinite: false
            }}
          >
            {invitingOrgLeagues.map((league, index) => (
              <div className={Style.cardContainer} key={index}>
                <EFDetailsCard
                  title={league.name}
                  image={league.imageUrl}
                  line1={league.game.title}
                  line2={`${league.events.length} Event${league.events.length === 1 ? "" : "s"}`}
                  line3={`${league.teams.length} Team${league.teams.length === 1 ? "" : "s"}`}
                />
              </div>
            ))}
          </Carousel>
        </div>
      </div>
    </div>
  );
};

export default LeagueInvitePage;
