import React from "react";
import Router from "next/router";
import getConfig from "next/config";
import EFCreateFlowLayout from "../../EFCreateFlowLayout/EFCreateFlowLayout";
import Style from "./Invite.module.scss";
import EFCopyURL from "../../EFCopyURL/EFCopyUrl";

interface InviteOrgsFlowProps {
  orgImage: string;
  inviteCode: string;
  orgId: string;
}

const InviteOrgs = ({ orgImage, inviteCode, orgId }: InviteOrgsFlowProps) => {
  const {
    publicRuntimeConfig: { feBaseUrl }
  } = getConfig();

  return (
    <EFCreateFlowLayout
      onNext={() =>
        Router.query.leagueId
          ? Router.push(`/leagues/${Router.query.leagueId}?tab=events`)
          : Router.push(`/leagues/dashboard/${orgId}`)
      }
      backgroundImageSrc={orgImage}
      titleText="Let's invite some organizations to your leagues!"
      buttonText="You're all set!"
    >
      <p className={Style.headerText}>Invite Organizations</p>
      <p className={Style.descriptionText}>
        *This link will give organizations access to all leagues you have create. Only share this link with
        organizations you trust!
      </p>
      <div style={{ marginTop: "10px" }}>
        <EFCopyURL url={`${feBaseUrl}/leagues/join/${inviteCode}`} />
      </div>
    </EFCreateFlowLayout>
  );
};

export default InviteOrgs;
