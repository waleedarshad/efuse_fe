import React, { useState } from "react";
import Router from "next/router";
import getConfig from "next/config";
import EFCreateFlowLayout from "../../EFCreateFlowLayout/EFCreateFlowLayout";
import Style from "./Invite.module.scss";
import EFCopyURL from "../../EFCopyURL/EFCopyUrl";
import TeamCreatedModal from "../Modals/TeamCreatedModal";

interface InviteTeamMembersFlowProps {
  orgId: string;
  ownerOrgId: string;
  inviteCode: string;
  gameImage: string;
}

const InviteTeamMembers = ({ orgId, ownerOrgId, inviteCode, gameImage }: InviteTeamMembersFlowProps) => {
  const {
    publicRuntimeConfig: { feBaseUrl }
  } = getConfig();

  const [finishedModalOpen, setFinishedModalOpen] = useState(false);
  return (
    <>
      <EFCreateFlowLayout
        onNext={() => setFinishedModalOpen(true)}
        backgroundImageSrc={gameImage}
        titleText="Let’s invite gamers to your new team!"
        buttonText="You're all set!"
      >
        <p className={Style.headerText}>Invite Gamers</p>
        <p className={Style.descriptionText}>
          *This link will allow gamers to join your new organization and team. Only share this link with gamers you
          trust!
        </p>
        <div style={{ marginTop: "10px" }}>
          <EFCopyURL url={`${feBaseUrl}/i/${inviteCode}`} />
        </div>
      </EFCreateFlowLayout>
      <TeamCreatedModal
        modalIsOpen={finishedModalOpen}
        gameImage={gameImage}
        onCreateMore={() => Router.push(`/leagues/teams/create/${ownerOrgId}/${orgId}`)}
        onFinished={() => Router.push(`/leagues/dashboard/${orgId}`)}
      />
    </>
  );
};

export default InviteTeamMembers;
