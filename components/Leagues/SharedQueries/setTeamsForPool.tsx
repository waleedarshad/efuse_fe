import { gql } from "@apollo/client";

export interface SetPoolTeamsData {
  updateLeaguePool: {
    teams: { _id: string }[];
  };
}

export interface SetPoolTeamsVars {
  poolId: string;
  poolBody: {
    teams: string[];
  };
}

export const SET_POOL_TEAMS = gql`
  mutation UpdateLeaguePoolMutation($poolId: String!, $poolBody: UpdateLeaguePoolBody!) {
    updateLeaguePool(poolId: $poolId, poolBody: $poolBody) {
      teams {
        _id
      }
    }
  }
`;
