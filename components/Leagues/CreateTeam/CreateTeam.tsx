import React from "react";
import { useForm } from "react-hook-form";
import Router from "next/router";
import { useMutation } from "@apollo/client";
import EFCreateFlowLayout from "../../EFCreateFlowLayout/EFCreateFlowLayout";
import Styles from "./CreateTeam.module.scss";
import EFInputControl from "../../FormControls/EFInputControl";
import EFSelectControl from "../../FormControls/EFSelectControl";
import InputLabel from "../../InputLabel/InputLabel";
import InputField from "../../InputField/InputField";
import {
  CREATE_TEAM_FOR_LEAGUE,
  CreateTeamForLeagueData,
  CreateTeamForLeagueVars
} from "../../../graphql/Team/LeagueTeam";
import { League } from "../../../graphql/interface/League";

interface CreateTeamProps {
  orgId: string;
  ownerOrgId: string;
  leagues: League[];
}

const CreateTeam = ({ orgId, ownerOrgId, leagues }: CreateTeamProps) => {
  const {
    control,
    handleSubmit,
    watch,
    formState: { errors }
  } = useForm({
    defaultValues: leagues.length === 1 ? { league: leagues?.[0]?._id } : { label: "Select a League", value: "" }
  });

  const selectedLeagueId = watch("league");
  const dummyOptions = leagues.map(it => ({ label: it.name, value: it._id }));
  const game = leagues.find(it => it._id === selectedLeagueId)?.game;
  const [createTeamForLeague, { loading }] = useMutation<CreateTeamForLeagueData, CreateTeamForLeagueVars>(
    CREATE_TEAM_FOR_LEAGUE
  );

  const onSubmit = data => {
    const variables: CreateTeamForLeagueVars = {
      leagueId: data.league,
      team: {
        owner: orgId,
        ownerType: "organizations",
        name: data.name,
        game: leagues.find(it => it._id === data?.league)?.game?._id
      }
    };

    createTeamForLeague({ variables }).then(response => {
      Router.push(`/leagues/teams/${response.data.createTeamForLeague._id}/${ownerOrgId}/invite`);
    });
  };

  return (
    <EFCreateFlowLayout
      titleText="Let’s give your team a name!"
      onNext={handleSubmit(onSubmit)}
      backgroundImageSrc={game?.gameImage?.url}
      buttonText="Create team"
      isLoading={loading}
      isRightButtonDisabled={loading}
    >
      <div className={Styles.inputMargin}>
        <EFSelectControl
          errors={errors}
          name="league"
          control={control}
          required
          label="Select League"
          options={
            dummyOptions.length === 1 ? dummyOptions : [{ label: "Select a League", value: "" }, ...dummyOptions]
          }
        />
      </div>
      <div className={Styles.inputMargin}>
        <InputLabel theme="darkColor">Team Game</InputLabel>
        <InputField theme="whiteShadow" size="lg" disabled value={game ? game.title : ""} />
      </div>
      <div className={Styles.inputMargin}>
        <EFInputControl
          errors={errors}
          name="name"
          control={control}
          required
          label="Team Name"
          placeholder="Enter name"
        />
      </div>
    </EFCreateFlowLayout>
  );
};

export default CreateTeam;
