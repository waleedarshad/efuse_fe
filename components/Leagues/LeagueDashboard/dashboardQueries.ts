import { gql } from "@apollo/client";
import { League } from "../../../graphql/interface/League";
import { Organization } from "../../../graphql/interface/Organization";

export interface GetLeaguesByOrgData {
  getLeaguesByOrganization: [League];
}

export interface GetLeaguesByOrgVars {
  organizationId: string;
}

export const GET_LEAGUES_BY_ORG = gql`
  query Query($organizationId: String!) {
    getLeaguesByOrganization(organizationId: $organizationId) {
      _id
      name
      imageUrl
      state
      description
      teams {
        _id
        name
      }
      game {
        _id
        title
        gameImage {
          url
        }
      }
    }
  }
`;

export interface GetOrgInfoByIdData {
  getOrganizationById: Organization;
}

export interface GetOrgInfoByIdVars {
  organizationId: string;
}

export const GET_ORG_INFO_BY_ID = gql`
  query Query($organizationId: String!) {
    getOrganizationById(organizationId: $organizationId) {
      _id
      name
      shortName
      createdAt
      currentUserRole
      user {
        _id
      }
      headerImage {
        url
      }
      profileImage {
        url
      }
      description
      captains {
        _id
      }
      teams {
        _id
        name
        owner {
          ... on Organization {
            profileImage {
              url
            }
          }
        }
        members {
          _id
          user {
            name
            username
            profilePicture {
              url
            }
          }
        }
      }
      members {
        _id
        isBanned
        user {
          _id
          username
          profilePicture {
            url
          }
        }
      }
    }
  }
`;
