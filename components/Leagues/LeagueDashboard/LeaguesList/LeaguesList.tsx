import React from "react";
import { Col, Row } from "react-bootstrap";
import { useRouter } from "next/router";
import { useDispatch } from "react-redux";
import { useMutation, useQuery } from "@apollo/client";
import { faUserPlus } from "@fortawesome/pro-regular-svg-icons";
import { faTrashAlt } from "@fortawesome/pro-solid-svg-icons";
import EFCreateCard from "../../../EFCreateCard/EFCreateCard";
import EFDetailsCard from "../../../EFDetailsCard/EFDetailsCard";
import { League } from "../../../../graphql/interface/League";
import { setErrors } from "../../../../store/actions/errorActions";
import { sendNotification } from "../../../../helpers/FlashHelper";
import { ITeamCardMenuItem } from "../../../Cards/EFTeamCard/EFTeamCard";
import EFMenuItem from "../../../Dropdowns/EFMenuItem/EFMenuItem";
import EFAlert from "../../../EFAlert/EFAlert";
import {
  DELETE_LEAGUE,
  GetLeaguesByOrgData,
  GetLeaguesByOrgVars,
  GET_LEAGUES_BY_ORG
} from "../../../../graphql/leagues/Leagues";
import Style from "./LeaguesList.module.scss";
import { Organization } from "../../../../graphql/interface/Organization";

interface LeaguesListProps {
  orgId: string;
  organization: Organization;
  canCreate: boolean;
}

const LeaguesList: React.FC<LeaguesListProps> = ({ orgId, organization, canCreate }) => {
  const router = useRouter();
  const dispatch = useDispatch();
  const [deleteLeague] = useMutation(DELETE_LEAGUE, {
    refetchQueries: [{ query: GET_LEAGUES_BY_ORG, variables: { organizationId: orgId } }]
  });

  const { data } = useQuery<GetLeaguesByOrgData, GetLeaguesByOrgVars>(GET_LEAGUES_BY_ORG, {
    variables: { organizationId: orgId }
  });

  const leagues = data?.getLeaguesByOrganization;

  const onLeagueCardClick = async (league: League) => {
    await router.push(`/leagues/${league?._id}`);
  };

  const onInviteOrgClick = async () => {
    await router.push(`/leagues/org/${orgId}/invite`);
  };

  const onLeagueDeleteClick = async (league: League) => {
    deleteLeague({
      variables: {
        leagueId: league?._id
      }
    })
      .then(() => {
        sendNotification("Successfully deleted league", "success", "Success");
      })
      .catch(error => {
        dispatch(setErrors({ error: error.message }));
      });
  };

  // Removing invite link here temporarily

  const getMenuItems = (league: League) => {
    const menuItems: ITeamCardMenuItem[] = [
      // {
      //   option: <EFMenuItem icon={faUserPlus} text="Invite Org" iconColorTheme="blue" textColorTheme="black" />,
      //   onClick: () => onInviteOrgClick()
      // },
      {
        option: <EFMenuItem icon={faTrashAlt} text="Delete League" iconColorTheme="red" textColorTheme="red" />,
        onClick: () => {
          const title = "Delete this league?";
          const message = "Are you sure?";
          const confirmLabel = "Yes";
          const denyLabel = "No";
          const onYes = () => {
            onLeagueDeleteClick(league);
          };

          EFAlert(title, message, confirmLabel, denyLabel, onYes);
        }
      }
    ];
    return menuItems;
  };

  return (
    <Row className={Style.cardListContainer}>
      <Col className={Style.cardList}>
        {canCreate && (
          <div className={Style.createLeagueCard}>
            <EFCreateCard text="Create League" onClick={() => router.push(`/leagues/create/${orgId}`)} />
          </div>
        )}
        {leagues &&
          leagues.map(league => {
            return (
              <div key={league?._id} className={Style.leagueCardContainer}>
                <EFDetailsCard
                  title={league?.name}
                  line1={organization?.name}
                  line2={league?.game?.title}
                  line3={`${league?.teams?.length ?? 0} Teams`}
                  dropdownMenuItems={getMenuItems(league)}
                  image={league?.imageUrl}
                  onClick={() => onLeagueCardClick(league)}
                />
              </div>
            );
          })}
      </Col>
    </Row>
  );
};

export default LeaguesList;
