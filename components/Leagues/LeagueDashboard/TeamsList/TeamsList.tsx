import React, { useEffect, useState } from "react";
import { Row, Col } from "react-bootstrap";
import { faTrashAlt, faLink } from "@fortawesome/pro-regular-svg-icons";
import { useRouter } from "next/router";
import { useDispatch } from "react-redux";
import { useMutation } from "@apollo/client";
import { Team } from "../../../../graphql/interface/League";
import EFTeamCard from "../../../Cards/EFTeamCard/EFTeamCard";
import EFMenuItem from "../../../Dropdowns/EFMenuItem/EFMenuItem";
import Style from "./TeamsList.module.scss";
import { DELETE_LEAGUE_TEAM } from "../../../../graphql/Team/LeagueTeam";
import { sendNotification } from "../../../../helpers/FlashHelper";
import { setErrors } from "../../../../store/actions/errorActions";

interface TeamListProps {
  teams: Team[];
  hideInvite: boolean;
  isAdmin: boolean;
}

const TeamsList: React.FC<TeamListProps> = ({ teams, hideInvite, isAdmin }) => {
  const router = useRouter();
  const dispatch = useDispatch();

  const [deleteLeagueTeam] = useMutation(DELETE_LEAGUE_TEAM);
  const [currentTeams, setCurrentTeams] = useState(teams);

  const getInviteLinkHref = teamId => `/leagues/teams/${teamId}/${router.query.orgId}/invite`;

  useEffect(() => {
    setCurrentTeams(teams);
  }, [teams]);

  const deleteTeam = async (_id: string, index: number) => {
    deleteLeagueTeam({
      variables: {
        teamId: _id
      }
    })
      .then(() => {
        sendNotification("Team removed successfully", "success", "Success");

        // remove the deleted team from state
        const updatedTeams = [...currentTeams];
        updatedTeams.splice(index, 1);
        setCurrentTeams(updatedTeams);
      })
      .catch(error => {
        dispatch(setErrors({ error: error.message }));
      });
  };

  const getMenuItems = (team, index) => {
    const menu = [];
    menu.push({
      option: <EFMenuItem icon={faTrashAlt} text="Remove" iconColorTheme="red" textColorTheme="red" />,
      onClick: () => deleteTeam(team._id, index)
    });
    if (!hideInvite) {
      menu.push({
        option: <EFMenuItem icon={faLink} text="Invite Link" iconColorTheme="blue" textColorTheme="black" />,
        onClick: () => router.push(getInviteLinkHref(team._id))
      });
    }
    return menu;
  };

  return (
    <Row className={Style.cardListContainer}>
      <Col className={Style.cardList}>
        {currentTeams?.map((team, index) => {
          return (
            <EFTeamCard
              key={team._id}
              teamImageUrl={team?.owner?.profileImage?.url}
              teamMembers={team.members?.map(member => ({
                imageUrl: member.user.profilePicture.url,
                name: member.user.name
              }))}
              teamName={team.name}
              menuItems={getMenuItems(team, index)}
              inviteLink={getInviteLinkHref(team._id)}
              imageHref={`/leagues/dashboard/${team?.owner?._id}`}
              hideInvite={hideInvite}
              noMembersMessage="Currently no members are part of this team."
              isAdmin={isAdmin}
            />
          );
        })}
      </Col>
    </Row>
  );
};

export default TeamsList;
