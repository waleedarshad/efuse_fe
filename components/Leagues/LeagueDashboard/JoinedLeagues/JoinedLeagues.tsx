import React from "react";
import { Col, Row } from "react-bootstrap";
import { useRouter } from "next/router";
import { useQuery } from "@apollo/client";
import Link from "next/link";
import EFDetailsCard from "../../../EFDetailsCard/EFDetailsCard";
import { League } from "../../../../graphql/interface/League";
import { Organization } from "../../../../graphql/organizations/Organizations";
import {
  GET_JOINED_OR_PENDING_LEAGUES_BY_ORG,
  GetJoinedOrPendingLeaguesByOrgData,
  GetJoinedOrPendingLeaguesByOrgVars
} from "../../../../graphql/leagues/Leagues";
import Style from "../LeaguesList/LeaguesList.module.scss";

interface LeaguesListProps {
  orgId: string;
  organization: Organization;
}

const LeaguesList: React.FC<LeaguesListProps> = ({ orgId, organization }) => {
  const router = useRouter();

  const { data } = useQuery<GetJoinedOrPendingLeaguesByOrgData, GetJoinedOrPendingLeaguesByOrgVars>(
    GET_JOINED_OR_PENDING_LEAGUES_BY_ORG,
    {
      variables: { organizationId: orgId, joinStatus: "joined" }
    }
  );

  const joinedLeagues = data?.getJoinedOrPendingLeaguesByOrg;

  const onLeagueCardClick = async (league: League) => {
    await router.push(`/leagues/${league?._id}`);
  };

  const getJoinedTeam = leagueTeams => {
    const joinedTeams = organization.teams?.filter(team =>
      leagueTeams?.some(leagueTeam => leagueTeam._id === team._id)
    );

    // Temporary href value added
    return <Link href={`/leagues/dashboard/${orgId}?tab=joined`}>{joinedTeams[0].name}</Link>;
  };

  return (
    <Row className={Style.cardListContainer}>
      <Col className={Style.cardList}>
        {joinedLeagues &&
          joinedLeagues.map(joinedLeague => {
            return (
              <div key={joinedLeague.league?._id} className={Style.leagueCardContainer}>
                <EFDetailsCard
                  title={joinedLeague.league?.name}
                  line1={joinedLeague.league?.owner?.name}
                  line2={joinedLeague.league?.game?.title}
                  line3={<>Joined with {getJoinedTeam(joinedLeague.league.teams)}</>}
                  image={joinedLeague.league?.imageUrl}
                  onClick={() => onLeagueCardClick(joinedLeague.league)}
                />
              </div>
            );
          })}
      </Col>
    </Row>
  );
};

export default LeaguesList;
