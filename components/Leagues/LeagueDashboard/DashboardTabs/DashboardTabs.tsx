import React from "react";
import { useRouter } from "next/router";
import { Team } from "../../../../graphql/interface/League";
import EFTabs from "../../../EFTabs/EFTabs";
import LeaguesList from "../LeaguesList/LeaguesList";
import MembersList from "../MembersList/MembersList";
import TeamsList from "../TeamsList/TeamsList";
import JoinedLeagues from "../JoinedLeagues/JoinedLeagues";
import PendingLeagues from "../PendingLeagues/PendingLeagues";
import { Organization } from "../../../../graphql/interface/Organization";

interface DashboardTabsProps {
  orgId: string;
  teams: [Team];
  organization: Organization;
  canCreateLeagues: boolean;
  hasOrgPermissions: boolean;
}

const DashboardTabs: React.FC<DashboardTabsProps> = ({
  orgId,
  organization,
  teams,
  canCreateLeagues,
  hasOrgPermissions
}) => {
  const router = useRouter();

  const onTabSelect = tabKey => {
    router.push(`/leagues/dashboard/${orgId}/?tab=${tabKey}`, undefined, { shallow: true });
  };

  const canCreateLeaguesTabs = [
    {
      tabTitle: "Created Leagues",
      tabKey: "created",
      content: <LeaguesList orgId={orgId} organization={organization} canCreate={canCreateLeagues} />,
      onTabSelect
    }
  ];

  const publicTabs = [
    {
      tabTitle: "Joined Leagues",
      tabKey: "joined",
      content: <JoinedLeagues orgId={orgId} organization={organization} />,
      onTabSelect
    },
    {
      tabTitle: "Pending Leagues",
      tabKey: "pending",
      content: <PendingLeagues orgId={orgId} />,
      onTabSelect
    },
    {
      tabTitle: "Teams",
      tabKey: "teams",
      content: (
        <TeamsList
          teams={teams}
          orgProfileImageUrl={organization?.profileImage?.url}
          orgId={orgId}
          hideInvite={!hasOrgPermissions}
          isAdmin={hasOrgPermissions}
        />
      ),
      onTabSelect
    },
    {
      tabTitle: "Members",
      tabKey: "members",
      content: <MembersList members={organization?.members || []} orgId={orgId} organization={organization} />,
      onTabSelect
    }
  ];

  const tabs = [...(canCreateLeagues ? canCreateLeaguesTabs : []), ...publicTabs];

  return <EFTabs defaultActiveKey={router.query.tab?.toString()} tabs={tabs} tabSize="large" />;
};

export default DashboardTabs;
