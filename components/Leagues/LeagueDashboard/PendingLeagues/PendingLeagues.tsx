import React from "react";
import { Col, Row } from "react-bootstrap";
import { useRouter } from "next/router";
import { useQuery } from "@apollo/client";
import styled from "styled-components";
import EFDetailsCard from "../../../EFDetailsCard/EFDetailsCard";
import { League } from "../../../../graphql/interface/League";
import {
  GET_JOINED_OR_PENDING_LEAGUES_BY_ORG,
  GetJoinedOrPendingLeaguesByOrgData,
  GetJoinedOrPendingLeaguesByOrgVars
} from "../../../../graphql/leagues/Leagues";
import Style from "../LeaguesList/LeaguesList.module.scss";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";
import ConfirmAlert from "../../../ConfirmAlert/ConfirmAlert";
import colors from "../../../../styles/sharedStyledComponents/colors";

interface LeaguesListProps {
  orgId: string;
}

const LeaguesList: React.FC<LeaguesListProps> = ({ orgId }) => {
  const router = useRouter();

  const { data } = useQuery<GetJoinedOrPendingLeaguesByOrgData, GetJoinedOrPendingLeaguesByOrgVars>(
    GET_JOINED_OR_PENDING_LEAGUES_BY_ORG,
    {
      variables: { organizationId: orgId, joinStatus: "pending" }
    }
  );

  const pendingLeagues = data?.getJoinedOrPendingLeaguesByOrg;

  const onLeagueCardClick = async (league: League) => {
    await router.push(`/leagues/${league?._id}`);
  };

  const getActionButtons = leagueOwnerId => (
    <ButtonsWrapper>
      {/* Commenting now until we get BE ready for this */}
      {/* <ConfirmAlert
        title="Leave this league"
        message="Are you sure you want to leave this league?"
        onYes={() => console.log("yes")}
      >
        <EFRectangleButton
          colorTheme="transparent"
          text="Dismiss"
          shadowTheme="none"
          size="extraSmall"
          fontWeightTheme="bold"
        />
      </ConfirmAlert> */}

      <EFRectangleButton
        colorTheme="transparent"
        text="Create team"
        shadowTheme="none"
        size="extraSmall"
        fontWeightTheme="bold"
        fontColorTheme="primary"
        onClick={e => {
          e.stopPropagation();

          router.push(`/leagues/teams/create/${leagueOwnerId}/${orgId}`);
        }}
      />
    </ButtonsWrapper>
  );

  return pendingLeagues?.length > 0 ? (
    <Row className={Style.cardListContainer}>
      <Col className={Style.cardList}>
        {pendingLeagues.map(pendingLeague => {
          return (
            <div key={pendingLeague.league?._id} className={Style.leagueCardContainer}>
              <EFDetailsCard
                title={pendingLeague.league?.name}
                line1={pendingLeague.league?.owner?.name}
                line2={pendingLeague.league?.game?.title}
                line3={getActionButtons(pendingLeague.league?.owner?._id)}
                image={pendingLeague.league?.imageUrl}
                onClick={() => onLeagueCardClick(pendingLeague.league)}
              />
            </div>
          );
        })}
      </Col>
    </Row>
  ) : (
    <Placeholder>You currently do not have any Pending Leagues</Placeholder>
  );
};

const ButtonsWrapper = styled.div`
  display: flex;

  button {
    font-weight: 600 !important;
    padding: 0px;
  }
`;

const Placeholder = styled.div`
  display: flex;
  height: 450px;
  justify-content: center;
  align-items: center;
  font-weight: 500;
  font-size: 18px;
  color: ${colors.textStrong};
`;

export default LeaguesList;
