import React from "react";
import { Col, Row } from "react-bootstrap";
import { useSelector } from "react-redux";
import TableItemMember from "../../../Organizations/Table/TableItem/TableItemMember";
import { Member } from "../../../../graphql/interface/League";
import { Organization } from "../../../../graphql/interface/Organization";

interface MemberListProps {
  members: Member[];
  orgId: string;
  organization: Organization;
}

const MembersList: React.FC<MemberListProps> = ({ members, orgId, organization }) => {
  const authUser = useSelector(state => state.auth.currentUser);

  const orgCaptains = organization?.captains.map(orgCaptain => orgCaptain?._id);

  // Hardcoding this to have admin false in order to temporarily hide ellipsis button

  return (
    <Row>
      {members.map((member, index) => {
        return (
          <Col lg={12} md={12} key={index}>
            <TableItemMember
              member={member}
              currentUser={authUser}
              slug={organization?.shortName}
              isAdmin={false}
              owner={organization?.user?._id}
              captains={orgCaptains}
              hasChildOrgs={false}
              organizationId={orgId}
            />
          </Col>
        );
      })}
    </Row>
  );
};

export default MembersList;
