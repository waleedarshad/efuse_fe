import React from "react";
import { useRouter } from "next/router";
import EFProfileHeaderCard from "../../Cards/EFProfileHeaderCard/EFProfileHeaderCard";
import { withCdn } from "../../../common/utils";
import { League } from "../../../graphql/interface/League";
import DashboardTabs from "./DashboardTabs/DashboardTabs";
import LeagueNavContainer from "../LeagueNavigation/LeagueSidebar/LeagueNavContainer";
import { Organization, OrganizationUserRoles } from "../../../graphql/interface/Organization";
import LeaguesDashboardLayout from "../Layouts/LeaguesDashboardLayout/LeaguesDashboardLayout";

const LeagueDashboard = ({
  orgId,
  leagues,
  organization,
  userLeagueOrgRoles,
  canCreateLeagues = false,
  hasOrgPermissions = false
}: {
  orgId: string;
  leagues: League[];
  organization: Organization;
  userLeagueOrgRoles: OrganizationUserRoles;
  canCreateLeagues?: boolean;
  hasOrgPermissions?: boolean;
}) => {
  const router = useRouter();
  let totalTeams = 0;
  leagues?.forEach(league => {
    totalTeams = totalTeams + league?.teams?.length ?? 0;
  });
  return (
    <LeagueNavContainer userLeagueOrgRoles={userLeagueOrgRoles}>
      <LeaguesDashboardLayout
        header={
          <>
            {organization && (
              <EFProfileHeaderCard
                backgroundImage={organization?.headerImage?.url ?? withCdn("/static/images/valorant_banner.jpg")}
                title={organization?.name}
                date={organization?.createdAt}
                profileImage={organization?.profileImage?.url}
                teams={totalTeams}
                leagues={leagues?.length ?? 0}
                description={organization?.description}
                primaryBtnText="Create League"
                onPrimaryBtnClick={() => router.push(`/leagues/create/${orgId}`)}
                secondaryBtnText="Invite"
                onSecondaryBtnClick={() => router.push(`/leagues/org/${orgId}/invite`)}
                showButtons={canCreateLeagues}
              />
            )}
          </>
        }
      >
        <DashboardTabs
          orgId={orgId}
          organization={organization}
          teams={organization?.teams}
          canCreateLeagues={canCreateLeagues}
          hasOrgPermissions={hasOrgPermissions}
        />
      </LeaguesDashboardLayout>
    </LeagueNavContainer>
  );
};

export default LeagueDashboard;
