import React from "react";
import { withCdn } from "../../../../common/utils";
import Style from "../GameCard.module.scss";
import GroupStats from "../GameBoard/GroupStats/GroupStats";
import IndividualStats from "../GameBoard/IndividualStats/IndividualStats";
import EFImage from "../../../EFImage/EFImage";

const GameCardBody = ({
  cardBodyStats,
  structure,
  cardFooterStats,
  badgeImage,
  badgeTitle,
  className,
  sectionTitle,
  noStatsMessage,
  shouldDisplayFooter
}) => {
  return cardBodyStats?.value ? (
    <>
      <div className={Style.left}>
        <div className={Style.leaguePoints}>
          <span className={Style.value}>{cardBodyStats.value}</span>
          <span className={Style.label}>{cardBodyStats.unit}</span>
          {cardBodyStats.rank && <div className={Style.rank}>{cardBodyStats.rank}</div>}
        </div>

        {shouldDisplayFooter && (
          <div className={`${Style.winLossSection} ${className}`}>
            {structure === "GroupStats" ? (
              <GroupStats cardFooterStats={cardFooterStats} />
            ) : (
              <IndividualStats cardFooterStats={cardFooterStats} />
            )}
          </div>
        )}
      </div>
      {badgeImage && (
        <div className={Style.tier}>
          <div className={Style.imageContainer}>
            <EFImage className={Style.image} src={withCdn(badgeImage, true)} alt={badgeTitle} />
          </div>
          <span className={Style.label}>{badgeTitle}</span>
        </div>
      )}
    </>
  ) : (
    <div className={Style.left}>
      <div className={Style.leaguePoints}>{noStatsMessage || `This gamer has not played ${sectionTitle}.`}</div>
    </div>
  );
};

GameCardBody.defaultProps = {
  shouldDisplayFooter: true
};

export default GameCardBody;
