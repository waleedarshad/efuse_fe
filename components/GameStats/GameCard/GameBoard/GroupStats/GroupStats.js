import uniqueId from "lodash/uniqueId";
import React from "react";

import Style from "../../GameCard.module.scss";

const GroupStats = ({ cardFooterStats }) => {
  return cardFooterStats.map((stats, index) => (
    <div
      key={uniqueId()}
      className={`${Style.winsAndLosses} ${cardFooterStats.length > 0 && Style.group} ${cardFooterStats.length > 1 &&
        index === 0 &&
        Style.border}`}
    >
      <div>
        <span className={Style.value}>{stats.value1}</span>
        <span className={`${Style.label} ${Style.first}`}>{stats.unit1}</span>
        <span className={Style.value}>{stats.value2}</span>
        <span className={`${Style.label} ${Style.first}`}>{stats.unit2}</span>
      </div>
      {stats.winRatio && <div className={Style.winRatio}>{`Win Ratio ${stats.winRatio}%`}</div>}
      {stats.rank && <div className={Style.winRatio}>{`Top ${stats.rank}%`}</div>}
      {stats.subText && <div className={Style.winRatio}>{stats.subText}</div>}
    </div>
  ));
};
export default GroupStats;
