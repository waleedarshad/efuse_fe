import uniqueId from "lodash/uniqueId";
import React from "react";

import Style from "../../GameCard.module.scss";

const IndividualStat = ({ cardFooterStats }) => {
  return cardFooterStats.map(stats => (
    <div key={uniqueId()} className={stats.rank ? Style.rank : ""}>
      <div className={Style.winsAndLosses}>
        <span className={Style.value}>{stats.value}</span>
        <span className={`${Style.label} ${Style.first}`}>{stats.unit}</span>
      </div>
      {stats.rank && <div className={Style.winRatio}>{`Top ${stats.rank}%`}</div>}
      {stats.subText && <div className={Style.winRatio}>{stats.subText}</div>}
    </div>
  ));
};
export default IndividualStat;
