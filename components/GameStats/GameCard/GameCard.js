import React from "react";
import Style from "./GameCard.module.scss";
import GameCardBody from "./GameCardBody/GameCardBody";
import GameCardWrapper from "../GameCardWrapper/GameCardWrapper";

const GameCard = ({
  headerText,
  cardBodyStats = {},
  cardFooterStats = [],
  structure,
  badgeImage,
  badgeTitle,
  className,
  tooltip,
  noStatsMessage,
  shouldDisplayFooter
}) => {
  return (
    <GameCardWrapper tooltip={tooltip} headerText={headerText}>
      <div className={Style.stats}>
        <GameCardBody
          cardBodyStats={cardBodyStats}
          structure={structure}
          cardFooterStats={cardFooterStats}
          shouldDisplayFooter={shouldDisplayFooter}
          badgeImage={badgeImage}
          badgeTitle={badgeTitle}
          className={className}
          sectionTitle={headerText}
          noStatsMessage={noStatsMessage}
        />
      </div>
    </GameCardWrapper>
  );
};

GameCard.defaultProps = {
  shouldDisplayFooter: true
};

export default GameCard;
