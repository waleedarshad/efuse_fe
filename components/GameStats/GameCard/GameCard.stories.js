import GameCard from "./GameCard";

export default {
  title: "stats/GameCard",
  component: GameCard,
  args: {
    headerText: {
      control: {
        type: "string"
      }
    },
    cardBodyStats: {
      control: {
        type: "object"
      }
    },
    cardFooterStats: {
      control: {
        type: "array"
      }
    }
  }
};

// eslint-disable-next-line react/display-name
const Story = args => <GameCard {...args} />;

export const GameStatsCard = Story.bind({});
GameStatsCard.args = {
  headerText: "Competitive",
  cardBodyStats: { unit: "KD", value: "100", rank: "Top 20%" },
  cardFooterStats: [
    {
      unit: "EPG",
      value: "20"
    },
    {
      unit: "DPG",
      value: "40"
    },
    {
      unit: "ELIMS",
      value: "76",
      rank: "49"
    }
  ]
};
