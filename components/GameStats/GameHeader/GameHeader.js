import isNil from "lodash/isNil";
import React from "react";
import { withCdn } from "../../../common/utils";
import EFImage from "../../EFImage/EFImage";
import Style from "./GameHeader.module.scss";

const GameHeader = ({
  userName,
  characterTitle,
  characterName,
  characterAvatar,
  characterWins,
  characterLosses,
  characterWinPercent,
  characterKd,
  wins,
  losses,
  ranked,
  winPercent,
  platformLogo,
  platformName
}) => {
  const hasUserStats = () => {
    if (wins === 0 && losses === 0) {
      return false;
    }

    return !(isNil(wins) || isNil(losses) || isNil(winPercent));
  };

  const hasCharacterStats = () => {
    return !(isNil(characterWins) || isNil(characterLosses) || isNil(characterWinPercent) || isNil(characterKd));
  };

  const hasStats = () => {
    return hasUserStats() && hasCharacterStats();
  };

  const hasPlatform = () => {
    return !(isNil(platformLogo) || isNil(platformName));
  };

  const renderPlatform = () => {
    return (
      <>
        <div className={Style.statSection}>
          <EFImage className={Style.logoImage} src={withCdn(platformLogo)} alt={platformName} />
        </div>
        <div className={Style.statSection}>
          <span className={`${Style.statTitle} ${Style.first}`}>{platformName.toUpperCase()}</span>
        </div>
      </>
    );
  };

  const renderCharacterStats = () => (
    <>
      <div className={Style.statSection}>
        <span className={Style.statValue}>{characterWins}</span>
        <span className={Style.statLabel}>W</span>
      </div>

      <div className={Style.statSection}>
        <span className={Style.statValue}>{characterLosses}</span>
        <span className={Style.statLabel}>L</span>
      </div>

      <div className={Style.statSection}>
        <span className={Style.statValue}>
          {typeof characterWinPercent === "function" ? characterWinPercent() : characterWinPercent}
        </span>
        <span className={Style.statLabel}>%</span>
      </div>

      <div className={Style.statSection}>
        <span className={Style.statValue}>{characterKd}</span>
        <span className={Style.statLabel}>KD</span>
      </div>
    </>
  );

  const renderUserStats = () => (
    <>
      <div className={Style.statSection}>
        <span className={Style.statValue}>{wins}</span>
        <span className={Style.statLabel}>W</span>
      </div>
      <div className={Style.statSection}>
        <span className={Style.statValue}>{losses}</span>
        <span className={Style.statLabel}>L</span>
      </div>
      <div className={Style.statSection}>
        <span className={Style.statValue}>{typeof winPercent === "function" ? winPercent() : winPercent}</span>
        <span className={Style.statLabel}>%</span>
      </div>
    </>
  );

  const hasCharacterInfo = () => {
    return !(isNil(characterTitle) || isNil(characterAvatar) || isNil(characterName));
  };

  const renderCharacterSection = () => {
    return (
      <>
        <div className={Style.statSection}>
          <span className={Style.statTitle}>{characterTitle.toUpperCase()}</span>
        </div>
        <span className={Style.separator} />

        <div className={Style.statIcon}>
          <EFImage className={Style.logoImage} src={withCdn(characterAvatar, true)} alt={characterName} />
        </div>

        <div className={Style.statSection}>
          <span className={Style.statValue}>{characterName}</span>
        </div>
        {hasCharacterStats() && renderCharacterStats()}
      </>
    );
  };

  return (
    <div className={Style.header}>
      <div className={`${Style.headerSection} ${Style.first}`}>
        {hasPlatform() && renderPlatform()}

        <div className={Style.statSection}>
          <span className={`${Style.statTitle} ${Style.first}`}>
            {userName.toUpperCase()}
            {hasStats() && " STATS"}
          </span>
        </div>

        {hasStats() || !ranked ? <span className={Style.separator} /> : <></>}

        {!ranked && (
          <div className={Style.statSection}>
            <span className={Style.statValue}>Unranked</span>
          </div>
        )}

        {hasUserStats() && renderUserStats()}
        {hasCharacterInfo() && renderCharacterSection()}
      </div>
    </div>
  );
};

export default GameHeader;
