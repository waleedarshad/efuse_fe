import GameHeader from "./GameHeader";

export default {
  title: "stats/Header",
  component: GameHeader,
  args: {
    userName: {
      control: {
        type: "string"
      }
    },
    championTitle: {
      control: {
        type: "string"
      }
    },
    championName: {
      control: {
        type: "string"
      }
    },
    championAvatar: {
      control: {
        type: "string"
      }
    },
    wins: {
      control: {
        type: "string"
      }
    },
    losses: {
      control: {
        type: "string"
      }
    },
    ranked: {
      control: {
        type: "string"
      }
    },
    winPercent: {
      control: {
        type: "string"
      }
    },
    hasPlatform: {
      control: {
        type: "string"
      }
    },
    platformLogo: {
      control: {
        type: "string"
      }
    },
    platformName: {
      control: {
        type: "string"
      }
    }
  }
};

// eslint-disable-next-line react/display-name
const Story = args => <GameHeader {...args} />;

export const GameStatsHeader = Story.bind({});
GameStatsHeader.args = {
  userName: "Tobirama#11278",
  wins: "100",
  losses: "20",
  ranked: "23",
  championTitle: "",
  championName: "",
  championAvatar: "",
  winPercent: "100%",
  platformLogo: "/static/images/black_battlenet.png",
  platformName: "Tobirama",
  hasPlatform: true
};
