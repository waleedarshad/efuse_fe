import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faInfoCircle } from "@fortawesome/pro-regular-svg-icons";
import PropTypes from "prop-types";
import React from "react";
import EFFormattedTooltip from "../../tooltip/EFFormattedTooltip/EFFormattedTooltip";
import Style from "./GameCardWrapper.module.scss";

const GameCardWrapper = ({ children, tooltip, headerText, width }) => {
  return (
    <div className={`${Style.root} ${Style[width]}`}>
      {tooltip && (
        <div className={Style.tooltip}>
          <EFFormattedTooltip tooltipContent={tooltip}>
            <FontAwesomeIcon icon={faInfoCircle} />
          </EFFormattedTooltip>
        </div>
      )}
      <div className={Style.childrenWrapper}>
        <div className={Style.title}>{headerText}</div>
        {children}
      </div>
    </div>
  );
};

GameCardWrapper.propTypes = {
  width: PropTypes.oneOf(["fullWidth", "auto", "medium", "small"])
};

GameCardWrapper.defaultProps = {
  width: "medium"
};

export default GameCardWrapper;
