import React from "react";
import Style from "./SkillsCard.module.scss";

const SkillsCard = ({ value, label }) => {
  return (
    <div className={Style.skillCard}>
      <div className={Style.value}>{value}</div>
      <div className={Style.label}>{label}</div>
    </div>
  );
};

export default SkillsCard;
