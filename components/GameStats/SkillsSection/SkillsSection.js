import React from "react";
import uniqueId from "lodash/uniqueId";
import SkillsCard from "./SkillsCard/SkillsCard";
import Style from "./SkillsSection.module.scss";
import GameCardWrapper from "../GameCardWrapper/GameCardWrapper";

const SkillsSection = ({ skills }) => {
  return (
    <GameCardWrapper headerText="SKILLS" width="auto">
      <div className={Style.skillsWrapper}>
        {skills?.map(skill => (
          <div className={Style.marginRight} key={uniqueId()}>
            <SkillsCard value={skill?.value} label={skill.label} />
          </div>
        ))}
      </div>
    </GameCardWrapper>
  );
};

export default SkillsSection;
