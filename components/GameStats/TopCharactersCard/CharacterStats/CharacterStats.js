import uniqueId from "lodash/uniqueId";
import PropTypes from "prop-types";
import React from "react";
import EFImage from "../../../EFImage/EFImage";
import Style from "./CharacterStats.module.scss";

const CharacterStats = ({ avatar, name, stats, description }) => (
  <div className={Style.champion}>
    <div className={Style.championIcon}>
      <EFImage src={avatar} alt={name} />
    </div>
    <div className={Style.stats}>
      <span className={Style.name}>{name}</span>

      <div className={Style.statRow}>
        {stats.map(stat => (
          <div key={uniqueId()} className={Style.stat}>
            <span className={Style.value}>{stat.value}</span>
            <span className={Style.label}>{stat.unit}</span>
          </div>
        ))}
      </div>
      <span className={Style.description}>{description}</span>
    </div>
  </div>
);

CharacterStats.propTypes = {
  avatar: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  stats: PropTypes.arrayOf(
    PropTypes.shape({
      value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
      unit: PropTypes.string
    })
  ),
  description: PropTypes.string
};

CharacterStats.defaultProps = {
  stats: [],
  description: ""
};

export default CharacterStats;
