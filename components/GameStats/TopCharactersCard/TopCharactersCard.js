import isEmpty from "lodash/isEmpty";
import uniqueId from "lodash/uniqueId";
import React from "react";
import GameCardWrapper from "../GameCardWrapper/GameCardWrapper";
import Style from "./TopCharactersCard.module.scss";

const TopCharactersCard = ({ headerText, tooltip, charactersData, renderCharacter }) => {
  if (isEmpty(charactersData)) {
    return (
      <GameCardWrapper tooltip={tooltip} headerText={headerText} width="small">
        <div className={Style.missingText}>{`This gamer does not have any ${headerText.toLowerCase()}.`}</div>
      </GameCardWrapper>
    );
  }

  return (
    <GameCardWrapper tooltip={tooltip} headerText={headerText} width="small">
      {charactersData.map(character => (
        <React.Fragment key={uniqueId()}>{renderCharacter(character)}</React.Fragment>
      ))}
    </GameCardWrapper>
  );
};

TopCharactersCard.defaultProps = {
  charactersData: []
};

export default TopCharactersCard;
