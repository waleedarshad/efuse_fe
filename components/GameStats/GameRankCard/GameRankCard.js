import React from "react";
import Style from "./GameRankCard.module.scss";
import GameCardWrapper from "../GameCardWrapper/GameCardWrapper";
import { withCdn } from "../../../common/utils";

const GameRankCard = ({ image, label, subText }) => {
  return (
    <GameCardWrapper headerText="RANKING">
      <div className={Style.rankWrapper}>
        {image && <img className={Style.image} src={withCdn(image, true)} alt="Aim lab rank" />}
        <p className={Style.label}>{label}</p>
        <p className={Style.subText}>{subText}</p>
      </div>
    </GameCardWrapper>
  );
};

export default GameRankCard;
