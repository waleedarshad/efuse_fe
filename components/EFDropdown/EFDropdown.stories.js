import React from "react";
import { faEllipsisV } from "@fortawesome/pro-solid-svg-icons";

import EFDropdown from "./EFDropdown";

export default {
  title: "Dropdown/EFDropdown",
  component: EFDropdown
};

const Story = args => <EFDropdown {...args} />;

export const Default = Story.bind({});

Default.args = {
  icon: faEllipsisV,
  headerText: "Options",
  items: [
    {
      text: "Edit",
      onClick: () => {}
    },
    { text: "Delete", onClick: () => {} }
  ]
};
