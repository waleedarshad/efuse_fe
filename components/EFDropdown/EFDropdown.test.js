import { shallow, mount } from "enzyme";
import { Dropdown } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEllipsisV } from "@fortawesome/pro-solid-svg-icons";
import EFDropdown from "./EFDropdown";

describe("EFDropdown", () => {
  let subject;
  let fakeEdit;
  const dropdown = (
    <EFDropdown
      icon={faEllipsisV}
      headerText="Options"
      items={[
        {
          text: "Edit",
          onClick: () => fakeEdit()
        },
        { text: "Delete", onClick: () => {} }
      ]}
    />
  );

  beforeEach(() => {
    fakeEdit = jest.fn();
    subject = shallow(dropdown);
  });

  it("renders a dropdown", () => {
    expect(subject.find(Dropdown)).toHaveLength(1);
  });

  it("renders the icon provided as toggle", () => {
    subject = mount(dropdown);
    expect(subject.find(FontAwesomeIcon).prop("icon")).toEqual(faEllipsisV);
  });

  it("renders text for each option provided", () => {
    expect(subject.find(Dropdown.Item)).toHaveLength(2);
    expect(
      subject
        .find(Dropdown.Item)
        .at(0)
        .text()
    ).toEqual("Edit");
    expect(
      subject
        .find(Dropdown.Item)
        .at(1)
        .text()
    ).toEqual("Delete");
  });

  subject = shallow(dropdown);

  it("calls provided onClick handler", () => {
    subject
      .find(Dropdown.Item)
      .at(0)
      .simulate("click");

    expect(fakeEdit).toHaveBeenCalled();
  });
});
