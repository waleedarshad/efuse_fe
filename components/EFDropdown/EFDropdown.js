import React from "react";
import { Col, Dropdown } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEllipsisV } from "@fortawesome/pro-solid-svg-icons";
import PropTypes from "prop-types";
import Style from "./EFDropdown.module.scss";

const CustomToggle = icon =>
  // eslint-disable-next-line react/display-name
  React.forwardRef(({ children, onClick }, ref) => (
    // eslint-disable-next-line jsx-a11y/anchor-is-valid
    <a
      href=""
      ref={ref}
      onClick={e => {
        e.preventDefault();
        onClick(e);
      }}
    >
      {children}
      <FontAwesomeIcon icon={icon} className={Style.moreDropdownIcon} />
    </a>
  ));

const EFDropdown = ({ icon, headerText, items }) => {
  return (
    <Col md={{ span: 10, offset: 1 }}>
      <Dropdown>
        <Dropdown.Toggle as={CustomToggle(icon)} />
        <Dropdown.Menu size="sm" title="">
          <Dropdown.Header>{headerText}</Dropdown.Header>

          {items.map((item, index) => (
            <Dropdown.Item key={index} onClick={item.onClick} className={item.customClass}>
              {item.text}
            </Dropdown.Item>
          ))}
        </Dropdown.Menu>
      </Dropdown>
    </Col>
  );
};

export default EFDropdown;

EFDropdown.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  icon: PropTypes.object,
  items: PropTypes.arrayOf(
    PropTypes.shape({
      text: PropTypes.string,
      onClick: PropTypes.func
    })
  ).isRequired,
  headerText: PropTypes.string
};

EFDropdown.defaultProps = {
  icon: faEllipsisV,
  headerText: "Options"
};
