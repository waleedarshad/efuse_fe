import React from "react";
import { useSelector } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFlame, faEye } from "@fortawesome/pro-solid-svg-icons";
import { Tooltip, OverlayTrigger } from "react-bootstrap";
import { useQuery } from "@apollo/client";

import Style from "./SocialImpactScores.module.scss";
import { GET_USER_BY_ID } from "../../graphql/UserQuery";
import EFBomb from "../Icons/EFBomb/EFBomb";

const SocialImpactScores = ({ stats, authUserId }) => {
  const socialImpactScores = useSelector(state => state.features.social_impact_scores);

  const { data } = useQuery(GET_USER_BY_ID, { variables: { id: authUserId }, skip: !authUserId });
  const tokenBalance = data?.getUserById?.tokenBalance;

  if (!socialImpactScores) {
    return <></>;
  }
  return (
    <div className={Style.socialImpactScoreContainer}>
      <OverlayTrigger
        placement="top"
        overlay={
          <Tooltip id="tooltip-top" style={{ zIndex: 9999 }}>
            <p className={Style.tooltipTitle}>Increase Hype Score:</p>
            <ul className={Style.listAlign}>
              <li>Engagements with your posts</li>
              <li>Engagements with your comments</li>
            </ul>
          </Tooltip>
        }
      >
        <div className={Style.socialImpactCard}>
          <FontAwesomeIcon icon={faFlame} className={`${Style.socialIcon} ${Style.hypeColor}`} />
          {stats ? <p className={Style.score}>{stats.hypeScore}</p> : <p className={Style.score}>0</p>}
          <p className={Style.socialTitle}>Hype Score</p>
        </div>
      </OverlayTrigger>
      <OverlayTrigger
        placement="top"
        overlay={
          <Tooltip id="tooltip-top" style={{ zIndex: 9999 }}>
            <p className={Style.tooltipTitle}>Earn eFuse Tokens By:</p>
            <ul className={Style.listAlign}>
              <li>Commenting on Posts</li>
              <li>Hyping Posts</li>
            </ul>
            <p className={Style.tooltipTitle}>Spend eFuse Tokens By:</p>
            <ul className={Style.listAlign}>
              <li>Boosting Posts</li>
            </ul>
          </Tooltip>
        }
      >
        <div className={Style.socialImpactCard}>
          <EFBomb size="extra-small" darkBomb />
          <p className={Style.score}>{tokenBalance}</p>
          <p className={Style.socialTitle}>eFuse Tokens</p>
        </div>
      </OverlayTrigger>
      <OverlayTrigger
        placement="top"
        overlay={
          <Tooltip id="tooltip-top" style={{ zIndex: 9999 }}>
            Total views across all posts on eFuse
          </Tooltip>
        }
      >
        <div className={Style.socialImpactCard}>
          <FontAwesomeIcon icon={faEye} className={`${Style.socialIcon} ${Style.viewColor}`} />
          {stats ? <p className={Style.score}>{stats.postViews}</p> : <p className={Style.score}>0</p>}
          <p className={Style.socialTitle}>Post Views</p>
        </div>
      </OverlayTrigger>
    </div>
  );
};

export default SocialImpactScores;
