import React from "react";
import styled from "styled-components";

const EFBlurOverlay = ({ children, overlayContent }) => {
  return (
    <>
      <Overlay>{children}</Overlay>
      {overlayContent}
    </>
  );
};

export default EFBlurOverlay;

const Overlay = styled.div`
  border-radius: 10px;
  transition: 250ms;
  filter: blur(8px);
  pointer-events: none;
`;
