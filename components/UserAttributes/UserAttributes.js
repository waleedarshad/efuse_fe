import React from "react";
import Style from "./UserAttributes.module.scss";
import { getUserTraitsAndMotivationsMap } from "../../common/traitsAndMotivationsMap";

const UserAttributes = ({ userAttributes }) => {
  const traitsAndMotivationsMap = getUserTraitsAndMotivationsMap();
  const attributeLabels = userAttributes.map(item => traitsAndMotivationsMap.get(item));
  const attributes = attributeLabels.map((attribute, index) => {
    if (attribute) {
      return (
        <span key={index} className={Style.userAttributes}>
          {attribute}
        </span>
      );
    }
  });
  return <div className={Style.attributeContainer}>{attributes}</div>;
};

export default UserAttributes;
