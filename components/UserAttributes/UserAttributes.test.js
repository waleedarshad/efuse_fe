import React from "react";
import { mount } from "enzyme";
import UserAttributes from "./UserAttributes";
import USER_TRAITS from "../../common/userTraits";

describe("UserAttributes", () => {
  it("renders the user attributes correctly", () => {
    const userAttributes = [USER_TRAITS.TEAM_MEMBER_VALUE, USER_TRAITS.STREAMER_VALUE, USER_TRAITS.STUDENT_VALUE];
    const subject = mount(<UserAttributes userAttributes={userAttributes} />);
    expect(
      subject
        .find("span")
        .at(0)
        .text()
    ).toEqual(USER_TRAITS.TEAM_MEMBER_LABEL);
    expect(
      subject
        .find("span")
        .at(1)
        .text()
    ).toEqual(USER_TRAITS.STREAMER_LABEL);
    expect(
      subject
        .find("span")
        .at(2)
        .text()
    ).toEqual(USER_TRAITS.STUDENT_LABEL);
  });
});
