import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

import { getPathways } from "../../store/actions/onboardingActions";
import EFCardSelection from "../EFCardSelection/EFCardSelection";
import Style from "./EFPathwaysSelection.module.scss";

const EFPathwaysSelection = ({ onCardSelect, selectedPathwayID, size, valueKey }) => {
  const dispatch = useDispatch();
  const availablePathways = useSelector(state => state.onboarding.pathways);

  useEffect(() => {
    dispatch(getPathways());
  }, []);

  return (
    <div className={Style.cardsContainer}>
      <EFCardSelection
        cards={availablePathways}
        onChange={value => onCardSelect(value)}
        value={selectedPathwayID}
        size={size}
        valueKey={valueKey}
      />
    </div>
  );
};

EFPathwaysSelection.defaultProps = {
  size: "large",
  valueKey: "_id"
};

export default EFPathwaysSelection;
