import React from "react";
import TwitchVideoPlayerCustomChat from "./TwitchVideoPlayerCustomChat";

export default {
  title: "Twitch/TwitchVideoPlayerCustomChat",
  component: TwitchVideoPlayerCustomChat,
  argTypes: {
    title: {
      control: {
        type: "text"
      }
    },
    channel: {
      control: {
        type: "select",
        options: ["nickmercs", "shroud", "ninja", "pokimane"]
      }
    },
    darkTheme: {
      control: {
        type: "boolean"
      }
    },
    roundedCorners: {
      control: {
        type: "boolean"
      }
    },
    muted: {
      control: {
        type: "boolean"
      }
    },
    autoplay: {
      control: {
        type: "boolean"
      }
    }
  }
};

const Story = args => <TwitchVideoPlayerCustomChat {...args} />;

export const Dark = Story.bind({});
Dark.args = {
  title: "Twitch Video Player Title Here...",
  channel: "nickmercs",
  darkTheme: true,
  roundedCorners: true,
  muted: false,
  autoplay: false
};

export const Light = Story.bind({});
Light.args = {
  title: "Twitch Video Player Title Here...",
  channel: "nickmercs",
  darkTheme: false,
  roundedCorners: true,
  muted: false,
  autoplay: false
};
