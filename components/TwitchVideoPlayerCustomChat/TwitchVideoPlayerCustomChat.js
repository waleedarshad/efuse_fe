import React, { useState } from "react";
import { useSelector } from "react-redux";
import Style from "./TwitchVideoPlayerCustomChat.module.scss";
import Switch from "../Switch/Switch";
import TwitchPlayerIframe from "../TwitchPlayerIframe/TwitchPlayerIframe";
import TwitchChat from "../TwitchChat/TwitchChat";
import VideoPlayer from "../VideoPlayer/VideoPlayer";

const TwitchVideoPlayerCustomChat = ({ title, channel, roundedCorners, muted, autoplay }) => {
  const [displayChat, toggleChat] = useState(true);
  const currentTheme = useSelector(state => state.theme);

  return (
    <div className={`${Style.videoContainer} ${roundedCorners && Style.roundedCorners}`}>
      <div className={Style.header}>
        <p className={Style.headerText}>{title}</p>
        <div className={Style.toggleChat}>
          <p className={Style.toggleText}>Toggle Chat</p>
          <div className={Style.switch}>
            <Switch
              name="toggleChat"
              value={displayChat}
              checked={displayChat}
              onChange={() => toggleChat(!displayChat)}
            />
          </div>
        </div>
      </div>
      <div className="break" />
      <div className={Style.videoChatContainer}>
        {/* [PAVEL] THIS YOUTUBE FUNCTIONALITY IS TEMPORARY AND HACKY */}
        {channel?.includes("youtube.com") || channel?.includes("youtu.be") ? (
          <VideoPlayer url={channel}></VideoPlayer>
        ) : (
          <>
            <TwitchPlayerIframe twitchChannel={channel} muted={muted} autoplay={autoplay} />

            <div className={Style.breakChat} />
            {displayChat && (
              <div className={Style.chat}>
                <TwitchChat channel={channel} darkTheme={currentTheme?.currentThemeType === "dark"} />
              </div>
            )}
          </>
        )}
      </div>
    </div>
  );
};

TwitchVideoPlayerCustomChat.defaultProps = {
  muted: true,
  autoplay: false
};

export default TwitchVideoPlayerCustomChat;
