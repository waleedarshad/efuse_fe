import React from "react";
import SimpleBar from "simplebar-react";
import "simplebar-react/dist/simplebar.min.css";
import styled from "styled-components";
import Style from "./EFScrollbar.module.scss";

const CustomScrollBar = styled(SimpleBar)`
  position: absolute;
  left: 0;
  top: 0;
  bottom: 0;
  right: 0;
  height: 100%;
`;

const EFScrollbar = ({ children }) => {
  return (
    <div className={Style.scrollbarWrapper}>
      <CustomScrollBar>
        <div className={Style.contentWrapper}>{children}</div>
      </CustomScrollBar>
    </div>
  );
};

export default EFScrollbar;
