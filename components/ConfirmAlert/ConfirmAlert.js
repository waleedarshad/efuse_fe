import React from "react";

import PropTypes from "prop-types";
import EFAlert from "../EFAlert/EFAlert";

const ConfirmAlert = props => {
  const { title, message, onYes, onButtonClick } = props;
  const buildButton = React.Children.map(props.children, child =>
    React.cloneElement(child, {
      onClick: () => {
        onButtonClick && onButtonClick();
        EFAlert(title, message, "Yes", "No", onYes);
      }
    })
  );
  return <>{buildButton}</>;
};

ConfirmAlert.propTypes = {
  title: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired,
  onYes: PropTypes.func.isRequired,
  onClick: PropTypes.func,
  children: PropTypes.element.isRequired
};

export default ConfirmAlert;
