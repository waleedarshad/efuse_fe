import React from "react";
import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Style from "./EFPostBadge.module.scss";

const EFPostBadge = ({ size, colorTheme, text, icon }) => {
  return (
    <div className={`${Style.postBadgeContainer} ${Style[size]} ${Style[colorTheme]}`}>
      <div className={Style.clipPath}></div>
      <FontAwesomeIcon icon={icon} className={Style.icon} />
      <span className={Style.text}>{text}</span>
    </div>
  );
};

EFPostBadge.defaultProps = {
  size: "small",
  colorTheme: "grey"
};

EFPostBadge.propTypes = {
  size: PropTypes.oneOf(["small", "large"]),
  colorTheme: PropTypes.oneOf(["grey", "purple"])
};

export default EFPostBadge;
