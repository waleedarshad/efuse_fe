/* eslint-disable react/jsx-props-no-spreading */
import React from "react";
import { faBolt } from "@fortawesome/pro-solid-svg-icons";

import EFPostBadge from "./EFPostBadge";

export default {
  title: "Shared/EFPostBadge",
  component: EFPostBadge,
  argTypes: {
    size: {
      control: {
        type: "select",
        options: ["small", "large"]
      }
    },
    colorTheme: {
      control: {
        type: "select",
        options: ["grey", "purple"]
      }
    },
    text: {
      control: {
        type: "text"
      }
    },
    icon: {
      control: {
        type: "text"
      }
    }
  }
};

const Story = args => (
  <div style={{ width: "400px" }}>
    <EFPostBadge {...args} />
  </div>
);

export const Default = Story.bind({});
Default.args = {
  text: "241",
  icon: faBolt
};

export const Large = Story.bind({});
Large.args = {
  size: "large",
  text: "241",
  icon: faBolt
};

export const Purple = Story.bind({});
Purple.args = {
  colorTheme: "purple",
  text: "241",
  icon: faBolt
};
