import PropTypes from "prop-types";

import Style from "./LabelValue.module.scss";
import Hr from "../Hr/Hr";

const LabelValue = ({ label, value }) => (
  <>
    <strong className={Style.label}>{label}:</strong>
    <strong className={Style.value}>{value}</strong>
    <Hr />
  </>
);

LabelValue.propTypes = {
  label: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired
};

LabelValue.defaultProps = {
  label: "",
  value: ""
};

export default LabelValue;
