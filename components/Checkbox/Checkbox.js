import React from "react";
import { Form } from "react-bootstrap";
import PropTypes from "prop-types";

import Style from "./Checkbox.module.scss";

const Checkbox = props => {
  const { theme, meta, ...rest } = props;
  return (
    <Form.Check {...rest} className={`${Style.checkbox} ${Style[theme]}`}>
      <Form.Check.Input {...rest} />
      <Form.Check.Label {...rest} className={`${Style.inputLabel} ${Style[theme]}`}>
        {props.label}
      </Form.Check.Label>
    </Form.Check>
  );
};

Checkbox.propTypes = {
  theme: PropTypes.string
};

Checkbox.defaultProps = {
  theme: "external"
};

export default Checkbox;
