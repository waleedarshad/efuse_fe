import React, { Component } from "react";
import { connect } from "react-redux";

import isEmpty from "lodash/isEmpty";
import Styles from "./FollowsYou.module.scss";
import { checkIfFollowsYou } from "../../store/actions/followerActions";

class FollowsYou extends Component {
  componentDidMount() {
    const { profileId, currentUser } = this.props;
    !isEmpty(currentUser) && this.props.checkIfFollowsYou(profileId);
  }

  render() {
    const { followsYou } = this.props;
    const content = followsYou ? <h6>FOLLOWS YOU</h6> : "";
    return (
      <>
        <div className={Styles.followsContent}>{content}</div>
      </>
    );
  }
}

const mapStateToProps = state => ({
  currentUser: state.auth.currentUser,
  followsYou: state.followers.followsYou
});
export default connect(mapStateToProps, { checkIfFollowsYou })(FollowsYou);
