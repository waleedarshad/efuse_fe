const EFRenderConditionally = ({ condition, children }) => <>{condition ? children : <></>}</>;

export default EFRenderConditionally;
