/* eslint-disable react/display-name */
/* eslint-disable no-shadow */

import React, { useState } from "react";

import Style from "./DesktopOnboarding.module.scss";
import EFWatermark from "../Icons/EFWatermark/EFWatermark";
import EFRectangleButton from "../Buttons/EFRectangleButton/EFRectangleButton";
import CustomizeProfileLeft from "./Onboarding/CustomizeProfile/CustomizeProfileLeft";
import CustomizeProfileMainContent from "./Onboarding/CustomizeProfile/CustomizeProfileMainContent";
import CreatePostLeft from "./Onboarding/CreatePost/CreatePostLeft";
import CreatePostMainContent from "./Onboarding/CreatePost/CreatePostMainContent";
import ConnectPeopleLeft from "./Onboarding/ConnectPeople/ConnectPeopleLeft";
import ConnectPeopleMainContent from "./Onboarding/ConnectPeople/ConnectPeopleMainContent";

const DesktopOnboarding = () => {
  const [step, setStep] = useState(0);

  const onboardingSteps = [
    {
      leftSideComponent: <CustomizeProfileLeft />,
      mainContentComponent: () => <CustomizeProfileMainContent />
    },
    {
      leftSideComponent: <CreatePostLeft />,
      mainContentComponent: (step, setStep) => <CreatePostMainContent setStep={setStep} step={step} />
    },
    {
      leftSideComponent: <ConnectPeopleLeft />,
      mainContentComponent: () => <ConnectPeopleMainContent />
    }
  ];

  return (
    <div className={Style.onboardingLayoutContainer}>
      <div className={Style.leftSideMenyContainer}>
        <div className={Style.sideMenuTextContainer}>
          <div className={Style.watermark}>
            <EFWatermark type="dark" />
          </div>
          {onboardingSteps[step].leftSideComponent}
        </div>
        <div className={Style.bottomButtonRow}>
          {step !== 0 ? (
            <EFRectangleButton colorTheme="transparent" text="Previous" onClick={() => setStep(step - 1)} />
          ) : (
            <div />
          )}
          {
            <EFRectangleButton
              colorTheme={step === onboardingSteps.length - 1 ? "primary" : "secondary"}
              text={step !== onboardingSteps.length - 1 ? "Next" : "Done"}
              onClick={() => {
                if (step === onboardingSteps.length - 1) {
                  window.location = "/lounge/featured";
                } else {
                  setStep(step + 1);
                }
              }}
            />
          }
        </div>
      </div>
      <div className={Style.rightMainContent}>{onboardingSteps[step].mainContentComponent(step, setStep)}</div>
    </div>
  );
};

export default DesktopOnboarding;
