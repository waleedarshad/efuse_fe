import React, { useEffect } from "react";
import dynamic from "next/dynamic";
import FEATURE_FLAGS from "../../../../common/featureFlags";
import FeatureFlag from "../../../General/FeatureFlag/FeatureFlag";
import FeatureFlagVariant from "../../../General/FeatureFlag/FeatureFlagVariant/FeatureFlagVariant";
import NextStepsCardList from "./NextStepsCardList/NextStepsCardList";

const WhoToFollow = dynamic(import("./WhoToFollow/WhoToFollow"), {
  ssr: false
});

const CreatePostMainContent = () => {
  useEffect(() => {
    analytics.track("ONBOARDING_STEP", { step: "Who to Follow" });
  }, []);

  return (
    <FeatureFlag name={FEATURE_FLAGS.EXP_WEB_DESKTOP_ONBOARDING_STEP_3} experiment>
      <FeatureFlagVariant flagState>
        <NextStepsCardList />
      </FeatureFlagVariant>
      <FeatureFlagVariant>
        <WhoToFollow />
      </FeatureFlagVariant>
    </FeatureFlag>
  );
};

export default CreatePostMainContent;
