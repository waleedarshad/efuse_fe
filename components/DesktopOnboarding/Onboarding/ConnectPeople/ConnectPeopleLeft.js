import React from "react";
import Link from "next/link";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCouch, faGamepadAlt, faMobile, faNewspaper } from "@fortawesome/pro-regular-svg-icons";

import Style from "./ConnectPeople.module.scss";
import EFTwilioAppDownload from "../../../EFTwilioAppDownload/EFTwilioAppDownload";

const CreatePostLeft = () => {
  return (
    <>
      <h2>Next Steps</h2>
      <p>Here are a couple next steps to make the most of your time on eFuse.</p>
      <ul className={Style.conversationList}>
        <li className={Style.item}>
          <span className={Style.fontIcon}>
            <FontAwesomeIcon icon={faCouch} className={Style.icon} />
          </span>
          <div className={Style.textContainer}>
            <b>Join the Conversation</b>
            <p className={Style.links}>
              <Link href="/lounge/verified">Community Feed </Link> and <Link href="/organizations">Org Pages</Link>
            </p>
          </div>
        </li>
        <li className={Style.item}>
          <span className={Style.fontIcon}>
            <FontAwesomeIcon icon={faNewspaper} className={Style.icon} />
          </span>
          <div className={Style.textContainer}>
            <b>Read the News</b>
            <p className={Style.links}>
              <Link href="/lounge/featured">Featured</Link>, <Link href="/esports">Esports Business</Link>,{" "}
              <Link href="/recruiting">Recruiting</Link>
            </p>
          </div>
        </li>
        <li className={Style.item}>
          <span className={Style.fontIcon}>
            <FontAwesomeIcon icon={faGamepadAlt} className={Style.icon} />
          </span>
          <div className={Style.textContainer}>
            <b>Find Gaming Opportunities</b>
            <p className={Style.links}>
              <Link href="/opportunities/browse?subType=Casual&subType=Amateur&subType=Club&subType=Collegiate&subType=Semi-Pro&subType=Pro">
                Team Openings
              </Link>
              ,{" "}
              <Link href="/opportunities/browse?subType=Volunteer&subType=Internship&subType=Part-Time&subType=Full-Time">
                Jobs
              </Link>
              , <Link href="/erena">Tournaments</Link>
            </p>
          </div>
        </li>
        <li className={Style.item}>
          <span className={Style.fontIcon}>
            <FontAwesomeIcon icon={faMobile} className={Style.icon} />
          </span>
          <div className={Style.textContainer}>
            <div>
              <b>Download the App</b>
            </div>
            <p className={Style.links}>Search &quot;eFuse&quot; on the Apple App Store or Google Play</p>
          </div>
        </li>
      </ul>
      <EFTwilioAppDownload />
    </>
  );
};

export default CreatePostLeft;
