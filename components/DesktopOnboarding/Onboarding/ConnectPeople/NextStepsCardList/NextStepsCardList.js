import React from "react";
import { faCouch, faBuilding, faGamepadAlt, faUser } from "@fortawesome/pro-solid-svg-icons";
import { useSelector } from "react-redux";
import FeatureCard from "../../../../WelcomeSplashPage/FeatureCard/FeatureCard";
import Style from "./NextStepsCardList.module.scss";

const NextStepsCardList = () => {
  const currentUser = useSelector(state => state.auth.currentUser);
  const cardContent = [
    {
      title: "Lounge",
      description: "The home for your esports community. All gaming, all the time.",
      link: "/lounge/featured",
      buttonText: "Network with Gamers",
      icon: faCouch
    },
    {
      title: "Organizations",
      description: "Centralize and organize your esports team or organization.",
      link: "/organizations",
      buttonText: "Find an Organization",
      icon: faBuilding
    },
    {
      title: "Opportunities",
      description: "Esports jobs, team openings, scholarships, and events at your finger tips.",
      link: "/opportunities",
      buttonText: "Apply to Opportunities",
      icon: faGamepadAlt
    },
    {
      title: "Portfolio",
      description: "Chart your gaming journey. Connect game stats, social accounts, and create a gaming resume.",
      link: `/u/${currentUser?.username}`,
      buttonText: "Build your Portfolio",
      icon: faUser
    }
  ];
  return (
    <div className={Style.cardContainer}>
      {cardContent.map((card, index) => (
        <FeatureCard
          index={index}
          title={card.title}
          description={card.description}
          link={card.link}
          buttonText={card.buttonText}
          icon={card.icon}
          key={card.title}
        />
      ))}
    </div>
  );
};

export default NextStepsCardList;
