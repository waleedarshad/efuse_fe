import React from "react";

import { useSelector, useDispatch } from "react-redux";

import EFRectangleButton from "../../../../../Buttons/EFRectangleButton/EFRectangleButton";
import { unfollowThenRefresh, followThenRefresh } from "../../../../../../store/actions/followerActions";

const EFFollow = ({ id }) => {
  const currentUser = useSelector(state => state.auth.currentUser);
  const dispatch = useDispatch();

  const disable = useSelector(state => state.followers.disable);
  const followeeIds = useSelector(state => state.followers.followeeIds);

  const isFollowed = followeeIds.filter(followeeId => followeeId === id).length > 0;
  if (isFollowed) {
    return (
      <EFRectangleButton
        onClick={() => dispatch(unfollowThenRefresh(id, currentUser.id))}
        disabled={disable}
        text="Following"
        colorTheme={disable ? "white" : "transparent"}
      />
    );
  }

  return (
    <EFRectangleButton
      onClick={() => dispatch(followThenRefresh(id, currentUser.id))}
      disabled={disable}
      text="Follow"
      colorTheme={disable ? "white" : "transparent"}
    />
  );
};

export default EFFollow;
