import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import uniqueId from "lodash/uniqueId";
import Style from "./WhoToFollow.module.scss";
import { getFollowerRecommendations } from "../../../../../store/actions/recommendationsActions";
import EFFollow from "./FollowBtn/FollowBtn";
import { getFolloweeIds } from "../../../../../store/actions/followerActions";
import VerifiedIcon from "../../../../VerifiedIcon/VerifiedIcon";

const WhoToFollow = () => {
  const recommendations = useSelector(state => state.recommendations.followers);
  const currentUser = useSelector(state => state.auth.currentUser);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getFollowerRecommendations());
  }, []);

  useEffect(() => {
    dispatch(getFolloweeIds(currentUser.id));
  }, [JSON.stringify(currentUser)]);

  return (
    <div className={Style.whoToFollow}>
      {recommendations.map(user => {
        return (
          <div className={Style.profileCard} key={uniqueId()}>
            <div className={Style.profilePictureContainer}>
              <img width="130" src={user?.profilePicture?.url} alt={`${user.name} profile`} />
            </div>
            <div className={Style.nameContainer}>
              <div className={Style.name}>
                {user.name}
                {user.verified && <VerifiedIcon />}
              </div>
              <div className={Style.username}>@{user.username}</div>
            </div>
            <EFFollow id={user._id} />
          </div>
        );
      })}
    </div>
  );
};

export default WhoToFollow;
