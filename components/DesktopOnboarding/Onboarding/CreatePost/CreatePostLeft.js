import React from "react";

const CreatePostLeft = () => {
  return (
    <>
      <h2>Create a Post</h2>
      <p>eFuse is a community of gamers and esports competitors sharing content daily</p>
      <h2>How it Works</h2>
      <ol>
        <li>After you make a cool play or do something interesting, share it to eFuse</li>
        <li>Over time, you&lsquo;ll be discovered & build a community of like-minded gamers</li>
        <li>You&lsquo;ll grow YOUR audience and find new opportunities to grow professionally!</li>
      </ol>
      <h1>Let&lsquo;s get started - create your first post!</h1>
    </>
  );
};

export default CreatePostLeft;
