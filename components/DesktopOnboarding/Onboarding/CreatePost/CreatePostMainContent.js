import React, { useEffect } from "react";
import dynamic from "next/dynamic";

import Style from "../../DesktopOnboarding.module.scss";

const CreatePostContent = dynamic(import("../../../Modals/CreatePostModal/CreatePostContent"), {
  ssr: false
});

const CreatePostMainContent = ({ step, setStep }) => {
  useEffect(() => {
    analytics.track("ONBOARDING_STEP", { step: "Create Post" });
  }, []);

  return (
    <>
      <div className={Style.whiteContainer}>
        <div>
          <CreatePostContent
            onFormSubmitted={() => {
              setTimeout(() => {
                setStep(step + 1);
              }, 500);
            }}
          />
        </div>
      </div>
    </>
  );
};

export default CreatePostMainContent;
