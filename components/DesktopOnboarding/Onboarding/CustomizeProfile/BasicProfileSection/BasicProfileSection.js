import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { faCamera } from "@fortawesome/pro-regular-svg-icons";

import EFAvatar from "../../../../EFAvatar/EFAvatar";
import EFCircleIconButtonTooltip from "../../../../Buttons/EFCircleIconButtonTooltip/EFCircleIconButtonTooltip";
import { getImage } from "../../../../../helpers/GeneralHelper";
import Style from "./BasicProfileSection.module.scss";

import InputRow from "../../../../InputRow/InputRow";
import InputField from "../../../../InputField/InputField";

import InputLabel from "../../../../InputLabel/InputLabel";

import EditImageModal from "../../../../Portfolio/EditImageModal/EditImageModal";
import { updateCurrentUser } from "../../../../../store/actions/userActions";

const BasicProfileSection = () => {
  const dispatch = useDispatch();
  const user = useSelector(state => state.auth.currentUser);
  const [bio, setBio] = useState("");
  const [displayName, setDisplayName] = useState("");
  const [openModal, setOpenModal] = useState(false);

  useEffect(() => {
    setBio(user?.bio);
    setDisplayName(user?.name);
  }, [JSON.stringify(user)]);

  const closeModal = () => {
    setOpenModal(false);
  };

  const userSubmission = () => {
    const userData = new FormData();

    if (bio) {
      analytics.track("ADD_USER_BIO");
    }

    userData.append("bio", bio);
    userData.append("name", displayName);

    dispatch(updateCurrentUser(userData));
  };

  const validateBeforeSubmit = (name, value) => {
    if (value && value !== user[name]) userSubmission();
  };
  return (
    <div className={Style.basicProfileContainer}>
      <div className={Style.profilePictureContainer}>
        {user && (
          <div style={{ position: "relative" }}>
            <EFAvatar
              profilePicture={getImage(user.profilePicture)}
              size="large"
              online={user?.online && user?.showOnline}
              displayOnlineButton={false}
            />
            <div className={Style.imageUploadButton}>
              <EFCircleIconButtonTooltip
                icon={faCamera}
                tooltipContent="Edit Profile Picture"
                tooltipPlacement="top"
                onClick={() => setOpenModal(true)}
              />
            </div>
            <EditImageModal
              title="Update Profile Picture"
              imageUrl={user.profilePicture?.url}
              isOpen={openModal}
              closeModal={closeModal}
              imageProp="profilePicture"
              aspectRatioWidth={160}
              aspectRatioHeight={160}
            />
          </div>
        )}
      </div>
      <div className={Style.textInputContainer}>
        <InputRow>
          <InputLabel theme="internal">Display Name</InputLabel>
          <InputField
            name="name"
            placeholder="Enter a display name"
            value={displayName}
            onChange={e => setDisplayName(e.target.value)}
            onBlur={e => validateBeforeSubmit("name", e.target.value)}
            theme="internal"
          />
        </InputRow>
        <InputRow>
          <InputLabel theme="internal">Your Bio</InputLabel>
          <InputField
            as="textarea"
            name="bio"
            placeholder="Describe a little bit about yourself..."
            value={bio}
            onChange={e => setBio(e.target.value)}
            rows={2}
            maxLength={250}
            theme="internal"
            onBlur={e => validateBeforeSubmit("bio", e.target.value)}
          />
        </InputRow>
      </div>
    </div>
  );
};

export default BasicProfileSection;
