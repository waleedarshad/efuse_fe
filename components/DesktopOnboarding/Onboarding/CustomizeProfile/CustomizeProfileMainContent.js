import React, { useEffect } from "react";

import Style from "../../DesktopOnboarding.module.scss";
import BasicProfileSection from "./BasicProfileSection/BasicProfileSection";
import GamesSection from "./GamesSection/GamesSection";
import ExternalAccountSection from "./ExternalAccountSection/ExternalAccountSection";

const CustomizeProfileMainContent = () => {
  useEffect(() => {
    analytics.track("ONBOARDING_STEP", { step: "Customize Profile" });
  }, []);

  return (
    <>
      <div className={Style.whiteContainer} style={{ position: "relative" }}>
        <BasicProfileSection />
        <GamesSection />
        <ExternalAccountSection />
      </div>
    </>
  );
};

export default CustomizeProfileMainContent;
