import React from "react";
import { useMutation } from "@apollo/client";
import EFGameSelect from "../../../../EFGameSelect/EFGameSelect";
import { ADD_GAMES_TO_USER } from "../../../../../graphql/UserGamesQuery";

const GamesSection = () => {
  const [submitGamesGQL] = useMutation(ADD_GAMES_TO_USER);

  const userSubmission = games => {
    const selectedGameIds = games.map(game => game._id);
    const selectedGameTitles = games.map(game => game.title);
    analytics.track("USER_GAMES_SELECTED", { location: "onboarding", selectedGameIds, selectedGameTitles });
    submitGamesGQL({ variables: { addGamesToUserGameIds: selectedGameIds } });
  };

  return (
    <>
      <h2>Select the games you play</h2>
      <EFGameSelect onSelect={games => userSubmission(games)} />
    </>
  );
};

export default GamesSection;
