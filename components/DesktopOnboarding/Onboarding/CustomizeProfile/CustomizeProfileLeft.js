import React from "react";
import { useSelector } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowRight } from "@fortawesome/pro-regular-svg-icons";
import Style from "../../DesktopOnboarding.module.scss";

const CreatePostLeft = () => {
  const user = useSelector(state => state.auth.currentUser);
  return (
    <>
      <h2>Welcome to eFuse, {user?.name || ""}!</h2>
      <p className={Style.textFont}>
        Make it yours - stand out! A well made eFuse profile helps you stand out, build an audience, & get more value
      </p>
      <h2>Tips</h2>
      <ol>
        <li>Set a great profile picture and background </li>
        <li>Add your Interests to your Page</li>
        <li>Link your other platforms so your community can follow everywhere</li>
      </ol>
      <h1>
        Setup your profile now on the right <FontAwesomeIcon icon={faArrowRight} />
      </h1>
    </>
  );
};

export default CreatePostLeft;
