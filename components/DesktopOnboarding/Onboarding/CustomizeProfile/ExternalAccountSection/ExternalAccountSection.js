import React from "react";
import ExternalAccounts from "../../../../Settings/ExternalAccounts/index";

const ExternalAccountSection = () => {
  return (
    <div>
      <h2>Link your other platforms</h2>

      <ExternalAccounts
        onboardingFlow
        onlyShowLinks
        displaySteam={false}
        displayBattlenet={false}
        displayLinkedin={false}
        displayTwitter
        displayDiscord
        displayFacebook
        displayTwitch
        displayGoogle
        displayAimLab={false}
        displaySnapchat={false}
        displayValorant={false}
      />
    </div>
  );
};

export default ExternalAccountSection;
