import React, { useContext } from "react";
import EFWizard, { EFWizardContext } from "./EFWizard";

export default {
  title: "Shared/EFWizard",
  component: EFWizard
};

export const Default = () => (
  <div style={{ display: "flex", flexDirection: "column", alignItems: "center" }}>
    <h1>This is a wizard</h1>
    <p>You can access wizard actions through the EFWizard context</p>
    <div
      style={{
        border: "1px solid black",
        borderRadius: "15px",
        padding: "24px",
        width: "500px",

        alignItems: "center"
      }}
    >
      <EFWizard>
        <Step1 />
        <Step2 />
        <Step3 />
      </EFWizard>
    </div>
  </div>
);

const Step1 = () => {
  const { currentStep, goNextStep, goPrevStep, totalSteps } = useContext(EFWizardContext);
  return (
    <>
      <h2>
        This is step {currentStep} out of {totalSteps}
      </h2>
      <p>
        Vexillologist woke try-hard, pug pickled lumbersexual pabst PBR&B 3 wolf moon green juice. Poutine hexagon
        irony, unicorn umami ramps 90s marfa adaptogen shoreditch cliche. Irony tattooed hell of, waistcoat vice direct
        trade roof party kale chips affogato street art. Chia coloring book post-ironic, adaptogen polaroid brunch fixie
        tofu helvetica sartorial farm-to-table neutra scenester ugh
      </p>
      <button type="button" onClick={() => goPrevStep()}>
        Prev
      </button>
      <button type="button" onClick={() => goNextStep()}>
        Next
      </button>
    </>
  );
};

const Step2 = () => {
  const { currentStep, goNextStep, goPrevStep } = useContext(EFWizardContext);
  return (
    <>
      <h2>This is step {currentStep}</h2>
      <p>
        Tousled banjo franzen chia poutine cred plaid blue bottle scenester slow-carb locavore flannel. Vinyl scenester
        master cleanse disrupt glossier gluten-free gastropub ramps poke chia.
      </p>
      <button type="button" onClick={() => goPrevStep()}>
        Prev
      </button>
      <button type="button" onClick={() => goNextStep()}>
        Next
      </button>
    </>
  );
};

const Step3 = () => {
  const { currentStep, goToStep, goPrevStep } = useContext(EFWizardContext);
  return (
    <>
      <h2>This is step {currentStep}</h2>
      <p>
        Gastropub hoodie raclette direct trade. Chicharrones everyday carry blog crucifix, literally beard brunch fixie
        lo-fi chambray normcore. Kitsch 8-bit poutine, scenester schlitz tumblr bitters vaporware pork belly cronut
        kombucha mlkshk live-edge. Humblebrag banjo marfa farm-to-table small batch, ethical polaroid locavore
        fingerstache.
      </p>

      <button type="button" onClick={() => goPrevStep()}>
        Prev
      </button>
      <button type="button" onClick={() => goToStep(1)}>
        Go to Step 1
      </button>
    </>
  );
};
