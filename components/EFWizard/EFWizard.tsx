import React, { createContext, useState } from "react";

interface EFWizardProps {
  children: React.ReactNode | React.ReactNode[];
}

interface IWizardContext {
  currentStep: number;
  totalSteps: number;
  goNextStep: () => void;
  goPrevStep: () => void;
  goToStep: (step: number) => void;
  isFirstStep: boolean;
  isLastStep: boolean;
  progressPercent: number;
}

const defaultContext = {
  currentStep: 0,
  totalSteps: 0,
  goNextStep: () => {},
  goPrevStep: () => {},
  goToStep: () => {},
  isFirstStep: true,
  isLastStep: true,
  progressPercent: 0
};

export const EFWizardContext = createContext<IWizardContext>(defaultContext);

const EFWizard = ({ children }: EFWizardProps) => {
  const [currentStepIndex, setCurrentStep] = useState(0);
  const childSteps = Array.isArray(children) ? children : [children];
  const totalSteps = childSteps.length;

  const isLastStep = currentStepIndex === totalSteps - 1;
  const isFirstStep = currentStepIndex === 0;
  const progressPercent = ((currentStepIndex + 1) / totalSteps) * 100;

  const goNextStep = () => {
    if (currentStepIndex !== totalSteps - 1) {
      setCurrentStep(currentStepIndex + 1);
    }
  };

  const goPrevStep = () => {
    if (currentStepIndex > 0) {
      setCurrentStep(currentStepIndex - 1);
    }
  };

  const goToStep = stepNumber => {
    const stepIndex = stepNumber - 1;
    setCurrentStep(stepIndex);
  };

  const contextProps: IWizardContext = {
    currentStep: currentStepIndex + 1,
    totalSteps,
    goNextStep,
    goPrevStep,
    goToStep,
    isFirstStep,
    isLastStep,
    progressPercent
  };

  return <EFWizardContext.Provider value={contextProps}>{childSteps[currentStepIndex]}</EFWizardContext.Provider>;
};

export default EFWizard;
