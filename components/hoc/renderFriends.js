import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import InfiniteScroll from "react-infinite-scroller";
import { Row } from "react-bootstrap";
import capitalize from "lodash/capitalize";
import isEmpty from "lodash/isEmpty";
import Router from "next/router";
import pluralize from "pluralize";
import EmptyComponent from "../EmptyComponent/EmptyComponent";
import { getFollowers, getFollowees, follow, unfollow } from "../../store/actions/followerActions";
import Friend from "../Friend/Friend";
import AnimatedLogo from "../AnimatedLogo";
import { isUserLoggedIn } from "../../helpers/AuthHelper";

const renderFriends = key => {
  const FriendList = ({ id }) => {
    const [fetchRecords, setFetchRecords] = useState(true);
    const [currentUserLoaded, setCurrentUserLoaded] = useState(false);
    const singularKey = pluralize.singular(key);
    const friends = useSelector(state => state.followers[key]);
    const pagination = useSelector(state => state.followers.pagination);
    const currentUser = useSelector(state => state.auth.currentUser);
    const hasMore = pagination && pagination.hasNextPage;
    const associatedFriendKey = `associated${capitalize(singularKey)}`;
    const dispatch = useDispatch();

    useEffect(() => {
      const isLoggedIn = isUserLoggedIn();
      if (!isLoggedIn) {
        Router.push("/login");
      }
      loadData();
    }, []);

    useEffect(() => {
      loadData();
      if (currentUser && !currentUserLoaded) {
        if (currentUser.id !== id) {
          analytics.page(`Portfolio ${key.charAt(0).toUpperCase() + key.slice(1)} External`);
          analytics.track(`PORTFOLIO_${key.toUpperCase()}_VIEW_EXTERNAL`, {
            userId: id
          });
        } else {
          analytics.page(`Portfolio ${key.charAt(0).toUpperCase() + key.slice(1)}`);
          analytics.track(`PORTFOLIO_${key.toUpperCase()}_VIEW`, {
            userId: id
          });
        }

        setCurrentUserLoaded(prevState => ({ ...prevState, currentUserLoaded: true }));
      }
    }, [id, currentUser]);

    const loadData = () => {
      if (id && fetchRecords) {
        if (key === "followers") dispatch(getFollowers(id));
        else dispatch(getFollowees(id));
        setFetchRecords(false);
      }
    };

    const loadMore = () => {
      const nextPage = pagination ? pagination.page + 1 : 1;
      if (key === "followers") dispatch(getFollowers(id, nextPage, {}, false));
      else dispatch(getFollowees(id, nextPage, {}, false));
    };

    const onFollow = userId => {
      dispatch(follow(userId, false, key));
    };

    const onUnFollow = userId => {
      dispatch(unfollow(userId, false, currentUser.id, key));
    };

    const content = friends.map((friend, index) => {
      const user = friend && friend[associatedFriendKey];
      return (
        <Friend
          key={index}
          user={friend[associatedFriendKey]}
          renderAs={key}
          hasKey={!isEmpty(friend[associatedFriendKey][`is${capitalize(singularKey)}`])}
          notAFriend={friend[associatedFriendKey][`is${capitalize(singularKey)}`] === undefined}
          currentUser={currentUser}
          followOveride={() => onFollow(user?._id)}
          unfollowOveride={() => onUnFollow(user?._id)}
        />
      );
    });
    return (
      <InfiniteScroll
        pageStart={1}
        loadMore={loadMore}
        hasMore={hasMore}
        loader={<AnimatedLogo key={0} theme="inline" />}
      >
        {friends.length === 0 ? <EmptyComponent text={`There are currently no ${key}`} /> : <Row>{content}</Row>}
      </InfiniteScroll>
    );
  };

  return FriendList;
};

export default renderFriends;
