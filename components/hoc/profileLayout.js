import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useRouter } from "next/router";

import fetch from "isomorphic-unfetch";
import getConfig from "next/config";
import isEmpty from "lodash/isEmpty";
import { updateUserSimple } from "../../store/actions/userActions";
import { countFriends, checkIfFollowsYou, checkIfFollower } from "../../store/actions/followerActions";
import EFFollow from "../EFFollow/EFFollow";
import { isUserLoggedIn } from "../../helpers/AuthHelper";
import { getImage } from "../../helpers/GeneralHelper";
import Style from "./profileLayout.module.scss";
import PortfolioHeaderCard from "../Portfolio/PortfolioHeaderCard/PortfolioHeaderCard";
import PortfolioLayout from "../Layouts/PortfolioLayout/PortfolioLayout";
import Error from "../../pages/_error";
import EFSubHeader from "../Layouts/Internal/EFSubHeader/EFSubHeader";
import { getPortfolioNavigationList } from "../../navigation/portfolio";
import MockExternalUser from "../MockExternalUser/MockExternalUser";

const { publicRuntimeConfig } = getConfig();
const { defaultUrl } = publicRuntimeConfig;

const profileLayout = (WrappedComponent, paramId = null) => {
  const Layout = props => {
    const router = useRouter();
    const dispatch = useDispatch();

    const user = useSelector(state => state.user.currentUser);
    const currentUser = useSelector(state => state.auth.currentUser);
    const mockExternalUser = useSelector(state => state.portfolio.mockExternalUser);
    const webview = useSelector(state => state.webview.status);
    const responseCode = useSelector(state => state.errors.responseCode);
    const badgesFlag = useSelector(state => state.features.badges);
    const isFollowed = useSelector(state => state.followers.isFollowed);

    const { pageProps } = props;
    const id = pageProps?.user?._id;
    const { u } = router.query;
    const userLoggedIn = isUserLoggedIn();

    useEffect(() => {
      if (pageProps?.user && user && pageProps.user._id !== user._id) {
        dispatch(updateUserSimple(pageProps.user));
      }

      const userId = paramId || pageProps.user?._id;
      dispatch(countFriends(userId, !userLoggedIn));
      if (userLoggedIn) {
        // check if user follow current user
        dispatch(checkIfFollowsYou(userId));
        // check if current user is a follower
        dispatch(checkIfFollower(userId));
      }
    }, [pageProps.user?._id]);

    let isOwner = !isEmpty(currentUser) && currentUser.id === id;
    if (mockExternalUser) {
      isOwner = false;
    }
    const sscode = pageProps && pageProps.SsrStatusCode ? pageProps.SsrStatusCode : responseCode;
    if (sscode === 422 || sscode === 400) return <Error statusCode={sscode} />;

    if (!router.query.id || (u && user?.username === u)) {
      router.query.id = user?._id;
    }

    const actionButtons = [
      <div className={Style.externalViewButton} key={0}>
        <MockExternalUser currentUser={currentUser} id={id} newPortfolio type="profile" />
      </div>
    ];

    return (
      <PortfolioLayout webview={webview} headerImage={getImage(user.headerImage, "organization")} isOwner={isOwner}>
        <PortfolioHeaderCard
          user={user}
          currentUser={currentUser}
          isOwner={isOwner}
          followButton={<EFFollow id={id} currentUser={user} btnStyle={Style.followBtn} isFollowed={isFollowed} />}
        />
        <EFSubHeader
          positionUnderNav={false}
          navigationList={getPortfolioNavigationList(user, router, badgesFlag, userLoggedIn)}
          includeButtonsInScroll
          actionButtons={actionButtons}
        />
        <WrappedComponent
          user={user}
          currentUser={currentUser}
          mockExternalUser={mockExternalUser}
          webview={webview}
          responseCode={responseCode}
          badgesFlag={badgesFlag}
          isFollowed={isFollowed}
          pageProps={pageProps}
          id={id}
          router={router}
        />
      </PortfolioLayout>
    );
  };

  Layout.getInitialProps = async ctx => {
    const id = paramId || ctx.query.id;
    const { u } = ctx.query;

    const config = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json"
      }
    };
    const res = await fetch(`${defaultUrl}/public/users/${id || u}/portfolio`, config);
    const data = await res.json();
    const { user } = data;
    const SsrStatusCode = res.status;

    return {
      namespacesRequired: ["common"],
      pageProps: { user, SsrStatusCode }
    };
  };

  return Layout;
};
export default profileLayout;
