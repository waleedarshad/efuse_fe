import React from "react";
import { Tabs, Tab } from "react-bootstrap";
import { connect } from "react-redux";
import toLower from "lodash/toLower";

import Style from "./portfolioTabsLayout.module.scss";
import { isEmptyObject } from "../../../helpers/GeneralHelper";
import { getOwnerAndCaptain } from "../../../helpers/OrganizationHelper";

const portfolioTabsLayout = section => {
  const Layout = props => {
    const { user, currentUser, mockExternalUser, mockExternalOrganization, organization } = props;
    let isVisitor = false;
    if (!organization) {
      isVisitor = currentUser && user._id !== currentUser._id;
    } else {
      const { owner } = getOwnerAndCaptain(currentUser, organization);
      isVisitor = !owner;
    }
    if (mockExternalUser || mockExternalOrganization) {
      isVisitor = true;
    }
    let tabContent = "";
    if (!isEmptyObject(user) || !isEmptyObject(organization)) {
      if (section.tabs) {
        const filteredTabs = section.tabs.filter(tab => props[tab.tabFeature]);
        if (section.id === "stream_section" && isVisitor) {
          const streamTabs = filteredTabs.filter(f => user[f.auth] && f.auth !== user[f.auth]);
          tabContent = streamTabs.map((tab, index) => {
            const WrappedComponent = tab.component;
            return (
              <Tab
                className={Style.tabBody}
                key={index}
                eventKey={toLower(tab.title)}
                title={tab.title}
                mountOnEnter
                unmountOnExit
              >
                <WrappedComponent />
              </Tab>
            );
          });
        } else if (
          (section.id === "experience-section" ||
            section.id === "accolades-section" ||
            section.id === "skills-section") &&
          isVisitor
        ) {
          const finalTabs = filteredTabs.filter(f => user[f.auth]?.length > 0);
          tabContent = finalTabs.map((tab, index) => {
            const WrappedComponent = tab.component;
            return (
              <Tab
                className={Style.tabBody}
                key={index}
                eventKey={toLower(tab.title)}
                title={tab.title}
                mountOnEnter
                unmountOnExit
              >
                <WrappedComponent />
              </Tab>
            );
          });
        } else if (section.id === "organization-accolades-section" && isVisitor) {
          const finalTabs = filteredTabs.filter(f => organization[f.auth]?.length > 0);
          tabContent = finalTabs.map((tab, index) => {
            const WrappedComponent = tab.component;
            return (
              <Tab
                className={Style.tabBody}
                key={index}
                eventKey={toLower(tab.title)}
                title={tab.title}
                mountOnEnter
                unmountOnExit
              >
                <WrappedComponent />
              </Tab>
            );
          });
        } else if (section.id === "games-section" && isVisitor) {
          const experienceTabs = filteredTabs.filter(f => {
            if (f.auth === "fortnite" || f.auth === "callOfDuty" || f.auth === "pubg") {
              return user[f.auth].stats?.length > 0;
            }
            if (f.auth === "leagueOfLegends") {
              try {
                return user[f.auth].stats;
              } catch (error) {
                return false;
              }
            } else {
              return user[f.auth] !== undefined;
            }
          });
          tabContent = experienceTabs.map((tab, index) => {
            const WrappedComponent = tab.component;
            return (
              <Tab
                className={Style.tabBody}
                key={index}
                eventKey={toLower(tab.title)}
                title={tab.title}
                mountOnEnter
                unmountOnExit
              >
                <WrappedComponent />
              </Tab>
            );
          });
        } else {
          tabContent = filteredTabs.map((tab, index) => {
            const WrappedComponent = tab.component;
            return (
              <Tab
                className={Style.tabBody}
                key={index}
                eventKey={toLower(tab.title)}
                title={tab.title}
                mountOnEnter
                unmountOnExit
                disabled={tab.disabled}
              >
                <WrappedComponent />
              </Tab>
            );
          });
        }
      }
    }
    return (
      <>
        {props.enableFeature && (
          <div className={Style.container}>
            <Tabs className={`tab-header ${Style.tabHeader}`} id={section.id}>
              {tabContent}
            </Tabs>
          </div>
        )}
      </>
    );
  };

  const mapStateToProps = state => ({
    enableFeature: state.features[section.feature],
    user: state.user.currentUser,
    currentUser: state.auth.currentUser,
    mockExternalUser: state.portfolio.mockExternalUser,
    ...generateStatesForMapping(state)
  });

  const generateStatesForMapping = state => {
    let mappedFeatures = {};
    section.tabs.forEach(tabObject => {
      mappedFeatures = {
        ...mappedFeatures,
        [tabObject.tabFeature]: state.features[tabObject.tabFeature]
      };
    });
    return mappedFeatures;
  };

  return connect(mapStateToProps)(Layout);
};

export default portfolioTabsLayout;
