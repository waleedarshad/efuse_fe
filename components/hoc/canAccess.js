import Router from "next/router";
import React, { Component } from "react";
import { connect } from "react-redux";
import { setFlash } from "../../helpers/FlashHelper";

const canAccess = (WrappedComponent, accessType, bodyClass = "") => {
  class Accessible extends Component {
    state = {
      isAuthenticated: null
    };

    static getDerivedStateFromProps(props, state) {
      return props.isAuthenticated !== state.isAuthenticated ? { isAuthenticated: props.isAuthenticated } : null;
    }

    componentDidMount() {
      const hasModalOpenClasss = document.body.classList.value.includes("modal-open");
      document.body.removeAttribute("class");
      if (bodyClass) {
        document.body.classList.add(bodyClass);
      }
      if (hasModalOpenClasss) {
        document.body.classList.add("modal-open");
      }
    }

    componentDidUpdate(prevProps) {
      if (this.state.isAuthenticated !== prevProps.isAuthenticated) {
        this[accessType]();
      }
    }

    authenticated = () => {
      if (!this.state.isAuthenticated) {
        Router.push("/");
      }
    };

    unauthenticated = () => {
      if (this.state.isAuthenticated) {
        Router.push("/home");
      }
    };

    admin = () => {
      if (this.state.isAuthenticated) {
        const { currentUser } = this.props;
        if (currentUser && !currentUser.roles.includes("admin")) {
          setFlash("admin");
          Router.push("/home");
        }
      } else {
        this.authenticated();
      }
    };

    render() {
      return <WrappedComponent {...this.props} />;
    }
  }

  return connect(mapStateToProps)(Accessible);
};

const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated,
  currentUser: state.auth.currentUser
});

export default canAccess;
