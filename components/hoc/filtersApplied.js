import React from "react";
import { connect, useSelector } from "react-redux";
import { Card, Row, Col } from "react-bootstrap";
import * as opportunityFilterActions from "../../store/actions/opportunityFilterActions";
import * as organizationFilterActions from "../../store/actions/organizationFilterActions";
import * as applicantFilterActions from "../../store/actions/applicantFilterActions";
import * as recruitingFilterActions from "../../store/actions/recruitingFilterActions";
import DisplayFilters from "../DisplayFilters/DisplayFilters";
import SearchBar from "../SearchBar/SearchBar";

const filtersApplied = key => {
  let keyParam = key;
  const Style = {
    searchOpportunitiesCard: {
      paddingBottom: "0px"
    },
    cardHeader: {
      borderBottom: "0px"
    }
  };
  const actions = {
    opportunities: {
      removeFilter: opportunityFilterActions.removeFilter,
      clearFilters: opportunityFilterActions.clearFilters
    },
    opportunitiesSearch: {
      searchOpportunity: opportunityFilterActions.searchOpportunity,
      searchMyOpportunity: opportunityFilterActions.searchMyOpportunity,
      searchAppliedOpportunity: opportunityFilterActions.searchAppliedOpportunity
    },
    organizations: {
      removeFilter: organizationFilterActions.removeFilter,
      clearFilters: organizationFilterActions.clearFilters
    },
    organizationsSearch: {
      searchOrganization: organizationFilterActions.searchOrganization
    },
    applicants: {
      removeFilter: applicantFilterActions.removeFilter,
      clearFilters: applicantFilterActions.clearFilters
    },
    applicantsSearch: {
      searchApplicant: applicantFilterActions.searchApplicant
    },
    recruiting: {
      removeFilter: recruitingFilterActions.removeFilter,
      clearFilters:
        keyParam === "starredRecruiting"
          ? recruitingFilterActions.clearStarredFilters
          : recruitingFilterActions.clearFilters
    },
    recruitingSearch: {
      searchRecruiting:
        keyParam === "starredRecruiting"
          ? recruitingFilterActions.searchStarredRecruits
          : recruitingFilterActions.searchRecruits
    }
  };
  if (keyParam === "starredRecruiting") keyParam = "recruiting";
  const searchMethodKey = `${keyParam}Search`;
  const actionsToBeDispatched = {
    ...actions[keyParam],
    ...actions[searchMethodKey]
  };

  const Filters = ({ id, ...props }) => {
    const appliedFilters = useSelector(state => state[keyParam].filtersApplied);
    const view = useSelector(state => state.opportunities.view);
    const isFiltersApplied = appliedFilters.length > 0;
    let searchCallback = props[Object.keys(actions[searchMethodKey])[0]];

    if (view === "owned") {
      searchCallback = props[Object.keys(actions[searchMethodKey])[1]];
    } else if (view === "applied") {
      searchCallback = props[Object.keys(actions[searchMethodKey])[2]];
    }

    return (
      <Card className="customCard mb-4" style={!isFiltersApplied ? Style.searchOpportunitiesCard : {}}>
        <Card.Header style={!isFiltersApplied ? Style.cardHeader : {}}>
          <Row>
            <Col>
              <SearchBar filtersApplied={appliedFilters} searchCallback={searchCallback} id={id} />
            </Col>
          </Row>
        </Card.Header>
        {isFiltersApplied && (
          <DisplayFilters
            filtersApplied={appliedFilters}
            removeFilter={props.removeFilter}
            clearFilters={props.clearFilters}
            searchCallback={searchCallback}
            id={id}
          />
        )}
      </Card>
    );
  };

  return connect(null, actionsToBeDispatched)(Filters);
};

export default filtersApplied;
