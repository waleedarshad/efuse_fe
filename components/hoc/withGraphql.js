import { withApollo } from "next-apollo";
import { ApolloClient, InMemoryCache, HttpLink, from, Observable, ApolloLink } from "@apollo/client";
import { onError } from "@apollo/client/link/error";
import getConfig from "next/config";
import fetch from "node-fetch";

import { getToken, attemptToRefreshToken } from "../../helpers/Auth0Helper";

const { publicRuntimeConfig } = getConfig();
const { graphqlURL, defaultUrl } = publicRuntimeConfig;

const GRAPHQL_SERVER_URL = graphqlURL || `${defaultUrl}/graphql`;

let refreshedToken;

const getLoginToken = context => refreshedToken || getToken(context);

const authMiddleware = context => {
  const link = new ApolloLink((operation, forward) => {
    // add the authorization to the headers
    operation.setContext(({ headers = {} }) => ({
      headers: { ...headers, authorization: getLoginToken(context) }
    }));

    return forward(operation);
  });

  return link;
};

// Handled refresh token stuff
const attemptToRefreshLoginToken = (context, operation, forward) => {
  return new Observable(async observer => {
    try {
      const session = await attemptToRefreshToken(context);

      // Set new token to local variable so that it can be used for existing or further request
      refreshedToken = session.token;

      const oldHeaders = operation.getContext().headers;
      operation.setContext({ headers: { ...oldHeaders, authorization: session.token } });

      const subscriber = {
        next: observer.next.bind(observer),
        error: observer.error.bind(observer),
        complete: observer.complete.bind(observer)
      };

      // retry the request, returning the new observable
      return forward(operation).subscribe(subscriber);
    } catch (error) {
      console.error("[GRAPHQL Error]: Unable to refresh token", error);
      return observer.error(error);
    }
  });
};

// Setting up error link
const errorLink = context => {
  const errorHandler = onError(({ graphQLErrors, networkError, operation, forward }) => {
    if (graphQLErrors) {
      // eslint-disable-next-line no-restricted-syntax
      for (const error of graphQLErrors) {
        switch (error.extensions.code) {
          case "invalid-jwt":
            //  This error is thrown by hasura
            attemptToRefreshLoginToken(context, operation, forward);
            break;
          case "UNAUTHENTICATED":
            //  This error is thrown by our BE Apollo Server
            attemptToRefreshLoginToken(context, operation, forward);
            break;
          default:
            console.log("[GRAPHQL Error] Reached unhandled DEFAULT case", error);
            break;
        }
      }
    }

    if (networkError) {
      /**
       * We can make use of @apollo/client/link/retry to retry requests which are failed due
       * to network error
       */
      console.error("[GRAPHQL Network error]", networkError);
    }
  });

  return errorHandler;
};

// Setting up graphql link
const graphqlLink = new HttpLink({
  uri: GRAPHQL_SERVER_URL,
  fetch
});

// Setting up Apollo Client
const apolloClient = context => {
  const client = new ApolloClient({
    link: from([authMiddleware(context), errorLink(context), graphqlLink]),
    cache: new InMemoryCache()
  });

  return client;
};

export default withApollo(apolloClient);
