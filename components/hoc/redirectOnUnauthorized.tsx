import React, { useEffect } from "react";
import Router from "next/router";

const redirectOnUnauthorized = WrappedComponent => {
  const RedirectOnUnauthorizedWrapper = ({ pageProps }) => {
    const { authorized, redirect, ...other } = pageProps;
    useEffect(() => {
      if (!authorized) {
        Router.push(redirect);
      }
    });
    return authorized ? <WrappedComponent pageProps={other} /> : <></>;
  };

  return RedirectOnUnauthorizedWrapper;
};

export default redirectOnUnauthorized;
