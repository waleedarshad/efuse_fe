import { Elements } from "@stripe/react-stripe-js";
import { loadStripe } from "@stripe/stripe-js";
import getConfig from "next/config";

const { publicRuntimeConfig } = getConfig();

const stripePromise = loadStripe(publicRuntimeConfig.stripePublishableKey);

const withStripe = WrappedComponent => {
  const HOC = props => {
    return (
      <Elements stripe={stripePromise}>
        <WrappedComponent {...props} />
      </Elements>
    );
  };

  return HOC;
};

export default withStripe;
