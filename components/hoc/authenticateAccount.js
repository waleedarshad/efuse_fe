import axios from "axios";
import cloneDeep from "lodash/cloneDeep";
import uniqueId from "lodash/uniqueId";
import getConfig from "next/config";
import React from "react";
import { connect } from "react-redux";
import { getCurrentUser } from "../../store/actions/common/userAuthActions";
import { unlinkExternalAccount, updateUserSimple } from "../../store/actions/userActions";
import { unlinkConfirmation } from "../AuthenticateAccounts/utils";

const { publicRuntimeConfig } = getConfig();
const { defaultUrl, discordURL, xboxLiveAuthUrl } = publicRuntimeConfig;

const authenticateAccount = WrappedComponent => {
  const ExternalAccountWrapper = props => {
    const unlinkAccount = service => {
      const { user } = props;
      const _user = cloneDeep(user);
      _user[`${service}Verified`] = false;
      props.updateUserSimple(_user);
      props.unlinkExternalAccount({ service });
    };

    const toggleAccount = (service, isVerified) => {
      if (!isVerified) {
        startOAuth(service);
      } else {
        unlinkConfirmation(service, () => unlinkAccount(service));
      }
    };

    const startOAuth = service => {
      var id = `${service}-${uniqueId}`;
      // open a blank popup and set URL later - this is needed to prevent browser from blocking popup
      var popupOauthWindow = window.open("", id, "width=600,height=400,left=200,top=200");
      // show a simple message indicating that it's loading final OAuth URL
      popupOauthWindow.document.write(`Loading authentication to ${service}...`);

      getServiceUrl(service).then(url => {
        popupOauthWindow.location.replace(url);
        popupOauthWindow.focus();
      });
    };

    const getServiceUrl = async service => {
      const { user } = props;
      const urls = {
        discordURL,
        twitchURL: await getURL(service, "twitch"),
        steamURL: `${defaultUrl}/auth/steam?token=${user._id}`,
        linkedinURL: await getURL(service, "linkedin"),
        bnetURL: await getURL(service, "bnet"),
        xboxliveURL: xboxLiveAuthUrl,
        snapchatURL: await getURL(service, "snapchat"),
        googleURL: await getURL(service, "google"),
        twitterURL: await getURL(service, "twitter"),
        statespaceURL: await getURL(service, "statespace"),
        riotURL: await getURL(service, "riot")
      };

      return urls[`${service}URL`];
    };

    const getURL = async (incomingService, serviceToBuildURLFor) => {
      let url = "";

      if (incomingService === serviceToBuildURLFor) {
        try {
          const response = await axios.get(`/external_auth/${serviceToBuildURLFor}`);
          url = response.data.url;
        } catch (error) {
          console.error(`Error while fetching consent URL for ${serviceToBuildURLFor}`, error);
        }
      }

      return url;
    };

    // eslint-disable-next-line react/jsx-props-no-spreading
    return <WrappedComponent toggleAccount={toggleAccount} startOAuth={startOAuth} {...props} />;
  };

  const mapStateToProps = state => ({
    user: state.user.currentUser
  });
  return connect(mapStateToProps, {
    updateUserSimple,
    unlinkExternalAccount,
    getCurrentUser
  })(ExternalAccountWrapper);
};

export default authenticateAccount;
