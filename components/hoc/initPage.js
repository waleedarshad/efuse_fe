import React, { Component } from "react";
import Cookies from "js-cookie";
import { connect } from "react-redux";
import dynamic from "next/dynamic";
import nextCookie from "next-cookies";
import jwtDecode from "jwt-decode";
import fetch from "isomorphic-unfetch";
import getConfig from "next/config";
import Router from "next/router";

import Style from "./initPage.module.scss";
import { updateWebviewStatus } from "../../store/actions/webviewActions";
import { applyRedirection } from "../../helpers/AuthHelper";
import MetaHead from "../Layouts/MetaHead";

const CookieConsent = dynamic(() => import("react-cookie-consent"));

const { publicRuntimeConfig } = getConfig();
const { bulletTrainApi, bulletTrainEnvironment } = publicRuntimeConfig;

const initPage = (
  WrappedComponent,
  defaultNamespace = "common",
  namespacesRequired = ["common"],
  accessType = "private",
  hoc = null,
  view = null,
  disableRedirect = false,
  getInitialPropsForPage = null,
  featureFlag = null,
  metaTitle = null
) => {
  class Page extends Component {
    componentDidMount() {
      const referralUrl = document.referrer;
      const { query, updateWebviewStatus } = this.props;
      const isExternalReferrer = !!(
        referralUrl &&
        location?.host &&
        location.host !== "undefined" &&
        document.referrer.indexOf(`${location.protocol}//${location.host}`) === -1
      );
      const getReferralUrl = Cookies.get("referralUrl");
      if ((isExternalReferrer && !getReferralUrl) || (isExternalReferrer && referralUrl !== getReferralUrl)) {
        Cookies.set("referralUrl", referralUrl);
      }
      const routerQuery = this.props.router?.query;
      if ((query && query.webview) || (routerQuery && routerQuery.webview)) {
        updateWebviewStatus(true);
      }
    }

    static async getInitialProps(context) {
      // This checks if path should be redirected or not based on auth token
      if (!disableRedirect) applyRedirection(accessType, context);

      let pageProps;
      let flagSmithDataSSR;

      // Get flagsmith using SSR
      const { token } = nextCookie(context);
      if (token) {
        const user = jwtDecode(token);

        if (user.id) {
          const url = `${bulletTrainApi}identities/?identifier=${user.id}`;
          const config = {
            method: "GET",
            headers: {
              "Content-Type": "application/json",
              Accept: "application/json",
              "x-environment-key": bulletTrainEnvironment
            }
          };
          const res = await fetch(url, config);
          flagSmithDataSSR = await res.json();
        }
      }

      if (featureFlag && !flagSmithDataSSR?.flags?.find(flag => flag.feature.name === featureFlag)) {
        const redirectPath = "/lounge/featured";

        if (context.req) {
          context.res.writeHead(302, { Location: redirectPath });
          context.res.end();
        } else {
          await Router.push(redirectPath);
        }
      } else {
        if (getInitialPropsForPage) pageProps = await getInitialPropsForPage(context, token);

        return {
          namespacesRequired,
          query: context.query,
          pageProps,
          flagSmithDataSSR,
          isUserAuthenticated: !!token
        };
      }
    }

    render() {
      const { webview, pageProps } = this.props;

      return (
        <>
          {metaTitle && <MetaHead metaTitle={metaTitle} />}
          <WrappedComponent {...this.props} view={view} pageProps={pageProps} />
          {!webview && (
            <CookieConsent
              buttonText="AGREE"
              containerClasses={Style.cookieContainer}
              buttonClasses={Style.agreeButton}
            >
              By clicking Agree, you consent to the storing of cookies on your device to improve your browsing
              experience, analyze site usage and assist in our marketing efforts. You can use this tool to change your
              cookie settings.
            </CookieConsent>
          )}
        </>
      );
    }
  }

  const mapStateToProps = state => ({
    webview: state.webview.status
  });

  return connect(mapStateToProps, { updateWebviewStatus })(hoc ? hoc(Page) : Page);
};

export default initPage;
