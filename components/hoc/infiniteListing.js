import capitalize from "lodash/capitalize";
import isEmpty from "lodash/isEmpty";
import { withRouter } from "next/router";
import React from "react";
import { Col, Row } from "react-bootstrap";
import InfiniteScroll from "react-infinite-scroller";
import { connect, useDispatch, useSelector } from "react-redux";
import { getOpportunitiesParamsTail, getOrganizationsParamsTail } from "../../helpers/UrlHelper";
import {
  changePublishStatus,
  joinOrganization,
  leaveOrganization,
  removeOrganization,
  toggleModalShow
} from "../../store/actions/organizationActions";
import AnimatedLogo from "../AnimatedLogo";
import NoRecordFound from "../NoRecordFound/NoRecordFound";
import OpportunityLargeListItem from "../Opportunities/OpportunityWrapper/OpportunityLargeListing/OpportunityLargeListItem/OpportunityLargeListItem";
import OpportunitySmallListItem from "../Opportunities/OpportunityWrapper/OpportunitySmallListing/OpportunitySmallListItem/OpportunitySmallListItem";
import OrganizationListItem from "../Organizations/OrganizationWrapper/OrganizationListing/OrganizationListItem/OrganizationListItem";
import ApplicantComponentDeprecated from "../Opportunities/ApplicantsWrapper/ApplicantListing/ApplicantComponent/ApplicantComponentDeprecated";
import RecruitingCard from "../Recruiting/RecruitingList/RecruitingCard/RecruitingCard";
import Style from "./infiniteListing.module.scss";

const infiniteListing = (key, type) => {
  const InfiniteScrollableList = ({ id, profile, profileId, filterType, router, category, ...props }) => {
    const dispatch = useDispatch();
    const pagination = useSelector(state => state[key].pagination);
    const currentUser = useSelector(state => state.auth.currentUser);
    const modalShow = useSelector(state => state.organizations.modalShow);
    const opportunity = useSelector(state => state.opportunities.opportunity);
    const webview = useSelector(state => state.webview.status);
    const displayExternalSourceOpportunity = useSelector(state => state.features.display_external_source_opportunity);
    const recruitType = useSelector(state => state.recruiting.recruitType);
    const recruitId = useSelector(state => state.recruiting.recruitId);
    let searchObject = useSelector(state => state[key].searchObject);
    const starredRecruiting = key === "recruiting" && type === "starred";

    const loadMore = () => {
      searchObject = searchObject === undefined ? {} : searchObject;
      const nextPage = pagination ? pagination.page + 1 : 1;
      const defaultParams = getDefaultParams(nextPage);

      let params = id ? [id, ...defaultParams] : defaultParams;

      if (key === "applicants") {
        params = [...params, filterType];
      }

      if (!profile || (profile && profileId)) {
        if (key === "institution") {
          props[`get${type}`](...params);
        } else if (starredRecruiting) {
          if (recruitType === "ORGANIZATION") {
            params = [...params, "", `/recruitment_profile/organizations/${recruitId}/star`];
          }
          props[`getStarred${capitalize(key)}`](...params);
        } else {
          if (recruitType === "ORGANIZATION") {
            params = [...params, "", `/recruitment_profile/organizations/${recruitId}/all`];
          }
          props[`get${capitalize(key)}`](...params);
        }
      }
    };

    const onJoinClick = (orgId, canJoin, status) => {
      if (canJoin) dispatch(joinOrganization(`organizations/join/${orgId}`, status));
      else dispatch(leaveOrganization(orgId, status));
    };

    const statusSwitch = status => props.t("statusSwitch", { returnObjects: true })[status];

    const onToggleModalShow = orgId => {
      dispatch(toggleModalShow(orgId));
    };

    const changeOrgStatus = (orgId, status) => {
      const publishStatus = { status };
      dispatch(changePublishStatus(`organizations/${orgId}/change_status`, publishStatus));
    };

    const onRemoveOrganization = orgId => {
      dispatch(removeOrganization(orgId));
    };

    const getDefaultParams = nextPage => {
      let defaultParameters = [nextPage, 9, searchObject, false, false];
      switch (key) {
        case "opportunities":
          defaultParameters = [
            ...defaultParameters,
            ...getOpportunitiesParamsTail(router, profile, profileId, currentUser, category)
          ];
          break;
        case "organizations":
          defaultParameters.push(getOrganizationsParamsTail(router, currentUser));
          break;
        case "recruiting":
          defaultParameters = [nextPage, 9, searchObject, false, true];
          break;
        default:
          defaultParameters = [nextPage, 9, searchObject, false];
          break;
      }
      return defaultParameters;
    };

    const hasMore = pagination && pagination.hasNextPage;
    const listArray = props[key];

    const content = listArray.map((object, index) => {
      switch (key) {
        case "organizations":
          return (
            <OrganizationListItem
              key={index}
              organization={object}
              currentUser={currentUser}
              onJoinClick={onJoinClick}
              statusSwitch={statusSwitch}
              toggleModalShow={onToggleModalShow}
              modalShow={modalShow}
              changeOrgStatus={changeOrgStatus}
              removeOrganization={onRemoveOrganization}
            />
          );
        case "opportunities":
          if (type === "small") {
            return (
              <Col lg={12} md={12} className={Style.smallOppContainer}>
                <OpportunitySmallListItem
                  key={index}
                  opportunity={object}
                  colLg={12}
                  colMd={12}
                  webview={webview}
                  display_external_source_opportunity={displayExternalSourceOpportunity}
                />
                <hr className={Style.hr} />
              </Col>
            );
          }
          if (type === "large") {
            return (
              <Col lg={12} md={12}>
                <OpportunityLargeListItem
                  key={index}
                  opportunity={object}
                  colLg={12}
                  colMd={12}
                  webview={webview}
                  display_external_source_opportunity={displayExternalSourceOpportunity}
                  currentUser={currentUser}
                />
              </Col>
            );
          }
          break;
        case "applicants":
          return (
            <ApplicantComponentDeprecated
              key={index}
              user={object}
              applicant={object.applicant}
              filterType={filterType}
              opportunity={opportunity}
              currentUser={currentUser}
            />
          );
        case "recruiting":
          return (
            <Col lg={12} md={12} key={index}>
              <RecruitingCard
                key={index}
                user={object}
                filterType={filterType}
                recruit={object}
                starred={object.starred}
              />
            </Col>
          );

        default:
          return <></>;
      }

      return <></>;
    });

    return (
      <InfiniteScroll
        pageStart={1}
        loadMore={loadMore}
        hasMore={hasMore}
        loader={<AnimatedLogo key={0} theme="inline" />}
      >
        <Row>
          {isEmpty(pagination) && (
            <div className="text-center" style={{ width: "100%" }}>
              <AnimatedLogo key={0} theme="" />
            </div>
          )}
          {props[key].length === 0 ? <NoRecordFound /> : content}
        </Row>
      </InfiniteScroll>
    );
  };
  const mapStateToProps = state => ({
    [key]: state[key][key]
  });

  return connect(mapStateToProps, {
    toggleModalShow,
    joinOrganization,
    leaveOrganization,
    changePublishStatus,
    removeOrganization
  })(withRouter(InfiniteScrollableList));
};

export default infiniteListing;
