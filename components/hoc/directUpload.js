import React, { Component } from "react";
import PropTypes from "prop-types";

import { deleteFromS3 } from "../../store/actions/mediaActions";

const directUpload = WrappedComponent => {
  class Upload extends Component {
    state = {
      uploadProgress: 0,
      uploadStarted: false,
      fileKey: "",
      file: {
        url: "",
        filename: "",
        contentType: ""
      }
    };

    componentDidUpdate() {
      if (this.props.clearMedia) {
        this.clearStates();
        this.props.onClearMedia();
      }
    }

    handleFinishedUpload = info => {
      const file = {
        ...this.state.file,
        url: `${info.signedUrl.split("/uploads")[0]}/${info.fileKey}`,
        filename: info.filename
      };
      this.setState({
        file,
        fileKey: info.fileKey
      });
      this.props.onFileUpload(file);
    };

    onUploadProgress = uploadProgress => {
      this.setState({ uploadProgress });
    };

    onUploadError = info => {
      console.log("Uploading Error", info);
    };

    onUploadStart = (file, next) => {
      this.setState({
        uploadStarted: true,
        file: {
          ...this.state.file,
          contentType: file.type
        }
      });
      next(file);
    };

    cancelUpload = () => {
      deleteFromS3(this.state.fileKey);
      const file = { url: "", filename: "", contentType: "" };
      this.props.onFileUpload(file);
      this.clearStates();
    };

    clearStates = () => {
      this.setState({
        uploadProgress: 0,
        uploadStarted: false,
        fileKey: "",
        file: { url: "", filename: "", contentType: "" }
      });
    };

    render() {
      return (
        <WrappedComponent
          handleFinishedUpload={this.handleFinishedUpload}
          onUploadProgress={this.onUploadProgress}
          onUploadError={this.onUploadError}
          onUploadStart={this.onUploadStart}
          cancelUpload={this.cancelUpload}
          {...this.state}
          {...this.props}
        />
      );
    }
  }

  Upload.propTypes = {
    onFileUpload: PropTypes.func.isRequired
  };
  return Upload;
};

export default directUpload;
