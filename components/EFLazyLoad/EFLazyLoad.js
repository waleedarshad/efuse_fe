import React from "react";
import LazyLoad from "react-lazyload";
// https://www.npmjs.com/package/react-lazyload

const EFLazyLoad = ({ children, offset, height, once, style }) => {
  return (
    <LazyLoad offset={offset} height={height} once={once} style={style}>
      {children}
    </LazyLoad>
  );
};

export default EFLazyLoad;
