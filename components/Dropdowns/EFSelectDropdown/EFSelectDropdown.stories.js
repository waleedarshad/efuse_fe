import React from "react";
import EFSelectDropdown from "./EFSelectDropdown";

export default {
  title: "Dropdown/EFSelectDropdown",
  component: EFSelectDropdown
};

// eslint-disable-next-line react/jsx-props-no-spreading
const Story = args => <EFSelectDropdown {...args} />;

export const Simple = Story.bind({});
Simple.args = {
  defaultValue: "apples",
  options: [
    { value: "apples", label: "I like eating apples" },
    { value: "bananas", label: "actually, I like bananas" },
    { value: "strawberries", label: "JK, I like strawberries" }
  ],
  // eslint-disable-next-line no-console
  onSelect: value => console.log(value)
};

export const Complex = Story.bind({});
Complex.args = {
  defaultValue: { id: 1, name: "lucho" },
  options: [
    {
      value: { id: 1, name: "lucho" },
      label: (
        <div>
          <h1>This is an h1 in a div</h1>
        </div>
      )
    },
    { value: { id: 2, name: "carlone" }, label: "this is just a string. You can pass whatever you want" },
    { value: { id: 3, name: "mafioso" }, label: 99999 },
    { value: { id: 4, name: "wat" }, label: <h3>this is an h3</h3> }
  ],
  // eslint-disable-next-line no-console
  onSelect: value => console.log(value)
};
