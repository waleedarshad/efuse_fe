import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck, faChevronDown } from "@fortawesome/pro-regular-svg-icons";
import isEqual from "lodash/isEqual";
import PropTypes from "prop-types";
import { forwardRef, useEffect, useImperativeHandle, useRef, useState } from "react";
import Style from "./EFSelectDropdown.module.scss";

const EFSelectDropdown = ({ defaultValue, header, options, onSelect, disabled, disableCheckmark, className }, ref) => {
  const node = useRef();

  useImperativeHandle(ref, () => ({
    selectOption: findOption => {
      const optionFound = options.find(findOption);
      if (optionFound) {
        handleChange(optionFound.value);
      }
    }
  }));

  const [isOpen, setIsOpen] = useState(false);

  const handleClick = e => {
    if (!node.current.contains(e.target)) {
      // outside click
      setIsOpen(false);
    }
  };

  const handleChange = selectedValue => {
    if (!disabled) {
      onSelect(selectedValue);
      setIsOpen(false);
      setCurrentValue(selectedValue);
    }
  };

  useEffect(() => {
    document.addEventListener("mousedown", handleClick);

    if (defaultValue) {
      onSelect(defaultValue);
    }

    return () => {
      document.removeEventListener("mousedown", handleClick);
    };
  }, []);

  const [currentValue, setCurrentValue] = useState(defaultValue || header);

  return (
    <div ref={node} className={`${Style.selectDropdown} ${className}`} disabled={disabled}>
      <div
        role="button"
        tabIndex={0}
        onKeyDown={() => setIsOpen(true)}
        className={Style.selectedValue}
        onClick={() => setIsOpen(true)}
      >
        {options.find(option => isEqual(option.value, currentValue))?.label}
        <FontAwesomeIcon className={Style.chevron} icon={faChevronDown} />
      </div>
      {isOpen && (
        <div className={Style.optionsList}>
          {options.map((option, index) => (
            <div
              role="button"
              onKeyDown={() => handleChange(option.value)}
              tabIndex={index}
              // eslint-disable-next-line react/no-array-index-key
              key={index}
              className={Style.option}
              onClick={() => handleChange(option.value)}
            >
              {option.label}
              {!disableCheckmark && isEqual(option.value, currentValue) && (
                <FontAwesomeIcon icon={faCheck} className={Style.checkmark} />
              )}
            </div>
          ))}
        </div>
      )}
    </div>
  );
};

EFSelectDropdown.propTypes = {
  /**
   * initial value. It has to match one of the values in the options array.
   */
  defaultValue: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.object]),

  /**
   * header value. It has to match one of the values in the options array.
   */
  header: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.object]),
  /**
   * options to render in the select dropdown. Value can have multiple types. Label is what is rendered.
   */
  options: PropTypes.arrayOf(
    PropTypes.shape({
      value: PropTypes.oneOfType([PropTypes.string, PropTypes.number, PropTypes.object]),
      label: PropTypes.node
    })
  ).isRequired,

  /**
   * function that will get called with the value corresponding to the label clicked.
   */
  onSelect: PropTypes.func.isRequired,

  /**
   * Used to disable the dropdown
   */
  disabled: PropTypes.bool,

  /**
   * Used to disable the selected item's checkmark
   */
  disableCheckmark: PropTypes.bool
};

EFSelectDropdown.defaultProps = {
  disabled: false,
  disableCheckmark: false,
  defaultValue: "",
  header: ""
};

export default forwardRef(EFSelectDropdown);
