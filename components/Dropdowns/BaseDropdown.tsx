import React, { ReactNode, forwardRef } from "react";
import { Dropdown as BSDropdown } from "react-bootstrap";

export interface BaseDropdownProps {
  menuItems: { option: ReactNode | string; onClick: (event: any) => void }[];
  toggle: ReactNode;
}

type Props = {
  children: ReactNode;
  onClick: (event: any) => void;
};

const renderToggle = toggle =>
  // eslint-disable-next-line react/display-name
  forwardRef<Props>(({ children, onClick }: Props, ref: any) => (
    // eslint-disable-next-line jsx-a11y/anchor-is-valid
    <a
      href=""
      ref={ref}
      onClick={e => {
        e.preventDefault();
        e.stopPropagation();
        onClick(e);
      }}
    >
      {children}
      {toggle}
    </a>
  ));

const BaseDropdown = ({ menuItems, toggle }: BaseDropdownProps) => {
  const onItemClicked = (e, item) => {
    e.stopPropagation();
    item.onClick();
  };

  return (
    <BSDropdown alignRight>
      <BSDropdown.Toggle as={renderToggle(toggle)} />
      <BSDropdown.Menu>
        {menuItems.map((item, index) => (
          <BSDropdown.Item key={index} onClick={e => onItemClicked(e, item)}>
            {item.option}
          </BSDropdown.Item>
        ))}
      </BSDropdown.Menu>
    </BSDropdown>
  );
};

export default BaseDropdown;
