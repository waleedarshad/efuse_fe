import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import { IconProp } from "@fortawesome/fontawesome-svg-core";
import Style from "./EFMenuItem.module.scss";

interface EFMenuItemProps {
  icon: IconProp;
  text: string;
  iconColorTheme: "blue" | "red" | "black";
  textColorTheme: "red" | "black";
}

const EFMenuItem = ({ icon, text, iconColorTheme = "blue", textColorTheme = "black" }: EFMenuItemProps) => {
  return (
    <p className={`${Style.menuItem} ${Style[textColorTheme]}`}>
      <FontAwesomeIcon icon={icon} className={`${Style.icon} ${Style[iconColorTheme]}`} />
      {text}
    </p>
  );
};

export default EFMenuItem;
