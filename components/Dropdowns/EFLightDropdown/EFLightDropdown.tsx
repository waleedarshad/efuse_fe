import styled from "styled-components";
import { faEllipsisH } from "@fortawesome/pro-regular-svg-icons";
import React from "react";
import Style from "./EFLightDropdown.module.scss";
import BaseDropdown, { BaseDropdownProps } from "../BaseDropdown";
import EFCircleIconButton from "../../Buttons/EFCircleIconButton/EFCircleIconButton";

const DropdownWrapper = styled.div`
  .dropdown-menu {
    background: white;
    border: 1px solid #c7d3ea;
    box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.15);
    border-radius: 8px;
    padding: 0;
    overflow: hidden !important;
  }

  .dropdown-item {
    padding-top: 10px;
    padding-bottom: 10px;

    &:active {
      background: #eff2f8;
      color: inherit;
    }
  }
`;

const Toggle = () => (
  <div className={Style.toggle}>
    <EFCircleIconButton shadowTheme="none" colorTheme="transparent" icon={faEllipsisH} />
  </div>
);

// eslint-disable-next-line no-undef
interface EFLightDropdownProps extends Pick<BaseDropdownProps, "menuItems"> {}

const EFLightDropdown = ({ menuItems }: EFLightDropdownProps) => (
  <DropdownWrapper>
    <BaseDropdown menuItems={menuItems} toggle={<Toggle />} />
  </DropdownWrapper>
);

export default EFLightDropdown;
