import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faExclamationCircle } from "@fortawesome/pro-solid-svg-icons";

import Style from "./InfoMessage.module.scss";

const InfoMessage = props => {
  const { text } = props;
  return (
    <div className={Style.infoTextBox}>
      <FontAwesomeIcon className={Style.exclamation} icon={faExclamationCircle} />
      {text}
    </div>
  );
};

export default InfoMessage;
