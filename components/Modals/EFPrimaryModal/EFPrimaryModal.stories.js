const modalRoot = document.createElement("div");
modalRoot.setAttribute("id", "modal-root");
document.body.append(modalRoot);

import React from "react";
import EFPrimaryModal from "./EFPrimaryModal";

export default {
  title: "Modals/EFPrimaryModal",
  component: EFPrimaryModal,
  argTypes: {
    title: {
      control: {
        type: "text"
      }
    }
  }
};

const Story = args => <EFPrimaryModal {...args} />;

export const Basic = Story.bind({});
Basic.args = {
  children: <div>This is a test div!</div>,
  title: "This is a Title!",
  isOpen: true,
  allowBackgroundClickClose: false,
  displayCloseButton: true
};
