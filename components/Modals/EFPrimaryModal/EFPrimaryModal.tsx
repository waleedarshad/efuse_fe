import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft } from "@fortawesome/pro-solid-svg-icons";
import React from "react";
import EFModal from "../EFModal/EFModal";
import Style from "./EFPrimaryModal.module.scss";

interface EFPrimaryModalProps {
  children: React.ReactNode;
  title?: string;
  subtitle?: string | React.ReactNode;
  isOpen: boolean;
  onClose?: () => void;
  onOpen?: () => void;
  allowBackgroundClickClose?: boolean;
  displayCloseButton?: boolean;
  widthTheme?: "extraSmall" | "small" | "medium" | "large" | "fullscreen";
}

const EFPrimaryModal = ({
  children,
  title,
  subtitle,
  isOpen,
  onClose = () => {},
  onOpen = () => {},
  allowBackgroundClickClose = true,
  displayCloseButton = true,
  widthTheme = "small"
}: EFPrimaryModalProps) => {
  return (
    <EFModal
      isOpen={isOpen}
      onClose={onClose}
      onOpen={onOpen}
      allowBackgroundClickClose={allowBackgroundClickClose}
      displayCloseButton={displayCloseButton}
      widthTheme={widthTheme}
    >
      <div className={Style.modalContainer}>
        {title && <EFPrimaryModalHeader title={title} subtitle={subtitle} />}
        {children}
      </div>
    </EFModal>
  );
};

interface EFPrimaryModalHeaderProps {
  title: string;
  subtitle?: string | React.ReactNode;
  onBackClick?: () => void;
}

export const EFPrimaryModalHeader = ({ title, onBackClick, subtitle }: EFPrimaryModalHeaderProps) => {
  return (
    <>
      <div className={Style.headerContainer}>
        <div className={Style.titleContainer}>
          {onBackClick && <FontAwesomeIcon icon={faChevronLeft} className={Style.backButton} onClick={onBackClick} />}
          <h3 className={Style.title}>{title}</h3>
        </div>
        {subtitle && <div className={Style.subtitle}>{subtitle}</div>}
      </div>
    </>
  );
};

export default EFPrimaryModal;
