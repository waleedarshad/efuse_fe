import { useDispatch, useSelector } from "react-redux";

import CreatePostContent from "../CreatePostModal/CreatePostContent";
import EFPrimaryModal from "../EFPrimaryModal/EFPrimaryModal";
import { closePostEdit } from "../../../store/actions/feedActions";

const EditPostModal = () => {
  const dispatch = useDispatch();

  const postToEdit = useSelector(state => state.feed.postToEdit);

  const closeModalHandler = () => {
    analytics.track("EDIT_POST_MODAL_CLOSE");
    dispatch(closePostEdit());
  };

  if (!postToEdit) {
    return <></>;
  }

  return (
    <EFPrimaryModal
      isOpen={!!postToEdit}
      onClose={closeModalHandler}
      displayCloseButton
      allowBackgroundClickClose={false}
    >
      <CreatePostContent
        onFormSubmitted={() => dispatch(closePostEdit())}
        isPostEditing={!!postToEdit}
        postToEdit={postToEdit}
      />
    </EFPrimaryModal>
  );
};

export default EditPostModal;
