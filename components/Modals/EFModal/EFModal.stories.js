const modalRoot = document.createElement("div");
modalRoot.setAttribute("id", "modal-root");
document.body.append(modalRoot);

import React from "react";
import EFModal from "./EFModal";

export default {
  title: "Modals/EFModal",
  component: EFModal,
  argTypes: {
    isOpen: {
      control: {
        type: "boolean"
      }
    },
    allowBackgroundClickClose: {
      control: {
        type: "boolean"
      }
    },
    displayCloseButton: {
      control: {
        type: "boolean"
      }
    },
    widthTheme: {
      control: {
        type: "select",
        options: ["small", "medium", "large", "fullscreen"]
      }
    }
  }
};

const Story = args => <EFModal {...args} />;

export const Basic = Story.bind({});
Basic.args = {
  children: <div>This is a test div!</div>,
  isOpen: true,
  allowBackgroundClickClose: false,
  displayCloseButton: false
};

export const AllowBackgroundClick = Story.bind({});
AllowBackgroundClick.args = {
  children: <div>This is a test div!</div>,
  isOpen: true,
  allowBackgroundClickClose: true,
  displayCloseButton: false
};

export const DisplayCloseButton = Story.bind({});
DisplayCloseButton.args = {
  children: <div>This is a test div!</div>,
  isOpen: true,
  allowBackgroundClickClose: false,
  displayCloseButton: true
};
