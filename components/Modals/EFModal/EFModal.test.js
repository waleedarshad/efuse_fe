import { shallow, mount } from "enzyme";
import EFCircleIconButton from "../../Buttons/EFCircleIconButton/EFCircleIconButton";
import EFModal from "./EFModal";

describe("EFModal", () => {
  beforeEach(() => {
    const modalRoot = document.createElement("div");
    modalRoot.setAttribute("id", "modal-root");
    document.body.append(modalRoot);
  });

  it("renders children when it is open", () => {
    const subject = shallow(
      <EFModal isOpen>
        <div className="mydiv">my content</div>
      </EFModal>
    );

    expect(subject.find("div.mydiv").text()).toEqual("my content");
  });

  it("does NOT render children when is open is false", () => {
    const subject = shallow(
      <EFModal isOpen={false}>
        <div className="mydiv">my content</div>
      </EFModal>
    );

    expect(subject.find("div.mydiv")).toEqual({});
  });

  it("closes the modal when modal background is clicked", () => {
    const onCloseCallback = jest.fn();

    const subject = shallow(
      <EFModal isOpen onClose={onCloseCallback}>
        <div className="mydiv">my content</div>
      </EFModal>
    );

    subject.find("div.modalBackground").simulate("click");
    expect(onCloseCallback).toHaveBeenCalled();
    expect(subject.find("div.mydiv")).toEqual({});
  });

  it("closes the modal when icon button is clicked", () => {
    const onCloseCallback = jest.fn();

    const subject = shallow(
      <EFModal isOpen onClose={onCloseCallback}>
        <div className="mydiv">my content</div>
      </EFModal>
    );

    subject.find(EFCircleIconButton).simulate("click");
    expect(onCloseCallback).toHaveBeenCalled();
    expect(subject.find("div.mydiv")).toEqual({});
  });

  it("ensures the onOpen callback is executed when modal is opened", () => {
    const onOpenCallback = jest.fn();

    mount(<EFModal isOpen onOpen={onOpenCallback} />);

    expect(onOpenCallback).toHaveBeenCalled();
  });

  it("checks if the widthTheme props are added properly", () => {
    const subject = shallow(<EFModal isOpen widthTheme="medium" />);

    expect(subject.find("div.modal").hasClass("medium"));
  });
});
