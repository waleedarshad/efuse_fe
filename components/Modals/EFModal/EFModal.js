/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */

import { clearAllBodyScrollLocks, disableBodyScroll } from "body-scroll-lock";
import React, { useEffect, useRef, useState } from "react";
import ReactDOM from "react-dom";
import { faTimes } from "@fortawesome/pro-solid-svg-icons";

import EFCircleIconButton from "../../Buttons/EFCircleIconButton/EFCircleIconButton";
import Style from "./EFModal.module.scss";
import WidthStyle from "./ModalWidthThemes.module.scss";

const EFModal = ({
  children,
  isOpen,
  onClose,
  onOpen,
  allowBackgroundClickClose,
  displayCloseButton,
  widthTheme,
  allowCloseOnEscape,
  className = ""
}) => {
  const modalBackgroundRef = useRef();
  const [open, setOpenState] = useState(isOpen);

  const closeOnEscape = e => {
    if (e.key === "Escape") {
      closeModal();
    }
  };

  useEffect(() => {
    setOpenState(isOpen);

    if (isOpen) {
      lockBodyScroll();

      if (allowCloseOnEscape) {
        window.addEventListener("keydown", closeOnEscape);
      }

      if (onOpen) {
        onOpen();
      }
    }

    return () => {
      clearAllBodyScrollLocks();

      window.removeEventListener("keydown", closeOnEscape);
    };
  }, [isOpen]);

  const lockBodyScroll = () => {
    const targetElement = document.getElementById("modal-root");
    if (modalBackgroundRef.current) {
      disableBodyScroll(modalBackgroundRef.current);
    } else if (targetElement) {
      disableBodyScroll(targetElement);
    }
  };

  const closeModal = () => {
    if (onClose) {
      onClose();
    }
    setOpenState(false);
  };

  const modal = (
    <div
      className={Style.modalBackground}
      ref={modalBackgroundRef}
      onClick={() => {
        if (allowBackgroundClickClose) {
          closeModal();
        }
      }}
    >
      <div className={`${Style.modal} ${WidthStyle[widthTheme]} ${className}`} onClick={e => e.stopPropagation()}>
        {displayCloseButton && (
          <div className={Style.modalCloseButton}>
            <EFCircleIconButton
              colorTheme="transparent"
              onClick={() => closeModal()}
              icon={faTimes}
              shadowTheme="none"
            />
          </div>
        )}
        {children}
      </div>
    </div>
  );

  if (typeof window !== "undefined" && open) {
    return <>{ReactDOM.createPortal(modal, document.getElementById("modal-root"))}</>;
  }

  return <></>;
};

EFModal.defaultProps = {
  widthTheme: "small",
  displayCloseButton: true,
  allowBackgroundClickClose: true,
  allowCloseOnEscape: false
};

export default EFModal;

/* eslint-enable jsx-a11y/no-static-element-interactions */
/* eslint-enable jsx-a11y/click-events-have-key-events */
