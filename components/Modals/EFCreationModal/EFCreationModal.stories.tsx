import React from "react";
import EFCreationModal from "./EFCreationModal";

export default {
  title: "Modals/EFCreationModal",
  component: EFCreationModal,
  argTypes: {
    allowBackgroundClickClose: {
      control: {
        type: "boolean"
      }
    },
    imageText: {
      control: {
        type: "text"
      }
    },
    title: {
      control: {
        type: "text"
      }
    },
    subText: {
      control: {
        type: "text"
      }
    },
    displayCloseButton: {
      control: {
        type: "boolean"
      }
    },
    imgSrc: {
      control: {
        type: "text"
      }
    },
    widthTheme: {
      control: {
        type: "select",
        options: ["extraSmall", "small", "medium", "large", "fullscreen"]
      }
    },
    bottomRightButton: {
      control: {
        type: "text"
      }
    },
    bottomLeftButton: {
      control: {
        type: "text"
      }
    }
  }
};

// eslint-disable-next-line react/jsx-props-no-spreading
const Story = args => (
  <div className="m-3">
    <EFCreationModal {...args} />
  </div>
);

export const Default = Story.bind({});
Default.args = {
  imageText: "Welcome! Create your whatever",
  title: "Let's begin creating your insert-thing-here!",
  allowBackgroundClickClose: true,
  bottomRightButton: "Continue",
  displayCloseButton: false,
  imgSrc:
    "https://www.callofduty.com/content/dam/atvi/callofduty/cod-touchui/blog/hero/bocw/BOCW-S4-Announcement-TOUT.jpg",
  isOpen: true,
  subText: "Lorem ipsum lorem ipsum",
  widthTheme: "small",
  onClose: () => {}
};

export const WithLeftButton = Story.bind({});
WithLeftButton.args = {
  imageText: "Welcome! Create your whatever",
  title: "Let's begin creating your insert-thing-here!",
  allowBackgroundClickClose: true,
  bottomRightButton: "Continue",
  bottomLeftButton: "Next",
  displayCloseButton: false,
  imgSrc:
    "https://www.callofduty.com/content/dam/atvi/callofduty/cod-touchui/blog/hero/bocw/BOCW-S4-Announcement-TOUT.jpg",
  isOpen: true,
  subText: "Lorem ipsum lorem ipsum",
  widthTheme: "small",
  onClose: () => {}
};
