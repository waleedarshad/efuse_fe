import React from "react";
import EFModal from "../EFModal/EFModal";
import Style from "./EFCreationModal.module.scss";
import EFOpacityImage from "../../EFOpacityImage/EFOpacityImage";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";

interface EFCreationModalProps {
  isOpen: boolean;
  title: string;
  subText: string;
  imageText: string;
  imgSrc: string;
  onClose?: () => void;
  onOpen?: () => void;
  allowBackgroundClickClose?: boolean;
  displayCloseButton?: boolean;
  bottomLeftButton?: string;
  onLeftButtonClick?: () => void;
  bottomRightButton: string | React.ReactNode;
  onRightButtonClick?: () => void;
  rightButtonColorTheme?: "primary" | "secondary" | "transparent";
  rightButtonDisabled?: boolean;
  widthTheme?: "extraSmall" | "small" | "medium" | "large" | "fullscreen";
}

const EFCreationModal: React.FC<EFCreationModalProps> = ({
  imageText,
  title,
  subText,
  bottomLeftButton,
  bottomRightButton,
  imgSrc,
  isOpen,
  onClose = () => {},
  onOpen = () => {},
  allowBackgroundClickClose = false,
  displayCloseButton = false,
  widthTheme = "small",
  onLeftButtonClick,
  onRightButtonClick,
  rightButtonColorTheme = "transparent",
  rightButtonDisabled = false
}) => {
  return (
    <EFModal
      isOpen={isOpen}
      onClose={onClose}
      onOpen={onOpen}
      allowBackgroundClickClose={allowBackgroundClickClose}
      displayCloseButton={displayCloseButton}
      widthTheme={widthTheme}
    >
      <div className={Style.modalContainer}>
        <div className={Style.leftContainer}>
          <EFOpacityImage imgSrc={imgSrc} />
          <div className={Style.imageText}>{imageText}</div>
        </div>
        <div className={Style.rightContainer}>
          <div className={Style.rightTextWrapper}>
            <p className={Style.titleText}>{title}</p>
            <p className={Style.subText}>{subText}</p>
          </div>
          <div className={Style.bottomButtons}>
            <div className={Style.leftButtonWrapper}>
              {bottomLeftButton && (
                <EFRectangleButton
                  text={bottomLeftButton}
                  onClick={onLeftButtonClick || onClose}
                  shadowTheme="none"
                  fontWeightTheme="normal"
                  colorTheme="transparent"
                />
              )}
            </div>
            <div className={Style.rightButtonWrapper}>
              <EFRectangleButton
                text={bottomRightButton}
                onClick={onRightButtonClick || onClose}
                shadowTheme={rightButtonColorTheme === "transparent" ? "none" : "medium"}
                fontWeightTheme="normal"
                colorTheme={rightButtonColorTheme}
                disabled={rightButtonDisabled}
              />
            </div>
          </div>
        </div>
      </div>
    </EFModal>
  );
};

export default EFCreationModal;
