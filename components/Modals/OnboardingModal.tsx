import { useSelector } from "react-redux";
import DynamicModal from "../DynamicModal/DynamicModal";

const OnboardingModal = () => {
  const isOnboardingFlowEnabled = useSelector(state => state.features.onboarding_flow);

  if (!isOnboardingFlowEnabled) {
    return <></>;
  }
  return (
    <DynamicModal
      flow="Onboarding"
      openOnLoad
      startView="selectGames"
      displayCloseButton={false}
      allowBackgroundClickClose={false}
      customMaxWidth="800px"
    />
  );
};

export default OnboardingModal;
