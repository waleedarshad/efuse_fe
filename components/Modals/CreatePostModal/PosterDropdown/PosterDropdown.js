import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import PropTypes from "prop-types";

import { getPostableOrganizations } from "../../../../store/actions/userActions";
import EFSelectDropdown from "../../../Dropdowns/EFSelectDropdown/EFSelectDropdown";
import EFUserBadge from "../../../EFUserBadge/EFUserBadge";
import CurrentUserBadge from "../CurrentUserBadge/CurrentUserBadge";

const PosterDropdown = ({ onPosterSelected, defaultValue, disabled }) => {
  const dispatch = useDispatch();
  const currentUser = useSelector(state => state.auth.currentUser);
  const organizations = useSelector(state => state.user.postableOrganizations);
  const getValueOption = (id, type, verified) => ({ id, type, verified });
  const options = [];

  if (currentUser?._id)
    options.push({
      value: getValueOption(currentUser._id, "users", currentUser.verified),
      label: <CurrentUserBadge />
    });

  organizations &&
    options.push(
      ...organizations.map(org => ({
        value: getValueOption(org.id, "organizations", org.verified.status),
        label: <EFUserBadge profilePictureUrl={org.profileImage.url} fullname={org.name} username={org.name} />
      }))
    );

  if (defaultValue) {
    options.unshift({
      value: getValueOption(defaultValue.id, defaultValue.type, defaultValue.verified),
      label: (
        <EFUserBadge
          profilePictureUrl={defaultValue.image}
          fullname={defaultValue.name}
          username={defaultValue.username}
        />
      )
    });
  }

  useEffect(() => {
    dispatch(getPostableOrganizations(1));
  }, []);

  return (
    <EFSelectDropdown
      disabled={disabled}
      options={options}
      defaultValue={options.length > 0 ? options[0].value : null}
      onSelect={onPosterSelected}
    />
  );
};

PosterDropdown.propTypes = {
  onPosterSelected: PropTypes.func.isRequired,
  defaultValue: PropTypes.shape({
    id: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    verified: PropTypes.bool.isRequired,
    image: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    username: PropTypes.string.isRequired
  }),
  disabled: PropTypes.bool
};

PosterDropdown.defaultProps = {
  disabled: false,
  defaultValue: null
};

export default PosterDropdown;
