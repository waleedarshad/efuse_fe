import moment from "moment";
import { useEffect, useRef, useState } from "react";
import Flatpickr from "react-flatpickr";
import PropTypes from "prop-types";
import { faCalendarAlt } from "@fortawesome/pro-regular-svg-icons";

import { isIosMobile } from "../../../../../common/utils";
import EFPillButton from "../../../../Buttons/EFPillButton/EFPillButton";
import Style from "./ScheduleThisPost.module.scss";

const ScheduleThisPost = ({ onDateUpdate, disabled }) => {
  const [date, setDate] = useState([]);
  const ref = useRef(null);

  useEffect(() => {
    if (date.length > 0) {
      onDateUpdate(date);
    }
  }, [date]);

  const onClickHandler = () => {
    analytics.track("CREATE_POST_ACTION_BUTTON", { action: "Schedule This Post" });
    setTimeout(() => ref.current.flatpickr.open());
  };

  const today = new Date();

  return (
    <div className={Style.calendar}>
      <Flatpickr
        ref={ref}
        value={date}
        options={{
          position: "auto center",
          enableTime: true,
          minDate: new Date().setHours(0, 0, 0, 0),
          defaultHour: today.getHours(),
          defaultMinute: today.getMinutes(),
          minuteIncrement: 1,
          disableMobile: isIosMobile()
        }}
        onChange={selectedValue => {
          const datetime = selectedValue[0];

          if (moment(datetime).isAfter()) {
            setDate(selectedValue);
          } else {
            setDate([]);
          }
        }}
      />
      <EFPillButton
        buttonType="button"
        shadowTheme="small"
        colorTheme="light"
        fontWeightTheme="normal"
        icon={faCalendarAlt}
        text="Schedule This Post"
        width="fullWidth"
        size="large"
        onClick={onClickHandler}
        alignContent="left"
        disabled={disabled}
      />
    </div>
  );
};

ScheduleThisPost.propTypes = {
  onDateUpdate: PropTypes.func.isRequired,
  disabled: PropTypes.bool
};

ScheduleThisPost.defaultProps = {
  disabled: false
};

export default ScheduleThisPost;
