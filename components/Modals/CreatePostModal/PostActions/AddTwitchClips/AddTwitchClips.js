import { faTwitch } from "@fortawesome/free-brands-svg-icons";
import EFPillButton from "../../../../Buttons/EFPillButton/EFPillButton";
import EFTwitchClipSelect from "../../../../EFTwitchClipSelect/EFTwitchClipSelect";

const AddTwitchClips = ({ onTwitchClipSelected, isDisabled }) => {
  return (
    <EFTwitchClipSelect
      onTwitchClipSelected={selectedClip => {
        analytics.track("CREATE_POST_ACTION_BUTTON", { action: "Twitch Clips" });
        onTwitchClipSelected(selectedClip);
      }}
      renderTrigger={() => (
        <EFPillButton
          fullWidth
          buttonType="button"
          shadowTheme="small"
          colorTheme="light"
          fontWeightTheme="normal"
          icon={faTwitch}
          text="Twitch Clips"
          width="fullWidth"
          size="large"
          disabled={isDisabled}
          alignContent="left"
        />
      )}
      modalStackOrder="level2"
      disableTrigger={isDisabled}
    />
  );
};

export default AddTwitchClips;
