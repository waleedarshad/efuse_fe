import { useContext } from "react";
import { faPhotoVideo } from "@fortawesome/pro-regular-svg-icons";

import EFPillButton from "../../../../Buttons/EFPillButton/EFPillButton";
import DirectUpload from "../../../../DirectUpload/DirectUpload";
import { UploadContext } from "../../../../DirectUpload/UploadContext";

const AddMedia = ({ disabled }) => {
  const { onUploadStart, onUploadSuccess, onUploadError, onUploadProgress } = useContext(UploadContext);

  const onFileSelected = (fileToUpload, startCallback) => {
    analytics.track("CREATE_POST_ACTION_BUTTON", { action: "Add Clips, Media, & Camera" });
    onUploadStart(fileToUpload, startCallback);
  };

  return (
    <DirectUpload
      onProgress={onUploadProgress}
      onError={onUploadError}
      preprocess={onFileSelected}
      onFinish={onUploadSuccess}
      removeMargin
      noOpacityDisabled
      customDesign
      disabled={disabled}
    >
      <EFPillButton
        buttonType="button"
        shadowTheme="small"
        colorTheme="light"
        fontWeightTheme="normal"
        icon={faPhotoVideo}
        text="Add Clips, Media, &#38; Camera"
        width="fullWidth"
        size="large"
        disabled={disabled}
        alignContent="left"
      />
    </DirectUpload>
  );
};

export default AddMedia;
