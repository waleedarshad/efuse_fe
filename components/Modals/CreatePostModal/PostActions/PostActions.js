import { useContext, useEffect } from "react";
import { faGif } from "../../../../config/customIcons";

import FEATURE_FLAGS from "../../../../common/featureFlags";
import EFPillButton from "../../../Buttons/EFPillButton/EFPillButton";
import EFGiphySearch from "../../../EFGiphySearch/EFGiphySearch";
import EFSelectGamesField from "../../../EFSelectGamesField/EFSelectGamesField";
import FeatureFlag from "../../../General/FeatureFlag/FeatureFlag";
import FeatureFlagVariant from "../../../General/FeatureFlag/FeatureFlagVariant/FeatureFlagVariant";
import { UploadContext } from "../../../DirectUpload/UploadContext";
import AddMedia from "./AddMedia/AddMedia";
import AddTwitchClips from "./AddTwitchClips/AddTwitchClips";
import AddYoutubeClips from "./AddYoutubeClips/AddYoutubeClips";
import Style from "./PostActions.module.scss";
import ScheduleThisPost from "./ScheduleThisPost/ScheduleThisPost";

const PostActions = ({
  onFileUpload,
  onGifSelected,
  isMediaUploaded,
  onScheduledDateUpdated,
  onTwitchClipSelected,
  isTwitchClipsDisabled,
  onYoutubeClipSelected,
  isYoutubeClipsDisabled,
  isScheduleDisabled,
  taggedGameId,
  onTaggedGameIdChange
}) => {
  const { isUploading, uploadedFile } = useContext(UploadContext);

  const areMediaActionsDisabled = isMediaUploaded || isUploading;

  useEffect(() => {
    onFileUpload(uploadedFile);
  }, [uploadedFile.url]);

  return (
    <div className={Style.actionList}>
      <AddMedia disabled={areMediaActionsDisabled} />
      <ScheduleThisPost disabled={isScheduleDisabled} onDateUpdate={onScheduledDateUpdated} />

      <FeatureFlag name={FEATURE_FLAGS.FEATURE_WEB_GLOBAL_GAMES_CREATE_POST}>
        <FeatureFlagVariant flagState>
          <EFSelectGamesField
            selectMultiple={false}
            buttonText="Tag a Game"
            onChange={gameList => {
              analytics.track("CREATE_POST_ACTION_BUTTON", { action: "Tag Game" });
              gameList.length === 0 ? onTaggedGameIdChange("") : onTaggedGameIdChange(gameList[0]);
            }}
            value={taggedGameId}
            showGameChipList={false}
          />
        </FeatureFlagVariant>
      </FeatureFlag>
      <EFGiphySearch
        renderCustomTrigger={onTriggerClick => (
          <EFPillButton
            fullWidth
            buttonType="button"
            shadowTheme="small"
            colorTheme="light"
            fontWeightTheme="normal"
            icon={faGif}
            text="Add GIF"
            width="fullWidth"
            size="large"
            disabled={areMediaActionsDisabled}
            alignContent="left"
            onClick={event => {
              analytics.track("CREATE_POST_ACTION_BUTTON", { action: "Add GIF" });
              onTriggerClick(event);
            }}
          />
        )}
        disabled={areMediaActionsDisabled}
        onGifSelected={onGifSelected}
        placement="top"
      />
      <AddTwitchClips
        onTwitchClipSelected={onTwitchClipSelected}
        isDisabled={areMediaActionsDisabled || isTwitchClipsDisabled}
      />
      {/* TODO temporarily disabling to prevent mobile crashes */}
      {/*<AddYoutubeClips*/}
      {/*  onYoutubeClipSelected={onYoutubeClipSelected}*/}
      {/*  isDisabled={areMediaActionsDisabled || isYoutubeClipsDisabled}*/}
      {/*/>*/}
    </div>
  );
};

export default PostActions;
