import { useRouter } from "next/router";
import { faYoutube } from "@fortawesome/free-brands-svg-icons";
import { useSelector } from "react-redux";
import { useState } from "react";

import EFPillButton from "../../../../Buttons/EFPillButton/EFPillButton";

import EFPrimaryModal from "../../../EFPrimaryModal/EFPrimaryModal";
import EFAlert from "../../../../EFAlert/EFAlert";
import YoutubeClips from "../../../../YoutubeClips/YoutubeClips";

const AddYoutubeClips = ({ isDisabled, onYoutubeClipSelected }) => {
  const currentUser = useSelector(state => state.user.currentUser);
  const isYoutubeAcccountLinked = useSelector(state => state.user.currentUser.googleVerified);
  const Router = useRouter();
  const [showClips, setShowClips] = useState(false);

  const closeModalHandler = () => {
    analytics.track("CREATE_POST_MODAL_CLOSE");
    setShowClips(false);
  };

  const showAlert = () => {
    const onConfirm = () => Router.push("/settings/external_accounts");
    const title = "Link Youtube Account";
    const message =
      "To add Youtube Clips, a Youtube Account must be linked \n Would you like to leave this page to link you Youtube account?";

    EFAlert(title, message, "Yes", "No", onConfirm);
  };

  const showClipModal = () => {
    if (!isYoutubeAcccountLinked) {
      showAlert();
    } else {
      setShowClips(true);
    }
  };

  return (
    <>
      <EFPillButton
        buttonType="button"
        shadowTheme="small"
        colorTheme="light"
        fontWeightTheme="normal"
        icon={faYoutube}
        text="Add YouTube video"
        width="fullWidth"
        size="large"
        disabled={isDisabled}
        alignContent="left"
        onClick={showClipModal}
      />
      {showClips && (
        <EFPrimaryModal
          isOpen
          onOpen={() => {
            analytics.track("YOUTUBE_ADD_CLIP_MODAL_OPENED");
          }}
          onClose={closeModalHandler}
          displayCloseButton
          allowBackgroundClickClose={false}
          title="Select YouTube video"
          widthTheme="medium"
        >
          <YoutubeClips
            onYoutubeClipSelected={onYoutubeClipSelected}
            ownerKind="users"
            owner={currentUser._id}
            page={1}
            limit={10}
            showClips={setShowClips}
          />
        </EFPrimaryModal>
      )}
    </>
  );
};

export default AddYoutubeClips;
