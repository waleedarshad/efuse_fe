import { faDiscord, faTwitter } from "@fortawesome/free-brands-svg-icons";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import PlatformToggle from "./PlatformToggle";
import Style from "./ShareWithPlatforms.module.scss";

const getSharingPlatforms = userSettings => {
  return [
    {
      name: "twitter",
      icon: faTwitter,
      isAccountLinked: userSettings.twitterVerified,
      accountLinkModal: {
        title: "Link Twitter Account",
        body:
          "To share to Twitter, a Twitter account must be linked. Would you like to leave this page to link your Twitter account?"
      }
    },
    {
      name: "discord",
      icon: faDiscord,
      isAccountLinked: userSettings.discordVerified && userSettings.discordWebhooks,
      accountLinkModal: {
        title: "Please link your account and add a webhook to share your post to Discord.",
        body: "Would you like to leave this post to link your Discord account and add a webhook?"
      }
    }
  ];
};

const ShareWithPlatforms = ({ onPlatformChange, disableSharing }) => {
  const [shareList, setShareList] = useState([]);
  const userSettings = useSelector(state => state.user.currentUser);

  useEffect(() => {
    analytics.track("CREATE_POST_SHARE_WITH", { platforms: shareList });
    onPlatformChange(shareList);
  }, [shareList.length]);

  const onChange = (isSelected, platform) => {
    if (isSelected) {
      setShareList([...shareList, platform]);
    } else {
      setShareList(shareList.filter(p => p !== platform));
    }
  };

  const content = getSharingPlatforms(userSettings);

  return (
    <>
      {content.map(platform => (
        <span key={platform.name} className={Style.toggle}>
          <PlatformToggle
            onChange={isSelected => onChange(isSelected, platform.name)}
            icon={platform.icon}
            isAccountLinked={platform.isAccountLinked}
            accountLinkModal={platform.accountLinkModal}
            isDisabled={disableSharing}
          />
        </span>
      ))}
    </>
  );
};

export default ShareWithPlatforms;
