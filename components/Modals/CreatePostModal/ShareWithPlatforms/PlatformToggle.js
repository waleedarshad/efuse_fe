import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import EFCircleIconButton from "../../../Buttons/EFCircleIconButton/EFCircleIconButton";
import ConfirmAlert from "../../../ConfirmAlert/ConfirmAlert";

const PlatformToggle = ({ onChange, icon, isAccountLinked, accountLinkModal, isDisabled }) => {
  const [isSelected, setIsSelected] = useState(false);
  const Router = useRouter();

  const { title, body } = accountLinkModal;

  useEffect(() => {
    onChange(isSelected);
  }, [isSelected]);

  if (isAccountLinked) {
    return (
      <EFCircleIconButton
        onClick={() => setIsSelected(!isSelected)}
        buttonType="button"
        icon={icon}
        shadowTheme="small"
        colorTheme={isSelected ? "dark" : "light"}
        size="medium"
        disabled={isDisabled}
      />
    );
  }

  return (
    <ConfirmAlert title={title} message={body} onYes={() => Router.push("/settings/external_accounts")}>
      <EFCircleIconButton
        buttonType="button"
        icon={icon}
        shadowTheme="small"
        colorTheme="light"
        size="medium"
        disabled={isDisabled}
      />
    </ConfirmAlert>
  );
};

export default PlatformToggle;
