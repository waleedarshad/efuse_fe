import { useSelector } from "react-redux";
import EFUserBadge from "../../../EFUserBadge/EFUserBadge";

const CurrentUserBadge = () => {
  const currentUser = useSelector(state => state.auth.currentUser);

  return (
    <EFUserBadge
      profilePictureUrl={currentUser.profilePicture.url}
      fullname={currentUser.name}
      username={currentUser.username}
    />
  );
};

export default CurrentUserBadge;
