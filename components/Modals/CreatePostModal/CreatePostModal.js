import CreatePostContent from "./CreatePostContent";
import EFPrimaryModal from "../EFPrimaryModal/EFPrimaryModal";

const CreatePostModal = ({ onClose, isScheduleOnly }) => {
  const closeModalHandler = () => {
    analytics.track("CREATE_POST_MODAL_CLOSE");
    onClose();
  };

  return (
    <EFPrimaryModal isOpen onClose={closeModalHandler} displayCloseButton allowBackgroundClickClose={false}>
      <CreatePostContent onFormSubmitted={onClose} isScheduleOnly={isScheduleOnly} />
    </EFPrimaryModal>
  );
};

export default CreatePostModal;
