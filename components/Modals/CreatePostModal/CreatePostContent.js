import isEmpty from "lodash/isEmpty";
import { useEffect, useState } from "react";
import { confirmAlert } from "react-confirm-alert";
import { useDispatch } from "react-redux";
import styled from "styled-components";
import { createPost, createSchedulePost, updatePost } from "../../../store/actions/feedActions";
import CreatePostButtonWrapper from "./CreatePostButtonWrapper";
import EFWizard from "../../EFWizard/EFWizard";
import { EFPrimaryModalHeader } from "../EFPrimaryModal/EFPrimaryModal";
import DisplayMedia from "../../DirectUpload/DisplayMedia/DisplayMedia";
import PostActions from "./PostActions/PostActions";
import PosterDropdown from "./PosterDropdown/PosterDropdown";
import PostTextarea from "./PostTextarea/PostTextarea";
import ScheduledPosts from "./ScheduledPosts/ScheduledPosts";
import ShareWith from "./ShareWith/ShareWith";
import WithUploadContext from "../../DirectUpload/UploadContext";
import colors from "../../../styles/sharedStyledComponents/colors";

const CreatePostContent = ({ onFormSubmitted, isPostEditing, postToEdit, isLinkSharing, postLink, isScheduleOnly }) => {
  const dispatch = useDispatch();

  const [posterInfo, setPosterInfo] = useState({});
  const [postText, setPostText] = useState("");
  const [mentionsList, setMentionsList] = useState([]);
  const [sharingPlatforms, setSharingPlatforms] = useState([]);
  const [uploadedFile, setUploadedFile] = useState({});
  const [selectedGif, setSelectedGif] = useState({});
  const [scheduledDate, setScheduledDate] = useState([]);
  const [twitchClip, setTwitchClip] = useState({});
  const [youtubeClip, setYoutubeClip] = useState({});
  const [taggedGameId, setTaggedGameId] = useState("");

  useEffect(() => {
    if (isPostEditing) {
      const post = postToEdit;
      const mediaFile = post?.media[0];
      if (post.text) {
        setPostText(post.text);
      }

      if (post.mentions) {
        setMentionsList(post.mentions);
      }

      setPosterInfo(post.author || {});

      if (mediaFile) {
        setUploadedFile({ ...mediaFile.file, edit: true });
      }
    }
  }, [isPostEditing]);

  useEffect(() => {
    if (postLink) {
      setPostText(postLink);
    }
  }, [isLinkSharing]);

  const clearInputs = () => {
    setPostText("");
    setMentionsList([]);
    setSharingPlatforms([]);
    setUploadedFile({});
    setSelectedGif({});
    setScheduledDate([]);
    setTwitchClip({});
    setYoutubeClip({});
    setTaggedGameId("");
  };

  const createFeedPost = postData => {
    if (isSchedulingPost()) {
      dispatch(createSchedulePost(postData));
    } else {
      dispatch(createPost(postData));
    }
  };

  const onFormSubmit = () => {
    if (!isEmpty(selectedGif)) {
      delete selectedGif.images["480w_still"];
    }

    let postData = {
      text: postText.trim(),
      file: uploadedFile,
      giphy: selectedGif,
      mentions: mentionsList,
      kindId: posterInfo.id,
      kind: posterInfo.type,
      verified: posterInfo.verified,
      crossPosts: sharingPlatforms,
      platform: "web"
    };

    if (twitchClip.id) {
      postData = { ...postData, twitchClip: twitchClip.id };
    }

    if (youtubeClip._id) {
      postData = { ...postData, youtubeVideo: youtubeClip._id };
    }

    if (taggedGameId) {
      postData = { ...postData, associatedGame: taggedGameId };
    }

    postData = isPostEditing ? buildEditPostData(postData) : buildNewPostData(postData);

    if (isValidPost(postData)) {
      if (isPostEditing) {
        dispatch(updatePost(postToEdit.feedId, postData));
      } else {
        createFeedPost(postData);
      }

      setTimeout(() => {
        clearInputs();
        onFormSubmitted();
      }, 100);
    }
  };

  const isValidPost = postData => {
    let isValid = true;

    if (sharingPlatforms.includes("twitter") && postData.text.length > 280) {
      confirmAlert({
        message: "Cannot post to Twitter with over 280 characters",
        buttons: [
          {
            label: "OK"
          }
        ]
      });
      isValid = false;
    }

    return isValid;
  };

  const buildNewPostData = postData => {
    const newPostData = { ...postData };

    if (isSchedulingPost()) {
      // eslint-disable-next-line prefer-destructuring
      newPostData.scheduledAt = scheduledDate[0];
      newPostData.media = undefined;
    }

    return newPostData;
  };

  const buildEditPostData = postData => {
    const editPostData = {
      parentFeedId: postToEdit.parentFeedId,
      text: postData.text,
      mentions: postData.mentions,
      associatedGame: postData.associatedGame,
      youtubeVideo: postData.youtubeVideo,
      giphy: postData.giphy && postData.giphy?.type === "gif" ? postData.giphy : undefined
    };

    if (postData.file?.url) {
      editPostData.media = [{ ...postData.file, edit: undefined, __typename: undefined }];
    } else if (!isEmpty(postData.giphy) && postData.giphy?.contentType === "image/gif") {
      editPostData.media = [{ ...postData.giphy, edit: undefined, __typename: undefined }];
    } else {
      editPostData.media = [];
    }

    return editPostData;
  };

  const isSchedulingPost = () => scheduledDate.length > 0;

  const isPostingAsOrganization = posterInfo.type === "organizations";
  const isMediaUploaded = !!uploadedFile.url || !!selectedGif.url || !!twitchClip._id || !!youtubeClip._id;
  const canFormBeSubmitted = (!!postText.trim() || isMediaUploaded) && (!isScheduleOnly || !!scheduledDate[0]);

  useEffect(() => {
    if (isPostingAsOrganization) {
      setTwitchClip({});
      setSharingPlatforms([]);
    }
  }, [isPostingAsOrganization]);

  const getPostInfo = post => {
    if (post.author) {
      const { kindId, kind, author } = post;
      const { name, verified, username } = author;
      const image = author?.profileImage;

      return { id: kindId, verified, name, username, type: kind, image };
    }
    return null;
  };

  return (
    <WithUploadContext>
      <EFWizard>
        <>
          <EFPrimaryModalHeader title={`${isScheduleOnly ? "Schedule" : "Create"} Post`} />
          <PosterDropdown
            defaultValue={isPostEditing ? getPostInfo(postToEdit) : null}
            onPosterSelected={poster => {
              analytics.track("CREATE_POST_SET_POSTER", { timelineableType: poster.type });
              setPosterInfo(poster);
            }}
            disabled={isPostEditing}
          />
          <PostTextarea
            postText={postText}
            onPostTextUpdate={setPostText}
            onMentionsListUpdate={setMentionsList}
            currentPostScheduledDate={scheduledDate[0]}
            clearCurrentPostScheduledDate={() => setScheduledDate([])}
            displayScheduleInfo={!isPostEditing}
          />
          <DisplayMedia
            file={uploadedFile}
            gif={selectedGif}
            clearGif={() => setSelectedGif({})}
            twitchClip={twitchClip}
            clearTwitchClip={() => setTwitchClip({})}
            clearUploadedFile={setUploadedFile}
            youtubeClip={youtubeClip}
            clearYoutubeClip={() => setYoutubeClip({})}
          />
          {isScheduleOnly && !scheduledDate[0] && (
            <ScheduleOnlyWarningText>Please select a time for this post</ScheduleOnlyWarningText>
          )}
          <ShareWith
            onSharingPlatformChange={setSharingPlatforms}
            disablePlatformSharing={isPostingAsOrganization}
            taggedGameId={taggedGameId}
            removeTaggedGameId={() => setTaggedGameId("")}
          />
          <PostActions
            onFileUpload={setUploadedFile}
            onGifSelected={setSelectedGif}
            isMediaUploaded={isMediaUploaded}
            onScheduledDateUpdated={setScheduledDate}
            onTwitchClipSelected={setTwitchClip}
            onYoutubeClipSelected={setYoutubeClip}
            isTwitchClipsDisabled={isPostingAsOrganization}
            isScheduleDisabled={isPostEditing}
            taggedGameId={taggedGameId}
            onTaggedGameIdChange={setTaggedGameId}
          />
          <CreatePostButtonWrapper
            disabled={!canFormBeSubmitted}
            onClick={() => {
              analytics.track("CREATE_POST_SUBMIT");
              onFormSubmit();
            }}
          />
        </>
        <ScheduledPosts />
      </EFWizard>
    </WithUploadContext>
  );
};

const ScheduleOnlyWarningText = styled.p`
  font-family: Poppins, sans-serif;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 20px;
  color: ${colors.black};
`;

export default CreatePostContent;
