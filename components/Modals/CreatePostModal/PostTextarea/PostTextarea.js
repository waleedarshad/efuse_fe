import isEmpty from "lodash/isEmpty";
import { Mention, MentionsInput } from "react-mentions";
import { useDispatch, useSelector } from "react-redux";

import { withCdn } from "../../../../common/utils";
import { setMentionInfo } from "../../../../helpers/GeneralHelper";
import { globalSearchFollowersToMention } from "../../../../store/actions/followerActions";
import SchedulingInfo from "../SchedulingInfo/SchedulingInfo";
import Style from "./PostTextarea.module.scss";

const PostTextarea = ({
  postText,
  onPostTextUpdate,
  onMentionsListUpdate,
  currentPostScheduledDate,
  clearCurrentPostScheduledDate,
  displayScheduleInfo
}) => {
  const dispatch = useDispatch();
  const followersToMention = useSelector(state => state.followers.mentionableFollowers);

  const onChange = (event, newValue, newPlainTextValue, mentions) => {
    const updatedMentions = isEmpty(mentions) ? [] : setMentionInfo(mentions, "id");

    onMentionsListUpdate(updatedMentions);
    onPostTextUpdate(newValue);
  };

  const searchFollowersToMention = (query, callback) => {
    dispatch(globalSearchFollowersToMention(query));
    callback(followersToMention);
  };

  // this reduce is necessary in order to use the <MentionsInput/> "classNames" prop AND variables
  // in scss AND css modules
  const mentionWrapperStyles = Object.entries(Style).reduce((acc, [key, val]) => {
    return key.includes("mentionWrapper") ? { ...acc, [key]: val } : acc;
  }, {});

  return (
    <div className={Style.postTextarea}>
      {displayScheduleInfo && (
        <SchedulingInfo
          currentPostScheduledDate={currentPostScheduledDate}
          clearCurrentPostScheduledDate={clearCurrentPostScheduledDate}
        />
      )}
      <MentionsInput
        value={postText}
        placeholder="What&rsquo;s on your mind?"
        onChange={onChange}
        classNames={mentionWrapperStyles}
        rows="3"
        name="content"
        maxLength={1500}
        autoFocus
        allowSpaceInQuery
      >
        <Mention
          trigger="@"
          data={searchFollowersToMention}
          className={Style.mentionedFriend}
          markup="@[__display__](__id__)"
          displayTransform={(id, display) => `@${display}`}
          renderSuggestion={entry => <MentionSuggestion suggestedMention={entry} />}
        />
      </MentionsInput>
    </div>
  );
};

PostTextarea.defaultProps = {
  displayScheduleInfo: true
};

export default PostTextarea;

const MentionSuggestion = ({ suggestedMention }) => {
  const { profilePicture, display, type, fullName, verified } = suggestedMention;

  return (
    <span>
      <img className={Style.suggestionProfileImg} alt="profile-pic" src={profilePicture} />
      {display} {type === "user" && `(${fullName})`} &nbsp;
      {verified && (
        <img className={Style.suggestionVerifiedImg} alt="verified" src={withCdn("/static/images/efuse_verify.png")} />
      )}
    </span>
  );
};
