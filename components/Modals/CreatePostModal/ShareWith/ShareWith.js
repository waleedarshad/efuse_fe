import FEATURE_FLAGS from "../../../../common/featureFlags";
import getFoundGamesHook from "../../../../hooks/getHooks/getFoundGamesHook";
import EFGamesChip from "../../../EFGamesChip/EFGamesChip";
import FeatureFlag from "../../../General/FeatureFlag/FeatureFlag";
import FeatureFlagVariant from "../../../General/FeatureFlag/FeatureFlagVariant/FeatureFlagVariant";
import ShareWithPlatforms from "../ShareWithPlatforms/ShareWithPlatforms";
import Style from "./ShareWith.module.scss";

const ShareWith = ({ onSharingPlatformChange, disablePlatformSharing, taggedGameId, removeTaggedGameId }) => {
  const selectedGame = getFoundGamesHook([taggedGameId]);

  const gameInfo = selectedGame?.length > 0 ? selectedGame[0] : undefined;

  return (
    <div className={Style.shareWithContainer}>
      <h5 className={Style.title}>SHARE WITH</h5>
      <div className={Style.shareWithList}>
        <FeatureFlag name={FEATURE_FLAGS.KILL_SWITCH_DISABLE_CROSS_POSTING_CREATE_POST_MODAL}>
          <FeatureFlagVariant flagState={false}>
            <ShareWithPlatforms onPlatformChange={onSharingPlatformChange} disableSharing={disablePlatformSharing} />
          </FeatureFlagVariant>
        </FeatureFlag>
        {gameInfo && (
          <EFGamesChip.Removable image={gameInfo.gameImage.url} title={gameInfo.title} onRemove={removeTaggedGameId} />
        )}
      </div>
    </div>
  );
};

export default ShareWith;
