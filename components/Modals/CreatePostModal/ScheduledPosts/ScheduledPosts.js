import { useContext, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import SimpleBar from "simplebar-react";
import "simplebar-react/dist/simplebar.min.css";
import styled from "styled-components";
import { getScheduledPosts } from "../../../../store/actions/userActions";
import { EFWizardContext } from "../../../EFWizard/EFWizard";
import { EFPrimaryModalHeader } from "../../EFPrimaryModal/EFPrimaryModal";
import PosterDropdown from "../PosterDropdown/PosterDropdown";
import ScheduledPostPreview from "./ScheduledPostPreview/ScheduledPostPreview";
import Style from "./ScheduledPosts.module.scss";

const CustomScrollBar = styled(SimpleBar)`
  max-height: 420px;
  .simplebar-vertical {
    right: -20px;
  }
`;

const ScheduledPosts = () => {
  const dispatch = useDispatch();

  const { scheduledPosts } = useSelector(state => state.user);
  const { goPrevStep } = useContext(EFWizardContext);

  const [poster, setPoster] = useState({});

  useEffect(() => {
    dispatch(getScheduledPosts());
  }, []);

  const postsAssociatedWithPoster = scheduledPosts.filter(post => post.timelineable === poster.id);

  return (
    <div className={Style.scheduledPosts}>
      <EFPrimaryModalHeader title="Scheduled Posts" onBackClick={goPrevStep} />
      <PosterDropdown
        onPosterSelected={posterInfo => {
          analytics.track("CREATE_POST_SET_SCHEDULED_POSTS_POSTER", { timelineableType: posterInfo.type });
          setPoster(posterInfo);
        }}
      />

      <CustomScrollBar>
        {postsAssociatedWithPoster.map(post => (
          <ScheduledPostPreview post={post} key={post._id} />
        ))}
      </CustomScrollBar>
    </div>
  );
};

export default ScheduledPosts;
