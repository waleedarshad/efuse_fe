import uniqueId from "lodash/uniqueId";
import { Fragment } from "react";
import Linkify from "react-linkify";
import { buildMentionTag } from "../../../../../helpers/PostHelper";

const ParsedPostContent = ({ content, mentions = [], renderMention, renderLinkedContent }) => {
  const renderWithLinkify = text => linkifyContent(text, renderLinkedContent);

  if (mentions.length === 0) {
    return renderWithLinkify(content);
  }

  const mentionsInfo = mentions.reduce((acc, mention) => {
    const mentionTag = buildMentionTag(mention);

    return { ...acc, [mentionTag]: { ...mention, type: mention.type || "user" } };
  }, {});

  const mentionIds = Object.keys(mentionsInfo);

  const genericLookupMentionPattern = /(@\[.+?\]\([\w]+-(?:user|org)\)|@\[.+?\]\([\w]+\))/;

  return content
    .split(genericLookupMentionPattern)
    .map(item => (mentionIds.includes(item) ? renderMention(mentionsInfo[item]) : renderWithLinkify(item)))
    .map(element => <Fragment key={uniqueId()}>{element}</Fragment>);
};

const linkifyContent = (contentText, renderLinkedContent) => (
  <Linkify
    componentDecorator={(href, linkedText) => (
      <Fragment key={uniqueId()}>{renderLinkedContent(href, linkedText)}</Fragment>
    )}
  >
    {contentText}
  </Linkify>
);

export default ParsedPostContent;
