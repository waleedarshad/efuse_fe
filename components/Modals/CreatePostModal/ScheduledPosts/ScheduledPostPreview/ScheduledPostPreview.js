import moment from "moment";
import { useDispatch } from "react-redux";
import { faTrash } from "@fortawesome/pro-regular-svg-icons";

import { buildMentionHref } from "../../../../../helpers/PostHelper";
import { deleteScheduledPost } from "../../../../../store/actions/userActions";
import EFCircleIconButton from "../../../../Buttons/EFCircleIconButton/EFCircleIconButton";
import ParsedPostContent from "../ParsedPostContent/ParsedPostContent";
import Style from "./ScheduledPostPreview.module.scss";

const ScheduledPostPreview = ({ post }) => {
  const dateTimeFormat = "MMM Do @h:mm A";

  const dispatch = useDispatch();

  return (
    <div className={Style.postPreview}>
      <div className={Style.post}>
        <h3 className={Style.scheduledAt}>{moment(post.scheduledAt).format(dateTimeFormat)}</h3>
        <p className={Style.createdAt}>Created on {moment(post.createdAt).format(dateTimeFormat)}</p>
        <p className={Style.content}>
          <ParsedPostContent
            content={post.content}
            mentions={post.mentions}
            renderMention={mention => <a href={buildMentionHref(mention)}>{mention.display}</a>}
            renderLinkedContent={(href, text) => (
              <a target="_blank" rel="noopener noreferrer nofollow" href={href}>
                {text}
              </a>
            )}
          />
        </p>
      </div>
      <EFCircleIconButton
        buttonType="button"
        icon={faTrash}
        colorTheme="light"
        onClick={() => {
          analytics.track("CREATE_POST_SCHEDULED_POSTS_REMOVE");
          dispatch(deleteScheduledPost(post._id));
        }}
      />
    </div>
  );
};

export default ScheduledPostPreview;
