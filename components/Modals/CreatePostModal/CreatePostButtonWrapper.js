import React, { useContext } from "react";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";
import { UploadContext } from "../../DirectUpload/UploadContext";

const CreatePostButtonWrapper = ({ disabled, onClick }) => {
  const { isUploading } = useContext(UploadContext);

  return (
    <EFRectangleButton
      buttonType="button"
      text="post"
      width="fullWidth"
      shadowTheme="medium"
      fontCaseTheme="upperCase"
      disabled={disabled || isUploading}
      onClick={onClick}
    />
  );
};

export default CreatePostButtonWrapper;
