import moment from "moment";
import { useContext, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getScheduledPosts } from "../../../../store/actions/userActions";
import EFChip from "../../../EFChip/EFChip";
import { EFWizardContext } from "../../../EFWizard/EFWizard";
import Style from "./SchedulingInfo.module.scss";

const SchedulingInfo = ({ currentPostScheduledDate, clearCurrentPostScheduledDate }) => {
  const dispatch = useDispatch();

  const { scheduledPosts } = useSelector(state => state.user);

  const { goNextStep } = useContext(EFWizardContext);

  useEffect(() => {
    dispatch(getScheduledPosts());
  }, []);

  const numberScheduledPosts = scheduledPosts.length;

  if (!currentPostScheduledDate && numberScheduledPosts === 0) {
    return <></>;
  }

  const formattedDate = moment(currentPostScheduledDate).format("MMM D, YYYY @h:mm A");

  return (
    <div className={Style.schedulingInfo}>
      {numberScheduledPosts > 0 && (
        <span className={Style.scheduledChip}>
          <EFChip.Info
            content="Scheduled"
            badgeText={numberScheduledPosts}
            onClick={() => {
              analytics.track("CREATE_POST_SCHEDULED_POSTS");
              goNextStep();
            }}
            colorTheme="light"
          />
        </span>
      )}
      {currentPostScheduledDate && (
        <EFChip.Removable
          content={`Scheduled ${formattedDate}`}
          onRemove={clearCurrentPostScheduledDate}
          colorTheme="light"
        />
      )}
    </div>
  );
};

export default SchedulingInfo;
