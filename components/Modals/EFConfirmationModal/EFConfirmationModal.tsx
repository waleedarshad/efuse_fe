import React from "react";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";
import EFPrimaryModal from "../EFPrimaryModal/EFPrimaryModal";
import Style from "./EFConfirmationModal.module.scss";

interface EFConfirmationModalProps {
  cancelText?: string;
  confirmText?: string;
  title?: string;
  message?: string;
  isOpen: Boolean;
  displayCloseButton: Boolean;
  onClose: () => void;
  onConfirm: () => void;
}

const EFConfirmationModal: React.FC<EFConfirmationModalProps> = ({
  cancelText,
  confirmText,
  title,
  message,
  isOpen,
  onClose,
  onConfirm,
  displayCloseButton
}) => {
  const confirmSubmit = () => {
    onConfirm();
    closeModal();
  };

  const closeModal = () => {
    if (onClose) {
      onClose();
    }
  };

  return (
    <EFPrimaryModal
      title={title}
      isOpen={isOpen}
      allowBackgroundClickClose
      displayCloseButton={displayCloseButton}
      onClose={onClose}
      widthTheme="extraSmall"
    >
      <div className={Style.message}>{message}</div>
      <div className={Style.buttonWrapper}>
        <div className={Style.cancelButtonWrapper}>
          <EFRectangleButton
            width="fullWidth"
            shadowTheme="none"
            colorTheme="white"
            text={cancelText}
            buttonType="button"
            onClick={() => closeModal()}
          />
        </div>
        <div className={Style.confirmButtonWrapper}>
          <EFRectangleButton
            width="fullWidth"
            shadowTheme="none"
            colorTheme="azul"
            text={confirmText}
            onClick={() => confirmSubmit()}
          />
        </div>
      </div>
    </EFPrimaryModal>
  );
};

EFConfirmationModal.defaultProps = {
  cancelText: "Cancel",
  confirmText: "Confirm",
  title: null,
  message: "Are you sure you would like to perform this action?",
  isOpen: false
};

export default EFConfirmationModal;
