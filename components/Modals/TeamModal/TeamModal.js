import React, { useEffect, useState } from "react";
import EFPrimaryModal from "../EFPrimaryModal/EFPrimaryModal";
import Style from "./TeamModal.module.scss";
import TeamForm from "../../TeamForm/TeamForm";
import SelectUserForm from "../../SelectUserForm/SelectUserForm";

/**
 * onSubmit output:
 *
 * team {
 *    id - the team id
 *    userIds - user ids to add as team members
 *    name - the team name
 *    game - the game id for the teams main game
 * }
 *
 */
const TeamModal = ({ isOpen, onClose, onSubmit, team, viewAddTeamMember }) => {
  const [adjustedTeam, setTeam] = useState({
    name: team?.name || null,
    game: team?.game?._id || team?.gameId || null,
    userIds: []
  });
  const [viewAddMembers, setViewAddMembers] = useState(false);
  const [modalTitle, setModalTitle] = useState("Create Team");

  useEffect(() => {
    if (team) {
      setModalTitle("Edit Team");
    }
  }, [team]);

  // clear state values
  useEffect(() => {
    setViewAddMembers(false);
    setTeam({});
  }, [onClose]);

  useEffect(() => {
    setViewAddMembers(viewAddTeamMember);
  }, [viewAddTeamMember]);

  const createTeamSubmit = submittedTeam => {
    if (onSubmit) {
      onSubmit(submittedTeam);
    }
    if (onClose) {
      onClose();
    }
  };

  const addUsersToTeamAndSubmit = users => {
    const foundUserIds = users.map(user => user.value);
    const teamToSubmit = { ...adjustedTeam, userIds: foundUserIds };
    createTeamSubmit(teamToSubmit);
  };

  const updateTeamAndViewAddMembers = createTeam => {
    setTeam(createTeam);
    setViewAddMembers(true);
  };

  return (
    <EFPrimaryModal
      title={!viewAddMembers ? modalTitle : "Add Team Members"}
      isOpen={isOpen}
      allowBackgroundClickClose={false}
      displayCloseButton
      onClose={onClose}
      widthTheme="medium"
    >
      {!viewAddMembers && (
        <div className={Style.formHeight}>
          <TeamForm
            onCreateTeam={createTeamSubmit}
            onAddMember={updateTeamAndViewAddMembers}
            onClose={onClose}
            team={team}
          />
        </div>
      )}
      {viewAddMembers && (
        <SelectUserForm
          placeholder="Search..."
          colorTheme="azul"
          shadowTheme="none"
          submitFunction={users => addUsersToTeamAndSubmit(users)}
        />
      )}
    </EFPrimaryModal>
  );
};

export default TeamModal;
