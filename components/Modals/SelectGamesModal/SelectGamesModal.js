import React, { useState } from "react";
import EFGameSelect from "../../EFGameSelect/EFGameSelect";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";
import EFPrimaryModal from "../EFPrimaryModal/EFPrimaryModal";
import Style from "./SelectGamesModal.module.scss";

// value accepts an array of game ids to be autoselected (for edit)
const SelectGamesModal = ({ isOpen, onClose, onChange, value, selectMultiple }) => {
  const [selectedGames, setSelectedGames] = useState([]);

  const saveGamesSubmit = () => {
    onChange(selectedGames.map(game => game._id));
    onClose && onClose();
  };

  return (
    <EFPrimaryModal
      title="Select Games"
      isOpen={isOpen}
      allowBackgroundClickClose={false}
      displayCloseButton
      onClose={onClose}
    >
      <EFGameSelect
        onSelect={games => setSelectedGames(games)}
        selectMultiple={selectMultiple}
        preselectedGameIds={value}
      />
      <div className={Style.buttonWrapper}>
        <EFRectangleButton colorTheme="primary" text="Save" buttonType="submit" onClick={saveGamesSubmit} />
      </div>
    </EFPrimaryModal>
  );
};

SelectGamesModal.defaultProps = {
  selectMultiple: true
};

export default SelectGamesModal;
