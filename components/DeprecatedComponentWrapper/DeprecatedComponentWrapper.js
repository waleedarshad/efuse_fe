import React from "react";
import getConfig from "next/config";

const deprecated = (DeprecatedComponent, msg = "") => props => {
  const { publicRuntimeConfig } = getConfig();
  if (publicRuntimeConfig.environment !== "production") {
    // eslint-disable-next-line no-console
    console.warn(`${DeprecatedComponent.name} is deprecated: `, msg);
  }

  // eslint-disable-next-line react/jsx-props-no-spreading
  return <DeprecatedComponent {...props} />;
};

export default deprecated;
