import React from "react";
import { mount } from "enzyme";
import deprecated from "./DeprecatedComponentWrapper";

console.warn = jest.fn();
const ComponentToBeDeprecated = () => <div>words</div>;

describe("DeprecatedComponentWrapper", () => {
  beforeEach(() => {
    const DeprecatedComponent = deprecated(ComponentToBeDeprecated, "bad stuff");
    mount(<DeprecatedComponent />);
  });

  describe("if it is not in production", () => {
    it("prints a warning to the console", () => {
      expect(console.warn).toHaveBeenLastCalledWith("ComponentToBeDeprecated is deprecated: ", "bad stuff");
    });
  });
});
