import React from "react";
import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck } from "@fortawesome/pro-solid-svg-icons";
import Style from "./EFGameCard.module.scss";
import EFImage from "../../EFImage/EFImage";

const EFGameCard = ({ gameImage, isActive, size }) => {
  return (
    <div className={`${Style.cardWrapper} ${isActive && Style.active} ${Style[size]}`}>
      {isActive && <FontAwesomeIcon icon={faCheck} className={Style.icon} />}
      <EFImage src={gameImage} className={Style.gameImage} />
    </div>
  );
};

EFGameCard.defaultProps = {
  isActive: false,
  size: "small"
};

EFGameCard.propTypes = {
  isActive: PropTypes.bool,
  size: PropTypes.oneOf(["small", "medium"])
};

export default EFGameCard;
