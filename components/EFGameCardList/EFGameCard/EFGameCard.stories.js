import React from "react";
import EFGameCard from "./EFGameCard";

export default {
  title: "Games/EFGameCard",
  component: EFGameCard,
  argTypes: {
    gameImage: {
      control: {
        type: "text"
      }
    },
    isActive: {
      control: {
        type: "boolean"
      }
    },
    size: {
      control: {
        type: "select",
        options: ["small", "medium"]
      }
    }
  }
};

// eslint-disable-next-line react/jsx-props-no-spreading
const Story = args => <EFGameCard {...args} />;

export const Default = Story.bind({});
Default.args = {
  gameImage: "https://efuse.s3.amazonaws.com/uploads/games/d9a5432c-5ae7-4d9e-b833-2dce3b1ac4ef.jpg",
  isActive: false,
  size: "small"
};

export const isActive = Story.bind({});
isActive.args = {
  gameImage: "https://efuse.s3.amazonaws.com/uploads/games/d9a5432c-5ae7-4d9e-b833-2dce3b1ac4ef.jpg",
  isActive: true,
  size: "small"
};

export const medium = Story.bind({});
medium.args = {
  gameImage: "https://efuse.s3.amazonaws.com/uploads/games/d9a5432c-5ae7-4d9e-b833-2dce3b1ac4ef.jpg",
  isActive: false,
  size: "medium"
};
