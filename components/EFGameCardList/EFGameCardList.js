import React from "react";

import Style from "./EFGameCardList.module.scss";
import HorizontalScroll from "../HorizontalScroll/HorizontalScroll";
import EFCheckboxGameCard from "../Cards/EFCheckboxGameCard/EFCheckboxGameCard";

const EFGameCardList = ({ games, onGameClick, size, horizontalScroll }) => {
  const gamesList = (
    <div className={`${Style.gamesList} ${horizontalScroll && Style.horizontalScroll}`}>
      {games?.map(game => (
        <div key={game?._id} className={Style.gameWrapper}>
          <EFCheckboxGameCard
            gameImage={game?.gameImage?.url}
            selected={game?.selected}
            onChange={() => onGameClick && onGameClick(game)}
            id={game?._id}
            size={size}
          />
        </div>
      ))}
    </div>
  );

  return horizontalScroll ? (
    <HorizontalScroll customButtonMargin={Style[size]}>{gamesList}</HorizontalScroll>
  ) : (
    gamesList
  );
};

EFGameCardList.defaultProps = {
  size: "small",
  horizontalScroll: false
};

export default EFGameCardList;
