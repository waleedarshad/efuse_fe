import PropTypes from "prop-types";
import Link from "next/link";
import Style from "../FeedFilters.module.scss";

const FeedFilterItem = ({ title, icon, active, href, as, enabled }) => {
  return (
    <>
      {enabled && (
        <Link href={href} as={as}>
          <button type="button" className={`${Style.filterButton} ${Style.recentFilter} ${active && Style.active}`}>
            {title && <span className={Style.btnText}>{title}</span>}
            {icon}
          </button>
        </Link>
      )}
    </>
  );
};

FeedFilterItem.propTypes = {
  title: PropTypes.string.isRequired,
  icon: PropTypes.element.isRequired,
  active: PropTypes.bool.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  href: PropTypes.any.isRequired,
  as: PropTypes.string,
  enabled: PropTypes.bool.isRequired
};

FeedFilterItem.defaultProps = {
  as: ""
};

export default FeedFilterItem;
