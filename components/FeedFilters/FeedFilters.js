import { faBars } from "@fortawesome/pro-regular-svg-icons";
import { faPlus, faUsers } from "@fortawesome/pro-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useState } from "react";
import { useSelector } from "react-redux";
import { isUserLoggedIn } from "../../helpers/AuthHelper";
import FeedAdd from "../FeedAd/FeedAd";
import CreatePostModal from "../Modals/CreatePostModal/CreatePostModal";
import VerifiedIcon from "../VerifiedIcon/VerifiedIcon";
import FeedFilterItem from "./FeedFilterItem/FeedFilterItem";
import Style from "./FeedFilters.module.scss";

const FeedFilters = ({ currentFilter }) => {
  const enableLoungeSection = useSelector(state => state.features.lounge_section);
  const enableVerifiedFilter = useSelector(state => state.features.lounge_verified_filter);
  const loungeRightSquare = useSelector(state => state.features.lounge_right_square);
  const enableCreatePostModal = useSelector(state => state.features.create_post_modal_from_lounge);

  const [openCreatePostModal, setOpenCreatePostModal] = useState(false);

  const launchCreatePostModal = () => {
    if (isUserLoggedIn()) {
      analytics.track("GLOBAL_CREATE_POST_BUTTON");
      analytics.track("CREATE_POST_MODAL_OPEN");
      setOpenCreatePostModal(true);
    }
  };

  const filters = [
    {
      title: "Featured",
      icon: <FontAwesomeIcon icon={faBars} className={Style.recent} />,
      active: currentFilter === "featured",
      enabled: enableLoungeSection,
      href: { pathname: "/lounge/[type]" },
      as: "/lounge/featured"
    },
    {
      title: "Following",
      icon: <FontAwesomeIcon icon={faUsers} className={Style.recent} />,
      active: currentFilter === "following",
      enabled: true,
      href: "/home"
    },
    {
      title: "Verified",
      icon: <VerifiedIcon />,
      active: currentFilter === "verified",
      enabled: enableLoungeSection && enableVerifiedFilter,
      href: { pathname: "/lounge/[type]" },
      as: "/lounge/verified"
    }
  ];
  const renderFilters = filters.map((filter, index) => <FeedFilterItem key={index} {...filter} />);

  return (
    <div className={Style.filters}>
      <p className={Style.filterTitle}>Content Filters</p>
      {renderFilters}
      {enableCreatePostModal && (
        <button type="button" className={Style.createButton} onClick={launchCreatePostModal}>
          Create Post
          <FontAwesomeIcon className={Style.plus} icon={faPlus} />
        </button>
      )}
      {openCreatePostModal && <CreatePostModal onClose={() => setOpenCreatePostModal(false)} />}
      {loungeRightSquare && (
        <div className={Style.hideOnMobile}>
          <FeedAdd width={255} height={255} adID="178881" setID="432740" />
        </div>
      )}
    </div>
  );
};

export default FeedFilters;
