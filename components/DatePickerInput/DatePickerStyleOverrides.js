import styled from "styled-components";

const DatePickerStyleOverrides = styled.div`
  .SingleDatePickerInput,
  .DateRangePickerInput {
    display: block;
    background: transparent;
  }
  .SingleDatePickerInput__withBorder {
    border-radius: 2px;
    border: none;
  }

  .DateInput {
    display: block;
    width: 100%;
    background: transparent;
  }

  .DateRangePickerInput {
    border: none;
  }
  .SingleDatePicker,
  .DateRangePicker {
    display: block;
    width: 100%;
  }

  .DateRangePickerInput .DateRangePickerInput_arrow,
  .DateInput_fang_1 {
    display: none;
  }
  .DateRangePickerInput .DateInput_1 {
    margin-bottom: 1rem;
  }

  .whiteDatepicker .DateInput {
    background: #ffffff;
  }
  .DateInput_input {
    margin: 0;
    font-family: inherit;
    font-size: inherit;
    line-height: inherit;
    overflow: visible;
    display: block;
    width: 100%;
    font-weight: 500;
    line-height: 1.5;
    background-clip: padding-box;
    border: solid 1px #bcbdca;
    border-radius: 5px;
    transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
    color: #16181a;
    background: transparent;
    font-size: 0.9rem;
    padding: 5px 10px;
    height: calc(1.7em + 1rem + 2px);
    box-shadow: 0 8px 8px -5px rgba(0, 0, 0, 0.1);
  }
  .DateInput_input:hover {
    cursor: pointer;
    box-shadow: 0 3px 3px -2px rgba(0, 0, 0, 0.1);
  }

  #externalDob.DateInput_input {
    background: #ffffff;
    border-radius: 6px;
  }

  @media (prefers-reduced-motion: reduce) {
    .DateInput_input {
      transition: none;
    }
  }
  .DateInput_input::-ms-expand {
    background-color: transparent;
    border: 0;
  }
  .DateInput_input:focus {
    color: #737385;
    background-color: transparent;
    border-color: #3a546f;
    outline: 0;
    box-shadow: none;
    border-color: #16181a;
  }
  .DateInput_input::-webkit-input-placeholder {
    color: #737385;
    opacity: 1;
  }
  .DateInput_input::-moz-placeholder {
    color: #737385;
    opacity: 1;
  }
  .DateInput_input:-ms-input-placeholder {
    color: #737385;
    opacity: 1;
  }
  .DateInput_input::-ms-input-placeholder {
    color: #737385;
    opacity: 1;
  }
  .DateInput_input::placeholder {
    color: #737385;
    opacity: 1;
  }
  .DateInput_input:disabled {
    background-color: #e9ecef;
    opacity: 1;
  }
  .DateInput_input:valid {
    background-image: none !important;
    border-color: #3a546f !important;
  }
  .DateInput_input:valid:focus {
    box-shadow: none !important;
  }

  .DateInput_input-lg {
    height: calc(1.5em + 1rem + 2px);
    padding: 0.5rem 1rem;
    font-size: 1.25rem;
    line-height: 1.5;
    border-radius: 0.3rem;
  }

  @media only screen and (max-width: 1366px) {
    .DateInput_input {
      height: calc(1.7em + 1rem + 2px);
      font-size: 0.9rem;
    }
  }
`;

export default DatePickerStyleOverrides;
