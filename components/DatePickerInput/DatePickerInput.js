import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCalendarAlt } from "@fortawesome/pro-regular-svg-icons";
import uniqueId from "lodash/uniqueId";
import PropTypes from "prop-types";
import React, { useState } from "react";
import { FormControl } from "react-bootstrap";
import { SingleDatePicker } from "react-dates";
import "react-dates/initialize";
import "react-dates/lib/css/_datepicker.css";
import { compatibleForDatepicker } from "../../helpers/MomentHelper";
import Style from "./DatePickerInput.module.scss";
import DatePickerStyleOverrides from "./DatePickerStyleOverrides";

const DatePickerInput = ({
  invalid,
  errorMessage,
  date,
  dateHandler,
  direction,
  isOutsideRange,
  renderMonthElement,
  numberOfMonths,
  placeholder
}) => {
  const [isFocused, setIsFocused] = useState(null);

  return (
    <div className={`${Style.dateWrapper} ${invalid && Style.invalid}`}>
      <DatePickerStyleOverrides>
        <SingleDatePicker
          openDirection={direction}
          displayFormat="MM/DD/YYYY"
          date={compatibleForDatepicker(date)}
          onDateChange={dateHandler}
          focused={isFocused}
          onFocusChange={({ focused }) => setIsFocused(focused)}
          id={uniqueId()}
          placeholder={placeholder}
          isOutsideRange={isOutsideRange}
          readOnly
          renderMonthElement={renderMonthElement}
          numberOfMonths={numberOfMonths}
        />
      </DatePickerStyleOverrides>
      <FontAwesomeIcon icon={faCalendarAlt} className={Style.calendarIcon} />
      {invalid && (
        <FormControl.Feedback type="invalid" style={{ display: "block" }}>
          {errorMessage}
        </FormControl.Feedback>
      )}
    </div>
  );
};

DatePickerInput.propTypes = {
  date: PropTypes.string.isRequired,
  dateHandler: PropTypes.func.isRequired,
  numberOfMonths: PropTypes.number,
  direction: PropTypes.string,
  placeholder: PropTypes.string
};

DatePickerInput.defaultProps = {
  direction: "up",
  numberOfMonths: 2,
  placeholder: "MM/DD/YYYY"
};

export default DatePickerInput;
