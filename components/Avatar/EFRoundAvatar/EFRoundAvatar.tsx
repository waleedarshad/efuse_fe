import React from "react";
import styled from "styled-components";

interface EFRoundAvatarProps {
  src: string;
  altText: string;
  size?: string;
}

const EFRoundAvatar: React.FC<EFRoundAvatarProps> = ({ src, altText, size = "30" }) => {
  return <RoundAvatar src={src} alt={altText} size={size} />;
};

const RoundAvatar = styled.img`
  height: ${props => props.size}px;
  width: ${props => props.size}px;
  border-radius: 50%;
`;

export default EFRoundAvatar;
