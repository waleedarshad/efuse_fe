import React, { useEffect } from "react";
import { Container } from "react-bootstrap";

import LoginSignupFlow from "../../DynamicModal/LoginSignupFlow/LoginSignupFlow";
import Error from "../../../pages/_error";
import Style from "./Persona.module.scss";
import { EFHtmlParser } from "../../EFHtmlParser/EFHtmlParser";
import EFImage from "../../EFImage/EFImage";
import BackgroundImage from "./BackgroundImage/BackgroundImage";

const Persona = ({ pageProps }) => {
  useEffect(() => {
    analytics.page("Persona Landing Page");
  }, []);

  if (!pageProps.personaData || pageProps.ssrStatusCode === 400 || pageProps.ssrStatusCode === 422) {
    return <Error statusCode={pageProps.ssrStatusCode || 500} />;
  }

  const pageData = pageProps.personaData;

  return (
    <div>
      <BackgroundImage />
      <div>
        <Container>
          <div className={`row ${Style.logoContainer}`}>
            <EFImage
              src="https://cdn.efuse.gg/uploads/static/landing-page/whiteLogo-optimized.png"
              alt="eFuse logo white"
              className={Style.logoImage}
            />
          </div>
          <div className={`row ${Style.contentContainer}`}>
            <div className="col-lg-6 p-0">
              <div className={Style.textContainer} id="text">
                {pageData?.title && (
                  <h1 className={Style.heading}>
                    <EFHtmlParser>{pageData.title}</EFHtmlParser>
                  </h1>
                )}
                {pageData?.body && (
                  <p className={Style.description}>
                    <EFHtmlParser>{pageData.body}</EFHtmlParser>
                  </p>
                )}
              </div>
              <div className={Style.imageContainer}>
                {pageData?.secondaryImages.length >= 1 && (
                  <div className={Style.image1}>
                    <EFImage src={pageData.secondaryImages[0]} />
                  </div>
                )}

                {pageData?.secondaryImages.length >= 2 && (
                  <div className={Style.image2}>
                    <EFImage src={pageData.secondaryImages[1]} />
                  </div>
                )}
              </div>
            </div>
            <div className="col-lg-6 p-0">
              <div className={Style.signupFormContainer}>
                <LoginSignupFlow
                  signupModalTitle={pageData.modalTitle}
                  view="signup"
                  signupLocationForTracking="persona_page_embed"
                  customRedirect={pageData.signupRedirectURL && pageData.signupRedirectURL}
                />
              </div>
            </div>
          </div>
        </Container>
      </div>
    </div>
  );
};

export default Persona;
