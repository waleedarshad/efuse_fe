import React, { useEffect, useState } from "react";

import EFImage from "../../../EFImage/EFImage";
import Style from "./BackgroundImage.module.scss";

const BackgroundImage = () => {
  const [backgroundImageHeight, setBackgroundImageHeight] = useState(0);

  useEffect(() => {
    setBackgroundImageHeight(document.getElementById("text")?.clientHeight || 500);
  }, []);

  return (
    <EFImage
      src="https://cdn.efuse.gg/static/images/welcome_splash_background.jpg"
      className={Style.backgroundImage}
      height={backgroundImageHeight + 415}
    />
  );
};

export default BackgroundImage;
