import React, { useEffect, useState } from "react";

import { withRouter } from "next/router";
import { connect } from "react-redux";
import { NextSeo } from "next-seo";
import LandingFooter from "./LandingFooter/LandingFooter";

import LandingNav from "../Layouts/LandingNav/LandingNav";
import LandingComponent from "./LandingComponent/LandingComponent";

import DynamicModal from "../DynamicModal/DynamicModal";
import OpportunityTab from "./OpportunityTab/OpportunityTab";
import OrganizationTab from "./OrganizationTab/OrganizationTab";
import HighlightedUsers from "./HighlightedUsers/HighlightedUsers";

import Style from "./LandingPage.module.scss";
import { sendNotification } from "../../helpers/FlashHelper";

const LandingPage = props => {
  const {
    router: {
      query: { token, error, errorDescription }
    }
  } = props;

  const [resetPassword, setResetPassword] = useState(false);

  useEffect(() => {
    analytics.page("Join Landing Page");
    setResetPassword(!!token);
  }, [token]);

  // Handle auth0 errors
  useEffect(() => {
    if (error) {
      sendNotification(errorDescription, "danger", error);
      props.router.replace("/");
    }
  }, [error]);

  return (
    <>
      <NextSeo title="Discover Esports Scholarships and Tournaments | eFuse.gg" />
      <div className={Style.mainContainer}>
        <LandingNav />
        <LandingComponent />
        <OpportunityTab />
        <OrganizationTab />
        <HighlightedUsers />
        <LandingFooter />
      </div>
      {resetPassword && (
        <DynamicModal
          flow="LoginSignup"
          openOnLoad
          startView="reset_password"
          displayCloseButton
          allowBackgroundClickClose={false}
          token={token}
          onCloseModal={() => {
            props.router.push("/login");
          }}
        />
      )}
    </>
  );
};

export default connect(null, {})(withRouter(LandingPage));
