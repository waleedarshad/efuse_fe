import React from "react";

import Style from "./OrganizationTab.module.scss";

import LandingContent from "../../LandingContent/LandingContent";
import DynamicModal from "../../DynamicModal/DynamicModal";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";
import FeatureFlag from "../../General/FeatureFlag/FeatureFlag";
import FeatureFlagVariant from "../../General/FeatureFlag/FeatureFlagVariant/FeatureFlagVariant";
import useDeviceDetect from "../../../hooks/useDeviceDetect";
import FEATURE_FLAGS from "../../../common/featureFlags";
import { LINKS, getOneLink } from "../../../helpers/AppsFlyerHelper";

const OrganizationTab = () => {
  const { isMobile } = useDeviceDetect();
  return (
    <LandingContent
      title="Organizations"
      description="With over 500 elite organizations, eFuse is the place to go to grow your organization and find the best talent from across the globe."
      imageUrl="https://cdn.efuse.gg/uploads/static/landing-page/organizations-preview.png"
      id="opportunties"
      bottomComponent={
        <div className={Style.articleBox}>
          <div className={Style.bottomLeft}>
            <div className={Style.bottomLeftContent}>
              <div className={Style.userInfo}>
                <b>Sign up, build your organization page, and scale your impact on eFuse</b>
              </div>
            </div>
          </div>
          <div className={Style.bottomRight}>
            <div className={Style.btnLayout}>
              {isMobile ? (
                <FeatureFlag name={FEATURE_FLAGS.EXP_APP_DOWNLOAD_LANDING_PAGE}>
                  <FeatureFlagVariant flagState>
                    <EFRectangleButton
                      width="fullWidth"
                      colorTheme="secondary"
                      text="DOWNLOAD APP"
                      onClick={() => {
                        analytics.track(
                          "APP_DOWNLOAD_LINK_CLICKED",
                          { location: "Organizations Section" },
                          () => () => {
                            window.location.href = getOneLink(LINKS.DOWNLOAD_APP);
                          }
                        );
                      }}
                    />
                  </FeatureFlagVariant>
                  <FeatureFlagVariant flagState={false}>
                    <DynamicModal
                      flow="LoginSignup"
                      openOnLoad={false}
                      customRedirect="/welcome"
                      startView="signup"
                      displayCloseButton
                      allowBackgroundClickClose={false}
                    >
                      <EFRectangleButton width="fullWidth" colorTheme="secondary" text="CREATE ACCOUNT" />
                    </DynamicModal>
                  </FeatureFlagVariant>
                </FeatureFlag>
              ) : (
                <EFRectangleButton width="fullWidth" colorTheme="secondary" text="CREATE ACCOUNT" />
              )}
            </div>
          </div>
        </div>
      }
    />
  );
};
export default OrganizationTab;
