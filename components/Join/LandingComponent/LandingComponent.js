import React from "react";
import { Stream } from "@cloudflare/stream-react";
import { Container } from "react-bootstrap";

import LoginSignupFlow from "../../DynamicModal/LoginSignupFlow/LoginSignupFlow";
import Style from "./LandingComponent.module.scss";
import FeatureFlag from "../../General/FeatureFlag/FeatureFlag";
import FeatureFlagVariant from "../../General/FeatureFlag/FeatureFlagVariant/FeatureFlagVariant";
import useDeviceDetect from "../../../hooks/useDeviceDetect";
import FEATURE_FLAGS from "../../../common/featureFlags";

const LandingComponent = () => {
  const { isMobile } = useDeviceDetect();
  const videoIdOrSignedUrl = "3cf73e26918d0b905a7d94ce1653da69";
  return (
    <div className={Style.backgroundImageContainer} itemScope itemType="http://schema.org/WebApplication">
      <Container className={Style.landingPageHeaderContainer}>
        <meta itemProp="name" content="eFuse" />
        <meta itemProp="applicationCategory" content="Video Games, Esports" />
        <div className={Style.videoContainer}>
          <h1>
            Where <b>Gamers</b> <br />
            Get <b>Discovered</b>
          </h1>

          <Stream src={videoIdOrSignedUrl} muted autoplay loop height="100%" controls={false} />
        </div>

        {isMobile ? (
          <FeatureFlag name={FEATURE_FLAGS.EXP_APP_DOWNLOAD_LANDING_PAGE}>
            <FeatureFlagVariant flagState={false}>
              <div className={Style.signupFormContainer}>
                <LoginSignupFlow view="signup" signupLocationForTracking="home_page_embed" customRedirect="/welcome" />
              </div>
            </FeatureFlagVariant>
          </FeatureFlag>
        ) : (
          <div className={Style.signupFormContainer}>
            <LoginSignupFlow view="signup" signupLocationForTracking="home_page_embed" customRedirect="/welcome" />
          </div>
        )}
      </Container>
    </div>
  );
};

export default LandingComponent;
