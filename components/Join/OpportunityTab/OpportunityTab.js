import React from "react";
import Tilt from "react-tilt";

import EFImage from "../../EFImage/EFImage";
import Style from "./OpportunityTab.module.scss";

const OpportunityTab = () => {
  return (
    <div className={Style.containerTwo} id="opportunities">
      <div className={Style.cardContainer}>
        <Tilt className={`${Style.card}`} options={{ max: 5, scale: 1.01 }}>
          <div className={Style.tile}>
            <div className={`${Style.tileBackground} ${Style.tb1}`}>
              <img
                src="https://cdn.efuse.gg/uploads/static/landing-page/opportunity-resized.png"
                alt="eFuse opportunity preview"
              />
            </div>
            <div className={Style.content}>
              <b>Opportunities</b>
              <p>
                Access thousands of opportunities in the gaming industry including jobs, team openings, scholarships,
                tournaments, and many more. Apply now and get discovered on eFuse!
              </p>
            </div>
          </div>
        </Tilt>
        <Tilt className={`${Style.card}`} options={{ max: 5, scale: 1.01 }}>
          <div className={Style.tile}>
            <div className={`${Style.tileBackground} ${Style.tb2}`}>
              <img src="https://cdn.efuse.gg/uploads/static/landing-page/erena-resized.png" alt="eFuse eRena preview" />
            </div>
            <div className={Style.content}>
              <b>The eRena</b>
              <p>
                Are you ready to face-off in the eRena? Build, organize, watch, and run your own white-label tournaments
                right here on eFuse.
              </p>
            </div>
          </div>
        </Tilt>
        <Tilt className={`${Style.card}`} options={{ max: 5, scale: 1.01 }}>
          <div className={Style.tile}>
            <div className={`${Style.tileBackground} ${Style.tb3}`}>
              <img
                src="https://cdn.efuse.gg/uploads/static/landing-page/lounge-resized.png"
                alt="eFuse Lounge preview"
              />
            </div>
            <div className={Style.content}>
              <b>The Lounge</b>
              <p>
                Get your daily dose of esports on The Lounge, featuring the hottest clips from around the industry,
                exclusive content from the biggest influencers, and industry-leading news articles.
              </p>
            </div>
          </div>
        </Tilt>
      </div>
    </div>
  );
};
export default OpportunityTab;
