import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Link from "next/link";

import Style from "./NewsArticle.module.scss";
import LandingContent from "../../LandingContent/LandingContent";
import { getRecommendedNewsArticles } from "../../../store/actions/newsActions";
import { formatDate, getImage, rescueNil } from "../../../helpers/GeneralHelper";
import EFAvatar from "../../EFAvatar/EFAvatar";
import VerifiedIcon from "../../VerifiedIcon/VerifiedIcon";
import { EFHtmlParser } from "../../EFHtmlParser/EFHtmlParser";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";

const NewsArticle = props => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getRecommendedNewsArticles(21, true));
  }, []);

  const recommendedArticles = useSelector(state => state.news.recommendedNewsArticles) || [];
  const article = recommendedArticles && recommendedArticles.length > 0 && recommendedArticles[0];
  const authorType = article?.authorType;
  const user = article?.author;
  const profilePicture = rescueNil(user, authorType === "users" ? "profilePicture" : "profileImage");
  const displayName = article?.author?.name;
  const verified = rescueNil(user, "verified");

  return (
    <LandingContent
      title="News Article"
      description="Read articles written by industry leaders, recruiters, gaming experts, and influencers."
      imageUrl={article?.image?.url}
      id="opportunties"
      customClass={props.customClass}
      bottomComponent={
        <div className={Style.articleBox}>
          <div className={Style.articleTitle}>
            <b>{article?.title}</b>
          </div>
          <div className={Style.bottom}>
            <div className={Style.userDetail}>
              <div className={Style.avatar}>
                <EFAvatar
                  displayOnlineButton={false}
                  profilePicture={getImage(profilePicture, "avatar")}
                  size="small"
                  href={
                    authorType === "users"
                      ? `/u/${user?.username}?promptSignup=true`
                      : `/org/${user?.shortName}?promptSignup=true`
                  }
                />
              </div>
              <div>
                <div className={Style.userIcon}>
                  <b className={Style.username}>
                    {" "}
                    <EFHtmlParser>{displayName}</EFHtmlParser>
                  </b>{" "}
                  {verified && <VerifiedIcon />}
                </div>
                <p className={Style.date}>{formatDate(article?.createdAt)}</p>
              </div>
            </div>
            <div className={Style.bottomRight}>
              <Link href={`${article?.url}?promptSignup=true`}>
                <EFRectangleButton width="fullWidth" colorTheme="light" shadowTheme="medium" text="VIEW ARTICLE" />
              </Link>
            </div>
          </div>
        </div>
      }
    />
  );
};
export default NewsArticle;
