import React from "react";
import Style from "./LandingFooter.module.scss";
import NewsArticle from "../NewsArticle/NewsArticle";
import DynamicModal from "../../DynamicModal/DynamicModal";
import EFRectangleButton from "../../Buttons/EFRectangleButton/EFRectangleButton";
import EFImage from "../../EFImage/EFImage";
import Footer from "../../LandingPage/LandingFooter/LandingFooter";
import FeatureFlag from "../../General/FeatureFlag/FeatureFlag";
import FeatureFlagVariant from "../../General/FeatureFlag/FeatureFlagVariant/FeatureFlagVariant";
import FEATURE_FLAGS from "../../../common/featureFlags";
import useDeviceDetect from "../../../hooks/useDeviceDetect";
import { LINKS, getOneLink } from "../../../helpers/AppsFlyerHelper";

const LandingFooter = () => {
  const { isMobile } = useDeviceDetect();
  const loginSignupButtons = (
    <>
      <DynamicModal
        flow="LoginSignup"
        openOnLoad={false}
        startView="signup"
        displayCloseButton
        allowBackgroundClickClose={false}
      >
        <div className={Style.btnLayout}>
          <EFRectangleButton width="fullWidth" colorTheme="secondary" text="GET STARTED" />
        </div>
      </DynamicModal>
      <DynamicModal
        flow="LoginSignup"
        openOnLoad={false}
        startView="login"
        displayCloseButton
        allowBackgroundClickClose={false}
      >
        <div className={Style.btnLayout}>
          <EFRectangleButton width="fullWidth" colorTheme="primary" text="LOG IN" />
        </div>
      </DynamicModal>
    </>
  );

  return (
    <div className={Style.footerContainer}>
      <div className={Style.containerOne} />
      <div className={Style.footer}>
        <NewsArticle customClass={Style.footerContent} />
        <div className={Style.footerContent}>
          <div className={Style.logoImg}>
            <EFImage
              src="https://cdn.efuse.gg/uploads/static/landing-page/whiteLogo-optimized.png"
              alt="eFuse logo white"
              width={205}
              height={61}
            />
          </div>
          <div className={Style.btn}>
            {isMobile ? (
              <FeatureFlag name={FEATURE_FLAGS.EXP_APP_DOWNLOAD_LANDING_PAGE}>
                <FeatureFlagVariant flagState>
                  <div className={Style.btnLayout}>
                    <EFRectangleButton
                      buttonType="button"
                      colorTheme="secondary"
                      fontWeightTheme="bold"
                      shadowTheme="medium"
                      size="large"
                      onClick={() => {
                        analytics.track("APP_DOWNLOAD_LINK_CLICKED", { location: "Bottom Footer" }, () => () => {
                          window.location.href = getOneLink(LINKS.DOWNLOAD_APP);
                        });
                      }}
                      text="DOWNLOAD APP"
                      width="fullWidth"
                    />
                  </div>
                </FeatureFlagVariant>
                <FeatureFlagVariant flagState={false}>{loginSignupButtons}</FeatureFlagVariant>
              </FeatureFlag>
            ) : (
              loginSignupButtons
            )}
            <div className={Style.text}>
              <p>WHERE GAMERS GET DISCOVERED</p>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default LandingFooter;
