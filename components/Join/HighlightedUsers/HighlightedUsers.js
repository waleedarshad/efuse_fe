import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import { Container } from "react-bootstrap";
import Style from "./HighlightedUsers.module.scss";
import { getFollowerRecommendations } from "../../../store/actions/recommendationsActions";
import EFUserCarousel from "../../EFUserCarousel/EFUserCarousel";

const HighlightedUsers = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getFollowerRecommendations(true));
  }, []);

  const recommendedFollowers = useSelector(state => state.recommendations.followers);

  return (
    <Container>
      <div className={Style.containerTwo} id="opportunities">
        <div className={Style.cardContainer}>
          <div className={Style.title}>
            <b>Highlighted Gamers</b>
          </div>
          <div className={Style.desc}>
            <p>
              Learn from and connect with iconic gaming entertainers & business professionals to take your journey in
              gaming to the next level.
            </p>
          </div>
          <EFUserCarousel users={recommendedFollowers} />
        </div>
      </div>
    </Container>
  );
};
export default HighlightedUsers;
