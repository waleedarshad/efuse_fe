import { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faStar } from "@fortawesome/pro-solid-svg-icons";
import Tilt from "react-tilt";
import Style from "./RecommendationItem.module.scss";
import EFImage from "../../EFImage/EFImage";

const VisibilitySensor = require("react-visibility-sensor").default;

function onViewChange(isVisible, data) {
  if (isVisible) {
    analytics.track("RECOMMENDATION_VIEW", data);
  }
}

class RecentItem extends Component {
  render() {
    const {
      avatar,
      name,
      buttonLabel,
      imageType,
      itemType,
      isPromoted,
      url,
      type,
      id,
      index,
      showPromotedLabel
    } = this.props;

    return (
      <VisibilitySensor
        onChange={isVisible => {
          onViewChange(isVisible, { type, id });
        }}
        key={index}
      >
        <Tilt options={{ max: 10, scale: 1.02 }}>
          <div className={Style.mediaContainer}>
            <div className={Style.imageWrapper}>
              <EFImage className={`${Style.mediaImage} ${imageType && Style[imageType]}`} src={avatar} alt={name} />
            </div>
            <div>
              <h6 className={Style.titleName}>{name}</h6>
              <h6 className={Style.itemType}>{itemType}</h6>
            </div>
            <div
              className={Style.button}
              onClick={() => {
                analytics.track("RECOMMENDATION_CLICK", { type, id }, {}, () => {
                  document.location.href = url;
                });
              }}
            >
              <span>{buttonLabel}</span>
            </div>
            {isPromoted && showPromotedLabel && (
              <div className={Style.adHeader}>
                <FontAwesomeIcon icon={faStar} className={Style.starIcon} />
                <p className={Style.headerText}>Promoted</p>
              </div>
            )}
          </div>
        </Tilt>
      </VisibilitySensor>
    );
  }
}

RecentItem.propTypes = {
  avatar: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  buttonLabel: PropTypes.string,
  itemType: PropTypes.string
};

const mapStateToProps = state => ({
  showPromotedLabel: state.features.recommendations_show_promoted_label
});

export default connect(mapStateToProps, {})(RecentItem);
