import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { ListGroup } from "react-bootstrap";
import PropTypes from "prop-types";

import Style from "./Recommendation.module.scss";
import ListStyle from "../List/List.module.scss";
import { getRecommendedOpportunities } from "../../store/actions/opportunityActions";
import { getRecommendedOrganizations } from "../../store/actions/organizationActions";
import { getRecommendedNewsArticles } from "../../store/actions/newsActions";
import { getRecommendedUsers } from "../../store/actions/userActions";
import AnimatedLogo from "../AnimatedLogo";
import RecommendationItem from "./RecommendationItem/RecommendationItem";
import { getImage } from "../../helpers/GeneralHelper";
import Carousel from "../Carousels/Carousel/Carousel";
import { getFullOpportunityUrl, getOrganizationUrl } from "../../helpers/UrlHelper";
import EFRectangleButton from "../Buttons/EFRectangleButton/EFRectangleButton";

const Recommendation = ({ type, hideContainer }) => {
  const dispatch = useDispatch();
  const recommendedOpportunities = useSelector(state => state.opportunities.recommendedOpportunities);
  const recommendedOrganizations = useSelector(state => state.organizations.recommendedOrganizations);
  const recommendedNewsArticles = useSelector(state => state.news.recommendedNewsArticles);
  const recommendedUsers = useSelector(state => state.user.recommendedUsers);

  const totalItemsToGet = 21;

  useEffect(() => {
    switch (type) {
      case "opportunities":
        if (!recommendedOpportunities) dispatch(getRecommendedOpportunities(totalItemsToGet));
        break;
      case "organizations":
        if (!recommendedOrganizations) dispatch(getRecommendedOrganizations(totalItemsToGet));
        break;
      case "learning":
        if (!recommendedNewsArticles) dispatch(getRecommendedNewsArticles(totalItemsToGet));
        break;
      case "users":
        if (!recommendedUsers) dispatch(getRecommendedUsers(totalItemsToGet));
        break;
      default:
      // do nothing
    }
  }, []);

  let content = "";
  let recommendations;
  let containerTitle;
  let viewAllURL;
  let carouselContent;

  switch (type) {
    case "opportunities":
      containerTitle = "Recommended Opportunities";
      viewAllURL = "/opportunities";
      recommendations = recommendedOpportunities;
      if (recommendedOpportunities)
        carouselContent = recommendedOpportunities.map((opportunity, index) => (
          <RecommendationItem
            id={opportunity._id}
            type={type}
            buttonLabel="View Opportunity"
            name={opportunity.title}
            avatar={getImage(opportunity.image, "opportunity")}
            itemType={opportunity.opportunityType}
            url={getFullOpportunityUrl(opportunity)}
            isPromoted={opportunity.promoted}
            index={index}
            key={index}
          />
        ));
      break;
    case "organizations":
      containerTitle = "Popular Organizations";
      viewAllURL = "/organizations";
      recommendations = recommendedOrganizations;
      if (recommendedOrganizations)
        carouselContent = recommendedOrganizations.map((organization, index) => (
          <RecommendationItem
            id={organization._id}
            type={type}
            buttonLabel="View Organization"
            name={organization.name}
            avatar={getImage(organization.profileImage, "organization")}
            itemType={organization.organizationType}
            url={getOrganizationUrl(organization)}
            isPromoted={organization.promoted}
            index={organization._id}
            key={index}
          />
        ));
      break;
    case "learning":
      containerTitle = "Trending News Articles";
      viewAllURL = "/news";
      recommendations = recommendedNewsArticles;
      if (recommendedNewsArticles)
        carouselContent = recommendedNewsArticles.map((article, index) => (
          <RecommendationItem
            id={article._id}
            type={type}
            buttonLabel="View News Article"
            name={article.title}
            avatar={getImage(article.image, "learning")}
            itemType={article.category.name}
            url={article?.promoted_url || article.url}
            isPromoted={article.promoted}
            index={article._id}
            key={index}
          />
        ));
      break;
    case "users":
      containerTitle = "Who to Follow";
      recommendations = recommendedUsers;
      if (recommendedUsers)
        carouselContent = recommendedUsers.map((user, index) => (
          <RecommendationItem
            id={user.id}
            type={type}
            buttonLabel={`View ${user.name}'s Portfolio`}
            name={user.name}
            avatar={getImage(user.profilePicture, "avatar")}
            itemType={user.bio}
            url={user.promoted_url || `u/${user.username}`}
            isPromoted={user.promoted}
            index={index}
            key={index}
          />
        ));
      break;
    default:
    // do nothing
  }

  if (!recommendations) {
    content = (
      <span style={{ margin: "auto" }}>
        <AnimatedLogo key={0} theme="content" />
      </span>
    );
  } else if (recommendations.length === 0) {
    content = <div>No Recommendations Available</div>;
  } else {
    content = <Carousel>{carouselContent}</Carousel>;
  }

  if (hideContainer) {
    return <div className={Style.carouselContainer}>{content}</div>;
  }
  return (
    <ListGroup className={`mb-3 ${Style.recentOpportunitiesContainer}`}>
      <ListGroup.Item className={`${ListStyle.listItem} ${Style.listHeader}`}>
        <span>{containerTitle}</span>
        {viewAllURL && <EFRectangleButton text="View All" internalHref={viewAllURL} />}
      </ListGroup.Item>
      <ListGroup.Item className={`${ListStyle.listItem} ${Style.lightGreyBackground}`}>
        <div className={Style.carouselContainer}>{content}</div>
      </ListGroup.Item>
    </ListGroup>
  );
};

Recommendation.propTypes = {
  type: PropTypes.string,
  hideContainer: PropTypes.bool
};

Recommendation.defaultProps = {
  type: "",
  hideContainer: false
};

export default Recommendation;
