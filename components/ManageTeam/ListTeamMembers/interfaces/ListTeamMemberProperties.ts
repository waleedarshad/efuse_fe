import { TeamMember } from "./TeamMember";

export interface TeamMemberProperties {
  teamMembers?: TeamMember[];
  onDelete: (member: TeamMember) => TeamMember;
}
