export interface TeamMember {
  _id?: string;
  user: {
    name: string;
    username: string;
    _id: string;
    profilePicture: {
      url: string;
    };
  };
  name?: string;
}
