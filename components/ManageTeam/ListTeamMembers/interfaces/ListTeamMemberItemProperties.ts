import { TeamMember } from "./TeamMember";

export interface TeamMemberItemProperties {
  member: TeamMember;
  index: number;
  whichMemberIsHovering: string;
  setWhichMemberIsHovering: (member: string) => void;
  onDelete: (item: TeamMember) => TeamMember;
}
