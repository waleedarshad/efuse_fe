import React, { useState } from "react";
import { ListGroup, ListGroupItem, Row } from "react-bootstrap";
import Style from "./ListTeamMembers.module.scss";
import ListTeamMemberItem from "./ListTeamMemberItem/ListTeamMemberItem";
import { TeamMemberProperties } from "./interfaces/ListTeamMemberProperties";

const ListTeamMembers: React.FC<TeamMemberProperties> = ({ teamMembers, onDelete }) => {
  const [whichMemberIsHovering, setWhichMemberIsHovering] = useState("");

  const memberItems = teamMembers.map((member, index) => {
    return (
      <ListTeamMemberItem
        key={member?._id || index}
        setWhichMemberIsHovering={memberId => setWhichMemberIsHovering(memberId)}
        whichMemberIsHovering={whichMemberIsHovering}
        member={member}
        index={index}
        onDelete={deleteMember => onDelete(deleteMember)}
      />
    );
  });

  return (
    <ListGroup variant="flush">
      <ListGroupItem>
        <Row className={Style.headerRow}>
          <div className={`${Style.membersText} ${Style.headerText}`}>Team Members</div>
          <div className={`${Style.removeMemberText} ${Style.headerText}`}>Remove Member</div>
        </Row>
      </ListGroupItem>
      {memberItems}
    </ListGroup>
  );
};

export default ListTeamMembers;
