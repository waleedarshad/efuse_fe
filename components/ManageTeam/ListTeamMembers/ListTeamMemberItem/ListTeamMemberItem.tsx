import React from "react";
import { ListGroupItem, Row } from "react-bootstrap";
import { faTrashAlt } from "@fortawesome/pro-regular-svg-icons";
import Style from "./ListTeamMemberItem.module.scss";
import EFAvatar from "../../../EFAvatar/EFAvatar";
import EFCircleIconButton from "../../../Buttons/EFCircleIconButton/EFCircleIconButton";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";
import { TeamMemberItemProperties } from "../interfaces/ListTeamMemberItemProperties";

const ListTeamMemberItem: React.FC<TeamMemberItemProperties> = ({
  member,
  index,
  whichMemberIsHovering,
  setWhichMemberIsHovering,
  onDelete
}) => {
  return (
    <ListGroupItem key={member?._id || index}>
      <Row className={`${Style.rowJustify}`}>
        <Row>
          {member?.user?.profilePicture?.url && (
            <div className={Style.memberImage}>
              <EFAvatar
                borderTheme="lightColor"
                size="small"
                profilePicture={member?.user?.profilePicture?.url}
                displayOnlineButton={false}
              />
            </div>
          )}
          <div className={Style.nameText}>{member?.user?.name}</div>
        </Row>
        <div
          className={Style.buttonWrapper}
          onMouseEnter={() => setWhichMemberIsHovering(member._id)}
          onMouseLeave={() => setWhichMemberIsHovering("")}
        >
          {member?._id !== whichMemberIsHovering && (
            <EFCircleIconButton icon={faTrashAlt} colorTheme="transparent" shadowTheme="none" />
          )}
          {member?._id === whichMemberIsHovering && (
            <div className={Style.removeButtonWrapper}>
              <EFRectangleButton
                shadowTheme="none"
                fontColorTheme="dangerous"
                icon={faTrashAlt}
                text="Remove"
                fontWeightTheme="normal"
                onClick={() => onDelete(member)}
                colorTheme="transparent"
              />
            </div>
          )}
        </div>
      </Row>
    </ListGroupItem>
  );
};

export default ListTeamMemberItem;
