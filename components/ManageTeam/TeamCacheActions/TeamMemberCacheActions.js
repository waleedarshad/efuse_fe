import { ADD_TEAM_MEMBER_CACHE_QUERY } from "../../../graphql/Team/TeamMemberQuery";

export const addMembersToCache = (cache, { data: { addUsersToTeam: memberData } }) => {
  if (memberData && memberData.length > 0) {
    memberData.forEach(member => {
      cache.modify({
        id: `Team:${member.team._id}`,
        fields: {
          members: teamMembers => {
            const newMemberRef = cache.writeFragment({
              data: member,
              fragment: ADD_TEAM_MEMBER_CACHE_QUERY
            });
            return [...teamMembers, newMemberRef];
          }
        }
      });
    });
  }
};

export const deleteMemberFromCache = (cache, { data: { deleteTeamMember: memberData } }) => {
  cache.modify({
    id: `Team:${memberData.team._id}`,
    fields: {
      members: (exisitingMembers, { readField }) => {
        return exisitingMembers.filter(member => readField("_id", member) !== memberData._id);
      }
    }
  });
};
