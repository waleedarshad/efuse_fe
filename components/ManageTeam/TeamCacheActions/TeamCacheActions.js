import { UPDATE_TEAM_CACHE_QUERY } from "../../../graphql/Team/TeamQuery";

// eslint-disable-next-line import/prefer-default-export
export const updateTeamInCache = (cache, { data: { updateTeam: teamData } }) => {
  cache.writeQuery({
    query: UPDATE_TEAM_CACHE_QUERY,
    data: teamData,
    variables: {
      id: teamData._id
    }
  });
};
