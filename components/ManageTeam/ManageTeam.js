import React, { useEffect, useState } from "react";
import { Row } from "react-bootstrap";
import { faChevronLeft } from "@fortawesome/pro-regular-svg-icons";
import { useMutation } from "@apollo/client";
import Style from "./ManageTeam.module.scss";
import ListTeamMembers from "./ListTeamMembers/ListTeamMembers";
import EFRectangleButton from "../Buttons/EFRectangleButton/EFRectangleButton";
import EFConfirmationModal from "../modals/EFConfirmationModal/EFConfirmationModal";
import TeamModal from "../Modals/TeamModal/TeamModal";
import { UPDATE_TEAM } from "../../graphql/Team/TeamQuery";
import { ADD_MEMBERS_TO_TEAM, DELETE_TEAM_MEMBER } from "../../graphql/Team/TeamMemberQuery";
import EFImage from "../EFImage/EFImage";
import { addMembersToCache, deleteMemberFromCache } from "./TeamCacheActions/TeamMemberCacheActions";
import { updateTeamInCache } from "./TeamCacheActions/TeamCacheActions";

const ManageTeam = ({ team, onBackToTeams }) => {
  const [currentTeam, setCurrentTeam] = useState(team);
  const [isConfirmationOpen, toggleConfirmationModal] = useState(false);
  const [isTeamOpen, toggleTeamModal] = useState(false);
  const [memberToDelete, setMemberToDelete] = useState({});
  const [viewAddTeamMember, setViewAddTeamMember] = useState(false);

  const [updateTeam] = useMutation(UPDATE_TEAM, {
    update: updateTeamInCache
  });
  const [createTeamMembers] = useMutation(ADD_MEMBERS_TO_TEAM, {
    update: addMembersToCache
  });
  const [deleteTeamMember] = useMutation(DELETE_TEAM_MEMBER, {
    update: deleteMemberFromCache
  });

  useEffect(() => {
    setCurrentTeam(team);
  }, [team]);

  const closeConfirmationModalAndResetMemberToDelete = () => {
    setMemberToDelete({});
    toggleConfirmationModal(false);
  };

  const openConfirmationModalAndSetMemberToDelete = member => {
    setMemberToDelete(member);
    toggleConfirmationModal(true);
  };

  const onAddTeamMembers = userIds => {
    if (userIds && userIds.length > 0) {
      createTeamMembers({
        variables: {
          teamId: currentTeam._id,
          userIds
        }
      });
    }
  };

  const onDeleteTeamMember = () => {
    deleteTeamMember({
      variables: {
        teamMemberId: memberToDelete?._id
      }
    });
  };

  const onTeamSubmit = modifiedTeam => {
    onAddTeamMembers(modifiedTeam?.userIds);
    onUpdateTeam(modifiedTeam);
  };

  const onUpdateTeam = modifiedTeam => {
    updateTeam({
      variables: {
        teamId: currentTeam?._id,
        teamUpdates: {
          game: modifiedTeam?.game,
          name: modifiedTeam?.name
        }
      }
    });
  };

  return (
    <div className={Style.listGroup}>
      <TeamModal
        isOpen={isTeamOpen || viewAddTeamMember}
        team={currentTeam}
        onClose={() => {
          toggleTeamModal(false);
          setViewAddTeamMember(false);
        }}
        onSubmit={modifiedTeam => onTeamSubmit(modifiedTeam)}
        viewAddTeamMember={viewAddTeamMember}
      />
      <EFConfirmationModal
        message={`Are you sure you want to remove [${memberToDelete?.user?.name}] from the team`}
        isOpen={isConfirmationOpen}
        onConfirm={() => onDeleteTeamMember()}
        onClose={() => closeConfirmationModalAndResetMemberToDelete()}
      />
      <div className={Style.backToTeamsButton}>
        <EFRectangleButton
          colorTheme="transparent"
          icon={faChevronLeft}
          shadowTheme="none"
          fontWeightTheme="normal"
          onClick={() => onBackToTeams && onBackToTeams()}
          text="Back to Teams"
        />
      </div>
      <>
        <Row className={Style.imageAndButtons}>
          <div>
            <Row className={Style.imageAndText}>
              <EFImage width={50} height={50} src={team?.image?.url} className={Style.teamImage} />
              <div className={Style.teamNameText}>{currentTeam?.name}</div>
            </Row>
          </div>
          <div className={Style.buttonWrapper}>
            <div className={Style.addTeamMemberButtonWrapper}>
              <EFRectangleButton
                colorTheme="azul"
                shadowTheme="none"
                text="Add Team Members"
                width="fullWidth"
                onClick={() => setViewAddTeamMember(true)}
              />
            </div>
            <div className={Style.editTeamButtonWrapper}>
              <EFRectangleButton
                colorTheme="white"
                fontColorTheme="white"
                width="fullWidth"
                shadowTheme="none"
                text="Edit Team"
                onClick={() => toggleTeamModal(true)}
              />
            </div>
          </div>
        </Row>
        {team?.members && team?.members.length > 0 && (
          <ListTeamMembers
            onDelete={member => openConfirmationModalAndSetMemberToDelete(member)}
            teamMembers={team?.members}
          />
        )}
      </>
    </div>
  );
};

export default ManageTeam;
