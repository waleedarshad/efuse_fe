import Link from "next/link";
import PropTypes from "prop-types";
import EFImage from "../EFImage/EFImage";
import OnlineCircle from "../OnlineCircle/OnlineCircle";
import Style from "./EFAvatar.module.scss";

const EFAvatar = ({
  profilePicture,
  size,
  borderTheme,
  displayOnlineButton,
  online,
  href,
  as,
  withBorderShadow,
  className
}) => {
  const avatar = (
    <div
      className={`${Style.avatarWrapper} ${Style[size]} ${Style[borderTheme]} ${
        withBorderShadow ? Style.withBorderShadow : ""
      } ${className ?? ""}`}
    >
      <div className={`${Style.avatar}  ${href ? Style.asLink : ""}`}>
        <EFImage src={profilePicture} alt="Profile Image" className={Style.profilePicture} />
      </div>
      {displayOnlineButton && (
        <div className={Style.onlineWrapper}>
          <OnlineCircle online={online} size={size} borderTheme={borderTheme} />
        </div>
      )}
    </div>
  );

  if (href) {
    return (
      <Link href={href} as={as}>
        {avatar}
      </Link>
    );
  }

  return avatar;
};

EFAvatar.propTypes = {
  profilePicture: PropTypes.string,
  size: PropTypes.oneOf(["extraExtraSmall", "extraSmall", "small", "medium", "large", "extraLarge"]),
  online: PropTypes.bool,
  borderTheme: PropTypes.string,
  displayOnlineButton: PropTypes.bool
};

EFAvatar.defaultProps = {
  profilePicture: "",
  size: "medium",
  borderTheme: "lightColor",
  online: false,
  displayOnlineButton: true,
  withBorderShadow: false
};

export default EFAvatar;
