import React from "react";
import EFAvatar from "./EFAvatar";

export default {
  title: "Users/EFAvatar",
  component: EFAvatar,
  argTypes: {
    profilePicture: {
      control: {
        type: "text"
      }
    },
    size: {
      control: {
        type: "select",
        options: ["extraSmall", "small", "medium", "large", "extraLarge"]
      }
    },
    borderTheme: {
      control: {
        type: "select",
        options: ["primary", "navColor", "lightColor"]
      }
    },
    online: {
      control: {
        type: "boolean"
      }
    },
    displayOnlineButton: {
      control: {
        type: "boolean"
      }
    }
  }
};

// eslint-disable-next-line react/jsx-props-no-spreading
const Story = args => <EFAvatar {...args} />;

export const Primary = Story.bind({});
Primary.args = {
  profilePicture:
    "https://static.wikia.nocookie.net/youtube/images/f/fc/NewMrBeastLogo.jpg/revision/latest?cb=20180426004557",
  size: "medium",
  borderTheme: "primary",
  online: true
};

export const NavColor = Story.bind({});
NavColor.args = {
  profilePicture:
    "https://static.wikia.nocookie.net/youtube/images/f/fc/NewMrBeastLogo.jpg/revision/latest?cb=20180426004557",
  size: "medium",
  borderTheme: "navColor",
  online: true
};

export const LightColor = Story.bind({});
LightColor.args = {
  profilePicture:
    "https://static.wikia.nocookie.net/youtube/images/f/fc/NewMrBeastLogo.jpg/revision/latest?cb=20180426004557",
  size: "medium",
  borderTheme: "lightColor",
  online: true
};

export const Offline = Story.bind({});
Offline.args = {
  profilePicture:
    "https://static.wikia.nocookie.net/youtube/images/f/fc/NewMrBeastLogo.jpg/revision/latest?cb=20180426004557",
  size: "medium",
  borderTheme: "primary",
  online: false
};

export const ExtraSmall = Story.bind({});
ExtraSmall.args = {
  profilePicture:
    "https://static.wikia.nocookie.net/youtube/images/f/fc/NewMrBeastLogo.jpg/revision/latest?cb=20180426004557",
  size: "extraSmall",
  borderTheme: "primary",
  online: true
};

export const Small = Story.bind({});
Small.args = {
  profilePicture:
    "https://static.wikia.nocookie.net/youtube/images/f/fc/NewMrBeastLogo.jpg/revision/latest?cb=20180426004557",
  size: "small",
  borderTheme: "primary",
  online: true
};

export const Medium = Story.bind({});
Medium.args = {
  profilePicture:
    "https://static.wikia.nocookie.net/youtube/images/f/fc/NewMrBeastLogo.jpg/revision/latest?cb=20180426004557",
  size: "medium",
  borderTheme: "primary",
  online: true
};

export const Large = Story.bind({});
Large.args = {
  profilePicture:
    "https://static.wikia.nocookie.net/youtube/images/f/fc/NewMrBeastLogo.jpg/revision/latest?cb=20180426004557",
  size: "large",
  borderTheme: "primary",
  online: true
};

export const ExtraLarge = Story.bind({});
ExtraLarge.args = {
  profilePicture:
    "https://static.wikia.nocookie.net/youtube/images/f/fc/NewMrBeastLogo.jpg/revision/latest?cb=20180426004557",
  size: "extraLarge",
  borderTheme: "primary",
  online: true
};

export const RemoveOnlineButton = Story.bind({});
RemoveOnlineButton.args = {
  profilePicture:
    "https://static.wikia.nocookie.net/youtube/images/f/fc/NewMrBeastLogo.jpg/revision/latest?cb=20180426004557",
  size: "medium",
  borderTheme: "primary",
  online: true,
  displayOnlineButton: false
};

export const WithBorderShadow = Story.bind({});
WithBorderShadow.args = {
  profilePicture:
    "https://static.wikia.nocookie.net/youtube/images/f/fc/NewMrBeastLogo.jpg/revision/latest?cb=20180426004557",
  size: "medium",
  borderTheme: "white",
  displayOnlineButton: false,
  WithBorderShadow: true
};
