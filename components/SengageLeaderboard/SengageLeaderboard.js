import React, { Component } from "react";

class SengageLeaderboard extends Component {
  render() {
    const { source, id, height } = this.props;
    return (
      <iframe
        id={id}
        src={source}
        allowFullScreen=""
        frameBorder="0"
        width="100%"
        height={height || "100%"}
        sandbox="allow-scripts allow-same-origin allow-popups allow-popups-to-escape-sandbox"
      />
    );
  }
}

export default SengageLeaderboard;
