import React from "react";

import DynamicModal from "../DynamicModal/DynamicModal";
import LandingPage from "../Join/LandingPage";

const ForgotPassword = () => {
  return (
    <>
      <LandingPage />
      <DynamicModal
        flow="LoginSignup"
        openOnLoad
        startView="forgot_password"
        displayCloseButton
        allowBackgroundClickClose={false}
      />
    </>
  );
};

export default ForgotPassword;
