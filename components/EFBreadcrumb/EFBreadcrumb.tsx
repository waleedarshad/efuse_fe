import React from "react";
import Link from "next/link";
import styled from "styled-components";

interface LinkProps {
  name: string;
  href: string;
}

interface EFBreadcrumbProps {
  links: LinkProps[];
  addBottomMargin?: boolean;
}

const BreadcrumbContainer = styled.div`
  font-family: Poppins, sans-serif;
  font-style: normal;
  font-weight: 500;
  font-size: 0.875rem;
  margin-bottom: ${props => (props.addBottomMargin ? "10" : "0")}px;
`;

const LinkSpan = styled.span`
  cursor: pointer;
  color: ${props => (props.index === props.links.length - 1 ? "#12151d" : "#12151d99")};
  &:hover {
    text-decoration: none;
    color: #12151d;
  }
`;

const Separator = styled.span`
  margin: 0 5px;
  color: ${props => (props.index === props.links.length - 2 ? "#12151d" : "#12151d99")};
`;

const EFBreadcrumb: React.FC<EFBreadcrumbProps> = ({ links, addBottomMargin = true }) => {
  return (
    <BreadcrumbContainer addBottomMargin={addBottomMargin}>
      {links.map((link, index) => (
        <>
          <Link href={link.href} passHref>
            <LinkSpan links={links} index={index}>
              {link.name}
            </LinkSpan>
          </Link>
          {index !== links.length - 1 && (
            <Separator links={links} index={index}>
              &gt;
            </Separator>
          )}
        </>
      ))}
    </BreadcrumbContainer>
  );
};

export default EFBreadcrumb;
