/* eslint-disable react/jsx-props-no-spreading */
import React from "react";
import EFChip from "./EFChip";

export default {
  title: "Buttons/EFChip"
};

const InfoStory = args => <EFChip.Info {...args} />;

export const InfoChipLight = InfoStory.bind({});
InfoChipLight.args = {
  content: "some information",
  badgeText: "3",
  onClick: () => {},
  colorTheme: "light"
};

export const InfoChipDark = InfoStory.bind({});
InfoChipDark.args = {
  content: "Dark and 3 digits in badge",
  badgeText: "100",
  onClick: () => {},
  colorTheme: "dark"
};

export const InfoChipNoBadge = InfoStory.bind({});
InfoChipNoBadge.args = {
  content: "NO BADGE HERE",
  onClick: () => {},
  colorTheme: "light"
};

const RemovableStory = args => <EFChip.Removable {...args} />;

export const RemovableChipDark = RemovableStory.bind({});
RemovableChipDark.args = {
  content: "SOME REMOVABLE CHIP",
  onClick: () => {},
  colorTheme: "dark"
};

export const RemovableChipLight = RemovableStory.bind({});
RemovableChipLight.args = {
  content: "NO BADGE HERE",
  onClick: () => {},
  colorTheme: "light"
};
