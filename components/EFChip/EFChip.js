import { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimesCircle } from "@fortawesome/pro-solid-svg-icons";
import PropTypes from "prop-types";

import EFPillButton from "../Buttons/EFPillButton/EFPillButton";
import EFBadge from "../EFBadge/EFBadge";
import Style from "./EFChip.module.scss";

const Info = ({ content, badgeText, onClick, colorTheme }) => {
  return (
    <div className={`${Style.info} ${onClick ? "" : Style.notClickable}`}>
      <EFPillButton buttonType="button" shadowTheme="small" colorTheme={colorTheme} text={content} onClick={onClick} />
      <div className={Style.badge}>
        <EFBadge text={badgeText} />
      </div>
    </div>
  );
};

Info.propTypes = {
  content: PropTypes.node,
  badgeText: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  onClick: PropTypes.func,
  colorTheme: PropTypes.oneOf(["dark", "light"])
};

Info.defaultProps = {
  content: "",
  badgeText: "",
  onClick: undefined,
  colorTheme: "light"
};

const Removable = ({ content, badgeText, onRemove, colorTheme }) => {
  const [isVisible, setIsVisible] = useState(true);

  if (!isVisible) {
    return <></>;
  }

  const onRemoveClick = () => {
    setIsVisible(false);
    onRemove();
  };

  const text = (
    <div className={Style.contentContainer}>
      {content}
      <div className={Style.iconContainer}>
        <FontAwesomeIcon icon={faTimesCircle} className={Style.icon} />
      </div>
    </div>
  );

  return (
    <div className={Style.removable}>
      <Info content={text} badgeText={badgeText} colorTheme={colorTheme} onClick={onRemoveClick} />
    </div>
  );
};

Removable.propTypes = {
  content: PropTypes.node,
  badgeText: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  onRemove: PropTypes.func,
  colorTheme: PropTypes.oneOf(["dark", "light"])
};

Removable.defaultProps = {
  content: "",
  badgeText: "",
  onRemove: () => {},
  colorTheme: "light"
};

const EFChip = {
  Info,
  Removable
};

export default EFChip;
