import Form from "react-bootstrap/Form";

import Style from "./InputRow.module.scss";

const InputRow = props => <Form.Row {...props} className={Style.inputRow} />;

export default InputRow;
