import React, { useEffect, useState } from "react";
import EFListCheckbox from "../EFListCheckbox/EFListCheckbox";

interface Input {
  imageUrl: string;
  _id: string;
  title: string;
  isChecked: boolean;
  disabled: boolean;
}

interface EFMultiCheckboxSelectProps {
  inputs: any;
  onSelect: (inputs: Input[]) => void;
}

const EFMultiCheckboxSelect: React.FC<EFMultiCheckboxSelectProps> = ({ inputs, onSelect }) => {
  const [checkboxInputs, setCheckboxInputs] = useState([]);

  useEffect(() => {
    setCheckboxInputs(inputs);
  }, [inputs]);

  const onInputClick = selectedInput => {
    const updatedInputs = [...checkboxInputs];

    const inputIndex = checkboxInputs.findIndex(x => x._id === selectedInput?._id);
    updatedInputs[inputIndex] = { ...selectedInput, isChecked: !selectedInput?.isChecked };

    setCheckboxInputs(updatedInputs);
    onSelect(updatedInputs.filter(input => input.isChecked));
  };
  return <EFListCheckbox inputs={checkboxInputs} onSelect={onInputClick} />;
};

export default EFMultiCheckboxSelect;
