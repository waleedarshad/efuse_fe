import React from "react";
import EFMultiCheckboxSelect from "./EFMultiCheckboxSelect";

export default {
  title: "Inputs/EFMultiCheckboxSelect",
  component: EFMultiCheckboxSelect,
  argTypes: {
    inputs: {
      control: {
        type: "array"
      }
    },
    onSelect: {
      control: {
        type: "function"
      }
    }
  }
};

// eslint-disable-next-line react/jsx-props-no-spreading
const Story = args => <EFMultiCheckboxSelect {...args} />;

export const Default = Story.bind({});
Default.args = {
  inputs: [
    {
      imageUrl: "https://i.ytimg.com/vi/0E44DClsX5Q/maxresdefault.jpg",
      title: "New York Excelsior",
      _id: "1"
    },
    {
      imageUrl: "https://i.ytimg.com/vi/0E44DClsX5Q/maxresdefault.jpg",
      title: "New York Excelsior",
      _id: "2"
    },
    {
      imageUrl: "https://i.ytimg.com/vi/0E44DClsX5Q/maxresdefault.jpg",
      title: "New York Excelsior",
      _id: "3"
    }
  ],
  onSelect: () => {}
};
