import Style from "./TwitchPlayerIframe.module.scss";

const TwitchPlayerIframe = props => {
  const {
    twitchChannel,
    backgroundShadow,
    roundedCorners,
    topRoundedCorners,
    marginBottom,
    fullWidth,
    autoplay,
    maxHeight,
    fullHeight,
    muted
  } = props;
  return (
    <div
      className={`${!fullWidth && "embed-responsive embed-responsive-16by9"} ${backgroundShadow &&
        Style.backgroundShadow} ${roundedCorners && Style.roundedCorners} ${topRoundedCorners &&
        Style.topRoundedCorners} ${marginBottom && Style.marginBottom} ${fullWidth &&
        Style.fullWidthVideo} ${maxHeight && Style.maxHeight} ${fullHeight && Style.fullHeight}`}
    >
      <iframe
        title={`Twitch Player ${twitchChannel}`}
        src={`https://player.twitch.tv/?channel=${twitchChannel}&parent=localhost&parent=efuse.gg&parent=efuse.wtf&autoplay=${autoplay}&muted=${muted}`}
        frameBorder="0"
        scrolling="no"
        allowFullScreen
        className={`${!fullWidth && "embed-responsive-item"} ${fullWidth && Style.fullWidthIframe}`}
      />
    </div>
  );
};

TwitchPlayerIframe.defaultProps = {
  roundedCorners: false,
  backgroundShadow: false,
  marginBottom: false,
  autoplay: false,
  muted: true
};

export default TwitchPlayerIframe;
