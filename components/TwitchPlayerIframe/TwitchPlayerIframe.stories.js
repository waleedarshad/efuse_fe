import React from "react";
import TwitchPlayerIframe from "./TwitchPlayerIframe";

export default {
  title: "Twitch/TwitchPlayerIframe",
  component: TwitchPlayerIframe,
  argTypes: {
    twitchChannel: {
      control: {
        type: "select",
        options: ["nickmercs", "shroud", "ninja", "pokimane"]
      }
    },
    backgroundShadow: {
      control: {
        type: "boolean"
      }
    },
    roundedCorners: {
      control: {
        type: "boolean"
      }
    },
    marginBottom: {
      control: {
        type: "boolean"
      }
    },
    fullWidth: {
      control: {
        type: "boolean"
      }
    },
    autoplay: {
      control: {
        type: "boolean"
      }
    },
    maxHeight: {
      control: {
        type: "boolean"
      }
    },
    muted: {
      control: {
        type: "boolean"
      }
    }
  }
};

const Story = args => <TwitchPlayerIframe {...args} />;

export const TwitchPlayer = Story.bind({});
TwitchPlayer.args = {
  twitchChannel: "nickmercs",
  backgroundShadow: true,
  roundedCorners: true,
  marginBottom: false,
  fullWidth: false,
  autoplay: true,
  maxHeight: false,
  muted: false
};
