import React, { useEffect } from "react";
import styled from "styled-components";

const DataTransparency = () => {
  useEffect(() => {
    analytics.page("Data Transparency");
  }, []);

  return (
    <MainDiv>
      <SubDiv>
        <pre style={{ whiteSpace: "pre-wrap" }}>
          <H1>Data Transparency</H1>
          <P>Last Modified: November 2nd, 2021</P>
          <H2>Scope</H2>
          <P>
            This Data Transparency statement applies to information that may be collected from you through our website,
            web applications, productions, or our member portal on https://efuse.gg (<b>“eFuse,”</b> <b>“We,”</b> or{" "}
            <b>“Us”</b>). We require certain information to provide services to you and, at times, we collect your
            information to be of better service and enhance your customer experience. We appreciate that you are
            trusting eFuse with information that is important to you, and we want to be transparent about how we use it.
          </P>
          <H2>Information Collected</H2>
          <P>
            We collect several types of information from users through our website or third-party service providers.
            Some of this information may be personally identifiable, such as your contact information or purchase
            related information. Some information collected may be about you is unable to be used to identify you such
            as location-based information. Other information we collect is done so in aggregate, combining data from a
            multitude of users and customers enabling us to track trends, overall experience, and customer preferences
            that enable us to provide you with increasingly better services. The information that we collect is either
            (a) provided directly by you, (b) by third-parties such as business partners, service providers, or
            affiliates, and (c) collected automatically from sources such as cookies, web beacons, or other tracking
            technologies. For more information about the information we collect, including more information about what
            we mean by the terms above,{" "}
            <a target="_blank" href="https://efuse.gg/privacy" rel="noreferrer">
              here
            </a>
            .
          </P>
          <H2>Use and Sharing</H2>
          <P>
            We use your personal information to provide you with the services you request and to provide offers and
            information regarding our products and services, and we may share your information with third-parties that
            we hire or contract with to help us provide products and services to you. Your information may be shared
            with trusted third-parties or valued brand partners, but your personal information will <b>NEVER</b> be sold
            for the benefit of eFuse or anyone else, as defined under the California Consumer Privacy Act. For more
            information on about how we share your information with others, please review Section 9 and 11 of our{" "}
            <a target="_blank" href="https://efuse.gg/privacy" rel="noreferrer">
              Privacy Policy
            </a>
            .
          </P>
          <H2>Your Rights</H2>
          <P>
            Depending on where you live, there are several regulations that provide data subjects (you) significant
            rights over how your personal data is collected, processed, and transferred by data controllers and
            processors (eFuse). Regulations such as the General Data Protection Regulation (GDPR) and the California
            Consumer Privacy Act (CCPA) both aim to guarantee strong protection for individuals regarding their personal
            data and apply to businesses that collect, use, or share data, whether the information is obtained online or
            offline. eFuse is, and is committed to remain, compliant with the most up to date data privacy regulations
            such as CCPA and GDPR. For more information on the CCPA and the GDPR, you can access the full text of each
            at the links below.
            <br />
            CCPA:{" "}
            <a
              target="_blank"
              rel="noreferrer"
              href="https://leginfo.legislature.ca.gov/faces/billTextClient.xhtml?bill_id=201720180SB1121"
            >
              https://leginfo.legislature.ca.gov/faces/billTextClient.xhtml?bill_id=201720180SB1121
            </a>
            <br />
            GDPR:{" "}
            <a target="_blank" rel="noreferrer" href="https://gdpr.eu/tag/gdpr/">
              https://gdpr.eu/tag/gdpr/
            </a>
            <br />
            In addition to our commitment to CCPA and GDPR compliance, eFuse offers other ways that you can take control
            of, and protect, your own data. Through the eFuse platform, you may:
            <ul>
              <li>Create or change your privacy preferences by editing your profile settings.</li>
              <li>Request removal from our database by emailing us at opt-out@efuse.io.</li>
            </ul>{" "}
          </P>
          <H2>Privacy Policy</H2>
          <P>
            You can view our full privacy policy here. California residents may have additional personal information
            rights and choices. Please see{" "}
            <a target="_blank" rel="noreferrer" href="https://efuse.gg/privacy#cali">
              Section 13
            </a>{" "}
            of our{" "}
            <a target="_blank" rel="noreferrer" href="https://efuse.gg/privacy">
              Privacy Policy
            </a>{" "}
            for more information.
          </P>
          <H2>How to Contact Us</H2>
          <P>
            If you have any questions about our Privacy Policy, Terms of Service, how we collect your personal data, or
            anything else related to our privacy practices, feel free to contact us at any time by email at
            privacy@efuse.io or by mail to the address below:
            <br />
            eFuse, Inc.
            <br />
            800 N High St., Floor 3
            <br />
            Columbus, OH 43215
          </P>
        </pre>
      </SubDiv>
    </MainDiv>
  );
};

const MainDiv = styled.div`
  background-color: rgb(236, 236, 236);
  padding: 20px;
`;

const SubDiv = styled.div`
  background-color: white;
  padding: 40px;
  max-width: 1000px;
  margin: auto;
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);
`;

const H1 = styled.h1`
  font-family: "Roboto", sans-serif;
  text-align: center;
  font-size: 2em;
  font-weight: 700;
  margin: 40px 0;
  word-wrap: break-word;
`;

const H2 = styled.h2`
  font-family: "Roboto", sans-serif;
  text-align: center;
  text-decoration: underline;
  font-size: 1.6em;
  font-weight: 600;
  margin: 30px 0;
  word-wrap: break-word;
`;

const P = styled.p`
  font-family: "Roboto", sans-serif;
  text-indent: 30px;
  word-wrap: break-word;
`;

export default DataTransparency;
