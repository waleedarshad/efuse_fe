interface EFImageProps {
  src: string;
  width?: number;
  height?: number;
  alt?: string;
  className?: string;
  loading?: "eager" | "lazy";
}

const EFImage = ({ src, width, height, alt, className, loading }: EFImageProps) => {
  if (!src) {
    return <></>;
  }

  return <img src={src} width={width} height={height} alt={alt} className={className} loading={loading} />;
};

export default EFImage;
