import React from "react";
import Style from "./PointRaceTitleGrid.module.scss";

const PointRaceTitleGrid = ({ mobileArrayDisplay, changeArrayStateTeams, changeArrayStatePlayers }) => {
  return (
    <div className={Style.titleGrid}>
      <div
        className={`${Style.titleContainer} ${mobileArrayDisplay === "teams" && Style.titleContainerActive}`}
        onClick={() => changeArrayStateTeams("teams")}
      >
        <p className={Style.title}>Team Standings</p>
        <hr className={Style.divider} />
      </div>
      <div
        className={`${Style.titleContainer} ${mobileArrayDisplay === "players" && Style.titleContainerActive}`}
        onClick={() => changeArrayStatePlayers("players")}
      >
        <p className={Style.title}>Players Rank</p>
        <hr className={Style.divider} />
      </div>
    </div>
  );
};

export default PointRaceTitleGrid;
