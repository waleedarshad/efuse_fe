import React from "react";
import Style from "./PointRaceHeader.module.scss";

const PointRaceHeader = ({
  primaryColor,
  secondaryColor,
  brandLogoUrl,
  tournamentLogoUrl,
  tournamentName,
  brandName
}) => {
  return (
    <>
      <div style={{ backgroundColor: primaryColor }} className={Style.mainHeader}>
        <img className={Style.brandLogo} src={brandLogoUrl} />
        <div className={Style.mainHeaderTitle}>{brandName} Leaderboard</div>
      </div>
      <div style={{ backgroundColor: secondaryColor }} className={Style.subHeader}>
        <img className={Style.tournamentLogoUrl} src={tournamentLogoUrl} />
        <div className={Style.subHeaderTitle}>{tournamentName}</div>
      </div>
    </>
  );
};

PointRaceHeader.defaultProps = {
  brandLogoUrl: "https://cdn.efuse.gg/uploads/static/global/erenaLogo.png",
  primaryColor: "#2e87ff",
  secondaryColor: "rgb(33, 99, 185)",
  brandName: "eRena"
};

export default PointRaceHeader;
