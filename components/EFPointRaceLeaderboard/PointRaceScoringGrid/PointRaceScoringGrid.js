import React from "react";
import Style from "./PointRaceScoringGrid.module.scss";
import DisplayPlayersList from "../DisplayPlayersList/DisplayPlayersList";
import DisplayTeamAccordions from "../DisplayTeamAccordions/DisplayTeamAccordions";

const PointRaceScoringGrid = ({ mobileArrayDisplay, teamsArray, playersArray }) => {
  return (
    <div className={Style.leaderboardGrid}>
      <div className={`${Style.leftBox} ${!(mobileArrayDisplay === "teams") && Style.mobileTeamsList}`}>
        <div className={Style.subTitle}>
          <p>Team</p>
          <p>Players</p>
          <p>Score</p>
        </div>
        <DisplayTeamAccordions teamsArray={teamsArray} />
      </div>

      <div className={`${Style.rightBox} ${!(mobileArrayDisplay === "players") && Style.mobileTeamsList}`}>
        <div className={Style.subTitle}>
          <p>Team</p>
          <p>Players</p>
          <p>Score</p>
        </div>
        <DisplayPlayersList playersArray={playersArray} />
      </div>
    </div>
  );
};

export default PointRaceScoringGrid;
