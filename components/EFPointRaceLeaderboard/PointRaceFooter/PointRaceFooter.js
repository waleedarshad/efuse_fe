import React from "react";
import Style from "./PointRaceFooter.module.scss";

const PointRaceFooter = () => {
  return (
    <div className={Style.footer}>
      <a href="https://efuse.gg" target="_blank">
        <div className={Style.poweredBy}>Powered by</div>
        <img className={Style.efuseLogoLeaderboard} src="https://cdn.efuse.gg/uploads/static/global/efuseLogo.png" />
        <img className={Style.efuseNameLeaderboard} src="https://cdn.efuse.gg/uploads/static/global/efuseName.png" />
      </a>
    </div>
  );
};

export default PointRaceFooter;
