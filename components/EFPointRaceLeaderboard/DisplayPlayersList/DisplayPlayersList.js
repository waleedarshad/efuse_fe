import React from "react";
import Style from "../EFPointRaceLeaderboard.module.scss";
import PointRaceLeaderboardItem from "../../Accordions/AccordionTriggers/PointRaceLeaderboardItem/PointRaceLeaderboardItem";

const DisplayPlayersList = ({ playersArray }) => {
  return (
    <div className={Style.teamList}>
      {playersArray.map((player, index) => {
        return (
          <div className={Style.addMarginBottom} key={player?._id}>
            <PointRaceLeaderboardItem
              name={player?.name}
              score={player?.score}
              scoreLabel="Points"
              rank={index + 1}
              isActive={true}
              displayDropdown={false}
            />
          </div>
        );
      })}
    </div>
  );
};

export default DisplayPlayersList;
