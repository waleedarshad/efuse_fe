import React from "react";
import Style from "./PointRaceButtons.module.scss";

const PointRaceButtons = ({ buttonActiveState, remainingOnClick, allTeamsOnClick }) => {
  return (
    <div className={Style.buttonWrapper}>
      <div className={Style.selectButtonsContainer}>
        <p
          className={`${Style.button} ${buttonActiveState === "remaining" && Style.buttonActiveState}`}
          onClick={() => remainingOnClick("remaining")}
        >
          Remaining
        </p>
        <p
          className={`${Style.button} ${buttonActiveState === "allTeams" && Style.buttonActiveState}`}
          onClick={() => allTeamsOnClick("allTeams")}
        >
          All Teams
        </p>
      </div>
    </div>
  );
};

export default PointRaceButtons;
