import React from "react";
import Style from "../EFPointRaceLeaderboard.module.scss";
import EFLeaderboardAccordion from "../../Accordions/EFLeaderboardAccordion/EFLeaderboardAccordion";
import PointRaceLeaderboardChild from "../../Accordions/AccordionChildren/PointRaceLeaderboardChild/PointRaceLeaderboardChild";

const DisplayTeamAccordions = ({ teamsArray }) => {
  return (
    <div className={Style.teamList}>
      {teamsArray?.map((team, index) => {
        return (
          <div className={Style.addMarginBottom} key={team?._id}>
            <EFLeaderboardAccordion
              isOpenOnMount={false}
              teamName={team?.name}
              teamScore={team?.score}
              teamRank={index + 1}
              isActive={true}
              displayDropdown={true}
              players={team?.players}
            >
              <div className={Style.teamDetails}>
                {team?.players?.map((player, index) => {
                  return (
                    <PointRaceLeaderboardChild
                      name={player?.name}
                      rank={index + 1}
                      score={player?.score}
                      scoreLabel="Points"
                      key={player?._id}
                    />
                  );
                })}
              </div>
            </EFLeaderboardAccordion>
          </div>
        );
      })}
    </div>
  );
};

export default DisplayTeamAccordions;
