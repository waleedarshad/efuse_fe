import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

import PointRaceButtons from "./PointRaceButtons/PointRaceButtons";
import PointRaceFooter from "./PointRaceFooter/PointRaceFooter";
import PointRaceScoringGrid from "./PointRaceScoringGrid/PointRaceScoringGrid";
import PointRaceTitleGrid from "./PointRaceTitleGrid/PointRaceTitleGrid";
import PointRaceHeader from "./PointRaceHeader/PointRaceHeader";
import Style from "./EFPointRaceLeaderboard.module.scss";
import { getPublicTeamsPointRace, getPublicPlayersPointRace } from "../../store/actions/erenaV2/erenaActionsV2";

const EFPointRaceLeaderboard = () => {
  const dispatch = useDispatch();

  const [mobileArrayDisplay, setMobileArrayDisplay] = useState("teams");
  const [buttonActiveState, setButtonActiveState] = useState("allTeams");

  const tournament = useSelector(state => state.erenaV2.tournament);
  const pointRace = useSelector(state => state.erenaV2.pointRace);

  useEffect(() => {
    getTeams();
    getPlayers();
    const getTeamsInterval = setInterval(() => {
      getTeams();
    }, 30000);
    const getPlayersInterval = setInterval(() => {
      getPlayers();
    }, 30000);
    return () => {
      clearInterval(getTeamsInterval);
      clearInterval(getPlayersInterval);
    };
  }, []);

  const getTeams = () => {
    if (tournament?._id) {
      dispatch(getPublicTeamsPointRace(tournament?._id));
    }
  };

  const getPlayers = () => {
    if (tournament?._id) {
      dispatch(getPublicPlayersPointRace(tournament?._id));
    }
  };

  // sort array
  const newTeamsArray = pointRace?.teams?.sort((p, p2) => {
    return p2.score - p.score;
  });

  // sort array
  const newPlayersArray = pointRace?.players?.sort((p, p2) => {
    return p2.score - p.score;
  });

  return (
    <div>
      <div className={Style.tournamentLeaderboardContainer}>
        <PointRaceHeader
          primaryColor={tournament?.primaryColor}
          secondaryColor={tournament?.secondaryColor}
          brandName={tournament?.brandName}
          tournamentName={tournament?.tournamentName}
          brandLogoUrl={tournament?.brandLogoUrl}
          tournamentLogoUrl={tournament?.tournamentLogoUrl}
        />

        <div className={Style.bodyContainer}>
          <div
            style={{
              backgroundImage: `${
                tournament?.primaryColor
                  ? `linear-gradient(${tournament.primaryColor}, rgb(29, 31, 39))`
                  : "linear-gradient(#2e87ff, rgb(29, 31, 39))"
              }`
            }}
            className={Style.backgroundGradient}
          />
          <PointRaceButtons
            buttonActiveState={buttonActiveState}
            remainingOnClick={() => setButtonActiveState("remaining")}
            allTeamsOnClick={() => setButtonActiveState("allTeams")}
          />

          <PointRaceTitleGrid
            mobileArrayDisplay={mobileArrayDisplay}
            changeArrayStateTeams={() => setMobileArrayDisplay("teams")}
            changeArrayStatePlayers={() => setMobileArrayDisplay("players")}
          />

          <PointRaceScoringGrid
            mobileArrayDisplay={mobileArrayDisplay}
            teamsArray={newTeamsArray}
            playersArray={newPlayersArray}
          />
        </div>
        <PointRaceFooter />
      </div>
    </div>
  );
};

export default EFPointRaceLeaderboard;
