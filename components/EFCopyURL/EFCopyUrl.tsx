import React from "react";
import { sendNotification } from "../../helpers/FlashHelper";
import Styles from "./EFCopyUrl.module.scss";
import EFRectangleButton from "../Buttons/EFRectangleButton/EFRectangleButton";

interface EFCopyUrlProps {
  url: string;
  showPreview?: boolean;
}

const EFCopyURL = ({ url, showPreview = false }: EFCopyUrlProps) => {
  const copyUrl = () => {
    navigator.clipboard.writeText(url).then(() => {
      sendNotification("URL copied!", "success", "Success");
    });
  };

  const seePreview = () => {
    const newWindow = window.open(url, "_blank", "noopener,noreferrer");
    if (newWindow) {
      newWindow.opener = null;
    }
  };

  return (
    <div className={Styles.container}>
      <input className={Styles.urlInput} value={url} readOnly />
      <div className={Styles.copyButtons}>
        <EFRectangleButton text="Copy" buttonType="button" colorTheme="light" shadowTheme="none" onClick={copyUrl} />
        {showPreview && (
          <EFRectangleButton
            text="Preview"
            buttonType="button"
            colorTheme="light"
            shadowTheme="none"
            onClick={seePreview}
          />
        )}
      </div>
    </div>
  );
};

export default EFCopyURL;
