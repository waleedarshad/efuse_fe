import React from "react";
import EFCopyUrl from "./EFCopyUrl";

export default {
  title: "EFCopyUrl",
  component: EFCopyUrl,
  argTypes: {
    url: {
      control: {
        type: "text"
      }
    },
    showPreview: {
      control: {
        type: "boolean"
      }
    }
  }
};

const Story = args => <EFCopyUrl {...args} />;

export const Default = Story.bind({});

Default.args = {
  url: "efuse.gg/lounge/featured",
  showPreview: false
};
