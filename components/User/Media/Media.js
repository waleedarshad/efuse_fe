import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import InfiniteScroll from "react-infinite-scroller";
import Router from "next/router";
import uniqueId from "lodash/uniqueId";
import EmptyComponent from "../../EmptyComponent/EmptyComponent";
import { getMedia, expandMedia } from "../../../store/actions/mediaActions";
import MediaItem from "./MediaItem/MediaItem";
import AnimatedLogo from "../../AnimatedLogo";
import MediaModal from "./MediaModal/MediaModal";
import { isUserLoggedIn } from "../../../helpers/AuthHelper";

const Media = ({ id }) => {
  const [currentUserLoaded, setCurrentUserLoaded] = useState(false);
  const currentUser = useSelector(state => state.auth.currentUser);
  const media = useSelector(state => state.media.media);
  const pagination = useSelector(state => state.media.pagination);
  const dispatch = useDispatch();

  useEffect(() => {
    const isLoggedIn = isUserLoggedIn();
    if (!isLoggedIn) {
      Router.push("/login");
    }
    dispatch(getMedia(id));
  }, []);

  const loadMore = () => {
    const nextPage = pagination ? pagination.page + 1 : 1;
    dispatch(getMedia(id, nextPage));
  };

  if (currentUser && !currentUserLoaded) {
    if (currentUser.id !== id) {
      analytics.page("Portfolio Media External");
      analytics.track("PORTFOLIO_MEDIA_VIEW_EXTERNAL", { userId: id });
    } else {
      analytics.page("Portfolio Media");
      analytics.track("PORTFOLIO_MEDIA_VIEW", { userId: id });
    }
    setCurrentUserLoaded(true);
  }

  const hasMore = pagination && pagination.hasNextPage;
  const mediaItems = media.map(m => (
    <MediaItem key={uniqueId()} media={m} expandMedia={e => dispatch(expandMedia(e))} />
  ));
  return (
    <>
      <MediaModal />
      <InfiniteScroll
        pageStart={1}
        loadMore={loadMore}
        hasMore={hasMore}
        loader={<AnimatedLogo key={0} theme="inline" />}
      >
        {media.length === 0 ? (
          <EmptyComponent text="No media has been posted" />
        ) : (
          <div className="card-columns">{mediaItems}</div>
        )}
      </InfiniteScroll>
    </>
  );
};

export default Media;
