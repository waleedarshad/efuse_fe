import { Card } from "react-bootstrap";
import ReactPlayer from "react-player";
import { useState } from "react";
import Skeleton from "react-loading-skeleton";
import { faPlayCircle, faEye } from "@fortawesome/pro-solid-svg-icons";
import Style from "./MediaItem.module.scss";
import { formatDate } from "../../../../helpers/GeneralHelper";
import ExpandMedia from "./ExpandMedia/ExpandMedia";

const MediaItem = ({ media, expandMedia }) => {
  const [loading, setLoading] = useState(true);
  const { file } = media;
  return (
    <Card className={`customCard pb-0 ${Style.mediaWrapper}`}>
      {file?.contentType?.includes("video") ? (
        <>
          <ReactPlayer width="100%" height="auto" url={file.url} className={Style.mediaPlayer} />
          <ExpandMedia icon={faPlayCircle} onClick={() => expandMedia(media)} Style={Style} />
        </>
      ) : (
        <>
          {loading && <Skeleton width="100%" height={271} />}
          <Card.Img
            onLoad={() => setLoading(false)}
            variant="top"
            src={file.url || file[0].url}
            className={Style.img}
            hidden={loading}
          />
          <ExpandMedia
            icon={faEye}
            onClick={() => {
              analytics.track("PORTFOLIO_MEDIA_ITEM_CLICK", { media });
              expandMedia(media);
            }}
            Style={Style}
          />
        </>
      )}
      <p className={Style.mediaCaption}>Uploaded on: {formatDate(media.createdAt)}</p>
    </Card>
  );
};

export default MediaItem;
