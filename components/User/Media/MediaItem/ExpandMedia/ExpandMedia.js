import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import EFButton from "../../../../Buttons/EFButton/EFButton";

const ExpandMedia = ({ icon, Style, onClick }) => (
  <EFButton className={Style.transparentBtn} onClick={onClick}>
    <FontAwesomeIcon icon={icon} />
  </EFButton>
);

export default ExpandMedia;
