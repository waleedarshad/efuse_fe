import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { closeMedia } from "../../../../store/actions/mediaActions";
import EFModal from "../../../Modals/EFModal/EFModal";
import DisplayMedia from "../../../Feed/Post/Comments/CommentMedia/DisplayMedia/DisplayMedia";

const MediaModal = () => {
  const dispatch = useDispatch();

  const media = useSelector(state => state.media.expandMedia);
  const show = useSelector(state => state.media.showModal);

  return (
    <EFModal isOpen={show} onClose={() => dispatch(closeMedia())} widthTheme="medium" displayCloseButton={false}>
      <DisplayMedia media={media} />
    </EFModal>
  );
};

export default MediaModal;
