import React, { Component } from "react";
import { connect } from "react-redux";

import profileLayout from "../../hoc/profileLayout";
import OpportunityListing from "../../Opportunities/OpportunityWrapper/OpportunityListing/OpportunityListing";
import { clearOpportunities, getOpportunities } from "../../../store/actions/opportunityActions";

class Opportunities extends Component {
  state = {
    currentUserLoaded: false
  };

  componentDidMount() {
    const { getOpportunities, clearOpportunities, query } = this.props;
    const { id } = query;
    clearOpportunities();
    getOpportunities(1, 9, {}, true, false, id);
  }

  componentDidUpdate() {
    const { id, currentUser } = this.props;

    if (currentUser && !this.state.currentUserLoaded) {
      if (currentUser.id !== id) {
        analytics.page("Portfolio Opportunities External");
        analytics.track("PORTFOLIO_OPPORTUNITIES_VIEW_EXTERNAL", { userId: id });
      } else {
        analytics.page("Portfolio Opportunities");
        analytics.track("PORTFOLIO_OPPORTUNITIES_VIEW", { userId: id });
      }
      this.setState({ ...this.state, currentUserLoaded: true });
    }
  }

  render() {
    const { query, t } = this.props;
    return <OpportunityListing t={t} profile profileId={query.id} />;
  }
}

const mapStateToProps = state => ({
  currentUser: state.auth.currentUser,
  pagination: state.opportunities.pagination
});

export default connect(mapStateToProps, { getOpportunities, clearOpportunities })(profileLayout(Opportunities));
