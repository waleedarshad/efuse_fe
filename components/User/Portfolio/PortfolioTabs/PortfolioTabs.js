import React, { Component } from "react";
import { Tabs, Tab } from "react-bootstrap";
import Style from "./PortfolioTabs.module.scss";

import ClipsComponent from "../ClipsComponent/ClipsComponent";
import SkillsComponent from "../SkillsComponent/SkillsComponent";

class PortfolioTabs extends Component {
  render() {
    const { tabHeader } = this.props;
    const { componentWrapper } = this.props;
    if (tabHeader) {
      return (
        <div className={Style.container}>
          <Tabs className={`tab-header ${Style.tabHeader}`} id="stats-tab">
            {tabHeader.map((tabLink, index) => {
              if (componentWrapper == "clips") {
                return (
                  <Tab
                    className={Style.tabBody}
                    key={index}
                    eventKey={tabLink.clipSection.title}
                    title={tabLink.clipSection.title}
                  >
                    <ClipsComponent title={tabLink.clipSection.title} />
                  </Tab>
                );
              }
              if (componentWrapper == "skills") {
                return (
                  <Tab
                    className={Style.tabBody}
                    key={index}
                    eventKey={tabLink.skillSection.title}
                    title={tabLink.skillSection.title}
                  >
                    <SkillsComponent />
                  </Tab>
                );
              }
            })}
          </Tabs>
        </div>
      );
    }
    return <div />;
  }
}
export default PortfolioTabs;
