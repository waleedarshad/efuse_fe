import PropTypes from "prop-types";
import { faPencilAlt, faTrash } from "@fortawesome/pro-solid-svg-icons";

import { withCdn } from "../../../../../common/utils";
import { formatDateMonthYear } from "../../../../../helpers/GeneralHelper";
import ConfirmAlert from "../../../../ConfirmAlert/ConfirmAlert";
import { EFHtmlParser } from "../../../../EFHtmlParser/EFHtmlParser";
import EducationExperienceModal from "../../../../GlobalModals/EducationExperienceModal/EducationExperienceModal";
import ExperienceIcon from "../../Experience/ExperienceIcon/ExperienceIcon";
import Style from "./DisplayExperience.module.scss";
import EFCircleIconButton from "../../../../Buttons/EFCircleIconButton/EFCircleIconButton";

const DisplayExperience = ({ experience, displayHr, onRemove, isCurrentUser }) => {
  const { school, degree, fieldofstudy, grade, startDate, endDate, present, description, image } = experience;
  const formattedDate = present
    ? `${formatDateMonthYear(startDate)} - Present`
    : `${formatDateMonthYear(startDate)} - ${formatDateMonthYear(endDate)}`;

  return (
    <div className={Style.container}>
      <div className={Style.socialContainer}>
        <ExperienceIcon url={(image && image.url) || withCdn("/static/images/briefcase.png")} />
        <div className={Style.textContainer}>
          <h4 className={Style.socialTitle}>
            {school}

            {isCurrentUser && (
              <>
                <EducationExperienceModal experience={experience} edit>
                  <EFCircleIconButton colorTheme="transparent" shadowTheme="none" icon={faPencilAlt} />
                </EducationExperienceModal>
                <ConfirmAlert
                  title="This action can't be undone"
                  message="Are you sure you want to remove?"
                  onYes={onRemove}
                >
                  <EFCircleIconButton colorTheme="transparent" shadowTheme="none" icon={faTrash} onClick={onRemove} />
                </ConfirmAlert>
              </>
            )}
          </h4>
          <p className={Style.socialName}>
            {degree}{" "}
            {fieldofstudy && (
              <span>
                {degree && <>-</>} {fieldofstudy}
              </span>
            )}
          </p>
          {grade && <p className={Style.grade}>{grade}</p>}
          <div className={Style.socialTime}>
            <p>{formattedDate}</p>
            {/* <p className={Style.schoolLevel}>{currentLevel.label}</p> */}
          </div>
          <div className={Style.socialDescription}>
            <EFHtmlParser>{description}</EFHtmlParser>
          </div>
        </div>
      </div>
      {displayHr && <hr className={Style.divider} />}
    </div>
  );
};

DisplayExperience.propTypes = {
  experience: PropTypes.object.isRequired,
  displayHr: PropTypes.bool,
  onRemove: PropTypes.func.isRequired,
  isCurrentUser: PropTypes.bool.isRequired
};

export default DisplayExperience;
