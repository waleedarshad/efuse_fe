import EducationExperience from "./EducationExperience";
import portfolioTabsLayout from "../../../hoc/portfolioTabsLayout/portfolioTabsLayout";

export default portfolioTabsLayout({
  feature: "education_section",
  id: "education-section",
  tabs: [
    {
      title: "Education",
      component: EducationExperience,
      tabFeature: "education_tab",
      auth: "educationExperience"
    }
  ]
});
