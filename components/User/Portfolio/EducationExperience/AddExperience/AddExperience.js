import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Form, Col, FormControl } from "react-bootstrap";
import InputRow from "../../../../InputRow/InputRow";
import InputField from "../../../../InputField/InputField";
import InputLabel from "../../../../InputLabel/InputLabel";
import Checkbox from "../../../../Checkbox/Checkbox";
import {
  getMonthFromDate,
  getYearFromDate,
  monthsOptions,
  yearsOptions,
  rescueNil
} from "../../../../../helpers/GeneralHelper";
import ActionButtonGroup from "../../../../Layouts/Internal/ActionButtonGroup/ActionButtonGroup";
import { manageExperience } from "../../../../../store/actions/portfolioActions";
import SelectBox from "../../../../SelectBox/SelectBox";
import levelList from "../data/educationLevels.json";
import Style from "./AddExperience.module.scss";
import ImageUploader from "../../../../ImageUploader/ImageUploader";
import EFHtmlInput from "../../../../EFHtmlInput/EFHtmlInput";

const AddExperience = ({
  edit,
  experience,
  removeCancelOption,
  onboardingAnalyticsOptions,
  closeModal,
  resetOnboardingState
}) => {
  const [validated, setValidated] = useState(false);
  const [valuesSet, setValuesSet] = useState(false);
  const [startMonth, setStartMonth] = useState("");
  const [startYear, setStartYear] = useState("");
  const [endMonth, setEndMonth] = useState("");
  const [endYear, setEndYear] = useState("");
  const [startDateError, setStartDateError] = useState(false);
  const [endDateError, setEndDateError] = useState(false);
  const [level, setLevel] = useState(0);
  const [school, setSchool] = useState("");
  const [degree, setDegree] = useState("");
  const [fieldofstudy, setFieldofstudy] = useState("");
  const [grade, setGrade] = useState("");
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);
  const [present, setPresent] = useState(false);
  const [image, setImage] = useState({});
  const [description, setDescription] = useState("");
  const [_id, setId] = useState("");
  const { inProgress } = useSelector(state => state.portfolio);
  const dispatch = useDispatch();

  useEffect(() => {
    if (edit && !valuesSet) {
      setLevel(experience.level);
      setSchool(experience.school);
      setDegree(experience.degree);
      setFieldofstudy(experience.fieldofstudy);
      setGrade(experience.grade);
      setStartDate(experience.startDate);
      setEndDate(experience.endDate);
      setPresent(experience.present);
      setImage(experience.image);
      setDescription(experience.description);
      setId(experience._id);
      setStartMonth(getMonthFromDate(experience.startDate));
      setStartYear(getYearFromDate(experience.startDate));
      setEndMonth(getMonthFromDate(experience.endDate));
      setEndYear(getYearFromDate(experience.endDate));
      setValuesSet(true);
    }
  }, [edit, valuesSet]);

  const getUpdatedDate = (year, month) => {
    return new Date(year, month - 1);
  };

  const onPresentChange = checked => {
    setPresent(checked);
    setEndDate(getUpdatedDate(endYear, endMonth));
  };

  const onEditorStateChange = editorState => {
    setDescription(editorState);
  };

  const onDrop = selectedFile => {
    setImage(selectedFile[0]);
  };

  const onSubmit = event => {
    event.preventDefault();
    const educationExperience = {
      level,
      school,
      degree,
      fieldofstudy,
      grade,
      startDate,
      endDate,
      present,
      image,
      description,
      _id
    };

    if (!validated) {
      setValidated(true);
    }
    const startDateGreater = new Date(startDate) >= new Date(endDate);
    const endDateSmaller = new Date(endDate) <= new Date(startDate);
    if (!present && startDate && startDateGreater) {
      setStartDateError(true);
    } else if (!present && endDate && endDateSmaller) {
      setEndDateError(true);
    } else if (event.currentTarget.checkValidity()) {
      saveExperience();
      if (onboardingAnalyticsOptions) {
        analytics.track("PORTFOLIO_EDUCATION_EXPERIENCE_CREATE", {
          experience: educationExperience,
          location: "onboarding"
        });
      } else {
        analytics.track("PORTFOLIO_EDUCATION_EXPERIENCE_CREATE", {
          experience: educationExperience
        });
      }
      if (closeModal) closeModal();
      if (resetOnboardingState) resetOnboardingState();
    }
  };

  const saveExperience = () => {
    const data = new FormData();
    const educationExperience = {
      level,
      school,
      degree,
      fieldofstudy,
      grade,
      startDate,
      endDate,
      present,
      image,
      description,
      _id
    };
    Object.keys(educationExperience).forEach(key => {
      if (educationExperience[key] !== undefined) {
        if (!(key === "endDate" && educationExperience.present)) {
          data[key] = educationExperience[key];
          data.append(key, educationExperience[key]);
        }
      }
    });
    dispatch(manageExperience("/portfolio/education_experience", data));
  };

  return (
    <Form noValidate validated={validated} onSubmit={onSubmit}>
      <InputRow>
        <Col sm={12}>
          <InputLabel theme="darkColor">School Level</InputLabel>
          <SelectBox
            name="level"
            validated={validated}
            options={levelList}
            size="lg"
            theme="whiteShadow"
            onChange={e => setLevel(e.target.value)}
            value={level}
            required
            errorMessage="School level is required"
          />
        </Col>
      </InputRow>
      <InputRow>
        <Col sm={6}>
          <InputLabel theme="darkColor">School</InputLabel>
          <InputField
            name="school"
            placeholder="Ex: Ohio State University"
            theme="whiteShadow"
            size="lg"
            onChange={e => setSchool(e.target.value)}
            value={school}
            required
            errorMessage="School is required"
            maxLength={40}
          />
        </Col>
        <Col sm={6}>
          <InputLabel theme="darkColor">Degree</InputLabel>
          <InputField
            name="degree"
            placeholder="Ex: Bachelor's"
            theme="whiteShadow"
            size="lg"
            onChange={e => setDegree(e.target.value)}
            value={degree}
            maxLength={30}
          />
        </Col>
      </InputRow>
      <InputRow>
        <Col sm={6}>
          <InputLabel theme="darkColor">Field of study</InputLabel>
          <InputField
            name="fieldofstudy"
            placeholder="Ex: Computer Science"
            theme="whiteShadow"
            size="lg"
            onChange={e => setFieldofstudy(e.target.value)}
            value={fieldofstudy}
            maxLength={40}
          />
        </Col>
        <Col sm={6}>
          <InputLabel theme="darkColor">Grade (if still attending)</InputLabel>
          <InputField
            name="grade"
            placeholder="Ex: Sophomore"
            theme="whiteShadow"
            size="lg"
            onChange={e => setGrade(e.target.value)}
            value={grade}
            maxLength={30}
          />
        </Col>
      </InputRow>
      <InputRow>
        <Col sm={present ? "6" : "3"}>
          <InputLabel theme="darkColor">Start Date</InputLabel>
          <SelectBox
            options={monthsOptions()}
            validated={validated}
            required
            errorMessage="Month is required"
            value={startMonth}
            name="startMonth"
            onChange={e => {
              setStartMonth(e.target.value);
              setStartDate(getUpdatedDate(startYear, e.target.value));
            }}
            theme="whiteShadow"
          />
          {startDateError && (
            <FormControl.Feedback type="invalid" style={{ display: "block" }}>
              Start Date can&apos;t be equal or greater than end date
            </FormControl.Feedback>
          )}
        </Col>
        <Col sm={present ? "6" : "3"} className={Style.emptyLabel}>
          <InputLabel theme="darkColor" />
          <SelectBox
            options={yearsOptions()}
            validated={validated}
            required
            errorMessage="Year is required"
            value={startYear}
            name="startYear"
            onChange={e => {
              setStartYear(e.target.value);
              setStartDate(getUpdatedDate(e.target.value, startMonth));
            }}
            theme="whiteShadow"
          />
        </Col>

        {/* end date starts here */}
        {!present && (
          <>
            <Col sm="3">
              <InputLabel theme="darkColor">End Date</InputLabel>
              <SelectBox
                options={monthsOptions()}
                validated={validated}
                required
                errorMessage="Month is required"
                value={endMonth}
                name="endMonth"
                onChange={e => {
                  setEndMonth(e.target.value);
                  setEndDate(getUpdatedDate(endYear, e.target.value));
                }}
                theme="whiteShadow"
              />
              {endDateError && (
                <FormControl.Feedback type="invalid" style={{ display: "block" }}>
                  End date can&apos;t be equal or less than start date
                </FormControl.Feedback>
              )}
            </Col>
            <Col sm="3" className={Style.emptyLabel}>
              <InputLabel theme="darkColor" />
              <SelectBox
                options={yearsOptions()}
                validated={validated}
                required
                errorMessage="Year is required"
                value={endYear}
                name="endYear"
                onChange={e => {
                  setEndYear(e.target.value);
                  setEndDate(getUpdatedDate(e.target.value, endMonth));
                }}
                theme="whiteShadow"
              />
            </Col>
          </>
        )}
      </InputRow>

      <InputRow>
        <Col>
          <Checkbox
            custom
            name="present"
            type="checkbox"
            label="Present?"
            id="presentExp"
            theme="darkColor"
            checked={present}
            value={present}
            onChange={e => onPresentChange(e.target.checked)}
          />
        </Col>
      </InputRow>
      <InputRow>
        <Col sm={12}>
          <InputLabel theme="darkColor" optional>
            Education Logo
          </InputLabel>
        </Col>
        <Col sm={12}>
          <ImageUploader
            text="Select Custom Image"
            name="image"
            onDrop={onDrop}
            theme="lightButton"
            value={rescueNil(image, "url")}
            showCropper={false}
          />
        </Col>
      </InputRow>
      <InputRow>
        <Col sm={12}>
          <InputLabel theme="darkColor">Description/Activities/Societies</InputLabel>
        </Col>
        <Col sm={12}>
          <EFHtmlInput value={description} onChange={onEditorStateChange} />
        </Col>
      </InputRow>
      <ActionButtonGroup disabled={inProgress} removeCancelOption={removeCancelOption} />
    </Form>
  );
};

export default AddExperience;
