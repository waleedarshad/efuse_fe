import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "next/router";
import { faPlus } from "@fortawesome/pro-solid-svg-icons";

import DisplayExperience from "./DisplayExperience/DisplayExperience";
import { removeData } from "../../../../store/actions/portfolioActions";
import EducationExperienceModal from "../../../GlobalModals/EducationExperienceModal/EducationExperienceModal";
import AddButton from "../AddButton/AddButton";

class EducationExperience extends Component {
  render() {
    const { user, router, currentUser, mockExternalUser } = this.props;
    let isCurrentUser = currentUser && user._id === currentUser._id;
    if (mockExternalUser) {
      isCurrentUser = false;
    }
    let content = <></>;
    const { educationExperience } = user;
    if (user && educationExperience) {
      content = educationExperience.map((experience, index) => {
        return (
          <DisplayExperience
            key={index}
            experience={experience}
            displayHr={educationExperience.length !== index + 1}
            onEdit={() => "empty"}
            onRemove={() => this.props.removeData("/portfolio/education_experience", experience._id)}
            isCurrentUser={isCurrentUser}
          />
        );
      });
    }

    return (
      <>
        {content}
        {isCurrentUser && (
          <EducationExperienceModal>
            <AddButton desc="Add Education Experience" icon={faPlus} />
          </EducationExperienceModal>
        )}
      </>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user.currentUser,
  currentUser: state.auth.currentUser,
  mockExternalUser: state.portfolio.mockExternalUser
});

export default connect(mapStateToProps, {
  removeData
})(withRouter(EducationExperience));
