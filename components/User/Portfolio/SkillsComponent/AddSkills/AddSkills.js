import React, { Component } from "react";
import { connect } from "react-redux";
import { Form, Col } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck } from "@fortawesome/pro-solid-svg-icons";
import uniqueId from "lodash/uniqueId";
import InputRow from "../../../../InputRow/InputRow";
import SelectBox from "../../../../SelectBox/SelectBox";
import InputLabel from "../../../../InputLabel/InputLabel";
import ActionButtonGroup from "../../../../Layouts/Internal/ActionButtonGroup/ActionButtonGroup";
import { manageSkills } from "../../../../../store/actions/portfolioActions";
import { dispatchNotification } from "../../../../../store/actions/notificationActions";
import businessSkills from "../BusinessSkills/data/businessSkills.json";
import gamingSkills from "../GamingSkills/data/gamingSkills.json";
import Style from "../SkillsComponent.module.scss";

class AddSkills extends Component {
  constructor(props) {
    super(props);
    this.state = {
      validated: false,
      valuesSet: false,
      skillInfo: {
        skill: "",
        _id: ""
      },
      addedSkills: []
    };
  }

  static getDerivedStateFromProps(props, state) {
    return props.edit && !state.valuesSet
      ? {
          skillInfo: {
            ...props.gamingSkills
          },
          valuesSet: true
        }
      : {};
  }

  onChange = () => {
    // eslint-disable-next-line no-restricted-globals
    if (event.target.value !== "-1") {
      this.setState(prevState => ({
        skillInfo: {
          ...prevState.skillInfo,
          // eslint-disable-next-line no-restricted-globals
          [event.target.name]: event.target.value
        }
      }));
      // eslint-disable-next-line no-restricted-globals
      if (this.state.addedSkills.indexOf(event.target.value) < 0) {
        this.setState(prevState => ({
          // eslint-disable-next-line no-restricted-globals
          addedSkills: [...prevState.addedSkills, event.target.value]
        }));
      }
    }
  };

  onSubmit = event => {
    event.preventDefault();
    const { validated } = this.state;
    if (!validated) {
      this.setState({ validated: true });
    }
    analytics.track(`PORTFOLIO_${this.props.type.toUpperCase()}_SKILLS_CREATE`, {
      newSkills: this.state.addedSkills
    });
    this.props.manageSkills(`/portfolio/${this.props.type}_skill`, this.state.addedSkills);
    if (this.props.closeModal) this.props.closeModal();
  };

  delete(skill) {
    this.setState(prevState => ({
      addedSkills: prevState.addedSkills.filter(item => {
        return item !== skill;
      })
    }));
  }

  render() {
    const { inProgress, type } = this.props;
    const { validated, skillInfo } = this.state;
    const { skill } = skillInfo;
    const { addedSkills } = this.state;

    const skillsList = type === "Gaming" ? gamingSkills : businessSkills;
    const selectedSkills = [];
    addedSkills.forEach(skillItem => {
      const selected = skillsList.find(item => item.value === parseInt(skillItem, 10));
      if (selected) {
        selectedSkills.push(
          <button
            type="button"
            onClick={() => this.delete(skillItem)}
            key={uniqueId()}
            className={Style.addItemContainer}
          >
            <div className={Style.addItem}>
              {selected.label}
              <FontAwesomeIcon className={Style.check} icon={faCheck} />
            </div>
          </button>
        );
      }
    });

    return (
      <Form noValidate validated={validated} onSubmit={this.onSubmit}>
        <InputRow>
          <Col sm={12}>
            <InputLabel theme="darkColor">Select a {type} Skill</InputLabel>
            <SelectBox
              name="skill"
              validated={validated}
              options={skillsList}
              size="lg"
              theme="whiteShadow"
              onChange={this.onChange}
              value={skill}
              required
              errorMessage={`${type} Skill is required`}
            />
          </Col>
        </InputRow>
        {selectedSkills}
        <ActionButtonGroup disabled={inProgress} />
      </Form>
    );
  }
}

const mapStateToProps = state => ({
  inProgress: state.portfolio.inProgress
});

export default connect(mapStateToProps, {
  manageSkills,
  dispatchNotification
})(AddSkills);
