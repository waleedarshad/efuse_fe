import portfolioTabsLayout from "../../../hoc/portfolioTabsLayout/portfolioTabsLayout";
import BusinessSkills from "./BusinessSkills/Index";
import GamingSkills from "./GamingSkills/Index";

export default portfolioTabsLayout({
  feature: "skills_section",
  id: "skills-section",
  tabs: [
    {
      title: "Business Skills",
      component: BusinessSkills,
      tabFeature: "business_skills_section",
      auth: "businessSkills"
    },
    { title: "Gaming Skills", component: GamingSkills, tabFeature: "gaming_skills_section", auth: "gamingSkills" }
  ]
});
