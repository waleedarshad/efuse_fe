import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "next/router";
import { faPlus } from "@fortawesome/pro-solid-svg-icons";

import AddButton from "../../AddButton/AddButton";
import SkillsLayout from "../SkillsLayout/SkillsLayout";
import SkillsModal from "../../../../GlobalModals/SkillsModal/SkillsModal";

class Index extends Component {
  render() {
    const { user, router, currentUser, mockExternalUser } = this.props;
    let isCurrentUser = currentUser && user._id === currentUser._id;
    if (mockExternalUser) {
      isCurrentUser = false;
    }
    let hasGamingSkills = false;
    if (user.gamingSkills) {
      hasGamingSkills = user.gamingSkills.length > 0;
    }
    const { gamingSkills } = user;

    return (
      <>
        {isCurrentUser && (
          <SkillsModal type="Gaming" trackAnalytics={() => analytics.track("PORTFOLIO_GAMING_SKILLS_MODAL_OPEN")}>
            <AddButton desc="Add Gaming Skills" icon={faPlus} onClick={this.loadModal} />
          </SkillsModal>
        )}
        {hasGamingSkills && <SkillsLayout skills={gamingSkills} type="Gaming" isCurrentUser={isCurrentUser} />}
      </>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user.currentUser,
  currentUser: state.auth.currentUser,
  mockExternalUser: state.portfolio.mockExternalUser
});

export default connect(mapStateToProps, {})(withRouter(Index));
