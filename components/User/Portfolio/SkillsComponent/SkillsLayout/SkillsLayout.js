import { connect } from "react-redux";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMinusCircle } from "@fortawesome/pro-solid-svg-icons";
import React, { Component } from "react";
import businessSkills from "../BusinessSkills/data/businessSkills.json";
import ConfirmAlert from "../../../../ConfirmAlert/ConfirmAlert";
import gamingSkills from "../GamingSkills/data/gamingSkills.json";
import Style from "./SkillsLayout.module.scss";
import { deleteSkill } from "../../../../../store/actions/portfolioActions";

class SkillsLayout extends Component {
  onSubmit(id) {
    analytics.track(`PORTFOLIO_${this.props.type.toUpperCase()}_SKILLS_DELETE`, {
      skillId: id
    });
    this.props.deleteSkill(`/portfolio/${this.props.type}_skill`, id);
  }

  render() {
    const { type, isCurrentUser, skills } = this.props;
    const skillsList = type == "Gaming" ? gamingSkills : businessSkills;
    const content = [];
    skills.map((skill, i) =>
      skillsList.find(item => {
        if (item.value == skill.value) {
          content.push(
            <div key={i} className={`${Style.skillsContainer} ${isCurrentUser && Style.marginTop}`}>
              <p className={Style.skillTitle}>{item.label}</p>
              {isCurrentUser && (
                <ConfirmAlert
                  title="This action can't be undone"
                  message="Are you sure you want to remove this skill?"
                  onYes={() => this.onSubmit(skill.value)}
                >
                  <FontAwesomeIcon icon={faMinusCircle} className={Style.icon} />
                </ConfirmAlert>
              )}
            </div>
          );
        }
      })
    );

    return <div className={Style.container}>{content}</div>;
  }
}

const mapStateToProps = state => ({
  user: state.user.currentUser,
  currentUser: state.auth.currentUser
});

export default connect(mapStateToProps, {
  deleteSkill
})(SkillsLayout);
