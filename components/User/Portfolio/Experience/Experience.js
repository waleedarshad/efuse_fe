import portfolioTabsLayout from "../../../hoc/portfolioTabsLayout/portfolioTabsLayout";
import EducationExperience from "./EducationExperience/EducationExperience";

export default portfolioTabsLayout({
  feature: "experience_section",
  id: "experience-section",
  tabs: [
    {
      title: "Education",
      component: EducationExperience,
      tabFeature: "education_experience",
      auth: "educationExperience"
    }
  ]
});
