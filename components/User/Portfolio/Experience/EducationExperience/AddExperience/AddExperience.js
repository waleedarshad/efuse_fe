import React, { Component } from "react";
import { connect } from "react-redux";
import { Form, Col, FormControl } from "react-bootstrap";
import moment from "moment";

import InputRow from "../../../../../InputRow/InputRow";
import InputField from "../../../../../InputField/InputField";
import InputLabel from "../../../../../InputLabel/InputLabel";
import DatePickerInput from "../../../../../DatePickerInput/DatePickerInput";
import DatepickerMonthElement from "../../../../../DatepickerMonthElement/DatepickerMonthElement";
import Checkbox from "../../../../../Checkbox/Checkbox";
import { toMoment } from "../../../../../../helpers/MomentHelper";
import ActionButtonGroup from "../../../../../Layouts/Internal/ActionButtonGroup/ActionButtonGroup";
import { manageData } from "../../../../../../store/actions/portfolioActions";
import Error from "../../../../../Error/Error";
import EFHtmlInput from "../../../../../EFHtmlInput/EFHtmlInput";
import EFPrimaryModal from "../../../../../Modals/EFPrimaryModal/EFPrimaryModal";

class AddExperience extends Component {
  constructor(props) {
    super(props);

    this.state = {
      validated: false,
      valuesSet: false,
      educationExperience: {
        title: "",
        subTitle: "",
        startDate: null,
        endDate: null,
        present: false,
        description: "",
        _id: ""
      },
      startDateError: false,
      endDateError: false
    };
  }

  static getDerivedStateFromProps(props, state) {
    return props.edit && !state.valuesSet
      ? {
          educationExperience: {
            ...props.experience
          },
          valuesSet: true
        }
      : {};
  }

  onChange = () => {
    this.setState({
      educationExperience: {
        ...this.state.educationExperience,
        [event.target.name]: event.target.value
      }
    });
  };

  onDateChange = (name, date) => {
    this.setState({
      educationExperience: {
        ...this.state.educationExperience,
        [name]: toMoment(date)
      },
      [`${name}Error`]: false
    });
  };

  onPresentChange = event => {
    this.setState({
      educationExperience: {
        ...this.state.educationExperience,
        present: event.target.checked
      }
    });
  };

  onEditorStateChange = editorState => {
    this.setState({
      educationExperience: {
        ...this.state.educationExperience,
        description: editorState
      }
    });
  };

  onSubmit = event => {
    event.preventDefault();
    const { validated, educationExperience } = this.state;
    const { startDate, endDate, present } = educationExperience;
    if (!validated) {
      this.setState({ validated: true });
    }
    if (present && !startDate) {
      this.setState({ startDateError: true });
    } else if (!present && (!startDate || !endDate)) {
      this.setState({ startDateError: !startDate, endDateError: !endDate });
    } else if (event.currentTarget.checkValidity() && ((present && startDate) || (!present && startDate && endDate))) {
      let educationExperienceData = { ...educationExperience };
      educationExperienceData = {
        ...educationExperienceData,
        description: ValidationHelper.validateHtml(educationExperienceData.description)
      };
      analytics.track("PORTFOLIO_EDUCATION_EXPERIENCE_CREATE", {
        experience: educationExperienceData
      });
      this.props.manageData("/portfolio/education_experience", educationExperienceData);
    }
  };

  render() {
    const { show, onHide, inProgress } = this.props;
    const { validated, educationExperience, startDateError, endDateError } = this.state;
    const { title, subTitle, startDate, endDate, present, description } = educationExperience;
    return (
      <EFPrimaryModal
        title="Add Education Experience"
        widthTheme="medium"
        isOpen={show}
        onClose={onHide}
        allowBackgroundClickClose={false}
      >
        <Error />
        <Form noValidate validated={validated} onSubmit={this.onSubmit}>
          <InputRow>
            <Col sm={6}>
              <InputLabel theme="darkColor">Title</InputLabel>
              <InputField
                name="title"
                placeholder="Add Title"
                theme="whiteShadow"
                size="lg"
                onChange={this.onChange}
                value={title}
                required
                errorMessage="Title is required"
                maxLength={50}
              />
            </Col>
            <Col sm={6}>
              <InputLabel theme="darkColor">Sub Title</InputLabel>
              <InputField
                name="subTitle"
                placeholder="Add Sub title"
                theme="whiteShadow"
                size="lg"
                onChange={this.onChange}
                value={subTitle}
                required
                errorMessage="Sub title is required"
                maxLength={50}
              />
            </Col>
          </InputRow>
          <InputRow>
            <Col>
              <Checkbox
                custom
                name="present"
                type="checkbox"
                label="Present?"
                id="presentExp"
                theme="darkColor"
                checked={present}
                value={present}
                onChange={this.onPresentChange}
              />
            </Col>
          </InputRow>
          <InputRow>
            <Col sm={present ? 12 : 6}>
              <InputLabel theme="darkColor">Start Date</InputLabel>
              <DatePickerInput
                date={startDate}
                dateHandler={date => this.onDateChange("startDate", date)}
                isOutsideRange={day => day.isAfter(moment(endDate))}
                renderMonthElement={calenderProps => <DatepickerMonthElement {...calenderProps} includeCurrentYear />}
                numberOfMonths={1}
                direction="down"
              />
              {startDateError && (
                <FormControl.Feedback type="invalid" style={{ display: "block" }}>
                  Start Date is required
                </FormControl.Feedback>
              )}
            </Col>
            {!present && (
              <Col sm={6}>
                <InputLabel theme="darkColor">End Date</InputLabel>
                <DatePickerInput
                  date={endDate}
                  dateHandler={date => this.onDateChange("endDate", date)}
                  isOutsideRange={day => day.isBefore(moment(startDate))}
                  renderMonthElement={calenderProps => <DatepickerMonthElement {...calenderProps} includeCurrentYear />}
                  numberOfMonths={1}
                  direction="down"
                />
                {endDateError && (
                  <FormControl.Feedback type="invalid" style={{ display: "block" }}>
                    End Date is required
                  </FormControl.Feedback>
                )}
              </Col>
            )}
          </InputRow>
          <InputRow>
            <Col sm={12}>
              <InputLabel theme="darkColor">Description</InputLabel>
            </Col>
            <Col sm={12}>
              <EFHtmlInput value={description} onChange={this.onEditorStateChange} />
            </Col>
          </InputRow>
          <ActionButtonGroup onCancel={onHide} disabled={inProgress} />
        </Form>
      </EFPrimaryModal>
    );
  }
}

const mapStateToProps = state => ({
  inProgress: state.portfolio.inProgress
});

export default connect(mapStateToProps, { manageData })(AddExperience);
