import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "next/router";
import { faPlus } from "@fortawesome/pro-solid-svg-icons";

import AddButton from "../../AddButton/AddButton";
import AddExperience from "./AddExperience/AddExperience";
import DisplayExperience from "./DisplayExperience/DisplayExperience";
import { togglePortfolioModal, removeData } from "../../../../../store/actions/portfolioActions";

class EducationExperience extends Component {
  state = {
    edit: false,
    experience: null
  };

  loadModal = () => {
    analytics.track("PORTFOLIO_EDUCATION_EXPERIENCE_MODAL_OPEN");
    this.props.togglePortfolioModal(true, "education");
  };

  onHide = () => {
    analytics.track("PORTFOLIO_EDUCATION_EXPERIENCE_MODAL_CLOSE");
    this.props.togglePortfolioModal(false);
    this.setState({ edit: false, experience: null });
  };

  editModal = experience => {
    analytics.track("PORTFOLIO_EDUCATION_EXPERIENCE_MODAL_OPEN_FROM_EDIT");
    this.props.togglePortfolioModal(true, "education");
    this.setState({ edit: true, experience });
  };

  render() {
    const { user, modal, modalSection, router, currentUser, mockExternalUser } = this.props;
    const { edit, experience } = this.state;
    let isCurrentUser = currentUser && user._id === currentUser._id;
    if (mockExternalUser) {
      isCurrentUser = false;
    }
    let content = <></>;
    const { educationExperience } = user;
    if (user && educationExperience) {
      content = educationExperience.map((experience, index) => {
        return (
          <DisplayExperience
            key={index}
            experience={experience}
            displayHr={educationExperience.length !== index + 1}
            onEdit={() => this.editModal(experience)}
            onRemove={() => {
              analytics.track("PORTFOLIO_EDUCATION_EXPERIENCE_DELETE", {
                experienceId: experience._id
              });
              this.props.removeData("/portfolio/education_experience", experience._id);
            }}
            isCurrentUser={isCurrentUser}
          />
        );
      });
    }

    return (
      <div>
        {content}
        {isCurrentUser && <AddButton desc="Add Education Experience" icon={faPlus} onClick={this.loadModal} />}
        {modal && modalSection === "education" && (
          <AddExperience show={modal} onHide={this.onHide} edit={edit} experience={experience} />
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user.currentUser,
  modal: state.portfolio.modal,
  modalSection: state.portfolio.modalSection,
  currentUser: state.auth.currentUser,
  mockExternalUser: state.portfolio.mockExternalUser
});

export default connect(mapStateToProps, {
  togglePortfolioModal,
  removeData
})(withRouter(EducationExperience));
