import { withCdn } from "../../../../../common/utils";
import Style from "./ExperienceIcon.module.scss";

const ExperienceIcon = ({ url }) => {
  return <img className={Style.experienceIcon} src={url || withCdn("/static/images/briefcase.png")} />;
};

export default ExperienceIcon;
