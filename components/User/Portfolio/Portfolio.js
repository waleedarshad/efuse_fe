import isEmpty from "lodash/isEmpty";
import { NextSeo } from "next-seo";
import getConfig from "next/config";
import React, { Component } from "react";
import { DragDropContext, Draggable, Droppable } from "react-beautiful-dnd";
import { connect } from "react-redux";
import { faArrowLeft, faArrowRight } from "@fortawesome/pro-solid-svg-icons";

import { isOwner as checkIfOwner } from "../../../helpers/UserHelper";
import {
  getUserFilledPortfolioFields,
  USER_FIELD_KINDS,
  generateMugShotBotMetaTags
} from "../../../helpers/UserPortfolioHelper";
import {
  getAimlabAuthStatusForPortfolioUser,
  getValorantAuthStatusForPortfolioUser
} from "../../../store/actions/externalAuth/externalAuthActions";
import { getGameRequirements } from "../../../store/actions/gameRequirementActions";
import { getSocialLive, portfolioSortLayout, updateUserSimple } from "../../../store/actions/userActions";
import sanitizeMetaDescription from "../../../store/utils/sanitizeMetaDescription";
import CustomCarousel from "../../Carousels/CustomCarousel/CustomCarousel";
import EmptyComponent from "../../EmptyComponent/EmptyComponent";
import Education from "../../Portfolio/Education/Education";
import Experiences from "../../Portfolio/Experiences/Experiences";
import HonorsAndEvents from "../../Portfolio/HonorsAndEvents/HonorsAndEvents";
import Skills from "../../Portfolio/Skills/Skills";
import GameStatsAccordion from "./GameStatsAccordion/GameStatsAccordion";
import Platforms from "./Platforms/Platforms";
import PortfolioDragComponent from "./PortfolioDragComponent/PortfolioDragComponent";
import SocialCardList from "./SocialCardList/SocialCardList";

const { publicRuntimeConfig } = getConfig();
const { feBaseUrl } = publicRuntimeConfig;

class Portfolio extends Component {
  constructor() {
    super();
    this.state = {
      gameRequirement: {},
      currentUserLoaded: false,
      componentLayoutLoaded: false,
      portfolioComponents: [],
      portfolioFieldsLoaded: false,
      portfolioFields: {}
    };
    this.modal = React.createRef();
  }

  componentDidMount() {
    if (this.props.pageProps?.user) {
      this.props.updateUserSimple(this.props.pageProps.user);
      this.props.getAimlabAuthStatusForPortfolioUser(this.props.pageProps.user._id);
      this.props.getValorantAuthStatusForPortfolioUser(this.props.pageProps.user._id);
    }
  }

  componentDidUpdate(prevProps) {
    const { componentLayoutLoaded, portfolioFieldsLoaded } = this.state;
    const { currentUser, user, pageProps } = this.props;
    /**
     * If component is navigated via next/link when it is already mounted
     * previously then user is not set, setting user from updateUserSimple
     * if it is not set
     * Fakhir
     */

    if (pageProps?.user && user && pageProps.user._id !== user._id) {
      this.props.updateUserSimple(pageProps.user);
      this.props.getAimlabAuthStatusForPortfolioUser(pageProps.user._id);
      this.props.getValorantAuthStatusForPortfolioUser(pageProps.user._id);
      this.setState({ portfolioFieldsLoaded: false });
    }

    if (prevProps.user.riot !== this.props.user.riot || prevProps.user.statespace !== this.props.user.statespace) {
      this.setPortfolioFields();
    }

    const isOwner = !isEmpty(currentUser) && currentUser.id === user._id;
    const userLoggedIn = this.props.currentUser?.id !== undefined;

    if (!componentLayoutLoaded) {
      const initialLayout = [];

      initialLayout.push(
        { name: "stats-component" },
        { name: "social-cards-component" },
        { name: "platforms-component" },
        { name: "experience-component" },
        { name: "education-component" },
        { name: "accolades-component" },
        { name: "skills-component" }
      );

      if (user?.componentLayout?.length === initialLayout.length) {
        this.setState({
          componentLayoutLoaded: true,
          portfolioComponents: user.componentLayout
        });
      } else {
        this.setState({
          componentLayoutLoaded: true,
          portfolioComponents: initialLayout
        });
        isOwner && this.props.portfolioSortLayout(initialLayout);
      }
    }
    if (!isEmpty(currentUser) && !isEmpty(user) && !this.state.currentUserLoaded) {
      if (currentUser.id == user.id) {
        analytics.page("Portfolio View");
        analytics.track("PORTFOLIO_PORTFOLIO_VIEW", { userId: user.id });
      } else {
        analytics.page("Portfolio View External");
        analytics.track("PORTFOLIO_PORTFOLIO_VIEW_EXTERNAL", {
          userId: user.id
        });
      }

      this.setState({ ...this.state, currentUserLoaded: true });
      if (userLoggedIn) this.props.getSocialLive(this.props.currentUser.id);
    }
    if (!portfolioFieldsLoaded) {
      this.setPortfolioFields();
      this.setState({
        portfolioFieldsLoaded: true
      });
    }
    if (
      prevProps.user.educationExperience !== user.educationExperience ||
      prevProps.user.businessExperience !== user.businessExperience ||
      prevProps.user.businessSkills !== user.businessSkills ||
      prevProps.user.gamingSkills !== user.gamingSkills ||
      prevProps.user.portfolioHonors !== user.portfolioHonors ||
      prevProps.user.portfolioEvents !== user.portfolioEvents ||
      prevProps.user.gamePlatforms !== user.gamePlatforms
    ) {
      this.setPortfolioFields();
    }
  }

  onDragEnd = result => {
    const { destination, source, draggableId } = result;
    if (!destination) {
      return;
    }

    if (destination.droppableId === source.droppableId && destination.index === source.index) {
      return;
    }

    const { portfolioComponents } = this.state;
    const newComponents = Array.from(portfolioComponents);
    newComponents.splice(source.index, 1);
    newComponents.splice(destination.index, 0, { name: draggableId });

    this.setState({
      ...this.state,
      portfolioComponents: newComponents
    });
    this.props.portfolioSortLayout(newComponents);
    analytics.track("PORTFOLIO_DRAG_COMPONENT");
  };

  onDragStart = () => {
    if (window.navigator.vibrate) {
      window.navigator.vibrate(100);
    }
  };

  // set the initial portfolio fields and check if the user has data available for them
  setPortfolioFields = () => {
    const { user } = this.props;
    const userFields = getUserFilledPortfolioFields(user);

    this.setState({
      portfolioFields: {
        isAnyVideoLinked: userFields.includes(USER_FIELD_KINDS.VIDEO_LINKED),
        isAnyGame: userFields.includes(USER_FIELD_KINDS.GAME_STATS),
        isAnyPlatform: userFields.includes(USER_FIELD_KINDS.GAME_PLATFORM),
        isAnyAccolade:
          userFields.includes(USER_FIELD_KINDS.PORTFOLIO_EVENTS) ||
          userFields.includes(USER_FIELD_KINDS.PORTFOLIO_HONORS),
        isAnySkills:
          userFields.includes(USER_FIELD_KINDS.BUSINESS_SKILLS) || userFields.includes(USER_FIELD_KINDS.GAMING_SKILLS),
        isAnyWork: userFields.includes(USER_FIELD_KINDS.BUSINESS_EXPERIENCE),
        isAnyEducation: userFields.includes(USER_FIELD_KINDS.EDUCATION_EXPERIENCE),
        isAnySocial: userFields.includes(USER_FIELD_KINDS.SOCIAL_LINKED)
      }
    });
  };

  // switch case to return the correct component with the correct variables
  returnComponent = (isOwner, componentDragId, index) => {
    const { user, portfolio_drag_buttons, socialcards_section } = this.props;
    const { portfolioFields } = this.state;
    let hasStats = false;
    let displayComponent = <></>;
    switch (componentDragId) {
      case "video-component":
        displayComponent = (
          <CustomCarousel
            iconLeft={faArrowLeft}
            iconRight={faArrowRight}
            itemsList={[{ videoUrl: user?.embeddedLink }, { stream: user?.twitch?.displayName }]}
          />
        );
        hasStats = portfolioFields?.isAnyVideoLinked;
        break;
      case "stats-component":
        displayComponent = <GameStatsAccordion />;
        hasStats = portfolioFields?.isAnyGame;
        break;
      case "social-cards-component":
        displayComponent = socialcards_section ? <SocialCardList /> : <></>;
        hasStats = true;
        break;
      case "platforms-component":
        displayComponent = <Platforms />;
        hasStats = portfolioFields?.isAnyPlatform;
        break;
      case "experience-component":
        displayComponent = <Experiences user={user} />;
        hasStats = portfolioFields?.isAnyWork;
        break;
      case "education-component":
        displayComponent = <Education />;
        hasStats = portfolioFields?.isAnyEducation;
        break;
      case "skills-component":
        displayComponent = <Skills />;
        hasStats = portfolioFields?.isAnySkills;
        break;
      case "accolades-component":
        displayComponent = <HonorsAndEvents />;
        hasStats = portfolioFields?.isAnyAccolade;
        break;
      default:
        break;
    }
    return (
      <>
        {!isOwner && !hasStats ? (
          <></>
        ) : (
          <Draggable draggableId={componentDragId} index={index} key={componentDragId}>
            {(provided, snapshot) => (
              <PortfolioDragComponent
                portfolio_drag_buttons={portfolio_drag_buttons}
                isOwner={isOwner}
                snapshot={snapshot}
                provided={provided}
                innerComponent={displayComponent}
              />
            )}
          </Draggable>
        )}
      </>
    );
  };

  render() {
    const { currentUser, mockExternalUser, pageProps, user } = this.props;
    const { portfolioComponents, portfolioFields } = this.state;
    const isOwner = checkIfOwner(currentUser, user, mockExternalUser);
    const userFullName = pageProps?.user?.name;

    return (
      <DragDropContext onDragEnd={this.onDragEnd} onDragStart={this.onDragStart}>
        {pageProps?.user && (
          <NextSeo
            title={`${userFullName} | eFuse Portfolio`}
            description={sanitizeMetaDescription(
              pageProps.user.bio ||
                `View ${pageProps.user.name}'s gaming portfolio, social accounts, and games stats on eFuse.`
            )}
            canonical={`https://efuse.gg/u/${pageProps.user.username}`}
            additionalMetaTags={generateMugShotBotMetaTags(pageProps.user)}
            openGraph={{
              type: "website",
              url: `https://efuse.gg/u/${pageProps.user.username}`,
              title: `${userFullName} | eFuse Portfolio`,
              site_name: "eFuse.gg",
              description:
                pageProps.user.bio ||
                `View ${pageProps.user.name}'s gaming portfolio, social accounts, and games stats on eFuse.`,
              images: [
                {
                  url: `https://mugshotbot.com/m?theme=two_up&mode=light&color=2e87ff&pattern=topography&hide_watermark=true&url=${feBaseUrl}/u/${pageProps.user.username}`
                }
              ]
            }}
            twitter={{
              handle: "@eFuseOfficial",
              site: `https://efuse.gg/u/${pageProps.user.username}`,
              cardType: "summary_large_image"
            }}
          />
        )}
        {!isOwner &&
          !portfolioFields?.isAnyVideoLinked &&
          !portfolioFields?.isAnyGame &&
          !portfolioFields?.isAnyExperience &&
          !portfolioFields?.isAnyPlatform &&
          !portfolioFields?.isAnyAccolade &&
          !portfolioFields?.isAnySkills &&
          !portfolioFields?.isAnyWork &&
          !portfolioFields?.isAnyEducation &&
          !portfolioFields?.isAnySocial && <EmptyComponent text="This user has no portfolio information" />}
        <Droppable droppableId="portfolio">
          {provided => (
            <div {...provided.droppableProps} ref={provided.innerRef}>
              {portfolioComponents.length > 0 &&
                portfolioComponents.map((portfolioComponent, index) => {
                  return this.returnComponent(isOwner, portfolioComponent.name, index);
                })}
              {provided.placeholder}
            </div>
          )}
        </Droppable>
      </DragDropContext>
    );
  }
}

const mapStateToProps = state => ({
  event: state.settings.event,
  user: state.user.currentUser,
  isWindowView: state.messages.isWindowView,
  userLiveInfo: state.user.userLiveInfo,
  currentUser: state.auth.currentUser,
  showVideoClipsComponents: state.features.portfolio_videoclips_component,
  socialcards_section: state.features.socialcards_section,
  mockExternalUser: state.portfolio.mockExternalUser,
  portfolio_drag_buttons: state.features.portfolio_drag_buttons
});

export default connect(mapStateToProps, {
  getGameRequirements,
  getSocialLive,
  updateUserSimple,
  portfolioSortLayout,
  getAimlabAuthStatusForPortfolioUser,
  getValorantAuthStatusForPortfolioUser
})(Portfolio);
