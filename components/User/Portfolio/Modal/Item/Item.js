import React from "react";
import Style from "./Item.module.scss";

const Item = ({ platforms, stat, field, locale }) => {
  return (
    <>
      {!platforms ? (
        <div className={Style.statItemWrapper}>
          <p>
            <strong>{eval(`field.${locale}_name`)}</strong> : {stat.value}
          </p>
          <p />
        </div>
      ) : (
        <div className={Style.statItemWrapper}>
          <p>
            <strong>Platforms</strong> : {platforms}
          </p>
          <p />
        </div>
      )}
    </>
  );
};
export default Item;
