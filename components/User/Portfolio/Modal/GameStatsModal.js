import React, { Component } from "react";

import { Row, ListGroupItem, Col } from "react-bootstrap";
import EFPrimaryModal from "../../../Modals/EFPrimaryModal/EFPrimaryModal";

import Style from "./GameStatsModal.module.scss";

import Item from "./Item/Item";

class GameStatsModal extends Component {
  state = {
    show: false
  };

  handleShow = () => {
    this.setState({
      show: true
    });
  };

  handleClose = () => {
    this.setState({
      show: false
    });
  };

  render() {
    const { show } = this.state;
    const gameRequirement =
      this.props.initGameRequirement &&
      this.props.user.affiliatedGames &&
      this.props.user.affiliatedGames.filter(g => g.affiliatedGame._id == this.props.initGameRequirement._id)[0];
    const content = [];
    if (gameRequirement) {
      const user_fields = gameRequirement && gameRequirement.affiliatedGame.fields;
      const user_stats = gameRequirement && gameRequirement.stats;
      user_fields.map(field => {
        user_stats.filter(stat => {
          if (stat.stat == field._id) {
            content.push(
              <Col md="4">
                <Item stat={stat} field={field} game={gameRequirement} locale={this.props.user.locale} />
              </Col>
            );
          }
        });
      });
      content.push(
        <Col md="4">
          <Item platforms={gameRequirement.platforms} />
        </Col>
      );
    }

    return (
      <EFPrimaryModal
        title={`${gameRequirement && gameRequirement.affiliatedGame.title} Stats`}
        isOpen={show}
        onClose={this.handleClose}
      >
        <ListGroupItem className={Style.statItemWrapper}>
          <Row>{gameRequirement && gameRequirement.stats ? content : <h5>No Stats found</h5>}</Row>
        </ListGroupItem>
      </EFPrimaryModal>
    );
  }
}
export default GameStatsModal;
