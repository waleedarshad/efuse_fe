import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "next/router";
import { faPlus } from "@fortawesome/pro-solid-svg-icons";

import AddButton from "../AddButton/AddButton";
import DisplayExperience from "./DisplayExperience/DisplayExperience";
import { removeData } from "../../../../store/actions/portfolioActions";
import BusinessExperienceModal from "../../../GlobalModals/BusinessExperienceModal/BusinessExperienceModal";

class BusinessExperience extends Component {
  render() {
    const { user, router, currentUser, mockExternalUser } = this.props;
    let isCurrentUser = currentUser && user._id === currentUser._id;
    if (mockExternalUser) {
      isCurrentUser = false;
    }
    let content = <></>;
    const { businessExperience } = user;
    if (user && businessExperience) {
      content = businessExperience.map((experience, index) => {
        return (
          <DisplayExperience
            key={index}
            experience={experience}
            displayHr={businessExperience?.length !== index + 1}
            onEdit={() => "empty"}
            onRemove={() => {
              analytics.track("PORTFOLIO_WORK_EXPERIENCE_DELETE", {
                experienceId: experience._id
              });
              this.props.removeData("/portfolio/business_experience", experience._id);
            }}
            isCurrentUser={isCurrentUser}
          />
        );
      });
    }

    return (
      <>
        {content}
        {isCurrentUser && (
          <BusinessExperienceModal>
            <AddButton desc="Add Work Experience" icon={faPlus} />
          </BusinessExperienceModal>
        )}
      </>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user.currentUser,
  currentUser: state.auth.currentUser,
  mockExternalUser: state.portfolio.mockExternalUser
});

export default connect(mapStateToProps, {
  removeData
})(withRouter(BusinessExperience));
