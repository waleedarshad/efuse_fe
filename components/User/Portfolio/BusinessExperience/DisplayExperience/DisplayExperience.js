import PropTypes from "prop-types";
import { faPencilAlt, faTrash } from "@fortawesome/pro-solid-svg-icons";

import Style from "../../Experience/Experience.module.scss";
import { formatDate } from "../../../../../helpers/GeneralHelper";
import ConfirmAlert from "../../../../ConfirmAlert/ConfirmAlert";
import ExperienceIcon from "../../Experience/ExperienceIcon/ExperienceIcon";
import BusinessExperienceModal from "../../../../GlobalModals/BusinessExperienceModal/BusinessExperienceModal";
import { EFHtmlParser } from "../../../../EFHtmlParser/EFHtmlParser";
import EFCircleIconButton from "../../../../Buttons/EFCircleIconButton/EFCircleIconButton";

const DisplayExperience = ({ experience, displayHr, onRemove, isCurrentUser }) => {
  const { title, subTitle, startDate, endDate, present, description, image } = experience;
  const formattedDate = present
    ? `${formatDate(startDate)} - Present`
    : `${formatDate(startDate)} - ${formatDate(endDate)}`;
  return (
    <div className={Style.container}>
      <div className={Style.socialContainer}>
        <ExperienceIcon url={image?.url} />
        <div className={Style.textContainer}>
          <h4 className={Style.socialTitle}>
            {title}

            {isCurrentUser && (
              <>
                <BusinessExperienceModal experience={experience} edit>
                  <EFCircleIconButton colorTheme="transparent" shadowTheme="none" icon={faPencilAlt} />
                </BusinessExperienceModal>
                <ConfirmAlert
                  title="This action can't be undone"
                  message="Are you sure you want to remove?"
                  onYes={onRemove}
                >
                  <EFCircleIconButton colorTheme="transparent" shadowTheme="none" icon={faTrash} onClick={onRemove} />
                </ConfirmAlert>
              </>
            )}
          </h4>
          <p className={Style.socialName}>{subTitle}</p>
          <p className={Style.socialTime}>{formattedDate}</p>
          <div className={Style.socialDescription}>
            <EFHtmlParser>{description}</EFHtmlParser>
          </div>
        </div>
      </div>
      {displayHr && <hr className={Style.divider} />}
    </div>
  );
};

DisplayExperience.propTypes = {
  experience: PropTypes.object.isRequired,
  displayHr: PropTypes.bool,
  onEdit: PropTypes.func.isRequired,
  onRemove: PropTypes.func.isRequired,
  isCurrentUser: PropTypes.bool.isRequired
};

export default DisplayExperience;
