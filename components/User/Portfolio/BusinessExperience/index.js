import BusinessExperience from "./BusinessExperience";
import portfolioTabsLayout from "../../../hoc/portfolioTabsLayout/portfolioTabsLayout";

export default portfolioTabsLayout({
  feature: "work_section",
  id: "work-section",
  tabs: [
    {
      title: "Work",
      component: BusinessExperience,
      tabFeature: "work_tab",
      auth: "businessExperience"
    }
  ]
});
