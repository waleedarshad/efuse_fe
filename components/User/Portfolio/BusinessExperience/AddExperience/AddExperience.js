/* eslint-disable no-restricted-globals */
import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Form, Col, FormControl } from "react-bootstrap";
import moment from "moment";
import InputRow from "../../../../InputRow/InputRow";
import InputField from "../../../../InputField/InputField";
import InputLabel from "../../../../InputLabel/InputLabel";
import DatePickerInput from "../../../../DatePickerInput/DatePickerInput";
import DatepickerMonthElement from "../../../../DatepickerMonthElement/DatepickerMonthElement";
import Checkbox from "../../../../Checkbox/Checkbox";
import { rescueNil } from "../../../../../helpers/GeneralHelper";
import { toMoment } from "../../../../../helpers/MomentHelper";
import { validateHtml } from "../../../../../helpers/ValidationHelper";
import ActionButtonGroup from "../../../../Layouts/Internal/ActionButtonGroup/ActionButtonGroup";
import { manageExperience } from "../../../../../store/actions/portfolioActions";
import ImageUploader from "../../../../ImageUploader/ImageUploader";
import EFHtmlInput from "../../../../EFHtmlInput/EFHtmlInput";

const AddExperience = ({
  edit,
  experience,
  onboardingAnalyticsOptions,
  closeModal,
  resetOnboardingState,
  removeCancelOption
}) => {
  const [validated, setValidated] = useState(false);
  const [valuesSet, setValuesSet] = useState(false);
  const [businessExperience, setBusinessExperience] = useState({
    title: "",
    subTitle: "",
    startDate: null,
    endDate: null,
    present: false,
    image: {},
    description: "",
    _id: ""
  });
  const [startDateError, setStartDateError] = useState(false);
  const [endDateError, setEndDateError] = useState(false);
  const inProgress = useSelector(state => state.portfolio.inProgress);

  const dispatch = useDispatch();

  useEffect(() => {
    if (edit) {
      setBusinessExperience({
        title: experience.title,
        subTitle: experience.subTitle,
        startDate: experience.startDate,
        endDate: experience.endDate,
        present: experience.present,
        image: experience.image,
        description: experience.description,
        _id: experience._id
      });
      setValuesSet(true);
    }
  }, [edit, valuesSet]);

  const onChange = () => {
    setBusinessExperience({
      ...businessExperience,
      [event.target.name]: event.target.value
    });
  };

  const onDateChange = (name, date) => {
    setBusinessExperience({
      ...businessExperience,
      [name]: toMoment(date)
    });
    if (name === "startDate") {
      setStartDateError(false);
    }
    if (name === "endDate") {
      setEndDateError(false);
    }
  };

  const onPresentChange = event => {
    setBusinessExperience({
      ...businessExperience,
      present: event.target.checked
    });
  };

  const onEditorStateChange = editorState => {
    setBusinessExperience({
      ...businessExperience,
      description: editorState
    });
  };

  const onDrop = (selectedFile, name) => {
    setBusinessExperience({
      ...businessExperience,
      [name]: selectedFile[0]
    });
  };

  const onSubmit = event => {
    event.preventDefault();
    const { startDate, endDate, present } = businessExperience;
    if (!validated) {
      setValidated(true);
    }
    if (present && !startDate) {
      setStartDateError(true);
    } else if (!present && (!startDate || !endDate)) {
      setStartDateError(!startDate);
      setEndDateError(!endDate);
    } else if (event.currentTarget.checkValidity() && ((present && startDate) || (!present && startDate && endDate))) {
      let businessExperienceData = { ...businessExperience };
      businessExperienceData = {
        ...businessExperienceData,
        description: validateHtml(businessExperienceData.description)
      };
      saveExperience();
      if (onboardingAnalyticsOptions) {
        analytics.track("PORTFOLIO_WORK_EXPERIENCE_CREATE", {
          experience: businessExperienceData,
          location: "onboarding"
        });
      } else {
        analytics.track("PORTFOLIO_WORK_EXPERIENCE_CREATE", {
          experience: businessExperienceData
        });
      }
      if (closeModal) closeModal();
      if (resetOnboardingState) resetOnboardingState();
    }
  };

  const saveExperience = () => {
    const Data = new FormData();
    Object.keys(businessExperience).forEach(key => {
      if (businessExperience[key] !== undefined) {
        Data.append(key, businessExperience[key]);
      }
    });
    dispatch(manageExperience("/portfolio/business_experience", Data));
  };

  const { title, subTitle, startDate, endDate, present, image, description } = businessExperience;
  return (
    <Form noValidate validated={validated} onSubmit={onSubmit}>
      <InputRow>
        <Col sm={6}>
          <InputLabel theme="darkColor">Job Title</InputLabel>
          <InputField
            name="title"
            placeholder="Add Title"
            theme="whiteShadow"
            size="lg"
            onChange={onChange}
            value={title}
            required
            errorMessage="Title is required"
            maxLength={50}
          />
        </Col>
        <Col sm={6}>
          <InputLabel theme="darkColor">Company Name</InputLabel>
          <InputField
            name="subTitle"
            placeholder="Add Sub title"
            theme="whiteShadow"
            size="lg"
            onChange={onChange}
            value={subTitle}
            required
            errorMessage="Sub title is required"
            maxLength={50}
          />
        </Col>
      </InputRow>
      <InputRow>
        <Col>
          <Checkbox
            custom
            name="present"
            type="checkbox"
            label="Present?"
            id="presentExp"
            theme="darkColor"
            checked={present}
            value={present}
            onChange={onPresentChange}
          />
        </Col>
      </InputRow>
      <InputRow>
        <Col sm={present ? 12 : 6}>
          <InputLabel theme="darkColor">Start Date</InputLabel>
          <DatePickerInput
            date={startDate}
            dateHandler={date => onDateChange("startDate", date)}
            isOutsideRange={day => day.isAfter(moment(endDate))}
            renderMonthElement={calenderProps => <DatepickerMonthElement {...calenderProps} includeCurrentYear />}
            numberOfMonths={1}
            direction="down"
          />
          {startDateError && (
            <FormControl.Feedback type="invalid" style={{ display: "block" }}>
              Start Date is required
            </FormControl.Feedback>
          )}
        </Col>
        {!present && (
          <Col sm={6}>
            <InputLabel theme="darkColor">End Date</InputLabel>
            <DatePickerInput
              date={endDate}
              dateHandler={date => onDateChange("endDate", date)}
              isOutsideRange={day => day.isBefore(moment(startDate))}
              renderMonthElement={calenderProps => <DatepickerMonthElement {...calenderProps} includeCurrentYear />}
              numberOfMonths={1}
              direction="down"
            />
            {endDateError && (
              <FormControl.Feedback type="invalid" style={{ display: "block" }}>
                End Date is required
              </FormControl.Feedback>
            )}
          </Col>
        )}
      </InputRow>
      <InputRow>
        <Col sm={12}>
          <InputLabel theme="darkColor" optional>
            Company Logo
          </InputLabel>
        </Col>
        <Col sm={12}>
          <ImageUploader
            text="Select Custom Image"
            name="image"
            onDrop={onDrop}
            theme="lightButton"
            value={rescueNil(image, "url")}
            showCropper={false}
          />
        </Col>
      </InputRow>
      <InputRow>
        <Col sm={12}>
          <InputLabel theme="darkColor">Description</InputLabel>
        </Col>
        <Col sm={12}>
          <EFHtmlInput value={description} onChange={onEditorStateChange} />
        </Col>
      </InputRow>
      <ActionButtonGroup disabled={inProgress} removeCancelOption={removeCancelOption} />
    </Form>
  );
};

export default AddExperience;
