import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronRight } from "@fortawesome/pro-solid-svg-icons";
import Style from "./ClipsComponent.module.scss";

class ClipsComponent extends Component {
  render() {
    return (
      <div className={Style.clipsContainer}>
        <div className={Style.images}>
          <div className={Style.thumbContainer}>
            <img className={Style.thumbnail} src="https://i.ytimg.com/vi/tEmbOpVWfMw/maxresdefault.jpg" />
            <div className={Style.bottomBar}>
              <p className={Style.thumbText}>squad wipe last alive</p>
            </div>
          </div>
          <div className={Style.thumbContainer}>
            <img className={Style.thumbnail} src="https://i.ytimg.com/vi/YGHIiH5TGoY/maxresdefault.jpg" />
            <div className={Style.bottomBar}>
              <p className={Style.thumbText}>360 no scope final kill</p>
            </div>
          </div>
          <div className={Style.thumbContainer}>
            <img className={Style.thumbnail} src="https://i.ytimg.com/vi/ETx4-lOSE2Y/maxresdefault.jpg" />
            <div className={Style.bottomBar}>
              <p className={Style.thumbText}>random squads solo win</p>
            </div>
          </div>
          <div className={Style.thumbContainer}>
            <img
              className={Style.thumbnail}
              src="https://i2.wp.com/www.topgameplays.com/wp-content/uploads/2019/06/1559636999_maxresdefault.jpg?fit=1280%2C720&ssl=1"
            />
            <div className={Style.bottomBar}>
              <p className={Style.thumbText}>solo only victory</p>
            </div>
          </div>
          {/* <img className={Style.thumbnail} src="https://i.ytimg.com/vi/tEmbOpVWfMw/maxresdefault.jpg"/>
                    <img className={Style.thumbnail} src="https://i.ytimg.com/vi/tEmbOpVWfMw/maxresdefault.jpg"/>
                    <img className={Style.thumbnail} src="https://i.ytimg.com/vi/tEmbOpVWfMw/maxresdefault.jpg"/> */}
        </div>
        <button className={Style.button} src="https://i.ytimg.com/vi/tEmbOpVWfMw/maxresdefault.jpg">
          <FontAwesomeIcon icon={faChevronRight} />
        </button>
      </div>
    );
  }
}
export default ClipsComponent;
