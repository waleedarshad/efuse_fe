import React, { Component } from "react";
import { connect } from "react-redux";
import { faSnapchat, faTwitch, faDiscord, faYoutube, faTwitter, faTiktok } from "@fortawesome/free-brands-svg-icons";
import { faArrowLeft, faArrowRight } from "@fortawesome/pro-regular-svg-icons";

import SocialCard from "./SocialCard/SocialCard";
import Style from "./SocialCardList.module.scss";
import HorizontalScroll from "../../../HorizontalScroll/HorizontalScroll";
import EfuseCloutCard from "../../../EfuseCloutCard/EfuseCloutCard";

export class SocialCardList extends Component {
  render() {
    const { user, currentUser, mockExternalUser, followers } = this.props;
    let isOwner = currentUser && user._id === currentUser._id;
    if (mockExternalUser) {
      isOwner = false;
    }
    return (
      <HorizontalScroll customButtonMargin={Style.buttonMarginTop} iconLeft={faArrowLeft} iconRight={faArrowRight}>
        <div className={Style.cardList}>
          <div className={Style.efuseCardContainer}>
            <EfuseCloutCard cardSize="small" shadow="none" username={user.username} followers={followers} />
          </div>

          {(user?.twitchVerified || isOwner) && (
            <SocialCard
              icon={faTwitch}
              social="twitch"
              noStats={!user?.twitchVerified}
              username={user && user?.twitchVerified && user?.twitchUserName}
              count={user?.twitchFollowersCount}
              countLabel="Followers"
              link={user?.twitchUserName && `https://www.twitch.tv/${user?.twitchUserName}`}
              verified={user?.twitchVerified}
            />
          )}
          {(user?.twitterVerified || isOwner) && (
            <SocialCard
              icon={faTwitter}
              social="twitter"
              noStats={!user?.twitterVerified}
              username={user && user?.twitterVerified && user?.twitterUsername}
              count={user?.twitterfollowersCount}
              countLabel="Followers"
              link={user?.twitterUsername && `https://twitter.com/${user?.twitterUsername}`}
              verified={user?.twitterVerified}
            />
          )}
          {(user?.discordVerified || isOwner) && (
            <SocialCard
              icon={faDiscord}
              social="discord"
              noStats={!user?.discordVerified}
              username={user && user?.discordVerified && user?.discordUserName}
              verified={user?.discordVerified}
            />
          )}
          {(user?.snapchat?.displayName || isOwner) && (
            <SocialCard
              icon={faSnapchat}
              social="snapchat"
              verified={user?.snapchat?.displayName}
              username={user && user?.snapchat?.displayName}
              linkModal
              isOwner={isOwner}
            />
          )}
          {(user?.googleVerified || isOwner) && (
            <SocialCard
              icon={faYoutube}
              social="youtube"
              verified={user?.googleVerified}
              username={user?.youtubeChannel?.title || user?.google?.name}
              isOwner={isOwner}
              count={user?.youtubeChannel?.statistics?.subscriberCount}
              countLabel="Subscribers"
              link={user?.youtubeChannel?.id && `https://www.youtube.com/channel/${user?.youtubeChannel?.id}`}
            />
          )}
          {(user?.tikTok?.tiktokUsername || isOwner) && (
            <SocialCard
              icon={faTiktok}
              social="tiktok"
              verified={user?.tikTok?.tiktokUsername}
              username={user && user?.tikTok?.tiktokUsername}
              linkModal
              link={`https://www.tiktok.com/@${user?.tikTok?.tiktokUsername}`}
              isOwner={isOwner}
            />
          )}
        </div>
      </HorizontalScroll>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user.currentUser,
  currentUser: state.auth.currentUser,
  mockExternalUser: state.portfolio.mockExternalUser,
  followers: state.followers.totalFollowers
});

export default connect(mapStateToProps, {})(SocialCardList);
