import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUnlink } from "@fortawesome/pro-solid-svg-icons";

import capitalize from "lodash/capitalize";
import React, { Component } from "react";
import { connect } from "react-redux";
import { withCdn } from "../../../../../common/utils";
import {
  removeSnapchatUsername,
  removeTiktokUsername,
  removeYoutubeUsername
} from "../../../../../store/actions/portfolioActions";
import ConfirmAlert from "../../../../ConfirmAlert/ConfirmAlert";
import FeatureFlag from "../../../../General/FeatureFlag/FeatureFlag";
import FeatureFlagVariant from "../../../../General/FeatureFlag/FeatureFlagVariant/FeatureFlagVariant";
import LinkSocialUsernameModal from "../../../../GlobalModals/LinkSocialUsernameModal/LinkSocialUsernameModal";
import ExternalAccounts from "../../../../Settings/ExternalAccounts/index";
import SignupModal from "../../../../SignupModal/SignupModal";
import Style from "./SocialCard.module.scss";

export class SocialCard extends Component {
  unlinkAccount = () => {
    const { social } = this.props;
    if (social === "snapchat") {
      this.props.removeSnapchatUsername();
    }
    if (social === "youtube") {
      this.props.removeYoutubeUsername();
    }
    if (social === "tiktok") {
      this.props.removeTiktokUsername();
    }
  };

  render() {
    const { social, username, count, countLabel, link, isOwner, verified, linkModal, currentUser, icon } = this.props;
    const userLoggedIn = currentUser?.id;
    let socialStyle;
    let socialButtonStyle;

    let linkText = "";
    switch (social) {
      case "twitter":
        socialStyle = Style.twitter;
        socialButtonStyle = Style.twitterButton;
        linkText = "Link your Twitter to show off your clout!";
        break;
      case "discord":
        socialStyle = Style.discord;
        socialButtonStyle = Style.discordButton;
        linkText = "Link your Discord to show off your clout!";
        break;
      case "twitch":
        socialStyle = Style.twitch;
        socialButtonStyle = Style.twitchButton;
        linkText = "Link your Twitch to show off your clout!";
        break;
      case "linkedin":
        socialStyle = Style.linkedin;
        socialButtonStyle = Style.linkedinButton;
        linkText = "Link your Linkedin to show off your clout!";
        break;
      case "facebook":
        socialStyle = Style.facebook;
        socialButtonStyle = Style.facebookButton;
        linkText = "Link your Facebook to show off your clout!";
        break;
      case "snapchat":
        socialStyle = Style.snapchat;
        socialButtonStyle = Style.snapchatButton;
        linkText = "Link your Snapchat to show off your clout!";
        break;
      case "youtube":
        socialStyle = Style.youtube;
        socialButtonStyle = Style.youtubeButton;
        linkText = "Link your Youtube to show off your clout!";
        break;
      case "tiktok":
        socialStyle = Style.tiktok;
        socialButtonStyle = Style.tiktokButton;
        linkText = "Link your Tiktok to show off your clout!";
        break;
      case "instagram":
        socialStyle = Style.instagram;
        socialButtonStyle = Style.instagramButton;
        linkText = "Link your Instagram to show off your clout!";
        break;
      default:
        socialStyle = "";
        socialButtonStyle = "";
        linkText = "";
    }
    return (
      <FeatureFlag name={`enable_${social}_auth`}>
        <FeatureFlagVariant flagState>
          <div className={`${Style.socialCardContainer}`}>
            <FontAwesomeIcon icon={icon} className={`${Style.backgroundIcon} ${socialStyle}`} />
            {/* this will be removed when fontawesome supports tiktok */}
            {social === "tiktok" && (
              <img src={withCdn("/static/images/tiktok.svg")} alt="tiktok" className={Style.tiktokBackground} />
            )}
            {!verified && (
              <>
                <div className={Style.titleText}>{capitalize(social)}</div>
                <div className={Style.linkText}>{linkText}</div>
              </>
            )}
            {username && (
              <>
                <div className={Style.titleText}>{capitalize(social)}</div>
                <div className={Style.linkText}>@{username}</div>
              </>
            )}
            {count !== false && count !== null && count >= 0 && (
              <div className={Style.countContainer}>
                <p className={Style.countLabel}>{countLabel}</p>
                <h3 className={Style.count}>{count}</h3>
              </div>
            )}
            {verified &&
              link &&
              (userLoggedIn ? (
                <a href={link} target="_blank" rel="noreferrer" className={Style.removeLink}>
                  <div className={`${Style.viewButton} ${verified && Style.maxButtonWidth} ${socialButtonStyle}`}>
                    {verified ? "View" : `Link ${capitalize(social)}`}
                  </div>
                </a>
              ) : (
                <SignupModal
                  title={`Create an account to view @${username} on ${capitalize(social)}.`}
                  message="Connect gamers now. Sign up today!"
                >
                  <div className={`${Style.viewButton} ${verified && Style.maxButtonWidth} ${socialButtonStyle}`}>
                    {verified ? "View" : `Link ${capitalize(social)}`}
                  </div>
                </SignupModal>
              ))}
            {verified && linkModal && isOwner && (
              <ConfirmAlert title={`Unlinking ${social}`} message="Are you sure?" onYes={this.unlinkAccount}>
                <div className={Style.unlink}>
                  <FontAwesomeIcon icon={faUnlink} className={Style.unlinkIcon} />
                </div>
              </ConfirmAlert>
            )}
            {!verified && linkModal && (
              <div className={Style.linkAccountButton}>
                <LinkSocialUsernameModal social={social}>
                  <div className={`${Style.viewButton} ${verified && Style.maxButtonWidth} ${socialButtonStyle}`}>
                    {!verified && "Link"}
                  </div>
                </LinkSocialUsernameModal>
              </div>
            )}
            {!verified && !linkModal && (
              <div className={Style.linkAccountButton}>
                <ExternalAccounts
                  autoUpdate
                  portfolioView
                  onboardingFlow={false}
                  avoidGrid
                  onlyShowLinks
                  displaySteam={false}
                  displayBattlenet={false}
                  displayLinkedin={social === "linkedin"}
                  displayTwitter={social === "twitter"}
                  displayDiscord={social === "discord"}
                  displayFacebook={social === "facebook"}
                  displaySnapchat={social === "snapchat"}
                  displayTwitch={social === "twitch"}
                  displayGoogle={social === "youtube"}
                  displayAimLab={false}
                  displayValorant={false}
                >
                  <div className={`${Style.viewButton} ${verified && Style.maxButtonWidth} ${socialButtonStyle}`}>
                    {!verified && "Link"}
                  </div>
                </ExternalAccounts>
              </div>
            )}
          </div>
        </FeatureFlagVariant>
      </FeatureFlag>
    );
  }
}

const mapStateToProps = state => ({
  currentUser: state.auth.currentUser
});

export default connect(mapStateToProps, {
  removeSnapchatUsername,
  removeYoutubeUsername,
  removeTiktokUsername
})(SocialCard);
