import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUnlink, faPlus } from "@fortawesome/pro-solid-svg-icons";
import cloneDeep from "lodash/cloneDeep";
import { withRouter } from "next/router";
import React, { Component } from "react";

import { connect } from "react-redux";
import EFAlert from "../../../../EFAlert/EFAlert";
import AddLeagueOfLegendsContainer from "./AddLeagueOfLegends/AddLeagueOfLegendsContainer";
import Style from "./index.module.scss";
import LeagueOfLegendsStats from "./StatsComponents/LeagueOfLegendsStats";

import AddButton from "../../AddButton/AddButton";

// Note: The underscores are just to avoid the shadowed variables issue.
// This can be changed once we move to functional component
import { togglePortfolioModal as _togglePortfolioModal } from "../../../../../store/actions/portfolioActions";
import {
  updateUserSimple as _updateUserSimple,
  unlinkGameAccount as _unlinkGameAccount
} from "../../../../../store/actions/userActions";

import { LOL_CONNECTION_EVENTS } from "../../../../../common/analyticEvents";

class Index extends Component {
  constructor(props) {
    super(props);

    this.state = {
      edit: false,
      hasStats: false,
      waitingStats: false
    };
  }

  static getDerivedStateFromProps(props, state) {
    const { user } = props;
    const updatedState = cloneDeep(state);

    if (user?.leagueOfLegends?.verified && user?.leagueOfLegends?.stats?._id) {
      updatedState.hasStats = true;
      updatedState.waitingStats = false;
    } else if (user?.leagueOfLegends?.verified) {
      updatedState.waitingStats = true;
      updatedState.hasStats = false;
    } else {
      updatedState.hasStats = false;
    }

    return updatedState;
  }

  loadModal = () => {
    analytics.track(LOL_CONNECTION_EVENTS.VERIFICATION_MODAL_OPEN);
    const { togglePortfolioModal } = this.props;
    togglePortfolioModal(true, "league of legends");
  };

  onHide = () => {
    analytics.track(LOL_CONNECTION_EVENTS.VERIFICATION_MODAL_CLOSE);
    const { togglePortfolioModal } = this.props;
    togglePortfolioModal(false);
    this.setState({ edit: false });
  };

  editModal = () => {
    const { togglePortfolioModal } = this.props;
    togglePortfolioModal(true, "league of legends");
    this.setState({ edit: true });
  };

  onUnlink = game => {
    const title = `Unlink ${game} Account?`;
    const message = "Are You Sure?";
    const confirmLabel = "Yes";
    const cancelLabel = "No";
    const onConfirm = () => this.unlinkAccount(game);

    EFAlert(title, message, confirmLabel, cancelLabel, onConfirm);
  };

  unlinkAccount = game => {
    const { updateUserSimple, unlinkGameAccount, user } = this.props;
    const clonedUser = cloneDeep(user);

    delete user.leagueOfLegends;
    updateUserSimple(clonedUser);

    this.setState({ hasStats: false, waitingStats: false });

    unlinkGameAccount({ game });
  };

  render() {
    const { user, modal, modalSection, router, currentUser, mockExternalUser } = this.props;
    const { edit, hasStats, waitingStats } = this.state;
    let isCurrentUser = currentUser && router.query.id === currentUser.id;

    // this allows users on the recruit settings page to add LoL accounts
    if (router.pathname === "/settings/recruiting") isCurrentUser = true;

    if (mockExternalUser) {
      isCurrentUser = false;
    }

    return (
      <>
        {hasStats ? (
          <LeagueOfLegendsStats
            isCurrentUser={isCurrentUser}
            onClick={() => this.onUnlink("lol")}
            styleClass={Style.unlink}
            userStats={user.leagueOfLegends}
          />
        ) : (
          <>
            {isCurrentUser && !waitingStats && (
              <AddButton desc="Add League of Legends Account" icon={faPlus} onClick={this.loadModal} />
            )}
            {waitingStats && (
              <div className={Style.waitingStatsRoot}>
                Account has been linked, the stats will be added shortly.
                {isCurrentUser && (
                  <FontAwesomeIcon
                    icon={faUnlink}
                    title="Unlink LoL"
                    className={Style.unlink}
                    onClick={() => this.onUnlink("lol")}
                  />
                )}
              </div>
            )}
            {modal && modalSection === "league of legends" && (
              <AddLeagueOfLegendsContainer show={modal} onHide={this.onHide} edit={edit} />
            )}
          </>
        )}
      </>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user.currentUser,
  modal: state.portfolio.modal,
  modalSection: state.portfolio.modalSection,
  currentUser: state.auth.currentUser,
  mockExternalUser: state.portfolio.mockExternalUser
});

export default connect(mapStateToProps, {
  togglePortfolioModal: _togglePortfolioModal,
  updateUserSimple: _updateUserSimple,
  unlinkGameAccount: _unlinkGameAccount
})(withRouter(Index));
