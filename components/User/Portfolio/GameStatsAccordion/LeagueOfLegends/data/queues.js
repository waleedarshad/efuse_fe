const LeagueOfLegendsQueues = {
  RANKED_SOLO_QUEUE: "RANKED_SOLO_5x5",
  RANKED_FLEX_QUEUE: "RANKED_FLEX_SR"
};

export default LeagueOfLegendsQueues;
