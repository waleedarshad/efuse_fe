import { connect } from "react-redux";

import AddLeagueOfLegends from "./AddLeagueOfLegends";

import { dispatchNotification } from "../../../../../../store/actions/notificationActions";
import { togglePortfolioModal } from "../../../../../../store/actions/portfolioActions";
import { getCurrentUser } from "../../../../../../store/actions/common/userAuthActions";

const mapStateToProps = state => ({
  inProgress: state.portfolio.inProgress
});

export default connect(mapStateToProps, {
  dispatchNotification,
  togglePortfolioModal,
  getCurrentUser
})(AddLeagueOfLegends);
