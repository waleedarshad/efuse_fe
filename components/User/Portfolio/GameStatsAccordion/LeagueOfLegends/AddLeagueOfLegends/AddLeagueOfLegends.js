import axios from "axios";
import React, { useEffect, useState } from "react";
import { Form, Col } from "react-bootstrap";

import Style from "./AddLeague.module.scss";

import regions from "../data/regions.json";

import InputField from "../../../../../InputField/InputField";
import InputLabel from "../../../../../InputLabel/InputLabel";
import SelectBox from "../../../../../SelectBox/SelectBox";

import { LOL_CONNECTION_EVENTS } from "../../../../../../common/analyticEvents";
import { sendNotification } from "../../../../../../helpers/FlashHelper";
import { withCdn } from "../../../../../../common/utils";
import EFPrimaryModal from "../../../../../Modals/EFPrimaryModal/EFPrimaryModal";

const AddLeagueOfLegends = ({ edit, leagueoflegends, user, togglePortfolioModal, getCurrentUser, show, onHide }) => {
  const [code, setCode] = useState("");
  const [step, setStep] = useState(1);
  const [validated, setValidated] = useState(false);
  const [leagueInfo, setLeagueInfo] = useState({
    accountName: "",
    region: "BR1",
    _id: ""
  });

  useEffect(() => {
    if (leagueoflegends) {
      setCode(user.leagueoflegends.verificationToken);
    }

    if (edit) {
      setLeagueInfo({
        ...leagueoflegends
      });
    }
  }, []);

  const complete = () => {
    analytics.track(LOL_CONNECTION_EVENTS.VERIFICATION_ATTEMPT, {
      leagueoflegends
    });
    axios
      .put("/leagueoflegends/verify")
      .then(response => {
        togglePortfolioModal(false, "league of legends");

        if (response.data.verified === true) {
          analytics.track(LOL_CONNECTION_EVENTS.VERIFICATION_SUCCESS, {
            data: response.data
          });

          sendNotification("Successfully verified!", "success", "League of Legends");
          getCurrentUser();
        } else {
          analytics.track(LOL_CONNECTION_EVENTS.VERIFICATION_FAIL, {
            data: response.data
          });

          sendNotification(
            "Could not be verified, please verify your Summoner name and Region are correct. Also be sure your verification code was submitted and try again.",
            "danger",
            "League of Legends"
          );
        }
      })
      .catch(error => {
        analytics.track(LOL_CONNECTION_EVENTS.VERIFICATION_ERROR, {
          error
        });
        // eslint-disable-next-line no-console
        console.error(error);

        sendNotification(
          "Could not be verified, please verify your Summoner name and Region are correct. Also be sure your verification code was submitted and try again.",
          "danger",
          "League of Legends"
        );
      });
  };

  const onChange = event => {
    setLeagueInfo({
      ...leagueInfo,
      [event.target.name]: event.target.value
    });
  };

  const onSubmit = event => {
    event.preventDefault();
    const { region, accountName } = leagueInfo;

    analytics.track(LOL_CONNECTION_EVENTS.VERIFICATION_NEXT_STEP, {
      accountName,
      region
    });

    if (!validated) {
      setValidated(true);
    }

    axios
      .post(`/leagueoflegends/updatesummoner/${leagueInfo.accountName}/${leagueInfo.region}`)
      .then(response => {
        setCode(response.data.verificationToken);
      })
      .catch(error => {
        analytics.track(LOL_CONNECTION_EVENTS.VERIFICATION_ERROR, {
          error
        });
        // eslint-disable-next-line no-console
        console.error(error);
      });

    setStep(2);
  };

  const switchStep = () => {
    analytics.track(LOL_CONNECTION_EVENTS.VERIFICATION_PREVIOUS_STEP);

    setStep(1);
  };

  const { region, accountName } = leagueInfo;
  return (
    <EFPrimaryModal
      title="Add League of Legends Account"
      isOpen={show}
      onClose={onHide}
      allowBackgroundClickClose={false}
    >
      {step === 1 && (
        <Form noValidate validated={validated} onSubmit={onSubmit} className={Style.form}>
          <p className={Style.titleDesc}>
            Enter your summoner&apos;s name and region below, then press the &quot;Next Step&quot; button to proceed
            with linking your account.
          </p>
          <Col>
            <InputLabel theme="darkColor">League of Legends Account Name</InputLabel>
            <InputField
              name="accountName"
              placeholder="League of Legends Account Name"
              theme="whiteShadow"
              size="lg"
              onChange={onChange}
              value={accountName}
              required
              errorMessage="League of legends account name is required"
              maxLength={30}
            />
          </Col>
          <br />
          <Col>
            <InputLabel theme="darkColor">Region</InputLabel>
            <SelectBox
              name="region"
              validated={validated}
              options={regions}
              size="lg"
              theme="whiteShadow"
              onChange={onChange}
              value={region}
              required
              errorMessage="Region is required"
            />
          </Col>
          <br />
          <button type="button" className={Style.nextStep} onClick={onSubmit}>
            Next Step
          </button>
        </Form>
      )}
      {step === 2 && (
        <>
          <img className={Style.topImage} src={withCdn("/static/images/league_account_link.png")} alt="account link" />
          <div className={Style.textContainer}>
            <p className={Style.desc}>
              Log into your LoL account. Navigate to the settings section, then select &quot;VERIFICATION&quot; and
              enter the code below. Click on the &quot;SAVE&quot; button. Lastly, select &quot;Complete&quot; below to
              finish linking your account!
            </p>
            <p className={Style.code}>{code}</p>
            <button type="button" className={Style.nextStep} onClick={switchStep}>
              Previous Step
            </button>
            <button type="button" className={Style.nextStep} onClick={complete}>
              Complete
            </button>
          </div>
        </>
      )}
    </EFPrimaryModal>
  );
};

export default AddLeagueOfLegends;
