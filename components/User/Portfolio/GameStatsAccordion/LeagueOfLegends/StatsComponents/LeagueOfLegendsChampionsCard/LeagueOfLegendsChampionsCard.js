import React, { useEffect, useState } from "react";
import { withCdn } from "../../../../../../../common/utils";
import CharacterStats from "../../../../../../GameStats/TopCharactersCard/CharacterStats/CharacterStats";
import TopCharactersCard from "../../../../../../GameStats/TopCharactersCard/TopCharactersCard";

const LeagueOfLegendsChampionsCard = ({ championInfo }) => {
  const [champions, setChampions] = useState();

  useEffect(() => {
    if (championInfo) {
      const champs = championInfo.slice(0, 3).map(champ => {
        return {
          ...champ,
          iconImage: champ.iconImage || `/static/league-of-legends/champions/${champ?.championId}_icon.jpg`,
          points: champ.championPoints,
          name: champ.championName
        };
      });

      setChampions(champs);
    }
  }, []);

  return (
    <TopCharactersCard
      headerText="TOP CHAMPIONS"
      charactersData={champions}
      renderCharacter={character => (
        <CharacterStats
          avatar={withCdn(character.iconImage, true)}
          name={character.name}
          stats={[{ value: character.points, unit: "CP" }]}
        />
      )}
    />
  );
};

export default LeagueOfLegendsChampionsCard;
