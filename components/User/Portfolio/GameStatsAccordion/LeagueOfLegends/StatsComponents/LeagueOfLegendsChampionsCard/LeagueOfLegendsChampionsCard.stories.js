import React from "react";
import LeagueOfLegendsChampionsCard from "./LeagueOfLegendsChampionsCard";

export default {
  title: "Stats/LeagueOfLegends",
  component: LeagueOfLegendsChampionsCard,
  argTypes: {
    championInfo: {
      control: {
        type: "object"
      }
    }
  }
};

// eslint-disable-next-line react/jsx-props-no-spreading
const Story = args => <LeagueOfLegendsChampionsCard {...args} />;

export const ChampionsCard = Story.bind({});
ChampionsCard.args = {
  championInfo: [
    {
      _id: "5fd7c445c68ec0046d0b4ed5",
      championId: 432,
      championLevel: 7,
      championPoints: 628061,
      lastPlayTime: 1605229566000,
      championPointsSinceLastLevel: 606461,
      championPointsUntilNextLevel: 0,
      chestGranted: true,
      tokensEarned: 0,
      summonerId: "nR3HdrFVnCz6lgC-VMZ670lut59yUHqw4_k-OloJpyr7",
      championName: "Bard",
      loadingImage: "static/league-of-legends/champions/432_loading.jpg",
      iconImage: "static/league-of-legends/champions/432_icon.jpg",
      splashImage: "static/league-of-legends/champions/432_splash.jpg"
    },
    {
      _id: "5fd7c445c68ec01d070b4ed6",
      championId: 412,
      championLevel: 7,
      championPoints: 571985,
      lastPlayTime: 1607972902000,
      championPointsSinceLastLevel: 550385,
      championPointsUntilNextLevel: 0,
      chestGranted: true,
      tokensEarned: 0,
      summonerId: "nR3HdrFVnCz6lgC-VMZ670lut59yUHqw4_k-OloJpyr7",
      championName: "Thresh",
      loadingImage: "static/league-of-legends/champions/412_loading.jpg",
      iconImage: "static/league-of-legends/champions/412_icon.jpg",
      splashImage: "static/league-of-legends/champions/412_splash.jpg"
    },
    {
      _id: "5fd7c445c68ec086c40b4ed7",
      championId: 25,
      championLevel: 7,
      championPoints: 279762,
      lastPlayTime: 1604971402000,
      championPointsSinceLastLevel: 258162,
      championPointsUntilNextLevel: 0,
      chestGranted: true,
      tokensEarned: 0,
      summonerId: "nR3HdrFVnCz6lgC-VMZ670lut59yUHqw4_k-OloJpyr7",
      championName: "Morgana",
      loadingImage: "static/league-of-legends/champions/25_loading.jpg",
      iconImage: "static/league-of-legends/champions/25_icon.jpg",
      splashImage: "static/league-of-legends/champions/25_splash.jpg"
    },
    {
      _id: "5fd7c445c68ec067240b4ed8",
      championId: 12,
      championLevel: 7,
      championPoints: 246162,
      lastPlayTime: 1607970583000,
      championPointsSinceLastLevel: 224562,
      championPointsUntilNextLevel: 0,
      chestGranted: true,
      tokensEarned: 0,
      summonerId: "nR3HdrFVnCz6lgC-VMZ670lut59yUHqw4_k-OloJpyr7",
      championName: "Alistar",
      loadingImage: "static/league-of-legends/champions/12_loading.jpg",
      iconImage: "static/league-of-legends/champions/12_icon.jpg",
      splashImage: "static/league-of-legends/champions/12_splash.jpg"
    },
    {
      _id: "5fd7c445c68ec0a9370b4ed9",
      championId: 89,
      championLevel: 7,
      championPoints: 217581,
      lastPlayTime: 1604968749000,
      championPointsSinceLastLevel: 195981,
      championPointsUntilNextLevel: 0,
      chestGranted: true,
      tokensEarned: 0,
      summonerId: "nR3HdrFVnCz6lgC-VMZ670lut59yUHqw4_k-OloJpyr7",
      championName: "Leona",
      loadingImage: "static/league-of-legends/champions/89_loading.jpg",
      iconImage: "static/league-of-legends/champions/89_icon.jpg",
      splashImage: "static/league-of-legends/champions/89_splash.jpg"
    },
    {
      _id: "5fd7c445c68ec085190b4eda",
      championId: 53,
      championLevel: 7,
      championPoints: 216741,
      lastPlayTime: 1603092055000,
      championPointsSinceLastLevel: 195141,
      championPointsUntilNextLevel: 0,
      chestGranted: true,
      tokensEarned: 0,
      summonerId: "nR3HdrFVnCz6lgC-VMZ670lut59yUHqw4_k-OloJpyr7",
      championName: "Blitzcrank",
      loadingImage: "static/league-of-legends/champions/53_loading.jpg",
      iconImage: "static/league-of-legends/champions/53_icon.jpg",
      splashImage: "static/league-of-legends/champions/53_splash.jpg"
    },
    {
      _id: "5fd7c445c68ec082750b4edb",
      championId: 555,
      championLevel: 7,
      championPoints: 199111,
      lastPlayTime: 1607668673000,
      championPointsSinceLastLevel: 177511,
      championPointsUntilNextLevel: 0,
      chestGranted: true,
      tokensEarned: 0,
      summonerId: "nR3HdrFVnCz6lgC-VMZ670lut59yUHqw4_k-OloJpyr7",
      championName: "Pyke",
      loadingImage: "static/league-of-legends/champions/555_loading.jpg",
      iconImage: "static/league-of-legends/champions/555_icon.jpg",
      splashImage: "static/league-of-legends/champions/555_splash.jpg"
    },
    {
      _id: "5fd7c445c68ec0349f0b4edc",
      championId: 201,
      championLevel: 7,
      championPoints: 128723,
      lastPlayTime: 1605781587000,
      championPointsSinceLastLevel: 107123,
      championPointsUntilNextLevel: 0,
      chestGranted: true,
      tokensEarned: 0,
      summonerId: "nR3HdrFVnCz6lgC-VMZ670lut59yUHqw4_k-OloJpyr7",
      championName: "Braum",
      loadingImage: "static/league-of-legends/champions/201_loading.jpg",
      iconImage: "static/league-of-legends/champions/201_icon.jpg",
      splashImage: "static/league-of-legends/champions/201_splash.jpg"
    },
    {
      _id: "5fd7c445c68ec021330b4edd",
      championId: 43,
      championLevel: 7,
      championPoints: 118550,
      lastPlayTime: 1605118361000,
      championPointsSinceLastLevel: 96950,
      championPointsUntilNextLevel: 0,
      chestGranted: true,
      tokensEarned: 0,
      summonerId: "nR3HdrFVnCz6lgC-VMZ670lut59yUHqw4_k-OloJpyr7",
      championName: "Karma",
      loadingImage: "static/league-of-legends/champions/43_loading.jpg",
      iconImage: "static/league-of-legends/champions/43_icon.jpg",
      splashImage: "static/league-of-legends/champions/43_splash.jpg"
    },
    {
      _id: "5fd7c445c68ec074d60b4ede",
      championId: 497,
      championLevel: 7,
      championPoints: 118522,
      lastPlayTime: 1606706035000,
      championPointsSinceLastLevel: 96922,
      championPointsUntilNextLevel: 0,
      chestGranted: true,
      tokensEarned: 0,
      summonerId: "nR3HdrFVnCz6lgC-VMZ670lut59yUHqw4_k-OloJpyr7",
      championName: "Rakan",
      loadingImage: "static/league-of-legends/champions/497_loading.jpg",
      iconImage: "static/league-of-legends/champions/497_icon.jpg",
      splashImage: "static/league-of-legends/champions/497_splash.jpg"
    }
  ]
};
