import { faUnlink } from "@fortawesome/pro-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import React from "react";
import { withCdn } from "../../../../../../common/utils";
import Style from "./LeagueOfLegendsStats.module.scss";
import EFImage from "../../../../../EFImage/EFImage";

import LeagueOfLegendsLeagueCard from "./LeagueOfLegendsLeagueCard/LeagueOfLegendsLeagueCard";
import LeagueOfLegendsChampionsCard from "./LeagueOfLegendsChampionsCard/LeagueOfLegendsChampionsCard";
import LeagueOfLegendsQueues from "../data/queues";
import HorizontalScroll from "../../../../../HorizontalScroll/HorizontalScroll";

const LeagueOfLegendsStats = ({ userStats, isCurrentUser, onClick, styleClass }) => {
  const { stats } = userStats;
  const rankedSoloStats = stats?.leagueInfo?.find(li => li.queueType === LeagueOfLegendsQueues.RANKED_SOLO_QUEUE);
  const rankedFlexStats = stats?.leagueInfo?.find(li => li.queueType === LeagueOfLegendsQueues.RANKED_FLEX_QUEUE);

  return (
    <>
      {isCurrentUser && (
        <FontAwesomeIcon icon={faUnlink} title="Unlink League of Legends" onClick={onClick} className={styleClass} />
      )}
      <div className={Style.rootContainer}>
        <HorizontalScroll>
          <div className={Style.statDetails}>
            <div className={Style.gameImage}>
              <EFImage src={withCdn("/static/images/league_of_legends.jpg")} height="170px" width="128px" />
            </div>
            <div className={Style.statCard} key={LeagueOfLegendsQueues.RANKED_SOLO_QUEUE}>
              <LeagueOfLegendsLeagueCard
                queueType={LeagueOfLegendsQueues.RANKED_SOLO_QUEUE}
                leagueInfo={rankedSoloStats}
                userStats={userStats}
              />
            </div>
            <div className={Style.statCard} key={LeagueOfLegendsQueues.RANKED_FLEX_QUEUE}>
              <LeagueOfLegendsLeagueCard
                queueType={LeagueOfLegendsQueues.RANKED_FLEX_QUEUE}
                leagueInfo={rankedFlexStats}
                userStats={userStats}
              />
            </div>
            <LeagueOfLegendsChampionsCard championInfo={stats?.championInfo} />
          </div>
        </HorizontalScroll>
      </div>
    </>
  );
};

export default LeagueOfLegendsStats;
