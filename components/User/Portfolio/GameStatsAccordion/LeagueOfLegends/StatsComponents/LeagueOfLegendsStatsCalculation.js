const leagueOfLegendsStatsCalculation = ({ userStats }) => {
  if (!userStats?.verified) {
    return null;
  }

  const { riotNickname, stats } = userStats;

  if (!stats) {
    return null;
  }

  const { championInfo, leagueInfo } = stats;

  let wins;
  let losses;
  let hasRankedGames = false;
  const userName = riotNickname;
  let topChampion = null;

  if (leagueInfo?.length > 0) {
    hasRankedGames = true;

    let w = 0;
    let l = 0;

    leagueInfo.forEach(li => {
      w += li.wins;
      l += li.losses;
    });

    wins = w;
    losses = l;
  }

  if (championInfo?.length > 0) {
    const champ = championInfo[0];
    champ.iconImage = champ.iconImage
      ? champ.iconImage
      : `static/league-of-legends/champions/${champ.championId}_icon.jpg`;

    // eslint-disable-next-line prefer-destructuring
    topChampion = championInfo[0];
  }

  const winPercent = () => {
    return ((wins / (wins + losses)) * 100).toPrecision(4);
  };
  return {
    userName,
    wins,
    losses,
    ranked: hasRankedGames,
    championTitle: topChampion ? "TOP CHAMPION" : undefined,
    championName: topChampion?.championName,
    championAvatar: topChampion?.iconImage,
    winPercent
  };
};

export default leagueOfLegendsStatsCalculation;
