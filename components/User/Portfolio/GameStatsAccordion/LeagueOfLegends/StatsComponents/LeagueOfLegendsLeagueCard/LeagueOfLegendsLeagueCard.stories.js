import React from "react";
import LeagueOfLegendsLeagueCard from "./LeagueOfLegendsLeagueCard";

export default {
  title: "Stats/LeagueOfLegends",
  component: LeagueOfLegendsLeagueCard,
  argTypes: {
    leagueInfo: {
      control: {
        type: "object"
      }
    }
  }
};

// eslint-disable-next-line react/jsx-props-no-spreading
const Story = args => <LeagueOfLegendsLeagueCard {...args} />;

export const LeagueCard = Story.bind({});
LeagueCard.args = {
  leagueInfo: {
    _id: "5fd7c445c68ec07f830b4ed3",
    leagueId: "c60807e8-6afb-38fd-ab9b-ae8588dc8b27",
    queueType: "RANKED_SOLO_5x5",
    tier: "CHALLENGER",
    rank: "I",
    summonerId: "nR3HdrFVnCz6lgC-VMZ670lut59yUHqw4_k-OloJpyr7",
    summonerName: "aphromoo",
    leaguePoints: 853,
    wins: 391,
    losses: 358,
    veteran: true,
    inactive: false,
    freshBlood: false,
    hotStreak: false,
    tierImage: "static/league-of-legends/ranked-emblems/CHALLENGER.png"
  }
};
