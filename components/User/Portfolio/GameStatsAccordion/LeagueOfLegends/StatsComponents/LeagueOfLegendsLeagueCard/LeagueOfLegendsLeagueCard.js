import React, { useEffect, useState } from "react";

import GameCard from "../../../../../../GameStats/GameCard/GameCard";
import LeagueOfLegendsQueues from "../../data/queues";

const LeagueOfLegendsChampionsCard = ({ leagueInfo, queueType, userStats }) => {
  const { riotNickname } = userStats;
  const [queueName, setQueueName] = useState("");
  const [leaguePoints, setLeaguePoints] = useState("");
  const [wins, setWins] = useState(0);
  const [losses, setLosses] = useState(0);
  const [tierImage, setTierImage] = useState("");
  const [tierName, setTierName] = useState("");

  const winPercent = () => {
    return ((wins / (wins + losses)) * 100).toPrecision(4);
  };

  // load the stats
  useEffect(() => {
    switch (queueType) {
      case LeagueOfLegendsQueues.RANKED_SOLO_QUEUE:
        setQueueName("Ranked Solo 5:5");
        break;
      case LeagueOfLegendsQueues.RANKED_FLEX_QUEUE:
        setQueueName("Ranked Flex 5:5");
        break;
      default:
        setQueueName(queueType);
        break;
    }

    if (leagueInfo) {
      setLeaguePoints(leagueInfo.leaguePoints);
      setWins(leagueInfo.wins);
      setLosses(leagueInfo.losses);
      setTierImage(leagueInfo.tierImage);
      setTierName(`${leagueInfo?.tier?.toLowerCase()} ${leagueInfo?.rank}`);
    }
  });

  return (
    <GameCard
      headerText={queueName}
      cardBodyStats={{ unit: "LP", value: leaguePoints }}
      cardFooterStats={[
        {
          unit1: "W",
          value1: wins,
          unit2: "L",
          value2: losses,
          winRatio: winPercent(wins, losses)
        }
      ]}
      badgeImage={tierImage}
      badgeTitle={tierName}
      structure="GroupStats"
      tooltip={`Stats are for the current League of Legends season only. For more detailed stats head on over to our
                friends at <a href=https://na.op.gg/summoner/userName=${riotNickname} target="_blank"> op.gg </a>`}
    />
  );
};

export default LeagueOfLegendsChampionsCard;
