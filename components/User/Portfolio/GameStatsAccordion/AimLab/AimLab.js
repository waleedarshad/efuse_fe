import isEmpty from "lodash/isEmpty";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { faPlus } from "@fortawesome/pro-solid-svg-icons";

import { EXTERNAL_AUTH_SERVICES } from "../../../../../common/externalAuthServices";
import getGamesHook from "../../../../../hooks/getHooks/getGamesHook";
import {
  deleteLinkForAimlab,
  getAimlabAuthStatusForPortfolioUser
} from "../../../../../store/actions/externalAuth/externalAuthActions";
import { unlinkConfirmation } from "../../../../AuthenticateAccounts/utils";
import StatsCardWrapper from "../../../../Cards/StatsCardWrapper/StatsCardWrapper";
import EFGameCard from "../../../../EFGameCardList/EFGameCard/EFGameCard";
import GameRankCard from "../../../../GameStats/GameRankCard/GameRankCard";
import SkillsSection from "../../../../GameStats/SkillsSection/SkillsSection";
import authenticateAccount from "../../../../hoc/authenticateAccount";
import AddButton from "../../AddButton/AddButton";
import Style from "./AimLab.module.scss";
import { AIMLAB_SKILLS } from "./useAimLabStats";

const AimLab = ({ startOAuth, stats }) => {
  const isUserAuthenticatedWithStatespace = useSelector(state => state.user.currentUser.statespace);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getAimlabAuthStatusForPortfolioUser());
  }, []);

  const gamesList = getGamesHook();

  const findAimLabImage = () => {
    const aimlabImage = gamesList.find(game => game.slug === "aim-lab");
    return aimlabImage?.gameImage?.url;
  };

  if (!isUserAuthenticatedWithStatespace) {
    return (
      <AddButton
        desc="Add Aim Lab Account"
        icon={faPlus}
        onClick={() => startOAuth(EXTERNAL_AUTH_SERVICES.STATESPACE)}
      />
    );
  }

  const unlink = () => {
    unlinkConfirmation(EXTERNAL_AUTH_SERVICES.STATESPACE, () => dispatch(deleteLinkForAimlab()));
  };

  if (isEmpty(stats)) {
    return (
      <StatsCardWrapper onUnlinkClick={unlink} unlinkTooltipMessage="Unlink Aim Lab">
        <p>You are linked. Your stats will appear here shortly.</p>
      </StatsCardWrapper>
    );
  }

  return (
    <StatsCardWrapper onUnlinkClick={unlink} unlinkTooltipMessage="Unlink Aim Lab">
      <div className={Style.gameImage}>
        <EFGameCard gameImage={findAimLabImage()} size="small" />
      </div>
      <div className={Style.cardWrapper}>
        <GameRankCard
          image={stats.ranking.badgeImage}
          label={stats.ranking.name}
          subText={`Skill Rating: ${stats.ranking?.rating ? stats.ranking?.rating.toFixed(6) : "Unranked"}`}
        />
      </div>
      <div className={Style.cardWrapper}>
        <SkillsSection
          skills={[
            { value: stats.skills[AIMLAB_SKILLS.COGNITION], label: "Cognition" },
            { value: stats.skills[AIMLAB_SKILLS.FLICKING], label: "Flicking" },
            { value: stats.skills[AIMLAB_SKILLS.PERCEPTION], label: "Perception" },
            { value: stats.skills[AIMLAB_SKILLS.PRECISION], label: "Precision" },
            { value: stats.skills[AIMLAB_SKILLS.SPEED], label: "Speed" },
            { value: stats.skills[AIMLAB_SKILLS.TRACKING], label: "Tracking" }
          ]}
        />
      </div>
    </StatsCardWrapper>
  );
};

export default authenticateAccount(AimLab);
