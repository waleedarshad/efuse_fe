import isEmpty from "lodash/isEmpty";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getAimlabAuthStatusForPortfolioUser } from "../../../../../store/actions/externalAuth/externalAuthActions";
import GameHeader from "../../../../GameStats/GameHeader/GameHeader";

const AimLabGameHeader = ({ stats }) => {
  const isUserVerifiedWithStateSpace = useSelector(state => state.user.currentUser.statespace);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getAimlabAuthStatusForPortfolioUser());
  }, []);

  if (!isUserVerifiedWithStateSpace || isEmpty(stats)) {
    return <></>;
  }

  return (
    <GameHeader
      userName={`USERNAME: ${stats.userName}`}
      platformLogo="/static/images/aimlab_icon_black.svg"
      platformName="AIM LAB"
      ranked={!!stats.ranking?.rating}
    />
  );
};

export default AimLabGameHeader;
