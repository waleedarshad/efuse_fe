import { useQuery } from "@apollo/client";
import isNil from "lodash/isNil";
import { useEffect, useRef } from "react";
import { GET_AIMLAB_STATS_FOR_USER } from "../../../../../graphql/UserQuery";

export const AIMLAB_SKILLS = {
  COGNITION: "cognition",
  FLICKING: "flicking",
  PERCEPTION: "perception",
  PRECISION: "precision",
  SPEED: "speed",
  TRACKING: "tracking"
};

const useAimLabStats = userId => {
  const { error, loading, data } = useQuery(GET_AIMLAB_STATS_FOR_USER, { variables: { id: userId } });

  const aimLabStats = useRef({});

  useEffect(() => {
    if (loading || error || isNil(data?.getUserById?.aimlabStats)) {
      aimLabStats.current = {};
    } else {
      const stats = data.getUserById?.aimlabStats;

      const allSkillsFromServer = stats.skillScores.reduce((acc, skill) => ({ ...acc, [skill.name]: skill.score }), {});

      aimLabStats.current = {
        userName: stats.username,
        ranking: {
          name: stats.ranking.rank.displayName,
          badgeImage: stats.ranking.badgeImage,
          rating: stats.ranking.skill
        },
        skills: Object.values(AIMLAB_SKILLS).reduce(
          (acc, supportedSkill) => ({ ...acc, [supportedSkill]: allSkillsFromServer[supportedSkill]?.toFixed(1) }),
          {}
        )
      };
    }
  }, [loading, error, data]);

  return aimLabStats.current;
};

export default useAimLabStats;
