import React from "react";
import AccordionHeader from "../../../../Accordion/AccordionHeader/AccordionHeader";
import GameHeader from "../../../../GameStats/GameHeader/GameHeader";
import AimLab from "../AimLab/AimLab";
import AimLabGameHeader from "../AimLab/AimLabGameHeader";
import LeagueOfLegends from "../LeagueOfLegends/Index";
import Overwatch from "../Overwatch/Index";
import Pubg from "../Pubg/Pubg";
import Valorant from "../Valorant/Valorant";
import ValorantGameHeader from "../Valorant/ValorantGameHeader";

const accordionTab = (overwatchStats, lolStats, valorantStats, aimLabStats) => [
  {
    title: (
      <AccordionHeader
        text="League of Legends"
        component={
          lolStats && (
            <GameHeader
              userName={lolStats.userName}
              wins={lolStats.wins}
              losses={lolStats.losses}
              ranked={lolStats.ranked}
              characterTitle={lolStats.championTitle}
              characterName={lolStats.championName}
              characterAvatar={lolStats.championAvatar}
              winPercent={lolStats.winPercent}
            />
          )
        }
      />
    ),
    component: LeagueOfLegends,
    tabFeature: "leagueoflegends_section",
    auth: "leagueOfLegends"
  },
  {
    title: (
      <AccordionHeader
        text="Overwatch"
        component={
          overwatchStats && (
            <GameHeader
              userName={overwatchStats.userName}
              wins={overwatchStats.totalWins}
              losses={overwatchStats.totalLosses}
              ranked={overwatchStats.hasRankedGames}
              winPercent={overwatchStats.winPercent}
              platformLogo={overwatchStats.getPlatformIcon()}
              platformName={overwatchStats.platform}
              hasPlatform
            />
          )
        }
      />
    ),
    component: Overwatch,
    tabFeature: "overwatch_section",
    auth: "overwatch"
  },
  {
    title: "PUBG",
    component: Pubg,
    tabFeature: "pubg_section",
    auth: "pubg"
  },
  {
    title: <AccordionHeader text="Aim Lab" component={<AimLabGameHeader stats={aimLabStats} />} />,
    // eslint-disable-next-line react/display-name
    component: () => <AimLab stats={aimLabStats} />,
    tabFeature: "aimlab_section",
    auth: "statespace"
  },
  {
    title: <AccordionHeader text="Valorant" component={<ValorantGameHeader stats={valorantStats} />} />,
    // eslint-disable-next-line react/display-name
    component: () => <Valorant stats={valorantStats} />,
    tabFeature: "valorant_section",
    auth: "riot"
  }
];

export default accordionTab;
