import React from "react";
import { withCdn } from "../../../../../../common/utils";
import CharacterStats from "../../../../../GameStats/TopCharactersCard/CharacterStats/CharacterStats";
import TopCharactersCard from "../../../../../GameStats/TopCharactersCard/TopCharactersCard";

const ValorantTopAgents = ({ stats }) => {
  return (
    <TopCharactersCard
      headerText="TOP AGENTS"
      tooltip="Stats are for recent ranked games in the current act"
      charactersData={stats.topAgents}
      renderCharacter={agent => (
        <CharacterStats
          avatar={withCdn(agent.avatar, true)}
          name={agent.name}
          stats={[
            { value: agent.stats.wins, unit: "W" },
            { value: agent.stats.losses, unit: "L" },
            { value: agent.stats.kd, unit: "KD" }
          ]}
          description={`Win Ratio ${agent.stats.winPercent}%`}
        />
      )}
    />
  );
};

export default ValorantTopAgents;
