import GameCard from "../../../../../GameStats/GameCard/GameCard";

const ValorantKDCard = ({ stats }) => (
  <GameCard
    headerText="GAME STATS"
    tooltip="Stats are for recent ranked games in the current act"
    noStatsMessage="Player has no stats to show"
    cardBodyStats={{ unit: "KD", value: stats.totals?.kd, rank: `Primary Agent: ${stats.primaryAgent.stats?.kd} KD` }}
    cardFooterStats={[
      { unit: "KPG", value: stats.totals.kpg, subText: `${stats.totals.kills} Kills` },
      { unit: "DPG", value: stats.totals.dpg, subText: `${stats.totals.damage} Damage` },
      { unit: "APG", value: stats.totals.apg, subText: `${stats.totals.assists} Assists` }
    ]}
  />
);

export default ValorantKDCard;
