import isEmpty from "lodash/isEmpty";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { faPlus } from "@fortawesome/pro-solid-svg-icons";

import { EXTERNAL_AUTH_SERVICES } from "../../../../../common/externalAuthServices";
import getGamesHook from "../../../../../hooks/getHooks/getGamesHook";
import {
  deleteLinkForValorant,
  getValorantAuthStatusForPortfolioUser
} from "../../../../../store/actions/externalAuth/externalAuthActions";
import { unlinkConfirmation } from "../../../../AuthenticateAccounts/utils";
import StatsCardWrapper from "../../../../Cards/StatsCardWrapper/StatsCardWrapper";
import EFGameCard from "../../../../EFGameCardList/EFGameCard/EFGameCard";
import authenticateAccount from "../../../../hoc/authenticateAccount";
import AddButton from "../../AddButton/AddButton";
import Style from "./Valorant.module.scss";
import ValorantKDCard from "./ValorantKDCard/ValorantKDCard";
import ValorantRankCard from "./ValorantRankCard/ValorantRankCard";
import ValorantTopAgents from "./ValorantTopAgents/ValorantTopAgents";

const Valorant = ({ startOAuth, stats }) => {
  const isUserAuthenticatedWithRiot = useSelector(state => state.user.currentUser.riot);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getValorantAuthStatusForPortfolioUser());
  }, []);

  const gamesList = getGamesHook();

  const findValorantImage = () => {
    const valorantGame = gamesList.find(game => game.slug === "valorant");
    return valorantGame?.gameImage?.url;
  };

  if (!isUserAuthenticatedWithRiot) {
    return (
      <AddButton desc="Add Valorant Account" icon={faPlus} onClick={() => startOAuth(EXTERNAL_AUTH_SERVICES.RIOT)} />
    );
  }

  const unlink = () => {
    unlinkConfirmation(EXTERNAL_AUTH_SERVICES.RIOT, () => dispatch(deleteLinkForValorant()));
  };

  if (isEmpty(stats)) {
    return (
      <StatsCardWrapper onUnlinkClick={unlink} unlinkTooltipMessage="Unlink Valorant">
        <p>You are linked. Your stats will appear here shortly.</p>{" "}
      </StatsCardWrapper>
    );
  }

  return (
    <StatsCardWrapper onUnlinkClick={unlink} unlinkTooltipMessage="Unlink Valorant">
      <div className={Style.gameImage}>
        <EFGameCard gameImage={findValorantImage()} size="small" />
      </div>
      <div className={Style.cardWrapper}>
        <ValorantRankCard stats={stats} />
      </div>
      <div className={Style.cardWrapper}>
        <ValorantKDCard stats={stats} />
      </div>
      <ValorantTopAgents stats={stats} />
    </StatsCardWrapper>
  );
};

export default authenticateAccount(Valorant);
