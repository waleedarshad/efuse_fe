import isEmpty from "lodash/isEmpty";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getValorantAuthStatusForPortfolioUser } from "../../../../../store/actions/externalAuth/externalAuthActions";
import GameHeader from "../../../../GameStats/GameHeader/GameHeader";

const ValorantGameHeader = ({ stats }) => {
  const isUserVerifiedWithRiot = useSelector(state => state.user.currentUser.riot);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getValorantAuthStatusForPortfolioUser());
  }, []);

  if (!isUserVerifiedWithRiot || isEmpty(stats)) {
    return <></>;
  }

  return (
    <GameHeader
      userName={stats.userName}
      wins={stats.totals.wins}
      losses={stats.totals.losses}
      winPercent={stats.totals.winPercent}
      ranked={!!stats.totals.wins && !!stats.totals.losses}
      characterTitle="Primary Agent"
      characterName={stats.primaryAgent.name}
      characterAvatar={stats.primaryAgent.avatar}
      characterWins={stats.primaryAgent.stats?.wins}
      characterLosses={stats.primaryAgent.stats?.losses}
      characterWinPercent={stats.primaryAgent.stats?.winPercent}
      characterKd={stats.primaryAgent.stats?.kd}
    />
  );
};

export default ValorantGameHeader;
