import isNil from "lodash/isNil";
import GameCard from "../../../../../GameStats/GameCard/GameCard";

const ValorantRankCard = ({ stats }) => {
  const shouldDisplayFooter = !isNil(stats.totals.wins) && !isNil(stats.totals.losses);

  return (
    <GameCard
      headerText={`${stats.episodeName}: ${stats.actName}`}
      tooltip="Stats are for recent ranked games in the current act"
      badgeImage={stats.competitiveTier?.image}
      cardBodyStats={{
        value: stats.competitiveTier?.name,
        rank: `Rank Rating: ${stats.competitiveTier?.rankRating || "Unranked"}`
      }}
      cardFooterStats={[
        {
          unit1: "W",
          value1: stats.totals.wins,
          unit2: "L",
          value2: stats.totals.losses,
          winRatio: stats.totals.winPercent
        },
        {
          unit1: "GP",
          value1: stats.totals.gamesPlayed
        }
      ]}
      shouldDisplayFooter={shouldDisplayFooter}
      structure="GroupStats"
      noStatsMessage={`This gamer is unranked for ${stats.episodeName}: ${stats.actName}`}
    />
  );
};

export default ValorantRankCard;
