import { calculateValorantStats } from "./useValorantStats";

describe("useValorantStats", () => {
  describe("calculateValorantStats", () => {
    let rawStats;

    beforeEach(() => {
      rawStats = {
        gameName: "notRitzy",
        tagLine: "NA1",
        episodeName: "episode 2",
        actName: "act 3",
        rankedRating: 33,
        losses: 2,
        wins: 5,
        assists: 40,
        kills: 119,
        deaths: 127,
        damage: 100,
        competitiveTierName: "Gold 1",
        competitiveTierImage: "/some-image",
        agentMatchStats: [
          {
            agentData: {
              displayName: "Raze",
              displayIcon: "static/valorant/agents/f94c3b30-42be-e959-889c-5aa313dba261/displayicon.png"
            },
            wins: 1,
            losses: 0,
            deaths: 21,
            kills: 19,
            assists: 6
          },
          {
            agentData: {
              displayName: "Sage",
              displayIcon: "static/valorant/agents/569fdd95-4d10-43ab-ca70-79becc718b46/displayicon.png"
            },
            wins: 0,
            losses: 1,
            deaths: 18,
            kills: 15,
            assists: 7
          },
          {
            agentData: {
              displayName: "Reyna",
              displayIcon: "static/valorant/agents/a3bfb853-43b2-7238-a4f1-ad90e9e46bcc/displayicon.png"
            },
            wins: 1,
            losses: 0,
            deaths: 19,
            kills: 10,
            assists: 8
          }
        ]
      };
    });

    it("returns empty object when there are no stats", () => {
      expect(calculateValorantStats()).toEqual({});
    });

    it("does not return totals when wins and losses are missing", () => {
      delete rawStats.wins;
      delete rawStats.losses;

      const result = calculateValorantStats(rawStats);

      expect(result.totals).toEqual({});
    });

    it("does not return primary agent stats when agents are non existent", () => {
      delete rawStats.agentMatchStats;

      const result = calculateValorantStats(rawStats);

      expect(result.primaryAgent).toEqual({});
    });

    it("does not return primary agent stats when there are no agents", () => {
      rawStats.agentMatchStats = [];

      const result = calculateValorantStats(rawStats);

      expect(result.primaryAgent).toEqual({});
    });

    it("does not return stats for an agent when a single agent raw stat is missing", () => {
      rawStats.agentMatchStats[0].wins = undefined;

      const result = calculateValorantStats(rawStats);

      expect(result.primaryAgent).toEqual({
        name: "Raze",
        avatar: "static/valorant/agents/f94c3b30-42be-e959-889c-5aa313dba261/displayicon.png",
        stats: {}
      });

      expect(result.topAgents).toEqual([
        {
          avatar: "static/valorant/agents/f94c3b30-42be-e959-889c-5aa313dba261/displayicon.png",
          name: "Raze",
          stats: {}
        },
        {
          avatar: "static/valorant/agents/569fdd95-4d10-43ab-ca70-79becc718b46/displayicon.png",
          name: "Sage",
          stats: {
            kd: "0.8",
            losses: 1,
            winPercent: 0,
            wins: 0
          }
        },
        {
          avatar: "static/valorant/agents/a3bfb853-43b2-7238-a4f1-ad90e9e46bcc/displayicon.png",
          name: "Reyna",
          stats: {
            kd: "0.5",
            losses: 0,
            winPercent: 100,
            wins: 1
          }
        }
      ]);
    });

    it("does not return competitiveTier stats if one is missing", () => {
      delete rawStats.competitiveTierName;

      const result = calculateValorantStats(rawStats);

      expect(result.competitiveTier).toEqual({});
    });

    it("returns calculated stats when all data is present", () => {
      const result = calculateValorantStats(rawStats);

      expect(result).toEqual({
        userName: "notRitzy#NA1",
        episodeName: "episode 2",
        actName: "act 3",
        totals: {
          wins: 5,
          losses: 2,
          winPercent: 71,
          gamesPlayed: 7,
          apg: "5.7",
          kd: "0.9",
          kills: 119,
          kpg: "17.0",
          assists: 40,
          damage: 100,
          dpg: "14.3"
        },
        primaryAgent: {
          name: "Raze",
          avatar: "static/valorant/agents/f94c3b30-42be-e959-889c-5aa313dba261/displayicon.png",
          stats: {
            wins: 1,
            losses: 0,
            winPercent: 100,
            kd: "0.9"
          }
        },
        competitiveTier: {
          name: "Gold 1",
          image: "/some-image",
          rankRating: 33
        },
        topAgents: [
          {
            avatar: "static/valorant/agents/f94c3b30-42be-e959-889c-5aa313dba261/displayicon.png",
            name: "Raze",
            stats: {
              kd: "0.9",
              losses: 0,
              winPercent: 100,
              wins: 1
            }
          },
          {
            avatar: "static/valorant/agents/569fdd95-4d10-43ab-ca70-79becc718b46/displayicon.png",
            name: "Sage",
            stats: {
              kd: "0.8",
              losses: 1,
              winPercent: 0,
              wins: 0
            }
          },
          {
            avatar: "static/valorant/agents/a3bfb853-43b2-7238-a4f1-ad90e9e46bcc/displayicon.png",
            name: "Reyna",
            stats: {
              kd: "0.5",
              losses: 0,
              winPercent: 100,
              wins: 1
            }
          }
        ]
      });
    });
  });
});
