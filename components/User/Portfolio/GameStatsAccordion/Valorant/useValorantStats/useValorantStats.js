import { useQuery } from "@apollo/client";
import isNil from "lodash/isNil";
import isEmpty from "lodash/isEmpty";
import { useEffect, useRef } from "react";
import { GET_VALORANT_STATS } from "../../../../../../graphql/ValorantStatsQuery";

const useValorantStats = userId => {
  const { error, loading, data } = useQuery(GET_VALORANT_STATS, { variables: { getValorantStatsUserId: userId } });

  const valorantStats = useRef({});

  useEffect(() => {
    if (loading || error) {
      valorantStats.current = {};
    } else {
      const stats = data.getValorantStats;

      valorantStats.current = calculateValorantStats(stats);
    }
  }, [loading, error, data]);

  return valorantStats.current;
};

export const calculateValorantStats = stats => {
  if (isEmpty(stats)) {
    return {};
  }

  const calculateStatsForAgent = agent => {
    let agentData = {
      name: agent.agentData?.displayName,
      avatar: agent.agentData?.displayIcon,
      stats: {}
    };

    if ([agent.wins, agent.losses, agent.kills, agent.deaths].every(stat => !isNil(stat))) {
      agentData = {
        ...agentData,
        stats: {
          wins: agent.wins,
          losses: agent.losses,
          winPercent: Math.round((agent.wins / (agent.wins + agent.losses)) * 100),
          kd: (agent.kills / (agent.deaths || 1)).toFixed(1)
        }
      };
    }
    return agentData;
  };

  let calculatedStats = {
    userName: `${stats.gameName}#${stats.tagLine}`,
    episodeName: stats.episodeName,
    actName: stats.actName,
    totals: {},
    primaryAgent: {},
    competitiveTier: {},
    topAgents: []
  };

  if ([stats.wins, stats.losses, stats.kills, stats.assists, stats.deaths].every(stat => !isNil(stat))) {
    if (!(stats.wins === 0 && stats.losses === 0)) {
      const totalGamesPlayed = stats.wins + stats.losses;

      calculatedStats = {
        ...calculatedStats,
        totals: {
          wins: stats.wins,
          losses: stats.losses,
          winPercent: Math.round((stats.wins / totalGamesPlayed) * 100),
          gamesPlayed: totalGamesPlayed,
          kd: (stats.kills / (stats.deaths || 1)).toFixed(1),
          kills: stats.kills,
          damage: stats.damage,
          assists: stats.assists,
          kpg: (stats.kills / (totalGamesPlayed || 1)).toFixed(1),
          dpg: (stats.damage / (totalGamesPlayed || 1)).toFixed(1),
          apg: (stats.assists / (totalGamesPlayed || 1)).toFixed(1)
        }
      };
    }
  }

  if (stats.agentMatchStats && stats.agentMatchStats.length > 0) {
    calculatedStats = {
      ...calculatedStats,
      primaryAgent: calculateStatsForAgent(stats.agentMatchStats[0])
    };
  }

  if (!isNil(stats.competitiveTierName) && !isNil(stats.competitiveTierImage)) {
    calculatedStats = {
      ...calculatedStats,
      competitiveTier: {
        name: stats.competitiveTierName,
        image: stats.competitiveTierImage,
        rankRating: stats.rankedRating
      }
    };
  }

  if (stats.agentMatchStats && stats.agentMatchStats.length > 0) {
    const maxNumberOfTopAgentsToDisplay = 3;

    calculatedStats = {
      ...calculatedStats,
      topAgents: stats.agentMatchStats.slice(0, maxNumberOfTopAgentsToDisplay).map(calculateStatsForAgent)
    };
  }

  return calculatedStats;
};

export default useValorantStats;
