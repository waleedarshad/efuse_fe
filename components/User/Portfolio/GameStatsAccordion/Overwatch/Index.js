import cloneDeep from "lodash/cloneDeep";
import { withRouter } from "next/router";
import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUnlink, faPlus } from "@fortawesome/pro-solid-svg-icons";
import { connect } from "react-redux";

import EFAlert from "../../../../EFAlert/EFAlert";
import Style from "./index.module.scss";
import OverwatchStats from "./StatsComponents/OverwatchStats";
import AddButton from "../../AddButton/AddButton";
import AddOverwatch from "./AddOverwatch/AddOverwatch";
import { OVERWATCH_CONNECTION_EVENTS } from "../../../../../common/analyticEvents";
// Note: The underscores are just to avoid the shadowed variables issue.
// This can be changed once we move to functional component
import { togglePortfolioModal as _togglePortfolioModal } from "../../../../../store/actions/portfolioActions";
import {
  updateUserSimple as _updateUserSimple,
  unlinkGameAccount as _unlinkGameAccount
} from "../../../../../store/actions/userActions";

const Index = props => {
  const [waitingStats, setWaitingStats] = useState(false);
  const [isOpen, toggleModal] = useState(false);
  const { user, router, currentUser, mockExternalUser } = props;

  let isCurrentUser = currentUser && router.query.id === currentUser.id;

  useEffect(() => {
    if (user?.overwatch?.stats?._id) {
      setWaitingStats(false);
    }
  });

  const onUnlink = game => {
    let title = "Unlink Overwatch Account?";
    if (game) title = `Unlink ${game.toUpperCase()} Account?`;

    const message = "Are You Sure?";
    const confirmLabel = "Yes";
    const cancelLabel = "No";
    const onConfirm = () => unlinkAccount("overwatch");

    EFAlert(title, message, confirmLabel, cancelLabel, onConfirm);
  };

  const unlinkAccount = game => {
    const { updateUserSimple, unlinkGameAccount } = props;
    const clonedUser = cloneDeep(user);
    delete clonedUser.overwatch;
    updateUserSimple(clonedUser);
    setWaitingStats(false);
    unlinkGameAccount({ game });
  };

  // this allows users on the recruit settings page to add Overwatch accounts
  if (router.pathname === "/settings/recruiting") isCurrentUser = true;

  if (mockExternalUser) {
    isCurrentUser = false;
  }

  return (
    <>
      {user.overwatch && Object.keys(user.overwatch).length > 0 ? (
        <OverwatchStats
          isCurrentUser={isCurrentUser}
          onClick={() => onUnlink(user?.overwatch?.platform)}
          styleClass={Style.unlink}
          userStats={user.overwatch}
        />
      ) : (
        <>
          {isCurrentUser && !waitingStats && (
            <AddButton desc="Add Overwatch Account" icon={faPlus} onClick={() => toggleModal(true)} />
          )}
          {waitingStats && (
            <div className={Style.waitingStatsRoot}>
              Account has been linked, the stats will be added shortly.
              {isCurrentUser && (
                <FontAwesomeIcon
                  icon={faUnlink}
                  title="Unlink Overwatch"
                  className={Style.unlink}
                  onClick={() => onUnlink(user?.overwatch?.platform)}
                />
              )}
            </div>
          )}

          <AddOverwatch
            show={isOpen}
            onClick={() => toggleModal(false)}
            setWaitingStats={setWaitingStats}
            onHide={() => analytics.track(OVERWATCH_CONNECTION_EVENTS.VERIFICATION_MODAL_CLOSE) && toggleModal(false)}
            onOpen={() => analytics.track(OVERWATCH_CONNECTION_EVENTS.VERIFICATION_MODAL_OPEN)}
          />
        </>
      )}
    </>
  );
};

const mapStateToProps = state => ({
  user: state.user.currentUser,
  modal: state.portfolio.modal,
  modalSection: state.portfolio.modalSection,
  currentUser: state.auth.currentUser,
  mockExternalUser: state.portfolio.mockExternalUser
});

export default connect(mapStateToProps, {
  togglePortfolioModal: _togglePortfolioModal,
  updateUserSimple: _updateUserSimple,
  unlinkGameAccount: _unlinkGameAccount
})(withRouter(Index));
