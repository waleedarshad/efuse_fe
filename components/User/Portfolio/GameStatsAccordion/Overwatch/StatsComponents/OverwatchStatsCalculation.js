import isEmpty from "lodash/isEmpty";
import { withCdn } from "../../../../../../common/utils";

const getCompetitiveStats = (modes = []) => {
  let competitiveMode;
  modes.forEach(mode => {
    if (mode.realm === "competitive") {
      competitiveMode = mode;
    }
  });
  return competitiveMode.stats;
};

const statsCalculation = ({ userStats }) => {
  if (isEmpty(userStats?.stats)) {
    return null;
  }

  const { stats, nickname } = userStats;
  const modes = stats.segments;
  const platformName = stats?.platformInfo?.platformSlug;
  const competitiveModeStats = getCompetitiveStats(modes);

  let totalWins = 0;
  let winsPercentile = 0;
  let totalLosses = 0;
  let KD = "";
  let KDPercentile = "";
  let DPG = "";
  let EPG = "";
  let ELEMS = "";
  let ELEMSPercentile = 0;
  let matchesPlayed = 0;
  let matchesPercentile = 0;
  let timePlayed = 0;
  let timePlayedPercentile = 0;
  const platform = platformName;
  let hasRankedGames = false;
  const userName = nickname;

  if (competitiveModeStats) {
    hasRankedGames = true;
    const losses =
      parseInt(competitiveModeStats.matchesPlayed.displayValue, 10) -
      parseInt(competitiveModeStats.wins.displayValue, 10);
    const wins = parseInt(competitiveModeStats.wins.displayValue, 10);
    const eliminations = parseInt(competitiveModeStats.eliminations.displayValue, 10);
    const matches = parseInt(competitiveModeStats.matchesPlayed.displayValue, 10);
    matchesPlayed = matches;
    const eliminationsPerGame = (eliminations / matchesPlayed).toFixed(2);
    const damages = parseInt(competitiveModeStats.damageDone.displayValue, 10);
    const damagePerGame = (damages / matchesPlayed).toFixed(2);

    timePlayed = competitiveModeStats.timePlayed.displayValue;
    timePlayedPercentile = competitiveModeStats.timePlayed.percentile;
    totalWins = wins;
    winsPercentile = competitiveModeStats.wins.percentile;
    totalLosses = losses;
    KD = competitiveModeStats.kd.displayValue;
    KDPercentile = competitiveModeStats.kd.percentile;

    matchesPercentile = competitiveModeStats.matchesPlayed.percentile;
    DPG = damagePerGame;

    EPG = eliminationsPerGame;
    ELEMSPercentile = competitiveModeStats.eliminations.percentile;
    ELEMS = eliminations;
    totalWins = wins;
    totalLosses = losses;
  }

  const getPlatformIcon = () => {
    switch (platform) {
      case "psn":
        return withCdn("/static/images/black_playstation.png");
      case "battlenet":
        return withCdn("/static/images/black_battlenet.png");
      case "xbox":
        return withCdn("/static/images/black_xbox.png");
      default:
        return "";
    }
  };
  const winPercent = () => {
    return ((totalWins / (totalWins + totalLosses)) * 100).toPrecision(4);
  };

  return {
    totalWins,
    winsPercentile,
    totalLosses,
    KD,
    KDPercentile,
    DPG,
    EPG,
    ELEMS,
    ELEMSPercentile,
    matchesPlayed,
    matchesPercentile,
    timePlayed,
    timePlayedPercentile,
    platform,
    hasRankedGames,
    userName,
    winPercent,
    getPlatformIcon
  };
};

export default statsCalculation;
