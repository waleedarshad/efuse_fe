import React from "react";
import OverwatchStatsComponent from "./OverwatchStats";

export default {
  title: "Stats/OverwatchStats",
  component: OverwatchStatsComponent,
  argTypes: {
    stats: {
      control: {
        type: "object"
      }
    }
  }
};

// eslint-disable-next-line react/jsx-props-no-spreading
const Story = args => <OverwatchStatsComponent {...args} />;

export const OverwatchStats = Story.bind({});
OverwatchStats.args = {
  userStats: {
    nickname: "Tobirama#11278",
    platform: "battlenet",
    stats: {
      _id: "60080851ac1b2c8dca7a3cc7",
      platformInfo: {
        platformSlug: "battlenet",
        platformUserId: "Tobirama#11278",
        platformUserHandle: "Tobirama#11278",
        platformUserIdentifier: "Tobirama#11278",
        avatarUrl: null,
        additionalParameters: null
      },
      userInfo: {
        userId: null,
        isPremium: false,
        isVerified: false,
        isInfluencer: false,
        countryCode: null,
        customAvatarUrl: null,
        customHeroUrl: null,
        socialAccounts: null,
        pageviews: null,
        isSuspicious: null
      },
      segments: [
        {
          stats: {
            timePlayed: {
              displayValue: "1d 07h 48m",
              percentile: null
            },
            wins: {
              displayValue: "144",
              percentile: null
            },
            matchesPlayed: {
              displayValue: "0",
              percentile: null
            },
            timeSpentOnFire: {
              displayValue: "4h 06m 52s",
              percentile: null
            },
            cards: {
              displayValue: "86",
              percentile: null
            },
            wlPercentage: {
              displayValue: "144.0%",
              percentile: null
            },
            medals: {
              displayValue: "687",
              percentile: null
            },
            goldMedals: {
              displayValue: "271",
              percentile: null
            },
            silverMedals: {
              displayValue: "205",
              percentile: null
            },
            bronzeMedals: {
              displayValue: "211",
              percentile: null
            },
            multiKills: {
              displayValue: "22",
              percentile: null
            },
            soloKills: {
              displayValue: "650",
              percentile: null
            },
            objectiveKills: {
              displayValue: "1,160",
              percentile: null
            },
            environmentalKills: {
              displayValue: "17",
              percentile: null
            },
            finalBlows: {
              displayValue: "2,208",
              percentile: null
            },
            damageDone: {
              displayValue: "742,475",
              percentile: null
            },
            healingDone: {
              displayValue: "198,402",
              percentile: null
            },
            eliminations: {
              displayValue: "3,969",
              percentile: null
            },
            deaths: {
              displayValue: "1,470",
              percentile: null
            },
            kd: {
              displayValue: "2.70",
              percentile: null
            },
            kg: {
              displayValue: "3,969.00",
              percentile: null
            },
            objectiveTime: {
              displayValue: "2h 16m 10s",
              percentile: null
            },
            defensiveAssists: {
              displayValue: "294",
              percentile: null
            },
            offensiveAssists: {
              displayValue: "353",
              percentile: null
            },
            mostEliminations: {
              displayValue: "50",
              percentile: null
            },
            mostFinalBlows: {
              displayValue: "33",
              percentile: null
            },
            mostDamageDone: {
              displayValue: "19,689",
              percentile: null
            },
            mostHealingDone: {
              displayValue: "9,316",
              percentile: null
            },
            mostDefensiveAssists: {
              displayValue: "22",
              percentile: null
            },
            mostOffensiveAssists: {
              displayValue: "21",
              percentile: null
            },
            mostObjectiveKills: {
              displayValue: "20",
              percentile: null
            },
            mostObjectiveTime: {
              displayValue: "03m 22s",
              percentile: null
            },
            mostSoloKills: {
              displayValue: "33",
              percentile: null
            },
            mostTimeSpentOnFire: {
              displayValue: "10m 19s",
              percentile: null
            }
          },
          _id: "602a5432554b7cfe9b74306e",
          type: "casual",
          expiryDate: "2021-02-15T11:01:08.772Z"
        },
        {
          stats: {
            timePlayed: {
              displayValue: "1h 18m 44s",
              percentile: 12
            },
            wins: {
              displayValue: "5",
              percentile: 11
            },
            matchesPlayed: {
              displayValue: "8",
              percentile: 26
            },
            timeSpentOnFire: {
              displayValue: "04m 03s",
              percentile: 8
            },
            cards: {
              displayValue: "3",
              percentile: 4
            },
            wlPercentage: {
              displayValue: "62.5%",
              percentile: 45
            },
            medals: {
              displayValue: "25",
              percentile: 15
            },
            goldMedals: {
              displayValue: "12",
              percentile: 17
            },
            silverMedals: {
              displayValue: "6",
              percentile: 10
            },
            bronzeMedals: {
              displayValue: "7",
              percentile: 12
            },
            multiKills: {
              displayValue: "4",
              percentile: 8
            },
            soloKills: {
              displayValue: "16",
              percentile: 15
            },
            objectiveKills: {
              displayValue: "65",
              percentile: 13
            },
            environmentalKills: {
              displayValue: "0",
              percentile: null
            },
            finalBlows: {
              displayValue: "93",
              percentile: 18
            },
            damageDone: {
              displayValue: "62,436",
              percentile: 16
            },
            healingDone: {
              displayValue: "2,163",
              percentile: 4
            },
            eliminations: {
              displayValue: "187",
              percentile: 16
            },
            deaths: {
              displayValue: "59",
              percentile: 13
            },
            kd: {
              displayValue: "3.17",
              percentile: 89
            },
            kg: {
              displayValue: "23.38",
              percentile: 33
            },
            objectiveTime: {
              displayValue: "05m 50s",
              percentile: 8
            },
            defensiveAssists: {
              displayValue: "2",
              percentile: 0
            },
            offensiveAssists: {
              displayValue: "5",
              percentile: 2
            },
            mostEliminations: {
              displayValue: "29",
              percentile: 11
            },
            mostFinalBlows: {
              displayValue: "16",
              percentile: 19
            },
            mostDamageDone: {
              displayValue: "20,840",
              percentile: 40
            },
            mostHealingDone: {
              displayValue: "725",
              percentile: 2
            },
            mostDefensiveAssists: {
              displayValue: "2",
              percentile: 0
            },
            mostOffensiveAssists: {
              displayValue: "4",
              percentile: 2
            },
            mostObjectiveKills: {
              displayValue: "17",
              percentile: 16
            },
            mostObjectiveTime: {
              displayValue: "01m 27s",
              percentile: 4
            },
            mostSoloKills: {
              displayValue: "16",
              percentile: 19
            },
            mostTimeSpentOnFire: {
              displayValue: "56s",
              percentile: 3
            }
          },
          _id: "602a5432554b7c45d574306f",
          type: "competitive",
          expiryDate: "2021-02-15T11:01:08.772Z"
        }
      ],
      expiryDate: "2021-02-15T11:01:08.7720044+00:00",
      createdAt: "2021-01-20T10:39:13.576Z",
      updatedAt: "2021-02-15T11:00:02.639Z",
      __v: 495
    }
  }
};
