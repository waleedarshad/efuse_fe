import { faUnlink } from "@fortawesome/pro-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import React from "react";
import { withCdn } from "../../../../../../common/utils";
import Style from "./OverwatchStats.module.scss";
import EFImage from "../../../../../EFImage/EFImage";

import GameCard from "../../../../../GameStats/GameCard/GameCard";

import HorizontalScroll from "../../../../../HorizontalScroll/HorizontalScroll";

import GameHeader from "../../../../../GameStats/GameHeader/GameHeader";
import statsCalculation from "./OverwatchStatsCalculation";

const OverwatchStats = ({ userStats, isCurrentUser, onClick, styleClass }) => {
  const stats = statsCalculation({ userStats });
  const {
    totalWins,
    winsPercentile,
    totalLosses,
    KD,
    KDPercentile,
    DPG,
    EPG,
    ELEMS,
    ELEMSPercentile,
    matchesPlayed,
    matchesPercentile,
    timePlayed,
    timePlayedPercentile,
    platform,
    hasRankedGames,
    userName,
    winPercent,
    getPlatformIcon
  } = stats;
  return (
    <>
      {isCurrentUser && (
        <FontAwesomeIcon icon={faUnlink} title="Unlink Overwatch" onClick={onClick} className={styleClass} />
      )}
      <div className={Style.rootContainer}>
        <HorizontalScroll>
          <div className={Style.statDetails}>
            <div className={Style.gameImage}>
              <EFImage src={withCdn("/static/images/Overwatch.jpg")} height="170px" width="128px" />
            </div>
            <div className={Style.statCard}>
              <GameCard
                headerText="Competitive"
                cardBodyStats={{ unit: "KD", value: KD, rank: `Top ${KDPercentile}%` }}
                cardFooterStats={[
                  {
                    unit: "EPG",
                    value: EPG
                  },
                  {
                    unit: "DPG",
                    value: DPG
                  },
                  {
                    unit: "ELEMS",
                    value: ELEMS,
                    rank: ELEMSPercentile
                  }
                ]}
                structure="individualStats"
                className={Style.individual}
              />
            </div>
            <div className={Style.statCard}>
              <GameCard
                headerText="Casual"
                cardBodyStats={{ unit: "TP", value: timePlayed, rank: `Top ${timePlayedPercentile}%` }}
                cardFooterStats={[
                  {
                    unit1: "W",
                    value1: totalWins,
                    rank: winsPercentile,
                    unit2: "L",
                    value2: totalLosses
                  },
                  {
                    unit1: "GP",
                    value1: matchesPlayed,
                    rank: matchesPercentile,
                    unit2: "%",
                    value2: winPercent(totalWins, totalLosses)
                  }
                ]}
                structure="GroupStats"
                className={Style.individual}
              />
            </div>
          </div>
        </HorizontalScroll>
      </div>
    </>
  );
};

export default OverwatchStats;
