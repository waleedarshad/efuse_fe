import React, { useState } from "react";
import { Form, Col } from "react-bootstrap";
import axios from "axios";

import { connect } from "react-redux";
import Style from "./AddOverwatch.module.scss";

import platforms from "../data/platforms.json";

import InputField from "../../../../../InputField/InputField";
import InputLabel from "../../../../../InputLabel/InputLabel";
import SelectBox from "../../../../../SelectBox/SelectBox";

import { dispatchNotification } from "../../../../../../store/actions/notificationActions";
import { togglePortfolioModal } from "../../../../../../store/actions/portfolioActions";
import EFRectangleButton from "../../../../../Buttons/EFRectangleButton/EFRectangleButton";
import { sendNotification } from "../../../../../../helpers/FlashHelper";
import { OVERWATCH_CONNECTION_EVENTS } from "../../../../../../common/analyticEvents";
import { getCurrentUser } from "../../../../../../store/actions/common/userAuthActions";
import EFPrimaryModal from "../../../../../Modals/EFPrimaryModal/EFPrimaryModal";

const AddOverwatch = props => {
  const [accountName, setAccountName] = useState("");
  const [platform, setPlatform] = useState("battlenet");

  const onChange = event => {
    if (event.target.name === "accountName") {
      setAccountName(event.target.value);
    } else {
      setPlatform(event.target.value);
    }
  };

  const onSubmit = event => {
    event.preventDefault();
    analytics.track(OVERWATCH_CONNECTION_EVENTS.VERIFICATION_SUBMIT, {
      accountName,
      platform
    });
    let userAccount = accountName;
    if (platform === "battlenet") {
      userAccount = encodeURIComponent(userAccount);
    }
    axios
      .post(`/overwatch/stats/${platform}/${userAccount}`)
      .then(response => {
        if (response.data && response.data.stats) {
          props.setWaitingStats(true);
          props.onClick();
          props.getCurrentUser();
          sendNotification("Account has been linked, the stats will be added shortly..", "success", "Overwatch");
          analytics.track(OVERWATCH_CONNECTION_EVENTS.VERIFICATION_SUCCESS, {
            accountName,
            platform
          });
        }
      })
      .catch(error => {
        analytics.track(OVERWATCH_CONNECTION_EVENTS.VERIFICATION_FAIL, {
          error
        });
        sendNotification(
          "Could not be verified, please verify your Overwatch name and Platform are correct and try again.",
          "danger",
          "Overwatch"
        );
        // eslint-disable-next-line no-console
        console.error(error);
      });
  };

  const { show, onHide, onOpen } = props;

  return (
    <EFPrimaryModal
      title="Add Overwatch Account"
      isOpen={show}
      allowBackgroundClickClose={false}
      displayCloseButton
      onClose={onHide}
      onOpen={onOpen}
    >
      <Form noValidate onSubmit={onSubmit} className={Style.form}>
        <p className={Style.titleDesc}>
          Enter your overwatch&apos;s name and platfom below, then press the &quot;Add Account&quot; button to proceed
          with linking your account.
        </p>
        <Col>
          <InputLabel theme="darkColor">Overwatch Account Name</InputLabel>
          <InputField
            name="accountName"
            placeholder="Overwatch Account Name"
            theme="whiteShadow"
            size="lg"
            onChange={onChange}
            value={accountName}
            required
            errorMessage="overwatch account name is required"
            maxLength={30}
          />
        </Col>
        <br />
        <Col>
          <InputLabel theme="darkColor">Platform</InputLabel>
          <SelectBox
            name="platform"
            // validated={validated}
            options={platforms}
            size="lg"
            theme="whiteShadow"
            onChange={onChange}
            value={platform}
            required
            errorMessage="Region is required"
          />
        </Col>
        <br />
        <div className={Style.buttons}>
          <EFRectangleButton
            buttonType="button"
            text="cancel"
            width="autoWidth"
            disabled={false}
            colorTheme="light"
            shadowTheme="small"
            onClick={props.onClick}
          />

          <EFRectangleButton
            buttonType="submit"
            text="Add Account!"
            width="autoWidth"
            disabled={false}
            colorTheme="secondary"
            shadowTheme="small"
            onClick={onSubmit}
          />
        </div>
      </Form>
    </EFPrimaryModal>
  );
};

const mapStateToProps = state => ({
  inProgress: state.portfolio.inProgress
});

export default connect(mapStateToProps, {
  dispatchNotification,
  togglePortfolioModal,
  getCurrentUser
})(AddOverwatch);
