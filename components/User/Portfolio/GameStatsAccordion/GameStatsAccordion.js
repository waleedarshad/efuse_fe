import isEmpty from "lodash/isEmpty";
import React, { useEffect, useState } from "react";
import { connect, useDispatch } from "react-redux";
import {
  getAimlabAuthStatusForPortfolioUser,
  getValorantAuthStatusForPortfolioUser
} from "../../../../store/actions/externalAuth/externalAuthActions";
import { trackStats } from "../../../../store/actions/userActions";
import Accordion from "../../../Accordion/Accordion";
import accordionTab from "./AccordionTab/AccordionTab";
import useAimLabStats from "./AimLab/useAimLabStats";
import Style from "./GameStatsAccordion.module.scss";
import lolCalculation from "./LeagueOfLegends/StatsComponents/LeagueOfLegendsStatsCalculation";
import overwatchCalculation from "./Overwatch/StatsComponents/OverwatchStatsCalculation";
import useValorantStats from "./Valorant/useValorantStats/useValorantStats";

const GameStatsAccordion = ({
  user,
  currentUser,
  gamesBulletTrain,
  mockExternalUser,
  gamesToDisplay,
  openAllOnMount,
  showNoStatsMessage
}) => {
  const overwatchStats = overwatchCalculation({ userStats: user.overwatch });
  const lolStats = lolCalculation({ userStats: user.leagueOfLegends });
  const aimLabStats = useAimLabStats(user._id);
  const valorantStats = useValorantStats(user._id);

  const [tabs, setTab] = useState(accordionTab(overwatchStats, lolStats, valorantStats, aimLabStats));

  const overwatchStatsJson = JSON.stringify(overwatchStats);
  const lolStatsJson = JSON.stringify(lolStats);
  const valorantStatsJson = JSON.stringify(valorantStats);
  const aimLabStatsJson = JSON.stringify(aimLabStats);

  const dispatch = useDispatch();

  useEffect(() => {
    setTab(accordionTab(overwatchStats, lolStats, valorantStats, aimLabStats));
  }, [`${overwatchStatsJson} ${lolStatsJson} ${valorantStatsJson} ${aimLabStatsJson}`]);

  useEffect(() => {
    dispatch(trackStats());
    dispatch(getValorantAuthStatusForPortfolioUser());
    dispatch(getAimlabAuthStatusForPortfolioUser());
  }, []);

  let isPortfolioOfLoggedInUser = currentUser && user._id === (currentUser._id || currentUser.id);

  if (mockExternalUser) {
    isPortfolioOfLoggedInUser = false;
  }

  const shouldDisplayTab = tab => {
    if (!gamesBulletTrain[tab.tabFeature]) {
      return false;
    }

    const isUserAuthenticatedWithGame =
      typeof user[tab.auth] === "boolean" ? user[tab.auth] : !isEmpty(user[tab.auth]?.stats);

    if (!isPortfolioOfLoggedInUser && !isUserAuthenticatedWithGame) {
      return false;
    }

    return gamesToDisplay.length === 0 ? true : gamesToDisplay.includes(tab.auth);
  };

  const tabArray = tabs.map(tab => ({
    title: tab.title,
    component: tab.component,
    display: shouldDisplayTab(tab)
  }));

  const displayCount = tabArray.filter(value => value.display).length;

  if (displayCount > 0) {
    return (
      <div className={Style.container}>
        <Accordion tabArray={tabArray} openAllOnMount={openAllOnMount} />
      </div>
    );
  }
  if (showNoStatsMessage) {
    return <div>This player does not have any linked game stats.</div>;
  }
  return null;
};

GameStatsAccordion.defaultProps = {
  gamesToDisplay: [],
  openAllOnMount: false,
  showNoStatsMessage: false
};

const mapStateToProps = state => ({
  games_section: state.features.games_section,
  user: state.user.currentUser,
  currentUser: state.auth.currentUser,
  mockExternalUser: state.portfolio.mockExternalUser,
  gamesBulletTrain: {
    leagueoflegends_section: state.features.leagueoflegends_section,
    callofduty_section: state.features.callofduty_section,
    pubg_section: state.features.pubg_section,
    overwatch_section: state.features.overwatch_section,
    aimlab_section: state.features.aimlab_section,
    valorant_section: state.features.valorant_section
  }
});

export default connect(mapStateToProps)(GameStatsAccordion);
