const pubgDictionary = {
  assists: "Assists",
  damageDealt: "Damage Dealt",
  headshotKills: "Headshot Kills",
  kills: "Kills",
  longestKill: "Longest Kill",
  roundMostKills: "Most Kills in a Round",
  top10s: "Top 10s",
  wins: "Wins",
  roundsPlayed: "Rounds Played",
  revives: "Revives"
};

export { pubgDictionary };
