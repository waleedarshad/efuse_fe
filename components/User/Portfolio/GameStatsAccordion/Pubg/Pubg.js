import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "next/router";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUnlink, faPlus } from "@fortawesome/pro-solid-svg-icons";

import { togglePortfolioModal, removePubgAccount } from "../../../../../store/actions/portfolioActions";
import { convertStatToString } from "../../../../../helpers/GeneralHelper";
import Style from "./Pubg.module.scss";
import ConfirmAlert from "../../../../ConfirmAlert/ConfirmAlert";
import AddButton from "../../AddButton/AddButton";
import AddPubg from "./AddPubg/AddPubg";
import Switch from "../../../../Switch/Switch";
import * as pubg from "./data/pubgDictionary";

class Pubg extends Component {
  state = {
    edit: false,
    platform: null,
    isChecked: true
  };

  loadModal = () => {
    this.props.togglePortfolioModal(true, "pubg");
  };

  onHide = () => {
    this.props.togglePortfolioModal(false);
    this.setState({ edit: false, platform: null });
  };

  editModal = platform => {
    this.props.togglePortfolioModal(true, "pubg");
  };

  removeAccount = platform => {
    const { user } = this.props;
    const { pubg } = user;
    if (pubg?.platforms[0]._id) {
      this.props.removePubgAccount(pubg?.platforms[0]._id);
    }
  };

  onSwitch = () => {
    this.setState({
      isChecked: !this.state.isChecked
    });
  };

  render() {
    const { user, modal, modalSection, router, currentUser } = this.props;
    const { edit, platform, isChecked } = this.state;
    const isCurrentUser = currentUser && router.query.id == currentUser.id;
    const hasStats = user?.pubg?.stats[0]?.attributes;

    let accountLinkedNoStats = false;
    let gamemodes = [];

    if (user?.pubg?.stats[0]?.attributes?.gameModeStats) {
      gamemodes = Object.entries(user?.pubg?.stats[0]?.attributes?.gameModeStats);
    } else {
      accountLinkedNoStats = user?.pubg?.platforms?.length > 0;
    }

    return (
      <div className={`${Style.componentWrapper} ${hasStats && Style.marginLeft}`}>
        {modal && modalSection === "pubg" && isCurrentUser && <AddPubg show={modal} onHide={this.onHide} edit={edit} />}
        {isCurrentUser && (hasStats || accountLinkedNoStats) && (
          <ConfirmAlert title="Unlink your Pubg Account?" message="Are you sure?" onYes={this.removeAccount}>
            <div className={Style.unlink}>
              <FontAwesomeIcon icon={faUnlink} />
            </div>
          </ConfirmAlert>
        )}
        {hasStats ? (
          <>
            <div className={Style.topBar}>
              <p className={Style.username}>{user?.pubg?.platforms[0]?.username}</p>
              <p className={Style.platform}>{user?.pubg?.platforms[0]?.platform}</p>
              <div className={Style.switch}>
                <inline>FPP</inline>
                <div className={Style.switchIcon}>
                  <Switch name="FPP" value={isChecked} checked={isChecked} onChange={this.onSwitch} />
                </div>
              </div>
            </div>
            {gamemodes.map((mode, index) => {
              // add this check to filter first person perspective
              if ((!isChecked && mode[0].includes("fpp")) || (isChecked && !mode[0].includes("fpp"))) {
                return <></>;
              }
              const stats = Object.entries(mode[1]);
              return (
                <div key={index} className={Style.bottomContainer}>
                  <div className={Style.gamemode}>
                    <h3 className={Style.mode}>{mode[0]}</h3>
                  </div>
                  <div className={Style.statsGrid}>
                    {stats.map((stat, index) => {
                      const array = [
                        "kills",
                        "assists",
                        "damageDealt",
                        "wins",
                        "top10s",
                        "longestKill",
                        "headshotKills",
                        "roundMostKills",
                        "roundsPlayed",
                        "revives"
                      ];
                      if (array.includes(stat[0])) {
                        return (
                          <div key={index} className={Style.statContainer}>
                            <p className={Style.name}>{pubg.pubgDictionary[stat[0]]}</p>
                            <p className={Style.value}>{convertStatToString(stat[1])}</p>
                          </div>
                        );
                      }
                      return <></>;
                    })}
                    <div key={index} className={Style.statContainer}>
                      <p className={Style.name}>Win %</p>
                      <p className={Style.value}>{`${convertStatToString(stats[34][1] / stats[22][1])}%`}</p>
                    </div>
                  </div>
                </div>
              );
            })}
          </>
        ) : (
          <>
            {accountLinkedNoStats ? (
              <p className={Style.paddingTop}>
                Your Pubg stats are being linked with the username <b>{user?.pubg?.platforms[0]?.username}</b>
              </p>
            ) : (
              <AddButton desc="Add Pubg Account" icon={faPlus} onClick={this.loadModal} />
            )}
          </>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user.currentUser,
  modal: state.portfolio.modal,
  modalSection: state.portfolio.modalSection,
  currentUser: state.auth.currentUser
});

export default connect(mapStateToProps, {
  togglePortfolioModal,
  removePubgAccount
})(withRouter(Pubg));
