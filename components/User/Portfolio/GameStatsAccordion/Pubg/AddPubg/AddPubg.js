import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Form, Col } from "react-bootstrap";
import InputRow from "../../../../../InputRow/InputRow";

import SelectBox from "../../../../../SelectBox/SelectBox";
import InputField from "../../../../../InputField/InputField";
import InputLabel from "../../../../../InputLabel/InputLabel";
import ActionButtonGroup from "../../../../../Layouts/Internal/ActionButtonGroup/ActionButtonGroup";
import Error from "../../../../../Error/Error";
import platforms from "../data/platforms.json";
import { addPubgAccount } from "../../../../../../store/actions/portfolioActions";
import EFPrimaryModal from "../../../../../Modals/EFPrimaryModal/EFPrimaryModal";

const AddPubg = ({ edit, pubgInfo, show, onHide }) => {
  const dispatch = useDispatch();

  const inProgress = useSelector(state => state.portfolio.inProgress);

  const [validated, setValidated] = useState(false);
  const [pubgData, setPubgData] = useState({
    platform: "steam",
    username: "",
    _id: ""
  });

  useEffect(() => {
    if (edit) {
      setPubgData({
        ...pubgInfo
      });
    }
  }, []);

  const onChange = e => {
    setPubgData({
      ...pubgData,
      [e.target.name]: e.target.value
    });
  };

  const onSubmit = event => {
    event.preventDefault();

    if (!validated) {
      setValidated(true);
    }
    if (event.currentTarget.checkValidity()) {
      dispatch(addPubgAccount(pubgData));
    }
  };

  const { username, platform } = pubgData;
  return (
    <EFPrimaryModal
      title="Add Pubg Account"
      isOpen={show}
      onClose={onHide}
      widthTheme="medium"
      allowBackgroundClickClose={false}
    >
      <Error />
      <Form noValidate validated={validated} onSubmit={onSubmit}>
        <InputRow>
          <Col sm={6}>
            <InputLabel theme="darkColor">Platform</InputLabel>
            <SelectBox
              name="platform"
              validated={validated}
              options={platforms}
              size="lg"
              theme="whiteShadow"
              onChange={onChange}
              value={platform}
              required
              errorMessage="Platform is required"
            />
          </Col>
          <Col sm={6}>
            <InputLabel theme="darkColor">Username</InputLabel>
            <InputField
              name="username"
              placeholder="Username"
              theme="whiteShadow"
              size="lg"
              onChange={onChange}
              value={username}
              required
              errorMessage="Username is required"
              maxLength={40}
            />
          </Col>
        </InputRow>
        <ActionButtonGroup onCancel={onHide} disabled={inProgress} />
      </Form>
    </EFPrimaryModal>
  );
};

export default AddPubg;
