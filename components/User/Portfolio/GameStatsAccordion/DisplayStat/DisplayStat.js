import React, { Component } from "react";
import ProgressBar from "react-bootstrap/ProgressBar";
import Style from "./DisplayStat.module.scss";

export class DisplayStat extends Component {
  render() {
    const { large, light, progressBar, title, value, percent, index, type } = this.props;
    return (
      <div className={Style.statContainer} key={index || 0}>
        <h4 className={`${Style.title} ${light && Style.lightTitle} ${large && Style.largeTitle}`}>{title}</h4>
        <p className={`${Style.value} ${light && Style.lightValue} ${large && Style.largeValue}`}>{value}</p>
        {progressBar && <ProgressBar variant={type} className={Style.progressBar} now={percent || 0.0} />}
      </div>
    );
  }
}

export default DisplayStat;
