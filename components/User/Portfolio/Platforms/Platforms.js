import React, { Component } from "react";
import { connect } from "react-redux";
import { faXbox, faPlaystation } from "@fortawesome/free-brands-svg-icons";

import PlatformCard from "../../../Cards/PlatformCard/PlatformCard";
import Style from "./Platforms.module.scss";
import HorizontalScroll from "../../../HorizontalScroll/HorizontalScroll";
import AddPlatformModal from "../../../GlobalModals/AddPlatformModal/AddPlatformModal";
import ConfirmAlert from "../../../ConfirmAlert/ConfirmAlert";
import { removePlatform } from "../../../../store/actions/userActions";
import { withCdn } from "../../../../common/utils";
import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";
class Platforms extends Component {
  getDisplayName = (platform, list) => {
    let returnPlatform = {};
    list?.length &&
      list.map((item, index) => {
        if (item?.platform === platform && item?.displayName) {
          returnPlatform = {
            displayName: item.displayName,
            id: item?._id
          };
        }
      });
    return returnPlatform;
  };

  render() {
    const { user, currentUser, mockExternalUser } = this.props;
    let isOwner = currentUser && user._id === currentUser._id;
    if (mockExternalUser) {
      isOwner = false;
    }

    const xboxPlatform = this.getDisplayName("xbl", user?.gamePlatforms);
    const playstationPlatform = this.getDisplayName("psn", user?.gamePlatforms);
    const battlenetPlatform = this.getDisplayName("battlenet", user?.gamePlatforms);
    const linkButton = (
      <div className={Style.marginTop}>
        <EFRectangleButton colorTheme="primary" text="LINK" buttonType="button" />
      </div>
    );
    const unlinkButton = (
      <div className={Style.marginTop}>
        <EFRectangleButton colorTheme="primary" text="UNLINK" buttonType="button" />
      </div>
    );

    return (
      <HorizontalScroll>
        <div className={Style.platformsContainer}>
          {(xboxPlatform?.displayName || isOwner) && (
            <PlatformCard
              cardIcon={faXbox}
              platform="xbox"
              firstLabel="Platform"
              firstValue="Xbox"
              secondLabel="Username"
              secondValue={xboxPlatform?.displayName}
              displayButton={isOwner}
              buttonComponent={
                xboxPlatform?.displayName ? (
                  <ConfirmAlert
                    title="Delete this platform?"
                    message="Are you sure?"
                    onYes={() => this.props.removePlatform(user?.gamePlatforms, xboxPlatform?.id)}
                  >
                    {unlinkButton}
                  </ConfirmAlert>
                ) : (
                  <AddPlatformModal platform="xbox">{linkButton}</AddPlatformModal>
                )
              }
            />
          )}
          {(playstationPlatform?.displayName || isOwner) && (
            <PlatformCard
              cardIcon={faPlaystation}
              platform="playstation"
              firstLabel="Platform"
              firstValue="Playstation"
              secondLabel="Username"
              secondValue={playstationPlatform?.displayName}
              displayButton={isOwner}
              buttonComponent={
                playstationPlatform?.displayName ? (
                  <ConfirmAlert
                    title="Delete this platform?"
                    message="Are you sure?"
                    onYes={() => this.props.removePlatform(user?.gamePlatforms, playstationPlatform?.id)}
                  >
                    {unlinkButton}
                  </ConfirmAlert>
                ) : (
                  <AddPlatformModal platform="playstation">{linkButton}</AddPlatformModal>
                )
              }
            />
          )}
          {(battlenetPlatform?.displayName || isOwner) && (
            <PlatformCard
              displayImage
              cardIcon={withCdn("/static/images/battlenet.svg")}
              platform="battle.net"
              firstLabel="Platform"
              firstValue="Battle.net"
              secondLabel="Username"
              secondValue={battlenetPlatform?.displayName}
              displayButton={isOwner}
              buttonComponent={
                battlenetPlatform?.displayName ? (
                  <ConfirmAlert
                    title="Delete this platform?"
                    message="Are you sure?"
                    onYes={() => this.props.removePlatform(user?.gamePlatforms, battlenetPlatform?.id)}
                  >
                    {unlinkButton}
                  </ConfirmAlert>
                ) : (
                  <AddPlatformModal platform="battlenet">{linkButton}</AddPlatformModal>
                )
              }
            />
          )}
        </div>
      </HorizontalScroll>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user.currentUser,
  currentUser: state.auth.currentUser,
  mockExternalUser: state.portfolio.mockExternalUser
});

export default connect(mapStateToProps, { removePlatform })(Platforms);
