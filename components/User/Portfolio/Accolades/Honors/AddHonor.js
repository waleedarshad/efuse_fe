import React, { Component } from "react";
import { connect } from "react-redux";
import { Form, Col } from "react-bootstrap";
import InputRow from "../../../../InputRow/InputRow";
import InputField from "../../../../InputField/InputField";
import InputLabel from "../../../../InputLabel/InputLabel";
import { toMoment } from "../../../../../helpers/MomentHelper";
import { validateHtml } from "../../../../../helpers/ValidationHelper";
import ActionButtonGroup from "../../../../Layouts/Internal/ActionButtonGroup/ActionButtonGroup";
import { manageData } from "../../../../../store/actions/portfolioActions";
import { updateAccolades } from "../../../../../store/actions/organizationActions";
import EFHtmlInput from "../../../../EFHtmlInput/EFHtmlInput";

class AddHonor extends Component {
  constructor(props) {
    super(props);

    this.state = {
      validated: false,
      valuesSet: false,
      portfolioHonor: {
        title: "",
        subTitle: "",
        description: "",
        _id: ""
      }
    };
  }

  static getDerivedStateFromProps(props, state) {
    return props.edit && !state.valuesSet
      ? {
          portfolioHonor: {
            ...props.portfolioHonor
          },
          valuesSet: true
        }
      : {};
  }

  onChange = event => {
    this.setState({
      portfolioHonor: {
        ...this.state.portfolioHonor,
        [event.target.name]: event.target.value
      }
    });
  };

  onDateChange = (name, date) => {
    this.setState({
      portfolioHonor: {
        ...this.state.portfolioHonor,
        [name]: toMoment(date)
      },
      [`${name}Error`]: false
    });
  };

  onEditorStateChange = editorState => {
    this.setState({
      portfolioHonor: {
        ...this.state.portfolioHonor,
        description: editorState
      }
    });
  };

  onSubmit = event => {
    event.preventDefault();
    const { validated, portfolioHonor } = this.state;
    if (!validated) {
      this.setState({ validated: true });
    }

    if (event.currentTarget.checkValidity()) {
      if (this.props.organizationId) {
        let honorData = { ...portfolioHonor };
        honorData = {
          ...honorData,
          description: validateHtml(honorData.description),
          key: "honors"
        };

        const formData = new FormData();
        Object.keys(honorData).forEach(key => formData.append(key, honorData[key]));

        this.props.updateAccolades(this.props.organizationId, formData);
      } else {
        let portfolioHonorData = { ...portfolioHonor };

        portfolioHonorData = {
          ...portfolioHonorData,
          description: validateHtml(portfolioHonorData.description)
        };

        analytics.track("PORTFOLIO_HONORS_CREATE", {
          portfolioHonor: portfolioHonorData
        });

        this.props.manageData("/portfolio/portfolio_honor", portfolioHonorData);
      }

      this.props.closeModal && this.props.closeModal();
    }
  };

  render() {
    const { inProgress } = this.props;
    const { validated, portfolioHonor } = this.state;
    const { title, subTitle, description } = portfolioHonor;
    return (
      <Form noValidate validated={validated} onSubmit={this.onSubmit}>
        <InputRow>
          <Col sm={6}>
            <InputLabel theme="darkColor">Title</InputLabel>
            <InputField
              name="title"
              placeholder="Add Title"
              theme="whiteShadow"
              size="lg"
              onChange={this.onChange}
              value={title}
              required
              errorMessage="Title is required"
              maxLength={50}
            />
          </Col>
          <Col sm={6}>
            <InputLabel theme="darkColor">Sub Title</InputLabel>
            <InputField
              name="subTitle"
              placeholder="Add Sub Title"
              theme="whiteShadow"
              size="lg"
              onChange={this.onChange}
              value={subTitle}
              required
              errorMessage="Sub title is required"
              maxLength={50}
            />
          </Col>
        </InputRow>
        <InputRow>
          <Col sm={12}>
            <InputLabel theme="darkColor">Description</InputLabel>
          </Col>
          <Col sm={12}>
            <EFHtmlInput value={description} onChange={this.onEditorStateChange} />
          </Col>
        </InputRow>
        <ActionButtonGroup disabled={inProgress} />
      </Form>
    );
  }
}
const mapStateToProps = state => ({
  inProgress: state.portfolio.inProgress
});

export default connect(mapStateToProps, {
  manageData,
  updateAccolades
})(AddHonor);
