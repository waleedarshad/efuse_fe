import React, { PureComponent } from "react";
import { faPencilAlt, faTrash } from "@fortawesome/pro-solid-svg-icons";

import Style from "../Accolades.module.scss";
import { formatDate } from "../../../../../helpers/GeneralHelper";
import ConfirmAlert from "../../../../ConfirmAlert/ConfirmAlert";
import EventsModal from "../../../../GlobalModals/EventsModal/EventsModal";
import { EFHtmlParser } from "../../../../EFHtmlParser/EFHtmlParser";
import EFCircleIconButton from "../../../../Buttons/EFCircleIconButton/EFCircleIconButton";

class DisplayEvent extends PureComponent {
  render() {
    const { event, displayHr, onEdit, onRemove, owner, organizationId } = this.props;
    const { title, subTitle, eventDate, description } = event;
    const formattedDate = formatDate(eventDate);
    return (
      <div className={Style.container}>
        <div className={Style.socialContainer}>
          <div className={Style.textContainer}>
            <h4 className={Style.socialTitle}>
              {title}
              &nbsp;
              {owner && (
                <EventsModal portfolioEvent={event} edit organizationId={organizationId}>
                  <EFCircleIconButton colorTheme="transparent" shadowTheme="none" icon={faPencilAlt} onClick={onEdit} />
                </EventsModal>
              )}
              {owner && (
                <ConfirmAlert
                  title="This action can't be undone"
                  message="Are you sure you want to remove?"
                  onYes={onRemove}
                >
                  <EFCircleIconButton colorTheme="transparent" shadowTheme="none" icon={faTrash} onClick={onRemove} />
                </ConfirmAlert>
              )}
            </h4>

            <p className={Style.socialName}>{subTitle}</p>
            <p className={Style.socialTime}>{formattedDate}</p>
            <div className={Style.socialDescription}>
              <EFHtmlParser>{description}</EFHtmlParser>
            </div>
          </div>
        </div>
        {displayHr && <hr className={Style.divider} />}
      </div>
    );
  }
}

export default DisplayEvent;
