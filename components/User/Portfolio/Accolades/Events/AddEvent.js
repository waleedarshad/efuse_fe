import React, { Component } from "react";
import { connect } from "react-redux";
import { Form, Col, FormControl } from "react-bootstrap";
import InputRow from "../../../../InputRow/InputRow";
import InputField from "../../../../InputField/InputField";
import InputLabel from "../../../../InputLabel/InputLabel";
import DatePickerInput from "../../../../DatePickerInput/DatePickerInput";
import DatepickerMonthElement from "../../../../DatepickerMonthElement/DatepickerMonthElement";
import { toMoment } from "../../../../../helpers/MomentHelper";
import { validateHtml } from "../../../../../helpers/ValidationHelper";
import ActionButtonGroup from "../../../../Layouts/Internal/ActionButtonGroup/ActionButtonGroup";
import { manageData } from "../../../../../store/actions/portfolioActions";
import { updateAccolades } from "../../../../../store/actions/organizationActions";
import EFHtmlInput from "../../../../EFHtmlInput/EFHtmlInput";

class AddEvent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      validated: false,
      valuesSet: false,
      portfolioEvent: {
        title: "",
        subTitle: "",
        eventDate: null,
        description: "",
        _id: ""
      },
      eventDateError: false
    };
  }

  static getDerivedStateFromProps(props, state) {
    return props.edit && !state.valuesSet
      ? {
          portfolioEvent: {
            ...props.portfolioEvent
          },
          valuesSet: true
        }
      : {};
  }

  onChange = event => {
    this.setState({
      portfolioEvent: {
        ...this.state.portfolioEvent,
        [event.target.name]: event.target.value
      }
    });
  };

  onDateChange = (name, date) => {
    this.setState({
      portfolioEvent: {
        ...this.state.portfolioEvent,
        [name]: toMoment(date)
      },
      [`${name}Error`]: false
    });
  };

  onEditorStateChange = editorState => {
    this.setState({
      portfolioEvent: {
        ...this.state.portfolioEvent,
        description: editorState
      }
    });
  };

  onSubmit = event => {
    event.preventDefault();
    const { validated, portfolioEvent } = this.state;
    const { eventDate } = portfolioEvent;
    if (!validated) {
      this.setState({ validated: true });
    }
    if (!eventDate) {
      this.setState({ eventDateError: true });
    } else if (event.currentTarget.checkValidity() && eventDate) {
      if (this.props.organizationId) {
        let eventData = { ...portfolioEvent };
        const formData = new FormData();
        eventData = {
          ...eventData,
          description: validateHtml(eventData.description),
          key: "events"
        };
        Object.keys(eventData).forEach(key => formData.append(key, eventData[key]));
        this.props.updateAccolades(this.props.organizationId, formData);
      } else {
        let portfolioEventData = { ...portfolioEvent };
        portfolioEventData = {
          ...portfolioEventData,
          description: validateHtml(portfolioEventData.description)
        };
        analytics.track("PORTFOLIO_EVENTS_CREATE", {
          portfolioEvent: portfolioEventData
        });

        this.props.manageData("/portfolio/portfolio_event", portfolioEventData);
      }

      this.props.closeModal && this.props.closeModal();
    }
  };

  render() {
    const { inProgress } = this.props;
    const { validated, portfolioEvent, eventDateError } = this.state;
    const { title, subTitle, eventDate, description } = portfolioEvent;
    return (
      <Form noValidate validated={validated} onSubmit={this.onSubmit}>
        <InputRow>
          <Col sm={6}>
            <InputLabel theme="darkColor">Title</InputLabel>
            <InputField
              name="title"
              placeholder="Add Title"
              theme="whiteShadow"
              size="lg"
              onChange={this.onChange}
              value={title}
              required
              errorMessage="Title is required"
              maxLength={50}
            />
          </Col>
          <Col sm={6}>
            <InputLabel theme="darkColor">Sub Title</InputLabel>
            <InputField
              name="subTitle"
              placeholder="Add Sub Title"
              theme="whiteShadow"
              size="lg"
              onChange={this.onChange}
              value={subTitle}
              required
              errorMessage="Sub title is required"
              maxLength={50}
            />
          </Col>
        </InputRow>

        <InputRow>
          <Col sm={6}>
            <InputLabel theme="darkColor">Event Date</InputLabel>
            <DatePickerInput
              date={eventDate}
              dateHandler={date => this.onDateChange("eventDate", date)}
              isOutsideRange={() => false}
              renderMonthElement={calenderProps => <DatepickerMonthElement {...calenderProps} includeCurrentYear />}
              numberOfMonths={1}
              direction="down"
            />
            {eventDateError && (
              <FormControl.Feedback type="invalid" style={{ display: "block" }}>
                Event Date is required
              </FormControl.Feedback>
            )}
          </Col>
        </InputRow>
        <InputRow>
          <Col sm={12}>
            <InputLabel theme="darkColor">Description</InputLabel>
          </Col>
          <Col sm={12}>
            <EFHtmlInput value={description} onChange={this.onEditorStateChange} />
          </Col>
        </InputRow>
        <ActionButtonGroup disabled={inProgress} />
      </Form>
    );
  }
}
const mapStateToProps = state => ({
  inProgress: state.portfolio.inProgress
});

export default connect(mapStateToProps, {
  manageData,
  updateAccolades
})(AddEvent);
