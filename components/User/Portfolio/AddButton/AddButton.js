import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";

import EFRectangleButton from "../../../Buttons/EFRectangleButton/EFRectangleButton";
import Style from "./AddButton.module.scss";

const AddButton = ({ desc, icon, onClick, image }) => {
  return (
    <div className={Style.addButtonContainer}>
      <EFRectangleButton
        className={Style.addButton}
        colorTheme="light"
        onClick={onClick}
        width="fullWidth"
        alignContent="left"
        size="extraLarge"
        text={
          <>
            {icon && <FontAwesomeIcon className={Style.icon} icon={icon} />}
            {desc}
            {image && <img alt="button_img" className={Style.image} src={image} />}
          </>
        }
      />
    </div>
  );
};

export default AddButton;
