import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowsAlt } from "@fortawesome/pro-solid-svg-icons";
import Style from "./PortfolioDragComponent.module.scss";

const DragComponent = props => {
  const { isOwner, portfolio_drag_buttons, provided, snapshot, innerComponent } = props;
  return (
    <div ref={provided.innerRef} {...provided.draggableProps}>
      {isOwner && portfolio_drag_buttons && (
        <div
          className={`${Style.dragButton} ${snapshot.isDragging && Style.dragButtonDragging}`}
          {...provided.dragHandleProps}
        >
          <FontAwesomeIcon icon={faArrowsAlt} />
        </div>
      )}
      <div className={Style.dragItemContainer}>
        <div className={snapshot.isDragging && Style.dragBackground} />
        {innerComponent}
      </div>
    </div>
  );
};

export default DragComponent;
