import portfolioTabsLayout from "../../../hoc/portfolioTabsLayout/portfolioTabsLayout";
import YoutubePlayer from "./Youtube/YoutubePlayer";
import TwitchPlayer from "./Twitch/TwitchPlayer";

export default portfolioTabsLayout({
  feature: "stream_section",
  id: "stream_section",
  tabs: [
    {
      title: "Highlight Reel",
      component: YoutubePlayer,
      tabFeature: "youtube_section",
      auth: "embeddedLink"
    },
    {
      title: "Twitch Stream",
      component: TwitchPlayer,
      tabFeature: "twitch_section",
      auth: "twitchVerified"
    }
  ]
});
