import React, { Component } from "react";
import { connect } from "react-redux";
import Style from "./TwitchPlayer.module.scss";
import VideoPlayer from "../../../../VideoPlayer/VideoPlayer";
import Auth from "../Auth/Auth";

class TwitchPlayer extends Component {
  renderPlayer = twitchUserName => {
    return (
      <div className={Style.videoContainer}>
        <div className={Style.videoPlayer}>
          <VideoPlayer url={`https://www.twitch.tv/${twitchUserName}`} height="300px" />
        </div>
      </div>
    );
  };

  render() {
    const { user } = this.props;
    const { twitchVerified, twitchUserName } = user;
    return (
      <>
        {twitchVerified && twitchUserName ? (
          this.renderPlayer(twitchUserName)
        ) : (
          <Auth user={user} button="twitch" isVerified={twitchVerified} />
        )}
      </>
    );
  }
}
const mapStateToProps = state => ({
  user: state.user.currentUser,
  isWindowView: state.messages.isWindowView
});
export default connect(mapStateToProps, {})(TwitchPlayer);
