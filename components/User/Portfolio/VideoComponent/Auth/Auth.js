import React, { Component } from "react";

import { connect } from "react-redux";
import cloneDeep from "lodash/cloneDeep";
import intersection from "lodash/intersection";
import getConfig from "next/config";
import { faLink } from "@fortawesome/pro-solid-svg-icons";

import { updateUserSimple } from "../../../../../store/actions/userActions";
import { dispatchNotification } from "../../../../../store/actions/notificationActions";
import WindowPortal from "../../../../WindowPortal";
import AddButton from "../../AddButton/AddButton";
import { togglePortfolioModal } from "../../../../../store/actions/portfolioActions";
import { sendNotification } from "../../../../../helpers/FlashHelper";
import { withCdn } from "../../../../../common/utils";

const { publicRuntimeConfig } = getConfig();
const SERVICES = ["twitch"];
class Auth extends Component {
  state = {
    user: {
      twitchVerified: false
    },
    authWindow: false,
    serviceURL: ""
  };

  static getDerivedStateFromProps(props, state) {
    state.user.twitchVerified = props.user.twitchVerified;
    return state;
  }

  componentDidMount() {
    this.manualAuthCallback();
  }

  openAuthScreen = service => {
    if (SERVICES.includes(service)) {
      analytics.track(`PORTFOLIO_VIDEO_${service.toUpperCase()}_VERIFICATION_START`);
      const statesTobeUpdated = {
        authWindow: true,
        serviceURL: this.getServiceUrl()[`${service}URL`]
      };
      this.setState(statesTobeUpdated, () => {
        this.setState({ authWindow: false });
      });
    }
  };

  getServiceUrl = () => {
    const { twitchClientID, twitchRedirectUrl } = publicRuntimeConfig;
    return {
      twitchURL: `https://id.twitch.tv/oauth2/authorize?client_id=${twitchClientID}&redirect_uri=${twitchRedirectUrl}&response_type=code&scope=user:read:email`
    };
  };

  manualAuthCallback = () => {
    window.onmessage = e => {
      const common = intersection(Object.keys(e.data), SERVICES);
      if (common.length === 1) {
        this.authNotification(e.data.status, common[0]);
      }
    };
  };

  authNotification = (status, service) => {
    const title = service.toLocaleUpperCase();
    if (status === "success") {
      const user = cloneDeep(this.props.user);
      user[`${service}Verified`] = true;
      this.props.updateUserSimple(user);
      analytics.track(`PORTFOLIO_VIDEO_${service.toUpperCase()}_VERIFICATION_SUCCESS`);
      sendNotification("Successfully verified!", "success", title);
    } else {
      analytics.track(`PORTFOLIO_VIDEO_${service.toUpperCase()}_VERIFICATION_FAIL`);
      sendNotification("Can not be verified, please try again.", "danger", title);
    }
  };

  onHide = () => {
    this.props.togglePortfolioModal(false);
  };

  render() {
    const { button, customClickComponent } = this.props;

    return (
      <>
        {customClickComponent ? (
          <div onClick={() => this.openAuthScreen(button)}>{customClickComponent}</div>
        ) : (
          <AddButton
            desc={`Link ${button} Stream`}
            icon={faLink}
            image={withCdn(`/static/images/${button}_icon.png`)}
            onClick={() => this.openAuthScreen(button)}
          />
        )}
        {this.state.authWindow && WindowPortal(this.state.serviceURL, "Verification")}
      </>
    );
  }
}
const mapStateToProps = state => ({
  event: state.settings.event,
  isWindowView: state.messages.isWindowView,
  modal: state.portfolio.modal,
  modalSection: state.portfolio.modalSection
});

export default connect(mapStateToProps, {
  dispatchNotification,
  updateUserSimple,
  togglePortfolioModal
})(Auth);
