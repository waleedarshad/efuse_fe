import React, { useState } from "react";
import { Form, Col } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import InputRow from "../../../../InputRow/InputRow";
import InputField from "../../../../InputField/InputField";
import InputLabel from "../../../../InputLabel/InputLabel";
import ActionButtonGroup from "../../../../Layouts/Internal/ActionButtonGroup/ActionButtonGroup";
import { updateVideoInfo } from "../../../../../store/actions/portfolioActions";
import EFPrimaryModal from "../../../../Modals/EFPrimaryModal/EFPrimaryModal";

const EmbedVideo = ({ show, onHide }) => {
  const dispatch = useDispatch();

  const inProgress = useSelector(state => state.portfolio.inProgress);

  const [validated, setValidated] = useState(false);
  const [user, setUser] = useState({
    embeddedLink: null,
    youtubeChannelId: null
  });

  const onChange = event => {
    setUser({
      ...user,
      [event.target.name]: event.target.value
    });
  };

  const onSubmit = event => {
    event.preventDefault();

    if (!validated) {
      setValidated(true);
    }
    if (event.currentTarget.checkValidity()) {
      analytics.track("PORTFOLIO_VIDEO_YOUTUBE_HIGHLIGHT_CREATE", {
        channel: user.youtubeChannelId
      });
      dispatch(updateVideoInfo({ ...user }));
    }
  };

  const { embeddedLink, youtubeChannelId } = user;
  return (
    <EFPrimaryModal
      title="YouTube Info"
      isOpen={show}
      onClose={onHide}
      widthTheme="medium"
      allowBackgroundClickClose={false}
    >
      <Form noValidate validated={validated} onSubmit={onSubmit}>
        <InputRow>
          <Col sm={6}>
            <InputLabel theme="darkColor">Add Youtube Link</InputLabel>
            <InputField
              name="embeddedLink"
              placeholder="Add Youtube Link"
              theme="whiteShadow"
              size="lg"
              onChange={onChange}
              value={embeddedLink}
              required
              errorMessage="Link is required"
            />
          </Col>
          <Col sm={6}>
            <InputLabel theme="darkColor">Add Channel ID</InputLabel>
            <InputField
              name="youtubeChannelId"
              placeholder="Add Youtube Channel ID"
              theme="whiteShadow"
              size="lg"
              onChange={onChange}
              value={youtubeChannelId}
            />
          </Col>
        </InputRow>

        <ActionButtonGroup onCancel={onHide} disabled={inProgress} />
      </Form>
    </EFPrimaryModal>
  );
};

export default EmbedVideo;
