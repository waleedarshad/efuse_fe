import React, { Component } from "react";
import { connect } from "react-redux";
import { faPlus } from "@fortawesome/pro-solid-svg-icons";

import Style from "./YoutubePlayer.module.scss";
import VideoPlayer from "../../../../VideoPlayer/VideoPlayer";
import { togglePortfolioModal } from "../../../../../store/actions/portfolioActions";
import EmbedVideo from "./EmbedVideo";
import AddButton from "../../AddButton/AddButton";
import { withCdn } from "../../../../../common/utils";

class YoutubePlayer extends Component {
  loadVideoModal = () => {
    analytics.track("PORTFOLIO_VIDEO_YOUTUBE_HIGHLIGHT_MODAL_OPEN");
    this.props.togglePortfolioModal(true, "stream");
  };

  onHide = () => {
    analytics.track("PORTFOLIO_VIDEO_YOUTUBE_HIGHLIGHT_MODAL_CLOSE");
    this.props.togglePortfolioModal(false);
  };

  renderPlayer = user => {
    const { userLiveInfo } = this.props;
    let channelUrl = "";
    if (userLiveInfo) {
      if (!userLiveInfo.isLive) {
        channelUrl = user.embeddedLink;
      } else {
        channelUrl = `https://www.youtube.com/watch?v=${userLiveInfo.youtubeMediaID}`;
      }
    }
    return (
      <div className={Style.videoContainer}>
        <div className={Style.videoPlayer}>
          <VideoPlayer url={channelUrl} height="300px" />
        </div>
        {/* <div className={Style.videoTextContainer}>
          <h2 className={Style.videoTextTitle}>{GeneralHelper.fullName(user)}'s 2019 highlight reel</h2>
          <h4 className={Style.videoTextSubtitle}>1,273,128 views • 3 weeks ago</h4>
          <p className={Style.videoTextDescription}>This is my 2019 highlight reel compiled
          with all my greatest clips for the year. If you are interested in watching me play LIVE,
          please head over to my Twitch Stream (above). Feel free to follow all of my socials.
                    Make sure to share this reel if you’re a true Choco Taco supporter! Stay safe my friends.</p>
        </div> */}
      </div>
    );
  };

  render() {
    const { user, modal, userLiveInfo, modalSection } = this.props;
    const { embeddedLink } = user;
    return (
      <>
        {embeddedLink || (userLiveInfo && userLiveInfo.isLive) ? (
          this.renderPlayer(user)
        ) : (
          <>
            <AddButton
              desc="Embed YouTube Video"
              icon={faPlus}
              image={withCdn("/static/images/youtube_icon.png")}
              onClick={this.loadVideoModal}
            />
            {modal && modalSection === "stream" && <EmbedVideo show={modal} onHide={this.onHide} user={user} />}
          </>
        )}
      </>
    );
  }
}
const mapStateToProps = state => ({
  user: state.user.currentUser,
  isWindowView: state.messages.isWindowView,
  userLiveInfo: state.user.userLiveInfo,
  currentUser: state.auth.currentUser,
  modal: state.portfolio.modal,
  modalSection: state.portfolio.modalSection
});
export default connect(mapStateToProps, {
  togglePortfolioModal
})(YoutubePlayer);
