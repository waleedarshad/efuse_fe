import { useDispatch } from "react-redux";
import { useEffect } from "react";
import Router from "next/router";
import { Col, Row } from "react-bootstrap";
import Feed from "../Feed/Feed";
import { getUserFeed } from "../../store/actions/feedActions";
import { isUserLoggedIn } from "../../helpers/AuthHelper";
import EFSideOpportunities from "../EFSideOpportunities/EFSideOpportunities";

interface UserFeedProps {
  id: string;
}
const UserFeed = ({ id }: UserFeedProps) => {
  const dispatch = useDispatch();

  useEffect(() => {
    const isLoggedIn = isUserLoggedIn();
    if (!isLoggedIn) {
      Router.push("/login");
    }
  }, []);

  return (
    <Row>
      <Col className="col-lg-8">
        <Feed feedType="profile" getFeed={page => dispatch(getUserFeed(page, 10, id))} />
      </Col>
      <Col className="col-md-4 d-none d-lg-block">
        <EFSideOpportunities />
      </Col>
    </Row>
  );
};

export default UserFeed;
