import React from "react";
import ReactDOM from "react-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes, faLink } from "@fortawesome/pro-solid-svg-icons";
import { connect } from "react-redux";
import { Form } from "react-bootstrap";

import Style from "./LinkStreamModal.module.scss";
import Auth from "../User/Portfolio/VideoComponent/Auth/Auth";
import { updateVideoInfo } from "../../store/actions/portfolioActions";
import InputField from "../InputField/InputField";
import InputLabel from "../InputLabel/InputLabel";
import ActionButtonGroup from "../Layouts/Internal/ActionButtonGroup/ActionButtonGroup";
import AddButton from "../User/Portfolio/AddButton/AddButton";
import { withCdn } from "../../common/utils";

class LinkStreamModal extends React.Component {
  state = {
    showModal: false,
    validated: false,
    valuesSet: false,
    youtubeSelected: false,
    userYoutube: {
      youtubeChannelId: null
    }
  };

  openModal(e) {
    e.stopPropagation();
    this.setState({ ...this.state, showModal: true });
  }

  closeModal() {
    this.setState({ ...this.state, showModal: false });
  }

  doNothing(e) {
    e.stopPropagation();
  }

  onChange = event => {
    this.setState({
      userYoutube: {
        ...this.state.userYoutube,
        [event.target.name]: event.target.value
      }
    });
  };

  onSubmit = event => {
    event.preventDefault();
    const { validated } = this.state;
    if (!validated) {
      this.setState({ validated: true });
    }
    if (event.currentTarget.checkValidity()) {
      this.props.updateVideoInfo({ ...this.state.userYoutube });
      this.closeModal();
    }
  };

  showYoutube = () => {
    this.setState({
      ...this.state,
      youtubeSelected: !this.state.youtubeSelected
    });
  };

  render() {
    const { children, user, openOnClick } = this.props;
    const { showModal, validated, userYoutube, youtubeSelected } = this.state;
    const { youtubeChannelId } = userYoutube;

    const modal = (
      <div
        className={Style.modalBackground}
        onClick={() => {
          this.closeModal();
        }}
      >
        <div
          className={Style.modal}
          onClick={e => {
            this.doNothing(e);
          }}
        >
          <div
            className={Style.modalCloseButton}
            onClick={() => {
              this.closeModal();
            }}
          >
            <FontAwesomeIcon icon={faTimes} />
          </div>
          <div className={Style.modalTitle}>Link a Stream</div>
          <p className={Style.subText}>
            *To apply to this opportunity, you must have at least one of the following stream accounts linked.
          </p>
          <div className={Style.container}>
            <Auth user={user} button="twitch" isVerified={user.twitchVerified} />
          </div>
          <div className={Style.container}>
            <AddButton
              desc="Link YouTube Channel ID"
              icon={faLink}
              image={withCdn("/static/images/youtube_icon.png")}
              onClick={this.showYoutube}
            />
          </div>
          {youtubeSelected && (
            <div className={Style.container}>
              <Form noValidate validated={validated} onSubmit={this.onSubmit}>
                <InputLabel theme="internal">Add Channel ID</InputLabel>
                <InputField
                  name="youtubeChannelId"
                  placeholder="Add Youtube Channel ID"
                  theme="internal"
                  size="lg"
                  onChange={this.onChange}
                  value={youtubeChannelId}
                />
                <ActionButtonGroup />
              </Form>
            </div>
          )}
        </div>
      </div>
    );

    if (typeof window !== "undefined" && showModal) {
      return (
        <>
          {children}
          {ReactDOM.createPortal(modal, document.getElementById("modal-root"))}
        </>
      );
    }
    return (
      <span
        onClick={e => {
          openOnClick && this.openModal(e);
        }}
      >
        {children}
      </span>
    );
  }
}

LinkStreamModal.defaultProps = {
  openOnClick: true
};

const mapStateToProps = state => ({});

export default connect(mapStateToProps, { updateVideoInfo })(LinkStreamModal);
