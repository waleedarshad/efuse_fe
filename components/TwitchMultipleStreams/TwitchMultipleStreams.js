import React, { useState } from "react";
import TwitchVideoPlayerCustomChat from "../TwitchVideoPlayerCustomChat/TwitchVideoPlayerCustomChat";
import Style from "./TwitchMultipleStreams.module.scss";
import HorizontalScroll from "../HorizontalScroll/HorizontalScroll";
import StreamTeam from "./StreamTeam/StreamTeam";

const TwitchMultipleStreams = ({ mainStream, startingChannel, startingName, streams, streamTitle }) => {
  const [selectedChannel, setChannel] = useState(startingChannel);
  const [name, setName] = useState(startingName);

  /* [PAVEL] THIS YOUTUBE FUNCTIONALITY IS TEMPORARY AND HACKY */
  const getDisplayName = name => {
    if (name.includes("youtube.com") || name.includes("youtu.be")) {
      return "Youtube Live";
    } else {
      return name;
    }
  };

  return (
    <div className={Style.wrapper}>
      <TwitchVideoPlayerCustomChat
        channel={selectedChannel}
        title={`${streamTitle} | ${getDisplayName(name)}`}
        muted={false}
        autoplay
      />
      <HorizontalScroll noPadding customButtonMargin={Style.customButtonMargin}>
        <div className={Style.teamsWrapper}>
          {mainStream && (
            <StreamTeam
              setChannel={() => setChannel(mainStream?.players[0]?.channel)}
              setName={() => setName(mainStream?.players[0]?.name)}
              team={mainStream}
              teamName="Main Event"
              key="main"
              selectedChannel={selectedChannel}
            />
          )}
          {streams?.length > 0 &&
            streams.map((team, index) => {
              return (
                <StreamTeam
                  setChannel={setChannel}
                  setName={setName}
                  team={team}
                  teamName={team?.name}
                  key={index}
                  selectedChannel={selectedChannel}
                />
              );
            })}
        </div>
      </HorizontalScroll>
    </div>
  );
};

export default TwitchMultipleStreams;
