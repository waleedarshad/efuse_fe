import React from "react";
import Style from "./StreamTeam.module.scss";

const StreamTeam = ({ key, team, teamName, setChannel, setName, selectedChannel }) => {
  /* [PAVEL] THIS YOUTUBE FUNCTIONALITY IS TEMPORARY AND HACKY */
  const getDisplayName = name => {
    if (name.includes("youtube.com") || name.includes("youtu.be")) {
      return "Youtube Live";
    } else {
      return name;
    }
  };
  return (
    <div className={Style.team} key={key}>
      <div className={Style.teamName}>{teamName}</div>
      {team?.players?.map(player => {
        return (
          <div
            className={`${Style.player} ${selectedChannel === player?.channel && Style.activePlayer}`}
            onClick={
              player?.type === "twitch"
                ? () => {
                    setChannel(player?.channel);
                    setName(player?.name);
                  }
                : () => window.open(player?.channel, "_blank")
            }
            key={player?._id}
          >
            {getDisplayName(player?.name)}
          </div>
        );
      })}
    </div>
  );
};

export default StreamTeam;
