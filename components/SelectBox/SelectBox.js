import PropTypes from "prop-types";
import Style from "./SelectBox.module.scss";
import InputField from "../InputField/InputField";

const SelectBox = props => {
  const { options, validated, ...rest } = props;
  const buildChildren = options.map((option, index) => (
    <option key={index} value={option.value}>
      {option.label}
    </option>
  ));
  const styleKlass = validated ? `${Style.downArrow}` : `${Style.downArrow} ${Style.show}`;
  return (
    <div className={`${Style.selectBox} ${Style[props.theme]}`}>
      <InputField as="select" {...rest} theme={props.theme}>
        {buildChildren}
      </InputField>
      <span className={styleKlass} />
    </div>
  );
};

SelectBox.propTypes = {
  options: PropTypes.array.isRequired,
  validated: PropTypes.bool.isRequired,
  theme: PropTypes.string
};

SelectBox.defaultProps = {
  theme: "external"
};

export default SelectBox;
