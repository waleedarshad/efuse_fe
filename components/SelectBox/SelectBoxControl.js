import React from "react";
import Style from "./SelectBox.module.scss";
import InputStyle from "../InputField/InputField.module.scss";

const SelectBoxControl = ({ options, ...rest }) => {
  return (
    <div className={Style.selectBox}>
      <select {...rest} className={InputStyle.whiteShadow}>
        {options.map(({ value, label }) => (
          <option key={value} value={value}>
            {label}
          </option>
        ))}
      </select>
      <span className={Style.downArrow} />
    </div>
  );
};

export default SelectBoxControl;
