import React from "react";
import TimeAgo from "react-timeago";
import Link from "next/link";
import Style from "./HighlightFive.module.scss";

const HighlightFive = ({ listOfFive }) => {
  return (
    <div className={Style.headerHighlightWrapper}>
      {listOfFive &&
        listOfFive.map(item => {
          return (
            <Link href={item?.url}>
              <div>
                <div className={Style.imageGradient} />
                <img src={item?.image} className={Style.image} />
                <div className={Style.bottomText}>
                  <p className={Style.title}>{item?.title}</p>
                  <div className={Style.bottomSubText}>
                    <p className={Style.name}>{item?.name}</p>
                    <p className={Style.date}>
                      <TimeAgo date={item?.date} />
                    </p>
                  </div>
                </div>
              </div>
            </Link>
          );
        })}
    </div>
  );
};

export default HighlightFive;
