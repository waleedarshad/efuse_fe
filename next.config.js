const FilterWarningsPlugin = require("webpack-filter-warnings-plugin");
const { BundleAnalyzerPlugin } = require("webpack-bundle-analyzer");
const { redirects } = require("./routes/redirects");

const securityHeaders = [
  {
    key: "Content-Security-Policy",
    value: "frame-ancestors 'self'"
  }
];

module.exports = {
  eslint: {
    // Warning: This allows production builds to successfully complete even if
    // your project has ESLint errors.
    ignoreDuringBuilds: true
  },
  assetPrefix: process.env.ENVIRONMENT === "production" ? process.env.CDN_BASE_URL : "",
  // eslint-disable-next-line prefer-arrow/prefer-arrow-functions
  async headers() {
    return [
      {
        source: "/(.*)",
        headers: securityHeaders
      },
      {
        source: "/overlay/:token",
        headers: [
          {
            key: "Content-Security-Policy",
            value: "frame-ancestors 'self' *.golightstream.com"
          }
        ]
      }
    ];
  },
  webpack: (config, { isServer }) => {
    // eslint-disable-next-line no-param-reassign
    if (process.env.WEBPACK_DEVTOOL) {
      config.devtool = process.env.WEBPACK_DEVTOOL;
    }
    config.plugins.push(
      new FilterWarningsPlugin({
        exclude: /mini-css-extract-plugin[^]*Conflicting order between:/
      })
    );
    if (process.env.ANALYZE) {
      config.plugins.push(
        new BundleAnalyzerPlugin({
          analyzerMode: "server",
          analyzerPort: isServer ? 8888 : 8889,
          openAnalyzer: true
        })
      );
    }
    config.module.rules.push({
      test: /\.(eot|gif|otf|png|svg|ttf|woff)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
      use: [
        {
          loader: require.resolve("file-loader"),
          options: {
            publicPath: "/_next/static/images/",
            outputPath: "static/images/"
          }
        }
      ]
    });

    if (process.env.BUNDLE_ANALYZER) {
      config.plugins.push(
        new BundleAnalyzerPlugin({
          analyzerMode: "static",
          reportFilename: "report.html"
        })
      );
    }
    return config;
  },
  // image domains used for next/image (EFImage) component
  images: {
    domains: [
      "efuse.gg",
      "staging-cdn.efuse.gg",
      "cdn.efuse.gg",
      "staging-efuse.s3.amazonaws.com",
      "efuse.s3.amazonaws.com",
      "cdn.hitmarker.net",
      "efuse.wtf",
      "scontent.efcdn.io"
    ]
  },
  env: {
    defaultUrl: process.env.DEFAULT_URL,
    baseUrl: process.env.BASE_URL,
    discordURL: process.env.DISCORD_URL,
    facebookID: process.env.EFUSE_FACEBOOK_ID,
    twitchClientID: process.env.TWITCH_CLIENT_ID,
    googleMapsApiKey: process.env.GOOGLE_MAPS_API_KEY,
    environment: process.env.ENVIRONMENT,
    googleRecaptcha3Key: process.env.GOOGLE_RECAPTCHA3_CLIENT_KEY,
    feBaseUrl: process.env.FE_BASE_URL,
    giphyApiKey: process.env.GIPHY_API_KEY,
    twitchSignupRedirectUrl: process.env.TWITCH_SIGNUP_REDIRECT_URL,
    twitchLoginRedirectUrl: process.env.TWITCH_LOGIN_REDIRECT_URL,
    discordSignupURL: process.env.DISCORD_SIGNUP_URL,
    discordLoginURL: process.env.DISCORD_LOGIN_URL,
    stripePublishableKey: process.env.STRIPE_PUBLISHABLE_KEY,
    stripeClientId: process.env.STRIPE_CLIENT_ID,
    xboxLiveAuthUrl: process.env.XBOX_LIVE_AUTH_URL,
    viralLoopsCampaignId: process.env.VIRAL_LOOPS_CAMPAIGN_ID,
    cdnBaseUrl: process.env.CDN_BASE_URL
  },
  publicRuntimeConfig: {
    defaultUrl: process.env.DEFAULT_URL,
    baseUrl: process.env.BASE_URL,
    discordURL: process.env.DISCORD_URL,
    facebookID: process.env.EFUSE_FACEBOOK_ID,
    twitchClientID: process.env.TWITCH_CLIENT_ID,
    twitchRedirectUrl: process.env.TWITCH_REDIRECT_URL,
    googleMapsApiKey: process.env.GOOGLE_MAPS_API_KEY,
    environment: process.env.ENVIRONMENT,
    googleRecaptcha3Key: process.env.GOOGLE_RECAPTCHA3_CLIENT_KEY,
    feBaseUrl: process.env.FE_BASE_URL,
    giphyApiKey: process.env.GIPHY_API_KEY,
    twitchSignupRedirectUrl: process.env.TWITCH_SIGNUP_REDIRECT_URL,
    twitchLoginRedirectUrl: process.env.TWITCH_LOGIN_REDIRECT_URL,
    discordSignupURL: process.env.DISCORD_SIGNUP_URL,
    discordLoginURL: process.env.DISCORD_LOGIN_URL,
    stripePublishableKey: process.env.STRIPE_PUBLISHABLE_KEY,
    stripeClientId: process.env.STRIPE_CLIENT_ID,
    xboxLiveAuthUrl: process.env.XBOX_LIVE_AUTH_URL,
    viralLoopsCampaignId: process.env.VIRAL_LOOPS_CAMPAIGN_ID,
    cdnBaseUrl: process.env.CDN_BASE_URL,
    graphqlURL: process.env.GRAPHQL_URL,
    bulletTrainEnvironment: process.env.BULLET_TRAIN_ENVIRONMENT,
    bulletTrainApi: process.env.BULLET_TRAIN_API,
    appsFlyerWebKey: process.env.APPSFLYER_WEB_KEY,
    algoliaApplicationId: process.env.ALGOLIA_APP_ID,
    algoliaSearchApiKey: process.env.ALGOLIA_SEARCH_API_KEY,
    streamChatKey: process.env.STREAM_CHAT_KEY,
    ddClientToken: process.env.DD_CLIENT_TOKEN,
    ddApplicationId: process.env.DD_APPLICATION_ID
  },
  redirects: async () => redirects,
  typescript: {
    // !! WARN !!
    // Dangerously allow production builds to successfully complete even if
    // the project has type errors.
    // The goal is to work our way into fixing all these compile errors over time.
    // !! WARN !!
    ignoreBuildErrors: true
  }
};
