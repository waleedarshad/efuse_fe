import { ClippyResourceEnum, EFuseResourceEnum } from "../enums";

/**
 * A resource for permission can be very generic or very specific.
 *
 * If its a string then it would be an ID for something. It could be an ID for a league.
 *
 * If the general resource enum then it is a general resource that could be associated with alot of entities.
 * For instance if its a LEAGUE then the resource owner is probably an Org
 *
 * Clippy Resource is very generic and would basically be asking if the user has access to a command
 */
type PermissionResourceType = string | EFuseResourceEnum | ClippyResourceEnum;

export default PermissionResourceType;
