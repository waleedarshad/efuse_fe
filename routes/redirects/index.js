const { learning } = require("./learning");
const { other } = require("./other");

module.exports.redirects = [].concat(learning, other);
