module.exports.other = [
  {
    source: "/careers",
    destination: "https://efuse.careers",
    permanent: true
  },
  {
    source: "/career",
    destination: "https://efuse.careers",
    permanent: true
  },
  {
    source: "/branding",
    destination: "https://try.efuse.gg/branding",
    permanent: true
  },
  {
    source: "/brand",
    destination: "https://try.efuse.gg/branding",
    permanent: true
  },
  {
    source: "/lounge",
    destination: "/lounge/featured",
    permanent: true
  },
  {
    source: "/organizations/new",
    destination: "/organizations/create",
    permanent: true
  },
  {
    source: "/erena/new",
    destination: "/erena/create",
    permanent: true
  },
  {
    source: "/ad-redirect",
    destination: "/",
    permanent: true
  },
  {
    source: "/ad-redirect/:slug",
    destination: "/",
    permanent: true
  },
  {
    source: "/opportunities/show/:id",
    destination: "/opportunities/show?id=:id",
    permanent: true
  },
  {
    source: "/o/women-of-warzone",
    destination: "/e/wote",
    permanent: false
  },
  {
    source: "/settings/clippy",
    destination: "/clippy",
    permanent: true
  }
];
