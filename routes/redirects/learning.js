module.exports.learning = [
  {
    source: "/learning",
    destination: "/news",
    permanent: true
  },
  {
    source: "/learning/k12",
    destination: "/news/other",
    permanent: true
  },
  {
    source: "/learning/international-esports",
    destination: "/news/pipeline",
    permanent: true
  },
  {
    source: "/learning/global",
    destination: "/news/pipeline",
    permanent: true
  },
  {
    source: "/learning/featured-podcast",
    destination: "/news/esports-business",
    permanent: true
  },
  {
    source: "/learning/hitmarker",
    destination: "/news/professional-development",
    permanent: true
  },
  {
    source: "/learning/skill-development",
    destination: "/news/professional-development",
    permanent: true
  },
  {
    source: "/learning/interview-of-the-week",
    destination: "/news/professional-development",
    permanent: true
  },
  {
    source: "/learning/health-and-wellness",
    destination: "/news/health-and-wellness",
    permanent: true
  },
  {
    source: "/learning/pipeline",
    destination: "/news/pipeline",
    permanent: true
  },
  {
    source: "/learning/collegiate",
    destination: "/news/pipeline",
    permanent: true
  },
  {
    source: "/learning/k12/:slug",
    destination: "/news/other/:slug",
    permanent: true
  },
  {
    source: "/learning/international-esports/:slug",
    destination: "/news/pipeline/:slug",
    permanent: true
  },
  {
    source: "/learning/global/:slug",
    destination: "/news/pipeline/:slug",
    permanent: true
  },
  {
    source: "/learning/featured-podcast/:slug",
    destination: "/news/esports-business/:slug",
    permanent: true
  },
  {
    source: "/learning/hitmarker/:slug",
    destination: "/news/professional-development/:slug",
    permanent: true
  },
  {
    source: "/learning/skill-development/:slug",
    destination: "/news/professional-development/:slug",
    permanent: true
  },
  {
    source: "/learning/interview-of-the-week/:slug",
    destination: "/news/professional-development/:slug",
    permanent: true
  },
  {
    source: "/learning/health-and-wellness/:slug",
    destination: "/news/health-and-wellness/:slug",
    permanent: true
  },
  {
    source: "/learning/pipeline/:slug",
    destination: "/news/pipeline/:slug",
    permanent: true
  },
  {
    source: "/learning/collegiate/:slug",
    destination: "/news/pipeline/:slug",
    permanent: true
  },
  {
    source: "/news/efuse/discordbot",
    destination: "/news/efuse-help/discordbot",
    permanent: true
  },
  {
    source: "/news/efuse/efuse-affiliate",
    destination: "/news/efuse-help/efuse-affiliate",
    permanent: true
  },
  {
    source: "/news/efuse/efuse-partner",
    destination: "/news/efuse-help/efuse-partner",
    permanent: true
  },
  {
    source: "/learning/efuse/:slug",
    destination: "/news/efuse/:slug",
    permanent: true
  }
];
