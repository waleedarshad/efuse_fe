module.exports = server => {
  server.get("/__status__", (req, res) => {
    res.status(200).send("OK");
  });
};
