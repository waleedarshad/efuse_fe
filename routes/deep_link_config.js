module.exports = server => {
  server.get("/apple-app-site-association", (req, res, next) => {
    res.status(200).json({
      applinks: {
        apps: [],
        details: [
          {
            appID: "6A84YJ8U9U.com.EF.eFuse",
            paths: ["*", "NOT /clippy/*", "NOT /clippy"]
          },
          {
            appID: "W8FX6D8J77.com.EF.eFuse",
            paths: ["*", "NOT /clippy/*", "NOT /clippy"]
          }
        ]
      }
    });
  });
};
