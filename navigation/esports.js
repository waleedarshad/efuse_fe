import HREFS from "../common/hrefs";

// eslint-disable-next-line import/prefer-default-export
export const getEsportsNavigationList = (pathname, showErenaLink) => {
  const navigationList = [
    {
      href: HREFS.ESPORTS,
      name: "Esports",
      isActive: pathname === HREFS.ESPORTS
    },
    {
      href: HREFS.PIPELINE,
      name: "Pipeline",
      isActive: pathname === HREFS.PIPELINE
    }
  ];

  showErenaLink &&
    navigationList.push({
      href: HREFS.ERENA,
      name: "eRena",
      isActive: pathname === HREFS.ERENA
    });

  return navigationList;
};
