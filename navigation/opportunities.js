import HREFS from "../common/hrefs";

export const getOpportunitiesNavigationList = pathname => {
  return [
    {
      href: HREFS.OPPORTUNITIES,
      name: "All Opportunities",
      isActive: pathname === HREFS.OPPORTUNITIES
    },
    {
      href: HREFS.OPPORTUNITIES_ALL,
      name: "Browse",
      isActive: pathname === HREFS.OPPORTUNITIES_ALL
    },
    {
      href: HREFS.OPPORTUNITIES_APPLIED,
      name: "Applied",
      isActive: pathname === HREFS.OPPORTUNITIES_APPLIED
    },
    {
      href: HREFS.OPPORTUNITIES_OWNED,
      name: "My Opportunities",
      isActive: pathname === HREFS.OPPORTUNITIES_OWNED
    }
  ];
};
