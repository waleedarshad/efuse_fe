import HREFS from "../common/hrefs";

// eslint-disable-next-line import/prefer-default-export
export const getDiscoverNavigationList = (pathname, showErenaLink) => {
  const navigationList = [
    {
      href: HREFS.DISCOVER,
      name: "Discover",
      isActive: pathname === HREFS.DISCOVER
    },
    {
      href: HREFS.ORGANIZATIONS,
      name: "Organizations",
      isActive: pathname === HREFS.ORGANIZATIONS || pathname === HREFS.ORGANIZATIONS_NEW
    },
    {
      href: HREFS.NEWS,
      name: "News",
      isActive: pathname === HREFS.NEWS
    },
    {
      href: HREFS.PIPELINE,
      name: "Pipeline",
      isActive: pathname === HREFS.PIPELINE
    }
  ];

  showErenaLink &&
    navigationList.push({
      href: HREFS.ERENA,
      name: "eRena",
      isActive: pathname === HREFS.ERENA
    });

  return navigationList;
};
