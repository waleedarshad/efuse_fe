export const getLeaguesNavigationList = router => {
  const commonPath = `/leagues/event/${router.query.eventId}`;
  return [
    {
      href: "#",
      name: "Overview",
      isActive: false
    },
    {
      href: `${commonPath}/teams`,
      name: "Teams",
      isActive: router.asPath === `${commonPath}/teams`
    },
    {
      href: `${commonPath}/pools`,
      name: "Pools",
      isActive: router.asPath === `${commonPath}/pools`
    },
    {
      href: `${commonPath}/bracket`,
      name: "Bracket",
      isActive: router.asPath === `/leagues/${router.query.leagueId}/event/${router.query.eventId}/bracket`
    },
    {
      href: "#",
      name: "Description",
      isActive: false
    },
    {
      href: "#",
      name: "Rules",
      isActive: false
    }
  ];
};
