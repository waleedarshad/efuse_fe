const getOrganizationNavigationList = (router, organization, isAdmin, showOrganizationTeams) => {
  const dashboardPath = `/organizations/dashboard?id=${organization._id}`;
  const teamsPath = `/organizations/teams?id=${organization._id}`;
  const aboutPath = `/org/${organization.shortName}`;
  const timelinePath = `/organizations/timeline?id=${organization._id}`;
  const membersPath = `/org/${organization.shortName}/members`;
  const requestPath = `/organizations/organization_requests?id=${organization._id}`;
  const followerPath = `/organizations/followers?id=${organization._id}`;
  const opportunitiesPath = `/organizations/opportunities?id=${organization._id}`;
  const organizationsPath = `/org/${organization.shortName}/organizations`;

  const organizationHasChildren = organization?.children?.length > 0;

  const adminList = [
    {
      href: dashboardPath,
      name: "Dashboard",
      isActive: router.asPath === dashboardPath
    },
    {
      href: requestPath,
      name: "Requests",
      isActive: router.asPath === requestPath
    }
  ];

  const userList = [
    {
      href: aboutPath,
      name: "About",
      isActive: router.pathname === "/org/[org]" || router.asPath === aboutPath
    },
    {
      href: timelinePath,
      name: "Feed",
      isActive: router.asPath === timelinePath
    },
    {
      href: membersPath,
      name: "Members",
      isActive: router.pathname === "/org/[org]/members" || router.asPath === membersPath
    },
    {
      href: followerPath,
      name: "Followers",
      isActive: router.asPath === followerPath
    },
    {
      href: opportunitiesPath,
      name: "Opportunities",
      isActive: router.asPath === opportunitiesPath
    }
  ];

  const organizationTab = {
    href: organizationsPath,
    name: "Organizations",
    isActive: router.pathname === "/org/[org]/organizations" || router.asPath === organizationsPath
  };

  const teamsTab = {
    href: teamsPath,
    name: "Teams",
    isActive: router.asPath === teamsPath
  };

  if (organizationHasChildren) {
    userList.push(organizationTab);
  }

  if (showOrganizationTeams && (isAdmin || organization.teams?.length)) {
    userList.unshift(teamsTab);
  }

  return isAdmin ? adminList.concat(userList) : userList;
};

export default getOrganizationNavigationList;
