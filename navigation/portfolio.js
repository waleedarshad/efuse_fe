import { rescueNil, constructPath } from "../helpers/GeneralHelper";

// eslint-disable-next-line import/prefer-default-export
export const getPortfolioNavigationList = (user, router, badgesFlag, userLoggedIn) => {
  const portFolioPath = `/u/${rescueNil(user, "username")}`;
  const followersPath = constructPath("/users/followers", router.query);
  const postPath = constructPath("/users/posts", router.query);
  const followingPath = constructPath("/users/followees", router.query);
  const mediaPath = constructPath("/users/media", router.query);
  const badgesPath = constructPath("/users/badges", router.query);
  const badges = [
    { href: badgesPath, name: "BADGES", showOnPortfolio: userLoggedIn, isActive: router.asPath === badgesPath }
  ];
  const portfolioList = [
    {
      href: portFolioPath,
      name: "Portfolio",
      isActive: router.asPath === portFolioPath,
      showOnPortfolio: true
    },
    {
      href: postPath,
      name: "Posts",
      isActive: router.asPath === postPath,
      showOnPortfolio: userLoggedIn
    },
    {
      href: followersPath,
      name: "Followers",
      isActive: router.asPath === followersPath,
      showOnPortfolio: userLoggedIn
    },
    {
      href: followingPath,
      name: "Following",
      isActive: router.asPath === followingPath,
      showOnPortfolio: userLoggedIn
    },
    {
      href: mediaPath,
      name: "Media",
      isActive: router.asPath === mediaPath,
      showOnPortfolio: userLoggedIn
    }
  ];
  if (badgesFlag) {
    return portfolioList.concat(badges);
  }
  return portfolioList;
};
