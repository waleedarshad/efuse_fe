export const erenaNavigationList = (query, pathname, isBracket) => {
  //common navigation items
  let eRenaList = [
    {
      href: `/e/${query.e}`,
      name: "Main Page",
      isActive:
        !query?.teams && !query?.bracket && !query?.scoring && !query?.opportunity && !pathname?.includes("settings")
    },
    { href: `/e/${query.e}/settings`, name: "Settings", isActive: pathname?.includes("settings") },
    { href: { pathname: `/e/${query.e}/manage`, query: { teams: true } }, name: "Teams", isActive: query?.teams }
  ];

  //if bracket show the bracket route, else show the scoring route
  if (isBracket) {
    eRenaList.push({
      href: { pathname: `/e/${query.e}/manage`, query: { bracket: true } },
      name: "Bracket",
      isActive: query?.bracket
    });
  } else {
    eRenaList.push({
      href: { pathname: `/e/${query.e}/manage`, query: { scoring: true } },
      name: "Scoring",
      isActive: query?.scoring
    });
  }

  return eRenaList;
};

export const erenaNavigationListV2 = (query, pathname, isBracket) => {
  //common navigation items
  let eRenaList = [
    {
      href: `/erenav2/${query.e}`,
      name: "Main Page",
      isActive:
        !query?.teams && !query?.bracket && !query?.scoring && !query?.opportunity && !pathname?.includes("settings")
    },
    { href: `/erenav2/${query.e}/settings`, name: "Settings", isActive: pathname?.includes("settings") },
    { href: { pathname: `/erenav2/${query.e}/manage`, query: { teams: true } }, name: "Teams", isActive: query?.teams }
  ];

  //if bracket show the bracket route, else show the scoring route
  if (isBracket) {
    eRenaList.push({
      href: { pathname: `/erenav2/${query.e}/manage`, query: { bracket: true } },
      name: "Bracket",
      isActive: query?.bracket
    });
  } else {
    eRenaList.push({
      href: { pathname: `/erenav2/${query.e}/manage`, query: { scoring: true } },
      name: "Scoring",
      isActive: query?.scoring
    });
  }

  return eRenaList;
};

export const erenaLandingNavigationList = (showCompete, showWatch, showPipeline, showNews, pathname) => {
  const links = [
    {
      name: "Watch",
      href: "/",
      flag: showWatch,
      isActive: pathname === "/erena"
    },
    {
      name: "Compete",
      href: "",
      flag: showCompete,
      isActive: false
    },
    {
      name: "News",
      href: "",
      flag: showNews,
      isActive: false
    },
    {
      name: "Pipeline",
      href: "/pipeline",
      flag: showPipeline,
      isActive: false
    }
  ];

  let navigationList = [];
  links.map(link => {
    if (link.flag) {
      navigationList.push({
        name: link.name,
        href: link.href,
        isActive: link.isActive
      });
    }
  });

  return navigationList;
};
