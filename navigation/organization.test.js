import getOrganizationNavigationList from "./organization";

describe("getOrganizationNavigationList", () => {
  const router = {
    asPath: "/org/vandalay-industries"
  };

  const expected = [
    {
      href: "/org/vandalay-industries",
      name: "About",
      isActive: true
    },
    {
      href: "/organizations/timeline?id=2222222",
      name: "Feed",
      isActive: false
    },
    {
      href: "/org/vandalay-industries/members",
      name: "Members",
      isActive: false
    },
    {
      href: "/organizations/followers?id=2222222",
      name: "Followers",
      isActive: false
    },
    {
      href: "/organizations/opportunities?id=2222222",
      name: "Opportunities",
      isActive: false
    }
  ];

  const organizationsTab = {
    href: "/org/vandalay-industries/organizations",
    name: "Organizations",
    isActive: false
  };

  const adminsList = [
    {
      href: "/organizations/dashboard?id=2222222",
      name: "Dashboard",
      isActive: false
    },
    {
      href: "/organizations/organization_requests?id=2222222",
      name: "Requests",
      isActive: false
    }
  ];

  const teamsTab = {
    href: "/organizations/teams?id=2222222",
    name: "Teams",
    isActive: false
  };

  it("returns usersList by default", () => {
    const organization = {
      _id: "2222222",
      shortName: "vandalay-industries"
    };

    const actual = getOrganizationNavigationList(router, organization, false);

    expect(expected).toEqual(actual);
  });

  it("adds Organizations to usersList if organization has children", () => {
    const organization = {
      shortName: "vandalay-industries",
      _id: "2222222",
      children: [
        {
          _id: "123",
          shortName: "cool-name"
        },
        {
          _id: "321",
          shortName: "awesome-name"
        }
      ]
    };

    const newExpected = [...expected, organizationsTab];

    const actual = getOrganizationNavigationList(router, organization, false);

    expect(newExpected).toEqual(actual);
  });

  it("Shows admin list without teams link when flag is OFF and if logged in user", () => {
    const organization = {
      _id: "2222222",
      shortName: "vandalay-industries"
    };

    const expectedList = adminsList.concat(expected);

    const actual = getOrganizationNavigationList(router, organization, true, false);

    expect(expectedList).toEqual(actual);
  });

  it("also shows admin list and teams link when flag is ON and if logged in user", () => {
    const organization = {
      _id: "2222222",
      shortName: "vandalay-industries"
    };

    expected.unshift(teamsTab);

    const expectedList = adminsList.concat(expected);

    const actual = getOrganizationNavigationList(router, organization, true, true);

    expect(expectedList).toEqual(actual);
  });
});
