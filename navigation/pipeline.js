export const getPipelineNavigationList = query => {
  return [
    {
      href: { pathname: "/pipeline/[game]", query: { game: "league-of-legends" } },
      name: "League of Legends",
      isActive: query?.game === "league-of-legends" || !query?.game,
      onClick: () => analytics.track("PIPELINE_LEADERBOARD_CLICK", { kind: "league-of-legends" })
    },
    {
      href: { pathname: "/pipeline/[game]", query: { game: "valorant" } },
      name: "Valorant",
      isActive: query?.game === "valorant",
      onClick: () => analytics.track("PIPELINE_LEADERBOARD_CLICK", { kind: "valorant" })
    },
    {
      href: { pathname: "/pipeline/[game]", query: { game: "aimlab" } },
      name: "AimLab",
      isActive: query?.game === "aimlab",
      onClick: () => analytics.track("PIPELINE_LEADERBOARD_CLICK", { kind: "aimlab" })
    },
    {
      href: { pathname: "/pipeline/[game]", query: { game: "overwatch" } },
      name: "Overwatch",
      isActive: query?.game === "overwatch",
      onClick: () => analytics.track("PIPELINE_LEADERBOARD_CLICK", { kind: "overwatch" })
    },
    {
      href: { pathname: "/pipeline/[game]", query: { game: "rocket-league" } },
      name: "Rocket League",
      isActive: query?.game === "rocket-league",
      onClick: () => analytics.track("PIPELINE_LEADERBOARD_CLICK", { kind: "rocket-league" })
    },
    {
      href: { pathname: "/pipeline/[game]", query: { game: "fortnite" } },
      name: "Fortnite",
      isActive: query?.game === "fortnite",
      onClick: () => analytics.track("PIPELINE_LEADERBOARD_CLICK", { kind: "fortnite" })
    }
  ];
};
