## Overview

- Describe why the pull request is being submitted (describe the issue or feature request).
- Describe what the pull request is doing to achieve the “why”.

## Updates

- Please list any components `created` or `deleted`.
- Additions/Changes/Removals: Provide more information on additions, changes or removals of any new components/functions that might be useful to the team.

## Tasks

- Provide references to the related Jira tasks.
- [\[EF-XXXX\]](https://efuse.atlassian.net/browse/EF-XXXX)

## Test Plan

- How did you test the code? Did you create any Jest functions or test cases?
- Did you create any automated tests for this feature?

## Segment Tracking

- Provide any segment tracking events you added for this pull request.

## Documentation

- How did you document this code?
- Did you create a Jira Wiki?

## Additional Notes

- Note any dependencies for the pull request.
- Note any concerns or questions related to the pull request.
- Note any ‘next steps’ that need to be taken after the pull request is merged.

## Images

- Provide screenshots if the pull request updates any UI/UX (use the following template for original and updated).
- Add an image if it helps you describe the pull request.
