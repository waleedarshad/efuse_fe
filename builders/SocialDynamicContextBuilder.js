const buildAccountData = (linkedData, accountData) => {
  return {
    accountLinked: linkedData,
    accountInfo: {
      accountName: accountData?.accountName,
      supporterCount: accountData?.supporterCount,
      id: accountData?.id,
      supporterViews: accountData?.supporterViews,
      contentCount: accountData?.contentCount
    }
  };
};

export default buildAccountData;
