import buildAccountData from "./SocialDynamicContextBuilder";

describe("SocialPagePropsBuilder", () => {
  describe("buildTwitterData", () => {
    it("buildTwitterData returns the correct object when all data is populated", () => {
      const linkedData = false;

      const accountData = {
        accountName: "okBoomer",
        supporterCount: 12345
      };

      const expected = {
        accountLinked: false,
        accountInfo: {
          accountName: "okBoomer",
          supporterCount: 12345
        }
      };

      const actual = buildAccountData(linkedData, accountData);

      expect(actual).toEqual(expected);
    });

    it("buildAccountData returns the correct object when data is missing", () => {
      const linkedData = undefined;

      const accountData = {
        accountInfo: {}
      };

      const expected = {
        accountLinked: undefined,
        accountInfo: {
          accountName: undefined,
          supporterCount: undefined
        }
      };

      const actual = buildAccountData(linkedData, accountData);

      expect(actual).toEqual(expected);
    });
  });
});
