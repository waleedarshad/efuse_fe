import {
  TIMELINE_UPDATE_FLAG,
  SEND_MESSAGE,
  SET_FEED_HYPES,
  SET_FEED_USER_HYPES,
  BUMP_NOTIFICATION_COUNT,
  ONLINE_STATUS
} from "../store/actions/types";

// eslint-disable-next-line import/prefer-default-export
export const eventListeners = [
  TIMELINE_UPDATE_FLAG,
  SEND_MESSAGE,
  SET_FEED_HYPES,
  SET_FEED_USER_HYPES,
  BUMP_NOTIFICATION_COUNT,
  ONLINE_STATUS
].reduce((accum, msg) => {
  // eslint-disable-next-line no-param-reassign
  accum[msg] = msg;
  return accum;
}, {});
