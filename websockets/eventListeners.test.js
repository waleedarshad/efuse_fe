import { eventListeners } from "./eventListeners";

describe("eventListeners", () => {
  it("listens for SET_FEED_HYPES event", () => {
    expect(eventListeners.SET_FEED_HYPES).toEqual("SET_FEED_HYPES");
  });

  it("listens for SET_FEED_USER_HYPES event", () => {
    expect(eventListeners.SET_FEED_USER_HYPES).toEqual("SET_FEED_USER_HYPES");
  });
});
