import getConfig from "next/config";
import io from "socket.io-client";

import { eventListeners } from "./eventListeners";
import { getToken } from "../helpers/Auth0Helper";

let token = getToken();
token = token ? token.split(" ")[1] : token;

const { publicRuntimeConfig } = getConfig();
const { baseUrl } = publicRuntimeConfig;

const socket = io.connect(baseUrl, {
  query: { token },
  transports: ["websocket"]
});

export const initWebsocket = store => {
  Object.keys(eventListeners).forEach(type => {
    socket.off(type);
    socket.on(type, payload => store.dispatch({ type, payload }));
  });
};

export const emit = (type, payload) => socket.emit(type, payload);

export const getSocket = () => socket;
