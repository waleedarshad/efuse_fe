import { useMemo } from "react";
import getConfig from "next/config";
import {
  ApolloClient,
  InMemoryCache,
  HttpLink,
  from,
  Observable,
  ApolloLink,
  NormalizedCacheObject
} from "@apollo/client";
import { onError } from "@apollo/client/link/error";
import fetch from "node-fetch";

import { getToken, attemptToRefreshToken } from "../helpers/Auth0Helper";

const { publicRuntimeConfig } = getConfig();
const { defaultUrl } = publicRuntimeConfig;
let apolloClient: ApolloClient<NormalizedCacheObject>;

// eslint-disable-next-line no-unused-vars
const authMiddleware = (ssrToken = null, context) =>
  new ApolloLink((operation, forward) => {
    // add the authorization to the headers
    operation.setContext(({ headers = {} }) => ({
      headers: { ...headers, authorization: ssrToken || getToken(context) }
    }));

    return forward(operation);
  });

const errorLink = context => {
  // eslint-disable-next-line consistent-return
  return onError(({ graphQLErrors, networkError, operation, forward }) => {
    if (graphQLErrors) {
      // eslint-disable-next-line no-restricted-syntax
      for (const err of graphQLErrors) {
        switch (err.extensions.code) {
          case "UNAUTHENTICATED":
            // error code is set to UNAUTHENTICATED
            // when AuthenticationError thrown in resolver
            // Must return observable instead of promise
            // @ts-ignore
            return new Observable(async observer => {
              // refresh Token
              try {
                const session = await attemptToRefreshToken(context);

                // modify the operation context with a new token
                const oldHeaders = operation.getContext().headers;
                operation.setContext({ headers: { ...oldHeaders, authorization: session.token } });

                const subscriber = {
                  next: observer.next.bind(observer),
                  error: observer.error.bind(observer),
                  complete: observer.complete.bind(observer)
                };

                // retry the request, returning the new observable
                return forward(operation).subscribe(subscriber);
              } catch (error) {
                console.error("Error while refreshing token | graphql", error);
                return observer.error(error);
              }
            });
          default:
            console.error("default errors case");
        }
      }
    }
    if (networkError) {
      console.error(`[Network error]: ${networkError}`);
      // if you would also like to retry automatically on
      // network errors, we recommend that you use
      // @apollo/client/link/retry
    }
  });
};

const graphqlLink = new HttpLink({
  uri: defaultUrl ? `${defaultUrl}/graphql` : "http://localhost:5000/api/graphql",
  // @ts-ignore
  fetch
});

const createApolloClient = (ssrMode, ssrToken = null, context) => {
  const client = new ApolloClient({
    ssrMode,
    link: from([authMiddleware(ssrToken, context), errorLink(context), graphqlLink]),
    cache: new InMemoryCache()
  });
  return client;
};

export const initializeApollo = (initialState = null, ssrToken = null, context?, ssrMode = false) => {
  const _apolloClient = apolloClient || createApolloClient(ssrMode, ssrToken, context);

  if (initialState) {
    _apolloClient.cache.restore(initialState);
  }
  // For SSG and SSR always create a new Apollo Client
  if (typeof window === "undefined") return _apolloClient;
  // Create the Apollo Client once in the client
  if (!apolloClient) apolloClient = _apolloClient;

  return _apolloClient;
};

export const useApollo = initialState => {
  const store = useMemo(() => initializeApollo(initialState), [initialState]);
  return store;
};
