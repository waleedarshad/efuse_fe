import { addDecorator } from "@storybook/react";
import { withNextRouter } from "storybook-addon-next-router";
import "../static/styles/bootstrap.min.css";
import * as nextImage from "next/image";
import mockNextImage from "./mockNextImage";

addDecorator(
  withNextRouter({
    path: "/",
    asPath: "/",
    query: {},
    push() {}
  })
);

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" }
};

Object.defineProperty(nextImage, "default", mockNextImage);
