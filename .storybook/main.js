module.exports = {
  stories: ["../components/**/*.stories.@(js|jsx|ts|tsx)"],
  addons: [
    {
      name: "@storybook/preset-scss",
      options: {
        cssLoaderOptions: {
          modules: true
        }
      }
    },
    "@storybook/addon-links",
    "@storybook/addon-essentials"
  ]
};
