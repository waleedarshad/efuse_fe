export interface IClippyCommand {
  _id: string;
  active: boolean;
  command: string;
  kind: string;
  sortOrder: string;
  dateRange: string;
  allowedRoles: string[];
}
