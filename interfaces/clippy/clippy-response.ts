import { IClippyUser } from "./clippy-user";
import { IClippyCommand } from "./clippy-command";

export interface IClippyResponse {
  token: string;
  user: IClippyUser;
  commands: IClippyCommand[];
}
