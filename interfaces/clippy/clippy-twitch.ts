export interface IClippyTwitch {
  displayName: string;
  username: string;
  accountId: string;
}
