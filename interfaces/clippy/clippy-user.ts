import { IClippyTwitch } from "./clippy-twitch";

export interface IClippyUser {
  _id: string;
  username: string;
  name: string;
  twitch: IClippyTwitch;
}
