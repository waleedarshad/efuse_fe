import { ResourceOwnerKindEnum } from "../../enums";

export interface PermissionEntityBody {
  entityId: string;
  entityKind: ResourceOwnerKindEnum;
}
