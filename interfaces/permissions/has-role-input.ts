import { RoleValue } from "../../types/role-value.type";

export interface HasRoleInput {
  entityId?: string;
  roleIds?: string[];
  roleValues?: RoleValue[];
}
