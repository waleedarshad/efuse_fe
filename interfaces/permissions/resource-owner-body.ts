import { ResourceOwnerKindEnum } from "../../enums";

export interface ResourceOwnerBody {
  resourceOwner: string;
  resourceOwnerKind: ResourceOwnerKindEnum;
}
