import initPage from "../../components/hoc/initPage";
import Category from "../../components/Opportunities/Category/Category";

export default initPage(Category, "opportunities", ["opportunities", "createOpportunity"], "private", null, "index");
