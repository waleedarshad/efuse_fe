import initPage from "../../components/hoc/initPage";
import OpportunityWrapper from "../../components/Opportunities/OpportunityWrapper/OpportunityWrapper";

export default initPage(
  OpportunityWrapper,
  "opportunities",
  ["opportunities", "createOpportunity"],
  "private",
  null,
  "owned"
);
