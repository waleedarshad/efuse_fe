import fetch from "isomorphic-unfetch";
import getConfig from "next/config";
import jwt_decode from "jwt-decode";
import nextCookie from "next-cookies";
import initPage from "../../components/hoc/initPage";
import OpportunityDetail from "../../components/Opportunities/OpportunityDetail";

const { publicRuntimeConfig } = getConfig();
const { defaultUrl } = publicRuntimeConfig;

export default initPage(
  OpportunityDetail,
  "opportunities",
  ["opportunities"],
  "public",
  null,
  null,
  true,
  async context => {
    const { token } = nextCookie(context);
    const { id } = context.query;
    const user = token && jwt_decode(token);
    const url = `${defaultUrl}/public/opportunities/show/${id}`;
    // do not call this method if the user is logged in
    // we want to avoid the "/public" route if the user is logged in
    if (user?.id) {
      return;
    }
    const config = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json"
      }
    };
    const res = await fetch(url, config);
    const data = await res.json();
    return { opportunity: data.opportunity, ssrStatusCode: res.status };
  }
);
