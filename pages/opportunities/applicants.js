import initPage from "../../components/hoc/initPage";
import ApplicantsWrapper from "../../components/Opportunities/ApplicantsWrapper/ApplicantsWrapper";

export default initPage(ApplicantsWrapper, "applicants", ["applicants", "opportunities", "settings", "student"]);
