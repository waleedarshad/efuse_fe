import initPage from "../../components/hoc/initPage";
import OpportunityLanding from "../../components/Opportunities/OpportunityLanding/OpportunityLanding";

export default initPage(OpportunityLanding, "opportunities", ["opportunities"], null, null, "landing");
