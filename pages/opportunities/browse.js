import initPage from "../../components/hoc/initPage";
import OpportunitiesBrowse from "../../components/OpportunitiesV2/OpportunitiesBrowse";

export default initPage(OpportunitiesBrowse, "opportunities", ["opportunities"], null, null, null, true);
