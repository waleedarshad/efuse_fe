import initPage from "../../components/hoc/initPage";
import CreateOpportunityFlow from "../../components/Opportunities/CreateOpportunityFlow/CreateOpportunityFlow";

export default initPage(CreateOpportunityFlow, "createOpportunity", ["createOpportunity"]);
