import toLower from "lodash/toLower";
import OpportunityDetail from "../../components/Opportunities/OpportunityDetail";
import initPage from "../../components/hoc/initPage";

import efFetch from "../../store/utils/efFetch";

export default initPage(
  OpportunityDetail,
  "opportunities",
  ["opportunities"],
  "public",
  null,
  null,
  true,
  async context => {
    const { o } = context.query;

    try {
      const data = await efFetch(context, `/public/opportunities/byslug/${toLower(o)}`, { method: "GET" });
      const { body, response } = data;

      return { opportunity: body.opportunity, ssrStatusCode: response.status };
    } catch (error) {
      return { ssrStatusCode: error.status };
    }
  }
);
