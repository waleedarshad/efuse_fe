import React from "react";
import initPage from "../../../../components/hoc/initPage";
import FEATURE_FLAGS from "../../../../common/featureFlags";
import LeagueInvitePage from "../../../../components/Leagues/LeagueInvitePage/LeagueInvitePage";
import { LEAGUES_META_TITLE } from "../../../../components/Leagues/constants";
import { initializeApollo } from "../../../../config/apolloClient";
import {
  GET_ORG_FROM_INVITE_CODE,
  GetOrgFromInviteCodeResponse,
  GetOrgFromInviteCodeVars
} from "../../../../graphql/leagues/Invitation";

import {
  GET_LEAGUES_WITH_TEAMS_FOR_ORG,
  GetLeaguesWithTeamsForOrgData,
  GetLeaguesWithTeamsForOrgVars
} from "../../../../graphql/leagues/Leagues";

const getServerSideProps = async (context, token) => {
  const { query } = context;

  const apolloClient = initializeApollo(null, token, context, true);

  const getInvitingOrgResponse = await apolloClient.query<GetOrgFromInviteCodeResponse, GetOrgFromInviteCodeVars>({
    query: GET_ORG_FROM_INVITE_CODE,
    variables: { inviteCode: query.inviteCode }
  });

  const getLeaguesForOrg = await apolloClient.query<GetLeaguesWithTeamsForOrgData, GetLeaguesWithTeamsForOrgVars>({
    query: GET_LEAGUES_WITH_TEAMS_FOR_ORG,
    variables: { organizationId: getInvitingOrgResponse?.data?.getInviteCode?.organization._id }
  });

  return {
    inviteCode: query.inviteCode,
    invitingOrgLeagues: getLeaguesForOrg?.data?.getLeaguesByOrganization,
    invitingOrg: getInvitingOrgResponse?.data.getInviteCode?.organization
  };
};

const LeagueInvitePageWrapper = ({ pageProps }) => {
  const { inviteCode, invitingOrgLeagues, invitingOrg } = pageProps;

  return <LeagueInvitePage inviteCode={inviteCode} invitingOrgLeagues={invitingOrgLeagues} invitingOrg={invitingOrg} />;
};

export default initPage(
  LeagueInvitePageWrapper,
  "league",
  [],
  "private",
  null,
  null,
  null,
  getServerSideProps,
  FEATURE_FLAGS.LEAGUE_CREATION,
  LEAGUES_META_TITLE
);
