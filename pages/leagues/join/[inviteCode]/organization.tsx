import React from "react";
import initPage from "../../../../components/hoc/initPage";
import FEATURE_FLAGS from "../../../../common/featureFlags";
import LeagueInviteSelectOrgPage from "../../../../components/Leagues/LeagueInviteSelectOrgPage/LeagueInviteSelectOrgPage";
import { LEAGUES_META_TITLE } from "../../../../components/Leagues/constants";
import { initializeApollo } from "../../../../config/apolloClient";

import {
  GET_ORG_FROM_INVITE_CODE,
  GetOrgFromInviteCodeResponse,
  GetOrgFromInviteCodeVars
} from "../../../../graphql/leagues/Invitation";
import { VALIDATE_ORG_ID, ValidateOrgIdData, ValidateOrgIdVars } from "../../../../graphql/organizations/Organizations";

const getServerSideProps = async (context, token) => {
  const { query } = context;

  const apolloClient = initializeApollo(null, token, context, true);

  const getInvitingOrgResponse = await apolloClient.query<GetOrgFromInviteCodeResponse, GetOrgFromInviteCodeVars>({
    query: GET_ORG_FROM_INVITE_CODE,
    variables: { inviteCode: query.inviteCode },
    fetchPolicy: "no-cache"
  });

  const pageProps = {
    inviteCode: query.inviteCode,
    invitingOrg: getInvitingOrgResponse?.data.getInviteCode.organization
  };

  if (query.new_org_id) {
    const response = await apolloClient.query<ValidateOrgIdData, ValidateOrgIdVars>({
      query: VALIDATE_ORG_ID,
      variables: { orgId: query.new_org_id },
      fetchPolicy: "no-cache"
    });
    return {
      ...pageProps,
      newOrgId: response?.data.getOrganizationById._id
    };
  }

  return pageProps;
};

const LeagueInviteSelectOrgWrapper = ({ pageProps }) => {
  const { inviteCode, invitingOrg, newOrgId } = pageProps;

  return <LeagueInviteSelectOrgPage inviteCode={inviteCode} invitingOrg={invitingOrg} newOrgId={newOrgId} />;
};

export default initPage(
  LeagueInviteSelectOrgWrapper,
  "league",
  [],
  "private",
  null,
  null,
  null,
  getServerSideProps,
  FEATURE_FLAGS.LEAGUE_CREATION,
  LEAGUES_META_TITLE
);
