import Router from "next/router";
import React from "react";
import FEATURE_FLAGS from "../../../../common/featureFlags";
import initPage from "../../../../components/hoc/initPage";
import { LEAGUES_META_TITLE } from "../../../../components/Leagues/constants";
import { initializeApollo } from "../../../../config/apolloClient";
import EventDashboardPage from "../../../../components/Leagues/Pages/EventDashboardPage/EventDashboardPage";
import { EVENT_DASHBOARD_TABS } from "../../../../components/Leagues/Pages/EventDashboardPage/constants";
import {
  GET_LEAGUE_EVENT_BY_ID,
  GetLeaguesEventByIdData,
  GetLeaguesEventByIdVars
} from "../../../../graphql/leagues/LeagueEvents";
import { GET_USERS_LEAGUE_ORGS_AND_ROLES } from "../../../../graphql/leagues/Leagues";

const getServerSideProps = async (context, token) => {
  const { query, req, res } = context;

  if (typeof query?.tab === "string" && EVENT_DASHBOARD_TABS.includes(query.tab)) {
    const apolloClient = initializeApollo(null, token, context, true);

    const response = await apolloClient.query<GetLeaguesEventByIdData, GetLeaguesEventByIdVars>({
      query: GET_LEAGUE_EVENT_BY_ID,
      variables: { eventId: query.eventId }
    });

    const userLeagueOrgRolesResponse = await apolloClient.query({
      query: GET_USERS_LEAGUE_ORGS_AND_ROLES
    });

    const event = response?.data?.getLeagueEventById;
    const userLeagueOrgRoles = userLeagueOrgRolesResponse?.data?.getUsersLeagueOrgsAndRoles;

    const userOrgRole = userLeagueOrgRoles?.organizationRoles?.find(
      role => role.organization._id === event?.league?.owner?._id
    );

    const userHasAdminPrivileges = userOrgRole?.role === "owner" || userOrgRole?.role === "captain";

    return {
      event,
      userLeagueOrgRoles,
      userHasAdminPrivileges
    };
  }

  const redirectPath = `/leagues/event/${query.eventId}?tab=overview`;

  if (req) {
    res.writeHead(302, { Location: redirectPath });
    res.end();
  } else {
    await Router.push(redirectPath);
  }
  return {};
};

const EventDashboardPageWrapper = ({ pageProps }) => <EventDashboardPage {...pageProps} />;

export default initPage(
  EventDashboardPageWrapper,
  "common",
  [],
  "private",
  null,
  null,
  false,
  getServerSideProps,
  FEATURE_FLAGS.LEAGUE_CREATION,
  LEAGUES_META_TITLE
);
