import React from "react";
import FEATURE_FLAGS from "../../../common/featureFlags";
import initPage from "../../../components/hoc/initPage";
import LeagueDashboard from "../../../components/Leagues/LeagueDashboard/LeagueDashboard";
import { LEAGUES_META_TITLE } from "../../../components/Leagues/constants";
import { initializeApollo } from "../../../config/apolloClient";
import {
  CAN_USER_CREATE_LEAGUES_FOR_ORG,
  CanUserCreateLeaguesForOrgData,
  CanUserCreateLeaguesForOrgVars,
  GET_USERS_LEAGUE_ORGS_AND_ROLES
} from "../../../graphql/leagues/Leagues";
import Error from "../../_error";
import {
  GET_LEAGUES_BY_ORG,
  GET_ORG_INFO_BY_ID,
  GetLeaguesByOrgData,
  GetLeaguesByOrgVars,
  GetOrgInfoByIdData,
  GetOrgInfoByIdVars
} from "../../../components/Leagues/LeagueDashboard/dashboardQueries";
import { OrganizationUserRole } from "../../../graphql/interface/Organization";

const getServerSideProps = async (context, token) => {
  const { query } = context;

  const apolloClient = initializeApollo(null, token, context, true);

  try {
    const leaguesResponse = await apolloClient.query<GetLeaguesByOrgData, GetLeaguesByOrgVars>({
      query: GET_LEAGUES_BY_ORG,
      variables: { organizationId: query?.orgId }
    });

    const orgInfoResponse = await apolloClient.query<GetOrgInfoByIdData, GetOrgInfoByIdVars>({
      query: GET_ORG_INFO_BY_ID,
      variables: { organizationId: query?.orgId }
    });

    const canUserCreateLeagues = await apolloClient.query<
      CanUserCreateLeaguesForOrgData,
      CanUserCreateLeaguesForOrgVars
    >({
      query: CAN_USER_CREATE_LEAGUES_FOR_ORG,
      variables: { organizationId: query?.orgId }
    });

    const userLeagueOrgRolesResponse = await apolloClient.query({ query: GET_USERS_LEAGUE_ORGS_AND_ROLES });

    return {
      orgId: query.orgId,
      leagues: leaguesResponse?.data?.getLeaguesByOrganization,
      organization: orgInfoResponse?.data?.getOrganizationById,
      userLeagueOrgRoles: userLeagueOrgRolesResponse?.data.getUsersLeagueOrgsAndRoles,
      canCreateLeagues: canUserCreateLeagues?.data?.canUserCreateLeaguesForOrg,
      hasOrgPermissions: [OrganizationUserRole.CAPTAIN, OrganizationUserRole.OWNER].includes(
        orgInfoResponse?.data?.getOrganizationById?.currentUserRole
      )
    };
  } catch (error) {
    return { error };
  }
};

const LeagueDashboardWrapper = ({ pageProps }) => {
  if (pageProps.error) {
    console.error(pageProps.error);

    return <Error statusCode={404} />;
  }
  return <LeagueDashboard {...pageProps} />;
};

export default initPage(
  LeagueDashboardWrapper,
  "league",
  [],
  "private",
  null,
  null,
  null,
  getServerSideProps,
  FEATURE_FLAGS.LEAGUE_CREATION,
  LEAGUES_META_TITLE
);
