import React from "react";
import initPage from "../../../../components/hoc/initPage";
import FEATURE_FLAGS from "../../../../common/featureFlags";
import { initializeApollo } from "../../../../config/apolloClient";
import {
  GET_ORG_INVITE_INFO,
  GetOrgInfoByIdVars,
  GetOrgInfoByIdData
} from "../../../../graphql/organizations/Organizations";
import InviteOrgs from "../../../../components/Leagues/Invite/InviteOrgs";
import { LEAGUES_META_TITLE } from "../../../../components/Leagues/constants";

const getServerSideProps = async (context, token) => {
  const { query } = context;

  const apolloClient = initializeApollo(null, token, context, true);

  const response = await apolloClient.query<GetOrgInfoByIdData, GetOrgInfoByIdVars>({
    query: GET_ORG_INVITE_INFO,
    variables: { organizationId: query.orgId }
  });

  return {
    orgImage: response?.data?.getOrganizationById.profileImage?.url,
    inviteCode: response?.data?.getOrganizationById?.leaguesInviteCode,
    orgId: response?.data?.getOrganizationById?._id
  };
};

const InviteOrgsWrapper = ({ pageProps }) => (
  <InviteOrgs orgImage={pageProps.orgImage} inviteCode={pageProps.inviteCode} orgId={pageProps.orgId} />
);

export default initPage(
  InviteOrgsWrapper,
  "league",
  [],
  "private",
  null,
  null,
  null,
  getServerSideProps,
  FEATURE_FLAGS.LEAGUE_CREATION,
  LEAGUES_META_TITLE
);
