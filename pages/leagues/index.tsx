import React from "react";
import initPage from "../../components/hoc/initPage";
import FEATURE_FLAGS from "../../common/featureFlags";
import { LEAGUES_META_TITLE } from "../../components/Leagues/constants";

const PlaceholderIndex = () => <div>Placeholder for League homepage.</div>;

export default initPage(
  PlaceholderIndex,
  "league",
  [],
  "private",
  null,
  null,
  null,
  null,
  FEATURE_FLAGS.LEAGUE_CREATION,
  LEAGUES_META_TITLE
);
