import React from "react";
import initPage from "../../../components/hoc/initPage";
import FEATURE_FLAGS from "../../../common/featureFlags";
import { LEAGUES_META_TITLE } from "../../../components/Leagues/constants";
import { initializeApollo } from "../../../config/apolloClient";
import {
  GetLeagueByIdData,
  GetLeagueByIdVars,
  GET_LEAGUE_BY_ID,
  GET_USERS_LEAGUE_ORGS_AND_ROLES
} from "../../../graphql/leagues/Leagues";
import LeagueIndex from "../../../components/Leagues/LeagueIndex/LeagueIndex";
import Error from "../../_error";

const getServerSideProps = async (context, token) => {
  const { query } = context;

  const apolloClient = initializeApollo(null, token, context, true);

  try {
    const leagueResponse = await apolloClient.query<GetLeagueByIdData, GetLeagueByIdVars>({
      query: GET_LEAGUE_BY_ID,
      variables: { leagueId: query.leagueId }
    });

    const userLeagueOrgRolesResponse = await apolloClient.query({
      query: GET_USERS_LEAGUE_ORGS_AND_ROLES
    });

    const userLeagueOrgRoles = userLeagueOrgRolesResponse?.data.getUsersLeagueOrgsAndRoles;
    const events = leagueResponse?.data?.getLeagueById?.events;
    const league = leagueResponse?.data?.getLeagueById;

    const userOrgRole = userLeagueOrgRoles.organizationRoles.find(
      role => role.organization._id === leagueResponse?.data?.getLeagueById?.owner?._id
    );
    const userHasAdminPrivileges = userOrgRole?.role === "owner" || userOrgRole?.role === "captain";

    return {
      league,
      events,
      userLeagueOrgRoles,
      userHasAdminPrivileges
    };
  } catch (error) {
    return { error };
  }
};

const IndexWrapper = ({ pageProps }) => {
  if (pageProps.error) {
    console.error(pageProps.error);
    return <Error statusCode={404} error={pageProps.error} />;
  }
  return <LeagueIndex {...pageProps} />;
};

export default initPage(
  IndexWrapper,
  "league",
  [],
  "private",
  null,
  null,
  null,
  getServerSideProps,
  FEATURE_FLAGS.LEAGUE_CREATION,
  LEAGUES_META_TITLE
);
