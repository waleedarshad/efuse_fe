import React from "react";
import initPage from "../../../../components/hoc/initPage";
import FEATURE_FLAGS from "../../../../common/featureFlags";
import CreateLeagueEventFlow from "../../../../components/Leagues/CreateEvent/CreateLeagueEventFlow";
import { initializeApollo } from "../../../../config/apolloClient";
import { GET_LEAGUE_BY_ID, GetLeagueByIdData, GetLeagueByIdVars } from "../../../../graphql/leagues/Leagues";
import { LEAGUES_META_TITLE } from "../../../../components/Leagues/constants";

const getServerSideProps = async (context, token) => {
  const { query } = context;

  const apolloClient = initializeApollo(null, token, context, true);

  const response = await apolloClient.query<GetLeagueByIdData, GetLeagueByIdVars>({
    query: GET_LEAGUE_BY_ID,
    variables: { leagueId: query.leagueId }
  });

  return { league: response?.data?.getLeagueById };
};

const CreateLeagueEventWrapper = ({ pageProps }) => <CreateLeagueEventFlow league={pageProps?.league} />;

export default initPage(
  CreateLeagueEventWrapper,
  "league",
  [],
  "private",
  null,
  null,
  null,
  getServerSideProps,
  FEATURE_FLAGS.LEAGUE_CREATION,
  LEAGUES_META_TITLE
);
