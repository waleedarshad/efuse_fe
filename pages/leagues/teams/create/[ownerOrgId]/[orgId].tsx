import React from "react";
import initPage from "../../../../../components/hoc/initPage";
import FEATURE_FLAGS from "../../../../../common/featureFlags";
import CreateTeam from "../../../../../components/Leagues/CreateTeam/CreateTeam";
import { initializeApollo } from "../../../../../config/apolloClient";
import { GET_LEAGUES_BY_ORG, GetLeaguesByOrgData, GetLeaguesByOrgVars } from "../../../../../graphql/leagues/Leagues";
import { LEAGUES_META_TITLE } from "../../../../../components/Leagues/constants";

const getServerSideProps = async (context, token) => {
  const { query } = context;

  const apolloClient = initializeApollo(null, token, context, true);

  const response = await apolloClient.query<GetLeaguesByOrgData, GetLeaguesByOrgVars>({
    query: GET_LEAGUES_BY_ORG,
    variables: { organizationId: query.ownerOrgId },
    fetchPolicy: "no-cache"
  });

  return { orgId: query.orgId, ownerOrgId: query.ownerOrgId, leagues: response?.data?.getLeaguesByOrganization };
};

const CreateTeamWrapper = ({ pageProps }) => <CreateTeam {...pageProps} />;

export default initPage(
  CreateTeamWrapper,
  "league",
  [],
  "private",
  null,
  null,
  null,
  getServerSideProps,
  FEATURE_FLAGS.LEAGUE_CREATION,
  LEAGUES_META_TITLE
);
