import React from "react";
import initPage from "../../../../../components/hoc/initPage";
import FEATURE_FLAGS from "../../../../../common/featureFlags";
import InviteTeamMembers from "../../../../../components/Leagues/Invite/InviteTeamMembers";
import { LEAGUES_META_TITLE } from "../../../../../components/Leagues/constants";
import { initializeApollo } from "../../../../../config/apolloClient";
import { GET_TEAM_BY_ID, GetTeamByIdData, GetTeamByIdVars } from "../../../../../graphql/Team/LeagueTeam";

const getServerSideProps = async (context, token) => {
  const { query } = context;

  const apolloClient = initializeApollo(null, token, context, true);

  const response = await apolloClient.query<GetTeamByIdData, GetTeamByIdVars>({
    query: GET_TEAM_BY_ID,
    variables: { teamId: query.teamId }
  });

  return {
    orgId: response.data.getTeamById.owner._id,
    ownerOrgId: query.ownerOrgId,
    inviteCode: response.data.getTeamById.inviteCode,
    gameImage: response.data.getTeamById.game.gameImage?.url
  };
};

const InviteTeamMembersWrapper = ({ pageProps }) => <InviteTeamMembers {...pageProps} />;

export default initPage(
  InviteTeamMembersWrapper,
  "league",
  [],
  "private",
  null,
  null,
  null,
  getServerSideProps,
  FEATURE_FLAGS.LEAGUE_CREATION,
  LEAGUES_META_TITLE
);
