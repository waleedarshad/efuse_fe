import React from "react";
import initPage from "../../../components/hoc/initPage";
import FEATURE_FLAGS from "../../../common/featureFlags";
import CreateLeagueFlow from "../../../components/Leagues/CreateLeague/CreateLeagueFlow";
import { initializeApollo } from "../../../config/apolloClient";

import { VALIDATE_ORG_ID, ValidateOrgIdData, ValidateOrgIdVars } from "../../../graphql/organizations/Organizations";
import { LEAGUES_META_TITLE } from "../../../components/Leagues/constants";
import Error from "../../_error";
import {
  CAN_USER_CREATE_LEAGUES_FOR_ORG,
  CanUserCreateLeaguesForOrgData,
  CanUserCreateLeaguesForOrgVars
} from "../../../graphql/leagues/Leagues";

const FORBIDDEN_ERROR_CODE = 403;
const INTERNAL_ERROR_CODE = 500;

const getServerSideProps = async (context, token) => {
  const apolloClient = initializeApollo(null, token, context, true);

  try {
    const canUserCreateLeaguesForOrgResponse = await apolloClient.query<
      CanUserCreateLeaguesForOrgData,
      CanUserCreateLeaguesForOrgVars
    >({
      query: CAN_USER_CREATE_LEAGUES_FOR_ORG,
      variables: { organizationId: context?.query?.orgId }
    });

    if (!canUserCreateLeaguesForOrgResponse.data.canUserCreateLeaguesForOrg) {
      return { error: { statusCode: FORBIDDEN_ERROR_CODE } };
    }

    const response = await apolloClient.query<ValidateOrgIdData, ValidateOrgIdVars>({
      query: VALIDATE_ORG_ID,
      variables: { orgId: context?.query?.orgId }
    });

    return { orgId: response?.data?.getOrganizationById._id };
  } catch (error) {
    return { error };
  }
};

const CreateLeagueWrapper = ({ pageProps }) => {
  if (pageProps.error) {
    const statusCode = pageProps.error.statusCode ?? INTERNAL_ERROR_CODE;

    console.error(pageProps.error);

    return <Error statusCode={statusCode} />;
  }
  return <CreateLeagueFlow {...pageProps} />;
};

export default initPage(
  CreateLeagueWrapper,
  "league",
  [],
  "private",
  null,
  null,
  null,
  getServerSideProps,
  FEATURE_FLAGS.LEAGUE_CREATION,
  LEAGUES_META_TITLE
);
