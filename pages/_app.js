import { ApolloProvider } from "@apollo/client";
import Cookies from "js-cookie";
import isEmpty from "lodash/isEmpty";
import nextCookie from "next-cookies";
import { DefaultSeo } from "next-seo";
import App from "next/app";
import getConfig from "next/config";
import dynamic from "next/dynamic";
import Router, { withRouter } from "next/router";
import React from "react";
import { GoogleReCaptchaProvider } from "react-google-recaptcha-v3";
import { Provider } from "react-redux";
import dayjs from "dayjs";
import FEATURE_FLAGS from "../common/featureFlags";
import EFApplyTheme from "../components/EFApplyTheme/EFApplyTheme";
import { initializeApollo } from "../config/apolloClient";
import { isUserLoggedIn } from "../helpers/AuthHelper";
import { initializeFlagSmith } from "../helpers/FlagSmithHelper";
import { initSegment } from "../helpers/SegmentHelper";
import SEOConfig from "../next-seo.config";
import { verifyLogin } from "../store/actions/globalAppLevelActions/authGlobalActions";
import { getUnreadMessageCount } from "../store/actions/globalAppLevelActions/streamChatGlobalActions";
import { changeToDarkTheme } from "../store/actions/themeActions";
import store from "../store/store";
import setAxiosDefaults from "../store/utils/setAxiosDefaults";

const { publicRuntimeConfig } = getConfig();
const { googleRecaptcha3Key } = publicRuntimeConfig;

setAxiosDefaults();

const ReactNotification = dynamic(() => import("react-notifications-component"));
const SignupModal = dynamic(() => import("../components/DynamicModal/DynamicModal"));
const ShareLinkModal = dynamic(() => import("../components/ShareLinkModal/ShareLinkModal"));
const DemoButton = dynamic(() => import("../components/DemoButton/DemoButton"));
const GlobalVideo = dynamic(() => import("../components/GlobalVideo/GlobalVideo"));

class Efuse extends App {
  previousPage;

  timeOnPage;

  timeouts = [];

  static async getInitialProps({ Component, ctx }) {
    let pageProps = {};
    const { demoState, theme } = nextCookie(ctx);

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    return { pageProps, demoState, theme };
  }

  componentDidMount() {
    const { pageProps, demoState, theme } = this.props;
    this.timeOnPage = dayjs();

    // check and set theme for app
    if (theme === "dark") {
      store.dispatch(changeToDarkTheme());
    } else {
      Cookies.set("theme", "light");
    }

    store.dispatch(verifyLogin());
    this.initializeWebsockets();
    initializeFlagSmith(store, pageProps?.flagSmithDataSSR, demoState).catch(error =>
      console.error("Error Initializing Flagsmith", error)
    );
    initSegment(store);
    this.handleFirstUrlChange();
    Router.events.on("routeChangeStart", this.handleRouteChange);

    // request push notification permission with braze
    analytics.ready(() => {
      window.appboy.registerAppboyPushMessages();
    });
  }

  componentDidUpdate() {
    const { demoState } = this.props;
    this.initializeWebsockets();
    initializeFlagSmith(store, null, demoState).catch(error => console.error("Error Initializing Flagsmith", error));
  }

  initializeWebsockets = () => {
    if (Cookies.get("token")) {
      // eslint-disable-next-line global-require
      const { initWebsocket } = require("../websockets");

      initWebsocket(store);
    }
  };

  handleRouteChange = url => {
    this.emitPageView({
      url,
      previousPage: this.previousPage
    });
    this.fetchUnreadMessageCount();
  };

  handleFirstUrlChange = () => {
    this.emitPageView({ url: window.location.pathname });
    this.fetchUnreadMessageCount();
  };

  fetchUnreadMessageCount = () => {
    const enableStreamChatNotificationWeb = store.getState().features[FEATURE_FLAGS.STREAM_CHAT_NOTIFICATION_WEB];
    const currUser = store.getState().auth.currentUser;

    if (enableStreamChatNotificationWeb && !isEmpty(currUser) && Router.router.asPath !== "/messages") {
      store.dispatch(getUnreadMessageCount(currUser));
    }
  };

  emitPageView = pageView => {
    if (this.previousPage) {
      const timeOnPrevPage = dayjs().diff(this.timeOnPage, "seconds", true);
      analytics.page(this.previousPage, {
        durationSeconds: timeOnPrevPage
      });
    }
    this.previousPage = pageView.url;
    this.timeOnPage = dayjs();
  };

  componentWillUnmount() {
    this.timeouts.forEach(timeout => clearTimeout(timeout));
    Router.events.off("routeChangeStart", this.handleRouteChange);
  }

  render() {
    const { Component, pageProps, router } = this.props;
    const apolloClient = initializeApollo(pageProps.initialApolloState);

    /* eslint-disable react/jsx-props-no-spreading */
    return (
      <ApolloProvider client={apolloClient}>
        <Provider store={store}>
          <EFApplyTheme>
            <GoogleReCaptchaProvider
              reCaptchaKey={googleRecaptcha3Key}
              scriptProps={{
                defer: true,
                async: true
              }}
            >
              <DefaultSeo {...SEOConfig} />
              <div id="modal-root" />
              <ReactNotification />
              <GlobalVideo />
              <Component {...pageProps} />
              <DemoButton />
              <ShareLinkModal />
              {router.query.promptSignup && !isUserLoggedIn() && (
                <SignupModal
                  flow="LoginSignup"
                  openOnLoad
                  startView="signup"
                  displayCloseButton
                  allowBackgroundClickClose={false}
                  onCloseModal={() => {
                    router.push(router.asPath.split("?")[0]);
                  }}
                />
              )}
            </GoogleReCaptchaProvider>
          </EFApplyTheme>
        </Provider>
      </ApolloProvider>
    );
    /* eslint-enable react/jsx-props-no-spreading */
  }
}

export default withRouter(Efuse);
