import initPage from "../../components/hoc/initPage";
import ClippyLandingPage from "../../components/ClippyLandingPage/ClippyLandingPage";

export default initPage(ClippyLandingPage, "landing", ["landing"], "public", null, null, true);
