import initPage from "../../components/hoc/initPage";
import ScheduledPostsPage from "../../components/ClippyConfigPage/pages/ScheduledPostsPage";

export default initPage(ScheduledPostsPage);
