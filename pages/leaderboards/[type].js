import initPage from "../../components/hoc/initPage";
import FullLeaderboard from "../../components/Leaderboards/FullLeaderboard/FullLeaderboard";

export default initPage(FullLeaderboard, "leaderboards", ["leaderboards"], "private", null, "index");
