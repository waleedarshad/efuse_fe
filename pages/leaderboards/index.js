import initPage from "../../components/hoc/initPage";
import Leaderboards from "../../components/Leaderboards/AllLeaderBoards/AllLeaderBoards";

export default initPage(Leaderboards, "leaderboards", ["leaderboards"], "private", null, "index");
