import initPage from "../../components/hoc/initPage";
import profileLayout from "../../components/hoc/profileLayout";
import Followees from "../../components/User/Followees/Followees";

export default initPage(Followees, "common", ["common"], "private", profileLayout);
