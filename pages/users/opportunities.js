import initPage from "../../components/hoc/initPage";
import Opportunities from "../../components/User/Opportunities/Opportunities";

export default initPage(Opportunities, "opportunities", ["opportunities", "createOpportunity"]);
