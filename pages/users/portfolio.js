import initPage from "../../components/hoc/initPage";
import profileLayout from "../../components/hoc/profileLayout";
import Portfolio from "../../components/User/Portfolio/Portfolio";

export default initPage(Portfolio, "common", ["common"], "public", profileLayout, null, true);
