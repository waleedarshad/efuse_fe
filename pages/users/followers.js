import initPage from "../../components/hoc/initPage";
import Followers from "../../components/User/Followers/Followers";
import profileLayout from "../../components/hoc/profileLayout";

export default initPage(Followers, "common", ["common"], "private", profileLayout);
