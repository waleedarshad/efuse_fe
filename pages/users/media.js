import initPage from "../../components/hoc/initPage";
import Media from "../../components/User/Media/Media";
import profileLayout from "../../components/hoc/profileLayout";

export default initPage(Media, "common", ["common"], "private", profileLayout);
