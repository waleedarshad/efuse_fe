import React from "react";
import initPage from "../../components/hoc/initPage";
import profileLayout from "../../components/hoc/profileLayout";
import UserFeed from "../../components/User/UserFeed";

const PostsWrapper = props => <UserFeed {...props} />;

export default initPage(
  PostsWrapper,
  "homeFeed",
  ["homeFeed", "opportunityDetail"],
  "private",
  profileLayout,
  null,
  false
);
