import { Component } from "react";
import { connect } from "react-redux";
import Router from "next/router";
import { logoutUser } from "../store/actions/authActions";

class Logout extends Component {
  componentDidMount() {
    this.props.logoutUser();
    Router.push("/error", "/login");
  }

  render() {
    return null;
  }
}
export default connect(null, { logoutUser })(Logout);
