import initPage from "../../components/hoc/initPage";
import MediaSettings from "../../components/Settings/MediaSettings/MediaSettings";

export default initPage(MediaSettings, "settings", ["settings"]);
