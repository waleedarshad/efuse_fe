import initPage from "../../../components/hoc/initPage";
import BlockedUsers from "../../../components/Settings/BlockedUsers/BlockedUsers";

export default initPage(BlockedUsers, "settings", ["settings"]);
