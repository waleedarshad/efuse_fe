import initPage from "../../components/hoc/initPage";
import GeneralAccountSettings from "../../components/Settings/GeneralAccountSettings";

export default initPage(GeneralAccountSettings, "settings", ["settings", "signup", "obProfile"]);
