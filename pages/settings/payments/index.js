import initPage from "../../../components/hoc/initPage";
import PaymentSettingsPage from "../../../components/Settings/PaymentSettingsPage/PaymentSettingsPage";

export default initPage(PaymentSettingsPage, "settings", ["settings"]);
