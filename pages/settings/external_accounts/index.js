import initPage from "../../../components/hoc/initPage";
import ExternalAccounts from "../../../components/Settings/ExternalAccounts";

export default initPage(ExternalAccounts, "settings", ["settings"]);
