import initPage from "../../../components/hoc/initPage";
import DiscordAuth from "../../../components/Settings/DiscordAuth";

export default initPage(DiscordAuth, "settings", ["settings"]);
