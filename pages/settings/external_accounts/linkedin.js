import initPage from "../../../components/hoc/initPage";
import LinkedinAuth from "../../../components/Settings/LinkedinAuth/LinkedinAuth";

export default initPage(LinkedinAuth, "settings", ["settings"]);
