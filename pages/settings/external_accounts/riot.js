import { useRouter } from "next/router";
import { useEffect } from "react";

const RiotCallback = () => {
  const router = useRouter();
  const { query } = router;

  const callback = (status = "success") => {
    if (window.opener) {
      window.opener?.parent?.postMessage({ riot: true, status }, "*");
    }
    window.close();
  };

  useEffect(() => {
    callback(query.token ? "success" : "failure");
  }, []);

  return <div>Authenticating...</div>;
};

export default RiotCallback;
