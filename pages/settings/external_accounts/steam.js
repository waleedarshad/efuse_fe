import initPage from "../../../components/hoc/initPage";
import SteamAuth from "../../../components/Settings/SteamAuth/SteamAuth";

export default initPage(SteamAuth, "settings", ["settings"]);
