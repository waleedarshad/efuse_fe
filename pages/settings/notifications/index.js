import initPage from "../../../components/hoc/initPage";
import Notifications from "../../../components/Settings/Notifications/Notifications";

export default initPage(Notifications, "settings", ["settings"]);
