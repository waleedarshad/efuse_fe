import initPage from "../../../components/hoc/initPage";
import Badges from "../../../components/Settings/Badges";

export default initPage(Badges, "settings", ["settings"]);
