import initPage from "../../../components/hoc/initPage";
import RecruitingPage from "../../../components/Settings/Recruiting/Recruiting";

export default initPage(RecruitingPage, "settings", ["settings"]);
