import withGraphql from "../../../components/hoc/withGraphql";
import initPage from "../../../components/hoc/initPage";
import PartnerPage from "../../../components/Settings/PartnerPage/PartnerPage";

export default withGraphql({ ssr: true })(initPage(PartnerPage, "settings", ["settings"]));
