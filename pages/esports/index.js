import initPage from "../../components/hoc/initPage";
import EsportsLandingWrapper from "../../components/EsportsNews/EsportsLandingWrapper/EsportsLandingWrapper";
import { initializeApollo } from "../../config/apolloClient";
import { GET_ERENA_TOURNAMENTS } from "../../graphql/ERenaTournamentQuery";
import GET_ERENA_FEATURED_VIDEO from "../../graphql/ERenaFeaturedVideoQuery";
import GET_ARTICLE_CATEGORIES from "../../graphql/NewsArticlesCategoryQuery";

const getServerSideProps = async () => {
  const apolloClient = initializeApollo();
  const currentTime = Math.round(new Date().getTime() / 1000);

  const liveAndScheduledTournaments = (
    await apolloClient.query({
      query: GET_ERENA_TOURNAMENTS,
      variables: {
        page: 1,
        limit: 10,
        status: ["Active", "Scheduled"],
        endDateMin: currentTime,
        sortBy: "startDatetime"
      }
    })
  ).data.listERenaTournaments;

  const newsCategories = (
    await apolloClient.query({
      query: GET_ARTICLE_CATEGORIES
    })
  ).data.getCategories;

  const erenaFeaturedVideos = (await apolloClient.query({ query: GET_ERENA_FEATURED_VIDEO })).data
    .getERenaFeaturedVideos;

  return { liveAndScheduledTournaments, newsCategories, erenaFeaturedVideos };
};

export default initPage(EsportsLandingWrapper, "esports", ["esports"], null, null, null, true, getServerSideProps);
