import initPage from "../../components/hoc/initPage";
import RecruitingList from "../../components/Recruiting/RecruitingList/RecruitingList";

export default initPage(RecruitingList, "recruiting", ["recruiting"]);
