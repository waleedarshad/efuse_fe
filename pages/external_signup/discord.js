import initPage from "../../components/hoc/initPage";
import DiscordAuth from "../../components/ExternalAuth/DiscordAuth";

export default initPage(DiscordAuth, "ExternalAuth", ["ExternalAuth"], "public", null, null, true);
