import initPage from "../../components/hoc/initPage";
import TwitchAuth from "../../components/ExternalAuth/TwitchAuth";

export default initPage(TwitchAuth, "ExternalAuth", ["ExternalAuth"], "public", null, null, true);
