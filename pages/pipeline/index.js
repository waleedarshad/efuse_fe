import initPage from "../../components/hoc/initPage";
import PipelineLandingPage from "../../components/Recruiting/PipelineLandingPage";
import withGraphql from "../../components/hoc/withGraphql";
import { GET_PAGINATED_LEAGUE_OF_LEGENDS_LEADERBOARD } from "../../graphql/LeaderboardQuery";
import { initializeApollo } from "../../config/apolloClient";

const getServerSideProps = async () => {
  const apolloClient = initializeApollo();

  const { loading, error, data } = await apolloClient.query({
    query: GET_PAGINATED_LEAGUE_OF_LEGENDS_LEADERBOARD,
    variables: { page: 1, limit: 10 }
  });

  return { loading, error, data: data.getPaginatedLeagueOfLegendsLeaderboard };
};

export default withGraphql({ ssr: true })(
  initPage(PipelineLandingPage, "pipeline", ["pipeline"], null, null, null, true, getServerSideProps)
);
