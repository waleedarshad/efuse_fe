import initPage from "../../components/hoc/initPage";
import PipelineLandingPage from "../../components/Recruiting/PipelineLandingPage";
import withGraphql from "../../components/hoc/withGraphql";
import {
  GET_PAGINATED_AIMLAB_LEADERBOARD,
  GET_PAGINATED_VALORANT_LEADERBOARD,
  GET_PAGINATED_LEAGUE_OF_LEGENDS_LEADERBOARD
} from "../../graphql/LeaderboardQuery";
import { initializeApollo } from "../../config/apolloClient";

const getServerSideProps = async context => {
  const apolloClient = initializeApollo();
  const page = 1;
  const limit = 10;

  if (context.query.game === "league-of-legends") {
    const { loading, error, data } = await apolloClient.query({
      query: GET_PAGINATED_LEAGUE_OF_LEGENDS_LEADERBOARD,
      variables: { page, limit }
    });

    return { loading, error, data: data.getPaginatedLeagueOfLegendsLeaderboard };
  } else if (context.query.game === "valorant") {
    const { loading, error, data } = await apolloClient.query({
      query: GET_PAGINATED_VALORANT_LEADERBOARD,
      variables: { page, limit }
    });

    return { loading, error, data: data?.getPaginatedValorantLeaderboard };
  } else if (context.query.game === "aimlab") {
    const { loading, error, data } = await apolloClient.query({
      query: GET_PAGINATED_AIMLAB_LEADERBOARD,
      variables: { page, limit }
    });

    return { loading, error, data: data?.getPaginatedAimlabLeaderboard };
  }
};

export default withGraphql({ ssr: true })(
  initPage(PipelineLandingPage, "pipeline", ["pipeline"], null, null, null, true, getServerSideProps)
);
