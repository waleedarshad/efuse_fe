import initPage from "../../components/hoc/initPage";
import Inbox from "../../components/Messages/Inbox/Inbox";

export default initPage(Inbox, "messenger", ["messenger"]);
