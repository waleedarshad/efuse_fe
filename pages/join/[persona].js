import initPage from "../../components/hoc/initPage";
import Persona from "../../components/Join/Persona/Persona";
import efFetch from "../../store/utils/efFetch";

export default initPage(Persona, "persona", ["persona"], "public", null, null, false, async context => {
  try {
    const data = await efFetch(context, `/public/landing_pages/${context.query.persona}`, { method: "GET" });
    return { personaData: data.body.landingPage, ssrStatusCode: data.response.status };
  } catch (error) {
    return { ssrStatusCode: error.status };
  }
});
