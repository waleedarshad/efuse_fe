import initPage from "../components/hoc/initPage";
import ForgotPasswordPage from "../components/ForgotPasswordPage/ForgotPasswordPage";

export default initPage(ForgotPasswordPage, "forgotpassword", ["forgotpassword"], "public");
