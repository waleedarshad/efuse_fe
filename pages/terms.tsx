import initPage from "../components/hoc/initPage";
import TermsOfService from "../components/TermsOfService/TermsOfService";

export default initPage(TermsOfService, "terms", ["terms"], "public", null, null, true);
