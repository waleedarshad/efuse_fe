import Router from "next/router";
import nextCookie from "next-cookies";

import AuthenticateScope from "../../components/Oauth/AuthenticateScope/AuthenticateScope";

AuthenticateScope.getInitialProps = async context => {
  const { efuse_temp_token } = nextCookie(context);
  const { client_id, redirect_uri, scope } = context.query;
  if (!efuse_temp_token) {
    redirect(context, `/auth/token?client_id=${client_id}&redirect_uri=${redirect_uri}&scope=${scope}`);
    return;
  }
  return {
    client_id,
    redirect_uri,
    scope: scope || "",
    tokenObject: JSON.parse(efuse_temp_token)
  };
};

const redirect = (context, path) => {
  const { req, res } = context;
  if (req) {
    res.writeHead(302, { Location: path });
    res.end();
    return;
  }
  Router.push(path);
};

export default AuthenticateScope;
