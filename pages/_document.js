/* eslint-disable react/no-danger */
import React from "react";
import Document, { Head, Html, Main, NextScript } from "next/document";
import getConfig from "next/config";
import { ServerStyleSheet } from "styled-components";
import { withCdn } from "../common/utils";

const { publicRuntimeConfig } = getConfig();
const {
  environment,
  googleMapsApiKey,
  viralLoopsCampaignId,
  appsFlyerWebKey,
  ddClientToken,
  ddApplicationId
} = publicRuntimeConfig;

process.on("unhandledRejection", error => {
  console.error(error);
});

process.on("uncaughtException", error => {
  console.error(error);
});

const getPagePathname = pathname => {
  if (pathname === "/") {
    return "/index.js";
  }
  return `${pathname}.js`;
};
class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const sheet = new ServerStyleSheet();
    const originalRenderPage = ctx.renderPage;

    try {
      ctx.renderPage = () =>
        originalRenderPage({
          enhanceApp: App => props => sheet.collectStyles(<App {...props} />)
        });

      const initialProps = await Document.getInitialProps(ctx);
      const userAgent = ctx.req.headers["user-agent"] || "";
      // Check if platform mobile view by comparing user-agent
      const isMobile = /iPhone|iPad|iPod|Android/i.test(userAgent);

      return {
        ...initialProps,
        query: ctx.query,
        isMobile,
        styles: (
          <>
            {initialProps.styles}
            {sheet.getStyleElement()}
          </>
        )
      };
    } finally {
      sheet.seal();
    }
  }

  render() {
    // eslint-disable-next-line no-unused-vars
    const { __NEXT_DATA__, query } = this.props;
    const { page, props } = __NEXT_DATA__;
    const pagePathname = getPagePathname(page);
    if (!props) {
      throw new Error("Are you sure the BE is up?");
    }
    const mapsApiKey = ["production", "staging"].includes(environment)
      ? googleMapsApiKey
      : "AIzaSyAMI_c9Qn5SiEn-k2g3aZb-63e36ovTMH0";
    const segmentApiKey = ["production"].includes(environment)
      ? "Iy24Lr1mBexfVySGM9lPpjHBZlJM5NjS"
      : "ZMfmyoObZ4GVWcZYxCcIIqnny2IprxUS";

    const viralLoopsScript = (
      <script
        dangerouslySetInnerHTML={{
          __html: `!function(){var a=window.VL=window.VL||{};return a.instances=a.instances||{},a.invoked?void(window.console&&console.error&&console.error("VL snippet loaded twice.")):(a.invoked=!0,void(a.load=function(b,c,d){var e={};e.publicToken=b,e.config=c||{};var f=document.createElement("script");f.type="text/javascript",f.id="vrlps-js",f.defer=!0,f.src="https://app.viral-loops.com/client/vl/vl.min.js";var g=document.getElementsByTagName("script")[0];return g.parentNode.insertBefore(f,g),f.onload=function(){a.setup(e),a.instances[b]=e},e.identify=e.identify||function(a,b){e.afterLoad={identify:{userData:a,cb:b}}},e.pendingEvents=[],e.track=e.track||function(a,b){e.pendingEvents.push({event:a,cb:b})},e.pendingHooks=[],e.addHook=e.addHook||function(a,b){e.pendingHooks.push({name:a,cb:b})},e.$=e.$||function(a){e.pendingHooks.push({name:"ready",cb:a})},e}))}();var campaign=VL.load("${viralLoopsCampaignId}",{autoLoadWidgets:!0});`
        }}
      />
    );

    return (
      <Html>
        <Head>
          <meta charSet="utf-8" />
          <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
          <link rel="icon" type="image/png" href={withCdn("/static/images/favicon.png")} />
          <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,500i,600,700,700i" rel="stylesheet" />
          <link href="https://fonts.googleapis.com/css?family=Open+Sans:100,400,400i,500,700" rel="stylesheet" />
          <link href="https://fonts.googleapis.com/css?family=Poppins:100,400,400i,500,600,700" rel="stylesheet" />
          <script src={`https://maps.googleapis.com/maps/api/js?key=${mapsApiKey}&libraries=places`} async defer />
          <style>
            {`
              body {
                font-family: 'Open Sans', sans-serif;
                width: 100%;
                height: 100vh;
              }
              svg {
                height: 1em;
                width: 1em;
              }
              video {
                outline: none;
                vertical-align: middle;
              }
              @media (max-width: 1024px) {
                [class*="col-"],
                [class="col"] {
                  margin-bottom: 1rem;
                }
              }
              @media (max-width: 991px){
                .container {
                  width: 100%;
                  max-width: none;
                }
              }
              .grecaptcha-badge { visibility: hidden; }
            `}
          </style>
          <link rel="stylesheet" href={withCdn("/static/styles/bootstrap.min.css")} />
          <link rel="stylesheet" href="//cdn.quilljs.com/1.2.6/quill.snow.css" />
          <link href={withCdn("/static/styles/reactDropdown.css")} rel="stylesheet" />
          <link href={withCdn("/static/styles/react-multi-email.css")} rel="stylesheet" />
          <link href={withCdn("/static/styles/notification.css")} rel="stylesheet" />
          <link href={withCdn("/static/styles/geosuggest.css")} rel="stylesheet" />
          <link href={withCdn("/static/styles/dropdown.css")} rel="stylesheet" />
          <link href={withCdn("/static/styles/card.css")} rel="stylesheet" />
          <link href={withCdn("/static/styles/rc-slider.css")} rel="stylesheet" />
          <link href={withCdn("/static/styles/react-table.css")} rel="stylesheet" />
          <link href={withCdn("/static/styles/react-confirm-alert.css")} rel="stylesheet" />
          <link href={withCdn("/static/styles/react-select.css")} rel="stylesheet" />
          <link href={withCdn("/static/styles/custom-modal.css")} rel="stylesheet" />
          <link href={withCdn("/static/styles/mention.css")} rel="stylesheet" />
          <link href={withCdn("/static/styles/general.css")} rel="stylesheet" />
          <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css" />
          <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/themes/airbnb.css" />
          <link href={withCdn("/static/styles/emojiPickerOverrides.css")} rel="stylesheet" />
          <link href={withCdn("/static/styles/portfolioTabs.css")} rel="stylesheet" />
          <link href={withCdn("/static/styles/progressbar.css")} rel="stylesheet" />
          <link
            rel="stylesheet"
            type="text/css"
            charSet="UTF-8"
            href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css"
          />
          <link
            rel="stylesheet"
            type="text/css"
            href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css"
          />
          {/* Segment */}
          <script
            dangerouslySetInnerHTML={{
              __html: `
              !function(){var analytics=window.analytics=window.analytics||[];if(!analytics.initialize)if(analytics.invoked)window.console&&console.error&&console.error("Segment snippet included twice.");else{analytics.invoked=!0;analytics.methods=["trackSubmit","trackClick","trackLink","trackForm","pageview","identify","reset","group","track","ready","alias","debug","page","once","off","on"];analytics.factory=function(t){return function(){var e=Array.prototype.slice.call(arguments);e.unshift(t);analytics.push(e);return analytics}};for(var t=0;t<analytics.methods.length;t++){var e=analytics.methods[t];analytics[e]=analytics.factory(e)}analytics.load=function(t,e){var n=document.createElement("script");n.type="text/javascript";n.async=!0;n.src="https://segment-cdn.efcdn.io/analytics.js/v1/"+t+"/analytics.min.js";var a=document.getElementsByTagName("script")[0];a.parentNode.insertBefore(n,a);analytics._loadOptions=e};analytics.SNIPPET_VERSION="4.1.0";
              analytics.load("${segmentApiKey}");
              }}();`
            }}
          />
          {/* Linkedin ad tracking */}
          <script
            dangerouslySetInnerHTML={{
              __html: `
            _linkedin_partner_id = "1553074";
            window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || [];
            window._linkedin_data_partner_ids.push(_linkedin_partner_id);`
            }}
          />
          <script
            dangerouslySetInnerHTML={{
              __html: `
                (function(){if(window.location.hostname == "efuse.gg"){var s = document.getElementsByTagName("script")[0];
                var b = document.createElement("script");
                b.type = "text/javascript";b.async = true;
                b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
                s.parentNode.insertBefore(b, s);}})();
              `
            }}
          />
          <noscript
            dangerouslySetInnerHTML={{
              __html:
                // eslint-disable-next-line quotes
                '<img height="1" width="1" style="display:none;" alt="" src="https://px.ads.linkedin.com/collect/?pid=1553074&fmt=gif" />'
            }}
          />
          {/* Facebook tracking */}
          <script
            dangerouslySetInnerHTML={{
              __html: `
                !function(f,b,e,v,n,t,s)
                {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t,s)}(window, document,'script',
                'https://connect.facebook.net/en_US/fbevents.js');
                fbq('init', '2502478163341749');
                fbq('track', 'PageView');
              `
            }}
          />
          <noscript
            dangerouslySetInnerHTML={{
              __html: `
              <img
                height="1"
                width="1"
                style={{ display: "none" }}
                src="https://www.facebook.com/tr?id=2502478163341749&ev=PageView&noscript=1"
              />`
            }}
          />
          <script
            type="text/javascript"
            dangerouslySetInnerHTML={{
              __html: `
                      window.fbAsyncInit = function() {
                        FB.init({
                          appId            : '1460130250805345',
                          autoLogAppEvents : true,
                          xfbml            : true,
                          version          : 'v6.0'
                        });
                      };`
            }}
          />
          <script async defer src="https://connect.facebook.net/en_US/sdk.js" />
          <script type="text/javascript" src="https://www.youtube.com/iframe_api" />
          {/* WYSIWYG Editor used for learning article blog */}
          {pagePathname.startsWith("/news") ? (
            <>
              <link href={withCdn("/static/plugins/article-editor/article-editor.min.css")} rel="stylesheet" />
            </>
          ) : (
            <></>
          )}
          {/* AppsFlyer.io */}
          <script
            type="text/javascript"
            dangerouslySetInnerHTML={{
              __html:
                // eslint-disable-next-line quotes
                `!function(t,e,n,s,a,c,i,o,p){t.AppsFlyerSdkObject=a,t.AF=t.AF||function(){
                (t.AF.q=t.AF.q||[]).push([Date.now()].concat(Array.prototype.slice.call(arguments)))},
                t.AF.id=t.AF.id||i,t.AF.plugins={},o=e.createElement(n),p=e.getElementsByTagName(n)[0],o.async=1,
                o.src="https://websdk.appsflyer.com?"+(c.length>0?"st="+c.split(",").sort().join(",")+"&":"")+(i.length>0?"af_id="+i:""),
                p.parentNode.insertBefore(o,p)}(window,document,"script",0,"AF","banners",{banners: {key: "${appsFlyerWebKey}"}});
                // Smart Banners are by default set to the max z-index value, so they won't be hidden by the website elements. This can be changed if you want some website components to be on top of the banner.
                AF('banners', 'showBanner', { bannerZIndex: 1000, additionalParams: { p1: "v1", p2: "v2"}});`
            }}
          />
          {/* Tik Tok Ads Pixel Code */}
          {["production"].includes(environment) && (
            <script
              type="text/javascript"
              dangerouslySetInnerHTML={{
                __html:
                  "(function() {var ta = document.createElement('script'); ta.type = 'text/javascript'; ta.async = true; ta.src = 'https://analytics.tiktok.com/i18n/pixel/sdk.js?sdkid=BS4M97B4J692HOFKS1F0'; var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ta, s); })();"
              }}
            />
          )}
          {/* Snapchat Ads Pixel Code */}
          {["production"].includes(environment) && (
            <script
              type="text/javascript"
              dangerouslySetInnerHTML={{
                __html:
                  "(function(e,t,n){if(e.snaptr)return;var a=e.snaptr=function() {a.handleRequest?a.handleRequest.apply(a,arguments):a.queue.push(arguments)}; a.queue=[];var s='script';r=t.createElement(s);r.async=!0; r.src=n;var u=t.getElementsByTagName(s)[0]; u.parentNode.insertBefore(r,u);})(window,document, 'https://sc-static.net/scevent.min.js');  snaptr('init', 'bcfc6e94-a0b5-46ce-a30c-5132d0660c5b', {});  snaptr('track', 'PAGE_VIEW');"
              }}
            />
          )}

          {/* DD LOGS CDN */}
          <script
            type="text/javascript"
            dangerouslySetInnerHTML={{
              __html: `(function(h,o,u,n,d) {
                h=h[d]=h[d]||{q:[],onReady:function(c){h.q.push(c)}}
                d=o.createElement(u);d.async=1;d.src=n
                n=o.getElementsByTagName(u)[0];n.parentNode.insertBefore(d,n)
                })(window,document,'script','https://www.datadoghq-browser-agent.com/datadog-logs-v3.js','DD_LOGS')
                DD_LOGS.onReady(function() {
                  DD_LOGS.init({
                    clientToken: "${ddClientToken}",
                    service: "fe",
                    env: "${environment}",
                    forwardErrorsToLogs: true,
                    sampleRate: 100
                  })
                })`
            }}
          />

          {/* DD RUM CDN */}
          <script
            type="text/javascript"
            dangerouslySetInnerHTML={{
              __html: `(function(h,o,u,n,d) {
                h=h[d]=h[d]||{q:[],onReady:function(c){h.q.push(c)}}
                d=o.createElement(u);d.async=1;d.src=n
                n=o.getElementsByTagName(u)[0];n.parentNode.insertBefore(d,n)
                })(window,document,'script','https://www.datadoghq-browser-agent.com/datadog-rum-v3.js','DD_RUM')
                DD_RUM.onReady(function() {
                  DD_RUM.init({
                    clientToken: "${ddClientToken}",
                    applicationId: "${ddApplicationId}",
                    service: "fe",
                    env: "${environment}",
                    forwardErrorsToLogs: true,
                    sampleRate: 100,
                    trackInteractions: true
                  })
                })`
            }}
          />
        </Head>
        <body>
          <Main />
          <NextScript />
          {viralLoopsScript}
        </body>
      </Html>
    );
  }
}

export default MyDocument;
