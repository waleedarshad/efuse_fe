import axios from "axios";
import nextCookie from "next-cookies";

const LinkShortening = () => {};

LinkShortening.getInitialProps = async ctx => {
  const fallbacRedirectUrl = "/lounge/featured";
  const { query } = ctx;
  const { currentUser } = nextCookie(ctx);
  try {
    const user = JSON.parse(currentUser);
    const response = await axios.post("/link_click", {
      shortName: query.l,
      userId: user.id
    });
    const { link } = response.data;
    if (!link) {
      return redirect(ctx, fallbacRedirectUrl);
    }
    return redirect(ctx, link.url);
  } catch (error) {
    return redirect(ctx, fallbacRedirectUrl);
  }
};

const redirect = (context, path) => {
  const { req, res } = context;
  if (req) {
    res.writeHead(302, { Location: path });
    res.end();
  }
};

export default LinkShortening;
