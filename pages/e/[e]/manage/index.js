import fetch from "isomorphic-unfetch";
import getConfig from "next/config";
import toLower from "lodash/toLower";
import TournamentManagement from "../../../../components/Erena/TournamentManagement/TournamentManagement";
import initPage from "../../../../components/hoc/initPage";

const { publicRuntimeConfig } = getConfig();
const { defaultUrl } = publicRuntimeConfig;

export default initPage(TournamentManagement, "events", ["events"], "private", null, null, true, async context => {
  const { e } = context.query;
  const config = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json"
    }
  };
  const res = await fetch(`${defaultUrl}/tournament/${toLower(e)}/stats`, config);
  const data = await res.json();

  return { event: data };
});
