import getConfig from "next/config";
import toLower from "lodash/toLower";
import initPage from "../../../components/hoc/initPage";
import OrganizationDetail from "../../../components/Organizations/OrganizationDetail/OrganizationDetail";

const { publicRuntimeConfig } = getConfig();
const { defaultUrl } = publicRuntimeConfig;

export default initPage(
  OrganizationDetail,
  "organizations",
  ["organizations"],
  null,
  null,
  null,
  true,
  async context => {
    const { org } = context.query;

    const config = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json"
      }
    };

    try {
      const res = await fetch(`${defaultUrl}/public/organizations/slug/${toLower(org)}`, config);
      const data = await res.json();
      if (data.message) {
        return { error: data.message };
      } else {
        return { organization: data.organization };
      }
    } catch (error) {
      return { ssrStatusCode: error.status };
    }
  }
);
