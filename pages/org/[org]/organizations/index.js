import toLower from "lodash/toLower";
import initPage from "../../../../components/hoc/initPage";
import efFetch from "../../../../store/utils/efFetch";
import ChildOrganizationsWrapper from "../../../../components/Organizations/ChildOrganizations/ChildOrganizationsWrapper/ChildOrganizationsWrapper";

export default initPage(
  ChildOrganizationsWrapper,
  "organizations",
  ["organizations"],
  "private",
  null,
  null,
  false,
  async context => {
    const { org } = context.query;

    const config = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json"
      }
    };

    try {
      const data = await efFetch(context, `/org-trees/slug/${toLower(org)}`, config);
      const { body, response } = data;

      return { organization: body.items[0], ssrStatusCode: response.status };
    } catch (error) {
      return { ssrStatusCode: error.status };
    }
  }
);
