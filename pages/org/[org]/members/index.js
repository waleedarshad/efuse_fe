import toLower from "lodash/toLower";
import initPage from "../../../../components/hoc/initPage";
import OrganizationMembersWrapper from "../../../../components/Organizations/OrganizationMembers/OrganizationMembersWrapper/OrganizationMembersWrapper";
import efFetch from "../../../../store/utils/efFetch";

export default initPage(
  OrganizationMembersWrapper,
  "organizations",
  ["organizations"],
  "private",
  null,
  null,
  false,
  async context => {
    const { org } = context.query;

    const config = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json"
      }
    };

    try {
      const data = await efFetch(context, `/public/organizations/slug/${toLower(org)}`, config);
      const { body, response } = data;

      return { organization: body.organization, ssrStatusCode: response.status };
    } catch (error) {
      return { ssrStatusCode: error.status };
    }
  }
);
