import getConfig from "next/config";
import toLower from "lodash/toLower";
import initPage from "../../../../components/hoc/initPage";
import OrganizationLoginPage from "../../../../components/Organizations/OrganizationLoginPage/OrganizationLoginPage";

const { publicRuntimeConfig } = getConfig();
const { defaultUrl } = publicRuntimeConfig;

export default initPage(
  OrganizationLoginPage,
  "organizations",
  ["organizations"],
  null,
  null,
  null,
  true,
  async context => {
    const { org } = context.query;

    const config = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json"
      }
    };

    const res = await fetch(`${defaultUrl}/public/organizations/slug/${toLower(org)}`, config);
    const data = await res.json();

    return { organization: data.organization, ssrStatusCode: res.status };
  }
);
