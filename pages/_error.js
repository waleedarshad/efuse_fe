import { useEffect } from "react";
import dynamic from "next/dynamic";

const EFErrorPage = dynamic(() => import("../components/EFErrorPage/EFErrorPage"), {
  ssr: false
});

const CustomError = ({ statusCode }) => {
  useEffect(() => {
    analytics.track("CLIENT_ERROR", { errorType: statusCode || "client" });
  }, []);

  return <EFErrorPage statusCode={statusCode} />;
};

CustomError.getInitialProps = ({ res, err }) => {
  let statusCode = null;
  if (res) {
    ({ statusCode } = res);
  } else if (err) {
    ({ statusCode } = err);
  }
  return {
    statusCode
  };
};

export default CustomError;
