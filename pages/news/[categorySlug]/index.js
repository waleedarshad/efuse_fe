import initPage from "../../../components/hoc/initPage";
import NewsHomePage from "../../../components/EsportsNews/NewsHomePage";
import { initializeApollo } from "../../../config/apolloClient";
import GET_ARTICLE_CATEGORIES from "../../../graphql/NewsArticlesCategoryQuery";
import withGraphql from "../../../components/hoc/withGraphql";

const getServerSideProps = async () => {
  const apolloClient = initializeApollo();

  const categories = (await apolloClient.query({ query: GET_ARTICLE_CATEGORIES })).data.getCategories;

  return { categories };
};

export default withGraphql({ ssr: true })(
  initPage(NewsHomePage, "news", ["news"], null, null, null, true, getServerSideProps)
);
