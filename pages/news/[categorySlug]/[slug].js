import fetch from "isomorphic-unfetch";
import getConfig from "next/config";
import initPage from "../../../components/hoc/initPage";
import SingleArticle from "../../../components/EsportsNews/SingleArticle/SingleArticleLayout";

const { publicRuntimeConfig } = getConfig();
const { defaultUrl } = publicRuntimeConfig;

export default initPage(SingleArticle, "news", ["news"], null, null, null, true, async context => {
  const { slug } = context.query;
  const filters = { slug };

  const url = `${defaultUrl}/public/learning?filters=${JSON.stringify(filters)}`;
  const config = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json"
    }
  };
  const res = await fetch(url, config);
  const data = await res.json();
  return { article: data.articles.docs[0] };
});
