import React, { useEffect } from "react";
import initPage from "../../components/hoc/initPage";

const CallbackPage = () => {
  useEffect(() => {
    if (window.opener) {
      window.opener.focus();
      window.close();
    }
  }, []);
  return <div>Authenticating...</div>;
};

export default initPage(CallbackPage, "callback", ["callback"], null, null, null, true);
