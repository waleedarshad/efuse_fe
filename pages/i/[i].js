import fetch from "isomorphic-unfetch";
import getConfig from "next/config";
import initPage from "../../components/hoc/initPage";
import InviteWrapper from "../../components/InviteWrapper/InviteWrapper";
import { initializeApollo } from "../../config/apolloClient";
import { GET_TEAM_BY_ID } from "../../graphql/Team/LeagueTeam";

const { publicRuntimeConfig } = getConfig();
const { defaultUrl } = publicRuntimeConfig;

const withTeam = async code => {
  const apolloClient = initializeApollo();
  const response = await apolloClient.query({
    query: GET_TEAM_BY_ID,
    variables: { teamId: code.resource }
  });

  return { ...code, team: response?.data?.getTeamById };
};

const getServerSideProps = async ({ query: { i }, req }) => {
  const config = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json"
    }
  };

  const response = await fetch(`${defaultUrl}/public/invite_code/${i.toUpperCase()}`, config);
  const data = await response.json();

  const inviteCode = data.code?.resourceType === "TEAM" ? await withTeam(data.code) : data.code;

  return {
    inviteCode,
    q_params: req?.originalUrl,
    SsrStatusCode: response.status
  };
};

export default initPage(
  InviteWrapper,
  "organizations",
  ["organizations"],
  "public",
  null,
  null,
  true,
  getServerSideProps
);
