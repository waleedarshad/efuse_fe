import { useRouter } from "next/router";
import { GraphqlContextProvider } from "../../components/GraphqlContext/GraphqlContext";
import Shoutout from "../../components/Clippy/Shoutout/Shoutout";
import withGraphql from "../../components/hoc/withGraphql";
import { GetClippyByToken } from "../../graphql/clippy/GetClippyByToken";

const ShoutoutPage = () => {
  const router = useRouter();
  const { token } = router.query;

  return (
    <GraphqlContextProvider query={GetClippyByToken} options={{ variables: { token } }}>
      <Shoutout token={token} />
    </GraphqlContextProvider>
  );
};

export default withGraphql({ ssr: true })(ShoutoutPage);
