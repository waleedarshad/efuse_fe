import initPage from "../../components/hoc/initPage";
import withGraphql from "../../components/hoc/withGraphql";
import LoungePage from "../../components/LoungePage/LoungePage";

const HomeWrapper = () => <LoungePage loungeFeedType="following" />;

export default withGraphql({ ssr: true })(initPage(HomeWrapper, "homeFeed", ["homeFeed", "opportunityDetail"]));
