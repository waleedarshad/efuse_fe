import toLower from "lodash/toLower";

import withGraphql from "../../../../components/hoc/withGraphql";
import initPage from "../../../../components/hoc/initPage";

import TournamentManagement from "../../../../components/ErenaV2/TournamentManagement/TournamentManagement";

import { initializeApollo } from "../../../../config/apolloClient";
import { GET_ERENA_TOURNAMENT } from "../../../../graphql/ERenaTournamentQuery";

const getServerSideProps = async context => {
  const { e } = context.query;
  const apolloClient = initializeApollo();
  const data = await apolloClient.query({
    query: GET_ERENA_TOURNAMENT,
    variables: { tournamentIdOrSlug: toLower(e) },
    fetchPolicy: "network-only"
  });
  const pageProps = { event: data?.data?.getERenaTournament, userRoles: data?.data?.getERenaTournament.staff };
  return pageProps;
};

export default withGraphql({ ssr: true })(
  initPage(TournamentManagement, "events", ["events"], "private", null, null, true, getServerSideProps)
);
