import fetch from "isomorphic-unfetch";
import getConfig from "next/config";
import toLower from "lodash/toLower";
import ErenaV2 from "../../../components/ErenaV2/ErenaV2";
import initPage from "../../../components/hoc/initPage";

const { publicRuntimeConfig } = getConfig();
const { defaultUrl } = publicRuntimeConfig;

export default initPage(ErenaV2, "events", ["events"], "public", null, null, true, async context => {
  const { e } = context.query;
  const config = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json"
    }
  };
  const res = await fetch(`${defaultUrl}/erena/v2/public/tournament/${toLower(e)}`, config);
  const data = await res.json();

  const bracketResponse = await fetch(
    `${defaultUrl}/erena/v2/public/tournament/${toLower(
      e
    )}/bracket?includeRounds=true&includeMatches=true&includeTeams=true&includePlayers=true&includeScores=true`,
    config
  );
  const bracketData = await bracketResponse.json();

  return { event: data, bracketData };
});
