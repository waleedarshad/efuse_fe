import initPage from "../../components/hoc/initPage";
import CreateEventPage from "../../components/ErenaV2/CreateEventPage/CreateEventPage";

export default initPage(CreateEventPage, "events", ["events"], "private");
