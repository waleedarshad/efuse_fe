import Router from "next/router";
import initPage from "../../components/hoc/initPage";
import { FeedType } from "../../components/Feed/types";
import LoungePage from "../../components/LoungePage/LoungePage";

const ALLOWED_LOUNGE_FEEDS: FeedType[] = ["featured", "verified"];

const getInitialPropsForPage = ({ query, res, req }) => {
  if (!ALLOWED_LOUNGE_FEEDS.includes(query.type)) {
    const redirectPath = "/lounge/featured";

    if (req) {
      res.writeHead(302, { Location: redirectPath });
      res.end();
    } else {
      Router.push(redirectPath);
    }
  }
  return {};
};

const LoungeWrapper = props => <LoungePage loungeFeedType={props.query.type} />;

export default initPage(
  LoungeWrapper,
  "homeFeed",
  ["homeFeed", "lounge"],
  "private",
  null,
  null,
  false,
  getInitialPropsForPage
);
