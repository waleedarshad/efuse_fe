import Games from "../../components/Games/Games";
import initPage from "../../components/hoc/initPage";

export default initPage(Games, "games", ["games"]);
