import React from "react";
import initPage from "../../components/hoc/initPage";
import Dashboard from "../../components/Organizations/Dashboard/Dashboard";
import { initializeApollo } from "../../config/apolloClient";
import {
  ORG_LEAGUES_FOR_DASHBOARD,
  OrgLeaguesForDashboardData,
  OrgLeaguesForDashboardVars
} from "../../graphql/leagues/Leagues";

const getServerSideProps = async (context, token) => {
  const apolloClient = initializeApollo(null, token, context);

  try {
    const orgLeaguesForDashboardResponse = (
      await apolloClient.query<OrgLeaguesForDashboardData, OrgLeaguesForDashboardVars>({
        query: ORG_LEAGUES_FOR_DASHBOARD,
        variables: { orgId: context?.query?.id, joinStatus: "joinedAndPending" },
        fetchPolicy: "no-cache"
      })
    )?.data;

    const canOrgCreateLeagues = orgLeaguesForDashboardResponse?.canUserCreateLeaguesForOrg ?? false;
    const orgLeagues = orgLeaguesForDashboardResponse?.getLeaguesByOrganization?.length ?? 0;
    const orgJoinedOrPendingLeagues = orgLeaguesForDashboardResponse?.getJoinedOrPendingLeaguesByOrg?.length ?? 0;

    return { canOrgCreateLeagues, leagues: orgLeagues + orgJoinedOrPendingLeagues };
  } catch (error) {
    return { error };
  }
};

const OrganizationDashboardWrapper = ({ pageProps }) => {
  return <Dashboard {...pageProps} />;
};

// export default initPage(Dashboard, "organizations", ["organizations"]);
export default initPage(
  OrganizationDashboardWrapper,
  "organizations",
  ["organizations"],
  "public",
  null,
  null,
  true,
  getServerSideProps
);
