import initPage from "../../components/hoc/initPage";
import MyOrganizationWrapper from "../../components/Organizations/OrganizationWrapper/MyOrganizationWrapper";

export default initPage(MyOrganizationWrapper, "organizations", ["organizations"], "private", null, "owned");
