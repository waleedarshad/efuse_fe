import React from "react";
import initPage from "../../components/hoc/initPage";
import OrganizationFeed from "../../components/Organizations/OrganizationFeed";

const OrgProfileFeedWrapper = props => <OrganizationFeed {...props} />;

export default initPage(OrgProfileFeedWrapper, "homeFeed", ["organizations"]);
