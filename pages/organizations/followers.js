import initPage from "../../components/hoc/initPage";
import Followers from "../../components/Organizations/Followers/Followers";

export default initPage(Followers, "organizations", ["organizations"]);
