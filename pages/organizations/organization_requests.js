import initPage from "../../components/hoc/initPage";
import OrganizationRequests from "../../components/Organizations/OrganizationRequests/OrganizationRequestsWrapper/OrganizationRequestsWrapper";

export default initPage(OrganizationRequests, "organizations", ["organizations"]);
