import initPage from "../../components/hoc/initPage";
import OrganizationWrapper from "../../components/Organizations/OrganizationWrapper/OrganizationWrapper";

export default initPage(
  OrganizationWrapper,
  "organizations",
  ["organizations", "createOpportunity"],
  null,
  null,
  "index",
  true
);
