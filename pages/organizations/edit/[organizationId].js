import initPage from "../../../components/hoc/initPage";
import EditOrganization from "../../../components/Organizations/EditOrganization";

const EditOrganizationWrapper = ({ props }) => <EditOrganization {...props} />;

export default initPage(
  EditOrganizationWrapper,
  "createOrganization",
  ["createOrganization", "organizations", "createOpportunity"],
  "private"
);
