import initPage from "../../components/hoc/initPage";
import Opportunities from "../../components/Organizations/Opportunities/Opportunities";

export default initPage(
  Opportunities,
  "opportunities",
  ["opportunities", "createOpportunity", "organizations"],
  "private",
  null,
  "organizations/opportunities",
  true
);
