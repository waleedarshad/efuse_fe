import initPage from "../../components/hoc/initPage";
import OrganizationMembersWrapper from "../../components/Organizations/OrganizationMembers/OrganizationMembersWrapper/OrganizationMembersWrapper";

export default initPage(OrganizationMembersWrapper, "organizations", ["organizations"]);
