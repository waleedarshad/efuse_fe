import initPage from "../../components/hoc/initPage";
import CreateOrganizationFlow from "../../components/Organizations/CreateOrganizationNew/CreateOrganizationFlow";

const CreateOrganizationWrapper = () => <CreateOrganizationFlow />;

export default initPage(
  CreateOrganizationWrapper,
  "createOrganization",
  ["createOrganization", "organizations", "createOpportunity"],
  "private"
);
