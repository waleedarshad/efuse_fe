import fetch from "isomorphic-unfetch";
import getConfig from "next/config";
import initPage from "../../components/hoc/initPage";
import OrganizationDetail from "../../components/Organizations/OrganizationDetail/OrganizationDetail";

const { publicRuntimeConfig } = getConfig();
const { defaultUrl } = publicRuntimeConfig;

export default initPage(
  OrganizationDetail,
  "organizations",
  ["organizations"],
  null,
  null,
  null,
  true,
  async context => {
    const { id } = context.query;

    const url = `${defaultUrl}/public/organizations/show/${id}`;
    const config = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json"
      }
    };
    const res = await fetch(url, config);
    const data = await res.json();
    return { organization: data.organization, ssrStatusCode: res.status };
  }
);
