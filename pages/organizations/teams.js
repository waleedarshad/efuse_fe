import initPage from "../../components/hoc/initPage";
import OrganizationTeams from "../../components/Organizations/OrganizationTeams/OrganizationTeams";

export default initPage(OrganizationTeams, "organizations", ["organizations"]);
