import initPage from "../components/hoc/initPage";
import WeeklySpark from "../components/WeeklySpark/WeeklySpark";

export default initPage(WeeklySpark, "ws", ["ws"], "public", null, null, true);
