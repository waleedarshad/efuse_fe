import initPage from "../components/hoc/initPage";
import Login from "../components/Login/Login";

export default initPage(Login, "login", ["login", "onboarding"], "public");
