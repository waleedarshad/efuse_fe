import initPage from "../../components/hoc/initPage";
import DataTransparency from "../../components/DataTransparency/DataTransparency";

export default initPage(DataTransparency, "privacy", ["privacy"], "public", null, null, true);
