import initPage from "../../components/hoc/initPage";
import PrivacyPolicy from "../../components/PrivacyPolicy/PrivacyPolicy";

export default initPage(PrivacyPolicy, "privacy", ["privacy"], "public", null, null, true);
