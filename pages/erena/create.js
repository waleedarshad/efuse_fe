import initPage from "../../components/hoc/initPage";
import CreateEventPage from "../../components/Erena/CreateEventPage/CreateEventPage";

export default initPage(CreateEventPage, "events", ["events"], "private");
