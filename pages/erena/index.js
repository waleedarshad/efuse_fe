import initPage from "../../components/hoc/initPage";
import ErenaLandingPageWrapper from "../../components/Erena/ErenaLandingPage/ErenaLandingPageWrapper/ErenaLandingPageWrapper";
import withGraphql from "../../components/hoc/withGraphql";
import { initializeApollo } from "../../config/apolloClient";
import { GET_GAMES } from "../../graphql/GameQuery";

const getServerSideProps = async () => {
  const apolloClient = initializeApollo();

  const games = (await apolloClient.query({ query: GET_GAMES })).data.getGames;

  return { games };
};

export default withGraphql({ ssr: true })(
  initPage(ErenaLandingPageWrapper, "events", ["events"], null, null, null, true, getServerSideProps)
);
