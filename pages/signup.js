import initPage from "../components/hoc/initPage";
import SignupPage from "../components/Signup/SignupPage";

export default initPage(SignupPage, "signup", ["signup"], "public");
