import { gql } from "@apollo/client";

import { ClippyFragment } from "../fragments/ClippyFragment";

// eslint-disable-next-line import/prefer-default-export
export const GetClippyByToken = gql`
  ${ClippyFragment}
  query GetClippyByToken($token: String!) {
    getClippyByToken(token: $token) {
      ...ClippyFragment
    }
  }
`;
