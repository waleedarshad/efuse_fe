import { gql } from "@apollo/client";

import { ClippyShoutoutClipFragment } from "../fragments/ClippyFragment";

// eslint-disable-next-line import/prefer-default-export
export const GetShoutoutClip = gql`
  ${ClippyShoutoutClipFragment}
  query GetShoutoutClip($commandId: String!, $channelName: String!, $token: String!) {
    getShoutoutClip(commandId: $commandId, channelName: $channelName, token: $token) {
      ...ClippyShoutoutClipFragment
    }
  }
`;
