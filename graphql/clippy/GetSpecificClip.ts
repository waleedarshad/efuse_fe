import { gql } from "@apollo/client";

import { ClippyShoutoutClipFragment } from "../fragments/ClippyFragment";

// eslint-disable-next-line import/prefer-default-export
export const GetSpecificClip = gql`
  ${ClippyShoutoutClipFragment}
  query GetSpecificClip($token: String!, $url: String!) {
    getSpecificClip(token: $token, url: $url) {
      ...ClippyShoutoutClipFragment
    }
  }
`;
