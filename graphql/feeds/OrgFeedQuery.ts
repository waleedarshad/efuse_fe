import { gql } from "@apollo/client";
import { POST_FIELDS_FRAGMENT } from "./postFieldsFragment";
import { Post } from "./Models";

export interface OrgFeedData {
  OrganizationFeed: {
    hasNextPage: boolean;
    page: number;
    docs: [Post];
  };
}

export interface OrgFeedVars {
  page: number;
  limit: number;
  orgId: string;
}

export const GET_ORG_FEED = gql`
  ${POST_FIELDS_FRAGMENT}
  query Query($page: Int, $limit: Int, $orgId: String) {
    OrganizationFeed(page: $page, limit: $limit, orgId: $orgId) {
      hasNextPage
      page
      docs {
        ...PostFields
      }
    }
  }
`;
