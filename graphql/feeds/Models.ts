// eslint-disable-next-line import/no-cycle
import { CommentModel } from "../comments/Models";
import { HypeUser } from "../interface/Hype";
import { Clip } from "./PostClipQuery";

export interface Pagination {
  totalDocs?: number;
  hasNextPage: boolean;
  page: number;
}

export interface PaginatedComments {
  docs: CommentModel[];
  pagination?: Pagination;
}

export interface PaginatedHypeUser {
  docs: HypeUser[];
  pagination?: Pagination;
}
export interface Post {
  _id: string;
  parentFeedId: string;
  feedId: string;
  text: string;
  currentUserHypes: number;
  hypes: number;
  comments: number;
  createdAt: string;
  views: number;
  feedRank: number;
  kind: "userPost" | "orgPost";
  currentUserFollows: boolean;
  boosted: boolean;
  author: {
    _id: string;
    bio: string;
    name: string;
    username: string;
    verified: boolean;
    online: boolean;
    profileImage: string;
    streak: number;
    badges: {
      badge: {
        slug: string;
        active: boolean;
        name: string;
      };
    }[];
  };
  mentions: {
    type: string;
    display: string;
    id: string;
  }[];
  openGraph: {
    url: string;
    opengraph: {
      title: string;
      description: string;
      img: string;
    };
  }[];
  media: {
    file: {
      contentType: string;
      url: string;
    };
  }[];
  organization: {
    id: string;
    name: string;
    verified: boolean;
    profileImage: string;
  };
  giphy: {
    images: {
      downsized_medium: {
        url: string;
      };
    };
  };
  enableCommentCreation?: boolean;
  postComments?: PaginatedComments;
  loadHypeUser?: boolean;
  hypedUser: PaginatedHypeUser;
  isFetchingPostComments?: boolean;
  youtubeVideo: Clip;
  isNewPost?: boolean;
  topComment: CommentModel;
  game: {
    _id: string;
    slug: string;
    kind: string;
    title: string;
  };
}
