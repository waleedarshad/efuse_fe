import { gql } from "@apollo/client";
import { POST_FIELDS_FRAGMENT } from "./postFieldsFragment";
import { Post } from "./Models";

export interface FollowingFeedData {
  FollowingFeed: {
    hasNextPage: boolean;
    page: number;
    docs: [Post];
  };
}

export interface FollowingFeedVars {
  page: number;
  limit: number;
}

export const GET_FOLLOWING_FEED = gql`
  ${POST_FIELDS_FRAGMENT}
  query Query($page: Int, $limit: Int) {
    FollowingFeed(page: $page, limit: $limit) {
      hasNextPage
      page
      docs {
        ...PostFields
      }
    }
  }
`;
