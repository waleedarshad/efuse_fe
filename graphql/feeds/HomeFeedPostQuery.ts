import { gql } from "@apollo/client";
import { POST_FIELDS_FRAGMENT } from "./postFieldsFragment";
import { Post } from "./Models";

export interface GetHomeFeedPostData {
  gethomeFeedPost: Post;
}

export interface GetHomeFeedPostVars {
  parentFeedId: string;
}

export const GET_HOME_FEED_POST = gql`
  ${POST_FIELDS_FRAGMENT}
  query Query($parentFeedId: ID!) {
    gethomeFeedPost(feedId: $parentFeedId) {
      ...PostFields
    }
  }
`;
