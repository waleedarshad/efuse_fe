import { gql } from "@apollo/client";
import { FILE_FRAGMENT } from "../fragments/FileFragment";
import { GIPHY_FIELDS_FRAGMENT } from "../fragments/GiphyFragment";
import { HYPE_FRAGMENT } from "../fragments/HypeFragment";
import { MENTION_FIELDS_FRAGMENT } from "../fragments/MentionFragment";

// eslint-disable-next-line import/prefer-default-export
export const CommentFields = `
    _id
    content
    user {
      _id
      bio
      name
      username
      verified
      online
      profileImage
      streak
    }
    commentable {
      parentFeedId
      feedId
    }
    commentableType
    image {
      ...FileFields
    }
    mentions {
      ...MentionFields
    }
    likes
    giphy {
      ...GiphyFields
    }
    parent
    hype {
      ...HypeFields
    }

    platform
    media {
      file {
        ...FileFields
      }
    }
    createdAt
    updatedAt
    deleted
`;

// eslint-disable-next-line import/prefer-default-export
export const POST_FIELDS_FRAGMENT = gql`
  ${FILE_FRAGMENT}
  ${MENTION_FIELDS_FRAGMENT}
  ${GIPHY_FIELDS_FRAGMENT}
  ${HYPE_FRAGMENT}
  fragment PostFields on Post {
    parentFeedId
    feedId
    text
    currentUserHypes
    hypes
    comments
    createdAt
    views
    feedRank
    kind
    kindId
    currentUserFollows
    boosted
    author {
      _id
      bio
      name
      username
      verified
      online
      profileImage
      streak
      twitch {
        username
      }
      badges {
        badge {
          slug
          active
          name
        }
      }
    }
    mentions {
      type
      display
      id
    }
    openGraph {
      url
      opengraph {
        title
        description
        img
      }
    }
    media {
      file {
        contentType
        url
      }
    }
    organization {
      id
      name
      verified
      profileImage
    }
    giphy {
      images {
        downsized_medium {
          url
        }
      }
    }
    youtubeVideo {
      videoUrl
      thumbnails {
        medium {
          url
        }
      }
    }
    topComment {
      ${CommentFields}  
      thread {
        totalDocs
      }
    }
  }
`;
