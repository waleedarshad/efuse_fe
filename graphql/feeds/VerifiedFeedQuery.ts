import { gql } from "@apollo/client";
import { POST_FIELDS_FRAGMENT } from "./postFieldsFragment";
import { Post } from "./Models";

export interface VerifiedFeedData {
  VerifiedFeed: {
    hasNextPage: boolean;
    page: number;
    docs: [Post];
  };
}

export interface VerifiedFeedVars {
  page: number;
  limit: number;
}

export const GET_VERIFIED_FEED = gql`
  ${POST_FIELDS_FRAGMENT}
  query Query($page: Int, $limit: Int) {
    VerifiedFeed(page: $page, limit: $limit) {
      hasNextPage
      page
      docs {
        ...PostFields
      }
    }
  }
`;
