import { gql } from "@apollo/client";
import { POST_FIELDS_FRAGMENT } from "./postFieldsFragment";
import { Post } from "./Models";

export interface UserFeedData {
  UserFeed: {
    hasNextPage: boolean;
    page: number;
    docs: [Post];
  };
}

export interface UserFeedVars {
  page: number;
  limit: number;
  userId: string;
}

export const GET_USER_FEED = gql`
  ${POST_FIELDS_FRAGMENT}
  query Query($page: Int, $limit: Int, $userId: String) {
    UserFeed(page: $page, limit: $limit, userId: $userId) {
      hasNextPage
      page
      docs {
        ...PostFields
      }
    }
  }
`;
