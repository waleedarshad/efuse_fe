import { gql } from "@apollo/client";
import { POST_FIELDS_FRAGMENT } from "./postFieldsFragment";
import { Post } from "./Models";
import { Mention } from "../interface/Mention";
import { File } from "../interface/File";
import { Game } from "../interface/Game";
import { Giphy } from "../interface/Giphy";

export interface FeedPost {
  updatePost?: Post;
  createPost?: Post;
  getPostById?: Post;
  createSchedulePost?: Post;
  post?: Post;
}

export interface PostVars {
  feedId: string;
  body?: UpdatePostInput;
}

export const GET_POST = gql`
  ${POST_FIELDS_FRAGMENT}
  query Query($feedId: ID!) {
    getPostById(id: $feedId) {
      ...PostFields
    }
  }
`;

export interface UpdatePostInput {
  parentFeedId: string;
  text: string;
  mentions: Mention[];
  media: File[];
  associatedGame: Game;
}

export const UPDATE_POST = gql`
  ${POST_FIELDS_FRAGMENT}
  mutation Mutation($feedId: ID!, $body: UpdatePostInput!) {
    updatePost(id: $feedId, body: $body) {
      ...PostFields
    }
  }
`;

export interface PostInput {
  text: string;
  twitchClip: string;
  file: File;
  files: File[];
  giphy: Giphy;
  kindId: string;
  kind: string;
  verified: Boolean;
  platform: string;
  mentions: Mention[];
  shared: Boolean;
  sharedFeed: string;
  sharedTimeline: string;
  sharedTimelineable: string;
  sharedPostUser: string;
  crossPosts: string[];
  associatedGame: string;
  youtubeVideo: string;
  scheduledAt: string;
}
export interface CreatePostInput {
  body: PostInput;
}

export const CREATE_POST = gql`
  ${POST_FIELDS_FRAGMENT}
  mutation Mutation($body: PostInput!) {
    createPost(body: $body) {
      ...PostFields
    }
  }
`;

export const DELETE_POST = gql`
  mutation Mutation($feedId: ID!) {
    deletePost(feedId: $feedId)
  }
`;

export interface CreateScheduleInput {
  body: PostInput;
}

export const CREATE_SCHEDULE_POST = gql`
  ${POST_FIELDS_FRAGMENT}
  mutation Mutation($body: PostInput!) {
    createSchedulePost(body: $body) {
      ...PostFields
    }
  }
`;
