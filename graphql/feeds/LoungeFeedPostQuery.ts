import { gql } from "@apollo/client";
import { POST_FIELDS_FRAGMENT } from "./postFieldsFragment";
import { Post } from "./Models";

export interface GetLoungeFeedPostData {
  getLoungeFeedPost: Post;
}

export interface GetLoungeFeedPostVars {
  parentFeedId: string;
}

export const GET_LOUNGE_FEED_POST = gql`
  ${POST_FIELDS_FRAGMENT}
  query Query($parentFeedId: ID!) {
    getLoungeFeedPost(feedId: $parentFeedId) {
      ...PostFields
    }
  }
`;
