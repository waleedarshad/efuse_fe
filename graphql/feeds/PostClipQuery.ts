import { gql } from "@apollo/client";

interface Thumbnail {
  medium: {
    url: string;
    width: string;
    height: string;
  };
}

export interface Clip {
  _id: string;
  videoId: string;
  videoUrl: string;
  thumbnails: Thumbnail;
  title: string;
  videoPublishedAt: string;
}

export interface YoutubeVideo {
  GetPaginatedYoutubeVideos: {
    page: number;
    hasNextPage: boolean;
    docs: Clip[];
  };
}

export interface YoutubeVideoVars {
  ownerKind: string;
  owner: string;
  page: number;
  limit: number;
}

// eslint-disable-next-line import/prefer-default-export
export const GET_PAGINATED_YOUTUBE_VIDEOS = gql`
  query GetPaginatedYoutubeVideos($ownerKind: String!, $owner: String, $page: Int, $limit: Int) {
    GetPaginatedYoutubeVideos(ownerKind: $ownerKind, owner: $owner, page: $page, limit: $limit) {
      page
      hasNextPage
      totalPages
      docs {
        _id
        videoId
        videoUrl
        title
        videoPublishedAt
        thumbnails {
          medium {
            url
            width
            height
          }
        }
      }
    }
  }
`;
