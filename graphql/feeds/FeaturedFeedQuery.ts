import { gql } from "@apollo/client";
import { Post } from "./Models";
import { POST_FIELDS_FRAGMENT } from "./postFieldsFragment";

export interface FeaturedFeedData {
  FeaturedFeed: {
    hasNextPage: boolean;
    page: number;
    docs: [Post];
  };
}

export interface FeaturedFeedVars {
  page: number;
  limit: number;
}

export const GET_FEATURED_FEED = gql`
  ${POST_FIELDS_FRAGMENT}
  query Query($page: Int, $limit: Int) {
    FeaturedFeed(page: $page, limit: $limit) {
      hasNextPage
      page
      docs {
        ...PostFields
      }
    }
  }
`;
