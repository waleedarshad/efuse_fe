import { gql } from "@apollo/client";

export const GET_USER_BY_ID = gql`
  query getUserById($id: ID!) {
    getUserById(id: $id) {
      _id
      name
      bio
      verified
      profilePicture {
        filename
        contentType
        url
      }
      headerImage {
        filename
        contentType
        url
      }
      username
      createdAt
      url
      score
      tokenBalance
    }
  }
`;

export const GET_AIMLAB_LEADERBOARD_FOR_USER = gql`
  query Query($id: ID!) {
    getUserById(id: $id) {
      pipelineLeaderboardRanks {
        aimlabLeaderboardRank {
          rank
          user {
            name
            username
            profilePicture {
              filename
              url
            }
          }
          recruitmentProfile {
            highSchool
            graduationClass
            city
            state
          }
          stats {
            ... on AimlabStats {
              ranking {
                rank {
                  displayName
                  level
                  tier
                }
              }
            }
          }
        }
      }
    }
  }
`;

export const GET_LEAGUE_OF_LEGENDS_LEADERBOARD_FOR_USER = gql`
  query Query($id: ID!) {
    getUserById(id: $id) {
      pipelineLeaderboardRanks {
        leagueOfLegendsLeaderboardRank {
          rank
          user {
            name
            username
            profilePicture {
              filename
              url
            }
          }
          recruitmentProfile {
            highSchool
            graduationClass
            city
            state
          }
          stats {
            ... on LeagueOfLegendsStats {
              leagueInfo {
                summonerName
                rank
                tier
                queueType
              }
            }
          }
        }
      }
    }
  }
`;

export const GET_VALORANT_LEADERBOARD_FOR_USER = gql`
  query Query($id: ID!) {
    getUserById(id: $id) {
      pipelineLeaderboardRanks {
        valorantLeaderboardRank {
          rank
          user {
            name
            username
            profilePicture {
              filename
              url
            }
          }
          recruitmentProfile {
            highSchool
            graduationClass
            city
            state
          }
          stats {
            ... on ValorantUserStats {
              competitiveTierName
              rankedRating
              gameName
            }
          }
        }
      }
    }
  }
`;

export const GET_AIMLAB_STATS_FOR_USER = gql`
  query getUserById($id: ID!) {
    getUserById(id: $id) {
      aimlabStats {
        username
        ranking {
          rank {
            displayName
          }
          skill
          badgeImage
        }
        skillScores {
          name
          score
        }
      }
    }
  }
`;

export const UPDATE_PASSWORD = gql`
  mutation CreateTeamMutation($updatePasswordBody: UserPasswordInput) {
    updatePassword(body: $updatePasswordBody) {
      success
    }
  }
`;

export const GET_AUTHENTICATED_USER = gql`
  query getAuthenticatedUser {
    getAuthenticatedUser {
      _id
    }
  }
`;

export const BAN_USER = gql`
  mutation banUser($idOrUsername: String) {
    banUser(idOrUsername: $idOrUsername) {
      success
    }
  }
`;
