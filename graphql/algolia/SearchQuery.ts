import { gql } from "@apollo/client";

import { ALGOLIA_FRAGMENT } from "../fragments/AlgoliaFragment";

// eslint-disable-next-line import/prefer-default-export
export const SEARCH_ORGANIZATION = gql`
  ${ALGOLIA_FRAGMENT}
  query Query($requests: [AlgoliaSearchInput]!) {
    OrganizationAlgoliaSearch(requests: $requests) {
      results {
        ...AlgoliaSpecificFields
        hits {
          _id
          name
          about
          description
          followersCount
          verified {
            status
          }
          _highlightResult
          membersCount
          headerImage {
            url
          }
          profileImage {
            url
          }
          status
          shortName
          publishStatus
          user {
            _id
            username
            name
          }
          isCurrentUserAFollower {
            _id
          }
          isCurrentUserAMember {
            isBanned
            title
          }
          currentUserRequestToJoin {
            status
          }
        }
      }
    }
  }
`;
