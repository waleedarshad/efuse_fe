import { gql } from "@apollo/client";

const GET_ERENA_FEATURED_VIDEO = gql`
  query getERenaFeaturedVideos {
    getERenaFeaturedVideos {
      _id
      title
      description
      youtubeLink
      order
    }
  }
`;

export default GET_ERENA_FEATURED_VIDEO;
