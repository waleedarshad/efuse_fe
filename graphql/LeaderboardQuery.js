import { gql } from "@apollo/client";

export const GET_PAGINATED_AIMLAB_LEADERBOARD = gql`
  query Query($page: Int, $limit: Int) {
    getPaginatedAimlabLeaderboard(page: $page, limit: $limit) {
      docs {
        rank
        user {
          name
          username
          profilePicture {
            filename
            url
          }
        }
        recruitmentProfile {
          highSchool
          graduationClass
          city
          state
        }
        stats {
          ... on AimlabStats {
            username
            ranking {
              rank {
                displayName
                level
                tier
              }
            }
          }
        }
      }
      limit
      page
      totalPages
      nextPage
      prevPage
      hasPrevPage
      hasNextPage
      totalDocs
    }
  }
`;

export const GET_PAGINATED_LEAGUE_OF_LEGENDS_LEADERBOARD = gql`
  query Query($page: Int, $limit: Int) {
    getPaginatedLeagueOfLegendsLeaderboard(page: $page, limit: $limit) {
      docs {
        rank
        user {
          name
          username
          profilePicture {
            filename
            url
          }
        }
        recruitmentProfile {
          highSchool
          graduationClass
          city
          state
        }
        stats {
          ... on LeagueOfLegendsStats {
            leagueInfo {
              summonerName
              rank
              tier
              queueType
            }
          }
        }
      }
      limit
      page
      totalPages
      nextPage
      prevPage
      hasPrevPage
      hasNextPage
      totalDocs
    }
  }
`;

export const GET_PAGINATED_VALORANT_LEADERBOARD = gql`
  query Query($page: Int, $limit: Int) {
    getPaginatedValorantLeaderboard(page: $page, limit: $limit) {
      docs {
        rank
        user {
          name
          username
          profilePicture {
            filename
            url
          }
        }
        recruitmentProfile {
          highSchool
          graduationClass
          city
          state
        }
        stats {
          ... on ValorantUserStats {
            competitiveTierName
            rankedRating
            gameName
          }
        }
      }
      limit
      page
      totalPages
      nextPage
      prevPage
      hasPrevPage
      hasNextPage
      totalDocs
    }
  }
`;
