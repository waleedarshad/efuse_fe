import { gql } from "@apollo/client";

export interface AcceptInvitationResponse {
  acceptInvite: boolean;
}

export interface AcceptInvitationVars {
  inviteCode: string;
  joiningOrgId: string;
}

export const ACCEPT_INVITATION = gql`
  mutation AcceptInviteMutation($inviteCode: String!, $joiningOrgId: String!) {
    acceptInvite(code: $inviteCode, entityId: $joiningOrgId)
  }
`;

export interface GetOrgFromInviteCodeResponse {
  getInviteCode: {
    organization: {
      _id: string;
      name: string;
    };
  };
}

export interface GetOrgFromInviteCodeVars {
  inviteCode: string;
}
export const GET_ORG_FROM_INVITE_CODE = gql`
  query Query($inviteCode: String!) {
    getInviteCode(code: $inviteCode) {
      organization {
        _id
        name
      }
    }
  }
`;
