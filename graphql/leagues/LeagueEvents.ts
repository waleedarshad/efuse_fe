import { gql } from "@apollo/client";
import { LeagueEvent } from "../interface/League";

type BracketType = "pointRace" | "singleElim" | "doubleElim" | "roundRobin" | "swis";
type LeagueEventState =
  | "active"
  | "cancelled"
  | "finished"
  | "pending"
  | "pendingOrganizationInvites"
  | "pendingPools"
  | "pendingTeams";

export interface CreateLeagueEventResponse {
  createLeagueEvent: {
    _id: string;
  };
}

export interface CreateLeagueEventVars {
  event: {
    bracketType: BracketType;
    description: string;
    gamesPerMatch: number;
    leagueId: string;
    name: string;
    numberOfPools?: number;
    maxTeams?: number;
    startDate: string;
    timingMode: "weekly";
    state: LeagueEventState;
  };
}

export const CREATE_LEAGUE_EVENT = gql`
  mutation CreateLeagueEventMutation($event: CreateLeagueEventArg!) {
    createLeagueEvent(leagueEvent: $event) {
      _id
    }
  }
`;

export interface GetLeaguesEventByIdVars {
  eventId: string;
}

export interface GetLeaguesEventByIdData {
  getLeagueEventById: LeagueEvent;
}

export const GET_LEAGUE_EVENT_BY_ID = gql`
  query Query($eventId: String!) {
    getLeagueEventById(eventId: $eventId) {
      _id
      bracketType
      description
      gamesPerMatch
      rules
      league {
        _id
        imageUrl
        name
        owner {
          ... on Organization {
            name
            user {
              _id
            }
            profileImage {
              url
            }
            _id
          }
        }
        game {
          title
          gameImage {
            url
          }
        }
        teams {
          _id
          name
          owner {
            ... on Organization {
              _id
              profileImage {
                url
              }
            }
          }
          members {
            _id
            user {
              name
              profilePicture {
                url
              }
            }
          }
        }
      }
      maxTeams
      name
      teams {
        _id
        name
        image {
          url
        }
        owner {
          ... on Organization {
            _id
            profileImage {
              url
            }
          }
        }
        members {
          _id
          user {
            name
            profilePicture {
              url
            }
          }
        }
      }
      startDate
      state
      timingMode
      pools {
        _id
        name
        teams {
          _id
          name
          image {
            url
          }
          owner {
            ... on Organization {
              profileImage {
                url
              }
            }
          }
        }
      }
    }
  }
`;

export const DELETE_LEAGUE_EVENT = gql`
  mutation deleteLeagueEvent($eventId: String!) {
    deleteLeagueEvent(eventId: $eventId)
  }
`;

export interface UpdateEventsVars {
  eventId: string;
  updates: {
    rules?: string;
    description?: string;
  };
}

export const UPDATE_LEAGUE_EVENT = gql`
  mutation UpdateLeagueEvent($eventId: String!, $updates: UpdateLeagueEventArg!) {
    updateLeagueEvent(eventId: $eventId, leagueEvent: $updates) {
      _id
      name
      rules
      description
    }
  }
`;
