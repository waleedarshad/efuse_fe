import { gql } from "@apollo/client";
import { League } from "../interface/League";

export interface GetLeagueByIdData {
  getLeagueById: League;
}

export interface GetLeagueByIdVars {
  leagueId: string;
}

export const GET_LEAGUE_BY_ID = gql`
  query Query($leagueId: String!) {
    getLeagueById(leagueId: $leagueId) {
      _id
      name
      imageUrl
      state
      description
      rules
      owner {
        ... on Organization {
          _id
          name
          leaguesInviteCode
          profileImage {
            url
          }
          user {
            _id
          }
        }
      }
      game {
        _id
        title
        gameImage {
          url
        }
      }
      teams {
        image {
          url
        }
        _id
        name
        members {
          _id
          user {
            name
            username
            profilePicture {
              url
            }
          }
        }
        owner {
          ... on Organization {
            _id
            profileImage {
              url
            }
          }
        }
      }
      events {
        _id
        name
        bracketType
        startDate
        maxTeams
        state
        teams {
          _id
        }
      }
    }
  }
`;

export interface CreateLeagueDataResponse {
  createLeague: CreateLeagueData;
}

export interface CreateLeagueData {
  _id: string;
  game: {
    gameImage: {
      url: string;
    };
  };
}

export interface CreateLeagueVars {
  league: {
    name: string;
    description: string;
    imageUrl: string;
    game: string;
  };
}

export const CREATE_LEAGUE = gql`
  mutation CreateLeagueMutation($league: CreateLeagueArgs!) {
    createLeague(league: $league) {
      _id
      game {
        gameImage {
          url
        }
      }
    }
  }
`;

export interface GetLeaguesByOrgData {
  getLeaguesByOrganization: [League];
}

export interface GetLeaguesByOrgVars {
  organizationId: string;
}

export const GET_LEAGUES_BY_ORG = gql`
  query Query($organizationId: String!) {
    getLeaguesByOrganization(organizationId: $organizationId) {
      _id
      name
      imageUrl
      state
      description
      teams {
        _id
        name
      }
      game {
        _id
        title
        gameImage {
          url
        }
      }
    }
  }
`;

export interface GetLeaguesWithTeamsForOrgData {
  getLeaguesByOrganization: [
    {
      name: string;
      imageUrl: string;
      game: {
        title: string;
      };
      teams: [{ _id: string }];
      events: [{ _id: string }];
    }
  ];
}

export interface GetLeaguesWithTeamsForOrgVars {
  organizationId: string;
}

export const GET_LEAGUES_WITH_TEAMS_FOR_ORG = gql`
  query Query($organizationId: String!) {
    getLeaguesByOrganization(organizationId: $organizationId) {
      name
      imageUrl
      game {
        title
      }
      teams {
        _id
      }
      events {
        _id
      }
    }
  }
`;

export interface UpdateLeagueVars {
  leagueId: string | string[];
  updates: {
    name?: string;
    imageUrl?: string;
    description?: string;
    entryType?: "closed" | "invitational" | "open";
    game?: string;
    teams?: [string];
    rules?: string;
  };
}

export const UPDATE_LEAGUE = gql`
  mutation UpdateLeagueMutation($leagueId: String!, $updates: UpdateLeagueArgs!) {
    updateLeague(leagueId: $leagueId, updates: $updates) {
      _id
      name
      description
      rules
    }
  }
`;

export const DELETE_LEAGUE = gql`
  mutation deleteLeague($leagueId: String!) {
    deleteLeague(leagueId: $leagueId)
  }
`;

export const GET_USERS_LEAGUE_ORGS_AND_ROLES = gql`
  query Query {
    getUsersLeagueOrgsAndRoles {
      organizationRoles {
        organization {
          _id
          name
          profileImage {
            url
          }
        }
        role
      }
    }
  }
`;

export interface CanUserCreateLeaguesForOrgData {
  canUserCreateLeaguesForOrg: boolean;
}

export interface CanUserCreateLeaguesForOrgVars {
  organizationId: string;
}

export const CAN_USER_CREATE_LEAGUES_FOR_ORG = gql`
  query Query($organizationId: String!) {
    canUserCreateLeaguesForOrg(orgId: $organizationId)
  }
`;

export interface OrgLeaguesForDashboardData {
  canUserCreateLeaguesForOrg: boolean;
  getLeaguesByOrganization: [
    {
      _id: string;
    }
  ];
  getJoinedOrPendingLeaguesByOrg: [
    {
      league: {
        _id: string;
      };
      joinStatus: string;
    }
  ];
}

export interface OrgLeaguesForDashboardVars {
  orgId: string;
  joinStatus: string;
}

export const ORG_LEAGUES_FOR_DASHBOARD = gql`
  query Query($orgId: String!, $joinStatus: LeagueJoinStatus!) {
    canUserCreateLeaguesForOrg(orgId: $orgId)
    getLeaguesByOrganization(organizationId: $orgId) {
      _id
    }
    getJoinedOrPendingLeaguesByOrg(organizationId: $orgId, joinStatus: $joinStatus) {
      league {
        _id
      }
      joinStatus
    }
  }
`;

export interface GetJoinedOrPendingLeaguesByOrgData {
  getJoinedOrPendingLeaguesByOrg: [{ joinStatus: string; league: League }];
}

export interface GetJoinedOrPendingLeaguesByOrgVars {
  organizationId: string;
  joinStatus: "pending" | "joined" | "joinedAndPending";
}

export const GET_JOINED_OR_PENDING_LEAGUES_BY_ORG = gql`
  query Query($organizationId: String!, $joinStatus: LeagueJoinStatus) {
    getJoinedOrPendingLeaguesByOrg(organizationId: $organizationId, joinStatus: $joinStatus) {
      joinStatus
      league {
        _id
        name
        imageUrl
        game {
          title
        }
        owner {
          ... on Organization {
            _id
            name
          }
        }
        teams {
          _id
          name
        }
      }
    }
  }
`;
