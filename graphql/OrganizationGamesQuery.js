import { gql } from "@apollo/client";

export const ADD_GAMES_TO_ORGANIZATION = gql`
  mutation AddGamesToOrganizationMutation(
    $addGamesToOrganizationOrganizationId: String!
    $addGamesToOrganizationGameIds: [String]!
  ) {
    AddGamesToOrganization(
      organizationId: $addGamesToOrganizationOrganizationId
      gameIds: $addGamesToOrganizationGameIds
    ) {
      _id
      name
      games {
        _id
        gameImage {
          url
        }
        slug
      }
    }
  }
`;
