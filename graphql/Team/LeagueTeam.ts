import { gql } from "@apollo/client";

export interface LeagueTeam {
  _id: string;
  name: string;
  inviteCode: string;
  game: {
    gameImage: {
      url: string;
    };
  };
  owner: {
    _id: string;
    name: string;
    profileImage?: {
      url: string;
    };
  };
}

const leagueTeamFields = `
  _id
  name
  inviteCode
  game {
    gameImage {
      url
    }
  }
  owner {
    ... on Organization {
      _id
      name
      profileImage {
        url
      }
    }
  }
`;

export interface CreateTeamForLeagueVars {
  team: {
    name: string;
    owner: string;
    ownerType: string;
    game: string;
  };
  leagueId: string;
}

export interface CreateTeamForLeagueData {
  createTeamForLeague: LeagueTeam;
}

export const CREATE_TEAM_FOR_LEAGUE = gql`
  mutation createTeamForLeague($team: CreateTeamForLeagueArgs!, $leagueId: String!) {
    createTeamForLeague(team: $team, leagueId: $leagueId) {
      ${leagueTeamFields}
    }
  }
`;

export interface GetTeamByIdVars {
  teamId: string;
}

export interface GetTeamByIdData {
  getTeamById: LeagueTeam;
}

export const GET_TEAM_BY_ID = gql`
  query getTeamById($teamId: String!) {
    getTeamById(teamId: $teamId) {
      ${leagueTeamFields}
    }
  }
`;

export const ACCEPT_INVITE_CODE = gql`
  mutation acceptInvite($code: String!, $entityId: String!) {
    acceptInvite(code: $code, entityId: $entityId)
  }
`;

export const DELETE_LEAGUE_TEAM = gql`
  mutation Mutation($teamId: String!) {
    deleteTeam(teamId: $teamId)
  }
`;
