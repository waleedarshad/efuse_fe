import { gql } from "@apollo/client";

const memberFields = `
  _id
  role
  user {
    _id
    name
    username
    profilePicture {
      url
    }
  }
  team {
    _id
  }
  status
`;

export const DELETE_TEAM_MEMBER = gql`
  mutation DeleteTeamMemberMutation($teamMemberId: String!) {
    deleteTeamMember(teamMemberId: $teamMemberId) {
      _id
      team {
        _id
      }
    }
  }
`;

export const ADD_MEMBERS_TO_TEAM = gql`
  mutation AddUsersToTeamMutation($teamId: String!, $userIds: [String]!, $role: String) {
    addUsersToTeam(teamId: $teamId, userIds: $userIds, role: $role) {
      ${memberFields}
    }
  }
`;

export const UPDATE_TEAM_MEMBER = gql`
  mutation UpdateTeamMemberMutation($teamMemberId: String!, $teamMemberUpdates: UpdateTeamMemberArgs!) {
    updateTeamMember(teamMemberId: $teamMemberId, teamMemberUpdate: $teamMemberUpdates) {
      ${memberFields}
    }
  }
`;

// Queries to fetch results from GRAPHQL cache
export const ADD_TEAM_MEMBER_CACHE_QUERY = gql`
  fragment team on Team {
    members {
      ${memberFields}
    }
  }
`;
