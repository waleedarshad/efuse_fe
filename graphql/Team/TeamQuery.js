import { gql } from "@apollo/client";

const teamFields = `
  _id
  description
  image {
    url
  }
  name
  game {
    _id
    title
  }
  members {
    _id
    status
    role
    user {
      _id
      name
      username
      profilePicture {
        url
      }
    }
  }
`;

export const GET_TEAMS_BY_OWNER = gql`
  query Query($ownerId: String!) {
    getTeamsByOwner(owner: $ownerId) {
      ${teamFields}
    }
  }
`;

export const GET_TEAM_BY_ID = gql`
  query Query($teamId: String!) {
    getTeamById(teamId: $teamId) {
      ${teamFields}
    }
  }
`;

export const GET_PAGINATED_TEAMS_BY_OWNER = gql`
  query Query($ownerId: String, $page: Int, $pageSize: Int) {
    getPaginatedTeams(owner: $ownerId, page: $page, limit: $pageSize) {
      docs {
        ${teamFields}
      }
      limit
      page
      totalPages
      nextPage
      prevPage
      pagingCounter
      hasPrevPage
      hasNextPage
    }
  }
`;

export const CREATE_TEAM = gql`
  mutation CreateTeamMutation($teamArgs: CreateTeamArgs!, $userIds: [String!]) {
    createTeam(team: $teamArgs, userIds: $userIds) {
      ${teamFields}
    }
  }
`;

export const UPDATE_TEAM = gql`
  mutation UpdateTeamMutation($teamId: String!, $teamUpdates: UpdateTeamArgs!) {
    updateTeam(teamId: $teamId, teamUpdates: $teamUpdates) {
      ${teamFields}
    }
  }
`;

export const UPDATE_TEAM_CACHE_QUERY = gql`
  query updateTeam($id: String!) {
    updateTeam(id: $id) {
      ${teamFields}
    }
  }
`;
