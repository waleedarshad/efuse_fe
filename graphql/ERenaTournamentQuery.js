import { gql } from "@apollo/client";

const tournamentFields = `
    _id
    backgroundImageUrl
    bracketType
    brandLogoUrl
    brandName
    challongeLeaderboard
    createdAt
    currentRound
    endDatetime
    extensionLinkoutUrl
    extensionNamespace
    extensionPointer
    gameSelection
    hideLeaderboard
    imageLink1
    imageLink2
    imageLink3
    imageLink4
    imageLink5
    imageLink6
    imageUrl1
    imageUrl2
    imageUrl3
    imageUrl4
    imageUrl5
    imageUrl6
    isFinal
    landingPageUrl
    logoUrl
    logoURL
    ownerType
    owner {
      ... on Organization {
        _id
      }
      ... on User {
        _id
      }
    }
    playersPerTeam
    primaryColor
    rulesMarkdown
    secondaryColor
    slug
    startDatetime
    status
    totalRounds
    totalTeams
    tournamentDescription
    tournamentId
    tournamentName
    twitchChannel
    type
    updatedAt
    websiteUrl
    youtubeChannel
  `;

export const GET_ERENA_TOURNAMENTS = gql`
  query listERenaTournaments(
    $page: Int
    $limit: Int
    $sortBy: String
    $sortDirection: Int
    $status: [String]
    $startDateMin: Float
    $startDateMax: Float
    $endDateMin: Float
    $endDateMax: Float
  ) {
    listERenaTournaments(
      page: $page
      limit: $limit
      sortBy: $sortBy
      sortDirection: $sortDirection
      status: $status
      startDateMin: $startDateMin
      startDateMax: $startDateMax
      endDateMin: $endDateMin
      endDateMax: $endDateMax
    ) {
      docs {
        ${tournamentFields}
      }
      limit
      page
      totalPages
      nextPage
      prevPage
      pagingCounter
      hasPrevPage
      hasNextPage
    }
  }
`;

export const getTournamentFields = () => {
  return `
    ${tournamentFields}
    scores {
      _id
      erenaKind
      match {
        _id
      }
      ownerId
      ownerKind
      score
      tournament {
        _id
      }
    }
    teams {
      _id
      teamScore
      summedPlayerScore
      name
      isActive
      tournament {
        _id
      }
      type
      players {
        _id
        name
        type
        user {
          _id
          username
          twitch {
            username
          }
        }
      }
    }
    staff {
      _id
      role
      user {
        _id
        username
        name
      }
    }
    bracket {
      totalMatches
      tournament {
        _id
      }
      rounds{
        _id
        roundDate
        roundTitle
        roundNumber
        tournament {
          _id
        }
        matches {
          _id
          matchNumber
          tournament {
            _id
          }
          winner{
            _id
          }
          teams {
            _id
            teamScore
            summedPlayerScore
            name
            isActive
            tournament {
              _id
            }
            type
            players {
              _id
              name
            }
          }
          teamOneImage {
            contentType
            filename
            url
          }
          teamTwoImage {
            contentType
            filename
            url
          }
        }
      }
    }
    matches{
      _id
      matchNumber
      tournament {
        _id
      }
      winner{
        _id
      }
      teams {
        _id
      }
      teamOneImage {
        contentType
        filename
        url
      }
      teamTwoImage {
        contentType
        filename
        url
      }
    }
    requirements {
      name
      description
      isActive
      target
      property
      custom
    }
    opportunity{
      _id
    }
    `;
};

export const GET_ERENA_TOURNAMENT = gql`
  query getERenaTournament($tournamentIdOrSlug: String) {
    getERenaTournament(tournamentIdOrSlug: $tournamentIdOrSlug){
      ${getTournamentFields()}
    }
  }
`;

export const CREATE_TOURNAMENT = gql`
  mutation createTournament($body: ERenaTournamentInputs) {
    createTournament(body: $body) {
      ${getTournamentFields()}
    }
  }
`;

export const UPDATE_TOURNAMENT = gql`
  mutation updateTournament($tournamentIdOrSlug: String, $files: ERenaTournamentFileUploaderInputs, $body: ERenaTournamentInputs){
    updateTournament(tournamentIdOrSlug: $tournamentIdOrSlug, files: $files, body: $body){
      ${getTournamentFields()}
    }
  }
`;

export const UPDATE_TOURNAMENT_AD = gql`
  mutation updateTournamentAd($tournamentIdOrSlug: String, $link: String, $imageURL: String, $index: Int){
    updateTournamentAd(tournamentIdOrSlug: $tournamentIdOrSlug, link:$link, imageURL: $imageURL, index: $index){
      ${getTournamentFields()}
    }
  }
`;

export const DELETE_TOURNAMENT_AD = gql`
  mutation removeTournamentAd($tournamentIdOrSlug: String, $index: Int){
    removeTournamentAd(tournamentIdOrSlug: $tournamentIdOrSlug, index: $index){
      ${getTournamentFields()}
    }
  }
`;

// Query to fetch results from cache
export const TOURNAMENT_CACHE_FRAGMENT = gql`
  fragment tournament on ERenaTournament {
    _id
    teams {
      _id
      isActive
      name
      players {
        _id
        name
        isActive
        team {
          _id
        }
        tournament {
          _id
        }
        type
        user {
          _id
          username
        }
      }
      summedPlayerScore
      teamScore
      tournament {
        _id
      }
      type
    }
  }
`;

export const TOURNAMENT_DESIGN_CACHE_FRAGMENT = gql`
  fragment tournament on ERenaTournament {
    _id
    hideLeaderboard
    backgroundImageUrl
    brandLogoUrl
    primaryColor
    secondaryColor
  }
`;
