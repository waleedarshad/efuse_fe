import { gql } from "@apollo/client";

export const PROMOTED_LIST = gql`
  query discoverList {
    promotedList {
      imageUrl
      title
      subTitle
      summary
      stat
      statName
      redirectLink
      twitchChannelId
      badgeText
      type
    }
  }
`;
