/* eslint-disable no-shadow */
/* eslint-disable import/no-cycle */
import { Game } from "./Game";
import { Organization } from "./Organization";

export interface LeagueMatch {
  _id: string;
  teams: Team[];
  finalScores: LeagueGameScore[];
}

export interface LeagueGameScore {
  _id: string;
  gameNumber: number;
  image: string;
  type: SubmittedScoreType;
  scores: {
    _id: string;
    value: number;
    competitor: Team;
  }[];
  creator: Organization | Team;
}

export interface Round {
  name: string;
  createdAt: Date;
  matches: LeagueMatch;
  startDate: Date;
}

export interface LeagueEventPoolBracket {
  name: string;
  rounds: Round[];
}

export interface LeagueEventPool {
  _id: string;
  name: string;
  bracket?: LeagueEventPoolBracket;
  teams: Team[];
}

export interface Member {
  _id: string;
  isBanned?: boolean;
  user: {
    _id: string;
    name: string;
    username?: string;
    profilePicture: {
      url: string;
    };
  };
}

export interface Team {
  _id: string;
  name: string;
  description: string;
  game: Game;
  image: {
    url: string;
  };
  members: Member[];
  owner: Organization;
}

export enum LeagueBracketType {
  POINT_RACE = "pointRace",
  SINGLE_ELIM = "singleElim",
  DOUBLE_ELIM = "doubleElim",
  ROUND_ROBIN = "roundRobin",
  SWISS = "swiss"
}

export enum LeagueEventState {
  ACTIVE = "active",
  CANCELLED = "cancelled",
  FINISHED = "finished",
  PENDING = "pending",
  PENDING_POOLS = "pendingPools",
  PENDING_TEAMS = "pendingTeams",
  PENDING_BRACKET = "pendingBracket"
}

export enum SubmittedScoreType {
  CONFLICT = "conflict",
  FINALIZED = "finalized",
  SUBMITTED = "submitted"
}

export interface LeagueEvent {
  _id: string;
  bracketType: string;
  description: string;
  gamesPerMatch: number;
  maxTeams: number;
  name: string;
  teams: Team[];
  startDate: Date;
  state?: LeagueEventState;
  timingMode: string;
  pools: LeagueEventPool[];
  league: League;
  rules: string;
}

export interface League {
  _id: string;
  name: string;
  description: string;
  game: Game;
  imageUrl?: string;
  state: string;
  rules: string;
  owner: Organization;
  teams: Team[];
  events: LeagueEvent[];
}
