import { File } from "./File";

export interface Media {
  _id: string;
  file: File;
  profile: string;
}
