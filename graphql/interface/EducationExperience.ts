export interface UserEducationExperience {
  level: number;
  subTitle: string;
  school: string;
  degree: string;
  fieldofstudy: string;
  grade: string;
  startDate: Date;
  endDate: Date;
  present: boolean;
  image: File;
  description: string;
}
