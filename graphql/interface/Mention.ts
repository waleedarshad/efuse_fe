export interface Mention {
  user: string;
  childIndex: number;
  display: string;
  id: string;
  index: number;
  plainTextIndex: number;
  type: string;
}
