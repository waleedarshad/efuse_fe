// eslint-disable-next-line import/no-cycle
import { Member, Team } from "./League";
import { User } from "./User";

export interface Organization {
  _id: string;
  name: string;
  shortName: string;
  description: string;
  createdAt: string;
  games: [string];
  associatedGames: [string];
  organizationType: string;
  status: string;
  profileImage: {
    url: string;
  };
  headerImage: {
    url: string;
  };
  teams: [Team];
  members: [Member];
  user: {
    _id: string;
  };
  captains: [User];
  leaguesInviteCode: string;
  currentUserRole?: OrganizationUserRole;
}

export interface OrganizationUserRoles {
  user: User;
  organizationRoles: [OrganizationRole];
}

export interface OrganizationRole {
  organization: Organization;
  role: OrganizationUserRole;
}

export enum OrganizationUserRole {
  CAPTAIN = "captain",
  MEMBER = "member",
  OWNER = "owner",
  NONE = "none"
}
