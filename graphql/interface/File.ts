export interface File {
  filename: string;
  contentType: string;
  url: string;
  edit?: boolean;
}
