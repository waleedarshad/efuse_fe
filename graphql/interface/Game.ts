export interface AssociatedGame {
  _id: string;
  description: string;
  kind: string;
  slug: string;
  title: string;
  createdAt: Date;
  updatedAt: Date;
  gameImage: File;
}

export interface Game {
  _id: string;
  title: string;
  gameImage: {
    url: string;
  };
}
