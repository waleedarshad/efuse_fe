interface TwitchWebhookSubscription {
  isActive: boolean;
  callback: string;
  expiresAt: string;
}

interface TwitchStream {
  isLive: boolean;
  title: string;
  viewerCount: number;
  startedAt: Date;
  endedAt: Date;
}

export interface TwitchAccountInfo {
  owner: string;

  ownerKind: string;

  kind: string;

  verified: boolean;

  displayName: string;

  username: string;

  accountId: string;

  link: string;

  description: string;

  followers: number;

  profileImage: string;

  accountEmail: string;

  viewCount: number;

  accountType: string;

  broadcasterType: string;

  sendStartStreamMessage: boolean;

  webhookSubscription: TwitchWebhookSubscription;

  stream: TwitchStream;
}
