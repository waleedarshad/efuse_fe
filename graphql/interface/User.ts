import { File } from "./File";
import { TwitchAccountInfo } from "./TwitchAccountInfo";
import { UserEducationExperience } from "./EducationExperience";

interface BadgeField {
  slug: string;
  active: boolean;
  name: string;
}

interface Badge {
  badge: BadgeField;
}

interface UserBadge {
  user: User;
  badge: Badge;
}

export interface User {
  _id: string;

  name: string;

  bio: string;

  verified: boolean;

  profilePicture: File;

  headerImage: File;

  username: string;

  createdAt: Date;

  url: string;

  score: number;

  tokenBalance: number;

  email: string;

  gender: string;

  address: string;

  dateOfBirth: Date;

  firstName: string;

  lastName: string;

  profileImage: string;

  streak: number;

  online: boolean;

  badges: [UserBadge];

  educationExperience: [UserEducationExperience];

  twitch: TwitchAccountInfo;
}
