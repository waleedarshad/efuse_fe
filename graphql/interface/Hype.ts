import { File } from "./File";

export interface HypeUser {
  id: string;
  name: string;
  username: string;
  profilePicture: File;
  verified: boolean;
  currentUserFollows: boolean;
  isFollowedByUser: boolean;
}

export interface Hype {
  _id: string;
  user: HypeUser;
  objId: string;
  objType: string;
  platform: string;
  hypeCount: number;
}
