import gql from "graphql-tag";

// eslint-disable-next-line import/prefer-default-export
export const CREATE_REPORT = gql`
  mutation CreateReportMutation($reportItem: String!, $reportKind: String!, $reportReason: String) {
    createReport(reportItem: $reportItem, reportkind: $reportKind, reportReason: $reportReason) {
      reportItem
      reportkind
      reportReason
    }
  }
`;
