import { gql } from "@apollo/client";

export const GET_PROFILE_NOTES = gql`
  query Query($getNotesRelatedDoc: String!, $getNotesOwner: String!) {
    getNotes(relatedDoc: $getNotesRelatedDoc, owner: $getNotesOwner) {
      _id
      createdAt
      author {
        _id
        name
        profilePicture {
          url
        }
      }
      content
      ownerType
      updatedAt
      owner {
        ... on Organization {
          _id
          name
          profileImage {
            url
          }
        }
      }
    }
  }
`;

export const CREATE_PROFILE_NOTE = gql`
  mutation CreateTeamMutation($createNote: CreateNoteArgs!) {
    createNote(note: $createNote) {
      _id
      author {
        _id
        name
        profilePicture {
          url
        }
      }
      content
      owner {
        ... on Organization {
          _id
          name
          profileImage {
            url
          }
        }
      }
      ownerType
      updatedAt
      relatedModel
    }
  }
`;

export const MARK_RECRUIT_STARRED = gql`
  mutation CreateFavoriteMutation($starData: CreateFavorite!) {
    createFavorite(favorite: $starData) {
      _id
    }
  }
`;

export const DELETE_STARRED_RECRUIT = gql`
  mutation DeleteFavoriteMutation($id: String!) {
    deleteFavorite(favoriteId: $id)
  }
`;

export const GET_FAVORITE = gql`
  query Query($owner: String!, $relatedDoc: String!) {
    getFavorite(owner: $owner, relatedDoc: $relatedDoc) {
      _id
    }
  }
`;
