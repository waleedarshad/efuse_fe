import { gql } from "@apollo/client";

export const ENTITY_HAS_PERMISSION = gql`
  query Query($params: HasPermissionInput!) {
    entityHasPermission(params: $params)
  }
`;

export const ENTITY_HAS_ROLE = gql`
  query Query($params: HasRoleInput!) {
    entityHasRole(params: $params)
  }
`;
