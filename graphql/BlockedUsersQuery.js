import { gql } from "@apollo/client";

export const BLOCK_USER = gql`
  mutation blockUser($blockeeId: String!) {
    blockUser(blockeeId: $blockeeId) {
      blockerId
      blockeeId
    }
  }
`;

export const GET_BLOCKED_USERS = gql`
  query blockedUsers {
    blockedUsers {
      _id
      profilePicture {
        url
        contentType
      }
      username
      name
    }
  }
`;

export const UNBLOCK_USER = gql`
  mutation unBlockUser($blockeeId: String!) {
    unBlockUser(blockeeId: $blockeeId)
  }
`;
