import { Mention } from "../interface/Mention";
import { User } from "../interface/User";
import { Giphy } from "../interface/Giphy";
import { Hype } from "../interface/Hype";
import { Media } from "../interface/Media";

// eslint-disable-next-line import/no-cycle
import { PaginatedHypeUser, Post } from "../feeds/Models";

export interface CommentModel {
  _id: string;
  content?: string;
  user: User;
  commentable: Post;
  commentableType: string;
  mentions?: Mention[];

  giphy?: Giphy;
  parent?: string;
  deleted: boolean;
  platform: string;
  likes: number;
  image: File;
  hype: Hype;
  thread?: {
    totalDocs: number;
    hasNextPage: boolean;
    page: number;
    docs: CommentModel[];
  };
  media: Media;
  createdAt: Date;
  updatedAt: Date;
  threadLoading?: boolean;
  enableThreadReply?: boolean;
  enableEdit?: boolean;
  loadHypeUser?: boolean;
  hypedUser: PaginatedHypeUser;
}
