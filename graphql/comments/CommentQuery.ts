import { gql } from "@apollo/client";
import { Giphy } from "../interface/Giphy";
import { Mention } from "../interface/Mention";
import { POST_COMMENT_FRAGMENT } from "./CommentFieldsFragment";
import { CommentModel } from "./Models";
import { File } from "../interface/File";

interface CommentDocs {
  totalDocs: number;
  hasNextPage: boolean;
  page: number;
  docs: [CommentModel];
}
export interface PostCommentData {
  GetPaginatedComments: CommentDocs;
}

export interface PostCommentThreadData {
  GetPaginatedThreadComments: CommentDocs;
}

export interface PostCommentVars {
  feedId: string;
  page: number;
  limit: number;
  parentId?: string;
}

export const GET_POST_COMMENTS = gql`
  ${POST_COMMENT_FRAGMENT}
  query Query($feedId: String!, $page: Int, $limit: Int) {
    GetPaginatedComments(feedId: $feedId, page: $page, limit: $limit) {
      hasNextPage
      page
      docs {
        ...CommentFields
      }
    }
  }
`;

export const GET_POST_THREAD_COMMENTS = gql`
  ${POST_COMMENT_FRAGMENT}
  query Query($feedId: String!, $parentId: String!, $page: Int, $limit: Int) {
    GetPaginatedThreadComments(feedId: $feedId, parentId: $parentId, page: $page, limit: $limit) {
      totalDocs
      hasNextPage
      page
      docs {
        ...CommentFields
      }
    }
  }
`;

export interface CommentBody {
  CreateFeedComment: CommentModel;
  DeleteFeedComment: CommentModel;
  UpdateFeedComment: CommentModel;
}

export interface PostUpdateBody {
  content: String;
  file?: File;
  giphy?: Giphy;
  commentId?: string;
}

export interface PostCommentBody extends PostUpdateBody {
  feedId: string;
  content: String;
  mentions: Mention[];
  file?: File;
  giphy?: Giphy;
  platform?: string;
  parent?: string;
  commentId?: string;
}
export interface CommentBodyVar {
  body?: PostCommentBody;
  commentId?: string;
}

export const ADD_FEED_COMMENTS = gql`
  ${POST_COMMENT_FRAGMENT}
  mutation CreateFeedComment($body: CommentInput) {
    CreateFeedComment(body: $body) {
      ...CommentFields
    }
  }
`;

export interface CommentUpdateBodyVar {
  body?: PostUpdateBody;
  commentId?: string;
}
export const UPDATE_FEED_COMMENTS = gql`
  ${POST_COMMENT_FRAGMENT}
  mutation UpdateFeedComment($body: CommentUpdateInput) {
    UpdateFeedComment(body: $body) {
      ...CommentFields
    }
  }
`;

export const DELETE_FEED_COMMENTS = gql`
  ${POST_COMMENT_FRAGMENT}
  mutation DeleteFeedComment($commentId: String!) {
    DeleteFeedComment(commentId: $commentId) {
      ...CommentFields
    }
  }
`;
