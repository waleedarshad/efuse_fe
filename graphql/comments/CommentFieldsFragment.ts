import { gql } from "@apollo/client";
import { POST_FIELDS_FRAGMENT } from "../feeds/postFieldsFragment";
import { FILE_FRAGMENT } from "../fragments/FileFragment";
import { MENTION_FIELDS_FRAGMENT } from "../fragments/MentionFragment";
import { GIPHY_FIELDS_FRAGMENT } from "../fragments/GiphyFragment";
import { HYPE_FRAGMENT } from "../fragments/HypeFragment";

const CommentFields = `
    _id
    content
    user {
      _id
      bio
      name
      username
      verified
      online
      profileImage
      streak
    }
    commentable {
      ...PostFields
    }
    commentableType
    image {
      ...FileFields
    }
    mentions {
      ...MentionFields
    }
    likes
    giphy {
      ...GiphyFields
    }
    parent
    hype {
      ...HypeFields
    }

    platform
    media {
      file {
        ...FileFields
      }
    }
    createdAt
    updatedAt
    deleted
`;

// eslint-disable-next-line import/prefer-default-export
export const POST_COMMENT_FRAGMENT = gql`
  ${POST_FIELDS_FRAGMENT}
  ${FILE_FRAGMENT}
  ${MENTION_FIELDS_FRAGMENT}
  ${GIPHY_FIELDS_FRAGMENT}
  ${HYPE_FRAGMENT}
  fragment CommentFields on Comment {
    ${CommentFields}
    thread {
      hasNextPage
      totalDocs
      page
      docs {
        ${CommentFields}
      }
    }

  }
`;
