import gql from "graphql-tag";
import { Hype, HypeUser } from "../interface/Hype";
import { HYPE_USER_FRAGMENT } from "../fragments/HypeUserFragment";

export interface CommentHypeData {
  HypeComment: {
    newHype: boolean;
    hype: Hype;
  };
}

export interface CommentHypeVar {
  commentId: string;
  hypeCount: number;
  platform: string;
}

// eslint-disable-next-line import/prefer-default-export
export const HYPE_COMMENT = gql`
  mutation HypeComment($commentId: String!, $hypeCount: Int!, $platform: String) {
    HypeComment(commentId: $commentId, hypeCount: $hypeCount, platform: $platform) {
      newHype
      hype {
        _id
      }
    }
  }
`;

export interface UnHypeCommentData {
  UnHypeComment: {
    hype: Hype;
  };
}

export interface UnHypeCommentVar {
  commentId: string;
}

// eslint-disable-next-line import/prefer-default-export
export const UNHYPE_COMMENT = gql`
  mutation UnHypeComment($commentId: String!) {
    UnHypeComment(commentId: $commentId) {
      _id
    }
  }
`;

export interface PaginatedHypeUser {
  hasNextPage: boolean;
  page: number;
  docs: HypeUser[];
}

export interface HypeUserData {
  getCommentHypedUsers?: PaginatedHypeUser;
  getFeedHypedUsers?: PaginatedHypeUser;
}

export interface HypeUserVar {
  feedId?: string;
  commentId?: string;
  page: number;
  limit: number;
}

export const WHO_HYPED_COMMENT = gql`
  ${HYPE_USER_FRAGMENT}
  query getCommentHypedUsers($commentId: String!, $page: Int, $limit: Int) {
    getCommentHypedUsers(commentId: $commentId, page: $page, limit: $limit) {
      hasNextPage
      totalDocs
      page
      docs {
        ...HypeUserFields
      }
    }
  }
`;

export const WHO_HYPED_POST = gql`
  ${HYPE_USER_FRAGMENT}
  query getFeedHypedUsers($feedId: String!, $page: Int, $limit: Int) {
    getFeedHypedUsers(feedId: $feedId, page: $page, limit: $limit) {
      hasNextPage
      totalDocs
      page
      docs {
        ...HypeUserFields
      }
    }
  }
`;
