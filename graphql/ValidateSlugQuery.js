import { gql } from "@apollo/client";

export const VALIDATE_SLUG = gql`
  query Query($slug: String!, $slugType: String!) {
    ValidateSlug(slug: $slug, slugType: $slugType) {
      valid
      error
    }
  }
`;
