import { gql } from "@apollo/client";

// eslint-disable-next-line import/prefer-default-export
export const ADD_GAMES_TO_USER = gql`
  mutation AddGamesToUserMutation($addGamesToUserGameIds: [String]!) {
    AddGamesToUser(gameIds: $addGamesToUserGameIds) {
      _id
    }
  }
`;
