import { gql } from "@apollo/client";

export const GET_GAMES = gql`
  query getGames {
    getGames {
      _id
      description
      createdAt
      gameImage {
        filename
        url
        __typename
      }
      kind
      slug
      title
      updatedAt
      __typename
    }
  }
`;

export const GET_PIPELINE_GAMES = gql`
  query getPipelineGames {
    getPipelineGames {
      description
      gameImage {
        url
      }
      slug
      title
      pipelineInfo {
        pipelineEnabled
        roles
      }
    }
  }
`;
