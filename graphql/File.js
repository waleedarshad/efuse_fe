import { gql } from "@apollo/client";

// eslint-disable-next-line import/prefer-default-export
export const IMAGE_FIELDS = gql`
  fragment ImageFields on File {
    filename
    contentType
    url
  }
`;
