import { gql } from "@apollo/client";

const GET_ARTICLE_CATEGORIES = gql`
  query getCategories {
    getCategories {
      _id
      slug
      name
    }
  }
`;

export default GET_ARTICLE_CATEGORIES;
