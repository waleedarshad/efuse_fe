import { gql } from "@apollo/client";
import { Organization } from "../interface/Organization";

export interface CreateOrganizationDataResponse {
  createOrganization: {
    _id: string;
    name: string;
    shortName: string;
  };
}

export interface CreateOrganizationVars {
  organization: {
    name: string;
    shortName: string;
    associatedGames: [string];
    organizationType: string;
    status: string;
  };
}

export const CREATE_ORGANIZATION = gql`
  mutation CreateOrganizationMutation($organization: CreateOrganizationArgs!) {
    createOrganization(organization: $organization) {
      _id
      name
      shortName
    }
  }
`;
export interface ValidateOrgIdData {
  getOrganizationById: {
    _id: string;
  };
}

export interface ValidateOrgIdVars {
  orgId: string;
}

export const VALIDATE_ORG_ID = gql`
  query Query($orgId: String!) {
    getOrganizationById(organizationId: $orgId) {
      _id
    }
  }
`;

export interface getUserOrganizationsVars {
  userId: string;
  includeOrgsWhereMember: boolean;
}

export interface getUserOrganizationsData {
  getUserOrganizations: [Organization];
}

export const GET_USER_ORGANIZATIONS = gql`
  query Query($userId: String!, $includeOrgsWhereMember: Boolean) {
    getUserOrganizations(userId: $userId, includeOrgsWhereMember: $includeOrgsWhereMember) {
      _id
      name
      followersCount
      membersCount
      profileImage {
        url
      }
      teams {
        _id
      }
      captains {
        _id
      }
      currentUserRequestToJoin {
        status
      }
      headerImage {
        url
      }
      verified {
        status
      }
      user {
        _id
      }
      shortName
      publishStatus
      status
      organizationType
    }
  }
`;

export interface GetOrgInfoByIdData {
  getOrganizationById: Organization;
}

export interface GetOrgInfoByIdVars {
  organizationId: string;
}

export const GET_ORG_INFO_BY_ID = gql`
  query Query($organizationId: String!) {
    getOrganizationById(organizationId: $organizationId) {
      _id
      createdAt
      name
      shortName
      description
      user {
        _id
      }
      captains {
        _id
      }
      members {
        _id
        isBanned
        user {
          _id
          name
          username
          profilePicture {
            url
          }
        }
      }
      profileImage {
        url
      }
      headerImage {
        url
      }
      organizationType
      status
      teams {
        image {
          url
        }
        _id
        name
        members {
          _id
          user {
            name
            username
            profilePicture {
              url
            }
          }
        }
      }
    }
  }
`;

export const GET_ORG_INVITE_INFO = gql`
  query Query($organizationId: String!) {
    getOrganizationById(organizationId: $organizationId) {
      _id
      name
      leaguesInviteCode
      profileImage {
        url
      }
    }
  }
`;
