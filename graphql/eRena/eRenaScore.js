import { gql } from "@apollo/client";

export const ADD_ERENA_SCORE = gql`
  mutation createScore($tournamentIdOrSlug: String!, $body: ERenaScoreInput!) {
    createScore(tournamentIdOrSlug: $tournamentIdOrSlug, body: $body) {
      _id
      erenaKind
      ownerKind
      ownerId
      match {
        _id
      }
      tournament {
        _id
      }
      score
    }
  }
`;

export const UPDATE_ERENA_SCORE = gql`
  mutation updateScore($id: ID!, $body: ERenaScoreInputValue!) {
    updateScore(id: $id, body: $body) {
      _id
      erenaKind
      ownerKind
      ownerId
      match {
        _id
      }
      tournament {
        _id
      }
      score
    }
  }
`;

export const DELETE_ERENA_SCORE = gql`
  mutation deleteScore($id: ID!) {
    deleteScore(id: $id) {
      _id
      erenaKind
      ownerKind
      ownerId
      match {
        _id
      }
      tournament {
        _id
      }
      score
    }
  }
`;

export const BULK_CREATE_ERENA_SCORES = gql`
  mutation bulkCreateScores($tournamentIdOrSlug: String!, $body: [ERenaScoreInput]!) {
    bulkCreateScores(tournamentIdOrSlug: $tournamentIdOrSlug, body: $body) {
      _id
      erenaKind
      ownerKind
      ownerId
      match {
        _id
      }
      tournament {
        _id
      }
      score
    }
  }
`;

export const BULK_UPDATE_ERENA_SCORES = gql`
  mutation bulkUpdateScores($tournamentIdOrSlug: String!, $body: [ERenaScoreInput]!) {
    bulkUpdateScores(tournamentIdOrSlug: $tournamentIdOrSlug, body: $body) {
      _id
      erenaKind
      ownerKind
      ownerId
      match {
        _id
      }
      tournament {
        _id
      }
      score
    }
  }
`;

// update data in graphql cache
export const ADD_SCORE_CACHE_FRAGMENT = gql`
  fragment score on ERenaTournament {
    _id
    erenaKind
    ownerKind
    ownerId
    match {
      _id
    }
    tournament {
      _id
    }
    score
  }
`;

export const UPDATE_SCORE_CACHE_FRAGMENT = gql`
  query updateScore($id: String) {
    updateScore(id: $id) {
      score
    }
  }
`;
