import { gql } from "@apollo/client";

export const ADD_ERENA_TEAMS = gql`
  mutation createErenaTeam($tournamentIdOrSlug: String!, $body: ERenaTeamInput!) {
    createErenaTeam(tournamentIdOrSlug: $tournamentIdOrSlug, body: $body) {
      _id
      name
      isActive
      tournament {
        _id
      }
      type
      teamScore
      summedPlayerScore
      players {
        _id
        name
        isActive
        team {
          _id
        }
        type
        tournament {
          _id
        }
        score
      }
    }
  }
`;

export const DELETE_ERENA_TEAMS = gql`
  mutation deleteErenaTeam($id: ID!) {
    deleteErenaTeam(id: $id) {
      _id
      tournament {
        _id
      }
    }
  }
`;

export const UPDATE_ERENA_TEAMS = gql`
  mutation updateErenaTeam($id: ID!, $body: ERenaTeamInputAttributes!) {
    updateErenaTeam(id: $id, body: $body) {
      _id
      name
      isActive
      tournament {
        _id
      }
      type
      teamScore
      summedPlayerScore
      players {
        _id
        name
        isActive
        team {
          _id
        }
        type
        tournament {
          _id
        }
        score
      }
    }
  }
`;

// Queries to fetch results from GRAPHQL cache
export const TEAM_UPDATE_CACHE_QUERY = gql`
  query updateErenaTeam($id: String!) {
    updateErenaTeam(id: $id) {
      isActive
      name
      players {
        _id
        name
        isActive
        team {
          _id
        }
        type
        tournament {
          _id
        }
        score
      }
      summedPlayerScore
      teamScore
      tournament {
        _id
      }
      type
    }
  }
`;
