import { gql } from "@apollo/client";

export const ADD_ERENA_PLAYER = gql`
  mutation createPlayer($teamId: ID!, $body: PlayerArgs!) {
    createPlayer(teamId: $teamId, body: $body) {
      _id
      name
      isActive
      team {
        _id
      }
      type
    }
  }
`;

export const UPDATE_ERENA_PLAYER = gql`
  mutation updatePlayer($id: ID!, $body: PlayerUpdateArgs!) {
    updatePlayer(id: $id, body: $body) {
      _id
      name
      isActive
      team {
        _id
      }
      type
    }
  }
`;

export const DELETE_ERENA_PLAYER = gql`
  mutation deletePlayer($id: ID!) {
    deletePlayer(id: $id) {
      _id
      name
      team {
        _id
      }
      type
    }
  }
`;

// Queries to fetch results from GRAPHQL cache
export const ADD_PLAYER_CACHE_QUERY = gql`
  fragment team on ERenaTeam {
    players {
      _id
      name
      isActive
      team {
        _id
      }
      tournament {
        _id
      }
      type
      user {
        _id
      }
    }
  }
`;

export const UPDATE_PLAYER_CACHE_QUERY = gql`
  query updatePlayer($id: String!) {
    updatePlayer(id: $id) {
      isActive
      name
    }
  }
`;
