import { gql } from "@apollo/client";

export const CREATE_STAFF_MEMBER = gql`
  mutation createStaff($body: ERenaStaffInput!, $tournamentIdOrSlug: String!) {
    createStaff(body: $body, tournamentIdOrSlug: $tournamentIdOrSlug) {
      _id
      user {
        _id
        name
        username
      }
      tournament {
        _id
      }
      role
    }
  }
`;

export const DELETE_STAFF_MEMBER = gql`
  mutation deleteStaff($id: ID!) {
    deleteStaff(id: $id) {
      _id
      tournament {
        _id
      }
      role
    }
  }
`;

export const ERENA_TOURNAMENT_STAFF_FRAGMENT = gql`
  fragment staff on ERenaTournament {
    staff {
      _id
      user {
        _id
        name
        username
      }
      tournament {
        _id
      }
      role
    }
  }
`;
