import { gql } from "@apollo/client";
import { IMAGE_FIELDS } from "../File";

export const ERENA_MATCH_FIELDS = gql`
  ${IMAGE_FIELDS}
  fragment MatchFields on ERenaMatch {
    _id
    matchDate
    winner {
      _id
    }
    matchNumber
    tournament {
      _id
    }
    teams {
      _id
    }
    teamOneImage {
      ...ImageFields
    }
    teamTwoImage {
      ...ImageFields
    }
  }
`;

export const UPDATE_MATCH = gql`
  ${ERENA_MATCH_FIELDS}
  mutation updateMatch($id: ID!, $body: UpdateERenaMatchInput!) {
    updateMatch(id: $id, body: $body) {
      currentMatch {
        ...MatchFields
      }
      nextMatch {
        ...MatchFields
      }
    }
  }
`;

export const SAVE_SCREENSHOT = gql`
  ${ERENA_MATCH_FIELDS}
  mutation saveScreenShot($id: ID!, $body: screenshotsInputs) {
    saveScreenShot(id: $id, body: $body) {
      ...MatchFields
    }
  }
`;
