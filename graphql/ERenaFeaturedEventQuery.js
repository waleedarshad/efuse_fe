import { gql } from "@apollo/client";

const GET_ERENA_FEATURED_EVENT = gql`
  query getFeaturedEvent {
    getFeaturedEvent {
      _id
      tournamentId
      tournamentName
      slug
      tournamentDescription
      twitchChannel
      backgroundImageUrl
      brandLogoUrl
      landingPageUrl
      startDatetime
      endDatetime
      rulesMarkdown
      brandName
    }
  }
`;

export default GET_ERENA_FEATURED_EVENT;
