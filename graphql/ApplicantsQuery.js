import { gql } from "@apollo/client";

// eslint-disable-next-line import/prefer-default-export
export const UPDATE_APPLICANT_STATUS = gql`
  mutation updateApplicantStatus($applicantId: ID!, $opportunityId: ID!, $sendEmail: Boolean, $status: String) {
    updateApplicantStatus(
      applicantId: $applicantId
      opportunityId: $opportunityId
      sendEmail: $sendEmail
      status: $status
    ) {
      status
      user {
        _id
      }
    }
  }
`;

export const GET_APPLICANTS_QUERY = gql`
  query Query($idOrShortName: String, $page: Int, $limit: Int, $status: String) {
    getOpportunityByIdOrShortName(idOrShortName: $idOrShortName) {
      paginatedApplicants(page: $page, limit: $limit, status: $status) {
        totalDocs
        limit
        page
        totalPages
        nextPage
        prevPage
        pagingCounter
        hasPrevPage
        hasNextPage
        docs {
          _id
          entityType
          status
          favorite
          candidateQuestions {
            index
            question
            media {
              filename
              contentType
              url
            }
          }
          entity {
            ... on Opportunity {
              _id
            }
          }
          user {
            _id
            name
            bio
            profilePicture {
              filename
              contentType
              url
            }
            username
            online
            address
            educationExperience {
              level
              subTitle
              school
              degree
              fieldofstudy
            }
          }
        }
      }
    }
  }
`;
