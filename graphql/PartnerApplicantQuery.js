import { gql } from "@apollo/client";

export const CAN_APPLY_PARTNER_BADGE = gql`
  query CanApplyForPartnerBadge {
    CanApplyForPartnerBadge {
      canApply
      reasons
      partnerApplicant {
        status
      }
    }
  }
`;

export const APPLY_PARTNER_BADGE = gql`
  mutation ApplyForPartnerBadge {
    ApplyForPartnerBadge {
      status
      user {
        username
      }
    }
  }
`;
