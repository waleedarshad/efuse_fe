import { gql } from "@apollo/client";

// eslint-disable-next-line import/prefer-default-export
export const USER_FIELDS_FRAGMENT = gql`
  fragment UserFields on User {
    _id
    bio
    name
    username
    verified
    online
    profileImage
    streak
    twitch {
      username
    }
    badges {
      badge {
        slug
        active
        name
      }
    }
  }
`;
