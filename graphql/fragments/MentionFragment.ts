import { gql } from "@apollo/client";

// eslint-disable-next-line import/prefer-default-export
export const MENTION_FIELDS_FRAGMENT = gql`
  fragment MentionFields on Mention {
    type
    display
    id
  }
`;
