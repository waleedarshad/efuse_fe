import { gql } from "@apollo/client";

// eslint-disable-next-line import/prefer-default-export
export const ALGOLIA_FRAGMENT = gql`
  fragment AlgoliaSpecificFields on SearchResultFields {
    exhaustiveFacetsCount
    exhaustiveNbHits
    facets
    facets_stats
    hitsPerPage
    index
    nbHits
    nbPages
    page
    params
    processingTimeMS
    query
    renderingContent
  }
`;
