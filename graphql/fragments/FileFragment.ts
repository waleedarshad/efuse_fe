import { gql } from "@apollo/client";

// eslint-disable-next-line import/prefer-default-export
export const FILE_FRAGMENT = gql`
  fragment FileFields on File {
    filename
    contentType
    url
  }
`;
