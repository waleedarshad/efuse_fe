import { gql } from "@apollo/client";
import { FILE_FRAGMENT } from "./FileFragment";

// eslint-disable-next-line import/prefer-default-export
export const HYPE_USER_FRAGMENT = gql`
  ${FILE_FRAGMENT}
  fragment HypeUserFields on HypeUser {
    id
    name
    username
    profilePicture {
      ...FileFields
    }
    verified
    currentUserFollows
    isFollowedByUser
  }
`;
