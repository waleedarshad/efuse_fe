import { gql } from "@apollo/client";

export const ClippyFragment = gql`
  fragment ClippyFragment on Clippy {
    token
    user {
      _id
      username
      name
      twitch {
        displayName
        username
        accountId
      }
    }
    commands {
      _id
      active
      command
      kind
      sortOrder
      dateRange
      allowedRoles
      queueClips
      minimumViews
    }
  }
`;

export const ClippyShoutoutClipFragment = gql`
  fragment ClippyShoutoutClipFragment on ClippyTwitchClip {
    id
    url
    embed_url
    broadcaster_id
    broadcaster_name
    creator_id
    creator_name
    title
    language
    game_id
    video_id
    view_count
    created_at
    duration
    thumbnail_url
    mp4_url
    game {
      name
    }
  }
`;
