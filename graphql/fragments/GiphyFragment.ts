import { gql } from "@apollo/client";

// eslint-disable-next-line import/prefer-default-export
export const GIPHY_FIELDS_FRAGMENT = gql`
  fragment GiphyFields on Giphy {
    images {
      downsized_medium {
        url
      }
      fixed_height_downsampled {
        url
      }
    }
  }
`;
