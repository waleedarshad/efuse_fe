import { gql } from "@apollo/client";
import { HYPE_USER_FRAGMENT } from "./HypeUserFragment";

// eslint-disable-next-line import/prefer-default-export
export const HYPE_FRAGMENT = gql`
  ${HYPE_USER_FRAGMENT}
  fragment HypeFields on Hype {
    _id
    user {
      ...HypeUserFields
    }
    objId
    objType
    platform
    hypeCount
  }
`;
