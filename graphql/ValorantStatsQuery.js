import { gql } from "@apollo/client";

export const GET_VALORANT_PROFILE = gql`
  query getUserById($id: ID!) {
    getUserById(id: $id) {
      valorantAccountProfile {
        gameName
      }
    }
  }
`;

export const GET_VALORANT_STATS = gql`
  query Query($getValorantStatsUserId: String) {
    getValorantStats(userId: $getValorantStatsUserId) {
      gameName
      tagLine
      episodeName
      actName
      rankedRating
      losses
      wins
      kills
      deaths
      assists
      damage
      competitiveTier
      competitiveTierName
      competitiveTierImage
      agentMatchStats {
        agentData {
          displayName
          displayIcon
        }
        wins
        losses
        deaths
        kills
      }
    }
  }
`;
