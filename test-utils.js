import React from "react";
import { render as rtlRender } from "@testing-library/react";
import { Provider } from "react-redux";
import initStore from "./store/store";

const render = (ui, { preloadedState, store = initStore, ...renderOptions } = {}) => {
  const Wrapper = ({ children }) => {
    return <Provider store={store}>{children}</Provider>;
  };
  return rtlRender(ui, { wrapper: Wrapper, ...renderOptions });
};

// re-export everything
export * from "@testing-library/react";
// override render method
export { render };
