# FE Helm chart

This chart is used to deploy FE in a sane, and repeatable way.

```
helm upgrade --install NAME . --set image.tag=<SHA> --set ingress.enabled=true --set fqdn=<FQDN>
```

## Defaults
Rather than create a generically useful chart for all possible scenarios, this chart makes several assumptions. 

An ingress is not enabled by default. In order to make an ingress useful, an FQDN must be supplied. Rather than define a generic FQDN, this decision is left up to a human operator.

If an FQDN is defined, the ingress is annotated to have the external-dns component register that dns. The operator may opt to have CloudFlare DNS proxy enabled or disabled.

Requests:
* cpu: 500 millicores (0.5 vCPU)
* memory: 1 gibibytes

Limits:
* cpu: 2000 millicores (2 vCPU)
* memory: 2 gibibytes

## eFuse settings
This chart adds several new options to the standard array fo Helm values
* **albCertARN**: the ARN of an ACM certificate to apply to the ingress.
* **albGroupName**: the ALB group name to use (default: default).
* **cloudFlare**: whether to have external-dns create the CloudFlare record (default: true).
* **cloudflareProxied**: whether to use Cloudflare Proxy for the DNS (default: true).
* **clusterIssuer**: the name of a cert-manager ClusterIssuer from which to request a certificate (default: empty).
* **datadogAdmission**: whether to enable the DataDog admission controller annotations (default: true).
* **ddEnv**: the DataDog environment with which to tag everything (default: production).
* **ddService**: the DataDog service with which to tag everything (default fe).
* **efusePreview**: whether this is a preview deployment (default: false).
* **fqdn**: the FQDN to use for the ingress and TLS cert. Mandatory, no default defined.

## Examples
Install a production instance of FE, and enable the Horizontal Pod Autoscaler:
```
helm upgrade --install fe . --set image.tag=<SHA> --set ingress.enabled=true --set cloudFlare=false --set fqdn=efuse.gg --set albCertARN=arn:aws:.... --set autoscaling.enabled=true
```

> *Note*: in production, we will have a load balancer configured at the DNS name `efuse.gg`, and therefore we do not want to overwrite that record.


Install an FE PR at a preview URL, with a lower CPU and memory limit:
```
helm upgrade --install fe-pr-12345 . --set image.tag=<SHA> --set ingress.enabled=true --set fqdn=fe-pr-12345.efuse.wtf --set ddEnv=staging --set efusePreview=true --set albCertARN=arn:aws:.... --set resources.limits.memory=1.5Gi --set resources.limits.cpu=750m
```
