const colors = {
  borderGrey: "#c7d3ea",
  navigationBlue: "#006cfa",
  navigationText: "#12151d",
  stormGrey: "#737385"
};

export default colors;
