import { css } from "styled-components";

// eslint-disable-next-line import/prefer-default-export
export const SmallHeader = css`
  font-family: Poppins, sans-serif;
  font-size: 14px;
  font-style: normal;
  font-weight: 500;
  line-height: 20px;
  letter-spacing: 0;
`;
