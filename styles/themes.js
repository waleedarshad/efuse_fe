const immutableColors = {
  "--always-white": "#ffffff",
  "--always-black": "#000000",
  "--shadow-black-03": "rgba(0,0,0,0.03)",
  "--shadow-black-10": "rgba(0,0,0,0.10)",
  "--shadow-black-13": "rgba(0,0,0,0.13)",

  // eFuse colors
  "--efuse-blue": "#2e87ff",
  "--efuse-blue-dark": "#226ccc",
  "--efuse-dark": "#0f0f0f",
  "--verified-organization": "#51b456",
  "--verified-user": "#2e87ff",
  "--hype-color": "#ff5100",
  "--views-color": "#008000",
  "--streak-color": "#460fc9",
  "--events-color": "#ff7b00",
  "--jobs-color": "#16a65d",
  "--team-openings-color": "#4c0e83",
  "--scholarships-color": "#2e87ff",

  // social colors
  "--social-battlenet": "#002754",
  "--social-discord": "#7289da",
  "--social-facebook": "#3b5998",
  "--social-linkedin": "#006097",
  "--social-instagram": "#eb25ac",
  "--social-playstation": "#003087",
  "--social-steam": "#1b2838",
  "--social-twitch": "#6441a5",
  "--social-twitter": "#1da1f2",
  "--social-youtube": "#c4302b",
  "--social-xbox": "#107c10"
};

// This would be set equal to the root styles in our CSS
const lightTheme = {
  ...immutableColors,
  // global
  "--background-color": "#ffffff",
  "--background-text-strong": "#000000",
  "--background-text-weak": "#737385",
  "--background-rgba-color": "rgba(0,0,0,0.15)",
  "--background-rgba-hover": "rgba(0,0,0,0.35)",

  // navigation
  "--navbar-background": "#16181a",
  "--subheader-background": "#ffffff",

  // colors
  "--primary-color": "#4485fa",
  "--primary-color-hover": "#3b70d1",
  "--secondary-color": "#4485fa",
  "--secondary-color-hover": "#e24800",

  // text
  "--text-primary": "#4485fa",
  "--text-strong": "#15151a",
  "--text-weak": "#737385",

  // cards
  "--card-background": "#ffffff",
  "--card-border": "#ced7e7",
  "--card-lowlight": "#f7f7f7",
  "--card-highlight": "#ffffff"
};

// This is our dark mode
const darkTheme = {
  ...immutableColors,
  // global
  "--background-color": "#222222",
  "--background-text-strong": "#ffffff",
  "--background-text-weak": "#d6d6da",
  "--background-rgba-color": "rgba(255,255,255,0.05)",
  "--background-rgba-hover": "rgba(255,255,255,0.25)",

  // navigation
  "--navbar-background": "#16181a",
  "--subheader-background": "#222222",

  // colors
  "--primary-color": "#4485fa",
  "--primary-color-hover": "#3b70d1",
  "--secondary-color": "#4485fa",
  "--secondary-color-hover": "#e24800",

  // text
  "--text-primary": "#4485fa",
  "--text-strong": "#fdfdfd",
  "--text-weak": "#f0f0f0",

  // cards
  "--card-background": "rgb(60, 60, 60)",
  "--card-border": "rgb(42, 42, 42)",
  "--card-lowlight": "rgb(50, 50, 50)",
  "--card-highlight": "rgb(70, 70, 70)"
};

export { lightTheme, darkTheme };
