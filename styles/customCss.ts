import { css } from "styled-components";

// This style removes an outline for mouse clicks,
// but keeps it for keyboard users for accessibility reasons

// eslint-disable-next-line import/prefer-default-export
export const HideOutline = css`
  &:focus:not(:focus-visible) {
    outline: none;
  }
`;
