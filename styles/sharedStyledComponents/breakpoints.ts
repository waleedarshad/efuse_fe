export const breakpoints = {
  sm: 520,
  md: 767,
  lg: 991,
  xl: 1200
};

export const mediaQueries = (key: "sm" | "md" | "lg" | "xl") => {
  return (style: TemplateStringsArray | String) =>
    `@media only screen and (max-width: ${breakpoints[key]}px) { ${style} }`;
};

export const customBreakpoint = (breakpoint: Number) => {
  return (style: TemplateStringsArray | String) => `@media only screen and (max-width: ${breakpoint}px) { ${style} }`;
};
