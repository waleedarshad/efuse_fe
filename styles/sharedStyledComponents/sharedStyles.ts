import styled, { css } from "styled-components";
import colors from "./colors";

export const textNormal = css`
  font-family: Poppins, sans-serif;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 20px;
  color: ${colors.textStrong};
`;

export const textWeakest = css`
  font-family: Poppins, sans-serif;
  font-weight: 500;
  font-size: 10px;
  color: ${colors.textWeakest};
`;

export const BadgeText = css`
  font-family: Poppins, sans-serif;
  font-weight: 600;
  font-size: 10px;
  line-height: 15px;
`;

export const EFText = styled.p`
  ${textNormal};
  margin: 0;
`;

export const EFTextWeakest = styled.p`
  ${textWeakest};
  margin: 0;
`;

export const EFHeading2 = styled.h2`
  ${textNormal};
  font-size: 18px;
  line-height: 24px;
`;

export const TruncateWithEllipsis = css`
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;

export const PromoCardBoxShadow = css`
  -webkit-box-shadow: 0 2px 5px -2px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 0 2px 5px -2px rgba(0, 0, 0, 0.2);
  box-shadow: 0 2px 5px -2px rgba(0, 0, 0, 0.2);
`;
