import colors from "./colors";

export default {
  "Employment Opening": colors.brightGreen,
  Event: colors.lightOrange,
  Job: colors.brightGreen,
  "Team Opening": colors.darkPurple,
  Scholarship: colors.eRenaBlue,
  eRena: colors.eRenaBlue,
  LIVE: colors.alertRed,
  news: colors.brightPurple,
  "Under Construction": colors.constructionYellow,
  "Battle Underway": colors.alertRed,
  "Event Completed": colors.successGreen
};
