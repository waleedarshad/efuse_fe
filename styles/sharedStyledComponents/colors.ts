export default {
  // basic colors
  white: "#FFFFFF",
  black: "#000000",

  // eFuse colors
  eFuseBlue: "#006CFA",
  eFuseLightBlue: "#5B97F7",
  eFuseOrange: "#F44900",
  eRenaBlue: "#2E87FF",

  // text
  textWeakest: "rgba(18, 21, 29, 0.6)",
  textWeak: "#737385",
  textStrong: "#15151A",
  textDisabledWhite: "rgba(255, 255, 255, 0.6)",

  // background
  hover: "#F1F4F8",
  errorBackground: "rgba(255, 0, 0, 0.2)",
  snow: "#fafafa",

  // misc
  outerBorder: "#CED7E7",
  winnerGreen: "#50BC3A",
  successGreen: "#30CC49",
  brightGreen: "#16A65D",
  constructionYellow: "#F9D342",
  winnerYellow: "#FFC700",
  alertRed: "#FF0000",
  brightPurple: "#9937EA",
  lightOrange: "#FF7B00",
  darkPurple: "#4C0E83"
};
