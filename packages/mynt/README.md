# @efuse/mynt

To deploy on vercel, your git local git email must match your actual git email

## Pull Environment Variables from Vercel

1. Install Vercel CLI
   `npm install -g vercel`
2. Log into Vercel
   `vercel login`
3. Link to a Project
   `vercel link`
   > Choose `efuse-Engineering` as the scope
   > Choose `y` to link existing project
   > enter `efuse-mynt` as the project name
4. Pull Environment Variables
   `vercel env pull`
5. See variable in `.env`

## Set up environment

`npm i`

## Run Mynt

`npm run dev`
