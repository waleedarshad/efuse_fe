import { getAccessToken } from "@auth0/nextjs-auth0";
import { NextApiRequest, NextApiResponse } from "next";
import { IncomingMessage, ServerResponse } from "http";

const getJwt = async (req: IncomingMessage | NextApiRequest, res: ServerResponse | NextApiResponse) => {
  let token = "";
  try {
    const { accessToken } = await getAccessToken(req, res);
    token = accessToken || "";
  } catch {
    // intentional as not every request requires authentication
    // but attempting to get the access token throws an error.
    // So if it fails, try anyway and handle that error instead
  }

  return token;
};

export default getJwt;
