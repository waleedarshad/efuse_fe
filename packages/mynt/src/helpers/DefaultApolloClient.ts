import { ApolloClient, HttpLink, InMemoryCache } from "@apollo/client";
import { GetServerSidePropsContext } from "next";
import getJwt from "./getJwt";

export const getApolloClient = (ssr: boolean) => {
  return new ApolloClient({
    ssrMode: ssr,
    uri: `${process.env.NEXT_PUBLIC_GRAPHQL_URL}`,
    cache: new InMemoryCache()
  });
};

export const getAuthenticatedSSRApolloClient = async (ctx: GetServerSidePropsContext) => {
  const jwt = await getJwt(ctx.req, ctx.res);

  return new ApolloClient({
    ssrMode: true,
    link: new HttpLink({
      uri: `${process.env.EFUSE_API_BASE_URL}/api/graphql`,
      headers: {
        authorization: jwt ? `Bearer ${jwt}` : ""
      }
    }),
    cache: new InMemoryCache()
  });
};
