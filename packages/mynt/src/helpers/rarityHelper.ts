export const rarityImageMap: { [key: string]: any } = {
  rare: "/static/RareBadge.png",
  epic: "/static/EpicBadge.png",
  legendary: "/static/LegendaryBadge.png",
  preview: "/static/PreviewBadge.png"
};
