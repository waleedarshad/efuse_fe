import { gql } from "@apollo/client";
import { Rarity, MediaTypes } from "../data/NftTypes";

export const GET_NFT_ASSETS = gql`
  query getStagedNFTs {
    getStagedNFTs {
      _id
      assetFile
      name
      externalLink
      description
      unlockableContent
      eFuseUserId
      isListed
      sellPrice
      transactionHistory {
        _id
        nftId
        orderId
        payerId
        purchasePrice
        purchaserId
        sellerId
      }
      traits {
        rarity
        game
        owner
        creator
        mediaType
      }
    }
  }
`;

export const GET_NFT_COLLECTION = gql`
  query getNFTCollection {
    getNFTCollection {
      _id
      assetFile
      name
      externalLink
      description
      unlockableContent
      eFuseUserId
      isListed
      sellPrice
      transactionHistory {
        _id
        nftId
        orderId
        payerId
        purchasePrice
        purchaserId
        sellerId
      }
      traits {
        rarity
        game
        owner
        creator
        mediaType
      }
    }
  }
`;

export const GET_STAGED_NFT = gql`
  query findStagedNFT($_id: String!) {
    findStagedNFT(_id: $_id) {
      _id
      assetFile
      name
      externalLink
      description
      traits {
        rarity
        game
        owner
        creator
        mediaType
      }
      unlockableContent
      eFuseUserId
      isListed
      sellPrice
      transactionHistory {
        _id
        nftId
        orderId
        payerId
        purchasePrice
        purchaserId
        sellerId
      }
    }
  }
`;

export const FIND_MYNT_ASSET = gql`
  query findMyntAsset($tokenId: String) {
    findMyntAsset(tokenId: $tokenId) {
      tokenId
      name
      description
      imageUrl
      assetContract {
        address
      }
      traits {
        rarity
        game
        owner
        mediaType
      }
      creator {
        profileImgUrl
        user {
          username
        }
      }
      assetContract {
        name
        address
        type
        schemaName
        sellerFeeBasisPoints
        buyerFeeBasisPoints
        description
        tokenSymbol
        imageUrl
        stats
        traitStats
        externalLink
        wikiLink
      }
      assetCollection {
        name
        slug
        editors
        description
        imageUrl
        largeImageUrl
        featuredImageUrl
        stats
        displayData
        payoutAddress
        traitStats
        externalLink
        wikiLink
      }
      description
      owner {
        profileImgUrl
        user {
          username
        }
      }
      isPresale
      imageUrl
      imagePreviewUrl
      imageUrlOriginal
      imageUrlThumbnail
      animationUrl
      animationOriginalUrl
      openSeaLink
      numSales
      lastSale {
        eventType
        eventTimestamp
        auctionType
        totalPrice
        transaction {
          fromAccount {
            address
            config
            profileImgUrl
          }
          toAccount {
            address
            config
            profileImgUrl
          }
          createdDate
          modifiedDate
          transactionHash
          transactionIndex
          blockNumber
          blockHash
          timestamp
        }
        paymentToken
      }
    }
  }
`;

export const CREATE_NFT = gql`
  mutation createNFT($args: NFTArgs!) {
    createNFT(args: $args) {
      _id
    }
  }
`;

export const LIST_NFT_FOR_SALE = gql`
  mutation listNFTForSale($nftId: String!, $sellPrice: String!) {
    listNFTForSale(nftId: $nftId, sellPrice: $sellPrice) {
      _id
    }
  }
`;

export interface NFTArgs {
  name: string;
  externalLink?: string;
  description?: string;
  unlockableContent?: string;
  assetFile: string;
  traits: {
    game?: string;
    rarity?: Rarity;
    mediaType: MediaTypes;
  };
}
