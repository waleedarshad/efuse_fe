import { gql } from "@apollo/client";

export default gql`
  query getGames {
    getGames {
      _id
      gameImage {
        url
      }
      title
      slug
    }
  }
`;
