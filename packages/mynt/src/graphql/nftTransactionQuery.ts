import { gql } from "@apollo/client";

export const CREATE_NFT_TRANSACTION = gql`
  mutation createNFTTransaction($args: NFTTransactionArgs!) {
    createNFTTransaction(args: $args) {
      _id
    }
  }
`;

export interface NFTTransactionArgs {
  orderId: string;
  payerId: string;
  purchaseAmount: string;
  nftId: string;
  sellerId: string;
}
