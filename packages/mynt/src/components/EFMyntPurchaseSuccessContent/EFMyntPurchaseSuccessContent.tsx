import Link from "next/link";
import React from "react";
import styled from "styled-components";
import EFMyntButton from "../EFMyntButton/EFMyntButton";
import { INft } from "../../data/NftTypes";
import ImageHighlight from "../../pageLayouts/NftCustomListingPage/ImageHighlight/ImageHighlight";
import mediaQueries from "../../styles/mediaQueries";
import buttonThemes from "../../styles/buttonThemes";

const EFMyntPurchaseSuccessContent = ({ nft }: { nft: INft }) => {
  return (
    <>
      <StyledHeaderSection>
        <StyledHeader>Transaction Successful</StyledHeader>
      </StyledHeaderSection>
      <MainImageContainer>
        <ImageHighlight image={nft.assetFile} />
      </MainImageContainer>
      <Message>NFT Purchased</Message>
      <Link href="/mycollection">
        <EFMyntButton theme={buttonThemes.regularMynt} text="View My Collection" />
      </Link>
    </>
  );
};

const StyledHeaderSection = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 145px;
  margin-bottom: 50px;
`;

const StyledHeader = styled.h1`
  font-family: Open-Sans, sans-serif;
  font-size: 28px;
  font-style: normal;
  font-weight: 700;
  line-height: 38px;
  letter-spacing: 0em;
  text-align: left;
  border-bottom: 5px solid #00c9b7;
  color: white;
  text-transform: uppercase;
`;

const Message = styled.div`
  color: white;
  font-size: 28px;
  margin: 10px 0;
`;

const MainImageContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 550px;
  flex: 3;
  border: 1px solid #323232;
  border-radius: 10px;
  margin 0 auto;
  @media ${mediaQueries.mobileL} {
    width: 90%;
  }
`;

export default EFMyntPurchaseSuccessContent;
