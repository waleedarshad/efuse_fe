import React from "react";
import styled, { ThemeProvider } from "styled-components";
import EFMyntReactionSection from "../EFMyntReactionSection/EFMyntReactionSection";
import EFMyntCardDetailsSection from "../EFMyntCardDetailsSection/EFMyntCardDetailsSection";
import { INft } from "../../data/NftTypes";
import rarityThemes from "../../styles/rarityThemes";
import colors from "../../styles/colors";

const EFMyntCard = ({ nft, isOwner, isOwnersListing }: { nft: INft; isOwner: boolean; isOwnersListing: boolean }) => {
  return (
    <ThemeProvider theme={rarityThemes[nft.traits.rarity] || rarityThemes.default}>
      <CardWrapper>
        <CardHeader>
          <EFMyntReactionSection />
          <StyledRarity>{nft.traits.rarity}</StyledRarity>
        </CardHeader>
        <ImageWrapper>
          <MainImage alt={nft.description} src={nft.assetFile} />
        </ImageWrapper>
        <EFMyntCardDetailsSection nft={nft} isOwner={isOwner} isOwnersListing={isOwnersListing} />
      </CardWrapper>
    </ThemeProvider>
  );
};

const CardWrapper = styled.div`
  overflow: hidden;
  position: relative;
  width: 300px;
  height: 400px;
  border: 2px solid ${colors.greyBorder};
  border-radius: 10px;
  padding: 15px 0px 20px 0px;
  position: relative;
`;

const CardHeader = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 0px 20px;
`;

const StyledRarity = styled.p`
  font-family: Open-Sans, sans-serif;
  font-size: 18px;
  font-style: normal;
  font-weight: 700;
  letter-spacing: 0em;
  text-align: center;
  text-transform: uppercase;
  color: ${props => props.theme.color};
  margin: 0px;
`;

const ImageWrapper = styled.div`
  height: 250px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin-top: 15px;
`;

const MainImage = styled.img`
  display: block;
  margin: 0 auto;
  margin-top: 15px;
  margin-bottom: 20px;
  max-height: 250px;
  max-width: 280px;
  border-radius: 10px;
`;

export default EFMyntCard;
