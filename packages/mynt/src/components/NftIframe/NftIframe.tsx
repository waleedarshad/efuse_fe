const NftIframe = ({ address, token }: { address: String; token: String }) => {
  return (
    <iframe
      title="nft-purchase-frame"
      src={`https://opensea.io/assets/${address}/${token}?embed=true`}
      width="100%"
      height="100%"
      frameBorder="0"
      allowFullScreen
    />
  );
};

export default NftIframe;
