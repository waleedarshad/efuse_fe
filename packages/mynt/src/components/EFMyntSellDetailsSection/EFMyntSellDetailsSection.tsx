import React from "react";
import styled from "styled-components";
import colors from "../../styles/colors";

const EFMyntSellDetailsSection = () => {
  return (
    <>
      <div>
        {/* <StyledSpan>#/00</StyledSpan> */}
        {/* <StyledSpan>00 For sale</StyledSpan> */}
        {/* <StyledSpan>00 Collectors</StyledSpan> */}
      </div>
      <div>
        <StyledHeader>Overview:</StyledHeader>
        <StyledText>Fortnite calling card</StyledText>
        <StyledText>Gladiator skin</StyledText>
        <StyledText>Rarity*</StyledText>
      </div>
      <div>
        <StyledHeader>Details:</StyledHeader>
        <StyledText>Purchased for $0.00 USD</StyledText>
        <StyledText>FEB 20, 2021</StyledText>
      </div>
    </>
  );
};

const StyledHeader = styled.h3`
  color: ${colors.headerText};
  margin-bottom: 5px;
  font-family: Open-Sans, sans-serif;
  font-size: 18px;
  font-style: normal;
  font-weight: 700;
  line-height: 25px;
  letter-spacing: 0em;
  text-align: left;
  margin-top: 10px;
`;

const StyledText = styled.p`
  color: ${colors.headerText};
  font-family: Open-Sans, sans-serif;
  font-size: 12px;
  font-style: normal;
  font-weight: 400;
  line-height: 16px;
  letter-spacing: 0em;
  text-align: left;
  margin: 0;
`;

export default EFMyntSellDetailsSection;
