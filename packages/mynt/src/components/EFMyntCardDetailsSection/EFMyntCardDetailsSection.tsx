import Link from "next/link";
import styled from "styled-components";
import React from "react";
import EFMyntButton from "../EFMyntButton/EFMyntButton";
import { INft } from "../../data/NftTypes";
import colors from "../../styles/colors";
import buttonThemes from "../../styles/buttonThemes";

const EFMyntCardDetailsSection = ({
  nft,
  isOwner,
  isOwnersListing
}: {
  nft: INft;
  isOwner: boolean;
  isOwnersListing: boolean;
}) => {
  return (
    <StyledDetails>
      <StyledTitle>{nft.name}</StyledTitle>
      <SubInfoWrapper>
        <SubDetails>
          <MyntStyleText>{nft.traits.creator}</MyntStyleText>
          <div>
            {nft.isListed && <StyledPrice>{`$${nft.sellPrice} USD`}</StyledPrice>}
            <StyledSpan>#/00</StyledSpan>
          </div>
        </SubDetails>
        {nft.isListed && !isOwnersListing && (
          <Link href={`/list/${nft._id}`}>
            <EFMyntButton theme={buttonThemes.regularMynt} text="Buy Now" />
          </Link>
        )}
        {!nft?.isListed && isOwner && (
          <Link href={`/sell/${nft._id}`}>
            <EFMyntButton theme={buttonThemes.regularMynt} text="Sell" />
          </Link>
        )}
      </SubInfoWrapper>
    </StyledDetails>
  );
};

const StyledDetails = styled.div`
  position: absolute;
  width: 100%;
  bottom: 15px;
  padding: 0px 20px;
`;

const SubDetails = styled.div`
  margin: 0px;
`;

const StyledTitle = styled.h3`
  font-family: Open-Sans, sans-serif;
  font-size: 16px;
  font-style: normal;
  font-weight: 700;
  line-height: 22px;
  letter-spacing: 0em;
  text-align: left;
  color: ${colors.headerText};
  margin: 0px;
`;

const MyntStyleText = styled.span`
  font-family: Open-Sans, sans-serif;
  font-size: 12px;
  font-style: normal;
  font-weight: 700;
  line-height: 16px;
  letter-spacing: 0em;
  text-align: left;
  color: ${colors.accentColor};
  margin: 0px;
`;

const StyledPrice = styled.span`
  font-family: Open-Sans, sans-serif;
  font-size: 12px;
  font-style: normal;
  font-weight: 700;
  line-height: 14px;
  letter-spacing: 0em;
  text-align: left;
  color: ${colors.detailsText};
`;

const StyledSpan = styled.span`
  font-family: Open Sans;
  font-size: 12px;
  font-style: normal;
  font-weight: 400;
  line-height: 14px;
  letter-spacing: 0em;
  text-align: left;
  color: ${colors.detailsText};
  margin-left: 10px;
`;

const SubInfoWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-end;
`;

export default EFMyntCardDetailsSection;
