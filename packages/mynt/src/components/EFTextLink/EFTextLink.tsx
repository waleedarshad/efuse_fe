import React from "react";
import { useRouter } from "next/router";
import Link from "next/link";
import styled from "styled-components";
import colors from "../../styles/colors";

const EFTextLink = ({ href, children }: { href: string; children: React.ReactNode }) => {
  const path = useRouter().asPath;

  return (
    <Link href={href}>
      <TextLink active={href === path}>{children}</TextLink>
    </Link>
  );
};

const TextLink = styled.div`
  color: ${(props: { active: boolean }) => (props.active ? colors.accentColor : "white")};
  margin: 0 7px;
  font-weight: bold;
  cursor: pointer;
  font-family: Open Sans;
  font-size: 20px;
  line-height: 27px;
  letter-spacing: 0em;
  font-family: Open-Sans, sans-serif;
`;

export default EFTextLink;
