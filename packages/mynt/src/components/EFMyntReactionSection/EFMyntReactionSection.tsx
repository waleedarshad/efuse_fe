import React from "react";
import styled from "styled-components";

const EFMyntReactionSection = () => {
  const getRandomNumber = (min: number, max: number) => {
    return Math.floor(Math.random() * (max - min) + min);
  };

  return (
    <ReactionSection>
      <CounterWrapper>
        <StyledFlame src="/static/flame.png" alt="hypes" />
        <CounterText>{getRandomNumber(1, 100)}</CounterText>
      </CounterWrapper>
      <CounterWrapper>
        <StyledEye src="/static/eye.svg" alt="views" />
        <CounterText>{getRandomNumber(1, 100)}</CounterText>
      </CounterWrapper>
    </ReactionSection>
  );
};

const ReactionSection = styled.div`
  display: flex;
`;

const CounterWrapper = styled.div`
  display: flex;
  align-items: center;
`;

const StyledFlame = styled.img`
  height: 15px;
`;

const StyledEye = styled.img`
  height: 10px;
  margin-right: 3px;
`;

const CounterText = styled.div`
  font-family: Open-Sans, sans-serif;
  font-size: 12px;
  font-style: normal;
  font-weight: 400;
  line-height: 16px;
  letter-spacing: 0em;
  text-align: left;
  color: #f0f0f0;
  margin-left: 3px;
  margin-right: 10px;
`;

export default EFMyntReactionSection;
