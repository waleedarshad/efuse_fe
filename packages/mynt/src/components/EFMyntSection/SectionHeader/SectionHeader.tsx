import React from "react";
import styled from "styled-components";
import colors from "../../../styles/colors";

const SectionHeader = ({ title }: { title: String }) => {
  return (
    <SectionHeaderStyle>
      <Heading>{title}</Heading>
      <Divider />
    </SectionHeaderStyle>
  );
};

const SectionHeaderStyle = styled.div`
  display: flex;
`;

const Heading = styled.div`
  color: ${colors.headerText};
  font-size: 24px;
  font-weight: bold;
  text-transform: uppercase;
  margin: 0 12px;
`;

const Divider = styled.div`
  background-color: ${colors.accentColor};
  width: 100%;
  height: 2px;
  margin: 12px 0;
`;

export default SectionHeader;
