import React from "react";
import styled from "styled-components";
import { useUser } from "@auth0/nextjs-auth0";
import { INft } from "../../../data/NftTypes";
import EFNftCard from "../../EFHexCard/EFNftCard";

const NftList = ({ nftList, isOwnersListing }: { nftList: INft[]; isOwnersListing: boolean }) => {
  const { user }: any = useUser();
  let profileKey;
  let userId: any;

  if (user) {
    profileKey = Object.keys(user).filter(item => item.includes("/profile"));
    userId = user[profileKey[0]]?.id;
  }

  return (
    <NftListWrapper>
      {nftList.map(nft => (
        // eslint-disable-next-line react/jsx-key
        <NftWrapper>
          <div key={nft._id}>
            <EFNftCard nft={nft} isOwner={userId === nft?.eFuseUserId} isOwnersListing={isOwnersListing} />
          </div>
        </NftWrapper>
      ))}
    </NftListWrapper>
  );
};

const NftListWrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(300px, 1fr));
  justify-items: center;
  gap: 20px 10px;
  flex-wrap: wrap;
  margin-top: 57px;
  margin-top: 46px;
`;

const NftWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

export default NftList;
