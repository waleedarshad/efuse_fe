import React from "react";
import styled from "styled-components";

import mediaQueries from "../../styles/mediaQueries";
import SectionHeader from "./SectionHeader/SectionHeader";

const EFMyntSection = ({ title, children }: { title: string; children: React.ReactNode }) => {
  return (
    <SectionWrapper>
      <SectionHeader title={title} />
      {children}
    </SectionWrapper>
  );
};

const SectionWrapper = styled.div`
  margin: 20px 60px;
  @media ${mediaQueries.laptop} {
    margin: 20px 15px;
  }
`;

export default EFMyntSection;
