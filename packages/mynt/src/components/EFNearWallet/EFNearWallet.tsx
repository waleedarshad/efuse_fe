import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { connect, keyStores, WalletConnection } from "near-api-js";
import getNearWalletConfig from "../../config/nearWalletConfig";

const EFNearWallet = () => {
  const [nearUser, setNearUser] = useState(false);
  const [nearUserBalance, setNearUserBalance] = useState(0);

  const getNearWallet = async () => {
    const config = getNearWalletConfig(new keyStores.BrowserLocalStorageKeyStore());
    const near = await connect(config);
    return new WalletConnection(near, null);
  };

  const nearSignOut = () => {
    getNearWallet().then(wallet => {
      wallet.signOut();
      setNearUser(false);
    });
  };

  const nearLogin = () => {
    getNearWallet().then(wallet => {
      wallet.requestSignIn(
        "example-contract.testnet", // contract requesting access
        "Mynt.gg"
      );
    });
  };

  useEffect(() => {
    // update some client side state to say it is now safe to render the client-side only component
    getNearWallet().then(wallet => {
      if (wallet.isSignedIn()) {
        wallet
          .account()
          .getAccountBalance()
          .then(balance => {
            const userBalance = (Number(balance.available) / 10 ** 24).toFixed(3);
            setNearUserBalance(Number(userBalance));
            setNearUser(true);
          });
      }
    });
  });

  return (
    <WalletContainer>
      {nearUser ? (
        <>
          <WalletBalanceContainer onClick={nearSignOut}>
            <MyntIcon src="/static/PreviewBadge.png" />
            <WalletBalance>{nearUserBalance}</WalletBalance>
          </WalletBalanceContainer>
        </>
      ) : (
        <>
          <SmallText>Sign into</SmallText>
          <NearLogo onClick={nearLogin} src="/static/near-logo-white.png" />
        </>
      )}
    </WalletContainer>
  );
};

const SmallText = styled.div`
  font-size: 12px;
  color: white;
  text-transform: uppercase;
  align-self: center;
`;

const NearLogo = styled.img`
  height: 25px;
  align-self: center;
`;

const MyntIcon = styled.img`
  height: 37px;
  padding-right: 5px;
`;

const WalletContainer = styled.div`
  display: flex;
  flex-direction: column;
  cursor: pointer;
  height: 100%;
  margin-left: 15px;
`;

const WalletBalanceContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-self: flex-end;
  height: 100%;
`;

const WalletBalance = styled.span`
  color: white;
  font-weight: bold;
  align-self: center;
`;

export default EFNearWallet;
