import Link from "next/link";
import React from "react";
import styled from "styled-components";
import EFMyntButton from "../EFMyntButton/EFMyntButton";
import buttonThemes from "../../styles/buttonThemes";

const EFMyntSuccessOverlayContent = () => {
  return (
    <>
      <Image src="../../static/PreviewBadge.png" />
      <Message>NFT Created</Message>
      <Link href="/">
        <EFMyntButton theme={buttonThemes.regularMynt} text="Take me home" />
      </Link>
    </>
  );
};

const Message = styled.div`
  color: white;
  font-size: 28px;
  margin: 10px 0;
`;

const Image = styled.img`
  width: 200px;
  object-fit: scale-down;
  margin: 10% auto 10px 0;
`;

export default EFMyntSuccessOverlayContent;
