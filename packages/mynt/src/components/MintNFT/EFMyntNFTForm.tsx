import React, { useState } from "react";
import styled from "styled-components";
import { useMutation, useQuery } from "@apollo/client";
import EFHexCard from "../../components/EFHexCard/EFHexCard";
import EFMyntUploader from "../../components/MintNFT/EFMyntUploader";
import EFMyntInput from "../../components/MintNFT/EFMyntInput";
import EFGamesList from "../../components/EFGamesList/EFGamesList";
import EFMyntButton from "../../components/EFMyntButton/EFMyntButton";
import { IGames } from "../../data/IGame";
import GET_GAMES from "../../graphql/gamesQuery";
import { CREATE_NFT, NFTArgs } from "../../graphql/nftQuery";
import { MediaTypes } from "../../data/NftTypes";
import ImageHighlight from "../../pageLayouts/NftCustomListingPage/ImageHighlight/ImageHighlight";
import buttonThemes from "../../styles/buttonThemes";

const MintNFTForm = ({ onCreateSuccess }: { onCreateSuccess: () => void }) => {
  const { data } = useQuery(GET_GAMES);
  const games: IGames = data;

  const [assetFile, setFile] = useState<string>();
  const [name, setName] = useState<string>("");
  const [externalLink, setExternalLink] = useState<string>("");
  const [unlockableContent, setUnlockableContent] = useState<string>("");
  const [description, setDescription] = useState<string>("");
  const [game, setGame] = useState<string>("");
  const [mediaType, setMediaType] = useState<MediaTypes>();
  const [error, setError] = useState<boolean>(false);

  const [createNFT] = useMutation<NFTArgs>(CREATE_NFT);

  const handleGameSelect = (slug: string) => {
    if (game === slug) {
      setGame("");
    } else {
      setGame(slug);
    }
  };

  const handleFileUpload = (url: string, mediaType: MediaTypes) => {
    setMediaType(mediaType);
    setFile(url);
  };

  const handleSubmit = () => {
    if (name && assetFile && mediaType) {
      const args: NFTArgs = {
        name,
        externalLink,
        description,
        unlockableContent,
        assetFile,
        traits: { game, mediaType }
      };
      createNFT({ variables: { args } })
        .then(onCreateSuccess)
        .catch(() => {
          alert("Oops! Something went wrong. Please try again");
        });
    } else {
      setError(true);
      window.scrollTo({ top: 0, behavior: "smooth" });
    }
  };

  return (
    <PageWrapper>
      <HeaderText>Create NFT</HeaderText>
      <PageContent>
        <FormColumn>
          <LabelText>Upload File</LabelText>
          <EFMyntUploader error={error && !assetFile} onUpload={handleFileUpload} />
          <EFMyntInput
            placeholder="Item name"
            name="name"
            error={error && !name}
            value={name}
            label="Name"
            onChange={setName}
          />
          <EFMyntInput
            placeholder="External link"
            name="externalLink"
            value={externalLink}
            description="Mynt will include a link to the URL on this items detail page, so users can learn more about it."
            label="External link"
            onChange={setExternalLink}
          />
          <EFMyntInput
            placeholder="Unlockable content"
            name="unlockableContent"
            value={unlockableContent}
            description="Unlockable content that can only be revealed by the owner of the item."
            label="Unlockable Content"
            onChange={setUnlockableContent}
          />
          <EFGamesList onGameClick={handleGameSelect} activeGame={game} games={games} />
          <EFMyntInput
            placeholder="Enter a detailed description of your item."
            name="description"
            value={description}
            label="Description"
            textArea
            onChange={setDescription}
          />
          <EFMyntButton theme={buttonThemes.regularMynt} onClick={handleSubmit} text="Create Item" />
        </FormColumn>
        <PreviewColumn>
          <LabelText>Preview</LabelText>
          {assetFile && <ImageHighlight image={assetFile} />}
        </PreviewColumn>
      </PageContent>
    </PageWrapper>
  );
};

const PageWrapper = styled.div`
  width: 80%;
  margin: 35px auto;
`;

const PageContent = styled.div`
  display: flex;
  margin: 35px 0;
`;

const FormColumn = styled.div`
  flex: 2;
  margin-right: 34px;
  margin-bottom: 50px;
  overflow: hidden;
`;

const PreviewColumn = styled.div`
  flex: 1;
`;

const HeaderText = styled.div`
  font-size: 32px;
  color: white;
  font-weight: bold;
`;

const LabelText = styled.div`
  font-size: 18px;
  color: white;
  font-weight: bold;
  margin-bottom: 10px;
`;

export default MintNFTForm;
