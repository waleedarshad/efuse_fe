import React, { ChangeEvent } from "react";
import styled from "styled-components";
import colors from "../../styles/colors";

const EFMyntInput = ({
  placeholder,
  name,
  value,
  onChange,
  label,
  description,
  error = false,
  textArea = false
}: {
  placeholder: string;
  name: string;
  value: string;
  onChange: (value: string) => void;
  label: string;
  description?: string;
  textArea?: boolean;
  error?: boolean;
}) => {
  return (
    <InputWrapper>
      <Label>{label}</Label>
      {description && <Description>{description}</Description>}
      {textArea ? (
        <StyledTextArea
          error={error}
          placeholder={placeholder}
          value={value}
          onChange={e => onChange(e.target.value)}
          name={name}
        />
      ) : (
        <StyledInput
          error={error}
          placeholder={placeholder}
          value={value}
          onChange={e => onChange(e.target.value)}
          name={name}
        />
      )}
    </InputWrapper>
  );
};

const Description = styled.div`
  font-size: 14px;
  color: white;
  margin: 5px 0;
`;

const Label = styled.div`
  font-size: 18px;
  color: white;
  font-weight: bold;
  margin: 5px 0;
`;

const InputWrapper = styled.div`
  margin: 10px 0;
`;

const commonInputStyles = (error: boolean) => `
  border-radius: 10px;
  width: 100%;
  background-color: transparent;
  border: 1px solid ${error ? colors.errorColor : colors.accentColor};
  margin: 5px 0;
  padding: 10px;
  color: white;
`;

const StyledInput = styled.input`
  height: 50px;
  ${(props: { error: boolean }) => commonInputStyles(props.error)}
`;

const StyledTextArea = styled.textarea`
  height: 200px;
  ${(props: { error: boolean }) => commonInputStyles(props.error)}
`;

export default EFMyntInput;
