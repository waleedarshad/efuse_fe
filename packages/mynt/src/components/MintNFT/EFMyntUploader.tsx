import React from "react";
import styled from "styled-components";
import colors from "../../styles/colors";
import { MediaTypes } from "../../data/NftTypes";
import axios from "axios";
import ReactS3Uploader, { S3Response } from "react-s3-uploader";

const getSignedUrl = async (file: File, callback: (data: S3Response) => void) => {
  const url = `api/uploads/s3/sign?objectName=${encodeURIComponent(file.name)}&contentType=${encodeURIComponent(
    file.type
  )}&dir=uploads/mynt/`;
  const response = await axios.get(url);
  callback(response.data);
};

const getMediaType = (fileType: string) => {
  switch (fileType) {
    case "image/gif":
      return MediaTypes.GIF;
    case "video/mp4":
    case "video/quicktime":
      return MediaTypes.VIDEO;
    default:
      return MediaTypes.IMAGE;
  }
};

const EFMyntUploader = ({
  onUpload,
  error = false
}: {
  onUpload: (url: string, type: MediaTypes) => void;
  error?: boolean;
}) => {
  const onFinish = (data: S3Response, file: File) => {
    const url = `${data.signedUrl.split("/uploads")[0]}/${data.fileKey}`;
    onUpload(url, getMediaType(file.type));
  };

  return (
    <UploadRectangle error={error}>
      <Text>Image, Video, GIF. Max 40mb</Text>
      <Button>
        Upload
        <ReactS3Uploader
          getSignedUrl={getSignedUrl}
          accept=".gif, .png, .jpg, .jpeg, .mov, .mp4"
          s3path="/mynt"
          uploadRequestHeaders={{ "x-amz-acl": "public-read" }}
          contentDisposition="auto"
          server={process.env.NEXT_PUBLIC_BASE_URL}
          scrubFilename={(filename: string) => filename.replace(/[^\w\d_\-.]+/gi, "")}
          onProgress={() => {}}
          onError={() => {}}
          onFinish={onFinish}
          style={{ display: "none" }}
        />
      </Button>
    </UploadRectangle>
  );
};

const Text = styled.p`
  color: white;
  font-size: 14px;
  margin: 10px 0;
`;

const UploadRectangle = styled.div`
  width: 100%;
  height: 150px;
  border: 2px dashed ${(props: { error: boolean }) => (props.error ? colors.errorColor : colors.accentColor)};
  border-radius: 10px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  margin: 10px 0;
`;

const Button = styled.label`
  background-color: ${colors.accentColor};
  color: black;
  text-transform: uppercase;
  font-weight: bold;
  font-size: 12px;
  border-radius: 5px;
  height: 34px;
  min-width: 99px;
  cursor: pointer;
  border: none;
  text-align: center;
  padding: 9px;
`;

export default EFMyntUploader;
