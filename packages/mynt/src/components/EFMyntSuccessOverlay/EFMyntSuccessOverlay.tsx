import React from "react";
import styled from "styled-components";

const EFMyntSuccessOverlay = ({ content, open }: { content: any; open: boolean }) => {
  return <>{open && <Overlay>{content}</Overlay>}</>;
};

const Overlay = styled.div`
  position: fixed;
  text-align: center;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(0, 0, 0, 0.8);
  z-index: 100;
  overflow: hidden;
`;

export default EFMyntSuccessOverlay;
