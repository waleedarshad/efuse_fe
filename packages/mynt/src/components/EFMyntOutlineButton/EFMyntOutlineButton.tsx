import React from "react";
import styled from "styled-components";
import colors from "../../styles/colors";

// eslint-disable-next-line react/require-default-props
const EFMyntOutlineButton = ({ text, onClick }: { text: string; onClick?: () => void }) => {
  return <Button onClick={onClick || (() => {})}>{text}</Button>;
};

const Button = styled.button`
  background-color: ${colors.greyBackground};
  color: ${colors.accentColor};
  text-transform: uppercase;
  font-weight: bold;
  font-size: 12px;
  border-radius: 5px;
  height: 40px;
  min-width: 200px;
  cursor: pointer;
  border: 2px solid ${colors.accentColor};
`;

export default EFMyntOutlineButton;
