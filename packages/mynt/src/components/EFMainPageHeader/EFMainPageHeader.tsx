import styled from "styled-components";

const EFMainPageHeader = ({ text }: { text: string }) => {
  return (
    <SectionWrapper>
      <StyledHeader>{text}</StyledHeader>
    </SectionWrapper>
  );
};

export default EFMainPageHeader;

const StyledHeader = styled.h1`
  font-family: Open-Sans, sans-serif;
  font-size: 28px;
  font-style: normal;
  font-weight: 700;
  line-height: 38px;
  letter-spacing: 0em;
  text-align: left;
  border-bottom: 5px solid #00c9b7;
  color: white;
  text-transform: uppercase;
`;

const SectionWrapper = styled.div`
  display: flex;
  margin-top: 25px;
  margin-left: 72px;
`;
