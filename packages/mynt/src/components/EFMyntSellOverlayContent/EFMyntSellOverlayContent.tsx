import Link from "next/link";
import React from "react";
import styled from "styled-components";
import { useMutation } from "@apollo/client";
import EFMyntOutlineButton from "../EFMyntOutlineButton/EFMyntOutlineButton";
import EFMyntLargeButton from "../EFMyntLargeButton/EFMyntLargeButton";
import colors from "../../styles/colors";
import { INft } from "../../data/NftTypes";
import { LIST_NFT_FOR_SALE } from "../../graphql/nftQuery";
import ImageHighlight from "../../pageLayouts/NftCustomListingPage/ImageHighlight/ImageHighlight";
import mediaQueries from "../../styles/mediaQueries";
import buttonThemes from "../../styles/buttonThemes";
import EFMyntButton from "../EFMyntButton/EFMyntButton";

const EFMyntSellOverlayContent = ({ salePrice, nft }: { salePrice: string; nft: INft }) => {
  const [listNFTForSale] = useMutation(LIST_NFT_FOR_SALE);

  const handleNFTListing = () => {
    listNFTForSale({ variables: { nftId: nft._id, sellPrice: salePrice } })
      .then(() => {
        window.location.href = "/mycollection";
      })
      .catch(() => {
        alert("Oops! Looks like we couldn't list the NFT for sale. Please try again.");
      });
  };

  return (
    <>
      <StyledHeaderSection>
        <StyledHeader>Confirm Transaction</StyledHeader>
      </StyledHeaderSection>
      <MainImageContainer>
        <ImageHighlight image={nft.assetFile} />
      </MainImageContainer>
      <SaleDetailsSection>
        <SaleDetailsHeader>{nft?.name || "Austin NFT Title Here"}</SaleDetailsHeader>
        <div>
          <StyledSpan>Confirm listing for</StyledSpan>
          <StyledSpan>{salePrice}</StyledSpan>
          <StyledSpan>USD</StyledSpan>
        </div>
      </SaleDetailsSection>
      <StyledButtonWrapper>
        <Link href="/mycollection">
          <EFMyntButton theme={buttonThemes.largeOutline} text="Cancel" />
        </Link>
        <EFMyntButton theme={buttonThemes.largeMynt} text="Confirm Listing" onClick={handleNFTListing} />
      </StyledButtonWrapper>
    </>
  );
};

const StyledHeaderSection = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 145px;
  margin-bottom: 50px;
`;

const StyledHeader = styled.h1`
  font-family: Open-Sans, sans-serif;
  font-size: 28px;
  font-style: normal;
  font-weight: 700;
  line-height: 38px;
  letter-spacing: 0em;
  text-align: left;
  border-bottom: 5px solid #00c9b7;
  color: white;
  text-transform: uppercase;
`;

const MainImageContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 550px;
  flex: 3;
  border: 1px solid #323232;
  border-radius: 10px;
  margin 0 auto;
  @media ${mediaQueries.mobileL} {
    width: 90%;
  }
`;

const SaleDetailsSection = styled.div`
  padding-bottom: 50px;
`;

const SaleDetailsHeader = styled.h2`
  color: white;
`;

const StyledSpan = styled.span`
  color: ${colors.headerText};
  padding: 0px;
  margin: 0px;
  font-size: 14px;
  font-weight: 300;
  margin-top: 5px;
  margin-right: 10px;
`;

const StyledButtonWrapper = styled.div`
  width: 430px;
  display: flex;
  justify-content: space-between;
  margin: 0 auto;
`;

export default EFMyntSellOverlayContent;
