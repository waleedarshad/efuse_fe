import React, { ReactNode, useState } from "react";
import styled from "styled-components";
import {
  Accordion,
  AccordionItem,
  AccordionItemHeading,
  AccordionItemButton,
  AccordionItemPanel
} from "react-accessible-accordion";
import Style from "./EFMyntAccordion.module.scss";

const EFMyntAccordion = ({
  title,
  children,
  isOpenOnRender
}: {
  title: string;
  children: ReactNode;
  isOpenOnRender: boolean;
}) => {
  const [isOpen, toggleOpen] = useState<boolean>(isOpenOnRender);

  return (
    <Accordion
      allowZeroExpanded
      preExpanded={isOpenOnRender ? ["itemId"] : []}
      className={Style.accordion}
      onChange={() => toggleOpen(!isOpen)}
    >
      <AccordionItem className={Style.accordionItem} uuid="itemId">
        <AccordionItemHeading className={Style.accordionHeader}>
          <AccordionItemButton className={Style.accordionItemButton}>
            <AccordionHeaderItem>
              {title}
              <Chevron alt="chevron-up" isOpen={isOpen} />
            </AccordionHeaderItem>
          </AccordionItemButton>
        </AccordionItemHeading>
        <AccordionItemPanel className={Style.accordionItemPanel}>{children}</AccordionItemPanel>
      </AccordionItem>
    </Accordion>
  );
};

const AccordionHeaderItem = styled.div`
  display: flex;
  flex-direction: row;
  gap: 20px;
  width: 100%;
  height: auto;
`;

const Chevron = styled.img`
  width: 20px;
  filter: brightness(0) invert(1);
  margin-left: auto;
  transition: 200ms;
  transform: ${(props: { isOpen: boolean }) => props.isOpen && "rotate(180deg)"};
`;

Chevron.defaultProps = {
  src: "/static/chevron-up.svg"
};

EFMyntAccordion.defaultProps = {
  isOpenOnRender: false
};

export default EFMyntAccordion;
