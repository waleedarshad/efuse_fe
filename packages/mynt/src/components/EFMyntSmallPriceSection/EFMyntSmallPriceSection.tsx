import React from "react";
import styled from "styled-components";
import colors from "../../styles/colors";

const EFMyntSmallPriceSection = () => {
  return (
    <PriceContainer>
      <div>
        <StyledPrice>
          <BoldSpan>$0.00</BoldSpan>
          <StyledSpan>USD</StyledSpan>
        </StyledPrice>
        <StyledSpan>Lowest Ask</StyledSpan>
      </div>
      <VerticalLine />
      <div>
        <StyledPrice>
          <BoldSpan>$0.00</BoldSpan>
          <StyledSpan>USD</StyledSpan>
        </StyledPrice>
        <StyledSpan>Top Sale</StyledSpan>
      </div>
    </PriceContainer>
  );
};

const PriceContainer = styled.div`
  display: flex;
  margin-top: 10px;
`;

const VerticalLine = styled.div`
  width: 3px;
  margin-left: 15px;
  margin-right: 15px;
  background: ${colors.headerText};
`;

const StyledPrice = styled.div`
  display: flex;
`;

const StyledSpan = styled.span`
  color: ${colors.headerText};
  padding: 0px;
  margin: 0px;
  font-size: 14px;
  font-weight: 300;
`;

const BoldSpan = styled.span`
  font-family: Open-Sans, sans-serif;
  font-size: 16px;
  font-style: normal;
  font-weight: 700;
  line-height: 22px;
  letter-spacing: 0em;
  color: ${colors.headerText};
  margin-right: 5px;
`;

export default EFMyntSmallPriceSection;
