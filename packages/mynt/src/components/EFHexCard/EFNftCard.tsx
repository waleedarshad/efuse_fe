import React from "react";
import Link from "next/link";
import { INft } from "../../data/NftTypes";
import EFMyntCard from "../EFMyntCard/EFMyntCard";

const EFNftCard = ({ nft, isOwner, isOwnersListing }: { nft: INft; isOwner: boolean; isOwnersListing: boolean }) => {
  const getLinkUrl = () => {
    if (isOwner) {
      return `/sell/${nft._id}`;
    }
    return `/list/${nft._id}`;
  };
  return (
    <Link href={getLinkUrl()}>
      <div>
        <EFMyntCard nft={nft} isOwner={isOwner} isOwnersListing={isOwnersListing} />
      </div>
    </Link>
  );
};

export default EFNftCard;
