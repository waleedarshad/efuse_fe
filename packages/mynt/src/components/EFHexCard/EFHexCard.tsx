import React from "react";
import Style from "./EFHexCard.module.scss";

interface Props {
  theme: string;
  badge: string;
  badgeAlt: string;
  hoverComponent?: React.ReactNode;
  image: string;
  alt: string;
}

const EFHexCard = ({ theme, badge, badgeAlt, hoverComponent, image, alt }: Props) => {
  return (
    <div className={`${Style.cardWrapper} ${Style[theme]}`}>
      <img alt={badgeAlt} className={Style.rarityImage} src={badge} />
      <div className={Style.hexWrapper}>
        {hoverComponent && hoverComponent}
        <img alt={alt} className={Style.hexImage} src={image} />
      </div>
    </div>
  );
};

export default EFHexCard;
