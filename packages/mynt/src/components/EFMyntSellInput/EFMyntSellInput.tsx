import styled from "styled-components";
import React from "react";
import colors from "../../styles/colors";

const EFMyntSellInput = ({
  placeholder,
  name,
  value,
  onChange,
  description,
  error = false,
  textArea = false
}: {
  placeholder: string;
  name: string;
  value: string;
  onChange: (value: string) => void;
  description?: string;
  textArea?: boolean;
  error?: boolean;
}) => {
  return (
    <InputWrapper>
      {/* <Label>{label}</Label> */}
      {description && <Description>{description}</Description>}
      {textArea ? (
        <StyledTextArea
          error={error}
          placeholder={placeholder}
          value={value}
          onChange={e => onChange(e.target.value)}
          name={name}
        />
      ) : (
        <StyledInput
          error={error}
          placeholder={placeholder}
          value={value}
          onChange={e => onChange(e.target.value)}
          name={name}
        />
      )}
    </InputWrapper>
  );
};

const Description = styled.div`
  font-size: 14px;
  color: white;
  margin: 5px 0;
`;

const InputWrapper = styled.div`
  margin: 10px 0;
`;

const commonInputStyles = (error: boolean) => `
  width: 100%;
  background-color: transparent;
  border-bottom: 2px solid ${error ? colors.errorColor : colors.accentColor};
  margin: 5px 0;
  padding: 10px;
  padding-left: 22px;
  color: white;
  border-right: none;
  border-left: none;
  border-top: none;
  font-size: 18px;
`;

const StyledInput = styled.input`
  height: 50px;
  ${(props: { error: boolean }) => commonInputStyles(props.error)} ::placeholder {
    color: ${colors.lightGrey};
  }
`;

const StyledTextArea = styled.textarea`
  height: 200px;
  ${(props: { error: boolean }) => commonInputStyles(props.error)}
`;

export default EFMyntSellInput;
