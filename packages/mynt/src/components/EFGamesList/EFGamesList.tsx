import React from "react";
import styled from "styled-components";
import Carousel from "react-multi-carousel";
import { IGame, IGames } from "../../data/IGame";
import colors from "../../styles/colors";
import "react-multi-carousel/lib/styles.css";

const EFGamesList = ({
  onGameClick = () => {},
  activeGame,
  games
}: {
  onGameClick: (slug: string) => void;
  activeGame: string | null;
  games: IGames;
}) => {
  const renderGame = (game: IGame) => {
    return (
      <GameImageWrapper key={game._id} onClick={() => onGameClick(game.slug)}>
        <GameImage alt={game.title} src={game.gameImage.url} isActive={activeGame === game.slug} />
      </GameImageWrapper>
    );
  };

  return (
    <GamesListWrapper>
      <Carousel responsive={carouselResponsiveProps} showDots={false} autoPlay={false} keyBoardControl={false}>
        {games?.getGames.map(renderGame) || []}
      </Carousel>
    </GamesListWrapper>
  );
};

const GamesListWrapper = styled.div`
  margin: 20px 0;
`;

const GameImageWrapper = styled.div`
  text-align: center;
  border-radius: 10px;
`;

const GameImage = styled.img`
  object-fit: cover;
  border-radius: 10px;
  border: ${(props: { isActive: boolean }) => (props.isActive ? `4px solid ${colors.accentColor}` : "none")};
  box-shadow: 2px 2px 2px 2px rgba(0, 0, 0, 0.8);
  height: 250px;
  width: 90%;
  margin: 0 10px;
  transition: 200ms;
  &:hover {
    opacity: 80%;
    cursor: pointer;
  }
`;

const carouselResponsiveProps = {
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 7
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 3
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1
  }
};

export default EFGamesList;
