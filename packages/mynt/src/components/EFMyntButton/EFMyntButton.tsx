import React from "react";
import styled, { ThemeProvider } from "styled-components";
import buttonThemes from "../../styles/buttonThemes";
import colors from "../../styles/colors";

// eslint-disable-next-line react/require-default-props
const EFMyntButton = ({ theme, text, onClick }: { theme: object; text: string; onClick?: () => void }) => {
  return (
    <ThemeProvider theme={theme || buttonThemes.regularMynt}>
      <Button onClick={onClick || (() => {})}>{text}</Button>
    </ThemeProvider>
  );
};

const Button = styled.button`
  background-color: ${props => props.theme.backgroundColor};
  color: ${props => props.theme.color};
  text-transform: uppercase;
  font-weight: bold;
  font-size: 12px;
  border-radius: 5px;
  height: 34px;
  min-width: ${props => props.theme.minWidth};
  cursor: pointer;
  border: 2px solid ${colors.accentColor};
`;

export default EFMyntButton;
