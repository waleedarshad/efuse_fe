const mockOwnedNFTs: any = [
  {
    _id: "6089aee4abbeaa2b76e207dc",
    thirdPartyId: null,
    tokenId: "53642712718606013495140431127895653418637098887479042582029685258062049312769",
    tokenAddress: null,
    schemaName: null,
    version: null,
    name: "eFuse Background",
    decimals: null,
    assetContract: {
      name: "OpenSea Collection",
      address: "0x495f947276749ce646f68ac8c248420045cb7b5e",
      type: null,
      schemaName: "ERC1155",
      sellerFeeBasisPoints: 250,
      buyerFeeBasisPoints: 0,
      description: null,
      tokenSymbol: "OPENSTORE",
      imageUrl: null,
      stats: null,
      traitStats: [],
      externalLink: null,
      wikiLink: null
    },
    assetCollection: null,
    description: "# eFuse!!!",
    owner: {
      address: "0x0000000000000000000000000000000000000000",
      config: null,
      profileImgUrl: null,
      user: {
        username: "NullAddress"
      }
    },
    isPresale: null,
    imageUrl:
      "https://lh3.googleusercontent.com/BkRkbtawCFy-59CUTdtZ35i_c2JX7uEqWwOVAHQHQtejbNhtR-iGS_7hW5qZY03TgWhmi22GhdkQ6w5st7qX40hJaG7L3lxHe5Pt",
    imagePreviewUrl:
      "https://lh3.googleusercontent.com/BkRkbtawCFy-59CUTdtZ35i_c2JX7uEqWwOVAHQHQtejbNhtR-iGS_7hW5qZY03TgWhmi22GhdkQ6w5st7qX40hJaG7L3lxHe5Pt=s250",
    imageUrlOriginal: null,
    imageUrlThumbnail:
      "https://lh3.googleusercontent.com/BkRkbtawCFy-59CUTdtZ35i_c2JX7uEqWwOVAHQHQtejbNhtR-iGS_7hW5qZY03TgWhmi22GhdkQ6w5st7qX40hJaG7L3lxHe5Pt=s128",
    animationUrl: null,
    animationOriginalUrl: null,
    openSeaLink:
      "https://opensea.io/assets/0x495f947276749ce646f68ac8c248420045cb7b5e/53642712718606013495140431127895653418637098887479042582029685258062049312769",
    externalLink: "https://efuse.gg/org/efuse",
    traits: {
      rarity: "legendary",
      game: "warzone",
      owner: "eFuse",
      mediaType: "image"
    },
    numSales: 0,
    lastSale: null,
    backgroundColor: null,
    transferFee: null,
    creator: {
      address: "0x7698b30f8ddaac2210a72bc7c791d8a3a972a665",
      config: null,
      profileImgUrl: null,
      user: {
        username: "ef_test"
      }
    }
  },
  {
    _id: "6089aee4abbeaa2b76e207e6",
    thirdPartyId: null,
    tokenId: "53642712718606013495140431127895653418637098887479042582029685260261072568321",
    tokenAddress: null,
    schemaName: null,
    version: null,
    name: "eFuse Mobile Background Image",
    decimals: null,
    assetContract: {
      name: "OpenSea Collection",
      address: "0x495f947276749ce646f68ac8c248420045cb7b5e",
      type: null,
      schemaName: "ERC1155",
      sellerFeeBasisPoints: 250,
      buyerFeeBasisPoints: 0,
      description: null,
      tokenSymbol: "OPENSTORE",
      imageUrl: null,
      stats: null,
      traitStats: [],
      externalLink: null,
      wikiLink: null
    },
    assetCollection: null,
    description: "eFuse epic phone background",
    owner: {
      address: "0x0000000000000000000000000000000000000000",
      config: null,
      profileImgUrl: null,
      user: {
        username: "NullAddress"
      }
    },
    isPresale: null,
    imageUrl:
      "https://lh3.googleusercontent.com/KnpACwLnthBMkznb897P1g1YMy2P99V0qMIBb_JN7xhlXz59bL4WLAvtoA4hA8dqr6RZ_ElnY_D7HAVrkD4uySY_9RoEsTj5mk4QimA",
    imagePreviewUrl:
      "https://lh3.googleusercontent.com/KnpACwLnthBMkznb897P1g1YMy2P99V0qMIBb_JN7xhlXz59bL4WLAvtoA4hA8dqr6RZ_ElnY_D7HAVrkD4uySY_9RoEsTj5mk4QimA=s250",
    imageUrlOriginal: null,
    imageUrlThumbnail:
      "https://lh3.googleusercontent.com/KnpACwLnthBMkznb897P1g1YMy2P99V0qMIBb_JN7xhlXz59bL4WLAvtoA4hA8dqr6RZ_ElnY_D7HAVrkD4uySY_9RoEsTj5mk4QimA=s128",
    animationUrl: null,
    animationOriginalUrl: null,
    openSeaLink:
      "https://opensea.io/assets/0x495f947276749ce646f68ac8c248420045cb7b5e/53642712718606013495140431127895653418637098887479042582029685260261072568321",
    externalLink: "https://twitter.com/eFuseOfficial/status/1275193626985971714",
    traits: {
      rarity: "epic",
      game: "cold-war",
      owner: "eFuse",
      mediaType: "image"
    },
    numSales: 0,
    lastSale: null,
    backgroundColor: null,
    transferFee: null,
    creator: {
      address: "0x7698b30f8ddaac2210a72bc7c791d8a3a972a665",
      config: null,
      profileImgUrl: null,
      user: {
        username: "ef_test"
      }
    }
  },
  {
    _id: "6089aee4abbeaa2b76e207eb",
    thirdPartyId: null,
    tokenId: "53642712718606013495140431127895653418637098887479042582029685256962537684993",
    tokenAddress: null,
    schemaName: null,
    version: null,
    name: "Star Gif",
    decimals: null,
    assetContract: {
      name: "OpenSea Collection",
      address: "0x495f947276749ce646f68ac8c248420045cb7b5e",
      type: null,
      schemaName: "ERC1155",
      sellerFeeBasisPoints: 250,
      buyerFeeBasisPoints: 0,
      description: null,
      tokenSymbol: "OPENSTORE",
      imageUrl: null,
      stats: null,
      traitStats: [],
      externalLink: null,
      wikiLink: null
    },
    assetCollection: null,
    description:
      "# Star Gif. \nIt's very *starry*. And this is a long description. \n\n---\n\n## Sub Section\nThe description supports markdown. Maybe we should too?\n\n> Quote section?!?!",
    owner: {
      address: "0x0000000000000000000000000000000000000000",
      config: null,
      profileImgUrl: null,
      user: {
        username: "NullAddress"
      }
    },
    isPresale: null,
    imageUrl:
      "https://lh3.googleusercontent.com/IuZc0Z44kETUk4o3MtY-Ul257fL0Lo2d-_R7zlx3hMdo_70ER0N9vITXrUmV9mhz3yo39nm50WnKYnOlxXOpk0vEo1Bl2P82rCP9NWU",
    imagePreviewUrl:
      "https://lh3.googleusercontent.com/IuZc0Z44kETUk4o3MtY-Ul257fL0Lo2d-_R7zlx3hMdo_70ER0N9vITXrUmV9mhz3yo39nm50WnKYnOlxXOpk0vEo1Bl2P82rCP9NWU=s250",
    imageUrlOriginal: null,
    imageUrlThumbnail:
      "https://lh3.googleusercontent.com/IuZc0Z44kETUk4o3MtY-Ul257fL0Lo2d-_R7zlx3hMdo_70ER0N9vITXrUmV9mhz3yo39nm50WnKYnOlxXOpk0vEo1Bl2P82rCP9NWU=s128",
    animationUrl: null,
    animationOriginalUrl: null,
    openSeaLink:
      "https://opensea.io/assets/0x495f947276749ce646f68ac8c248420045cb7b5e/53642712718606013495140431127895653418637098887479042582029685256962537684993",
    externalLink: "https://efuse.wtf",
    traits: {
      rarity: "epic",
      game: "rocket-league",
      owner: null,
      mediaType: "gif"
    },
    numSales: 0,
    lastSale: null,
    backgroundColor: null,
    transferFee: null,
    creator: {
      address: "0x7698b30f8ddaac2210a72bc7c791d8a3a972a665",
      config: null,
      profileImgUrl: null,
      user: {
        username: "ef_test"
      }
    }
  },
  {
    _id: "6089aee4abbeaa2b76e207f0",
    thirdPartyId: null,
    tokenId: "53642712718606013495140431127895653418637098887479042582029685255863026057217",
    tokenAddress: null,
    schemaName: null,
    version: null,
    name: "BigBunny",
    decimals: null,
    assetContract: {
      name: "OpenSea Collection",
      address: "0x495f947276749ce646f68ac8c248420045cb7b5e",
      type: null,
      schemaName: "ERC1155",
      sellerFeeBasisPoints: 250,
      buyerFeeBasisPoints: 0,
      description: null,
      tokenSymbol: "OPENSTORE",
      imageUrl: null,
      stats: null,
      traitStats: [],
      externalLink: null,
      wikiLink: null
    },
    assetCollection: null,
    description: null,
    owner: {
      address: "0x0000000000000000000000000000000000000000",
      config: null,
      profileImgUrl: null,
      user: {
        username: "NullAddress"
      }
    },
    isPresale: null,
    imageUrl:
      "https://lh3.googleusercontent.com/LC-CeIcy-0275nHtTCXNjOPXBPULXwLovxW0j0_ONsHnGbYzgLe4BUuZsBhtthySLbM5B6lSm5aGhAvDdo2Z4gmNsZCL-ZPN5yqxJzA",
    imagePreviewUrl:
      "https://lh3.googleusercontent.com/LC-CeIcy-0275nHtTCXNjOPXBPULXwLovxW0j0_ONsHnGbYzgLe4BUuZsBhtthySLbM5B6lSm5aGhAvDdo2Z4gmNsZCL-ZPN5yqxJzA=s250",
    imageUrlOriginal: null,
    imageUrlThumbnail:
      "https://lh3.googleusercontent.com/LC-CeIcy-0275nHtTCXNjOPXBPULXwLovxW0j0_ONsHnGbYzgLe4BUuZsBhtthySLbM5B6lSm5aGhAvDdo2Z4gmNsZCL-ZPN5yqxJzA=s128",
    animationUrl: "https://storage.opensea.io/files/7e245fc2483742414604ce7e67c13111.mp4",
    animationOriginalUrl: "https://storage.opensea.io/files/7e245fc2483742414604ce7e67c13111.mp4",
    openSeaLink:
      "https://opensea.io/assets/0x495f947276749ce646f68ac8c248420045cb7b5e/53642712718606013495140431127895653418637098887479042582029685255863026057217",
    externalLink: null,
    traits: {
      rarity: "rare",
      game: "fortnite",
      owner: "eFuse",
      mediaType: "video"
    },
    numSales: 0,
    lastSale: null,
    backgroundColor: null,
    transferFee: null,
    creator: {
      address: "0x7698b30f8ddaac2210a72bc7c791d8a3a972a665",
      config: null,
      profileImgUrl: null,
      user: {
        username: "ef_test"
      }
    }
  }
];

export default mockOwnedNFTs;
