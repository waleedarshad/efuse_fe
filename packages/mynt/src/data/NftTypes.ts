export interface INft {
  _id: string;
  assetFile: string;
  name: string;
  externalLink: string;
  description: string;
  unlockableContent: string;
  eFuseUserId: string;
  isListed: boolean;
  traits: Traits;
  sellPrice: string;
  transactionHistory: NFTTransaction[];
}

export interface NftQuery {
  address: String;
  token: String;
}

export interface Traits {
  rarity: Rarity;
  owner: string;
  creator: string;
  game: string;
  mediaType: MediaTypes;
}

export enum MediaTypes {
  GIF = "gif",
  VIDEO = "video",
  IMAGE = "image"
}

export enum Rarity {
  RARE = "rare",
  LEGENDARY = "legendary",
  EPIC = "epic"
}

export interface NFTTransaction {
  purchaserId: string;
  sellerId: string;
  nftId: string;
  orderId: string;
  payerId: string;
  purchasePrice: string;
}
