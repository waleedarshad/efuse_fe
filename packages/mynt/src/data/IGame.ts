export interface IGame {
  gameImage: {
    url: string;
  };
  _id: string;
  title: string;
  slug: string;
}

export interface IGames {
  getGames: [IGame];
}
