const getNearWalletConfig = (keyStore: any) => {
  return {
    networkId: process.env.NEXT_PUBLIC_NEAR_NETWORK_ID || "testnet",
    keyStore, // optional if not signing transactions
    nodeUrl: process.env.NEXT_PUBLIC_NEAR_NODE_URL || "https://rpc.testnet.near.org",
    walletUrl: process.env.NEXT_PUBLIC_NEAR_WALLET_URL || "https://wallet.testnet.near.org",
    helperUrl: process.env.NEXT_PUBLIC_NEAR_HELPER_URL || "https://helper.testnet.near.org",
    explorerUrl: process.env.NEXT_PUBLIC_NEAR_EXPLORER_URL || "https://explorer.testnet.near.org"
  };
};

export default getNearWalletConfig;
