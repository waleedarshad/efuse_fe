import { NextApiRequest, NextApiResponse } from "next";
import getJwt from "../../helpers/getJwt";

export default async (req: NextApiRequest, res: NextApiResponse) => {
  try {
    const jwt = await getJwt(req, res);

    const baseURL =
      process.env.EFUSE_API_BASE_URL?.indexOf("http") === 0
        ? process.env.EFUSE_API_BASE_URL
        : `https://${process.env.EFUSE_API_BASE_URL}`;

    const headers = new Headers();

    if (req.headers["content-type"]) headers.set("Content-Type", req.headers["content-type"]);
    if (jwt) headers.set("Authorization", jwt ? `Bearer ${jwt}` : "");

    const response = await fetch(`${baseURL}${req.url}`, {
      headers,
      method: req.method,
      body: req.body ? JSON.stringify(req.body) : null
    });

    const result = await response.json();
    res.status(response.status || 200).json(result);
  } catch (error) {
    res.status(error.status || 500).json({
      code: error.code,
      error: error.message
    });
  }
};
