import { initAuth0 } from "@auth0/nextjs-auth0";

const baseURL = process.env.AUTH0_BASE_URL ? process.env.AUTH0_BASE_URL : `https://${process.env.VERCEL_URL}`;

// https://auth0.github.io/nextjs-auth0/modules/config.html#optional
const auth0 = initAuth0({
  baseURL,
  secret: process.env.AUTH0_SECRET,
  issuerBaseURL: process.env.AUTH0_ISSUER_BASE_URL,
  clientID: process.env.AUTH0_CLIENT_ID,
  clientSecret: process.env.AUTH0_CLIENT_SECRET
});

export default auth0.handleAuth();
