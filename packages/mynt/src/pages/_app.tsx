/* eslint-disable react/jsx-filename-extension */
import { AppProps } from "next/app";
import { ApolloProvider } from "@apollo/client";
import { UserProvider } from "@auth0/nextjs-auth0";
import { DefaultSeo } from "next-seo";
import React from "react";
import { PayPalScriptProvider } from "@paypal/react-paypal-js";
import SEO from "../../next-seo.config";
import { getApolloClient } from "../helpers/DefaultApolloClient";
import "../styles/globals.css";

const client = getApolloClient(false);

const MyApp = ({ Component, pageProps }: AppProps) => {
  return (
    <UserProvider>
      <ApolloProvider client={client}>
        <PayPalScriptProvider options={{ "client-id": String(process.env.PAYPAL_CLIENT_ID) }}>
          <DefaultSeo {...SEO} />
          <Component {...pageProps} />
        </PayPalScriptProvider>
      </ApolloProvider>
    </UserProvider>
  );
};

export default MyApp;
