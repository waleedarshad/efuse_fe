import React from "react";
import NftCustomListingPage from "../../../pageLayouts/NftCustomListingPage/NftCustomListingPage";
import { GET_STAGED_NFT } from "../../../graphql/nftQuery";
import { INft } from "../../../data/NftTypes";
import { getApolloClient } from "../../../helpers/DefaultApolloClient";

export const getServerSideProps = async (context: { query: any }) => {
  const client = getApolloClient(true);
  const { query } = context;
  let stagedNftData = null;

  await client
    .query({
      query: GET_STAGED_NFT,
      variables: { _id: query.id }
    })
    .then(result => {
      stagedNftData = result.data.findStagedNFT;
    });

  return {
    props: {
      nft: stagedNftData
    }
  };
};

const CustomListPage = ({ nft }: { nft: INft }) => {
  return <NftCustomListingPage nft={nft} />;
};

export default CustomListPage;
