import React from "react";
import SellNFTPage from "../../../pageLayouts/SellNFTPage/SellNFTPage";
import { getApolloClient } from "../../../helpers/DefaultApolloClient";
import { GET_STAGED_NFT } from "../../../graphql/nftQuery";
import { INft } from "../../../data/NftTypes";

export const getServerSideProps = async (context: { query: any }) => {
  const client = getApolloClient(true);
  const { tokenId } = context.query;
  let stagedNftData = null;

  await client
    .query({
      query: GET_STAGED_NFT,
      variables: { _id: tokenId }
    })
    .then(result => {
      stagedNftData = result.data.findStagedNFT;
    });

  return {
    props: {
      nft: stagedNftData
    }
  };
};

const SellPage = ({ nft }: { nft: INft }) => {
  return <SellNFTPage nft={nft} />;
};

export default SellPage;
