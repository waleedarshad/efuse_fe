import React from "react";
import MintNFTPage from "../../pageLayouts/MintNFTPage/MintNFTPage";

const Page = () => {
  return <MintNFTPage />;
};

export default Page;
