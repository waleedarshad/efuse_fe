import React from "react";
import { NftQuery } from "../../../../data/NftTypes";
import NftListingPage from "../../../../pageLayouts/NftListingPage/NftListingPage";

export async function getServerSideProps(context: { query: any }) {
  return {
    props: {
      query: context.query
    }
  };
}

const NftPage = ({ query }: { query: NftQuery }) => {
  return <NftListingPage query={query} />;
};

export default NftPage;
