import CollectionsPage from "../pageLayouts/CollectionsPage/CollectionsPage";
import { getApolloClient } from "../helpers/DefaultApolloClient";
import { IGames } from "../data/IGame";
import { INft } from "../data/NftTypes";
import GET_GAMES from "../graphql/gamesQuery";
import { GET_NFT_ASSETS } from "../graphql/nftQuery";

export const getServerSideProps = async () => {
  const client = await getApolloClient(true);
  let gamesData: IGames | undefined;
  let nftData;

  await client
    .query({
      query: GET_GAMES
    })
    .then(result => {
      gamesData = result.data;
    });

  await client
    .query({
      query: GET_NFT_ASSETS
    })
    .then(result => {
      nftData = result.data.getStagedNFTs;
    });

  return {
    props: {
      games: gamesData,
      nfts: nftData
    }
  };
};

const Home = ({ games, nfts }: { games: IGames; nfts: [INft] }) => {
  return <CollectionsPage games={games} nfts={nfts} />;
};

export default Home;
