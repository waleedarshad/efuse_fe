import React from "react";
import { withPageAuthRequired } from "@auth0/nextjs-auth0";
import MyCollectionPage from "../../pageLayouts/MyCollectionPage/MyCollectionPage";
import { getAuthenticatedSSRApolloClient } from "../../helpers/DefaultApolloClient";
import { INft } from "../../data/NftTypes";
import { GET_NFT_COLLECTION } from "../../graphql/nftQuery";

export const getServerSideProps = withPageAuthRequired({
  getServerSideProps: async ctx => {
    const client = await getAuthenticatedSSRApolloClient(ctx);

    const nfts: INft[] = (
      await client.query({
        query: GET_NFT_COLLECTION
      })
    ).data.getNFTCollection;

    return {
      props: {
        nfts
      }
    };
  }
});
const MyCollection = ({ nfts }: { nfts: INft[] }) => {
  return <MyCollectionPage nfts={nfts} />;
};

export default MyCollection;
