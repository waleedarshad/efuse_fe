import colors from "./colors";

const rarityThemes = {
  rare: { color: colors.rare },
  epic: { color: colors.epic },
  legendary: { color: colors.legendary },
  default: { color: colors.accentColor }
};

export default rarityThemes;
