const colors = {
  headerText: "#ffffff",
  navColor: "#000000",
  accentColor: "#00c9b7",
  errorColor: "#FF0000",
  rare: "#2e87ff",
  epic: "#fb2eff",
  legendary: "#ff7d00",
  greyBackground: "#16181a",
  lightGrey: "#d6d6da",
  detailsText: "#fdfdfd",
  greyBorder: "#464646"
};

export default colors;
