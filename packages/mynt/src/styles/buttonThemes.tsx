import colors from "./colors";

const buttonThemes = {
  regularMynt: {
    backgroundColor: colors.accentColor,
    color: colors.navColor,
    minWidth: "99px"
  },
  regularOutline: {
    backgroundColor: colors.navColor,
    color: colors.accentColor,
    minWidth: "99px"
  },
  largeMynt: {
    backgroundColor: colors.accentColor,
    color: colors.navColor,
    minWidth: "200px"
  },
  largeOutline: {
    backgroundColor: colors.navColor,
    color: colors.accentColor,
    minWidth: "200px"
  }
};

export default buttonThemes;
