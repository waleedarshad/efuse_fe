import React from "react";
import NavigationWrapper from "../NavigationWrapper/NavigationWrapper";
import EFMyntSection from "../../components/EFMyntSection/EFMyntSection";
import NftList from "../../components/EFMyntSection/NftList/NftList";
import EFMainPageHeader from "../../components/EFMainPageHeader/EFMainPageHeader";
import { INft } from "../../data/NftTypes";

const MyCollectionPage = ({ nfts }: { nfts: INft[] }) => {
  return (
    <>
      <NavigationWrapper navHeight={100}>
        <EFMainPageHeader text="My Collection" />
        <EFMyntSection title="Owned">
          <NftList nftList={nfts.filter(nft => !nft.isListed)} isOwnersListing={false} />
        </EFMyntSection>
        <EFMyntSection title="Listed">
          <NftList nftList={nfts.filter(nft => nft.isListed)} isOwnersListing />
        </EFMyntSection>
      </NavigationWrapper>
    </>
  );
};

export default MyCollectionPage;
