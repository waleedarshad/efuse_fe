import React from "react";
import styled from "styled-components";
import Link from "next/link";
import { useUser } from "@auth0/nextjs-auth0";
import colors from "../../../styles/colors";
import EFMyntButton from "../../../components/EFMyntButton/EFMyntButton";
import EFTextLink from "../../../components/EFTextLink/EFTextLink";
import buttonThemes from "../../../styles/buttonThemes";
import EFNearWallet from "../../../components/EFNearWallet/EFNearWallet";

const NavBar = ({ navHeight }: { navHeight: number }) => {
  const { user }: any = useUser();
  let profileKey;

  let userRoles;

  if (user) {
    profileKey = Object.keys(user).filter(item => item.includes("/profile"));
    userRoles = user[profileKey[0]]?.roles;
  }

  const loggedInUserIsAdmin: boolean = userRoles?.includes("admin");

  return (
    <NavStyle navHeight={navHeight}>
      <Link href="/">
        <Logo alt="Mynt: Powered by eFuse" src="/static/myntLogoTagline.png" />
      </Link>

      <LinkArea>
        <EFTextLink href="/">Marketplace</EFTextLink>
        <EFTextLink href={user ? "/mycollection" : "/api/auth/login"}>My Collection</EFTextLink>
      </LinkArea>

      <ButtonsArea>
        {loggedInUserIsAdmin && (
          <Link href={user ? "/create" : "/api/auth/login"}>
            <EFMyntButton theme={buttonThemes.regularMynt} text="Create" />
          </Link>
        )}

        {user ? (
          <Link href="/api/auth/logout">
            <EFMyntButton theme={buttonThemes.regularMynt} text="Logout" />
          </Link>
        ) : (
          <Link href="/api/auth/login">
            <EFMyntButton theme={buttonThemes.regularOutline} text="Login" />
          </Link>
        )}

        {user && user.picture && (
          <ProfilePicture src={user.picture} alt={user.name || ""} title={user.name || undefined} />
        )}
        <EFNearWallet />
      </ButtonsArea>
    </NavStyle>
  );
};

export default NavBar;

const NavStyle = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  background-color: ${colors.navColor};
  height: ${(props: { navHeight: number }) => `${props.navHeight}px`};
  position: fixed;
  z-index: 999;
  padding: 10px 50px 10px 35px;
`;

const ButtonsArea = styled.div`
  margin-left: auto;
  display: flex;
  column-gap: 8px;
  align-items: center;
`;

const Logo = styled.img`
  height: 37px;
  &:hover {
    cursor: pointer;
  }
`;

const LinkArea = styled.div`
  display: flex;
  align-self: center;
  justify-self: center;
  width: 80%;
  justify-content: center;
`;

const ProfilePicture = styled.img`
  height: 42px;
  width: 42px;
  border-radius: 46px;
  border: 2px solid ${colors.rare};
  padding: 2px;
  background-color: ${colors.navColor};
  margin: 0px 0px 0px 15px;ß
`;
