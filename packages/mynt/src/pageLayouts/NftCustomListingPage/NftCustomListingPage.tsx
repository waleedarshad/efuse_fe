import React, { useState } from "react";
import styled from "styled-components";
import DetailsContainer from "./DetailsContainer/DetailsContainer";
import SaleComponent from "./SaleComponent/SaleComponent";
import ImageHighlight from "./ImageHighlight/ImageHighlight";
import NavigationWrapper from "../NavigationWrapper/NavigationWrapper";
import EFMyntAccordion from "../../components/EFMyntAccordion/EFMyntAccordion";
import { INft } from "../../data/NftTypes";
import mediaQueries from "../../styles/mediaQueries";
import EFMyntSuccessOverlay from "../../components/EFMyntSuccessOverlay/EFMyntSuccessOverlay";
import EFMyntPurchaseSuccessContent from "../../components/EFMyntPurchaseSuccessContent/EFMyntPurchaseSuccessContent";

const NftCustomListingPage = ({ nft }: { nft: INft }) => {
  const [showSuccessModal, setShowSuccessModal] = useState(false);

  return (
    <NavigationWrapper navHeight={100}>
      <PageWrapper>
        <FlexGrid>
          <LeftPanel>
            <ImageHighlight image={nft?.assetFile} />
          </LeftPanel>
          <RightPanel>
            <DetailsContainer
              name={nft.name}
              owner={nft.traits.owner}
              creator={nft.traits.creator}
              rarity={nft.traits.rarity}
            />
            <SaleComponent
              nftId={nft._id}
              sellerId={nft.eFuseUserId}
              sellPrice={nft.sellPrice}
              onPurchaseSuccess={() => setShowSuccessModal(true)}
            />
          </RightPanel>
        </FlexGrid>
        <BottomSection>
          <EFMyntAccordion title="NFT Details" isOpenOnRender>
            {nft.description}
          </EFMyntAccordion>
          <EFMyntAccordion title="Sale History">No sale history</EFMyntAccordion>
          <EFMyntAccordion title="Offer History">No price history</EFMyntAccordion>
        </BottomSection>
      </PageWrapper>
      <EFMyntSuccessOverlay content={<EFMyntPurchaseSuccessContent nft={nft} />} open={showSuccessModal} />
    </NavigationWrapper>
  );
};

const PageWrapper = styled.div`
  height: auto;
  padding: 30px 15%;
  @media ${mediaQueries.laptop} {
    padding: 30px 30px;
  }
`;

const FlexGrid = styled.div`
  display: flex;
  width: 100%;
  gap: 20px;
  flex-wrap: wrap;
  flex: 1 1 auto;
`;

const LeftPanel = styled.div`
  height: 100%;
  flex: 3;
  display: flex;
  flex-direction: column;
  align-items: center;
  flex-shrink: 0;
  @media ${mediaQueries.mobileL} {
    flex-basis: 100%;
    flex-direction: row;
  }
`;

const RightPanel = styled.div`
  display: flex;
  flex-direction: column;
  gap: 7px;
  height: 100%;
  flex: 2;
  justify-content: center;
  @media ${mediaQueries.laptop} {
    flex-basis: 100%;
  }
`;

const BottomSection = styled.div`
  display: flex;
  flex-direction: column;
  gap: 20px;
  width: 100%;
  height: auto;
  margin-top: 50px;
`;

export default NftCustomListingPage;
