import { PayPalButtons } from "@paypal/react-paypal-js";
import React from "react";
import styled from "styled-components";
import { CreateOrderActions, OnApproveData, OnApproveActions } from "@paypal/paypal-js/types/components/buttons";
import { useMutation } from "@apollo/client";
import SalePrice from "./SalePrice/SalePrice";
import colors from "../../../styles/colors";
import mediaQueries from "../../../styles/mediaQueries";
import { CREATE_NFT_TRANSACTION, NFTTransactionArgs } from "../../../graphql/nftTransactionQuery";

const SaleComponent = ({
  nftId,
  sellerId,
  sellPrice,
  onPurchaseSuccess
}: {
  nftId: string;
  sellerId: string;
  sellPrice: string;
  onPurchaseSuccess: () => void;
}) => {
  const [createNFTTransaction] = useMutation<NFTTransactionArgs>(CREATE_NFT_TRANSACTION);

  const captureNFTTransaction = (details: any) => {
    const args: NFTTransactionArgs = {
      orderId: details.id,
      payerId: details.payer.payer_id,
      purchaseAmount: details.purchase_units[0].amount.value,
      nftId,
      sellerId
    };

    createNFTTransaction({ variables: { args } })
      .then(onPurchaseSuccess)
      .catch(() => {
        alert("Oops! Something went wrong. Please try again.");
      });
  };

  const createOrder = (data: any, actions: CreateOrderActions) => {
    // NOTE: value MUST be in format "123.00" or paypal login screen will NOT work
    return actions.order.create({
      purchase_units: [
        {
          amount: {
            value: sellPrice
          }
        }
      ]
    });
  };

  const onApprove = async (data: OnApproveData, actions: OnApproveActions) => {
    await actions.order.capture().then(details => {
      if (details.status === "COMPLETED" && details.purchase_units[0].amount.value === sellPrice) {
        captureNFTTransaction(details);
        // TODO: update existing stagedNFT with sellPrice = 0 and add transaction history
      } else {
        alert("Oh no! Either status is not complete or purchase price doesn't match");
      }
    });
  };

  // TODO: Disable button or show indication to user to login before trying to purchase item

  return (
    <SaleComponentWrapper>
      <SalePriceWrapper>
        <SalePrice price={sellPrice} />
      </SalePriceWrapper>
      <SalePriceWrapper>
        <SalePrice price={sellPrice} priceSubText="Top Sale" />
      </SalePriceWrapper>
      <PayPalButtonWrapper>
        <PayPalButtons
          style={{ color: "blue", shape: "pill", label: "pay", height: 40 }}
          fundingSource="paypal"
          createOrder={(data: any, actions) => createOrder(data, actions)}
          onApprove={(data: OnApproveData, actions: OnApproveActions) => onApprove(data, actions)}
        />
      </PayPalButtonWrapper>
    </SaleComponentWrapper>
  );
};

const SaleComponentWrapper = styled.div`
  width: 100%;
  color: ${colors.headerText};
`;

const SalePriceWrapper = styled.div`
  padding: 10px 0px;
  @media ${mediaQueries.laptop} {
    padding: 5px 0px;
  }
`;

const PayPalButtonWrapper = styled.div`
  margin-top: 30px;
`;

export default SaleComponent;
