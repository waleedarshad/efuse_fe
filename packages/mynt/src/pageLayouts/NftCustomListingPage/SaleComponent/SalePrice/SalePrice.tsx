import React from "react";
import styled from "styled-components";
import colors from "../../../../styles/colors";
import mediaQueries from "../../../../styles/mediaQueries";

const SalePrice = ({
  price,
  priceCurrency,
  priceSubText
}: {
  price: string;
  priceCurrency: string;
  priceSubText: string;
}) => {
  return (
    <>
      <PriceWrapper>
        <Price>{price}</Price>
        <PriceCurrency>{priceCurrency}</PriceCurrency>
      </PriceWrapper>
      <SubPriceText>{priceSubText}</SubPriceText>
    </>
  );
};

SalePrice.defaultProps = {
  price: "$0.00",
  priceCurrency: "USD",
  priceSubText: "Lowest Ask"
};

const PriceWrapper = styled.div`
  display: flex;
  align-items: flex-end;
`;

const Price = styled.p`
  color: ${colors.headerText};
  font-size: 28px;
  font-weight: bold;
  padding: 0px;
  margin: 0px;
  @media ${mediaQueries.laptop} {
    font-size: 24px;
  }
`;

const PriceCurrency = styled.p`
  color: ${colors.headerText};
  font-size: 14px;
  text-transform: uppercase;
  margin: 0px;
  padding: 0px;
  padding-bottom: 4px;
  margin-left: 5px;
  @media ${mediaQueries.laptop} {
    font-size: 12px;
  }
`;

const SubPriceText = styled.p`
  color: ${colors.headerText};
  padding: 0px;
  margin: 0px;
  font-size: 14px;
  font-weight: 300;
  margin-top: 5px;
  @media ${mediaQueries.laptop} {
    margin-top: 0px;
  }
`;

export default SalePrice;
