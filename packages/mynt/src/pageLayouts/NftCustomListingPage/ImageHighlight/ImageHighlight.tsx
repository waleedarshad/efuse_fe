import React from "react";
import styled from "styled-components";

const ImageHighlight = ({ image }: { image: string }) => {
  return (
    <HighlightWrapper>
      <MainImageContainer>
        <MainImage src={image} />
      </MainImageContainer>
    </HighlightWrapper>
  );
};

const HighlightWrapper = styled.div`
  position: relative;
  display: flex;
  width: 100%;
  gap: 20px;
  height: 400px;
`;

const MainImageContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
  flex: 3;
  border: 1px solid #323232;
  padding: 30px 30px 30px 30px;
  border-radius: 10px;
`;

const MainImage = styled.img`
  max-height: 300px;
  border-radius: 10px;
  align-self: center;
`;

export default ImageHighlight;
