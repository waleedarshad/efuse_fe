import React from "react";
import styled, { ThemeProvider } from "styled-components";
import colors from "../../../styles/colors";
import mediaQueries from "../../../styles/mediaQueries";
import rarityThemes from "../../../styles/rarityThemes";
import { Rarity } from "../../../data/NftTypes";

const DetailsContainer = ({
  name,
  owner,
  creator,
  rarity
}: {
  name: string;
  owner: string;
  creator: string;
  rarity: Rarity;
}) => {
  return (
    <ThemeProvider theme={rarityThemes[rarity] || rarityThemes.default}>
      <DetailsWrapper>
        <SubTitle>
          Created by <b>{creator}</b>
        </SubTitle>
        <Title>{name}</Title>
        <OwnerWrapper>
          <OwnerImage />
          <OwnedTitle>
            Owned by <b>{owner === "NullAddress" ? creator : owner}</b>
          </OwnedTitle>
        </OwnerWrapper>
        <StyledRarity>{rarity || ""}</StyledRarity>
      </DetailsWrapper>
    </ThemeProvider>
  );
};

const DetailsWrapper = styled.div`
  border-bottom: 1px solid #6a6a6f;
`;

const Title = styled.h1`
  color: #ffffff;
  padding: 0px;
  margin: 0px;
  @media ${mediaQueries.laptop} {
    font-size: 26px;
  }
`;

const SubTitle = styled.h3`
  color: ${colors.accentColor};
  font-weight: 300;
  margin: 0px;
  margin-bottom: 10px;
  @media ${mediaQueries.laptop} {
    font-size: 14px;
    margin-bottom: 5px;
  }
`;

const StyledRarity = styled.p`
  font-family: Open-Sans, sans-serif;
  font-size: 18px;
  font-style: normal;
  font-weight: 700;
  letter-spacing: 0em;
  text-align: left;
  text-transform: uppercase;
  color: ${props => props.theme.color};
`;

const OwnerWrapper = styled.div`
  display: flex;
  align-items: center;
  margin: 0px;
  margin-top: 10px;
`;

const OwnedTitle = styled.p`
  color: #ffffff;
  font-weight: 300;
  padding: 0px 0px 0px 10px;
  margin: 0px;
  font-size: 20px;
  @media ${mediaQueries.laptop} {
    font-size: 12px;
  }
`;

const OwnerImage = styled.img`
  width: 35px;
  height: 35px;
  @media ${mediaQueries.laptop} {
    width: 25px;
    height: 25px;
  }
`;

OwnerImage.defaultProps = {
  src: "https://cdn3.iconfinder.com/data/icons/avatars-round-flat/33/avat-01-512.png"
};

export default DetailsContainer;
