import React, { useState } from "react";
import NavigationWrapper from "../NavigationWrapper/NavigationWrapper";
import EFMintNFTForm from "../../components/MintNFT/EFMyntNFTForm";
import EFMyntSuccessOverlay from "../../components/EFMyntSuccessOverlay/EFMyntSuccessOverlay";
import EFMyntSuccessOverlayContent from "../../components/MintNFT/EFMyntSuccessOverlayContent";

const MintNFTPage = () => {
  const [showSuccessModal, setShowSuccessModal] = useState(false);

  return (
    <NavigationWrapper navHeight={100}>
      <EFMintNFTForm
        onCreateSuccess={() => {
          setShowSuccessModal(true);
        }}
      />
      <EFMyntSuccessOverlay content={<EFMyntSuccessOverlayContent />} open={showSuccessModal} />
    </NavigationWrapper>
  );
};

export default MintNFTPage;
