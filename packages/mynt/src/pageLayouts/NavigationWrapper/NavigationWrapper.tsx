import React, { ReactNode } from "react";
import styled from "styled-components";
import NavBar from "../Navigation/NavBar/NavBar";

const NavigationWrapper = ({ children, navHeight }: { children: ReactNode; navHeight: number }) => {
  return (
    <NavigationWrapperStyle>
      <NavBar navHeight={navHeight} />
      <ContentWrapper navHeight={navHeight}>{children}</ContentWrapper>
    </NavigationWrapperStyle>
  );
};

const NavigationWrapperStyle = styled.div`
  height: 100%;
`;

const ContentWrapper = styled.div`
  padding-top: ${(props: { navHeight: number }) => `${props.navHeight}px`};
  height: 100%;
`;

export default NavigationWrapper;
