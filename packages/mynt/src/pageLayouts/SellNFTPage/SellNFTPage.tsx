import React, { useState } from "react";
import styled from "styled-components";
import Link from "next/link";
import NavigationWrapper from "../NavigationWrapper/NavigationWrapper";
import EFMyntOutlineButton from "../../components/EFMyntOutlineButton/EFMyntOutlineButton";
import EFMyntLargeButton from "../../components/EFMyntLargeButton/EFMyntLargeButton";
import EFMyntSuccessOverlay from "../../components/EFMyntSuccessOverlay/EFMyntSuccessOverlay";
import EFMyntSellOverlayContent from "../../components/EFMyntSellOverlayContent/EFMyntSellOverlayContent";
import ImageHighlight from "../NftCustomListingPage/ImageHighlight/ImageHighlight";
import DetailsContainer from "../NftCustomListingPage/DetailsContainer/DetailsContainer";
import colors from "../../styles/colors";
import { INft } from "../../data/NftTypes";
import EFMyntSmallPriceSection from "../../components/EFMyntSmallPriceSection/EFMyntSmallPriceSection";
import EFMyntSellDetailsSection from "../../components/EFMyntSellDetailsSection/EFMyntSellDetailsSection";
import EFMyntSellInput from "../../components/EFMyntSellInput/EFMyntSellInput";

const SellNFTPage = ({ nft }: { nft: INft }) => {
  const [showSuccessModal, setShowSuccessModal] = useState(false);
  const [salePrice, setSalePrice] = useState<string>("");

  // TODO: update hard-coded values where we are able

  return (
    <NavigationWrapper navHeight={100}>
      <PageWrapper>
        <FlexGrid>
          <LeftPanel>
            <ImageHighlight image={nft.assetFile} />
          </LeftPanel>
          <RightPanel>
            <DetailsContainer
              name={nft.name}
              owner={nft.traits.owner}
              creator={nft.traits.creator}
              rarity={nft.traits.rarity}
            />
            <div>
              <EFMyntSellDetailsSection />
              <EFMyntSmallPriceSection />
            </div>
          </RightPanel>
        </FlexGrid>
      </PageWrapper>
      <StyledSaleForm>
        <StyledInputSection>
          <StyledInputIcon src="../../static/tags.svg" alt="tags" />
          <EFMyntSellInput placeholder="Enter price in USD" name="price" value={salePrice} onChange={setSalePrice} />
        </StyledInputSection>
        <StyledFormText>Creator cut 10%</StyledFormText>
        <InfoSection>
          <StyledIcon src="../../static/info-circle.svg" alt="info tags" />
          <SmallStyledSpan>Creator cut</SmallStyledSpan>
        </InfoSection>
        <StyledFormText>Total after tax</StyledFormText>
      </StyledSaleForm>
      <StyledButtonWrapper>
        {/* eslint-disable-next-line no-return-assign */}
        <Link href="/mycollection">
          <EFMyntOutlineButton text="Cancel" />
        </Link>
        <EFMyntLargeButton
          text="Place for Sale"
          onClick={() => {
            setShowSuccessModal(true);
          }}
        />
      </StyledButtonWrapper>
      <EFMyntSuccessOverlay
        content={<EFMyntSellOverlayContent salePrice={salePrice} nft={nft} />}
        open={showSuccessModal}
      />
    </NavigationWrapper>
  );
};

const PageWrapper = styled.div`
  padding: 20px 15%;
`;

const FlexGrid = styled.div`
  display: flex;
  width: 100%;
  gap: 20px;
  border-bottom: 1px solid ${colors.accentColor};
  padding-bottom: 80px;
`;

const LeftPanel = styled.div`
  height: auto;
  flex: 3;
  display: flex;
  flex-direction: column;
  align-items: center;
  flex-shrink: 0;
`;

const RightPanel = styled.div`
  display: flex;
  flex-direction: column;
  gap: 7px;
  height: auto;
  flex: 2;
  justify-content: center;
`;

const SmallStyledSpan = styled.span`
  font-size: 12px;
  margin-left: 5px;
  color: ${colors.lightGrey};
`;

const InfoSection = styled.div`
  display: flex;
  align-items: center;
  margin-top: 5px;
`;

const StyledSaleForm = styled.div`
  width: 500px;
  margin: 0 auto;
  margin-bottom: 50px;
`;

const StyledInputSection = styled.div`
  position: relative;
`;

const StyledInputIcon = styled.img`
  position: absolute;
  top: 25px;
`;

const StyledFormText = styled.p`
  border-bottom: 2px solid #464646;
  color: #d6d6da;
  padding-top: 11px;
  padding-bottom: 11px;
  font-family: Open-Sans, sans-serif;
  font-size: 18px;
  font-style: normal;
  line-height: 25px;
  letter-spacing: 0em;
  text-align: left;
  margin-bottom: 0px;
`;

const StyledIcon = styled.img`
  color: white;
  height: 10px;
`;

const StyledButtonWrapper = styled.div`
  display: flex;
  width: 430px;
  justify-content: space-between;
  margin: 0 auto;
  padding-bottom: 115px;
`;

export default SellNFTPage;
