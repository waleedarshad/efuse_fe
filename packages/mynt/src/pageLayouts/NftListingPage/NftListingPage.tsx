import React from "react";
import { NextSeo } from "next-seo";
import styled from "styled-components";
import { NftQuery } from "../../data/NftTypes";
import NavigationWrapper from "../NavigationWrapper/NavigationWrapper";
import NftIframe from "../../components/NftIframe/NftIframe";

const NftListingPage = ({ query }: { query: NftQuery }) => {
  const { address, token } = query;
  return (
    <>
      <NextSeo
        title="Gaming NFTs | Mynt"
        description="Discover esports and gaming NFTs on Mynt. Don't miss your favorite creator's NFTs."
        openGraph={{
          type: "website",
          url: `https://mynt.gg/listing/${address}/${token}`,
          title: "Gaming NFTs | Mynt",
          images: [{ url: "https://cdn.mynt.gg/images/mynt-og-image.jpg" }],
          site_name: "eFuse.gg",
          description: "Discover esports and gaming NFTs on Mynt. Don't miss your favorite creator's NFTs."
        }}
        twitter={{
          handle: "@eFuseOfficial",
          site: `https://mynt.gg/listing/${address}/${token}`,
          cardType: "summary_large_image"
        }}
      />
      <NavigationWrapper navHeight={100}>
        <IframeWrapper>
          <NftIframe address={address} token={token} />
        </IframeWrapper>
      </NavigationWrapper>
    </>
  );
};

const IframeWrapper = styled.div`
  height: 100%;
`;

export default NftListingPage;
