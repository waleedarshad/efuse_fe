import { useState } from "react";

import { INft, MediaTypes } from "../../data/NftTypes";
import { IGames } from "../../data/IGame";
import NavigationWrapper from "../NavigationWrapper/NavigationWrapper";
import EFMyntSection from "../../components/EFMyntSection/EFMyntSection";
import EFGamesList from "../../components/EFGamesList/EFGamesList";
import NftList from "../../components/EFMyntSection/NftList/NftList";

const CollectionsPage = ({ games, nfts }: { games: IGames; nfts: [INft] }) => {
  const defaultGameFilter: string | null = null;
  const [gameFilter, setGameFilter] = useState(defaultGameFilter);

  const onGameClick = (slug: string) => {
    if (gameFilter === slug) {
      setGameFilter(null);
    } else {
      // @ts-ignore
      setGameFilter(slug);
    }
  };

  const filterByMediaTypeAndGame = (nft: any, byMediaType: any) => {
    return nft.traits.mediaType === byMediaType && (!gameFilter || nft.traits.game === gameFilter);
  };

  const filterNfts = (byMediaType: MediaTypes) => {
    return nfts?.filter((nft: INft) => filterByMediaTypeAndGame(nft, byMediaType) && nft.isListed) || [];
  };

  return (
    <>
      <NavigationWrapper navHeight={100}>
        <EFMyntSection title="games">
          <EFGamesList onGameClick={onGameClick} activeGame={gameFilter} games={games} />
        </EFMyntSection>
        <EFMyntSection title="clips">
          <NftList nftList={filterNfts(MediaTypes.GIF)} isOwnersListing={false} />
        </EFMyntSection>
        <EFMyntSection title="art">
          <NftList nftList={filterNfts(MediaTypes.IMAGE)} isOwnersListing={false} />
        </EFMyntSection>
        <EFMyntSection title="videos">
          <NftList nftList={filterNfts(MediaTypes.VIDEO)} isOwnersListing={false} />
        </EFMyntSection>
      </NavigationWrapper>
    </>
  );
};

export default CollectionsPage;
