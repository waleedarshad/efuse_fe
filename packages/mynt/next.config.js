module.exports = {
  future: {
    webpack5: true
  },
  env: {
    NEXT_PUBLIC_GRAPHQL_URL: process.env.NEXT_PUBLIC_GRAPHQL_URL,
    NEXT_PUBLIC_BASE_URL: process.env.NEXT_PUBLIC_BASE_URL,
    AWS_DEFAULT_REGION: process.env.AWS_DEFAULT_REGION,
    AWS_S3_ACCESS_ID: process.env.AWS_S3_ACCESS_ID,
    AWS_S3_BUCKET: process.env.AWS_S3_BUCKET,
    AWS_S3_REGION: process.env.AWS_S3_REGION,
    AWS_S3_SECRET_KEY: process.env.AWS_S3_SECRET_KEY,
    AUTH0_BASE_URL: process.env.AUTH0_BASE_URL,
    PAYPAL_CLIENT_ID: process.env.PAYPAL_CLIENT_ID || "test"
  },
  publicRuntimeConfig: {
    AUTH0_AUDIENCE: process.env.AUTH0_AUDIENCE
  }
};
