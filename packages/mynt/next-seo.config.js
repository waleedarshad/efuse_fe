const eFuseSEO = {
  title: "Mynt | Gaming Moments",
  description: "The Marketplace for Gaming Clips, Art, & Memorable Moments.",
  openGraph: {
    type: "website",
    locale: "en_US",
    url: "https://mynt.gg/",
    site_name: "mynt.gg",
    images: [{ url: "https://cdn.mynt.gg/images/mynt-og-image.jpg" }],
    title: "Mynt | Gaming Moments",
    description: "The Marketplace for Gaming Clips, Art, & Memorable Moments."
  },
  twitter: {
    handle: "@efuseofficial",
    site: "@site",
    cardType: "summary_large_image",
    description: "The Marketplace for Gaming Clips, Art, & Memorable Moments."
  }
};

export default eFuseSEO;
