import { ApolloError, useLazyQuery } from "@apollo/client";
import { useEffect } from "react";
import { PermissionActionEnum } from "../enums";
import { ENTITY_HAS_PERMISSION } from "../graphql/permissions/PermissionQuery";
import { PermissionEntityBody, ResourceOwnerBody } from "../interfaces";
import PermissionResourceType from "../types/permission-resource.type";

export interface IUsePermission {
  hasPermission: boolean;
  loading: boolean;
  hasError: ApolloError;
}

/**
 * This should be used client side when doing a check for an entities permissions
 *
 * If this is updated then "PermissionHelper.tsx" may need updated.
 *
 * @param action - In what way is the resource being used.
 * @param resource - What resource is being accessed. (This could be to change an organizations name.)
 * @param entityBody - ID of entity and what the enity. (Could be ID of a user). If not set then will use authenticated user.
 * @param resourceOwnerBody - Id of resource owner. This could be an Organization ID. Not required for every resource.
 */
const usePermission = (
  action: PermissionActionEnum,
  resource: PermissionResourceType,
  permissionEntityBody?: PermissionEntityBody,
  resourceOwnerBody?: ResourceOwnerBody
): IUsePermission => {
  const [entityHasPermission, { loading, error, data }] = useLazyQuery(ENTITY_HAS_PERMISSION, {
    fetchPolicy: "cache-first"
  });

  useEffect(() => {
    entityHasPermission({
      variables: {
        params: { action, resource, resourceOwnerData: resourceOwnerBody, entityData: permissionEntityBody }
      }
    });
  }, [permissionEntityBody, action, resource, resourceOwnerBody]);

  return { hasPermission: data?.entityHasPermission, loading, hasError: error } as IUsePermission;
};

export default usePermission;
