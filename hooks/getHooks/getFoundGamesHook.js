import getGamesHook from "./getGamesHook";

const getFoundGamesHook = gameIds => {
  const retrievedGames = getGamesHook();

  const gameIdsList = [gameIds].flat();

  return retrievedGames.filter(game => gameIdsList.includes(game._id));
};

export default getFoundGamesHook;
