import { useQuery } from "@apollo/client";
import Error from "../../components/Error/Error";
import {
  GET_AIMLAB_LEADERBOARD_FOR_USER,
  GET_LEAGUE_OF_LEGENDS_LEADERBOARD_FOR_USER,
  GET_VALORANT_LEADERBOARD_FOR_USER
} from "../../graphql/UserQuery";

const getUserLeaderBoardDataHook = (game, userId) => {
  const LeaderboardEnum = Object.freeze({
    aimlab: "aim-lab",
    leagueOfLegends: "league-of-legends",
    valorant: "valorant"
  });
  const getQueryData = () => {
    if (game === LeaderboardEnum.aimlab) {
      return data?.getUserById?.pipelineLeaderboardRanks?.aimlabLeaderboardRank;
    }
    if (game === LeaderboardEnum.leagueOfLegends) {
      return data?.getUserById?.pipelineLeaderboardRanks?.leagueOfLegendsLeaderboardRank;
    }
    return data?.getUserById?.pipelineLeaderboardRanks?.valorantLeaderboardRank;
  };
  const getQuery = () => {
    let query = GET_VALORANT_LEADERBOARD_FOR_USER;
    if (game === LeaderboardEnum.aimlab) {
      query = GET_AIMLAB_LEADERBOARD_FOR_USER;
    } else if (game === LeaderboardEnum.leagueOfLegends) {
      query = GET_LEAGUE_OF_LEGENDS_LEADERBOARD_FOR_USER;
    }
    return query;
  };
  const query = getQuery();
  const { loading, error, data } = useQuery(query, {
    variables: { id: userId }
  });
  if (loading) return [];
  if (error) return <Error />;
  const leaderBoardData = getQueryData();
  return leaderBoardData;
};

export default getUserLeaderBoardDataHook;
