import { useQuery } from "@apollo/client";

import { GET_GAMES } from "../../graphql/GameQuery";
import Error from "../../components/Error/Error";

const getGamesHook = () => {
  const { loading, error, data } = useQuery(GET_GAMES);
  if (loading) return [];
  if (error) return <Error />;
  const gamesList = data.getGames;
  return gamesList;
};

export default getGamesHook;
