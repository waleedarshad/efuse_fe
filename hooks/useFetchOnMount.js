import { useEffect } from "react";
import { useDispatch } from "react-redux";

const useFetchOnMount = fetchAction => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchAction());
  }, []);
};

export default useFetchOnMount;
