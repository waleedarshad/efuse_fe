import { ApolloError, useLazyQuery } from "@apollo/client";
import { useEffect } from "react";
import { ENTITY_HAS_ROLE } from "../graphql/permissions/PermissionQuery";
import { HasRoleInput } from "../interfaces";

export interface IUseRole {
  hasRole: boolean;
  loading: boolean;
  hasError: ApolloError;
}

/**
 * This should be used client side when doing a check for an entities roles
 *
 * If this is updated then "PermissionHelper.tsx" may need updated.
 *
 * @param hasRoleInput Many of the variables in this are optional.
 * If entityId is not set then the authorized user will be used.
 * For roleValues and roleIds one of them needs to be set, but only one of them needs to be. If both are set then roleIds will be used.
 */
const useRole = (hasRoleInput: HasRoleInput): IUseRole => {
  const { entityId, roleIds, roleValues } = hasRoleInput;

  const [entityHasRole, { loading, error, data }] = useLazyQuery(ENTITY_HAS_ROLE, {
    fetchPolicy: "cache-first"
  });

  useEffect(() => {
    entityHasRole({
      variables: {
        params: { entityId, roleIds, roleValues }
      }
    });
  }, []);

  return { hasRole: data?.entityHasRole, loading, hasError: error } as IUseRole;
};

export default useRole;
