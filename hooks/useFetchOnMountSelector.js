import { useSelector } from "react-redux";
import useFetchOnMount from "./useFetchOnMount";

const useFetchOnMountSelector = (fetchAction, fetchSelector) => {
  useFetchOnMount(() => fetchAction());
  const selectorData = useSelector(fetchSelector);

  return selectorData;
};

export default useFetchOnMountSelector;
