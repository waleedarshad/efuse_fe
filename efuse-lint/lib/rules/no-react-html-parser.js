/**
 * @fileoverview Do not use ReactHtmlParser
 * @author Shawn Pavel
 */

// "use strict";

//------------------------------------------------------------------------------
// Rule Definition
//------------------------------------------------------------------------------
const REACT_HTML_PARSER_ERROR = "Do not use ReactHtmlParser. Instead use the EFHtmlParser component";

module.exports = {
  meta: {
    docs: {
      description: "Do not use ReactHtmlParser",
      category: "rules",
      recommended: false
    },
    fixable: null, // or "code" or "whitespace"
    schema: [] // no options
  },

  create: context => {
    return {
      CallExpression: node => {
        const { callee } = node;

        if (callee.type === "Identifier" && callee.name === "ReactHtmlParser") {
          context.report({ node, message: REACT_HTML_PARSER_ERROR });
        }
      }
    };
  }
};
