#!/bin/bash

SERVICE=fe
CLUSTER=production
AWS_REGION=${AWS_REGION:-us-east-2}
SERVICE_JSON="$(aws ecs describe-services --cluster=${CLUSTER} --region=${AWS_REGION} --services=${SERVICE} | jq '.services[0]')"

SERVICE_ACTIVE=$(echo "$SERVICE_JSON" | jq '.status' -r)
SERVICE_TASK_DEF=$(echo $SERVICE_JSON | jq '.taskDefinition' -r)

SERVICE_TASK_DEF_GITSHA=$(aws ecs describe-task-definition --task-definition "${SERVICE_TASK_DEF}" --region ${AWS_REGION} | jq "(.taskDefinition.containerDefinitions[] | select(.name == \"${SERVICE}\")).image" -r | awk -F: '{print $2}')

GIT_COMMIT_DESC=$(git show --quiet $SERVICE_TASK_DEF_GITSHA)
# echo $SERVICE_JSON | jq '.'

# SERVICE_TASKS=$(aws ecs list-tasks --region=${AWS_REGION} --cluster=${CLUSTER} --service=${SERVICE} | jq '.taskArns[]' -r )
# SERVICE_TASK_STATUS=$(echo $SERVICE_TASKS | xargs aws ecs  describe-tasks --region=us-east-2 --cluster=production --tasks)

cat << EOF
-> General Info <-
Service: ${SERVICE}
Region: ${AWS_REGION}
Status: ${SERVICE_ACTIVE}
Task Definition: ${SERVICE_TASK_DEF}

-> ECS Tasks <-
$(ecs-cli ps --cluster production --region=us-east-2 | grep -Ev 'datadog|log_router')

-> Git Commit <-
${GIT_COMMIT_DESC}
EOF

