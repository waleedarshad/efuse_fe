#!/bin/bash


if [ -z "${1}" ]; then
    echo "Roll back an FE ECS deployment to a specific version"
    echo
    echo "Usage: ecs_rollback.sh GIT_SHA1"
    exit 1
fi

REGIONS='us-east-2 ap-south-1 us-west-2 eu-west-1'

ENVIRONMENT=production

GIT_SHA1=${1}
TASK_FAMILY=fe-${ENVIRONMENT}
ECR_IMAGE=efuse_fe
CLUSTER=${ENVIRONMENT}
SERVICE=fe

ECR_REPO="846560168436.dkr.ecr.us-east-1.amazonaws.com/${ECR_IMAGE}"
NEW_IMAGE="${ECR_REPO}:${GIT_SHA1}"

echo "About to update the ${ENVIRONMENT} ${SERVICE} to git hash ${GIT_SHA1} in the following regions: ${REGIONS}"
read -p 'Are you sure? [y/n]? ' yesno
 
if [[ "${yesno}" != "y" ]]; then
  echo "Exiting"
  echo 1
fi

for region in $REGIONS; do
  echo "[${region}] Retrieving task definition"
  TASK_DEFINITION=$(aws ecs describe-task-definition --task-definition "$TASK_FAMILY" --region $region)
  NEW_TASK_DEFINTIION=$(echo $TASK_DEFINITION | jq --arg IMAGE "$NEW_IMAGE" '.taskDefinition | .containerDefinitions |= map(if .name == "fe" then .image = $IMAGE else . end ) | del(.taskDefinitionArn) | del(.revision) | del(.status) | del(.requiresAttributes) | del(.compatibilities)')

  echo "[${region}] Registering new task definition"
  NEW_TASK_ARN=$(aws ecs register-task-definition --region ${region} --cli-input-json "$NEW_TASK_DEFINTIION" | jq -r .taskDefinition.taskDefinitionArn)
  echo "[${region}] New task definition created: ${NEW_TASK_ARN}"

  echo "[${region}] Updating service with new task definition"
  NEW_SERVICE=$(aws ecs update-service --region=${region} --cluster=${CLUSTER} --service=${SERVICE} --task-definition=${NEW_TASK_ARN})
  echo "[${region}] Service update complete!  See: https://${region}.console.aws.amazon.com/ecs/home?region=${region}#/clusters/production/services/${SERVICE}/tasks"
done





