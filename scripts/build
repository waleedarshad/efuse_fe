#!/bin/bash

set -xeuo pipefail

PROJECT_DIR=$(git rev-parse --show-toplevel)
source ${PROJECT_DIR}/scripts/constants

OPT_PUSH=false
WEBPACK_DEVTOOL=eval

while getopts ":pd" opt; do
  case ${opt} in
  p)
    OPT_PUSH=true
    ;;
  d)
    WEBPACK_DEVTOOL=
    ;;
   \?)
    echo "Usage: build [-p] [-f]"
    echo "  -p 		Push the image to ECR"
    echo "  -d 		Create production deployable build (default is development build)"
    exit 1
    ;;
  esac
done

GIT_SHA=$(git rev-parse HEAD)
if [[ $(git status --porcelain) ]]; then
  IMAGE_TAG=${GIT_SHA}-dirty
else
  IMAGE_TAG=${GIT_SHA}
fi

time docker build --build-arg DOPPLER_TOKEN=${DOPPLER_TOKEN} --build-arg WEBPACK_DEVTOOL=${WEBPACK_DEVTOOL} --build-arg GIT_SHA=${IMAGE_TAG} -f Dockerfile -t $ECR_REGISTRY/$ECR_REPOSITORY:$IMAGE_TAG .

# Login to AWS ECR
if [[ ${OPT_PUSH} == "true" ]]; then
  echo "Logging into AWS Docker Container Registry (ECR)"
  aws ecr get-login-password --region "${AWS_REGION}" |
    docker login --username AWS --password-stdin ${ECR_REGISTRY}

  # Push to the production ECR repo
  docker push $ECR_REGISTRY/$ECR_REPOSITORY:${IMAGE_TAG}

  # Print out message to deploy
  echo
  echo "To deploy a preview to kubernetes simply run:"
  echo "scripts/deploy staging ${IMAGE_TAG}"
fi
