FROM node:14.15.1-stretch as deps

WORKDIR /app

ADD efuse-lint ./efuse-lint
COPY .yarnrc.yml package.json yarn.lock ./
COPY .yarn ./.yarn
RUN yarn install --immutable --immutable-cache


### BUILD APP
FROM node:14.15.1-stretch AS builder
ARG WEBPACK_DEVTOOL

WORKDIR /app

COPY . .
COPY --from=deps /app/node_modules ./node_modules
RUN echo "WEBPACK_DEVTOOL=${WEBPACK_DEVTOOL}"
RUN WEBPACK_DEVTOOL=${WEBPACK_DEVTOOL} NODE_ENV=production yarn build


### RUN APP
FROM node:14.15.1-stretch AS runner
ARG GIT_SHA
ARG DOPPLER_TOKEN
ENV PORT=80

WORKDIR /app

# https://docs.doppler.com/docs/enclave-installation
RUN apt-get update && apt-get install -y apt-transport-https ca-certificates curl gnupg \
    && curl -sLf --retry 3 --tlsv1.2 --proto "=https" 'https://packages.doppler.com/public/cli/gpg.DE2A7741A397C129.key' | apt-key add - \
    && echo "deb https://packages.doppler.com/public/cli/deb/debian any-version main" | tee /etc/apt/sources.list.d/doppler-cli.list \
    && apt-get update && apt-get install doppler

RUN doppler secrets download doppler.encrypted.json -t $DOPPLER_TOKEN

COPY --from=builder /app/routes ./routes
COPY --from=builder /app/public ./public
COPY --from=builder /app/static ./static
COPY --from=builder /app/.next ./.next
COPY --from=builder /app/node_modules ./node_modules
COPY --from=builder /app/next.config.js ./
COPY --from=builder /app/package.json ./
COPY --from=builder /app/server.js ./

EXPOSE 80

CMD doppler run --fallback=doppler.encrypted.json --command="NODE_ENV=production node server.js"