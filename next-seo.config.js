import getConfig from "next/config";

const { publicRuntimeConfig } = getConfig();
const { facebookID } = publicRuntimeConfig;

export default {
  title: "Discover Esports Scholarships and Tournaments | eFuse.gg",
  description: "Find esports scholarships, join tournaments, and build your gaming portfolio on eFuse.",
  openGraph: {
    type: "website",
    locale: "en_US",
    url: "https://efuse.gg/",
    site_name: "eFuse.gg",
    images: [{ url: "https://cdn.efuse.gg/static/images/og_image_2022.jpg" }],
    title: "eFuse.gg | Where Gamers Get Discovered",
    description: "Find esports scholarships, join tournaments, and build your gaming portfolio on eFuse."
  },
  twitter: {
    handle: "@eFuse",
    site: "@site",
    cardType: "summary_large_image"
  },
  facebook: {
    appId: facebookID
  }
};
