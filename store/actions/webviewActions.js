import { SET_WEBVIEW_STATUS } from "./types";

/**
 @module webviewActions
 @category Actions
 */

export const updateWebviewStatus = newStatus => dispatch => {
  dispatch({
    type: SET_WEBVIEW_STATUS,
    payload: { status: newStatus }
  });
};
