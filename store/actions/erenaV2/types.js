// eslint-disable-next-line import/prefer-default-export
export const ERENA_V2 = {
  GET_TOURNAMENTS: {
    SUCCESS: "GET_ERENA_TOURNAMENTS_SUCCESS",
    FAILURE: "GET_ERENA_TOURNAMENTS_FAILURE"
  }
};
