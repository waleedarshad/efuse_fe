import axios from "axios";
import { setErrors } from "../errorActions";
import {
  SET_ERENA_EVENT,
  SET_ERENA_EVENT_USER_ROLES,
  SET_TEAMS,
  TOGGLE_MOCK_EXTERNAL_ERENA_EVENT,
  SET_SUBMITTING_NEW_EVENT_STATE,
  UPDATE_TEAM,
  CREATE_TEAM,
  DELETE_TEAM,
  SET_PLAYERS,
  CREATE_PLAYER,
  DELETE_PLAYER,
  UPDATE_PLAYER,
  SET_ERENA_SCORES,
  CREATE_SCORE,
  UPDATE_SCORE,
  SET_POINT_RACE_TEAMS,
  SET_POINT_RACE_PLAYERS,
  SET_BRACKET,
  SET_MATCHES,
  UPDATE_MATCH,
  SET_STAFFS,
  DELETE_STAFF,
  CREATE_STAFF
} from "../types";
import { toggleLoader } from "../loaderActions";
import { sendNotification } from "../../../helpers/FlashHelper";
import { initializeApollo } from "../../../config/apolloClient";
import { ERENA_V2 } from "./types";
import { GET_ERENA_TOURNAMENTS, GET_ERENA_TOURNAMENT } from "../../../graphql/ERenaTournamentQuery";

const apolloClient = initializeApollo();
/**
 @module erenaActions
 @category Actions
 */

// ========================
// ||     TOURNAMENT     ||
// ========================

export const getERenaTournament = tournamentId => dispatch => {
  apolloClient
    .query({
      query: GET_ERENA_TOURNAMENT,
      variables: { tournamentIdOrSlug: tournamentId }
    })
    .then(data => {
      if (data?.data?.getERenaTournament) {
        dispatch(setTournamentSSR(data?.data?.getERenaTournament));
      }
    });
};
export const getScheduledAndActiveErenaTournaments = page => dispatch => {
  const maxNumItemsPerPage = 12;
  const currentTime = Math.round(new Date().getTime() / 1000);

  apolloClient
    .query({
      query: GET_ERENA_TOURNAMENTS,
      variables: {
        page,
        limit: maxNumItemsPerPage,
        sortBy: "startDatetime",
        sortDirection: 1,
        status: ["Scheduled", "Active"],
        endDateMin: currentTime
      }
    })
    .then(response => {
      const { docs, ...rest } = response.data.listERenaTournaments;
      dispatch({
        type: ERENA_V2.GET_TOURNAMENTS.SUCCESS,
        page,
        payload: response.data.listERenaTournaments.docs,
        pagination: rest
      });
    })
    .catch(error => {
      dispatch({ type: ERENA_V2.GET_TOURNAMENTS.FAILURE, error: error.message });
    });
};

export const createErenaEvent = (tournament, router) => dispatch => {
  dispatch(toggleLoader(true));
  dispatch({
    type: SET_SUBMITTING_NEW_EVENT_STATE,
    payload: true
  });

  axios
    .post("/erena/v2/tournament", tournament)
    .then(response => {
      sendNotification("Tournament was successfully created.", "success", "Success");
      dispatch(toggleLoader(false));
      router.push(`/erenav2/${response.data.slug}`);
    })
    .catch(error => {
      dispatch(setErrors(error.response.data));
      dispatch(toggleLoader(false));
      dispatch({
        type: SET_SUBMITTING_NEW_EVENT_STATE,
        payload: false
      });
    });
};

export const setTournamentSSR = tournament => dispatch => {
  dispatch({
    type: SET_ERENA_EVENT,
    payload: tournament
  });
};

export const setUserRolesSSR = userRoles => dispatch => {
  dispatch({
    type: SET_ERENA_EVENT_USER_ROLES,
    payload: userRoles
  });
};

export const updateTournament = (tournament, updates) => dispatch => {
  axios
    .patch(`/erena/v2/tournament/${tournament.tournamentId}`, updates)
    .then(response => {
      sendNotification("Tournament was successfully updated.", "success", "Success");
      dispatch({
        type: SET_ERENA_EVENT,
        payload: response.data
      });
    })
    .catch(error => {
      dispatch(setErrors(error));
    });
};

export const editErenaAd = (tournament, formData, index) => dispatch => {
  axios
    .put(`/erena/v2/ad/${index + 1}/tournament/${tournament.tournamentId}`, formData)
    .then(response => {
      sendNotification("Ad successfully updated.", "success", "Success");
      dispatch({
        type: SET_ERENA_EVENT,
        payload: response.data
      });
    })
    .catch(error => {
      dispatch(setErrors(error));
    });
};

export const removeErenaAd = (tournament, index) => dispatch => {
  axios
    .delete(`/erena/v2/ad/${index}/tournament/${tournament.tournamentId}`)
    .then(response => {
      sendNotification("Ad successfully removed.", "success", "Success");
      dispatch({
        type: SET_ERENA_EVENT,
        payload: response.data
      });
    })
    .catch(error => {
      dispatch(setErrors(error));
    });
};

export const toggleMockExternalErenaEvent = () => dispatch => {
  dispatch({
    type: TOGGLE_MOCK_EXTERNAL_ERENA_EVENT
  });
};

// ========================
// ||       TEAMS        ||
// ========================
export const getSSRTeams = teams => dispatch => {
  dispatch({
    type: SET_TEAMS,
    payload: teams
  });
};

export const getTeams = tournamentId => dispatch => {
  axios
    .get(`/erena/v2/tournament/${tournamentId}/teams?includePlayers=true&includeScores=true`)
    .then(response => {
      dispatch({
        type: SET_TEAMS,
        payload: response.data
      });
    })
    .catch(error => {
      dispatch(setErrors(error));
    });
};

export const getTeamsPublic = tournamentId => dispatch => {
  axios
    .get(`/erena/v2/public/tournament/${tournamentId}/teams?includePlayers=true&includeScores=true`)
    .then(response => {
      dispatch({
        type: SET_TEAMS,
        payload: response.data
      });
    })
    .catch(error => {
      dispatch(setErrors(error));
    });
};

export const createTeam = tournamentId => dispatch => {
  const data = {
    name: "Team Name",
    isActive: true,
    type: "EFUSE"
  };
  axios
    .post(`/erena/v2/tournament/${tournamentId}/team`, data)
    .then(response => {
      dispatch({
        type: CREATE_TEAM,
        payload: response.data
      });
    })
    .catch(error => {
      dispatch(setErrors(error));
    });
};

export const updateTeam = (teamId, data) => dispatch => {
  axios
    .patch(`/erena/v2/team/${teamId}`, data)
    .then(response => {
      dispatch({
        type: UPDATE_TEAM,
        payload: response.data
      });
    })
    .catch(error => {
      dispatch(setErrors(error));
    });
};

export const deleteTeam = teamId => dispatch => {
  axios
    .delete(`/erena/v2/team/${teamId}`)
    .then(response => {
      dispatch({
        type: DELETE_TEAM,
        payload: response.data._id
      });
    })
    .catch(error => {
      dispatch(setErrors(error));
    });
};

// ========================
// ||      PLAYERS       ||
// ========================/erena/v2/tournament/:tournamentIdOrSlug/players?team=:teamId&includeScores=true
export const getAllPlayers = tournamentId => dispatch => {
  axios
    .get(`/erena/v2/tournament/${tournamentId}/players?includeScores=true`)
    .then(response => {
      dispatch({
        type: SET_PLAYERS,
        payload: response.data
      });
    })
    .catch(error => {
      dispatch(setErrors(error));
    });
};

export const createPlayer = (teamId, data) => dispatch => {
  axios
    .post(`/erena/v2/team/${teamId}/player`, data)
    .then(response => {
      dispatch({
        type: CREATE_PLAYER,
        payload: response.data
      });
      sendNotification("Player added successfully", "success", "Success");
    })
    .catch(error => {
      dispatch(setErrors(error));
    });
};

export const updatePlayer = (playerId, data) => dispatch => {
  axios
    .patch(`/erena/v2/player/${playerId}`, data)
    .then(response => {
      dispatch({
        type: UPDATE_PLAYER,
        payload: response.data
      });
    })
    .catch(error => {
      dispatch(setErrors(error));
    });
};

export const deletePlayer = playerId => dispatch => {
  axios
    .delete(`/erena/v2/player/${playerId}`)
    .then(response => {
      dispatch({
        type: DELETE_PLAYER,
        payload: response.data
      });
    })
    .catch(error => {
      dispatch(setErrors(error));
    });
};

// ========================
// ||       SCORES       ||
// ========================
export const getAllScores = tournamentId => dispatch => {
  axios
    .get(`/erena/v2/tournament/${tournamentId}/scores`)
    .then(response => {
      dispatch({
        type: SET_ERENA_SCORES,
        payload: response.data
      });
    })
    .catch(error => {
      dispatch(setErrors(error));
    });
};

export const getAllScoresPublic = tournamentId => dispatch => {
  axios
    .get(`/erena/v2/public/tournament/${tournamentId}/scores`)
    .then(response => {
      dispatch({
        type: SET_ERENA_SCORES,
        payload: response.data
      });
    })
    .catch(error => {
      dispatch(setErrors(error));
    });
};

// If submitting a score for a tournament with a bracket you must specify the match id in data.
// ownerKind can be "team" or "player"
export const createScore = (tournamentId, data) => dispatch => {
  axios
    .post(`/erena/v2/tournament/${tournamentId}/score`, data)
    .then(response => {
      dispatch({
        type: CREATE_SCORE,
        payload: response?.data
      });
    })
    .catch(error => {
      dispatch(setErrors(error));
    });
};

// Only the score value can be updated once an eRena Score is created.
export const updateScore = (scoreId, data) => dispatch => {
  axios
    .patch(`/erena/v2/score/${scoreId}`, data)
    .then(response => {
      dispatch({
        type: UPDATE_SCORE,
        payload: response?.data
      });
    })
    .catch(error => {
      dispatch(setErrors(error));
    });
};

// ========================
// ||     POINT RACE     ||
// ========================
export const getPublicTeamsPointRace = tournamentId => dispatch => {
  axios
    .get(`/erena/v2/public/leaderboard/${tournamentId}/teams?namespace=EFUSE`)
    .then(response => {
      dispatch({
        type: SET_POINT_RACE_TEAMS,
        payload: response.data
      });
    })
    .catch(error => {
      dispatch(setErrors(error));
    });
};

export const getPublicPlayersPointRace = tournamentId => dispatch => {
  axios
    .get(`/erena/v2/public/leaderboard/${tournamentId}/players?namespace=EFUSE`)
    .then(response => {
      dispatch({
        type: SET_POINT_RACE_PLAYERS,
        payload: response.data
      });
    })
    .catch(error => {
      dispatch(setErrors(error));
    });
};

// ========================
// ||      BRACKET       ||
// ========================
export const getBracket = tournamentId => dispatch => {
  axios
    .get(
      `/erena/v2/tournament/${tournamentId}/bracket?includeRounds=true&includeMatches=true&includeTeams=true&includePlayers=true&includeScores=true`
    )
    .then(response => {
      dispatch({
        type: SET_BRACKET,
        payload: response.data
      });
    })
    .catch(error => {
      dispatch(setErrors(error));
    });
};

export const getBracketPublic = tournamentId => dispatch => {
  axios
    .get(
      `/erena/v2/public/tournament/${tournamentId}/bracket?includeRounds=true&includeMatches=true&includeTeams=true&includePlayers=true&includeScores=true`
    )
    .then(response => {
      dispatch({
        type: SET_BRACKET,
        payload: response.data
      });
    })
    .catch(error => {
      dispatch(setErrors(error));
    });
};

// ========================
// ||      MATCHES       ||
// ========================
export const getAllMatches = tournamentId => dispatch => {
  axios
    .get(`/erena/v2/tournament/${tournamentId}/matches`)
    .then(response => {
      dispatch({
        type: SET_MATCHES,
        payload: response?.data
      });
    })
    .catch(error => {
      dispatch(setErrors(error));
    });
};

export const getAllMatchesPublic = tournamentId => dispatch => {
  axios
    .get(`/erena/v2/public/tournament/${tournamentId}/matches`)
    .then(response => {
      dispatch({
        type: SET_MATCHES,
        payload: response?.data
      });
    })
    .catch(error => {
      dispatch(setErrors(error));
    });
};

export const updateMatch = (matchId, data) => dispatch => {
  axios
    .patch(`/erena/v2/match/${matchId}`, data)
    .then(response => {
      dispatch({
        type: UPDATE_MATCH,
        payload: response?.data?.currentMatch
      });
      // check if next match is returned (update winner) and update that match as well
      if (response?.data?.nextMatch) {
        dispatch({
          type: UPDATE_MATCH,
          payload: response?.data?.nextMatch
        });
      }
    })
    .catch(error => {
      dispatch(setErrors(error));
    });
};

export const updateMatchScreenshot = (matchId, data) => dispatch => {
  axios
    .patch(`/erena/v2/match/${matchId}/screenshots`, data)
    .then(response => {
      dispatch({
        type: UPDATE_MATCH,
        payload: response?.data
      });
      sendNotification("Successfully uploaded an image", "success", "Success");
    })
    .catch(error => {
      dispatch(setErrors(error));
    });
};

// ========================
// ||      STAFF       ||
// ========================
export const getAllStaffSSR = staff => dispatch => {
  dispatch({
    type: SET_STAFFS,
    payload: staff
  });
};

export const getAllStaff = tournamentIdOrSlug => dispatch => {
  axios
    .get(`/erena/v2/tournament/${tournamentIdOrSlug}/staff`)
    .then(response => {
      dispatch({
        type: SET_STAFFS,
        payload: response?.data
      });
    })
    .catch(error => {
      dispatch(setErrors(error));
    });
};

export const removeStaff = staffId => dispatch => {
  axios
    .delete(`/erena/v2/staff/${staffId}`)
    .then(response => {
      dispatch({
        type: DELETE_STAFF,
        payload: response.data._id
      });
      sendNotification("Staff member removed successfully", "success");
    })
    .catch(error => {
      dispatch(setErrors(error));
    });
};

export const addStaff = (tournamentIdOrSlug, data) => dispatch => {
  axios
    .post(`/erena/v2/tournament/${tournamentIdOrSlug}/staff`, data)
    .then(response => {
      dispatch({
        type: CREATE_STAFF,
        payload: response.data
      });
      sendNotification("New staff member added successfully", "success");
    })
    .catch(error => {
      dispatch(setErrors(error));
    });
};
