import { TOGGLE_HEADER_MENU, TOGGLE_HEADER_CREATE_DROPDOWN } from "./types";

export const toggleHeaderMenu = () => dispatch => {
  dispatch({
    type: TOGGLE_HEADER_MENU
  });
};

export const toggleCreateDropdown = () => dispatch => {
  dispatch({
    type: TOGGLE_HEADER_CREATE_DROPDOWN
  });
};
