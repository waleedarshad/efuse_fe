import Router from "next/router";
import { initializeApollo } from "../../../config/apolloClient";
import { SET_EVENT_TEAMS } from "../../../components/Leagues/Pages/ManageTeamsPage/queries";
import { LeagueBracketType, LeagueEvent, LeagueEventState } from "../../../graphql/interface/League";
import { sendNotification } from "../../../helpers/FlashHelper";
import { SET_EVENT_STATE } from "../../../components/Leagues/Pages/ManagePoolsPage/eventPoolsPageQueries";

const apolloClient = initializeApollo();

const updateEventWithTeams = (event: LeagueEvent, newState: LeagueEventState, addedTeamsIds: string[]) => {
  return apolloClient.mutate({
    mutation: SET_EVENT_TEAMS,
    variables: { eventId: event._id, event: { teams: addedTeamsIds, state: newState } }
  });
};

// eslint-disable-next-line import/prefer-default-export
export const confirmTeams = (event: LeagueEvent, addedTeamsIds: string[]) => {
  if (event.bracketType === LeagueBracketType.ROUND_ROBIN) {
    Promise.all([
      updateEventWithTeams(
        event,
        event.pools.length === 1 ? LeagueEventState.PENDING : LeagueEventState.PENDING_POOLS,
        addedTeamsIds
      )
    ])
      .then(async () => {
        if (event.pools.length === 1) {
          await Router.push(`/leagues/event/${event._id}?tab=bracket`);
        } else {
          await Router.push(`/leagues/event/${event._id}?tab=pools`);
        }
      })
      .catch(() => {
        sendNotification("Failed to confirm teams", "danger", "Failure");
      });
  } else if (event.bracketType === LeagueBracketType.SINGLE_ELIM) {
    Promise.all([updateEventWithTeams(event, LeagueEventState.PENDING, addedTeamsIds)])
      .then(async () => {
        await Router.push(`/leagues/event/${event._id}?tab=bracket`);
      })
      .catch(() => {
        sendNotification("Failed to confirm teams", "danger", "Failure");
      });
  }
};

// eslint-disable-next-line import/prefer-default-export
export const confirmPools = async (eventId: string) => {
  await apolloClient
    .mutate({
      mutation: SET_EVENT_STATE,
      variables: {
        eventId,
        event: {
          state: LeagueEventState.PENDING
        }
      }
    })
    .then(async () => {
      sendNotification("Pools Confirmed!", "success", "Success");
      await Router.push(`/leagues/event/${eventId}?tab=bracket`);
    });
};
