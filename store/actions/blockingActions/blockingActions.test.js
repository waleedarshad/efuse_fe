import Router from "next/router";
import { blockUser } from "./blockingActions";
import { getMockStore } from "../../../common/testUtils";
import { sendNotification } from "../../../helpers/FlashHelper";
import { BLOCK_USER, GET_BLOCKED_USERS } from "../../../graphql/BlockedUsersQuery";
import { initializeApollo } from "../../../config/apolloClient";

jest.mock("next/router");
jest.mock("../../../helpers/FlashHelper");
jest.mock("../../../config/apolloClient", () => {
  const mockClient = {
    mutate: jest.fn()
  };
  return { initializeApollo: () => mockClient };
});

describe("blockingActions", () => {
  let mockStore;
  let mockApolloClient;

  const userId = "1";
  const username = "yasi";

  beforeEach(() => {
    mockStore = getMockStore();
    mockApolloClient = initializeApollo();
    mockApolloClient.mutate.mockResolvedValue();
  });

  describe("block user", () => {
    it("makes the correct call to the backend", async () => {
      const expectedCall = {
        mutation: BLOCK_USER,
        variables: { blockeeId: userId },
        refetchQueries: [{ query: GET_BLOCKED_USERS }]
      };
      await mockStore.dispatch(blockUser(userId, username));

      expect(mockApolloClient.mutate).toHaveBeenCalledWith(expectedCall);
    });

    describe("when the call comes back successfully", () => {
      beforeEach(() => {
        mockApolloClient.mutate.mockResolvedValue();
      });

      it("displays a success message", async () => {
        await mockStore.dispatch(blockUser(userId, username));

        expect(sendNotification).toHaveBeenCalledWith(`Blocked ${username}`, "success", "Success");
      });

      it("redirects back to the featured lounge", async () => {
        await mockStore.dispatch(blockUser(userId, username));

        expect(Router.push).toHaveBeenCalledWith("/lounge/featured");
      });
    });

    describe("when the call comes back with a failure", () => {
      beforeEach(() => {
        mockApolloClient.mutate.mockRejectedValue({ response: { data: "error" } });
      });

      it("dispatches setError with the error message", async () => {
        await mockStore.dispatch(blockUser(userId, username));
        expect(sendNotification).toHaveBeenCalledWith(expect.anything(), "danger", "Error");
      });
    });
  });
});
