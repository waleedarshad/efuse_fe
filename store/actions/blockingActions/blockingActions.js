import Router from "next/router";
import { setErrors } from "../errorActions";
import { sendNotification } from "../../../helpers/FlashHelper";
import { initializeApollo } from "../../../config/apolloClient";
import { BLOCK_USER, GET_BLOCKED_USERS } from "../../../graphql/BlockedUsersQuery";

const apolloClient = initializeApollo();

// eslint-disable-next-line import/prefer-default-export
export const blockUser = (userId, username) => () => {
  return apolloClient
    .mutate({
      mutation: BLOCK_USER,
      variables: { blockeeId: userId },
      refetchQueries: [{ query: GET_BLOCKED_USERS }]
    })
    .then(() => {
      sendNotification(`Blocked ${username}`, "success", "Success");
      return Router.push("/lounge/featured");
    })
    .catch(error => {
      setErrors(error.response ? error.response.data : error);
    });
};
