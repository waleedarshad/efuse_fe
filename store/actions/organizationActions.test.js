import mockAxios from "axios";
import { getMockStore } from "../../common/testUtils";
import {
  getOrganizationBySlug,
  getOrganizationMembers,
  getOrganizationRequests,
  getOrganizationsNew,
  kickMember,
  updateOrganizationMemberTitle
} from "./organizationActions";
import {
  GET_ORGANIZATION_SUCCESS,
  GET_ORGANIZATION_REQUESTS_REQUEST,
  GET_ORGANIZATION_REQUESTS_FAILURE,
  GET_ORGANIZATION_REQUESTS_SUCCESS,
  GET_ORGANIZATIONS_FAILURE,
  GET_ORGANIZATIONS_SUCCESS,
  GET_ORGANIZATIONS_REQUEST,
  INVALID_REQUEST,
  TOGGLE_LOADER,
  GET_ORGANIZATION_MEMBERS_REQUEST,
  GET_ORGANIZATION_MEMBERS_SUCCESS,
  GET_ORGANIZATION_MEMBERS_FAILURE,
  KICK_ORGANIZATION_MEMBER_SUCCESS,
  KICK_ORGANIZATION_MEMBER_FAILURE,
  UPDATE_ORGANIZATION_MEMBER_TITLE_SUCCESS,
  UPDATE_ORGANIZATION_MEMBER_TITLE_FAILURE
} from "./types";
import { sendNotification } from "../../helpers/FlashHelper";

jest.mock("../../helpers/FlashHelper.js", () => ({
  sendNotification: jest.fn()
}));

describe("organizationActions", () => {
  let mockStore;

  describe("getOrganizationBySlug", () => {
    beforeEach(() => {
      mockAxios.get.mockResolvedValue({ data: { organization: "organization" } });

      mockStore = getMockStore({});
      mockStore.dispatch(getOrganizationBySlug("my-slug", true));
    });

    it("makes a network request for opportunity by slug with correct route", () => {
      expect(mockAxios.get).toHaveBeenCalledWith("/public/organizations/slug/my-slug");
    });

    it("dispatches the correct actions", () => {
      expect(mockStore.getActions()).toEqual([
        {
          type: TOGGLE_LOADER,
          show: true
        },
        {
          type: GET_ORGANIZATION_SUCCESS,
          payload: "organization"
        },
        {
          type: TOGGLE_LOADER,
          show: false
        }
      ]);
    });
  });
  describe("getOrganizationRequests", () => {
    const organizationRequesters = {
      orgRequests: {
        docs: ["requester1", "requester2"],
        totalDocs: 10,
        hasNextPage: false,
        page: 1
      }
    };

    describe("getOpportunitiesNew successful call", () => {
      beforeEach(() => {
        mockAxios.get.mockResolvedValue({ data: organizationRequesters });

        mockStore = getMockStore({});
        mockStore.dispatch(getOrganizationRequests(1, "5ee9edfe81ca3851badd5288"));
      });

      it("makes the correct network call", () => {
        expect(mockAxios.get).toHaveBeenCalledWith("/organization_requests", {
          params: { page: 1, pageSize: 8, id: "5ee9edfe81ca3851badd5288" }
        });
      });

      it("dispatches the correct actions", () => {
        expect(mockStore.getActions()).toEqual([
          { type: GET_ORGANIZATION_REQUESTS_REQUEST },
          {
            type: GET_ORGANIZATION_REQUESTS_SUCCESS,
            page: 1,
            payload: ["requester1", "requester2"],
            hasNextPage: false
          }
        ]);
      });
    });

    describe("getOpportunitiesNew failed call", () => {
      const error = {
        response: {
          status: 422
        }
      };
      beforeEach(() => {
        mockAxios.get.mockRejectedValue(error);

        mockStore = getMockStore({});
        mockStore.dispatch(getOrganizationRequests());
      });

      it("dispatches the correct actions", () => {
        expect(mockStore.getActions()).toEqual([
          { type: GET_ORGANIZATION_REQUESTS_REQUEST },
          { type: GET_ORGANIZATION_REQUESTS_FAILURE },
          { type: INVALID_REQUEST, responseCode: 422 }
        ]);
      });
    });
  });

  describe("getOrganizationsNew", () => {
    const organizationsData = {
      organizations: {
        docs: ["organization1", "organization2"],
        totalDocs: 10,
        hasNextPage: false,
        page: 1
      }
    };

    describe("getOrganizationsNew successful call", () => {
      beforeEach(() => {
        mockAxios.get.mockResolvedValue({ data: organizationsData });

        mockStore = getMockStore({});
        mockStore.dispatch(getOrganizationsNew());
      });

      it("makes the correct network call", () => {
        expect(mockAxios.get).toHaveBeenCalledWith("/public/organizations", {
          params: { page: 1, pageSize: 9, filters: {} }
        });
      });

      it("dispatches the correct actions", () => {
        expect(mockStore.getActions()).toEqual([
          { type: GET_ORGANIZATIONS_REQUEST, payload: true },
          {
            type: GET_ORGANIZATIONS_SUCCESS,
            page: 1,
            payload: ["organization1", "organization2"],
            hasNextPage: false
          }
        ]);
      });
    });

    describe("getOpportunitiesNew failed call", () => {
      const error = {
        response: {
          status: 422
        }
      };
      beforeEach(() => {
        mockAxios.get.mockRejectedValue(error);

        mockStore = getMockStore({});
        mockStore.dispatch(getOrganizationsNew(2));
      });

      it("dispatches the correct actions", () => {
        expect(mockStore.getActions()).toEqual([
          { type: GET_ORGANIZATIONS_REQUEST, payload: false },
          { type: GET_ORGANIZATIONS_FAILURE },
          { type: INVALID_REQUEST, responseCode: 422 }
        ]);
      });
    });
  });

  describe("getOrganizationMembers", () => {
    const members = {
      docs: [{ _id: "1234" }, { _id: "2345" }, { _id: "3456" }],
      hasNextPage: false
    };

    describe("getOrganizationMembers successful call", () => {
      beforeEach(() => {
        mockAxios.get.mockResolvedValue({ data: { members } });

        mockStore = getMockStore({});
        mockStore.dispatch(getOrganizationMembers("111111", 1));
      });

      it("makes the correct network call", () => {
        expect(mockAxios.get).toHaveBeenCalledWith("/organizations/members/111111", {
          params: { page: 1, pageSize: 9, limit: 10 }
        });
      });

      it("dispatches the correct actions", () => {
        expect(mockStore.getActions()).toEqual([
          { type: GET_ORGANIZATION_MEMBERS_REQUEST, payload: true },
          { type: GET_ORGANIZATION_MEMBERS_SUCCESS, page: 1, payload: members.docs, hasNextPage: members.hasNextPage }
        ]);
      });
    });

    describe("getOrganizationMembers failed call", () => {
      const error = {
        response: {
          status: 422
        }
      };

      beforeEach(() => {
        mockAxios.get.mockRejectedValue(error);

        mockStore = getMockStore({});
        mockStore.dispatch(getOrganizationMembers("11111", 1));
      });

      it("dispatches the correct actions", () => {
        expect(mockStore.getActions()).toEqual([
          { type: GET_ORGANIZATION_MEMBERS_REQUEST, payload: true },
          { type: GET_ORGANIZATION_MEMBERS_FAILURE },
          { type: INVALID_REQUEST, responseCode: 422 }
        ]);
      });
    });
  });

  describe("kickMember", () => {
    const organizationId = "1111111";
    const userId = "555555";
    const memberData = {
      flashType: "success",
      members: [{ _id: "22222" }, { _id: "33333" }],
      message: "You done did it"
    };

    describe("kickMember successful call", () => {
      beforeEach(() => {
        mockAxios.delete.mockResolvedValue({ data: memberData });

        mockStore = getMockStore({});
        mockStore.dispatch(kickMember(userId, organizationId));
      });

      it("makes the correct network call", () => {
        expect(mockAxios.delete).toHaveBeenCalledWith("organizations/kick_member/1111111?userId=555555");
      });

      it("dispatches the correct actions", () => {
        expect(mockStore.getActions()).toEqual([
          {
            type: KICK_ORGANIZATION_MEMBER_SUCCESS,
            payload: memberData.members
          }
        ]);
      });

      it("sends the correct flash notification", () => {
        expect(sendNotification).toHaveBeenCalledWith(memberData.message, memberData.flashType, "Member Kicked");
      });
    });

    describe("kickMember failed call", () => {
      const error = "This is an error.";
      beforeEach(() => {
        mockAxios.delete.mockRejectedValue(error);

        mockStore = getMockStore({});
        mockStore.dispatch(kickMember(userId, organizationId));
      });

      it("dispatches the correct actions", () => {
        expect(mockStore.getActions()).toEqual([{ type: KICK_ORGANIZATION_MEMBER_FAILURE }]);
      });
    });
  });

  describe("updateOrganizationMember", () => {
    const fieldToUpdate = { title: "queen" };
    const organizationId = "12345";
    const memberId = "11111";
    const response = {
      data: {
        member: {
          _id: "11111",
          title: "queen"
        }
      }
    };
    describe("updateOrganizationMember successful call", () => {
      beforeEach(() => {
        mockAxios.patch.mockResolvedValue(response);

        mockStore = getMockStore({});
        mockStore.dispatch(updateOrganizationMemberTitle(organizationId, memberId, fieldToUpdate));
      });

      it("makes the correct network call", () => {
        expect(mockAxios.patch).toHaveBeenCalledWith("organizations/12345/members/11111", fieldToUpdate);
      });

      it("dispatches the correct actions", () => {
        expect(mockStore.getActions()).toEqual([
          { type: UPDATE_ORGANIZATION_MEMBER_TITLE_SUCCESS, payload: response.data.member }
        ]);
      });
    });

    describe("updateMemberTitle failed call", () => {
      const error = {
        response: {
          status: 422
        }
      };

      beforeEach(() => {
        mockAxios.patch.mockRejectedValue(error);

        mockStore = getMockStore({});
        mockStore.dispatch(updateOrganizationMemberTitle(organizationId, memberId));
      });

      it("dispatches the correct actions", () => {
        expect(mockStore.getActions()).toEqual([{ type: UPDATE_ORGANIZATION_MEMBER_TITLE_FAILURE }]);
      });
    });
  });
});
