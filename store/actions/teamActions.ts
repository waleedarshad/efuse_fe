import { initializeApollo } from "../../config/apolloClient";
import { setErrors } from "./errorActions";
import { CREATE_TEAM, GET_TEAMS_BY_OWNER } from "../../graphql/Team/TeamQuery";
import { CREATE_ORGANIZATION_TEAM, GET_TEAMS_BY_OWNER_BY_ID } from "./types";
import { sendNotification } from "../../helpers/FlashHelper";

const apolloClient = initializeApollo();

export const createTeam = (teamData: Object, userIds: String) => dispatch => {
  apolloClient
    .mutate({
      mutation: CREATE_TEAM,
      variables: { teamArgs: teamData, userIds }
    })
    .then(response => {
      dispatch({
        type: CREATE_ORGANIZATION_TEAM,
        payload: response.data.createTeam
      });
      sendNotification("Team created successfully", "success", "success");
      dispatch(getTeamsByOwner(teamData.owner, true));
    })
    .catch(error => {
      dispatch(setErrors(error));
    });
};

export const getTeamsByOwner = (ownerId: String, disableCache: Boolean = false) => dispatch => {
  apolloClient
    .query({
      query: GET_TEAMS_BY_OWNER,
      variables: { ownerId },
      fetchPolicy: disableCache ? "no-cache" : "cache-first"
    })
    .then(response => {
      dispatch({
        type: GET_TEAMS_BY_OWNER_BY_ID,
        payload: response.data.getTeamsByOwner
      });
    })
    .catch(error => {
      dispatch(setErrors(error));
    });
};
