import axios from "axios";
import { setErrors } from "./errorActions";
import { SET_PROMOCODE, SET_PROMOCODE_ERROR, CLEAR_PROMOCODE_ERROR } from "./types";
import { toggleLoader } from "./loaderActions";

/**
 @module orderActions
 @category Actions
 */

export const validatePromocode = (promocodeText, allowedOn) => dispatch => {
  dispatch(toggleLoader(true));
  axios
    .get(`/promocode/validate/${promocodeText.toUpperCase()}?allowedOn=${allowedOn.toUpperCase()}`)
    .then(response => {
      const { promocode } = response.data;
      dispatch({
        type: SET_PROMOCODE,
        payload: promocode
      });
      dispatch(toggleLoader(false));
    })
    .catch(error => {
      dispatch(toggleLoader(false));
      if (error.response.status == 422) {
        // promocode does not exist error
        dispatch({
          type: SET_PROMOCODE_ERROR,
          payload: error.response.data.error
        });
      } else {
        // all other error types
        dispatch(setErrors(error.response));
      }
    });
};

export const clearPromocodeError = () => dispatch => {
  dispatch({
    type: CLEAR_PROMOCODE_ERROR
  });
};
