import axios from "axios";

import {
  TOGGLE_ORGANIZATION_FOLLOW,
  TOGGLE_FOLLOW_DISABLE,
  GET_ORGANIZATION_FOLLOWERS_REQUEST,
  GET_ORGANIZATION_FOLLOWERS_SUCCESS,
  GET_ORGANIZATION_FOLLOWERS_FAILURE,
  INVALID_REQUEST,
  TOGGLE_ORGANIZATION_FOLLOW_LOUNGE,
  DISABLE_LOUNGE_FOLLOW
} from "./types";
import { setErrors } from "./errorActions";

/**
 @module organizationFollowerActions
 @category Actions
 */

export const followOrganization = (organizationId, lounge = false) => dispatch => {
  dispatch({
    type: lounge ? DISABLE_LOUNGE_FOLLOW : TOGGLE_FOLLOW_DISABLE,
    organizationId,
    value: true,
    disable: true
  });
  axios
    .post(`/organizations/${organizationId}/follow`, {})
    .then(response => {
      const { isFollowed, followObject } = response.data;
      if (isFollowed) {
        analytics.track("ORGANIZATION_FOLLOW", { organizationId });
        dispatch({
          type: lounge ? TOGGLE_ORGANIZATION_FOLLOW_LOUNGE : TOGGLE_ORGANIZATION_FOLLOW,
          followObject,
          organizationId,
          isFollowed: true,
          increment: 1
        });
      }
    })
    .catch(error => {
      dispatch({
        type: lounge ? DISABLE_LOUNGE_FOLLOW : TOGGLE_FOLLOW_DISABLE,
        organizationId,
        value: false,
        disable: false
      });
      dispatch(setErrors(error.response.data));
    });
};

export const unfollowOrganization = (organizationId, lounge = false) => dispatch => {
  dispatch({
    type: lounge ? DISABLE_LOUNGE_FOLLOW : TOGGLE_FOLLOW_DISABLE,
    organizationId,
    value: true,
    disable: true
  });
  axios
    .delete(`/organizations/${organizationId}/unfollow`)
    .then(response => {
      analytics.track("ORGANIZATION_UNFOLLOW", { organizationId });

      dispatch({
        type: lounge ? TOGGLE_ORGANIZATION_FOLLOW_LOUNGE : TOGGLE_ORGANIZATION_FOLLOW,
        organizationId,
        isFollowed: false,
        increment: -1
      });
    })
    .catch(error => {
      dispatch({
        type: lounge ? DISABLE_LOUNGE_FOLLOW : TOGGLE_FOLLOW_DISABLE,
        organizationId,
        value: false,
        disable: false
      });
      dispatch(setErrors(error.response.data));
    });
};

export const getFollowers = (organizationId, page = 1) => dispatch => {
  dispatch({ type: GET_ORGANIZATION_FOLLOWERS_REQUEST });
  axios
    .get(`/organizations/${organizationId}/followers`, { params: { page, pageSize: 9 } })
    .then(response => {
      const { docs, ...rest } = response.data.followers;
      dispatch({ type: GET_ORGANIZATION_FOLLOWERS_SUCCESS, page, payload: docs, pagination: rest });
    })
    .catch(error => {
      dispatch({ type: GET_ORGANIZATION_FOLLOWERS_FAILURE });
      if (error.response.status === 422 || error.response.status === 400) {
        dispatch({ type: INVALID_REQUEST, responseCode: error.response.status });
      }
      setErrors(error.response ? error.response.data : error);
    });
};
