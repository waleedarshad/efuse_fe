import axios from "axios";
import { setErrors } from "./errorActions";
import {
  SET_TOURNAMENT,
  ADD_TEAM,
  REMOVE_TEAM,
  ADD_PLAYER,
  REMOVE_PLAYER,
  SET_SUBMITTING_NEW_EVENT_STATE,
  UPDATE_WINNER_MATCHES,
  TOGGLE_MOCK_EXTERNAL_ERENA_EVENT
} from "./types";
import { toggleLoader } from "./loaderActions";
import { sendNotification } from "../../helpers/FlashHelper";

/**
 @module erenaActions
 @category Actions
 */

export const createErenaEvent = (tournament, router) => dispatch => {
  dispatch(toggleLoader(true));
  dispatch({
    type: SET_SUBMITTING_NEW_EVENT_STATE,
    payload: true
  });

  axios
    .post("/erena/create", tournament)
    .then(response => {
      sendNotification("Tournament was successfully created.", "success", "Success");
      dispatch(toggleLoader(false));
      router.push(`/e/${response.data.slug}`);
    })
    .catch(error => {
      dispatch(setErrors(error.response.data));
      dispatch(toggleLoader(false));
      dispatch({
        type: SET_SUBMITTING_NEW_EVENT_STATE,
        payload: false
      });
    });
};

export const setTournamentSSR = tournament => dispatch => {
  dispatch({
    type: SET_TOURNAMENT,
    payload: tournament
  });
};

export const updateTournament = (tournament, updates) => dispatch => {
  dispatch(toggleLoader(true));

  axios
    .patch(`/erena/${tournament.tournamentId}/update`, updates)
    .then(response => {
      sendNotification("Tournament was successfully updated.", "success", "Success");
      dispatch({
        type: SET_TOURNAMENT,
        payload: response.data
      });
      dispatch(toggleLoader(false));
    })
    .catch(error => {
      dispatch(setErrors(error.response.data));
      dispatch(toggleLoader(false));
    });
};

export const editErenaAd = (tournament, formData) => dispatch => {
  dispatch(toggleLoader(true));

  axios
    .put(`/erena/${tournament.tournamentId}/ad/update`, formData)
    .then(response => {
      sendNotification("Ad successfully updated.", "success", "Success");
      dispatch({
        type: SET_TOURNAMENT,
        payload: response.data
      });
      dispatch(toggleLoader(false));
    })
    .catch(error => {
      dispatch(setErrors(error.response.data));
      dispatch(toggleLoader(false));
    });
};

export const removeErenaAd = (tournament, index) => dispatch => {
  dispatch(toggleLoader(true));

  axios
    .delete(`/erena/${tournament.tournamentId}/ad/${index}/delete`)
    .then(response => {
      sendNotification("Ad successfully removed.", "success", "Success");
      dispatch({
        type: SET_TOURNAMENT,
        payload: response.data
      });
      dispatch(toggleLoader(false));
    })
    .catch(error => {
      dispatch(setErrors(error.response.data));
      dispatch(toggleLoader(false));
    });
};

// teams
export const createTeam = tournament => dispatch => {
  const data = {
    name: "Team Name",
    type: "EFUSE"
  };
  axios
    .post(`/erena/${tournament.tournamentId}/team/create`, data)
    .then(response => {
      dispatch({
        type: ADD_TEAM,
        payload: response.data
      });
    })
    .catch(error => {
      dispatch(setErrors(error.response.data));
    });
};

export const updateTeam = (tournamentId, teamId, data) => dispatch => {
  axios
    .patch(`/erena/${tournamentId}/team/${teamId}`, data)
    .then(response => {
      // dont need to dispatch a function since it is already being updated in state
    })
    .catch(error => {
      dispatch(setErrors(error.response.data));
    });
};

export const deleteTeam = (tournament, teamId, index) => dispatch => {
  axios
    .delete(`/erena/${tournament.tournamentId}/team/${teamId}`)
    .then(response => {
      dispatch({
        type: REMOVE_TEAM,
        payload: index
      });
    })
    .catch(error => {
      dispatch(setErrors(error.response.data));
    });
};

export const createPlayer = (tournament, teamId, teamIndex, data) => dispatch => {
  axios
    .post(`/erena/${tournament.tournamentId}/team/${teamId}/player/create`, data)
    .then(response => {
      dispatch({
        type: ADD_PLAYER,
        payload: response.data,
        index: teamIndex
      });
    })
    .catch(error => {
      dispatch(setErrors(error.response.data));
    });
};

export const updatePlayer = (tournament, teamId, playerId, playerParams) => dispatch => {
  axios
    .patch(`/erena/${tournament.tournamentId}/team/${teamId}/player/${playerId}`, playerParams)
    .then(response => {
      // dont need to dispatch a function since it is already being updated in state
    })
    .catch(error => {
      dispatch(setErrors(error.response.data));
    });
};

export const deletePlayer = (tournament, teamId, playerId, playerIndex, teamIndex) => dispatch => {
  axios
    .delete(`/erena/${tournament.tournamentId}/team/${teamId}/player/${playerId}`)
    .then(response => {
      dispatch({
        type: REMOVE_PLAYER,
        playerIndex,
        teamIndex
      });
    })
    .catch(error => {
      dispatch(setErrors(error.response.data));
    });
};

export const updateMatch = (tournamentId, matchId, data, successMessage) => dispatch => {
  axios
    .patch(`/erena/${tournamentId}/match/${matchId}`, data)
    .then(response => {
      sendNotification(successMessage, "success", "Success");
      dispatch({
        type: UPDATE_WINNER_MATCHES,
        payload: response?.data?.match?.bracket
      });
    })
    .catch(error => {
      dispatch(setErrors(error.response.data));
    });
};

export const updateMatchScreenshot = (tournamentId, matchId, data) => dispatch => {
  axios
    .patch(`/erena/${tournamentId}/match/${matchId}/save_screenshot`, data)
    .then(response => {
      sendNotification("Successfully uploaded an image", "success", "Success");
      dispatch({
        type: UPDATE_WINNER_MATCHES,
        payload: response?.data?.match?.bracket
      });
    })
    .catch(error => {
      dispatch(setErrors(error.response.data));
    });
};

export const toggleMockExternalErenaEvent = () => dispatch => {
  dispatch({
    type: TOGGLE_MOCK_EXTERNAL_ERENA_EVENT
  });
};
