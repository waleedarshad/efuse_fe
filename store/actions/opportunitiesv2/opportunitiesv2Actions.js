import axios from "axios";
import Router from "next/router";
import qs from "qs";
import { OPPORTUNITIES_HREFS } from "../../../common/opportunities";
import { OPPORTUNITIES, SET_OPPORTUNITY_PAGINATION } from "./types";

export const searchForOpportunities = (page, limit, opportunityTypeList, subTypeList, ownerUserId) => (
  dispatch,
  getState
) => {
  const params = {
    page,
    limit,
    publishStatus: "open",
    opportunityType: opportunityTypeList,
    subType: subTypeList,
    user: ownerUserId
  };

  const isPublic = !getState().auth?.currentUser?.id;

  dispatch({ type: OPPORTUNITIES.SEARCH.REQUEST });
  axios
    .get(`${isPublic ? "/public" : ""}/search/opportunities`, {
      params,
      paramsSerializer: paramsList => qs.stringify(paramsList, { arrayFormat: "repeat" })
    })
    .then(response => {
      dispatch({ type: OPPORTUNITIES.SEARCH.SUCCESS, payload: { opportunities: response.data.docs } });
      dispatch({ type: SET_OPPORTUNITY_PAGINATION, payload: { ...response.data, docs: null } });
    })
    .catch(error => {
      dispatch({ type: OPPORTUNITIES.SEARCH.FAILURE, error: error.message });
    });
};

export const getAllOpportunities = (page, limit) => dispatch => {
  dispatch(searchForOpportunities(page, limit, [], [], ""));
};

export const searchOpportunitiesBySubType = (page, limit, subTypeList) => dispatch => {
  dispatch(searchForOpportunities(page, limit, [], subTypeList, ""));
};

export const getCurrentUserOwnedOpportunities = (page, limit) => (dispatch, getState) => {
  const currentUserId = getState().auth.currentUser._id;

  // TODO: use /search/oppportunites?user={id} once it is fixed.
  // Currently, it will not return opportunities in draft state or not approved by eFuse staff.
  const params = {
    page,
    pageSize: limit,
    filters: {},
    id: currentUserId
  };

  dispatch({ type: OPPORTUNITIES.SEARCH.REQUEST });
  axios
    .get("/opportunities/owned", { params })
    .then(response => {
      dispatch({
        type: OPPORTUNITIES.SEARCH.SUCCESS,
        payload: {
          opportunities: response.data.opportunities.docs.map(doc => ({
            ...doc,
            applicantAvatars: doc.applicants.map(applicant => applicant.profilePicture.url)
          }))
        }
      });
      dispatch({ type: SET_OPPORTUNITY_PAGINATION, payload: { ...response.data.opportunities, docs: null } });
    })
    .catch(error => {
      dispatch({ type: OPPORTUNITIES.SEARCH.FAILURE, error: error.message });
    });
};

export const getCurrentUserAppliedOpportunities = (page, limit) => dispatch => {
  const params = {
    page,
    limit,
    publishStatus: "open"
  };

  dispatch({ type: OPPORTUNITIES.SEARCH.APPLIED.REQUEST });
  axios
    .get("/search/applied_opportunities", {
      params
    })
    .then(response => {
      dispatch({ type: OPPORTUNITIES.SEARCH.APPLIED.SUCCESS, payload: { opportunities: response.data.docs } });
      dispatch({ type: SET_OPPORTUNITY_PAGINATION, payload: { ...response.data, docs: null } });
    })
    .catch(error => {
      dispatch({ type: OPPORTUNITIES.SEARCH.APPLIED.FAILURE, error: error.message });
    });
};

export const toggleOpportunitiesFilterModal = () => ({
  type: OPPORTUNITIES.FILTER.MODAL.TOGGLE
});

export const setOpportunitiesFilters = filters => ({
  type: OPPORTUNITIES.FILTER.SET,
  payload: { filters }
});

export const navigateToBrowse = (appliedFilters = []) => dispatch => {
  Router.push({
    pathname: OPPORTUNITIES_HREFS.BROWSE,
    query: { subType: appliedFilters }
  });

  trackAppliedFilters(appliedFilters);
  dispatch(setOpportunitiesFilters(appliedFilters));
};

export const toggleCompactView = () => ({
  type: OPPORTUNITIES.COMPACT.TOGGLE
});

const trackAppliedFilters = filters => {
  filters.forEach(filter =>
    analytics.track("OPPORTUNITY_FILTER_APPLIED", {
      name: filter
    })
  );
};
