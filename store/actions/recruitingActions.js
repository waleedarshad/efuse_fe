import axios from "axios";
import {
  SET_RECRUITING_PROFILE,
  SET_SUBMITTING_QUESTIONNIARE_STATE,
  GET_RECRUITS,
  UPDATED_STARRED_RECRUIT,
  DELETE_RECRUIT_STARRED,
  CLEAR_RECRUITING_PROFILES,
  GET_RECRUITED_PEOPLE,
  CREATE_RECRUITED_PEOPLE,
  UPDATE_RECRUITED_PEOPLE,
  DELETE_RECRUITED_PEOPLE,
  OPEN_RECRUITING_PEOPLE_MODAL,
  EDIT_RECRUITING_PEOPLE,
  POST_RECRUITMENT_PROFILE_NOTE,
  GET_RECRUITMENT_PROFILE_NOTES,
  RECRUITING_SELECTION,
  TOGGLE_RECRUITING_SELECTION_MODAL,
  RECRUITING_SELECTION_ID,
  ONLOAD_RECRUITING_SELECTION,
  GET_STARRED_RECRUITS_FAILURE,
  GET_STARRED_RECRUITS_REQUEST,
  GET_STARRED_RECRUITS_SUCCESS,
  GET_RECRUITS_FAILURE,
  GET_RECRUITS_REQUEST,
  GET_RECRUITS_SUCCESS,
  INVALID_REQUEST,
  UPDATE_RECRUIT_ACTION,
  MARK_APPLICANT_FAVORITE,
  REMOVE_APPLICANT_FAVORITE
} from "./types";
import { getPaginatedCollection } from "./commonActions";
import { toggleLoader } from "./loaderActions";
import { setErrors } from "./errorActions";
import { sendNotification } from "../../helpers/FlashHelper";
import { removeEmptyOrNull } from "../../helpers/GeneralHelper";
import { initializeApollo } from "../../config/apolloClient";
import {
  GET_PROFILE_NOTES,
  CREATE_PROFILE_NOTE,
  MARK_RECRUIT_STARRED,
  DELETE_STARRED_RECRUIT,
  GET_FAVORITE
} from "../../graphql/pipeline/RecruitUserQuery";

const apolloClient = initializeApollo();

/**
 @module recruitingActions
 @category Actions
 */

export const getRecruitmentProfile = () => dispatch => {
  axios
    .get("/recruitment_profile")
    .then(response => {
      const combinedProfile = {
        graduationClass: "",
        addressLine1: "",
        addressLine2: "",
        city: "",
        state: "",
        zipCode: "",
        country: "",
        desiredCollegeMajor: [],
        desiredCollegeSize: "",
        desiredCollegeAttributes: [],
        desiredCollegeRegion: "",
        gender: "",
        esportsCareerField: "",
        highSchoolGPA: "",
        usaTestScores: "",
        toeflTestScores: "",
        extracurricularActivities: "",
        gradingScale: "",
        primaryGame: "",
        primaryGameUsername: "",
        primaryGameHoursPlayed: "",
        secondaryGame: "",
        secondaryGameUsername: "",
        highSchoolClubTeam: "",
        coachName: "",
        coachEmail: "",
        coachPhone: "",
        highSchool: "",
        ...response.data.recruitmentProfile
      };
      dispatch({
        type: SET_RECRUITING_PROFILE,
        payload: combinedProfile
      });
    })
    .catch(error => {
      dispatch(setErrors(error));
    });
};

export const updateRecruitmentProfile = (profileData, redirectURL) => dispatch => {
  dispatch({
    type: SET_SUBMITTING_QUESTIONNIARE_STATE,
    payload: true
  });
  axios
    .patch("/recruitment_profile", profileData)
    .then(response => {
      dispatch({
        type: SET_RECRUITING_PROFILE,
        payload: response.data.recruitmentProfile
      });

      dispatch({
        type: SET_SUBMITTING_QUESTIONNIARE_STATE,
        payload: false
      });

      if (redirectURL) {
        // Redirect to settings page
        document.location.href = redirectURL;
      }
      sendNotification("Profile updated successfully", "success", "success");
    })
    .catch(error => {
      dispatch(setErrors(error));

      dispatch({
        type: SET_SUBMITTING_QUESTIONNIARE_STATE,
        payload: false
      });
    });
};

export const getRecruiting = (
  page = 1,
  pageSize = 9,
  filters = {},
  displayLoader = true,
  useRootPath = true,
  id = "",
  customPath = "",
  limit = 10,
  reset = false
) => dispatch => {
  dispatch(
    getPaginatedCollection(
      page,
      pageSize,
      filters,
      displayLoader,
      "/recruitment_profile/all",
      "recruitmentProfiles",
      GET_RECRUITS,
      toggleLoader,
      setErrors,
      "",
      useRootPath,
      false,
      "",
      false,
      id,
      customPath,
      "",
      limit,
      reset
    )
  );
};

export const getRecruits = (page, recruiterType, recruiterId, filters) => dispatch => {
  dispatch({ type: GET_RECRUITS_REQUEST, payload: page === 1 });

  // clean the filters so we don't filter by empty strings
  const cleanedFilters = filters ? removeEmptyOrNull(filters) : null;

  axios
    .get(
      recruiterType === "ORGANIZATION"
        ? `/recruitment_profile/organizations/${recruiterId}/all`
        : "/recruitment_profile/all",
      { params: { page, pageSize: 9, limit: 10, filters: cleanedFilters } }
    )
    .then(response => {
      const { docs, ...rest } = response.data.recruitmentProfiles;
      dispatch({
        type: GET_RECRUITS_SUCCESS,
        page,
        payload: docs,
        pagination: rest
      });
    })
    .catch(error => {
      dispatch({ type: GET_RECRUITS_FAILURE });
      if (error.response.status === 422 || error.response.status === 400) {
        dispatch({
          type: INVALID_REQUEST,
          responseCode: error.response.status
        });
      }
      setErrors(error.response ? error.response.data : error);
    });
};

export const getStarredRecruiting = (
  page = 1,
  pageSize = 9,
  filters = {},
  displayLoader = true,
  useRootPath = true,
  id = "",
  customPath = "",
  limit = 10,
  reset = false
) => dispatch => {
  dispatch(
    getPaginatedCollection(
      page,
      pageSize,
      filters,
      displayLoader,
      "/recruitment_profile/star",
      "recruitmentProfiles",
      GET_RECRUITS,
      toggleLoader,
      setErrors,
      "",
      useRootPath,
      false,
      "",
      false,
      id,
      customPath,
      "",
      limit,
      reset
    )
  );
};

export const getStarredRecruits = (page, recruiterType, recruiterId, filters) => dispatch => {
  dispatch({ type: GET_STARRED_RECRUITS_REQUEST, payload: page === 1 });

  // clean the filters so we don't filter by empty strings
  const cleanedFilters = filters ? removeEmptyOrNull(filters) : null;

  axios
    .get(
      recruiterType === "User" ? "/recruitment_profile/star" : `/recruitment_profile/organizations/${recruiterId}/star`,
      { params: { page, pageSize: 9, limit: 10, filters: cleanedFilters } }
    )
    .then(response => {
      const { docs, ...rest } = response.data.recruitmentProfiles;
      dispatch({ type: GET_STARRED_RECRUITS_SUCCESS, page, payload: docs, pagination: rest });
    })
    .catch(error => {
      dispatch({ type: GET_STARRED_RECRUITS_FAILURE });
      if (error.response.status === 422 || error.response.status === 400) {
        dispatch({
          type: INVALID_REQUEST,
          responseCode: error.response.status
        });
      }
      setErrors(error.response ? error.response.data : error);
    });
};

export const markRecruitStarred = (starData, type = "recruitmentProfile") => dispatch => {
  return apolloClient
    .mutate({
      variables: { starData },
      mutation: MARK_RECRUIT_STARRED
    })
    .then(() => {
      dispatch({
        type: type === "applicant" ? MARK_APPLICANT_FAVORITE : UPDATED_STARRED_RECRUIT,
        recruitId: starData.relatedDoc
      });
    })
    .catch(error => {
      dispatch(setErrors(error));
    });
};

export const deleteStarredRecruit = (owner, relatedDoc, type = "recruitmentProfile") => async dispatch => {
  const id = await getFavorite(owner, relatedDoc);

  return apolloClient
    .mutate({
      variables: { id },
      mutation: DELETE_STARRED_RECRUIT
    })
    .then(() => {
      dispatch({
        type: type === "applicant" ? REMOVE_APPLICANT_FAVORITE : DELETE_RECRUIT_STARRED,
        recruitId: relatedDoc
      });
    })
    .catch(error => {
      dispatch(setErrors(error));
    });
};

export const clearProfiles = () => dispatch => {
  dispatch({
    type: CLEAR_RECRUITING_PROFILES
  });
};

export const getRecruitedPeople = () => dispatch => {
  axios
    .get("/recruitment_profile/people")
    .then(response => {
      dispatch({
        type: GET_RECRUITED_PEOPLE,
        payload: response.data
      });
    })
    .catch(error => {
      dispatch(setErrors(error));
    });
};

export const createdRecruitedPeople = data => dispatch => {
  axios
    .post("/recruitment_profile/people", data)
    .then(response => {
      dispatch({
        type: CREATE_RECRUITED_PEOPLE,
        payload: response.data
      });
    })
    .catch(error => {
      dispatch(setErrors(error));
    });
};

export const updateRecruitedPeople = (id, data) => dispatch => {
  axios
    .patch(`/recruitment_profile/people/${id}`, data)
    .then(response => {
      dispatch({
        type: UPDATE_RECRUITED_PEOPLE,
        payload: response.data
      });
    })
    .catch(error => {
      dispatch(setErrors(error));
    });
};

export const deleteRecruitedPeople = id => dispatch => {
  axios
    .delete(`/recruitment_profile/people/${id}`)
    .then(() => {
      dispatch({
        type: DELETE_RECRUITED_PEOPLE,
        payload: id
      });
    })
    .catch(error => {
      dispatch(setErrors(error));
    });
};

export const openRecruitingPeopleModal = isOpen => dispatch => {
  dispatch({
    type: OPEN_RECRUITING_PEOPLE_MODAL,
    isOpen
  });
};

export const editRecruitingPeople = data => dispatch => {
  dispatch({
    type: EDIT_RECRUITING_PEOPLE,
    payload: data
  });
};

export const recruitingAs = (recruitType, recruitId) => dispatch => {
  dispatch({
    type: RECRUITING_SELECTION,
    payload: {
      recruitType,
      recruitId
    }
  });
};

export const recrutingSelection = recruitId => dispatch => {
  dispatch({
    type: RECRUITING_SELECTION_ID,
    payload: recruitId
  });
};

export const toggleRecruitingModal = isOpen => dispatch => {
  dispatch({
    type: TOGGLE_RECRUITING_SELECTION_MODAL,
    payload: isOpen
  });
  dispatch({
    type: ONLOAD_RECRUITING_SELECTION,
    payload: false
  });
};

export const performRecruitAction = (recruitmentProfileId, action) => dispatch => {
  axios
    .post("/recruitment_profile/action", {
      recruitmentProfileId,
      action
    })
    .then(response => {
      dispatch({
        type: UPDATE_RECRUIT_ACTION,
        payload: {
          recruitId: response.data.recruitAction.recruitmentProfile,
          recruitAction: response.data.recruitAction.action
        }
      });
    })
    .catch(error => {
      dispatch(setErrors(error));
    });
};

export const performRecruitActionAsOrganization = (recruitmentProfileId, organizationId, action) => dispatch =>
  axios
    .post(`/recruitment_profile/organization/${organizationId}/action`, {
      recruitmentProfileId,
      action
    })
    .then(response => {
      dispatch({
        type: UPDATE_RECRUIT_ACTION,
        payload: {
          recruitId: response.data.recruitAction.recruitmentProfile,
          recruitAction: response.data.recruitAction.action
        }
      });
    })
    .catch(error => {
      dispatch(setErrors(error));
    });

export const getProfileNotes = (recruitId, recruiterId) => dispatch => {
  return apolloClient
    .query({
      query: GET_PROFILE_NOTES,
      variables: { getNotesRelatedDoc: recruitId, getNotesOwner: recruiterId },
      fetchPolicy: "network-only"
    })
    .then(response => {
      const { getNotes } = response.data;
      dispatch({
        type: GET_RECRUITMENT_PROFILE_NOTES,
        payload: getNotes
      });
    })
    .catch(error => {
      setErrors(error.response ? error.response.data : error);
    });
};

export const createProfileNote = createNote => dispatch => {
  return apolloClient
    .mutate({
      mutation: CREATE_PROFILE_NOTE,
      variables: { createNote }
    })
    .then(response => {
      dispatch({
        type: POST_RECRUITMENT_PROFILE_NOTE,
        payload: response.data.createNote
      });
      dispatch(getProfileNotes(createNote.relatedDoc, createNote.author));
    })
    .catch(error => {
      setErrors(error.response ? error.response.data : error);
    });
};

export const getFavorite = (owner, relatedDoc) => {
  return apolloClient
    .query({
      query: GET_FAVORITE,
      variables: { owner, relatedDoc }
    })
    .then(response => {
      return response.data.getFavorite._id;
    })
    .catch(error => {
      setErrors(error.response ? error.response.data : error);
    });
};
