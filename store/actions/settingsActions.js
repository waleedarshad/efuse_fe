import { CROPPER_MODAL } from "./types";

/**
 @module settingsActions
 @category Actions
 */

export const toggleCropModal = (bool, name) => dispatch => {
  dispatch({
    type: CROPPER_MODAL,
    modal: bool,
    name
  });
};
