import axios from "axios";
import Cookies from "js-cookie";

import { SET_OAUTH_ERROR, TOGGLE_OAUTH_BTN_DISABLE } from "./types";
import { redirect } from "../../helpers/OauthHelper";

/**
 @module oauthActions
 @category Actions
 */

export const login = (data, scope) => dispatch => {
  dispatch(toggleDisable(true));
  axios
    .post("/oauth/login", data)
    .then(response => {
      const oneHourFromNow = new Date(new Date().getTime() + 60 * 60 * 1000);
      const { clientName, email, token } = response.data;
      Cookies.set("efuse_temp_token", JSON.stringify({ clientName, email, token }), { expires: oneHourFromNow });
      redirect("authenticate_scope", data.clientId, scope, data.redirect_uri);
      dispatch(toggleDisable(false));
    })
    .catch(error => {
      const errorObject = getErrorObject(error);
      dispatch({ type: SET_OAUTH_ERROR, error: errorObject });
      dispatch(toggleDisable(false));
    });
};

export const authorizeScope = (data, client_id) => dispatch => {
  dispatch(toggleDisable(true));
  axios
    .post("/oauth/token", data)
    .then(response => {
      const { access_token, expires_in, token_type } = response.data;
      dispatch(toggleDisable(false));
      window.location = `${data.redirect_uri}?access_token=${access_token}&expires_in=${expires_in}&token_type=${token_type}`;
    })
    .catch(error => {
      const errorObject = getErrorObject(error);
      if (errorObject.code && errorObject.code === "INVALID_TOKEN") {
        Cookies.remove("efuse_temp_token");
        redirect("token", client_id, data.scope, data.redirect_uri);
      }
      dispatch({ type: SET_OAUTH_ERROR, error: errorObject });
      dispatch(toggleDisable(false));
    });
};

export const clearError = () => dispatch => {
  dispatch({ type: SET_OAUTH_ERROR, error: null });
};

const toggleDisable = disabled => dispatch => {
  dispatch({ type: TOGGLE_OAUTH_BTN_DISABLE, disabled });
};

const getErrorObject = error => {
  let errorObject = {};
  if (error.response?.data) {
    errorObject = error.response.data.error;
  } else {
    errorObject = error;
  }
  return errorObject;
};
