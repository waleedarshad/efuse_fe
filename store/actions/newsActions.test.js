import mockAxios from "axios";
import { getMockStore } from "../../common/testUtils";

import { getOwnedArticles, getNewsArticlesNew, updateNewsArticleGames } from "./newsActions";
import {
  GET_OWNED_NEWS_ARTICLES_FAILURE,
  GET_OWNED_NEWS_ARTICLES_REQUEST,
  GET_OWNED_NEWS_ARTICLES_SUCCESS,
  GET_NEWS_ARTICLES_FAILURE,
  GET_NEWS_ARTICLES_REQUEST,
  GET_NEWS_ARTICLES_SUCCESS,
  INVALID_REQUEST,
  SET_ARTICLE_GAMES
} from "./types";

describe("newsActions", () => {
  let store;

  describe("getNewsArticles", () => {
    const newsArticles = {
      articles: {
        docs: ["article1", "article2"],
        totalDocs: 5,
        page: 1,
        hasNextPage: true
      }
    };

    describe("updateNewsArticleGames successful call", () => {
      const games = [
        {
          _id: "id-of-game-123",
          description: "aim lab",
          createdAt: null,
          kind: "game",
          slug: "aim-lab",
          title: "AIM Lab"
        }
      ];
      const articleId = "articleId-123";
      beforeEach(() => {
        mockAxios.put.mockResolvedValue({ data: games });

        store = getMockStore({});
        store.dispatch(updateNewsArticleGames(articleId, games));
      });

      it("makes the correct network call with correct data", () => {
        expect(mockAxios.put).toHaveBeenCalledWith(`/learning/${articleId}`, { games });
      });

      it("dispatches the correct actions", () => {
        expect(store.getActions()).toEqual([{ type: SET_ARTICLE_GAMES, payload: games }]);
      });
    });

    describe("getNewsArticles successful call", () => {
      beforeEach(() => {
        mockAxios.get.mockResolvedValue({ data: newsArticles });

        store = getMockStore({});
        store.dispatch(getNewsArticlesNew(false, 1, 9, {}));
      });

      it("makes the correct network call", () => {
        expect(mockAxios.get).toHaveBeenCalledWith("/learning", {
          params: { page: 1, pageSize: 9, limit: 10, filters: { status: "Published" } }
        });
      });

      it("dispatches the correct actions", () => {
        expect(store.getActions()).toEqual([
          { type: GET_NEWS_ARTICLES_REQUEST, payload: true },
          {
            type: GET_NEWS_ARTICLES_SUCCESS,
            page: 1,
            payload: ["article1", "article2"],
            pagination: { totalDocs: 5, page: 1, hasNextPage: true }
          }
        ]);
      });
    });

    describe("getNewsArticles failed call", () => {
      const error = {
        response: { status: 422 }
      };
      beforeEach(() => {
        mockAxios.get.mockRejectedValue(error);

        store = getMockStore({});
        store.dispatch(getNewsArticlesNew(false, 2, 9, {}));
      });

      it("dispatches the correct action", () => {
        expect(store.getActions()).toEqual([
          { type: GET_NEWS_ARTICLES_REQUEST, payload: false },
          { type: GET_NEWS_ARTICLES_FAILURE },
          { type: INVALID_REQUEST, responseCode: 422 }
        ]);
      });
    });
  });

  describe("getOwnedArticles", () => {
    const newsArticles = {
      articles: {
        docs: ["article1", "article2"],
        totalDocs: 5,
        page: 1,
        hasNextPage: true
      }
    };

    describe("getOwnedArticles successful call", () => {
      beforeEach(() => {
        mockAxios.get.mockResolvedValue({ data: newsArticles });

        store = getMockStore({});
        store.dispatch(getOwnedArticles(1));
      });

      it("makes the correct network call", () => {
        expect(mockAxios.get).toHaveBeenCalledWith("/learning/owned", {
          params: { page: 1, pageSize: 9, limit: 10 }
        });
      });

      it("dispatches the correct actions", () => {
        expect(store.getActions()).toEqual([
          { type: GET_OWNED_NEWS_ARTICLES_REQUEST, payload: true },
          {
            type: GET_OWNED_NEWS_ARTICLES_SUCCESS,
            page: 1,
            payload: ["article1", "article2"],
            pagination: { totalDocs: 5, page: 1, hasNextPage: true }
          }
        ]);
      });
    });

    describe("getNewsArticles failed call", () => {
      const error = {
        response: { status: 422 }
      };
      beforeEach(() => {
        mockAxios.get.mockRejectedValue(error);

        store = getMockStore({});
        store.dispatch(getOwnedArticles(2));
      });

      it("dispatches the correct action", () => {
        expect(store.getActions()).toEqual([
          { type: GET_OWNED_NEWS_ARTICLES_REQUEST, payload: false },
          { type: GET_OWNED_NEWS_ARTICLES_FAILURE },
          { type: INVALID_REQUEST, responseCode: 422 }
        ]);
      });
    });
  });
});
