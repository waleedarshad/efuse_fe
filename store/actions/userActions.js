import axios from "axios";
import isEmpty from "lodash/isEmpty";
import getConfig from "next/config";
import { GLOBAL_SEARCH_EVENTS } from "../../common/analyticEvents";
import { withCdn } from "../../common/utils";
import { sendNotification } from "../../helpers/FlashHelper";
import { getImage } from "../../helpers/GeneralHelper";
import { loginUser, logoutUser } from "./authActions";
import { storeCurrentUser, setUser } from "./common/userAuthActions";
import { setErrors } from "./errorActions";
import { toggleLoader } from "./loaderActions";
import { initializeApollo } from "../../config/apolloClient";
import {
  BNET_VERIFIED,
  CU_ORGANIZATIONS,
  FACEBOOK_PAGES,
  FILTER_USERS,
  GET_ALIASED_NAME_SUCCESS,
  GET_CURRENT_USER,
  GET_NOTIFICATION_SETTINGS,
  GET_POSTABLE_ORGANIZATIONS,
  LINKEDIN_VALIDATED,
  REMOVE_USER_GAME_PLATFORM,
  SCHEDULED_POSTS,
  SET_PAYOUT_ACCOUNT,
  SET_RECOMMENDATIONS_USERS,
  SET_SNAPCHAT_VERIFIED,
  SET_TWITCH_CLIPS,
  SET_USER_GAME_PLATFORM,
  SET_USER_PAYMENT_METHODS,
  SET_USER_SETUP_INTENT,
  STEAM_VALIDATED,
  TOGGLE_ACTION_PROGRESS,
  TWITTER_VALIDATED,
  UPDATE_NOTIFICATION_SETTINGS,
  UPDATE_PUBLIC_TRAITS_MOTIVATIONS,
  UPDATE_USER,
  USER_LIVE_INFO,
  XBOX_LIVE_AUTH,
  UPDATE_ACCOUNT_STATS,
  UNLINK_ACCOUNT_SUCCESS,
  ALTER_USER_FIELDS
} from "./types";
import { getSocket } from "../../websockets";
import { UPDATE_PASSWORD } from "../../graphql/UserQuery";

const apolloClient = initializeApollo();
const { publicRuntimeConfig } = getConfig();
const { environment } = publicRuntimeConfig;

/**
 * @summary Retrieve the current authed user's organizations
 * @method
 * @module userActions
 * @category Actions
 */
export const getCurrentUserOrganizations = () => dispatch => {
  dispatch(toggleLoader(true));
  axios
    .get("/organizations/owned")
    .then(response => {
      dispatch({
        type: CU_ORGANIZATIONS,
        payload: response.data.organizations
      });
      dispatch(toggleLoader(false));
    })
    .catch(error => {
      dispatch(setErrors(error.response ? error.response.data : error));
      dispatch(toggleLoader(false));
    });
};

/**
 * @summary Retrieve the current user's notification settings
 * @method
 * @module userActions
 * @category Actions
 */
export const getCurrentNotificationSettings = () => dispatch => {
  dispatch(toggleLoader(true));
  axios
    .get("/users/settings/notifications")
    .then(response => {
      dispatch({
        type: GET_NOTIFICATION_SETTINGS,
        payload: response.data.notifications
      });
      dispatch(toggleLoader(false));
    })
    .catch(error => {
      dispatch(setErrors(error.response ? error.response.data : error));
      dispatch(toggleLoader(false));
    });
};

/**
 * @summary Updates current user's notification settings
 * @method
 * @param {*} notificationSettings
 * @module userActions
 * @category Actions
 */
export const updateNotificationSettings = notificationSettings => dispatch => {
  dispatch(toggleLoader(true));
  axios
    .patch("/users/settings/notifications", {
      notifications: notificationSettings
    })
    .then(response => {
      dispatch({
        type: UPDATE_NOTIFICATION_SETTINGS,
        newNotificationSettings: response.data.notifications
      });
      dispatch(toggleLoader(false));
    })
    .catch(error => {
      dispatch(setErrors(error.response));
      dispatch(toggleLoader(false));
    });
};

/**
 * @summary Updates the current authed user's data to match passed param
 * @method
 * @param {*} userData
 * @module userActions
 * @category Actions
 */
export const updateCurrentUser = userData => dispatch => {
  dispatch(toggleLoader(true));
  axios({
    method: "PATCH",
    config: { headers: { "Content-Type": "multipart/form-data" } },
    url: "/users/current_user",
    data: userData
  })
    .then(response => {
      if (response.data && response.data.deleted) {
        sendNotification(response.data.message, "success", "Success");
        dispatch(logoutUser());
      } else {
        sendNotification("Account settings saved.", "success", "Success");
        dispatch({
          type: GET_CURRENT_USER,
          payload: response.data.user
        });
        dispatch(storeCurrentUser(response.data.user));
        dispatch(toggleLoader(false));
      }
    })
    .catch(error => {
      dispatch(setErrors(error.response.data));
      dispatch(toggleLoader(false));
    });
};

/**
 * @summary Updates user
 * @method
 * @param {*} userData
 * @param {*} url
 * @param {*} router
 * @param {*} redirectPath
 * @module userActions
 * @category Actions
 */
export const updateUser = (userData, url, router = null, redirectPath = null) => dispatch => {
  axios
    .post(url, userData)
    .then(() => {
      router.push(redirectPath);
      dispatch(toggleLoader(false));
    })
    .catch(error => {
      dispatch(setErrors(error.response));
      dispatch(toggleLoader(false));
    });
};

/**
 * @summary Deletes user record and calls UpdateGrid
 * @method
 * @param {*} url
 * @param {*} UpdateGrid
 * @module userActions
 * @category Actions
 */
export const deleteUser = (url, UpdateGrid) => dispatch => {
  axios
    .delete(url)
    .then(() => {
      UpdateGrid();
    })
    .catch(error => {
      dispatch(setErrors(error.response));
      dispatch(toggleLoader(false));
    });
};

/**
 * @summary Updates user status
 * @method
 * @param {*} url
 * @param {*} fromData
 * @module userActions
 * @category Actions
 */
export const statusUpdate = (url, fromData) => dispatch => {
  const promise = new Promise((resolve, reject) => {
    axios
      .put(url, fromData)
      .then(response => {
        dispatch({
          type: UPDATE_USER,
          payload: response.data
        });
        const { message } = response.data;
        sendNotification(message, "success", "Success");
        return resolve();
      })
      .catch(error => {
        dispatch(setErrors(error.response));
        dispatch(toggleLoader(false));
        return reject();
      });
  });
  return promise;
};

/**
 * @summary Retrieves user based on url
 * @method
 * @param {*} url
 * @module userActions
 * @category Actions
 */
export const getUser = url => dispatch => {
  const promise = new Promise((resolve, reject) => {
    axios
      .get(url)
      .then(res => {
        dispatch(toggleLoader(false));
        return resolve(res.data);
      })
      .catch(error => {
        dispatch(setErrors(error.response));
        dispatch(toggleLoader(false));
        return reject();
      });
  });
  return promise;
};

/**
 * @summary Creates a new user
 * @method
 * @param {object} userData
 * @param {string} url
 * @param {*} router
 * @param {*} redirectPath
 * @param {*} errorCallback
 * @param {string} signInCaptchaToken
 * @module userAction
 * @category Actions
 */
export const createUser = (
  userData,
  url,
  router,
  redirectPath = null,
  errorCallback,
  signupLocationForTracking
) => dispatch => {
  const referralCode = router?.query.referralCode || "";
  const refSource = router?.query.refSource || "";

  const requestBody = {
    ...userData,
    referral: {
      referralCode,
      refSource
    },
    platform: "web"
  };

  axios
    .post(url, requestBody)
    .then(res => {
      analytics.track("USER_CREATE", {
        location: signupLocationForTracking,
        platform: "web",
        referralCode,
        refSource
      });

      callSnapchatPixel();

      dispatch(loginUser(undefined, redirectPath, res.data));
    })
    .catch(error => {
      if (errorCallback) {
        errorCallback(error?.response?.data || error);
      }

      dispatch(setErrors(error.response.data));
      dispatch(toggleLoader(false));
      dispatch(toggleInProgress(false));
    });
};

const callSnapchatPixel = () => {
  if (["production"].includes(environment) && snaptr) {
    snaptr("track", "SIGN_UP");
  }
};

// The below method is used to get portfolio information ONLY.
// We use this to track portfolio views.
//  Do NOT use for anything other purpose.
/**
 * @summary This method is used to get portfolio information ONLY. We use this to track portfolio views. Do NOT use for anything other purpose.
 * @method
 * @param {*} idOrUsername
 * @param {*} isPublic
 * @module userActions
 * @category Actions
 */
export const getUserPortfolioData = (idOrUsername, isPublic = false) => dispatch => {
  dispatch(setUser(`${isPublic ? "/public" : ""}/users/${idOrUsername}/portfolio`));
};

/**
 * @summary Retrieves a user based on their username
 * @method
 * @param {*} username
 * @param {*} isPublic
 * @module userActions
 * @category Actions
 */
export const getUserByUsername = (username, isPublic = false) => dispatch => {
  dispatch(setUser(`${isPublic ? "/public" : ""}/users/username/${username}`));
};

/**
 * @summary Retrieves a user with GQL based on their ID
 * @method
 * @param {*} id
 * @param {*} isPublic
 * @module userActions
 * @category Actions
 */
export const getUserByIdWithGQL = (id, isPublic = false) => dispatch => {
  dispatch(setUserWithGQL("id", id, isPublic));
};

/**
 * @summary Retrieves a user with GQL based on their username
 * @method
 * @param {*} username
 * @param {*} isPublic
 * @module userActions
 * @category Actions
 */
export const getUserByUsernameWithGQL = (username, isPublic = false) => dispatch => {
  dispatch(setUserWithGQL("username", username, isPublic));
};

/**
 * @summary Updates a user with GQL
 * @method
 * @param {*} key
 * @param {*} value
 * @param {*} includeSenstive
 * @module userActions
 * @category Actions
 */
const setUserWithGQL = (
  // eslint-disable-next-line no-unused-vars
  key,
  // eslint-disable-next-line no-unused-vars
  value,
  // eslint-disable-next-line no-unused-vars
  includeSenstive
) => async (dispatch, getState) => {
  dispatch(toggleLoader(true));
  let user;
  try {
    dispatch({
      type: GET_CURRENT_USER,
      payload: user
    });
    // Whenever we get currentUser object from BE, we update our User Object Cookie
    const { currentUser } = getState().auth;
    if (isEmpty(currentUser) || currentUser.id === user._id) {
      dispatch(storeCurrentUser(user));
    }
    dispatch(toggleLoader(false));
  } catch (error) {
    dispatch(setErrors(error.response ? error.response.data : error));
    window.location = "/lounge";
    dispatch(toggleLoader(false));
  }
};

/**
 * @summary Updates current user
 * Replaced `updateUserWithRemoteCall`, `setUserFromSSR`, and `setUserAfterAuth`
 * @method
 * @param {*} user
 * @category Category, I.E Actions, Reducers, Components
 * @return Return Value
 */
export const updateUserSimple = user => dispatch => {
  dispatch({
    type: GET_CURRENT_USER,
    payload: user
  });
};

/**
 * @summary Authenticates the current user with discord
 * @method
 * @param {*} code
 * @param {*} callback
 * @module userActions
 * @category Actions
 */
export const discordAuth = (code, callback) => dispatch => {
  dispatch(toggleLoader(true));
  axios({
    method: "PATCH",
    config: { headers: { "Content-Type": "multipart/form-data" } },
    url: "/users/discord_auth",
    data: { code }
  })
    .then(response => {
      const { user } = response.data;
      dispatch({
        type: GET_CURRENT_USER,
        payload: user
      });
      dispatch(storeCurrentUser(user));
      dispatch(toggleLoader(false));
      callback("success");
    })
    .catch(() => {
      /* per Fakhir
       * "A method is passed to this action, which is invoked as a callback.
       * The purpose of this callback is to close the consent window and display an error to the user:
       * 'Discord account can't be verified'"
       * Since it is opened in a new consent window we can't dispatch errors.
       */
      dispatch(toggleLoader(false));
      callback("failure");
    });
};

/**
 * @summary Calls API and passes discord server id.
 * @method
 * @param {string} data The Discord server
 */
export const setDiscordServer = data => dispatch => {
  dispatch(toggleLoader(true));

  axios
    .put("/users/discord_server/", data)
    .then(response => {
      dispatch(toggleLoader(false));
      dispatch({
        type: GET_CURRENT_USER,
        payload: response.data.updatedUser
      });
    })
    .catch(error => {
      dispatch(setErrors(error.response));
      dispatch(toggleLoader(false));
    });
};

/**
 * @summary Calls API to remove discord server.
 * @method
 */
export const removeDiscordServer = () => dispatch => {
  dispatch(toggleLoader(true));

  axios
    .put("/users/remove_discord_server")
    .then(response => {
      dispatch(toggleLoader(false));
      dispatch({
        type: GET_CURRENT_USER,
        payload: response.data.updatedUser
      });
    })
    .catch(error => {
      dispatch(setErrors(error.response));
      dispatch(toggleLoader(false));
    });
};

/**
 * @summary Calls API and passes a name and url.
 * @method
 * @param {string} discordServer
 */
export const setDiscordWebhook = data => dispatch => {
  dispatch(toggleLoader(true));
  axios
    .post("/users/discord_webhook", data.discordWebHook)
    .then(response => {
      dispatch(toggleLoader(false));
      dispatch({ type: GET_CURRENT_USER, payload: response.data.user });
    })
    .catch(error => {
      dispatch(setErrors(error.response));
      dispatch(toggleLoader(false));
    });
};

/**
 * @summary Calls API to remove discord webhook.
 * @method
 */
export const removeDiscordWebhook = id => dispatch => {
  dispatch(toggleLoader(true));
  axios
    .delete(`/users/discord_webhook/${id}`)
    .then(response => {
      dispatch(toggleLoader(false));
      dispatch({ type: GET_CURRENT_USER, payload: response.data.user });
    })
    .catch(error => {
      dispatch(setErrors(error.response));
      dispatch(toggleLoader(false));
    });
};

/**
 * @summary Authenticates current user with facebook
 * @method
 * @param {*} accessToken
 * @param {*} facebookId
 * @param {*} callback
 * @module userActions
 * @category Actions
 */
export const facebookAuth = (accessToken, facebookId, callback) => dispatch => {
  dispatch(toggleLoader(true));
  axios
    .post("/facebook/auth", { accessToken, facebookId })
    .then(response => {
      const { user } = response.data;
      dispatch({
        type: GET_CURRENT_USER,
        payload: user
      });
      dispatch(storeCurrentUser(user));
      dispatch(toggleLoader(false));
      callback("success");
    })
    .catch(() => {
      /* per Fakhir
       * "A method is passed to this action, which is invoked as a callback.
       * The purpose of this callback is to close the consent window and display an error to the user:
       * 'Discord account can't be verified'"
       * Since it is opened in a new consent window we can't dispatch errors.
       */
      dispatch(toggleLoader(false));
      callback("failure");
    });
};

/**
 * @summary Authenticates current user with twitch
 * @method
 * @param {*} code
 * @param {*} callback
 * @module userActions
 * @category Actions
 */
export const twitchAuth = (code, callback) => dispatch => {
  dispatch(toggleLoader(true));
  axios({
    method: "PATCH",
    config: { headers: { "Content-Type": "multipart/form-data" } },
    url: "/users/twitch_auth",
    data: { code }
  })
    .then(response => {
      const { user } = response.data;
      dispatch({
        type: GET_CURRENT_USER,
        payload: user
      });
      dispatch(storeCurrentUser(user));
      dispatch(toggleLoader(false));
      callback("success");
    })
    .catch(() => {
      /* per Fakhir
       * "A method is passed to this action, which is invoked as a callback.
       * The purpose of this callback is to close the consent window and display an error to the user:
       * 'Discord account can't be verified'"
       * Since it is opened in a new consent window we can't dispatch errors.
       */
      dispatch(toggleLoader(false));
      callback("failure");
    });
};

/**
 * @summary Sets user form steam
 * @method
 * @param {*} response
 * @param {*} callback
 * @module userActions
 * @category Actions
 */
export const setUserFromSteam = (response, callback) => dispatch => {
  dispatch({ type: STEAM_VALIDATED });
  callback(response);
};

/**
 * @summary Sets user form BattleNet
 * @method
 * @param {*} response
 * @param {*} callback
 * @module userActions
 * @category Actions
 */
export const setUserFromBnet = (response, callback) => dispatch => {
  dispatch({ type: BNET_VERIFIED });
  callback(response);
};

/**
 * @summary Sets user form LinkedIn
 * @method
 * @param {*} response
 * @param {*} callback
 * @module userActions
 * @category Actions
 */
export const setUserFromLinkedin = (response, callback) => dispatch => {
  dispatch({ type: LINKEDIN_VALIDATED });
  callback(response);
};

/**
 * @summary Unlink external account
 * @method
 * @param {*} data
 * @module userActions
 * @category Actions
 */
export const unlinkExternalAccount = data => dispatch => {
  axios
    .put("/users/unlink_external_account", data)
    .then(response => {
      sendNotification("Successfully Unlinked!", "warning", data.service.toLocaleUpperCase());
      dispatch(updateUserSimple(response.data.user));
    })
    .catch(err => dispatch(setErrors(err.response)));
};

/**
 * @summary Unlink Game account
 * @method
 * @param {*} data
 * @module userActions
 * @category Actions
 */
export const unlinkGameAccount = data => dispatch => {
  axios
    .put("/users/unlink_game_account", data)
    .then(() => {
      sendNotification("Successfully Unlinked!", "warning", data.game.toLocaleUpperCase());
      dispatch({
        type: UNLINK_ACCOUNT_SUCCESS,
        payload: data.game
      });
    })
    .catch(err => dispatch(setErrors(err.response)));
};

/**
 * @summary Search for users
 * @method
 * @param {*} query
 * @module userActions
 * @category Actions
 */
export const searchUsers = query => async dispatch => {
  dispatch(buildSearchBarOptions(query, `/search?${getSearchParam(query, "user")}`));
};

/**
 * @summary Do global search for users or organizations
 * @method
 * @param {*} query
 * @module userActions
 * @category Actions
 */
export const globalSearch = query => async dispatch => {
  dispatch(buildSearchBarOptions(query, `/search?${getSearchParam(query)}`));
};

/** @summary Generates search params based on querystring and kind passed in
 * @method
 * @param { string } queryString
 * @param { string } kind ("org"|"user"), omit if searching orgs and users
 * @module userActions
 * @category Actions
 */
const getSearchParam = (queryString, kind = null) => {
  // Root url: api/search?term={queryString}&kind=[org|user]
  let params = `term=${queryString}`;

  if (kind !== null) {
    params = `${params}&kind=${kind}`;
  }
  return params;
};

/**
 * @summary Build options for searches from the search bar
 * @method
 * @param {*} query
 * @param {*} endpoint
 * @module userActions
 * @category Actions
 */
const buildSearchBarOptions = (query, endpoint) => dispatch =>
  axios
    .get(endpoint)
    .then(response => {
      analytics.track(GLOBAL_SEARCH_EVENTS.QUERY, {
        query
      });

      const payload = response.data.data.users.docs.map(user => ({
        value: user._id,
        label: (
          <div className="optionWithImage">
            <img src={getImage(user.profilePicture, "avatar")} alt="Profile" />{" "}
            <span>
              <strong>{user.name}</strong>{" "}
            </span>
            {user.verified && (
              <span>
                <img
                  style={{ width: "16px", height: "auto" }}
                  src={withCdn("/static/images/efuse_verify.png")}
                  alt="eFuse Verify"
                />
              </span>
            )}
            <span>@{user.username}</span>
          </div>
        ),
        avatar: user.profilePicture,
        url: user.url,
        username: user.username,
        type: user.type
      }));

      dispatch({
        type: FILTER_USERS,
        payload
      });

      return payload;
    })
    .catch(err => dispatch(setErrors(err)));

/**
 * @summary Retrieve social live
 * @method
 * @param {*} id
 * @module userActions
 * @category Actions
 */
export const getSocialLive = id => dispatch => {
  getYoutubeStream(id, dispatch);
};

// spavel: Not currently references anywhere.
// /**
//  * @summary Retrieve Twitch Stream based on userID
//  * @method
//  * @param {*} userId
//  * @param {*} dispatch
//  * @module userActions
//  * @category Actions
//  */
// const getTwitchStream = (userId, dispatch) => {
//   axios.get(`/twitch/get_stream/${userId}`).then(response => {
//     dispatch({
//       type: USER_LIVE_INFO,
//       payload: response.data
//     });
//   });
// };

/**
 * @summary Retrieve youtube stream based on userID
 * @method
 * @param {*} userId
 * @param {*} dispatch
 * @module userActions
 * @category Actions
 */
const getYoutubeStream = (userId, dispatch) => {
  axios.get(`/users/get_youtube_stream/${userId}`).then(response => {
    dispatch({
      type: USER_LIVE_INFO,
      payload: response.data
    });
  });
};

/**
 * @summary Sends password reset email
 * @method
 * @param {*} data
 * @param {*} router
 * @module userActions
 * @category Actions
 */
export const forgotPassword = (data, router) => dispatch => {
  axios
    .post("/public/users/send_password_reset", data)
    .then(response => {
      if (response.data.emailSent) {
        analytics.track("USER_PASSWORD_RESET_EMAIL_SENT");
        sendNotification(response.data.message, "success", "Success");
        router.push(router.asPath);
      } else {
        sendNotification(
          "Something went wrong while sending you password reset instructions. Please try again.",
          "warning",
          "Forgot Password"
        );
      }
    })
    .catch(error => {
      dispatch(toggleLoader(false));
      sendNotification(error.response.data.message, error.response.data.flashType, "Forgot Password");
    });
};

/**
 * @summary Resets user password
 * @method
 * @param {*} data
 * @param {*} token
 * @param {*} router
 * @module userActions
 * @category Actions
 */
export const resetPassword = (data, token, router) => dispatch => {
  axios
    .put(`/public/users/reset_password/${token}`, data)
    .then(response => {
      dispatch(toggleLoader(true));
      if (response.status === 200) {
        analytics.track("USER_PASSWORD_RESET");
        dispatch(toggleLoader(false));
        sendNotification(response.data.message, "success", "Success");
        router.push("/login");
      }
    })
    .catch(error => {
      setErrors(error?.response?.data);
      dispatch(toggleLoader(false));
    });
};

/**
 * @summary Resends email verification to current user
 * @method
 * @param {*} data
 * @param {*} router
 * @module userActions
 * @category Actions
 */
export const resendEmailVerification = (data, router) => dispatch => {
  axios
    .put("/public/users/resend_email_verification", data)
    .then(response => {
      dispatch(toggleLoader(true));
      if (response.status === 200) {
        analytics.track("USER_VERIFICATION_EMAIL_SENT");
        dispatch(toggleLoader(false));
        sendNotification(response.data.message, "success", "Success");
        router.push("/login");
      }
    })
    .catch(error => {
      dispatch(setErrors(error.response.data));
      dispatch(toggleLoader(false));
    });
};

/**
 * @summary Verify user email
 * @method
 * @param {*} token
 * @param {*} router
 * @module userActions
 * @category Actions
 */
export const verifyEmail = (token, router) => dispatch => {
  axios
    .put(`/public/users/verify_email/${token}`)
    .then(response => {
      dispatch(toggleLoader(true));
      if (response.status === 200) {
        analytics.track("USER_EMAIL_VERIFIED");
        dispatch(toggleLoader(false));
        sendNotification(response.data.message, response.data.flashType, "Email Verified");
        router.push("/login");
      }
    })
    .catch(err => {
      dispatch(setErrors(err.response.data));
      dispatch(toggleLoader(false));
      router.push("/resend_verification_email");
    });
};

/**
 * @summary Authenticate user with Twitter
 * @method
 * @module userActions
 * @category Actions
 */
export const twitterAuth = callback => dispatch => {
  dispatch({ type: TWITTER_VALIDATED });
  callback();
};

/**
 * @summary Remove user picture
 * @method
 * @param {*} type
 * @module userActions
 * @category Actions
 */
export const removePic = type => dispatch => {
  axios.put(`/users/remove_pic?type=${type}`).then(response => {
    dispatch({
      type: GET_CURRENT_USER,
      payload: response.data.user.user
    });
  });
};

/**
 * @summary Update public visibility status of user traits and motivations in portfolio
 * @method
 * @module userActions
 * @category Actions
 * @param publicTraitsMotivations
 */
export const updatePublicTraitsAndMotivations = publicTraitsMotivations => dispatch => {
  axios
    .patch("users/public_traits_motivations", { publicTraitsMotivations })
    .then(response => {
      dispatch({
        type: UPDATE_PUBLIC_TRAITS_MOTIVATIONS,
        payload: response.data.publicTraitsMotivations
      });
    })
    .catch(error => {
      setErrors(error);
    });
};

/**
 * @summary Calls API and returns facebook pages
 * @method
 */
export const getFacebookPages = () => dispatch => {
  dispatch(toggleLoader(true));
  axios
    .get("/facebook/pages")
    .then(response => {
      dispatch({
        type: FACEBOOK_PAGES,
        payload: response.data
      });
      dispatch(toggleLoader(false));
    })
    .catch(error => {
      dispatch(setErrors(error.response));
      dispatch(toggleLoader(false));
    });
};

/**
 * @summary Calls API and passes Facebook page name and ID
 * @method
 * @param {string} pageName the name of the Facebook Page
 * @param {string} pageId the Facebook Page's ID
 */
export const addFacebookPage = data => dispatch => {
  dispatch(toggleLoader(true));
  axios
    .post("/facebook/pages", data)
    .then(response => {
      dispatch(toggleLoader(false));
      dispatch({ type: GET_CURRENT_USER, payload: response.data.user });
    })
    .catch(error => {
      dispatch(setErrors(error.response));
      dispatch(toggleLoader(false));
    });
};

/**
 * @summary Calls API to remove a Facebook page
 * @method
 * @desc remove a facebook Page from the user's facebook pages array
 * @param  id: the eFuse ID for the user's Facebook Page object
 */
export const removeFacebookPage = id => dispatch => {
  dispatch(toggleLoader(true));
  axios
    .delete(`/facebook/pages/${id}`)
    .then(response => {
      dispatch(toggleLoader(false));
      dispatch({ type: GET_CURRENT_USER, payload: response.data.user });
    })
    .catch(error => {
      dispatch(setErrors(error.response));
      dispatch(toggleLoader(false));
    });
};

/**
 * @summary Sets In Progress
 * @method
 * @param {*} inProgress
 * @module userActions
 * @category Actions
 */
export const toggleInProgress = inProgress => dispatch => dispatch({ type: TOGGLE_ACTION_PROGRESS, inProgress });

/**
 * @summary Retrieves postable organizations
 * @method
 * @param {*} page
 * @param {*} pageSize
 * @module userActions
 * @category Actions
 */
export const getPostableOrganizations = (page, pageSize = 10) => dispatch => {
  axios
    .get(`/organizations/postable?page=${page}&pageSize=${pageSize}`)
    .then(response => {
      const { docs, ...pagination } = response.data;
      dispatch({
        type: GET_POSTABLE_ORGANIZATIONS,
        organizations: docs,
        pagination
      });
    })
    .catch(error => {
      dispatch({
        type: GET_POSTABLE_ORGANIZATIONS,
        organizations: [],
        pagination: {}
      });
      dispatch(setErrors(error.response));
    });
};

/**
 * @summary Calls API and returns scheduled posts.
 * @method
 */
export const getScheduledPosts = () => dispatch => {
  dispatch(toggleLoader(true));
  axios
    .get("/feeds/scheduled")
    .then(response => {
      dispatch(toggleLoader(false));
      dispatch({
        type: SCHEDULED_POSTS,
        payload: response.data
      });
    })
    .catch(error => {
      dispatch(setErrors(error.response));
      dispatch(toggleLoader(false));
    });
};

/**
 * @summary Calls API and deletes the scheduled post.
 * @method
 * @param  id: the eFuse ID for the user's scheduled post object.
 */
export const deleteScheduledPost = id => dispatch => {
  dispatch(toggleLoader(true));
  axios
    .delete(`/feeds/scheduled/${id}`)
    .then(response => {
      dispatch(toggleLoader(false));
      dispatch({
        type: SCHEDULED_POSTS,
        payload: response.data
      });
    })
    .catch(error => {
      dispatch(setErrors(error.response));
      dispatch(toggleLoader(false));
    });
};

/**
 * @summary Calls API and updates the order of the new portfolio order.
 * @method
 * @param {object} data the new portfolio components array.
 */
export const portfolioSortLayout = data => dispatch => {
  dispatch(toggleLoader(true));
  axios
    .put("/users/update_component_layout", data)
    .then(() => {
      dispatch(toggleLoader(false));
    })
    .catch(() => {
      dispatch(toggleLoader(false));
    });
};

/**
 *
 */
/**
 * @summary Retrieves a set number of recommended users
 * @method
 * @param {*} amount
 * @module userActions
 * @category Actions
 */
export const getRecommendedUsers = (amount = 3) => dispatch => {
  axios
    .get("/recommendations/users", {
      params: {
        amount
      }
    })
    .then(response => {
      dispatch({
        type: SET_RECOMMENDATIONS_USERS,
        payload: response.data.users
      });
    })
    .catch(error => {
      dispatch(setErrors(error.response ? error.response.data : error));
    });
};

/**
 * @summary Retrieves payment methods for current user
 * @method
 * @module userActions
 * @category Actions
 */
export const getPaymentMethods = () => dispatch => {
  axios
    .get("/users/payments", {
      params: {}
    })
    .then(response => {
      dispatch({
        type: SET_USER_PAYMENT_METHODS,
        payload: response.data.cards
      });
    })
    .catch(error => {
      dispatch(setErrors(error.response ? error.response.data : error));
    });
};

/**
 * @summary Remove payment method based on card id
 * @method
 * @param {*} cardId
 * @module userActions
 * @category Actions
 */
export const removePaymentMethod = cardId => dispatch => {
  dispatch(toggleLoader(true));
  axios
    .delete(`/users/payments/${cardId}`, {
      params: {}
    })
    .then(() => {
      dispatch(toggleLoader(false));
      dispatch(getPaymentMethods());
    })
    .catch(error => {
      dispatch(toggleLoader(false));
      dispatch(setErrors(error.response ? error.response.data : error));
    });
};

/**
 * @summary Retrieves setup intent
 * @method
 * @module userActions
 * @category Actions
 */
export const getSetupIntent = () => dispatch => {
  axios
    .get("/users/payments/setup_intent", {
      params: {}
    })
    .then(response => {
      dispatch({
        type: SET_USER_SETUP_INTENT,
        payload: response.data.intent
      });
    })
    .catch(error => {
      dispatch(setErrors(error.response ? error.response.data : error));
    });
};

/**
 * @summary Retrieves payout account for stripe
 * @method
 * @module userActions
 * @category Actions
 */
export const getPayoutAccount = () => dispatch => {
  axios
    .get("/stripe/payout_account", {
      params: {}
    })
    .then(response => {
      dispatch({
        type: SET_PAYOUT_ACCOUNT,
        payload: response.data.account
      });
    })
    .catch(error => {
      dispatch(setErrors(error.response ? error.response.data : error));
    });
};

/**
 * @summary Verifies payout account for stripe
 * @method
 * @param {*} code
 * @module userActions
 * @category Actions
 */
export const verifyPayoutAccount = code => dispatch => {
  dispatch(toggleLoader(true));
  axios
    .post("/stripe/oauth", {
      code
    })
    .then(() => {
      dispatch(toggleLoader(false));
      sendNotification(
        "Success! You will now be able to recieve payouts from eFuse.",
        "success",
        "Payout Account Connected"
      );
      dispatch(getPayoutAccount());
    })
    .catch(error => {
      dispatch(setErrors(error.response.data));
      dispatch(toggleLoader(false));
    });
};

/**
 * @summary Add game platform
 * displayName = the user's username
 * platform = xbl, battlenet, psn, steam
 * @method
 * @param {*} data
 * @module userActions
 * @category Actions
 */
export const addPlatform = data => dispatch => {
  axios
    .post("/users/game_platforms", data)
    .then(response => {
      dispatch({
        type: SET_USER_GAME_PLATFORM,
        payload: response.data.gamePlatform
      });
    })
    .catch(error => {
      dispatch(setErrors(error.response));
    });
};

/**
 * @summary Remove game platform
 * @method
 * @param {*} platforms
 * @param {*} platformId
 * @module userActions
 * @category Actions
 */
export const removePlatform = (platforms, platformId) => dispatch => {
  axios
    .delete(`/users/game_platforms/${platformId}`)
    .then(response => {
      const platformIndex = platforms.findIndex(platform => platform._id === response?.data?.gamePlatform?._id);
      const platformArray = [...platforms];
      platformArray.splice(platformIndex, 1);
      dispatch({
        type: REMOVE_USER_GAME_PLATFORM,
        payload: platformArray
      });
    })
    .catch(error => {
      dispatch(setErrors(error.response));
    });
};

/**
 * @summary Retrieve Twitch clips for user
 * @method
 * @param {*} userId
 * @param {*} page
 * @param {*} pageSize
 * @module userActions
 * @category Actions
 */
export const getTwitchClips = (userId = "", page = 1, pageSize = 6) => dispatch => {
  axios
    .get(`/twitch/${userId}/clips?page=${page}&pageSize=${pageSize}`)
    .then(response => {
      const { clips } = response.data;
      dispatch({
        type: SET_TWITCH_CLIPS,
        payload: clips
      });
    })
    .catch(error => {
      dispatch(setErrors(error.response));
    });
};

/**
 * @summary Authenticate with Xbox Live
 * @method
 * @param {*} code
 * @param {*} callback
 * @module userActions
 * @category Actions
 */
export const xboxAuth = (code, callback) => async dispatch => {
  try {
    const response = await axios.patch("/users/xbox_auth", { code });
    const { user } = response.data;
    dispatch({ type: XBOX_LIVE_AUTH, user });
    callback("success", user);
  } catch (error) {
    dispatch(setErrors(error.response.data));
    callback("failure");
  }
};

/**
 * @summary Set Snapchat Verified status
 * @method
 * @param {*} callback
 * @module userActions
 * @category Actions
 */
export const setSnapchatVerified = callback => dispatch => {
  dispatch({ type: SET_SNAPCHAT_VERIFIED });
  callback();
};

/**
 * @summary Get a random anonymous name for the user
 * @method
 * @param
 * @module userActions
 * @category Actions
 */

export const getAliasedFullName = () => dispatch => {
  axios
    .put("/users/generate_alias")
    .then(response => {
      dispatch({
        type: GET_ALIASED_NAME_SUCCESS,
        name: response.data.user.name
      });
      sendNotification("You have successfully aliased your account", "success", "Success");
      analytics.track("USER_ALIAS_ENABLED");
    })
    .catch(error => {
      setErrors(error);
    });
};

/**
 * @summary Delete user's account
 *
 * @param {string} password
 * @module userActions
 * @category Actions
 */
export const deleteAccount = (password, callback) => async dispatch => {
  try {
    const response = await axios.post("/users/delete_account", { password });

    if (response.data.success) {
      dispatch(logoutUser());
      sendNotification("Your account has been deleted", "success", "Success");
    }
    callback();
  } catch (error) {
    setErrors(error?.response?.data);
    callback();
  }
};

/**
 * @summary update user stats
 *
 */
export const trackStats = () => async dispatch => {
  const socket = getSocket();
  socket.on("UPDATE_ACCOUNT_STATS", payload => {
    dispatch({
      type: UPDATE_ACCOUNT_STATS,
      payload
    });
  });
};

/**
 * @summary Update few fields of user in store state.user.currentUser
 *
 * @param {Record<string, unknown} payload
 */
export const alterUserFields = payload => dispatch => {
  dispatch({ type: ALTER_USER_FIELDS, payload });
};

/**
 * @summary Update user's account password
 *
 * @param {*} updatePasswordData
 * @module userActions
 * @category Actions
 */
export const updatePassword = (updatePasswordData, closeModal) => () => {
  return apolloClient
    .mutate({
      mutation: UPDATE_PASSWORD,
      variables: { updatePasswordBody: updatePasswordData }
    })
    .then(() => {
      sendNotification("Password Updated Successfully", "success", "Success");
      closeModal();
    })
    .catch(error => {
      sendNotification(error.message, "danger", "Error");
    });
};
