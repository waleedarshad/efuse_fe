import axios from "axios";
import { setErrors } from "./errorActions";
import { GET_EXPERIENCES } from "./types";

/**
 @module experiencesActions
 @category Actions
 */

// gets all user experiences to be displayed
export const getUserExperiences = () => dispatch => {
  axios
    .get("/users/get_user_experience")
    .then(response => {
      dispatch({
        type: GET_EXPERIENCES,
        payload: response.data
      });
    })
    .catch(error => {
      dispatch(setErrors(error.response));
    });
};

// increments global view of the component
export const incrementGlobalView = (experienceId, successCallback) => dispatch => {
  const data = {
    experienceId,
    isViewed: true,
    isGlobal: true
  };
  axios
    .put("/users/increment_views", data)
    .then(response => {
      if (successCallback) successCallback();
    })
    .catch(error => {
      dispatch(setErrors(error.response));
    });
};

// accepts experience id and subview
export const incrementSubView = (experienceId, subView) => dispatch => {
  const data = {
    experienceId,
    isViewed: true,
    isGlobal: false,
    viewName: subView
  };
  axios
    .put("/users/increment_views", data)
    .then(response => {})
    .catch(error => {
      dispatch(setErrors(error.response));
    });
};

// accepts experience id and subview
export const incrementSubSkip = (experienceId, subView) => dispatch => {
  const data = {
    experienceId,
    isViewed: false,
    isGlobal: false,
    viewName: subView
  };
  axios
    .put("/users/increment_views", data)
    .then(response => {})
    .catch(error => {
      dispatch(setErrors(error.response));
    });
};
