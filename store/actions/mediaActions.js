import axios from "axios";

import { toggleLoader } from "./loaderActions";
import { setErrors } from "./errorActions";
import { GET_MEDIA, EXPAND_MEDIA, CLOSE_MEDIA, INVALID_REQUEST } from "./types";

/**
 @module mediaActions
 @category Actions
 */

export const deleteFromS3 = fileKey => {
  axios
    .delete(`/media/s3/delete?fileKey=${fileKey}`)
    .then(response => console.log("Delete from S3"))
    .catch(error => console.log(error));
};

export const getMedia = (userId, page = 1, pageSize = 15) => dispatch => {
  dispatch(toggleLoader(true));
  axios
    .get(`/media/${userId}?page=${page}&pageSize=${pageSize}`)
    .then(response => {
      const { docs, ...rest } = response.data.media;
      dispatch({ type: GET_MEDIA, media: docs, pagination: { ...rest } });
      dispatch(toggleLoader(false));
    })
    .catch(error => {
      if (error.response && (error.response.status === 422 || error.response.status === 400)) {
        dispatch({
          type: INVALID_REQUEST,
          responseCode: error.response.status
        });
      }
      dispatch(setErrors(error.response.data));
      dispatch(toggleLoader(false));
    });
};

export const expandMedia = media => dispatch => {
  dispatch({ type: EXPAND_MEDIA, media });
};

export const closeMedia = () => dispatch => {
  dispatch({ type: CLOSE_MEDIA });
};
