import { SHARE_LINK_MODAL } from "./types";

// eslint-disable-next-line import/prefer-default-export
export const shareLinkModal = (show, title = "", link = "") => dispatch => {
  dispatch({ type: SHARE_LINK_MODAL, show, title, link });
};
