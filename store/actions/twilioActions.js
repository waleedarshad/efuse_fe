import axios from "axios";
import { setErrors } from "./errorActions";
import { toggleLoader } from "./loaderActions";
import { sendNotification } from "../../helpers/FlashHelper";

/**
 @module twilioActions
 @category Actions
 */

const sendAppDownloadText = phoneNumber => dispatch => {
  dispatch(toggleLoader(true));
  axios
    .post("https://download-app-text-8526.twil.io/welcome", { phoneNumber })
    .then(() => {
      dispatch(toggleLoader(false));
      sendNotification("App download link sent to your phone!", "success", "Text Message Sent");
    })
    .catch(error => {
      dispatch(toggleLoader(false));
      sendNotification;
      dispatch(setErrors(error?.response?.data?.error || "There was an error sending the text."));
    });
};

export default sendAppDownloadText;
