import mockAxios from "axios";

import { getMockStore } from "../../common/testUtils";
import { getRecruits, getStarredRecruits } from "./recruitingActions";
import {
  GET_STARRED_RECRUITS_FAILURE,
  GET_STARRED_RECRUITS_REQUEST,
  GET_STARRED_RECRUITS_SUCCESS,
  GET_RECRUITS_FAILURE,
  GET_RECRUITS_REQUEST,
  GET_RECRUITS_SUCCESS,
  INVALID_REQUEST
} from "./types";

describe("recruitingActions", () => {
  let mockStore;

  describe("getStarredRecruits", () => {
    const recruits = {
      recruitmentProfiles: {
        docs: ["starredRecruits1", "starredRecruits2"],
        hasNextPage: true,
        totalDocs: 15,
        page: 1
      }
    };

    describe("getStarredRecruits successful call", () => {
      beforeEach(() => {
        mockAxios.get.mockResolvedValue({ data: recruits });

        mockStore = getMockStore({});
        mockStore.dispatch(getStarredRecruits(1, "User"));
      });

      it("makes the correct network call", () => {
        expect(mockAxios.get).toHaveBeenCalledWith("/recruitment_profile/star", {
          params: { page: 1, pageSize: 9, limit: 10, filters: null }
        });
      });

      it("dispatches the correct actions", () => {
        expect(mockStore.getActions()).toEqual([
          { type: GET_STARRED_RECRUITS_REQUEST, payload: true },
          {
            type: GET_STARRED_RECRUITS_SUCCESS,
            payload: ["starredRecruits1", "starredRecruits2"],
            page: 1,
            pagination: { totalDocs: 15, hasNextPage: true, page: 1 }
          }
        ]);
      });
    });

    describe("getStarredRecruits failed call", () => {
      const error = {
        response: {
          status: 422
        }
      };

      beforeEach(() => {
        mockAxios.get.mockRejectedValue(error);

        mockStore = getMockStore({});
        mockStore.dispatch(getStarredRecruits(2, "org"));
      });

      it("dispatches the correct actions", () => {
        expect(mockStore.getActions()).toEqual([
          { type: GET_STARRED_RECRUITS_REQUEST, payload: false },
          { type: GET_STARRED_RECRUITS_FAILURE },
          { type: INVALID_REQUEST, responseCode: 422 }
        ]);
      });
    });
  });

  describe("getRecruits", () => {
    const recruits = {
      recruitmentProfiles: {
        docs: ["recruit1", "recruit2"],
        hasNextPage: true,
        totalDocs: 15,
        page: 1
      }
    };

    describe("getRecruits successful call", () => {
      beforeEach(() => {
        mockAxios.get.mockResolvedValue({ data: recruits });

        mockStore = getMockStore({});
        mockStore.dispatch(getRecruits(1, "User"));
      });

      it("makes the correct network call", () => {
        expect(mockAxios.get).toHaveBeenCalledWith("/recruitment_profile/all", {
          params: { page: 1, pageSize: 9, limit: 10, filters: null }
        });
      });

      it("dispatches the correct actions", () => {
        expect(mockStore.getActions()).toEqual([
          { type: GET_RECRUITS_REQUEST, payload: true },
          {
            type: GET_RECRUITS_SUCCESS,
            payload: ["recruit1", "recruit2"],
            page: 1,
            pagination: { totalDocs: 15, hasNextPage: true, page: 1 }
          }
        ]);
      });
    });

    describe("getRecruits failed call", () => {
      const error = {
        response: {
          status: 422
        }
      };

      beforeEach(() => {
        mockAxios.get.mockRejectedValue(error);

        mockStore = getMockStore({});
        mockStore.dispatch(getRecruits(2, "org"));
      });

      it("dispatches the correct actions", () => {
        expect(mockStore.getActions()).toEqual([
          { type: GET_RECRUITS_REQUEST, payload: false },
          { type: GET_RECRUITS_FAILURE },
          { type: INVALID_REQUEST, responseCode: 422 }
        ]);
      });
    });
  });
});
