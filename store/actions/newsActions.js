/* eslint-disable no-param-reassign */
import axios from "axios";
import { setErrors } from "./errorActions";
import {
  GET_NEWS_CATEGORIES,
  SET_CURRENT_ARTICLE,
  GET_NEWS_ARTICLES,
  SET_ARTICLE_GAMES,
  SET_RECOMMENDATIONS_NEWS,
  SET_SUBMITTING_NEW_ARTICLE_STATE,
  GET_OWNED_NEWS_ARTICLES_FAILURE,
  GET_OWNED_NEWS_ARTICLES_REQUEST,
  GET_OWNED_NEWS_ARTICLES_SUCCESS,
  GET_NEWS_ARTICLES_FAILURE,
  GET_NEWS_ARTICLES_REQUEST,
  GET_NEWS_ARTICLES_SUCCESS,
  INVALID_REQUEST
} from "./types";

import { toggleLoader } from "./loaderActions";
import { sendNotification } from "../../helpers/FlashHelper";
import { getPaginatedCollection } from "./commonActions";

/**
 @module newsActions
 @category Actions
 */

export const getCategories = (isPublic = false) => dispatch => {
  axios
    .get(`${isPublic ? "/public" : ""}/learning/categories`)
    .then(response => {
      const { categories } = response.data;
      dispatch({
        type: GET_NEWS_CATEGORIES,
        payload: categories
      });
    })
    .catch(error => {
      dispatch(setErrors(error.response));
    });
};

export const createNewsArticle = (formData, router) => dispatch => {
  dispatch(toggleLoader(true));

  dispatch({
    type: SET_SUBMITTING_NEW_ARTICLE_STATE,
    payload: true
  });
  axios({
    method: "POST",
    config: { headers: { "Content-Type": "multipart/form-data" } },
    url: "/learning",
    data: formData
  })
    .then(response => {
      const { url } = response.data;
      analytics.track("NEWS_ARTICLE_CREATE");
      sendNotification("Your news article has been created.", "success", "Success");
      dispatch(toggleLoader(false));
      router.push(url);
    })
    .catch(error => {
      dispatch({
        type: SET_SUBMITTING_NEW_ARTICLE_STATE,
        payload: false
      });
      dispatch(setErrors(error.response ? error.response.data : error));
      dispatch(toggleLoader(false));
    });
};

export const updateNewsArticleGames = (articleId, games) => dispatch => {
  axios
    .put(`/learning/${articleId}`, { games })
    .then(response => {
      dispatch({
        type: SET_ARTICLE_GAMES,
        payload: games
      });

      analytics.track("NEWS_ARTICLE_EDIT");

      const { message } = response.data;
      sendNotification(message, "success", "Success");
    })
    .catch(error => {
      dispatch(setErrors(error.response.data));
    });
};

export const updateNewsArticle = (id, formData) => dispatch => {
  dispatch(toggleLoader(true));
  axios
    .put(`/learning/${id}`, formData)
    .then(response => {
      analytics.track("NEWS_ARTICLE_EDIT");
      const { message } = response.data;
      sendNotification(message, "success", "Success");
      dispatch(getArticleById(id));
      dispatch(toggleLoader(false));
    })
    .catch(error => {
      dispatch(setErrors(error.response.data));
      dispatch(toggleLoader(false));
      dispatch(getArticleById(id));
    });
};

export const getArticle = (slug, category, isPublic = false) => dispatch => {
  // Send category if article.slug are ever made non-unique.
  // Sending category now is redundent since slugs must be unique.
  // const filters = { slug, category };
  const filters = { slug };

  axios({
    method: "GET",
    config: { headers: { "Content-Type": "multipart/form-data" } },
    url: `${isPublic ? "/public" : ""}/learning?&filters=${JSON.stringify(filters)}`
  })
    .then(response => {
      const { articles } = response.data;
      dispatch({
        type: SET_CURRENT_ARTICLE,
        payload: articles.docs[0]
      });
    })
    .catch(error => {
      dispatch(setErrors(error.response));
    });
};

export const setArticleSSR = article => dispatch => {
  dispatch({
    type: SET_CURRENT_ARTICLE,
    payload: article
  });
};

export const getArticleById = id => dispatch => {
  // Send category if article.slug are ever made non-unique.
  // Sending category now is redundent since slugs must be unique.
  // const filters = { slug, category };
  const filters = { _id: id };

  axios({
    method: "GET",
    config: { headers: { "Content-Type": "multipart/form-data" } },
    url: `/learning?&filters=${JSON.stringify(filters)}`
  })
    .then(response => {
      const { articles } = response.data;
      dispatch({
        type: SET_CURRENT_ARTICLE,
        payload: articles.docs[0]
      });
    })
    .catch(error => {
      dispatch(setErrors(error.response));
    });
};

export const getLearningArticles = (
  page = 1,
  pageSize = 9,
  filters = {},
  displayLoader = true,
  useRootPath = true,
  isPublic = false
) => dispatch => {
  filters.status = "Published";
  dispatch(
    getPaginatedCollection(
      page,
      pageSize,
      filters,
      displayLoader,
      `${isPublic ? "/public" : ""}/learning`,
      "articles",
      GET_NEWS_ARTICLES,
      toggleLoader,
      setErrors,
      "",
      useRootPath
    )
  );
};

export const getNewsArticlesNew = (isPublic, page, pageSize = 9, filters = {}) => dispatch => {
  filters.status = "Published";
  dispatch({ type: GET_NEWS_ARTICLES_REQUEST, payload: page === 1 });
  axios
    .get(`${isPublic ? "/public" : ""}/learning`, {
      params: { page, pageSize, limit: pageSize + 1, filters }
    })
    .then(response => {
      const { docs, ...rest } = response.data.articles;
      dispatch({
        type: GET_NEWS_ARTICLES_SUCCESS,
        page,
        payload: response.data.articles.docs,
        pagination: { ...rest }
      });
    })
    .catch(error => {
      dispatch({ type: GET_NEWS_ARTICLES_FAILURE });
      if (error.response.status === 422 || error.response.status === 400) {
        dispatch({
          type: INVALID_REQUEST,
          responseCode: error.response.status
        });
      }
      setErrors(error.response ? error.response.data : error);
    });
};

export const getOwnedArticles = page => dispatch => {
  dispatch({ type: GET_OWNED_NEWS_ARTICLES_REQUEST, payload: page === 1 });
  axios
    .get("/learning/owned", { params: { page, pageSize: 9, limit: 10 } })
    .then(response => {
      const { docs, ...rest } = response.data.articles;
      dispatch({
        type: GET_OWNED_NEWS_ARTICLES_SUCCESS,
        page,
        payload: docs,
        pagination: rest
      });
    })
    .catch(error => {
      dispatch({ type: GET_OWNED_NEWS_ARTICLES_FAILURE });
      if (error.response.status === 422 || error.response.status === 400) {
        dispatch({
          type: INVALID_REQUEST,
          responseCode: error.response.status
        });
      }
      setErrors(error.response ? error.response.data : error);
    });
};

export const getNewsArticlesByCategory = (
  page = 1,
  pageSize = 9,
  filters = {},
  displayLoader = true,
  useRootPath = false
) => dispatch => {
  filters.status = "Published";
  dispatch(
    getPaginatedCollection(
      page,
      pageSize,
      filters,
      displayLoader,
      "/learning",
      "articles",
      GET_NEWS_ARTICLES,
      toggleLoader,
      setErrors,
      "",
      useRootPath
    )
  );
};

export const getRecommendedNewsArticles = (amount = 3, isPublic = false) => dispatch => {
  const url = "recommendations/learning";
  const apiUrl = isPublic ? `public/${url}` : url;
  axios
    .get(apiUrl, {
      params: {
        amount
      }
    })
    .then(response => {
      dispatch({
        type: SET_RECOMMENDATIONS_NEWS,
        payload: response.data.articles
      });
    })
    .catch(error => {
      dispatch(setErrors(error.response ? error.response.data : error));
    });
};
