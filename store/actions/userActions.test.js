import mockAxios from "axios";
import { getMockStore } from "../../common/testUtils";
import { createUser, updatePublicTraitsAndMotivations } from "./userActions";
import { UPDATE_PUBLIC_TRAITS_MOTIVATIONS } from "./types";

describe("userActions", () => {
  describe("createUser", () => {
    it("creates a user in the backend with referral code when available", async () => {
      const mockStore = getMockStore();

      mockAxios.post.mockResolvedValue({
        data: { token: "some token", user: { username: "myname" } }
      });

      const userData = {
        user: {
          firstName: "carlos",
          lastName: "carlone",
          email: "carlone@email.com",
          password: "dontStealDis"
        }
      };
      const url = "/createMyUser";
      const router = {
        query: { referralCode: "555", refSource: "seagull" }
      };

      await mockStore.dispatch(createUser(userData, url, router));

      expect(mockAxios.post).toHaveBeenCalledWith(url, {
        ...userData,
        referral: {
          referralCode: "555",
          refSource: "seagull"
        },
        platform: "web"
      });
    });
  });

  it("updatePublicTraitsAndMotivations", async () => {
    const mockStore = getMockStore();

    mockAxios.patch.mockResolvedValue({
      data: { publicTraitsMotivations: true }
    });

    await mockStore.dispatch(updatePublicTraitsAndMotivations(true));

    expect(mockAxios.patch).toHaveBeenCalledWith("users/public_traits_motivations", {
      publicTraitsMotivations: true
    });

    expect(mockStore.getActions()).toEqual([{ type: UPDATE_PUBLIC_TRAITS_MOTIVATIONS, payload: true }]);
  });
});
