import axios from "axios";

import { initializeApollo } from "../../config/apolloClient";

import { setErrors } from "./errorActions";
import {
  GET_ENGAGEMENTS_LEADERBOARD,
  GET_LEADERBOARDS,
  GET_LEADERBOARDS_TOP,
  GET_STREAK_LEADERBOARD,
  GET_VIEWS_LEADERBOARD,
  GET_LEADERBOARD_DATA,
  GET_VALORANT_LEADERBOARD_DATA,
  GET_AIMLAB_LEADERBOARD_DATA,
  GET_LEAGUE_OF_LEGENDS_LEADERBOARD_DATA,
  GET_USER_LEADERBOARD_DATA,
  SET_LEADERBOARD_DATA
} from "./types";
/* eslint-disable */
import {
  GET_PAGINATED_VALORANT_LEADERBOARD,
  GET_PAGINATED_AIMLAB_LEADERBOARD,
  GET_PAGINATED_LEAGUE_OF_LEGENDS_LEADERBOARD
} from "../../graphql/LeaderboardQuery";
import {
  GET_AIMLAB_LEADERBOARD_FOR_USER,
  GET_LEAGUE_OF_LEGENDS_LEADERBOARD_FOR_USER,
  GET_VALORANT_LEADERBOARD_FOR_USER
} from "../../graphql/UserQuery";
/* eslint-enable */
const apolloClient = initializeApollo();
/**
 @module leaderboardActions
 @category Actions
 */

const LeaderboardEnum = Object.freeze({ aimlab: "aimlab", leagueOfLegends: "leagueoflegends", valorant: "valorant" });

export const getLeaderboards = (page = 1, pageSize = 10) => dispatch => {
  axios
    .get(`/leaderboards?page=${page}&limit=${pageSize}`)
    .then(response => {
      dispatch({
        type: GET_LEADERBOARDS,
        payload: response.data
      });
    })
    .catch(error => {
      dispatch(setErrors(error.response ? error.response.data : error));
    });
};

export const getViewsLeaderboard = (page = 1, pageSize = 10) => dispatch => {
  axios
    .get(`/leaderboards/views?page=${page}&limit=${pageSize}`)
    .then(response => {
      dispatch({
        type: GET_VIEWS_LEADERBOARD,
        payload: response.data
      });
    })
    .catch(error => {
      setErrors(error);
    });
};

export const getStreakLeaderboard = (page = 1, pageSize = 10) => dispatch => {
  axios
    .get(`/leaderboards/streaks?page=${page}&limit=${pageSize}`)
    .then(response => {
      dispatch({
        type: GET_STREAK_LEADERBOARD,
        payload: response.data
      });
    })
    .catch(error => {
      setErrors(error);
    });
};

export const getEngagementsLeaderboard = (page = 1, pageSize = 10) => dispatch => {
  axios
    .get(`/leaderboards/engagements?page=${page}&limit=${pageSize}`)
    .then(response => {
      dispatch({
        type: GET_ENGAGEMENTS_LEADERBOARD,
        payload: response.data
      });
    })
    .catch(error => {
      setErrors(error);
    });
};

export const getLeaderboardsTopThree = () => dispatch => {
  axios
    .get("/leaderboards/top")
    .then(response => {
      dispatch({
        type: GET_LEADERBOARDS_TOP,
        payload: response.data
      });
    })
    .catch(error => {
      setErrors(error);
    });
};

export const getCurrentUserLeaderboardData = (game, userId) => async dispatch => {
  try {
    dispatch({ type: GET_USER_LEADERBOARD_DATA, payload: {} });
    if (game === LeaderboardEnum.aimlab) {
      const { loading, error, data } = await apolloClient.query({
        query: GET_AIMLAB_LEADERBOARD_FOR_USER,
        variables: { id: userId }
      });

      dispatch({
        type: GET_USER_LEADERBOARD_DATA,
        // doing this to make data retrieval easier in webpage
        payload: { loading, error, data: data?.getUserById?.pipelineLeaderboardRanks?.aimlabLeaderboardRank }
      });
    } else if (game === LeaderboardEnum.leagueOfLegends) {
      const { loading, error, data } = await apolloClient.query({
        query: GET_LEAGUE_OF_LEGENDS_LEADERBOARD_FOR_USER,
        variables: { id: userId }
      });

      dispatch({
        type: GET_USER_LEADERBOARD_DATA,
        // doing this to make data retrieval easier in webpage
        payload: { loading, error, data: data?.getUserById?.pipelineLeaderboardRanks?.leagueOfLegendsLeaderboardRank }
      });
    } else if (game === LeaderboardEnum.valorant) {
      const { loading, error, data } = await apolloClient.query({
        query: GET_VALORANT_LEADERBOARD_FOR_USER,
        variables: { id: userId }
      });
      // eslint-disable-next-line no-console
      console.log(data);

      dispatch({
        type: GET_USER_LEADERBOARD_DATA,
        // doing this to make data retrieval easier in webpage
        payload: { loading, error, data: data?.getUserById?.pipelineLeaderboardRanks?.valorantLeaderboardRank }
      });
    } else {
      dispatch({ type: GET_USER_LEADERBOARD_DATA, payload: {} });
    }
  } catch (err) {
    dispatch({ type: GET_USER_LEADERBOARD_DATA, payload: {} });
  }
};

export const getLeaderboardData = (game, page, limit) => async dispatch => {
  try {
    if (game === LeaderboardEnum.aimlab) {
      const { loading, error, data } = await apolloClient.query({
        query: GET_PAGINATED_AIMLAB_LEADERBOARD,
        variables: { page, limit }
      });

      dispatch({
        type: GET_AIMLAB_LEADERBOARD_DATA,
        // doing this to make data retrieval easier in webpage
        payload: { loading, error, data: data?.getPaginatedAimlabLeaderboard }
      });
    } else if (game === LeaderboardEnum.leagueOfLegends) {
      const { loading, error, data } = await apolloClient.query({
        query: GET_PAGINATED_LEAGUE_OF_LEGENDS_LEADERBOARD,
        variables: { page, limit }
      });

      dispatch({
        type: GET_LEAGUE_OF_LEGENDS_LEADERBOARD_DATA,
        // doing this to make data retrieval easier in webpage
        payload: { loading, error, data: data?.getPaginatedLeagueOfLegendsLeaderboard }
      });
    } else if (game === LeaderboardEnum.valorant) {
      const { loading, error, data } = await apolloClient.query({
        query: GET_PAGINATED_VALORANT_LEADERBOARD,
        variables: { page, limit }
      });

      dispatch({
        type: GET_VALORANT_LEADERBOARD_DATA,
        // doing this to make data retrieval easier in webpage
        payload: { loading, error, data: data?.getPaginatedValorantLeaderboard }
      });
    } else {
      const response = await axios.get(`/public/leaderboard/${game}`);

      dispatch({ type: GET_LEADERBOARD_DATA, payload: response.data });
    }
  } catch (err) {
    dispatch({ type: GET_LEADERBOARD_DATA, payload: [] });
  }
};

export const setLeaderboardData = leaderboardData => dispatch => {
  dispatch({
    type: SET_LEADERBOARD_DATA,
    payload: leaderboardData
  });
};
