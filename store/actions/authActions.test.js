import mockAxios from "axios";
import { getMockStore } from "../../common/testUtils";
import { SET_ERRORS, SET_SIGNUP_FORM_ERROR } from "./types";
import { validateEmailInput, validateUsernameInput } from "./authActions";

describe("authActions", () => {
  let mockStore;
  describe("validateEmailInput", () => {
    describe("validateEmailInput for no email", () => {
      it("dispatches the correct action when email length is zero", () => {
        const email = "";

        mockStore = getMockStore({});
        mockStore.dispatch(validateEmailInput(email));

        expect(mockStore.getActions()).toEqual([
          {
            type: SET_SIGNUP_FORM_ERROR,
            payload: { email: "Email is required" }
          }
        ]);
      });
    });

    describe("validateEmailInput successful call with valid email", () => {
      const email = "abc123@efuse.gg";
      const response = {
        data: {
          valid: true
        }
      };

      beforeEach(() => {
        mockAxios.post.mockResolvedValue(response);

        mockStore = getMockStore({});
        mockStore.dispatch(validateEmailInput(email));
      });

      it("makes the correct network call", () => {
        expect(mockAxios.post).toHaveBeenCalledWith("/auth/validate_email", { email: "abc123@efuse.gg" });
      });

      it("dispatches the correct action", () => {
        expect(mockStore.getActions()).toEqual([{ type: SET_SIGNUP_FORM_ERROR, payload: { email: "" } }]);
      });
    });

    describe("validateEmailInput successful call with unavailable email", () => {
      const email = "abc123@efuse.gg";
      const response = {
        data: {
          valid: false,
          error: "No email for you"
        }
      };

      beforeEach(() => {
        mockAxios.post.mockResolvedValue(response);

        mockStore = getMockStore({});
        mockStore.dispatch(validateEmailInput(email));
      });

      it("dispatches the correct action", () => {
        expect(mockStore.getActions()).toEqual([
          { type: SET_SIGNUP_FORM_ERROR, payload: { email: "No email for you" } }
        ]);
      });
    });

    describe("validateEmailInput failed call", () => {
      const email = "abc123@efuse.gg";
      const error = {
        response: {
          data: "This is an error"
        }
      };

      beforeEach(() => {
        mockAxios.post.mockRejectedValue(error);

        mockStore = getMockStore({});
        mockStore.dispatch(validateEmailInput(email));
      });

      it("dispatches the correct actions", () => {
        expect(mockStore.getActions()).toEqual([
          { type: SET_SIGNUP_FORM_ERROR, payload: { email: "Error validating email. Please try again later" } },
          { type: SET_ERRORS }
        ]);
      });
    });
  });

  describe("validateUsernameInput", () => {
    describe("validateUsernameInput for no username", () => {
      it("dispatches the correct action when username length is zero", () => {
        const username = "";

        mockStore = getMockStore({});
        mockStore.dispatch(validateUsernameInput(username));

        expect(mockStore.getActions()).toEqual([
          {
            type: SET_SIGNUP_FORM_ERROR,
            payload: { username: "Username is required" }
          }
        ]);
      });
    });

    describe("validateUsernameInput successful response with valid username", () => {
      const username = "coolestUsernameEvah";
      const response = {
        data: {
          valid: true
        }
      };

      beforeEach(() => {
        mockAxios.post.mockResolvedValue(response);

        mockStore = getMockStore({});
        mockStore.dispatch(validateUsernameInput(username));
      });

      it("makes the correct network call", () => {
        expect(mockAxios.post).toHaveBeenCalledWith("/auth/validate_username", { username: "coolestUsernameEvah" });
      });

      it("dispatches the correct action", () => {
        expect(mockStore.getActions()).toEqual([{ type: SET_SIGNUP_FORM_ERROR, payload: { username: "" } }]);
      });
    });

    describe("validateUsernameInput successful call with invalid username", () => {
      const username = "usernameNotAvailable";
      const response = {
        data: {
          valid: false,
          error: "No username for you"
        }
      };

      beforeEach(() => {
        mockAxios.post.mockResolvedValue(response);

        mockStore = getMockStore({});
        mockStore.dispatch(validateUsernameInput(username));
      });

      it("dispatches the correct action", () => {
        expect(mockStore.getActions()).toEqual([
          { type: SET_SIGNUP_FORM_ERROR, payload: { username: "No username for you" } }
        ]);
      });
    });

    describe("validateUsernameInput failed call", () => {
      const username = "imGoingToFail";
      const error = {
        response: {
          data: "This is an error"
        }
      };

      beforeEach(() => {
        mockAxios.post.mockRejectedValue(error);

        mockStore = getMockStore({});
        mockStore.dispatch(validateUsernameInput(username));
      });

      it("dispatches the correct actions", () => {
        expect(mockStore.getActions()).toEqual([
          { type: SET_SIGNUP_FORM_ERROR, payload: { username: "Error validating username. Please try again later" } },
          { type: SET_ERRORS }
        ]);
      });
    });
  });
});
