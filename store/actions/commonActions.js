import axios from "axios";
import Router from "next/router";

import { setFlash } from "../../helpers/FlashHelper";
import { jsonStringifier } from "../../helpers/FilterHelper";
import { getPathname } from "../../helpers/GeneralHelper";
import { toggleLoader, toggleContentLoader, toggleChatLoader } from "./loaderActions";
import { setErrors } from "./errorActions";
import { INVALID_REQUEST } from "./types";
/**
 @module commonActions
 @category Actions
 */

export const getPaginatedCollection = (
  page = 1,
  pageSize = 9,
  filters = {},
  displayLoader = true,
  rootPath,
  key,
  type,
  toggleLoader,
  setErrors,
  subKey = "",
  useRootPath = false,
  inlineLoader = false,
  query = "",
  chatLoader = false,
  id = "",
  customPath = "",
  customType = "",
  limit = "",
  reset = false
) => dispatch => {
  if (displayLoader) dispatch(toggleLoader(true));
  if (inlineLoader) dispatch(toggleContentLoader(true, key));
  if (chatLoader) dispatch(toggleChatLoader(true));
  if (!subKey) subKey = key;
  let endPoint = useRootPath ? rootPath : getPathname() || rootPath;
  if (customPath) {
    endPoint = customPath;
  }
  let applyLimit = "";
  if (limit) {
    applyLimit = `&limit=${limit}`;
  }

  axios
    .get(
      `${endPoint}?page=${page}&pageSize=${pageSize}&filters=${JSON.stringify(
        filters
      )}&query=${query}&id=${id}${applyLimit}`
    )
    .then(response => {
      if (response.data.canAccess === false) {
        setFlash("admin");
        Router.push("/home");
      } else {
        const { docs, ...rest } = response.data[key];
        dispatch({
          type: customType || type,
          [subKey]: docs,
          pagination: { ...rest },
          reset
        });
        if (displayLoader) dispatch(toggleLoader(false));
        if (inlineLoader) dispatch(toggleContentLoader(false, key));
        if (chatLoader) dispatch(toggleChatLoader(false));
      }
    })
    .catch(error => {
      if (error.response && (error.response.status === 422 || error.response.status === 400)) {
        dispatch({
          type: INVALID_REQUEST,
          responseCode: error.response.status
        });
      }
      dispatch(setErrors(error.response ? error.response.data : error));
      if (displayLoader) dispatch(toggleLoader(false));
      if (inlineLoader) dispatch(toggleContentLoader(false, key));
      if (chatLoader) dispatch(toggleChatLoader(false));
    });
};

export const search = (endpoint, searchObject, filters, type, key, updateFilters, limit) => dispatch => {
  let applyLimit = "";
  if (limit) {
    applyLimit = `&limit=${limit}`;
  }
  dispatch(toggleLoader(true));
  axios
    .get(`${endpoint}?page=1&pageSize=9&filters=${jsonStringifier(searchObject)}${applyLimit}`)
    .then(response => {
      const { docs, ...rest } = response.data[key];
      dispatch({
        type,
        payload: docs,
        pagination: { ...rest },
        searchObject
      });
      dispatch(updateFilters(filters));
      dispatch(toggleLoader(false));
    })
    .catch(error => {
      dispatch(setErrors(error.response.data));
      dispatch(toggleLoader(false));
    });
};
