import axios from "axios";
import getConfig from "next/config";
import { isUserLoggedIn } from "../../helpers/AuthHelper";
import { sendNotification, setFlash } from "../../helpers/FlashHelper";
import { adminRedirectionPath } from "../../helpers/GeneralHelper";
import {
  GET_ORGANIZATIONS,
  GET_ORGANIZATIONS_FAILURE,
  GET_ORGANIZATIONS_REQUEST,
  GET_ORGANIZATIONS_SUCCESS,
  ORG_SEARCH_BY_NAME,
  GET_ORGANIZATION_SUCCESS,
  SET_RECENT_ORGANIZATIONS,
  GET_ADMIN_ORGANIZATIONS,
  UNJOIN_MEMBER,
  JOIN_MEMBER,
  UNJOIN_ORGANIZATION_MEMBER,
  GET_MEMBERS,
  CHANGE_OWNER,
  JOIN_MEMBER_REQUEST,
  GET_ORGANIZATION_REQUESTS_REQUEST,
  GET_ORGANIZATION_REQUESTS_SUCCESS,
  GET_ORGANIZATION_REQUESTS_FAILURE,
  APPROVE_ORG_JOIN_REQUEST,
  REJECT_ORG_JOIN_REQUEST,
  MAKE_CAPTAIN,
  REMOVE_CAPTAIN,
  KICK_ORGANIZATION_MEMBER_SUCCESS,
  BAN_MEMBER,
  UNBAN_MEMBER,
  ORGANIZATION_CURRENT_PAGE,
  CLEAR_ORGANIZATIONS,
  TOGGLE_MODAL_SHOW,
  INVITATION,
  ADD_ORGANIZATION,
  CLEAR_MEMBERS,
  CLEAR_ORGANIZATION,
  ORGANIZATION_SECTION_UPDATED,
  ORGANIZATION_GAMES,
  DELETE_ORGANIZATION,
  CHANGE_ORGANIZATION_STATUS,
  TOGGLE_IN_PROGRESS,
  ORGANIZATION_SLUG_ERROR,
  CHANGE_SINGLE_ORGANIZATION_PUBLISH_STATUS,
  MOCK_EXTERNAL_ORGANIZATION_VIEW,
  SET_ORGANIZATION_LOADING,
  SET_RECOMMENDATIONS_ORGANIZATIONS,
  INVALID_REQUEST,
  TOGGLE_ORGANIZATION_MODAL,
  GET_ORGANIZATION_MEMBERS_REQUEST,
  GET_ORGANIZATION_MEMBERS_SUCCESS,
  GET_ORGANIZATION_MEMBERS_FAILURE,
  KICK_ORGANIZATION_MEMBER_FAILURE,
  UPDATE_ORGANIZATION_MEMBER_TITLE_SUCCESS,
  UPDATE_ORGANIZATION_MEMBER_TITLE_FAILURE,
  GET_ORGANIZATION_FAILURE,
  SET_ORGANIZATIONS
} from "./types";
import { toggleLoader, toggleContentLoader } from "./loaderActions";
import { setErrors } from "./errorActions";
import { getPaginatedCollection } from "./commonActions";
import { shareLinkModal } from "./shareAction";
import { SHARE_LINK_KINDS } from "../../components/ShareLinkModal/ShareLinkModal";
import { ADD_GAMES_TO_ORGANIZATION } from "../../graphql/OrganizationGamesQuery";
import { initializeApollo } from "../../config/apolloClient";

const { publicRuntimeConfig } = getConfig();
const { feBaseUrl } = publicRuntimeConfig;
const apolloClient = initializeApollo();

/**
 @module organizationActions
 @category Actions
 */

export const toggleOrganizationModal = (modal, modalSection = "") => dispatch =>
  dispatch({ type: TOGGLE_ORGANIZATION_MODAL, modal, modalSection });

export const getAdminOrganizations = url => dispatch => {
  const promise = new Promise((resolve, reject) => {
    axios
      .get(url)
      .then(res => {
        dispatch(toggleLoader(false));
        return resolve(res.data.organizations);
      })
      .catch(error => {
        dispatch(setErrors(error.response.data));
        dispatch(toggleLoader(false));
        return reject();
      });
  });
  return promise;
};

export const createOrganization = (formData, router) => dispatch => {
  dispatch(toggleLoader(true));
  dispatch({ type: SET_ORGANIZATION_LOADING, payload: { loading: true } });

  axios({
    method: "POST",
    config: { headers: { "Content-Type": "multipart/form-data" } },
    url: "/organizations",
    data: formData
  })
    .then(response => {
      const { message, organization } = response.data;
      analytics.track("ORGANIZATION_CREATE", { formVersion: 1 });

      dispatch({ type: ADD_ORGANIZATION, organization });
      sendNotification(message, "success", "Success");

      dispatch(toggleLoader(false));

      dispatch({ type: SET_ORGANIZATION_LOADING, payload: { loading: false } });
      router.push(`/organizations/dashboard?id=${organization._id}`);

      dispatch(
        shareLinkModal(
          true,
          "Share Organization",
          `${feBaseUrl}/org/${organization.shortName}`,
          SHARE_LINK_KINDS.ORGANIZATION
        )
      );
    })
    .catch(error => {
      dispatch({ type: SET_ORGANIZATION_LOADING, payload: { loading: false } });
      dispatch(setErrors(error.response.data));
      dispatch(toggleLoader(false));
    });
};

export const searchByName = name => dispatch => {
  axios
    .get(`/public/organizations/search_by_name?name=${name}`)
    .then(response => {
      let { organizations } = response.data;
      if (organizations.length > 0) {
        organizations = organizations.map(org => ({
          label: org.name,
          value: org._id
        }));
      }
      dispatch({
        type: ORG_SEARCH_BY_NAME,
        payload: organizations
      });
    })
    .catch(errors => {
      dispatch(setErrors(errors.response.data));
    });
};

export const getOrganizationById = id => dispatch => {
  axios
    .get(`${isUserLoggedIn() ? "" : "/public"}/organizations/show/${id}`)
    .then(response => {
      dispatch({
        type: GET_ORGANIZATION_SUCCESS,
        payload: response.data.organization
      });
      dispatch(toggleLoader(false));
    })
    .catch(error => {
      dispatch({ type: GET_ORGANIZATION_FAILURE });
      if (error.response && (error.response.status === 422 || error.response.status === 400)) {
        dispatch({
          type: INVALID_REQUEST,
          responseCode: error.response?.status
        });
      }
      dispatch(setErrors(error?.response?.data));
      dispatch(toggleLoader(false));
    });
};

export const getOrganizationBySlug = slug => dispatch => {
  dispatch(toggleLoader(true));
  axios
    .get(`${isUserLoggedIn() ? "" : "/public"}/organizations/slug/${slug}`)
    .then(response => {
      dispatch({
        type: GET_ORGANIZATION_SUCCESS,
        payload: response.data.organization
      });
      dispatch(toggleLoader(false));
    })
    .catch(error => {
      dispatch(setErrors(error.response ? error.response.data : error));
      dispatch(toggleLoader(false));
    });
};

// Currently used for setting data from SSR getInitialProps()
export const getOrganizationFromSSR = organization => dispatch => {
  dispatch({
    type: GET_ORGANIZATION_SUCCESS,
    payload: organization
  });
};

export const getOrganizationToEdit = (id, router) => dispatch => {
  dispatch(toggleLoader(true));
  dispatch(clearOrganization());
  axios
    .get(`/organizations/edit/${id}`)
    .then(response => {
      const { canEdit, organization } = response.data;
      if (canEdit) {
        dispatch({ type: GET_ORGANIZATION_SUCCESS, payload: organization });
        dispatch(toggleLoader(false));
      } else {
        setFlash("admin");
        dispatch(toggleLoader(false));
        router.push("/organizations");
      }
    })
    .catch(error => {
      dispatch({ type: SET_ORGANIZATION_LOADING, payload: { loading: false } });
      dispatch(setErrors(error.response.data));
      dispatch(toggleLoader(false));
    });
};

export const updateOrganization = (orgId, formData, router, fromAdmin = false) => dispatch => {
  dispatch(toggleLoader(true));
  dispatch({ type: SET_ORGANIZATION_LOADING, payload: { loading: true } });
  axios
    .put(`/organizations/update/${orgId}`, formData)
    .then(response => {
      dispatch({ type: SET_ORGANIZATION_LOADING, payload: { loading: false } });
      const { canEdit, id, message } = response.data;
      if (canEdit) {
        sendNotification(message, "success", "Success");
        dispatch(toggleLoader(false));
        router.push(organizationsPathForAdmin(fromAdmin, `/organizations/show?id=${id}`));
      } else {
        setFlash("admin");
        dispatch(toggleLoader(false));
        router.push(organizationsPathForAdmin(fromAdmin, "/organizations"));
      }
    })
    .catch(error => {
      dispatch({ type: SET_ORGANIZATION_LOADING, payload: { loading: false } });
      dispatch(setErrors(error.response.data));
      dispatch(toggleLoader(false));
    });
};

export const getOrganizations = (
  page = 1,
  pageSize = 9,
  filters = {},
  displayLoader = true,
  useRootPath = false,
  customPath = ""
) => dispatch => {
  dispatch(
    getPaginatedCollection(
      page,
      pageSize,
      filters,
      displayLoader,
      "/admin/organizations",
      "organizations",
      useRootPath ? GET_ADMIN_ORGANIZATIONS : GET_ORGANIZATIONS,
      toggleLoader,
      setErrors,
      "",
      useRootPath,
      "",
      "",
      "",
      "",
      customPath
    )
  );
};

export const getOrganizationsNew = (
  page = 1,
  pageSize = 9,
  filters = {},
  url = `${isUserLoggedIn() ? "" : "/public"}/organizations`
) => dispatch => {
  dispatch({ type: GET_ORGANIZATIONS_REQUEST, payload: page === 1 });
  axios
    .get(url, { params: { page, pageSize, filters } })
    .then(response => {
      dispatch({
        type: GET_ORGANIZATIONS_SUCCESS,
        page,
        payload: response.data.organizations.docs,
        hasNextPage: response.data.organizations.hasNextPage
      });
    })
    .catch(error => {
      dispatch({ type: GET_ORGANIZATIONS_FAILURE });
      if (error.response.status === 422 || error.response.status === 400) {
        dispatch({
          type: INVALID_REQUEST,
          responseCode: error.response.status
        });
      }
      setErrors(error.response ? error.response.data : error);
    });
};

export const getRecentOrganizations = () => dispatch => {
  dispatch(toggleContentLoader(true, "recentOrganizations"));
  axios
    .get(`/organizations/recent?pageSize=${5}&filters={}`)
    .then(response => {
      dispatch({
        type: SET_RECENT_ORGANIZATIONS,
        payload: response.data.organizations.docs
      });
      dispatch(toggleContentLoader(false, "recentOrganizations"));
    })
    .catch(error => {
      dispatch(setErrors(error.response.data));
      dispatch(toggleContentLoader(false, "recentOrganizations"));
    });
};

// Delete a Organization record.
export const deleteOrganization = (url, UpdateGrid) => dispatch => {
  axios
    .delete(url)
    .then(() => {
      UpdateGrid();
    })
    .catch(error => {
      dispatch(setErrors(error.response));
      dispatch(toggleLoader(false));
    });
};

// TODO: tech debt - convert to proper redux thunk action
export const verifyOrganization = (url, userId) => dispatch => {
  const promise = new Promise((resolve, reject) => {
    axios
      .put(url, { user: userId })
      .then(response => {
        const { message } = response.data;
        sendNotification(message, "success", "Success");
        return resolve(response.data);
      })
      .catch(error => {
        dispatch(setErrors(error.response));
        dispatch(toggleLoader(false));
        return reject();
      });
  });
  return promise;
};

export const joinOrganization = (url, status, inviteCode = null) => dispatch => {
  axios
    .put(url, { inviteCode })
    .then(response => {
      const { message, flashType } = response.data;
      if (status === "Open") {
        analytics.track("ORGANIZATION_MEMBER_JOIN");
        dispatch({
          type: JOIN_MEMBER,
          payload: response.data
        });
        sendNotification(message, flashType, "Alert");
      } else {
        analytics.track("ORGANIZATION_MEMBER_REQUEST");
        dispatch({
          type: JOIN_MEMBER_REQUEST,
          payload: response.data.organizationRequest
        });
        sendNotification(response.data.message, response.data.flashType, "Alert");
      }
    })
    .catch(error => {
      dispatch(setErrors(error.response));
      dispatch(toggleLoader(false));
    });
};

export const leaveOrganization = (id, isShow) => dispatch => {
  axios
    .delete(`organizations/leave_organization/${id}`, {})
    .then(response => {
      const { message, flashType } = response.data;
      if (isShow) {
        dispatch({
          type: UNJOIN_ORGANIZATION_MEMBER,
          payload: response.data.members
        });
        sendNotification(message, flashType, "Alert");
      } else {
        dispatch({
          type: UNJOIN_MEMBER,
          payload: response.data
        });
        sendNotification(response.data.message, response.data.flashType, "Alert");
      }
    })
    .catch(error => {
      dispatch(setErrors(error.response));
      dispatch(toggleLoader(false));
    });
};

// TODO: remove this after replacing all usages with getOrganizationMembers
export const getMembers = (
  page = 1,
  pageSize = 9,
  orgId,
  displayLoader = true,
  useRootPath = true,
  filters = {}
) => dispatch => {
  dispatch(
    getPaginatedCollection(
      page,
      pageSize,
      filters,
      displayLoader,
      `/organizations/members/${orgId}`,
      "members",
      GET_MEMBERS,
      toggleLoader,
      setErrors,
      "members",
      useRootPath,
      false,
      "",
      false
    )
  );
};

export const getOrganizationMembers = (orgId, page) => dispatch => {
  dispatch({ type: GET_ORGANIZATION_MEMBERS_REQUEST, payload: page === 1 });
  axios
    .get(`/organizations/members/${orgId}`, { params: { page, pageSize: 9, limit: 10 } })
    .then(response => {
      dispatch({
        type: GET_ORGANIZATION_MEMBERS_SUCCESS,
        page,
        payload: response.data.members.docs,
        hasNextPage: response.data.members.hasNextPage
      });
    })
    .catch(error => {
      dispatch({ type: GET_ORGANIZATION_MEMBERS_FAILURE });
      if (error.response.status === 422 || error.response.status === 400) {
        dispatch({
          type: INVALID_REQUEST,
          responseCode: error.response.status
        });
      }
      // TODO: figure out appropriate error handling here
      setErrors(error.response ? error.response.data : error);
    });
};

export const changeOwner = (url, usId) => dispatch => {
  axios
    .put(url, { userId: usId })
    .then(response => {
      const { message } = response.data;
      sendNotification(message, "success", "Success");
      dispatch({
        type: CHANGE_OWNER,
        payload: response.data.user
      });
    })
    .catch(error => {
      dispatch(setErrors(error.response));
      dispatch(toggleLoader(false));
    });
};

export const getOrganizationRequests = (page = 1, id = "") => dispatch => {
  dispatch({ type: GET_ORGANIZATION_REQUESTS_REQUEST });
  axios
    .get("/organization_requests", { params: { page, pageSize: 8, id } })
    .then(response => {
      dispatch({
        type: GET_ORGANIZATION_REQUESTS_SUCCESS,
        page,
        payload: response.data.orgRequests.docs,
        hasNextPage: response.data.orgRequests.hasNextPage
      });
    })
    .catch(error => {
      dispatch({ type: GET_ORGANIZATION_REQUESTS_FAILURE });
      if (error.response?.status === 422 || error.response?.status === 400) {
        dispatch({
          type: INVALID_REQUEST,
          responseCode: error.response.status
        });
      }
      setErrors(error.response ? error.response.data : error);
    });
};

export const setCurrentPage = view => dispatch => {
  dispatch({ type: ORGANIZATION_CURRENT_PAGE, view });
};

export const clearOrganizations = () => dispatch => {
  dispatch({ type: CLEAR_ORGANIZATIONS });
};

const organizationsPathForAdmin = (fromAdmin, otherPath) =>
  adminRedirectionPath(fromAdmin, "/admin/opportunities", otherPath);
export const approveOrgRequest = requestId => dispatch => {
  axios
    .put(`organization_requests/approve/${requestId}`)
    .then(response => {
      dispatch({
        type: APPROVE_ORG_JOIN_REQUEST,
        payload: response.data
      });
    })
    .catch(error => {
      dispatch(setErrors(error.response));
    });
};
export const rejectOrgRequest = requestId => dispatch => {
  axios
    .put(`organization_requests/reject/${requestId}`)
    .then(response => {
      dispatch({
        type: REJECT_ORG_JOIN_REQUEST,
        payload: response.data
      });
    })
    .catch(error => {
      dispatch(setErrors(error.response));
    });
};
export const makeCaptain = (data, organizationId) => dispatch => {
  axios
    .put(`organizations/make_captain/${organizationId}`, data)
    .then(response => {
      dispatch({
        type: MAKE_CAPTAIN,
        payload: response.data
      });
      sendNotification(response.data.message, response.data.flashType, "Member Promoted");
    })
    .catch(error => {
      sendNotification(error.response.data.message, error.response.data.flashType, "Captains Limit Exceeded");
      dispatch(setErrors(error.response));
    });
};
export const removeCaptain = (data, organizationId) => dispatch => {
  axios
    .put(`organizations/remove_captain/${organizationId}`, data)
    .then(response => {
      dispatch({
        type: REMOVE_CAPTAIN,
        payload: response.data
      });
      sendNotification(response.data.message, response.data.flashType, "Member Demoted");
    })

    .catch(error => {
      dispatch(setErrors(error.response));
    });
};

export const kickMember = (userId, organizationId) => dispatch => {
  axios
    .delete(`organizations/kick_member/${organizationId}?userId=${userId}`)
    .then(response => {
      dispatch({
        type: KICK_ORGANIZATION_MEMBER_SUCCESS,
        payload: response.data.members
      });
      sendNotification(response.data.message, response.data.flashType, "Member Kicked");
    })
    .catch(error => {
      dispatch({ type: KICK_ORGANIZATION_MEMBER_FAILURE });
      // TODO: figure out appropriate error handling here
      setErrors(error.response);
    });
};

export const banMember = (data, id) => dispatch => {
  axios.put(`organizations/ban_member/${id}`, data).then(response => {
    dispatch({
      type: BAN_MEMBER,
      payload: response.data.bannedMember
    });
    sendNotification(response.data.message, response.data.flashType, "Member Banned");
  });
};

export const unBanMember = (data, id) => dispatch => {
  axios.put(`organizations/unban_member/${id}`, data).then(response => {
    dispatch({
      type: UNBAN_MEMBER,
      payload: response.data.unBannedMember
    });
    sendNotification(response.data.message, response.data.flashType, "Member Unbanned");
  });
};

export const toggleModalShow = orgId => dispatch => {
  dispatch({
    type: TOGGLE_MODAL_SHOW,
    payload: orgId
  });
};

export const sendInvite = data => dispatch => {
  axios.post("organization_invitations", data).then(response => {
    analytics.track("ORGANIZATION_MEMBER_INVITE");

    dispatch({
      type: INVITATION,
      payload: response.data
    });
    sendNotification(response.data.message, response.data.flashType, "Alert");
  });
};

export const clearMembers = () => dispatch => {
  dispatch({ type: CLEAR_MEMBERS });
};

export const clearOrganization = () => dispatch => {
  dispatch({ type: CLEAR_ORGANIZATION });
};

export const updateAboutSection = (organizationId, about) => dispatch => {
  axios
    .patch(`organizations/${organizationId}/about`, { about })
    .then(response => {
      const { success, organization } = response.data;
      if (success) {
        dispatch({ type: ORGANIZATION_SECTION_UPDATED, organization });
        dispatch(toggleOrganizationModal(false));
      } else {
        sendNotification("You are not authorized to perform this action!", "danger", "Alert");
      }
    })
    .catch(error => {
      dispatch(setErrors(error.response.data));
    });
};

export const updateVideoSection = (organizationId, promoVideo) => dispatch => {
  dispatch(toggleInProgress(true));
  axios
    .patch(`organizations/${organizationId}/promo_video`, { promoVideo })
    .then(response => {
      const { success, organization } = response.data;
      if (success) {
        dispatch({ type: ORGANIZATION_SECTION_UPDATED, organization });
        dispatch(toggleInProgress(false));
        dispatch(toggleOrganizationModal(false));
      } else {
        dispatch(toggleInProgress(false));
        sendNotification("You are not authorized to perform this action!", "danger", "Alert");
      }
    })
    .catch(error => {
      dispatch(setErrors(error.response.data));
    });
};
export const updateAccolades = (organizationId, formData) => dispatch => {
  axios({
    method: "PATCH",
    config: { headers: { "Content-Type": "multipart/form-data" } },
    url: `organizations/${organizationId}/accolades`,
    data: formData
  })
    .then(response => {
      manageAccoladeResponse(response, dispatch);
      dispatch(toggleOrganizationModal(false));
    })
    .catch(error => {
      dispatch(setErrors(error.response.data));
    });
};
export const managePlayerCards = (orgId, formData) => dispatch => {
  axios({
    method: "POST",
    config: { headers: { "Content-Type": "multipart/form-data" } },
    url: `organizations/${orgId}/player_card`,
    data: formData
  })
    .then(response => {
      const { organization } = response.data;
      dispatch({ type: ORGANIZATION_SECTION_UPDATED, organization });
      dispatch(toggleOrganizationModal(false));
      sendNotification("Player Card saved successfully.", "success", "Alert");
    })
    .catch(error => {
      dispatch(setErrors(error.response));
    });
};
export const removePlayerCard = (orgId, cardId) => dispatch => {
  axios
    .delete(`/organizations/${orgId}/player_card/${cardId}`)
    .then(response => {
      const { organization } = response.data;
      dispatch({ type: ORGANIZATION_SECTION_UPDATED, organization });
      sendNotification("Player Card successfully removed", "danger", "Alert");
    })
    .catch(error => {
      dispatch(setErrors(error.response));
    });
};

/**
 * @summary Calls API and passes discord server id.
 * @method
 * @param {string} orgId
 * @param {string} discordServer
 */
export const setDiscordServerOrg = (orgId, data) => dispatch => {
  dispatch(toggleLoader(true));

  axios
    .put(`/organizations/${orgId}/discord_server`, data)
    .then(response => {
      const { organization } = response.data;
      dispatch(toggleLoader(false));
      dispatch({ type: ORGANIZATION_SECTION_UPDATED, organization });
    })
    .catch(error => {
      dispatch(setErrors(error.response));
      dispatch(toggleLoader(false));
    });
};

/**
 * @summary Calls API to remove discord server.
 * @param {string} orgId
 */
export const removeDiscordServerOrg = orgId => dispatch => {
  dispatch(toggleLoader(true));

  axios
    .put(`/organizations/${orgId}/remove_discord_server`)
    .then(response => {
      const { organization } = response.data;
      dispatch(toggleLoader(false));
      dispatch({ type: ORGANIZATION_SECTION_UPDATED, organization });
    })
    .catch(error => {
      dispatch(setErrors(error.response));
      dispatch(toggleLoader(false));
    });
};

export const removeAccolades = (organizationId, accoladeKey, accoladeId) => dispatch => {
  axios
    .delete(`/organizations/${organizationId}/accolades/${accoladeKey}/${accoladeId}`)
    .then(response => {
      manageAccoladeResponse(response, dispatch);
    })
    .catch(error => {
      dispatch(setErrors(error.response.data));
    });
};

// this action is used for the organizations browse page
export const changePublishStatus = (url, status) => dispatch => {
  axios.patch(url, status).then(response => {
    const { success, organization } = response.data;
    if (success) {
      dispatch({ type: CHANGE_ORGANIZATION_STATUS, organization });
      sendNotification(
        `Organization public status has been successfully changed to ${organization.publishStatus}`,
        "success",
        "Alert"
      );
    } else {
      sendNotification("You are not authorized to perform this action!", "danger", "Alert");
    }
  });
};

// this action is used for the organization view page. This will update a single organization.
export const changeOrganizationPublishStatus = (url, status) => dispatch => {
  axios.patch(url, status).then(response => {
    const { success, organization } = response.data;
    if (success) {
      dispatch({
        type: CHANGE_SINGLE_ORGANIZATION_PUBLISH_STATUS,
        organization
      });
      sendNotification(
        `Organization public status has been successfully changed to ${organization.publishStatus}`,
        "success",
        "Alert"
      );
    } else {
      sendNotification("You are not authorized to perform this action!", "danger", "Alert");
    }
  });
};

export const toggleInProgress = inProgress => dispatch => dispatch({ type: TOGGLE_IN_PROGRESS, inProgress });

const manageAccoladeResponse = (response, dispatch) => {
  const { success, organization } = response.data;
  if (success) {
    dispatch({ type: ORGANIZATION_SECTION_UPDATED, organization });
    dispatch(toggleOrganizationModal(false));
  } else {
    sendNotification("You are not authorized to perform this action!", "danger", "Alert");
  }
};

export const removeOrganization = orgId => dispatch => {
  axios
    .delete(`organizations/${orgId}`)
    .then(response => {
      analytics.track("ORGANIZATION_DELETE", { organizationId: orgId });
      dispatch({ type: DELETE_ORGANIZATION, payload: response.data });
      sendNotification("Organization has been successfully removed.", "danger", "Alert");
    })
    .catch(error => {
      dispatch(setErrors(error.response));
    });
};

export const toggleOrganizationSlugError = error => dispatch => {
  dispatch({
    type: ORGANIZATION_SLUG_ERROR,
    error
  });
};

export const updateVideoCarousel = (organizationId, formData) => dispatch => {
  axios({
    method: "PATCH",
    config: { headers: { "Content-Type": "multipart/form-data" } },
    url: `organizations/${organizationId}/carousel_video`,
    data: formData
  })
    .then(response => {
      const { organization } = response.data;
      dispatch({ type: ORGANIZATION_SECTION_UPDATED, organization });
      dispatch(toggleOrganizationModal(false));
    })
    .catch(error => {
      dispatch(setErrors(error.response.data));
    });
};

export const removeVideoCarousel = (organizationId, carouselId) => dispatch => {
  axios
    .delete(`organizations/${organizationId}/carousel_video/${carouselId}`)
    .then(response => {
      const { organization } = response.data;
      dispatch({ type: ORGANIZATION_SECTION_UPDATED, organization });
      dispatch(toggleOrganizationModal(false));
    })
    .catch(error => {
      dispatch(setErrors(error.response.data));
    });
};

export const inviteUsersViaEmail = (organizationId, emails, inviteCode) => dispatch => {
  axios
    .post(`organizations/${organizationId}/invitebyemail`, {
      emails,
      inviteCode
    })
    .then(response => {
      const { message } = response.data;
      sendNotification(message, "success", "Success");
    })
    .catch(error => {
      dispatch(setErrors(error.response.data));
    });
};

export const mockExternalOrganizationView = mockExternalOrganization => dispatch =>
  dispatch({ type: MOCK_EXTERNAL_ORGANIZATION_VIEW, mockExternalOrganization });

export const getRecommendedOrganizations = (amount = 3, isPublic = false) => dispatch => {
  const url = "recommendations/organizations";
  const apiUrl = isPublic ? `public/${url}` : url;
  axios
    .get(apiUrl, {
      params: { amount }
    })
    .then(response => {
      dispatch({
        type: SET_RECOMMENDATIONS_ORGANIZATIONS,
        payload: response.data.organizations
      });
    })
    .catch(error => {
      dispatch(setErrors(error.response ? error.response.data : error));
    });
};

export const updateOrganizationMemberTitle = (organizationId, memberId, data) => dispatch => {
  axios
    .patch(`organizations/${organizationId}/members/${memberId}`, data)
    .then(response => {
      dispatch({
        type: UPDATE_ORGANIZATION_MEMBER_TITLE_SUCCESS,
        payload: response.data.member
      });
      getOrganizationMembers(organizationId, 1);
    })
    .catch(error => {
      dispatch({ type: UPDATE_ORGANIZATION_MEMBER_TITLE_FAILURE });
      // TODO: figure out appropriate error handling here sometime
      setErrors(error.response ? error.response.data : error);
    });
};

export const updateOrganizationGames = (organizationId, games) => dispatch => {
  apolloClient
    .mutate({
      mutation: ADD_GAMES_TO_ORGANIZATION,
      variables: {
        addGamesToOrganizationOrganizationId: organizationId,
        addGamesToOrganizationGameIds: games
      }
    })
    .then(response => {
      dispatch({
        type: ORGANIZATION_GAMES,
        games: response.data.AddGamesToOrganization.games
      });
    })
    .catch(error => {
      dispatch(setErrors(error.response.data));
    });
};

export const setOrganizations = organizations => dispatch => {
  dispatch({
    type: SET_ORGANIZATIONS,
    payload: organizations
  });
};
