import isEmpty from "lodash/isEmpty";

import { SET_ERRORS } from "./types";

// eslint-disable-next-line import/prefer-default-export
export const setErrors = errors => {
  let errorMessage = "Something Went Wrong!";

  if (typeof errors === "string") {
    errorMessage = errors;
  } else if (errors?.Session) {
    // eslint-disable-next-line no-console
    console.error("Your Session is Expired");
    errorMessage = null;
  } else if (errors?.token) {
    errorMessage = "Invalid token for email";
  } else if (typeof errors === "object" && !isEmpty(errors)) {
    const errorMessages = Object.entries(errors).map(([, value]) => (value?.length > 0 ? value : null));

    // set the error message to the combined error message if there were any, otherwise leave it
    errorMessage = errorMessages.filter(message => message).join(", ") || errorMessage;

    // Suppress ECONNABORTED Error, caused due to user cancelling outgoing client requests
    if (errorMessage.includes("ECONNABORTED")) {
      errorMessage = null;
    }
  }

  errorNotification(errorMessage).catch(error => console.error("Error dispatching error notification", error));

  return { type: SET_ERRORS };
};

const errorNotification = async errorMessage => {
  const { sendNotification } = await import("../../helpers/FlashHelper");

  if (errorMessage) {
    sendNotification(errorMessage, "danger", "Error");
  }
};
