import axios from "axios";
import { setErrors } from "./errorActions";
import { VALIDATE_EMAIL } from "./types";

/**
 @module validationActions
 @category Actions
 */

export const validateEmail = email => dispatch => {
  axios
    .get(`/auth/validate_email/${email}`)
    .then(res => {
      if (res.data.invalidFormat == true) {
        dispatch({
          type: VALIDATE_EMAIL,
          payload: { emailError: true }
        });
      } else {
        dispatch({
          type: VALIDATE_EMAIL,
          payload: { emailError: false }
        });
      }
    })
    .catch(err => {
      dispatch(setErrors(err.response.data));
    });
};
