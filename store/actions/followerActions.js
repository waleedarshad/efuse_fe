import axios from "axios";

import { setErrors } from "./errorActions";
import {
  IS_FOLLOWED,
  TOGGLE_FOLLOWER_BTN,
  FRIENDS_COUNT,
  INC_FOLLOWER_COUNT,
  DEC_FOLLOWER_COUNT,
  FOLLOW_FROM_OTHER_PROFILE,
  SEARCH_FOLLOWERS_TO_MENTION,
  GET_FOLLOWEE_IDS,
  FOLLOWS_YOU,
  INVALID_REQUEST,
  GET_FOLLOWEES_REQUEST,
  GET_FOLLOWEES_SUCCESS,
  GET_FOLLOWEES_FAILURE,
  GET_FOLLOWERS_REQUEST,
  GET_FOLLOWERS_SUCCESS,
  GET_FOLLOWERS_FAILURE
} from "./types";

export const checkIfFollower = id => dispatch => {
  dispatch(toggleFollowerBtn(true));
  axios
    .get(`/followers/is_followed/${id}`)
    .then(response => {
      dispatch(setIsFollowed(response.data.isFollowed));
      dispatch(toggleFollowerBtn(false));
    })
    .catch(error => {
      if (error.response && (error.response.status === 422 || error.response.status === 400)) {
        dispatch({
          type: INVALID_REQUEST,
          responseCode: error.response.status
        });
      }
      dispatch(setErrors(error));
      dispatch(toggleFollowerBtn(false));
    });
};

// Use this for Follow component INSIDE portfolio section
export const follow = (id, isCurrentUser, key) => dispatch => {
  dispatch(toggleFollowerBtn(true));
  axios
    .get(`/followers/follow/${id}`)
    .then(response => {
      analytics.track("USER_FOLLOW", { id });
      if (isCurrentUser) {
        dispatch({ type: INC_FOLLOWER_COUNT, friend: response.data.friend });
        dispatch(setIsFollowed(response.data.isFollowed));
      } else {
        dispatch({ type: FOLLOW_FROM_OTHER_PROFILE, id, follow: true, key });
      }
      dispatch(toggleFollowerBtn(false));
    })
    .catch(error => {
      dispatch(setErrors(error));
      dispatch(toggleFollowerBtn(false));
    });
};

// Use this for Follow component INSIDE portfolio section
export const unfollow = (id, isCurrentUser, currentUserId, key) => dispatch => {
  dispatch(toggleFollowerBtn(true));
  axios
    .delete(`/followers/unfollow/${id}`)
    .then(response => {
      analytics.track("USER_UNFOLLOW", { id });
      if (isCurrentUser) {
        dispatch({ type: DEC_FOLLOWER_COUNT, follower: currentUserId });
        dispatch(setIsFollowed(response.data.isFollowed));
      } else {
        dispatch({ type: FOLLOW_FROM_OTHER_PROFILE, id, follow: false, key });
      }
      dispatch(toggleFollowerBtn(false));
    })
    .catch(error => {
      dispatch(setErrors(error));
      dispatch(toggleFollowerBtn(false));
    });
};

// Use this for Follow component OUTSIDE portfolio section
// This will follow the user and retrieve a fresh list of current followers
export const followThenRefresh = (id, currentUserId) => dispatch => {
  dispatch(toggleFollowerBtn(true));
  axios
    .get(`/followers/follow/${id}`)
    .then(() => {
      analytics.track("USER_FOLLOW", { id });
      dispatch(getFolloweeIds(currentUserId));
      dispatch(toggleFollowerBtn(false));
    })
    .catch(error => {
      dispatch(setErrors(error));
      dispatch(toggleFollowerBtn(false));
    });
};

// Use this for Follow component OUTSIDE portfolio section
// This will unfollow the user and retrieve a fresh list of current followers
export const unfollowThenRefresh = (id, currentUserId) => dispatch => {
  dispatch(toggleFollowerBtn(true));
  axios
    .delete(`/followers/unfollow/${id}`)
    .then(() => {
      analytics.track("USER_UNFOLLOW", { id });
      dispatch(getFolloweeIds(currentUserId));
      dispatch(toggleFollowerBtn(false));
    })
    .catch(error => {
      dispatch(setErrors(error));
      dispatch(toggleFollowerBtn(false));
    });
};

const setIsFollowed = payload => {
  return { type: IS_FOLLOWED, payload };
};

export const toggleFollowerBtn = disable => dispatch => {
  dispatch({ type: TOGGLE_FOLLOWER_BTN, payload: disable });
};

export const countFriends = (id = "", isPublic = false) => dispatch => {
  axios
    .get(`${isPublic ? "/public" : ""}/followers/count?id=${id}`)
    .then(response => {
      const { followers, followees } = response.data;
      dispatch({ type: FRIENDS_COUNT, followers, followees });
    })
    .catch(error => {
      if (error.response && (error.response.status === 422 || error.response.status === 400)) {
        dispatch({
          type: INVALID_REQUEST,
          responseCode: error.response.status
        });
      }
      dispatch(setErrors(error));
    });
};

export const getFollowers = (id, page = 1) => dispatch => {
  dispatch({ type: GET_FOLLOWERS_REQUEST });
  axios
    .get(`/followers/get_followers/${id}`, { params: { page, pageSize: 12 } })
    .then(response => {
      const { docs, ...rest } = response.data.friends;
      dispatch({ type: GET_FOLLOWERS_SUCCESS, page, payload: docs, pagination: rest });
    })
    .catch(error => {
      dispatch({ type: GET_FOLLOWERS_FAILURE });
      if (error.response.status === 422 || error.response.status === 400) {
        dispatch({ type: INVALID_REQUEST, responseCode: error.response.status });
      }
      setErrors(error.response ? error.response.data : error);
    });
};

export const getFollowees = (id, page = 1) => dispatch => {
  dispatch({ type: GET_FOLLOWEES_REQUEST });
  axios
    .get(`/followers/get_followees/${id}`, { params: { page, pageSize: 12 } })
    .then(response => {
      const { docs, ...rest } = response.data.friends;
      dispatch({ type: GET_FOLLOWEES_SUCCESS, page, payload: docs, pagination: rest });
    })
    .catch(error => {
      dispatch({ type: GET_FOLLOWEES_FAILURE });
      if (error.response.status === 422 || error.response.status === 400) {
        dispatch({ type: INVALID_REQUEST, responseCode: error.response.status });
      }
      setErrors(error.response ? error.response.data : error);
    });
};

export const getFolloweeIds = id => dispatch => {
  axios
    .get(`/followers/get_followees_ids/${id}`)
    .then(response => {
      dispatch({
        type: GET_FOLLOWEE_IDS,
        payload: response.data
      });
    })
    .catch(error => {
      dispatch(setErrors(error));
    });
};

export const globalSearchFollowersToMention = query => dispatch => {
  axios
    .get(`/search/mentions?query=${query}`)
    .then(response => {
      dispatch({
        type: SEARCH_FOLLOWERS_TO_MENTION,
        followers: response.data.followers
      });
    })
    .catch(error => setErrors(error.response.data));
};
// on portfolio profile visit
export const checkIfFollowsYou = id => dispatch => {
  axios
    .get(`/followers/is_follows/${id}`)
    .then(response => {
      dispatch(setFollowsYou(response.data.isFollowsYou));
    })
    .catch(error => {
      if (error.response && (error.response.status === 422 || error.response.status === 400)) {
        dispatch({
          type: INVALID_REQUEST,
          responseCode: error.response.status
        });
      }
      dispatch(setErrors(error));
    });
};
const setFollowsYou = payload => {
  return { type: FOLLOWS_YOU, payload };
};
