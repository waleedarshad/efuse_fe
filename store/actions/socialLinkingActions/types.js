const SOCIAL_LINKING = {
  TWITTER: {
    LINKED: {
      SUCCESS: "GET_TWITTER_LINKED_STATUS_SUCCESS",
      FAILURE: "GET_TWITTER_LINKED_STATUS_FAILURE"
    },
    ACCOUNT_INFO: {
      SUCCESS: "GET_TWITTER_ACCOUNT_INFO_SUCCESS",
      FAILURE: "GET_TWITTER_ACCOUNT_INFO_FAILURE"
    },
    UNLINK: {
      SUCCESS: "UNLINK_TWITTER_ACCOUNT_SUCCESS",
      FAILURE: "UNLINK_TWITTER_ACCOUNT_FAILURE"
    }
  },
  YOUTUBE: {
    LINKED: {
      SUCCESS: "GET_YOUTUBE_LINKED_STATUS_SUCCESS",
      FAILURE: "GET_YOUTUBE_LINKED_STATUS_FAILURE"
    },
    ACCOUNT_INFO: {
      SUCCESS: "GET_YOUTUBE_ACCOUNT_INFO_SUCCESS",
      FAILURE: "GET_YOUTUBE_ACCOUNT_INFO_FAILURE"
    },
    UNLINK: {
      SUCCESS: "UNLINK_YOUTUBE_ACCOUNT_SUCCESS",
      FAILURE: "UNLINK_YOUTUBE_ACCOUNT_FAILURE"
    }
  },
  TWITCH: {
    LINKED: {
      SUCCESS: "GET_TWITCH_LINKED_STATUS_SUCCESS",
      FAILURE: "GET_TWITCH_LINKED_STATUS_FAILURE"
    },
    ACCOUNT_INFO: {
      SUCCESS: "GET_TWITCH_ACCOUNT_INFO_SUCCESS",
      FAILURE: "GET_TWITCH_ACCOUNT_INFO_FAILURE"
    },
    UNLINK: {
      SUCCESS: "UNLINK_TWITCH_ACCOUNT_SUCCESS",
      FAILURE: "UNLINK_TWITCH_ACCOUNT_FAILURE"
    }
  }
};

export default SOCIAL_LINKING;
