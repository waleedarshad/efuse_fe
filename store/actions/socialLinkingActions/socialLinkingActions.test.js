import mockAxios from "axios";
import { getMockStore } from "../../../common/testUtils";
import {
  getServiceUrl,
  getTwitchAccountInfo,
  getTwitchAccountLinkedStatus,
  getTwitterAccountInfo,
  getTwitterAccountLinkedStatus,
  setSocialMediaAccountInfoFromSocket,
  socialMediaLinkingFailure,
  getYouTubeAccountInfo,
  getYouTubeAccountLinkedStatus,
  unlinkOrganizationSocialMediaAccount
} from "./socialLinkingActions";
import { sendNotification } from "../../../helpers/FlashHelper";
import SOCIAL_LINKING from "./types";

jest.mock("../../../helpers/FlashHelper", () => ({
  sendNotification: jest.fn()
}));

global.console = { error: jest.fn() };

describe("socialLinkingActions", () => {
  let mockStore;

  describe("getServiceUrl", () => {
    const response = {
      data: {
        url: "thisisthecoolsociallinkingurl"
      }
    };

    beforeEach(() => {
      mockAxios.get.mockResolvedValue(response);
    });

    it("makes the correct network call when orgId exists", async () => {
      const context = {
        externalAuth: "facebook",
        ownerKind: "organization",
        ownerId: "12345"
      };

      await getServiceUrl(context);

      expect(mockAxios.get).toHaveBeenCalledWith("/external_auth/facebook?orgId=12345");
    });

    it("makes the correct network call when orgId does not exist", async () => {
      const context = {
        externalAuth: "facebook",
        ownerKind: "user",
        ownerId: "12345"
      };
      await getServiceUrl(context);

      expect(mockAxios.get).toHaveBeenCalledWith("/external_auth/facebook");
    });

    it("returns the correct url", async () => {
      const expected = "thisisthecoolsociallinkingurl";
      const actual = await getServiceUrl("facebook");

      expect(actual).toEqual(expected);
    });
  });

  describe("getTwitterAccountLinkedStatus", () => {
    const response = {
      data: {
        items: [true]
      }
    };

    describe("getTwitterAccountLinkedStatus successful call", () => {
      beforeEach(() => {
        mockAxios.get.mockResolvedValue(response);
        mockStore = getMockStore({});
        mockStore.dispatch(getTwitterAccountLinkedStatus("12345"));
      });

      it("makes the correct network call", () => {
        expect(mockAxios.get).toHaveBeenCalledWith("/external_auth/status/12345/service/twitter");
      });

      it("dispatches the correct actions", () => {
        expect(mockStore.getActions()).toEqual([{ type: SOCIAL_LINKING.TWITTER.LINKED.SUCCESS, payload: true }]);
      });
    });

    describe("getTwitterAccountLinkedStatus failed call", () => {
      const error = "This is an error";

      beforeEach(() => {
        mockAxios.get.mockRejectedValue(error);

        mockStore = getMockStore({});
        mockStore.dispatch(getTwitterAccountLinkedStatus("12345"));
      });

      it("dispatches the correct actions", () => {
        expect(mockStore.getActions()).toEqual([{ type: SOCIAL_LINKING.TWITTER.LINKED.FAILURE }]);
      });
    });
  });

  describe("getTwitchAccountLinkedStatus", () => {
    const response = {
      data: {
        items: [true]
      }
    };

    describe("getTwitterAccountLinkedStatus successful call", () => {
      beforeEach(() => {
        mockAxios.get.mockResolvedValue(response);
        mockStore = getMockStore({});
        mockStore.dispatch(getTwitchAccountLinkedStatus("12345"));
      });

      it("makes the correct network call", () => {
        expect(mockAxios.get).toHaveBeenCalledWith("/external_auth/status/12345/service/twitch");
      });

      it("dispatches the correct actions", () => {
        expect(mockStore.getActions()).toEqual([{ type: SOCIAL_LINKING.TWITCH.LINKED.SUCCESS, payload: true }]);
      });
    });

    describe("getTwitchAccountLinkedStatus failed call", () => {
      const error = "This is an error";

      beforeEach(() => {
        mockAxios.get.mockRejectedValue(error);

        mockStore = getMockStore({});
        mockStore.dispatch(getTwitchAccountLinkedStatus("12345"));
      });

      it("dispatches the correct actions", () => {
        expect(mockStore.getActions()).toEqual([{ type: SOCIAL_LINKING.TWITCH.LINKED.FAILURE }]);
      });
    });
  });

  describe("getTwitterAccountInfo", () => {
    const response = {
      data: {
        items: [
          {
            username: "mycoolusername",
            followers: 805900
          }
        ]
      }
    };

    describe("getTwitterAccountInfo successful call", () => {
      beforeEach(() => {
        mockAxios.get.mockResolvedValue(response);
        mockStore = getMockStore({});
        mockStore.dispatch(getTwitterAccountInfo("12345"));
      });

      it("makes the correct network call", () => {
        expect(mockAxios.get).toHaveBeenCalledWith("/twitter-account-info/owner/12345");
      });

      it("dispatches the correct actions", () => {
        expect(mockStore.getActions()).toEqual([
          {
            type: SOCIAL_LINKING.TWITTER.ACCOUNT_INFO.SUCCESS,
            payload: {
              accountName: "mycoolusername",
              supporterCount: 805900
            }
          }
        ]);
      });
    });

    describe("getTwitterAccountInfo failed call", () => {
      const error = "This is an error";

      beforeEach(() => {
        mockAxios.get.mockRejectedValue(error);

        mockStore = getMockStore({});
        mockStore.dispatch(getTwitterAccountInfo("12345"));
      });

      it("dispatches the correct actions", () => {
        expect(mockStore.getActions()).toEqual([{ type: SOCIAL_LINKING.TWITTER.ACCOUNT_INFO.FAILURE }]);
      });
    });
  });

  describe("getTwitchAccountInfo", () => {
    const response = {
      data: {
        items: [
          {
            username: "twitchUsername",
            followers: 43500
          }
        ]
      }
    };

    describe("getTwitchAccountInfo successful call", () => {
      beforeEach(() => {
        mockAxios.get.mockResolvedValue(response);
        mockStore = getMockStore({});
        mockStore.dispatch(getTwitchAccountInfo("12345"));
      });

      it("makes the correct network call", () => {
        expect(mockAxios.get).toHaveBeenCalledWith("/twitch-account-info/owner/12345");
      });

      it("dispatches the correct actions", () => {
        expect(mockStore.getActions()).toEqual([
          {
            type: SOCIAL_LINKING.TWITCH.ACCOUNT_INFO.SUCCESS,
            payload: {
              accountName: "twitchUsername",
              supporterCount: 43500
            }
          }
        ]);
      });
    });

    describe("getTwitchAccountInfo failed call", () => {
      const error = "This is an error";

      beforeEach(() => {
        mockAxios.get.mockRejectedValue(error);

        mockStore = getMockStore({});
        mockStore.dispatch(getTwitchAccountInfo("12345"));
      });

      it("dispatches the correct actions", () => {
        expect(mockStore.getActions()).toEqual([{ type: SOCIAL_LINKING.TWITCH.ACCOUNT_INFO.FAILURE }]);
      });
    });
  });

  describe("setSocialMediaAccountInfoFromPayload", () => {
    const payload = {
      data: {
        accountName: "coolUsername",
        supporterCount: 9876
      }
    };

    const context = {
      socialType: "twitter",
      actionTypes: {
        linkSuccess: SOCIAL_LINKING.TWITTER.LINKED.SUCCESS,
        accountInfoSuccess: SOCIAL_LINKING.TWITTER.ACCOUNT_INFO.SUCCESS,
        linkFailure: SOCIAL_LINKING.TWITTER.LINKED.FAILURE,
        accountInfoFailure: SOCIAL_LINKING.TWITTER.ACCOUNT_INFO.FAILURE
      }
    };

    beforeEach(() => {
      mockStore = getMockStore({});
      mockStore.dispatch(setSocialMediaAccountInfoFromSocket(payload, context));
    });

    it("dispatches the correct actions", () => {
      expect(mockStore.getActions()).toEqual([
        { type: SOCIAL_LINKING.TWITTER.LINKED.SUCCESS, payload: true },
        {
          type: SOCIAL_LINKING.TWITTER.ACCOUNT_INFO.SUCCESS,
          payload: {
            accountName: "coolUsername",
            supporterCount: 9876,
            id: "N/A",
            contentCount: "N/A",
            supporterViews: "N/A"
          }
        }
      ]);
    });

    it("sends the correct notification to the user", () => {
      expect(sendNotification).toHaveBeenCalledWith(
        "twitter account successfully linked",
        "success",
        "Social Media Linking"
      );
    });
  });

  describe("socialMediaLinkingFailure", () => {
    const context = {
      socialType: "twitter",
      actionTypes: {
        linkSuccess: SOCIAL_LINKING.TWITTER.LINKED.SUCCESS,
        accountInfoSuccess: SOCIAL_LINKING.TWITTER.ACCOUNT_INFO.SUCCESS,
        linkFailure: SOCIAL_LINKING.TWITTER.LINKED.FAILURE,
        accountInfoFailure: SOCIAL_LINKING.TWITTER.ACCOUNT_INFO.FAILURE
      }
    };

    beforeEach(() => {
      mockStore = getMockStore({});
      mockStore.dispatch(socialMediaLinkingFailure(context));
    });

    it("dispatches the correct actions", () => {
      expect(mockStore.getActions()).toEqual([
        { type: SOCIAL_LINKING.TWITTER.LINKED.FAILURE },
        { type: SOCIAL_LINKING.TWITTER.ACCOUNT_INFO.FAILURE }
      ]);
    });

    it("sends the correct notification to the user", () => {
      expect(sendNotification).toHaveBeenCalledWith(
        "twitter account was unable to be linked",
        "danger",
        "Social Media Linking"
      );
    });
  });

  describe("unlinkOrganizationSocialMediaAccount", () => {
    describe("unlinkOrganizationSocialMediaAccount successful call", () => {
      const response = {
        data: {
          items: [true]
        }
      };

      const context = {
        ownerId: "12345",
        externalAuth: "twitter",
        socialType: "twitter",
        actionTypes: {
          unlinkSuccess: SOCIAL_LINKING.TWITTER.UNLINK.SUCCESS,
          unlinkFailure: SOCIAL_LINKING.TWITTER.UNLINK.FAILURE
        }
      };

      beforeEach(() => {
        mockAxios.delete.mockResolvedValue(response);
        mockStore = getMockStore({});
        mockStore.dispatch(unlinkOrganizationSocialMediaAccount(context));
      });

      it("makes the correct network call", () => {
        expect(mockAxios.delete).toHaveBeenCalledWith("/external_auth/link/12345/service/twitter");
      });

      it("dispatches the correct actions", () => {
        expect(mockStore.getActions()).toEqual([{ type: SOCIAL_LINKING.TWITTER.UNLINK.SUCCESS, payload: true }]);
      });

      it("sends the correct notification to the user", () => {
        expect(sendNotification).toHaveBeenCalledWith(
          "twitter account successfully unlinked",
          "success",
          "Social Media Unlink"
        );
      });

      it("sends the correct analytic tracking", () => {
        expect(window.analytics.track).toHaveBeenCalledWith("EXTERNAL_ACCOUNT_UNLINKED_TWITTER", {
          location: "organization",
          type: "organization"
        });
      });
    });

    describe("unlinkOrganizationSocialMediaAccount failed call", () => {
      const error = "This is an error";

      const context = {
        ownerId: "12345",
        externalAuth: "twitter",
        socialType: "twitter",
        actionTypes: {
          unlinkSuccess: SOCIAL_LINKING.TWITTER.UNLINK.SUCCESS,
          unlinkFailure: SOCIAL_LINKING.TWITTER.UNLINK.FAILURE
        }
      };

      beforeEach(() => {
        mockAxios.delete.mockRejectedValue(error);

        mockStore = getMockStore({});
        mockStore.dispatch(unlinkOrganizationSocialMediaAccount(context));
      });

      it("dispatches the correct actions", () => {
        expect(mockStore.getActions()).toEqual([{ type: SOCIAL_LINKING.TWITTER.UNLINK.FAILURE }]);
      });

      it("sends the correct notification to the user", () => {
        expect(sendNotification).toHaveBeenCalledWith(
          "Unable to unlink twitter account",
          "danger",
          "Social Media Unlink"
        );
      });
    });
  });

  describe("getYouTubeAccountLinkedStatus", () => {
    describe("getYouTubeAccountLinkedStatus successful call", () => {
      const response = {
        data: {
          items: [true]
        }
      };

      beforeEach(() => {
        mockAxios.get.mockResolvedValue(response);
        mockStore = getMockStore({});
        mockStore.dispatch(getYouTubeAccountLinkedStatus("9876"));
      });

      it("makes the correct network call", () => {
        expect(mockAxios.get).toHaveBeenCalledWith("/external_auth/status/9876/service/google");
      });

      it("dispatches the correct actions", () => {
        expect(mockStore.getActions()).toEqual([{ type: SOCIAL_LINKING.YOUTUBE.LINKED.SUCCESS, payload: true }]);
      });
    });

    describe("getYouTubeAccountLinkedStatus failed call", () => {
      const error = "This is an error";

      beforeEach(() => {
        mockAxios.get.mockRejectedValue(error);

        mockStore = getMockStore({});
        mockStore.dispatch(getYouTubeAccountLinkedStatus("9876"));
      });

      it("dispatches the correct actions", () => {
        expect(mockStore.getActions()).toEqual([{ type: SOCIAL_LINKING.YOUTUBE.LINKED.FAILURE }]);
      });
    });
  });

  describe("getYouTubeAccountInfo", () => {
    describe("getYouTubeAccountInfo successful call", () => {
      const response = {
        data: {
          items: [
            {
              id: "123ABCxyz",
              statistics: {
                subscriberCount: 12345
              },
              title: "Amy Grant"
            }
          ]
        }
      };

      beforeEach(() => {
        mockAxios.get.mockResolvedValue(response);
        mockStore = getMockStore({});
        mockStore.dispatch(getYouTubeAccountInfo("9876"));
      });

      it("makes the correct network call", () => {
        expect(mockAxios.get).toHaveBeenCalledWith("/youtube-account-info/owner/9876");
      });

      it("dispatches the correct actions", () => {
        expect(mockStore.getActions()).toEqual([
          {
            type: SOCIAL_LINKING.YOUTUBE.ACCOUNT_INFO.SUCCESS,
            payload: {
              accountName: "Amy Grant",
              supporterCount: 12345,
              id: "123ABCxyz"
            }
          }
        ]);
      });
    });

    describe("getYouTubeAccountInfo failed call", () => {
      const error = "This is an error";

      beforeEach(() => {
        mockAxios.get.mockRejectedValue(error);

        mockStore = getMockStore({});
        mockStore.dispatch(getYouTubeAccountInfo("9876"));
      });

      it("dispatches the correct actions", () => {
        expect(mockStore.getActions()).toEqual([{ type: SOCIAL_LINKING.YOUTUBE.ACCOUNT_INFO.FAILURE }]);
      });
    });
  });
});
