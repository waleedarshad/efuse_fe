import axios from "axios";
import WindowPortal from "../../../components/WindowPortal";
import { sendNotification } from "../../../helpers/FlashHelper";

import SOCIAL_LINKING from "./types";

export const getServiceUrl = async context => {
  const queryParams = context.ownerKind === "organization" ? `?orgId=${context.ownerId}` : "";
  let url;

  try {
    const response = await axios.get(`/external_auth/${context.externalAuth}${queryParams}`);
    url = response.data.url;
  } catch (error) {
    console.error(`Error while fetching consent URL for ${context.socialType}`, error);
  }
  return url;
};

export const startAuthenticationFlow = context => {
  // eslint-disable-next-line @typescript-eslint/no-floating-promises
  getServiceUrl(context).then(url => {
    WindowPortal(url, "Verification");
  });
};

export const getTwitterAccountLinkedStatus = ownerId => dispatch => {
  axios
    .get(`/external_auth/status/${ownerId}/service/twitter`)
    .then(response => {
      dispatch({
        type: SOCIAL_LINKING.TWITTER.LINKED.SUCCESS,
        payload: response.data.items[0]
      });
    })
    .catch(error => {
      dispatch({
        type: SOCIAL_LINKING.TWITTER.LINKED.FAILURE
      });
      // eslint-disable-next-line no-console
      console.error(error);
    });
};

export const getTwitterAccountInfo = id => dispatch => {
  axios
    .get(`/twitter-account-info/owner/${id}`)
    .then(response => {
      dispatch({
        type: SOCIAL_LINKING.TWITTER.ACCOUNT_INFO.SUCCESS,
        payload: {
          accountName: response.data.items[0]?.username,
          supporterCount: response.data.items[0]?.followers
        }
      });
    })
    .catch(error => {
      dispatch({ type: SOCIAL_LINKING.TWITTER.ACCOUNT_INFO.FAILURE });
      console.error(error);
    });
};

export const getYouTubeAccountLinkedStatus = ownerId => dispatch => {
  axios
    .get(`/external_auth/status/${ownerId}/service/google`)
    .then(response => {
      dispatch({
        type: SOCIAL_LINKING.YOUTUBE.LINKED.SUCCESS,
        payload: response.data.items[0]
      });
    })
    .catch(error => {
      dispatch({
        type: SOCIAL_LINKING.YOUTUBE.LINKED.FAILURE
      });
      // eslint-disable-next-line no-console
      console.error(error);
    });
};

export const getYouTubeAccountInfo = id => dispatch => {
  axios
    .get(`/youtube-account-info/owner/${id}`)
    .then(response => {
      dispatch({
        type: SOCIAL_LINKING.YOUTUBE.ACCOUNT_INFO.SUCCESS,
        payload: {
          accountName: response.data.items[0]?.title,
          supporterCount: response.data.items[0]?.statistics?.subscriberCount,
          id: response.data.items[0]?.id
        }
      });
    })
    .catch(error => {
      dispatch({ type: SOCIAL_LINKING.YOUTUBE.ACCOUNT_INFO.FAILURE });
      console.error(error);
    });
};

export const getTwitchAccountLinkedStatus = ownerId => dispatch => {
  axios
    .get(`/external_auth/status/${ownerId}/service/twitch`)
    .then(response => {
      dispatch({
        type: SOCIAL_LINKING.TWITCH.LINKED.SUCCESS,
        payload: response.data.items[0]
      });
    })
    .catch(error => {
      dispatch({
        type: SOCIAL_LINKING.TWITCH.LINKED.FAILURE
      });
      // eslint-disable-next-line no-console
      console.error(error);
    });
};

export const getTwitchAccountInfo = id => dispatch => {
  axios
    .get(`/twitch-account-info/owner/${id}`)
    .then(response => {
      dispatch({
        type: SOCIAL_LINKING.TWITCH.ACCOUNT_INFO.SUCCESS,
        payload: {
          accountName: response.data.items[0]?.username,
          supporterCount: response.data.items[0]?.followers
        }
      });
    })
    .catch(error => {
      dispatch({ type: SOCIAL_LINKING.TWITCH.ACCOUNT_INFO.FAILURE });
      console.error(error);
    });
};

export const setSocialMediaAccountInfoFromSocket = (payload, context) => dispatch => {
  dispatch({ type: context.actionTypes.linkSuccess, payload: true });
  dispatch({
    type: context.actionTypes.accountInfoSuccess,
    payload: {
      accountName: payload.data.accountName,
      supporterCount: payload.data.supporterCount !== undefined ? payload.data.supporterCount : "N/A",
      id: payload.data.id || "N/A",
      supporterViews: payload.data.supporterViews !== undefined ? payload.data.supporterViews : "N/A",
      contentCount: payload.data.contentCount !== undefined ? payload.data.contentCount : "N/A"
    }
  });
  sendNotification(`${context.socialType} account successfully linked`, "success", "Social Media Linking");
};

export const socialMediaLinkingFailure = context => dispatch => {
  dispatch({
    type: context.actionTypes.linkFailure
  });
  dispatch({ type: context.actionTypes.accountInfoFailure });
  sendNotification(`${context.socialType} account was unable to be linked`, "danger", "Social Media Linking");
};

export const unlinkOrganizationSocialMediaAccount = context => dispatch => {
  axios
    .delete(`/external_auth/link/${context.ownerId}/service/${context.externalAuth}`)
    .then(response => {
      analytics.track(`EXTERNAL_ACCOUNT_UNLINKED_${context.socialType.toUpperCase()}`, {
        location: "organization",
        type: "organization"
      });
      dispatch({ type: context.actionTypes.unlinkSuccess, payload: response.data.items[0] });
      sendNotification(`${context.socialType} account successfully unlinked`, "success", "Social Media Unlink");
    })
    .catch(error => {
      dispatch({ type: context.actionTypes.unlinkFailure });
      console.error(error);
      sendNotification(`Unable to unlink ${context.socialType} account`, "danger", "Social Media Unlink");
    });
};
