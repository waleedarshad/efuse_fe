import mockAxios from "axios";
import { getMockStore } from "../../common/testUtils";
import {
  getStreakLeaderboard,
  getViewsLeaderboard,
  getEngagementsLeaderboard,
  getLeaderboardData
} from "./leaderboardActions";
import {
  GET_ENGAGEMENTS_LEADERBOARD,
  GET_STREAK_LEADERBOARD,
  GET_VIEWS_LEADERBOARD,
  GET_LEADERBOARD_DATA
} from "./types";

describe("leaderboardActions", () => {
  let mockStore;

  beforeEach(() => {
    const items = ["1", "2", "3"];
    mockAxios.get.mockResolvedValue({ data: items });
    mockStore = getMockStore();
  });

  describe("getViewsLeaderboard", () => {
    it("makes a network request to get leaderboard data for views", () => {
      mockStore.dispatch(getViewsLeaderboard());

      expect(mockAxios.get).toHaveBeenCalledWith("/leaderboards/views?page=1&limit=10");
    });

    it("dispatches action to update state with leaderboard views", async () => {
      await mockStore.dispatch(getViewsLeaderboard());

      expect(mockStore.getActions()).toEqual([
        {
          type: GET_VIEWS_LEADERBOARD,
          payload: ["1", "2", "3"]
        }
      ]);
    });
  });

  describe("getStreakLeaderboard", () => {
    it("makes a network request to get leaderboard data for streak", () => {
      mockStore.dispatch(getStreakLeaderboard());

      expect(mockAxios.get).toHaveBeenCalledWith("/leaderboards/streaks?page=1&limit=10");
    });

    it("dispatches action to update state with leaderboard views", async () => {
      await mockStore.dispatch(getStreakLeaderboard());

      expect(mockStore.getActions()).toEqual([
        {
          type: GET_STREAK_LEADERBOARD,
          payload: ["1", "2", "3"]
        }
      ]);
    });
  });

  describe("getEngagementsLeaderboard", () => {
    it("makes a network request to get leaderboard data for streak", () => {
      mockStore.dispatch(getEngagementsLeaderboard());

      expect(mockAxios.get).toHaveBeenCalledWith("/leaderboards/engagements?page=1&limit=10");
    });

    it("dispatches action to update state with leaderboard views", async () => {
      await mockStore.dispatch(getEngagementsLeaderboard());

      expect(mockStore.getActions()).toEqual([
        {
          type: GET_ENGAGEMENTS_LEADERBOARD,
          payload: ["1", "2", "3"]
        }
      ]);
    });
  });

  describe("getLeaderboardData", () => {
    it("makes a network request to get leaderboard data for rocket league", () => {
      mockStore.dispatch(getLeaderboardData("rocketleague"));

      expect(mockAxios.get).toHaveBeenCalledWith("/public/leaderboard/rocketleague");
    });

    it("dispatches action to update state with leaderboard rocket league stats", async () => {
      await mockStore.dispatch(getLeaderboardData("rocketleague"));

      expect(mockStore.getActions()).toEqual([
        {
          type: GET_LEADERBOARD_DATA,
          payload: ["1", "2", "3"]
        }
      ]);
    });
  });
});
