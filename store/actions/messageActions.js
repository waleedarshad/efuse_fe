import axios from "axios";
import getConfig from "next/config";

import { setErrors } from "./errorActions";
import {
  CHAT_RESET_HASH,
  CHAT_SELECTED,
  CHAT_UNSELECTED,
  GET_CHAT_CHANNELS,
  GET_CHAT_FOLLOWERS,
  IS_WINDOW_VIEW,
  RESET_UNREAD_MESSAGE_COUNT,
  RESET_USER,
  SEARCH_SHARE_RECIPIENTS,
  TOGGLE_FOLLOWING_LIST_MODAL
} from "./types";
import { initializeApollo } from "../../config/apolloClient";
import { GET_BLOCKED_USERS } from "../../graphql/BlockedUsersQuery";
import { StreamChatEnum } from "../../enums/stream-chat.enum";

const { publicRuntimeConfig } = getConfig();

const { streamChatKey } = publicRuntimeConfig;

const apolloClient = initializeApollo();

export const searchUsersToChat = query => async dispatch => {
  try {
    const response = await axios.get(`/messages/search_users?query=${query}`);
    dispatch({
      type: GET_CHAT_FOLLOWERS,
      followers: response.data.followers
    });
  } catch (error) {
    dispatch(setErrors(error.response.data));
  }
};

export const setWindowChatView = show => dispatch => {
  dispatch({
    type: IS_WINDOW_VIEW,
    show
  });
};
export const selectChat = (chatId, name) => dispatch => {
  dispatch({
    type: CHAT_SELECTED,
    payload: { chatId, name }
  });
  dispatch({
    type: RESET_USER
  });
};
export const clearChat = () => dispatch => {
  dispatch({
    type: CHAT_UNSELECTED
  });
};
export const chatReset = () => dispatch => {
  dispatch({
    type: CHAT_RESET_HASH,
    resetChat: true
  });
};

export const searchFollowers = query => dispatch => {
  axios
    .get(`/messages/get_followers?page=1&pageSize=10&query=${query}`)
    .then(response => {
      const { chatFollowers } = response.data;
      dispatch({
        type: SEARCH_SHARE_RECIPIENTS,
        payload: chatFollowers.docs.map(follower => {
          return {
            label: follower.name,
            value: follower._id,
            profilePicture: follower.profilePicture
          };
        })
      });
    })
    .catch(error => dispatch(setErrors(error.response.data)));
};

export const toggleFollowingListModal = modal => dispatch => {
  dispatch({ type: TOGGLE_FOLLOWING_LIST_MODAL, modal });
};

export const getChatChannels = currentUser => async dispatch => {
  try {
    await getUserChannels(currentUser, dispatch);
  } catch (error) {
    dispatch(
      setErrors({
        error: "Error getting chat info. Please try again after a few moments"
      })
    );
  }
};
const getUserChannels = async (currentUser, dispatch, streamChatToken = "") => {
  const currentUserId = currentUser._id || currentUser.id;
  try {
    analytics.track(StreamChatEnum.ANALYTICS_EVENT);

    const blockedUserResult = await apolloClient.query({
      query: GET_BLOCKED_USERS,
      fetchPolicy: "no-cache"
    });
    const blockedUsers = blockedUserResult.data.blockedUsers.map(it => it._id);

    const { StreamChat } = await import("stream-chat");

    const chatClient = new StreamChat(streamChatKey);
    await chatClient.setUser(
      {
        id: currentUserId,
        name: currentUser.name,
        image: currentUser.profilePicture.url
      },
      streamChatToken || currentUser.streamChatToken
    );
    const filter = { members: { $in: [currentUserId] } };
    const sort = { last_message_at: -1 };

    // Get chat 30 most recent chat channels where the current user is a member
    const channels = await chatClient.queryChannels(filter, sort, {
      limit: 30
    });

    const filteredChannels = channels.filter(channel => {
      return Object.keys(channel.state.members).find(
        memberId => !blockedUsers.includes(memberId) && memberId !== currentUserId
      );
    });

    dispatch({ type: GET_CHAT_CHANNELS, payload: { channels: filteredChannels } });
  } catch (error) {
    dispatch(
      setErrors({
        error: "Error getting chat info. Please try again after a few moments"
      })
    );
  }
};

export const startChatWith = (userId, userName) => async (dispatch, getState) => {
  const { currentUser } = getState().auth;

  const currentUserId = currentUser._id || currentUser.id;

  try {
    analytics.track(StreamChatEnum.ANALYTICS_EVENT);

    const { StreamChat } = await import("stream-chat");

    const chatClient = new StreamChat(streamChatKey);
    const userToken = currentUser.streamChatToken;

    await chatClient.setUser(
      {
        id: currentUserId,
        name: currentUser.name,
        image: currentUser.profilePicture.url
      },
      userToken
    );
    const channel = chatClient.channel("messaging", "", {
      name: userName,
      members: [userId, currentUserId]
    });
    await channel.create();
    analytics.track("MESSAGES_CONVERSATION_CREATE");
    dispatch({
      type: CHAT_SELECTED,
      payload: { chatId: channel.id, name: userName }
    });

    getChatChannels(currentUser);
  } catch (error) {
    dispatch(
      setErrors({
        error: "Error initiating the chat. Please try again after a few moments"
      })
    );
  }
};

export const resetUnreadMessageCount = () => ({ type: RESET_UNREAD_MESSAGE_COUNT });
