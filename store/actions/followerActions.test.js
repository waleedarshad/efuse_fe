import mockAxios from "axios";
import { getMockStore } from "../../common/testUtils";
import { getFollowees, getFollowers } from "./followerActions";
import {
  GET_FOLLOWERS_SUCCESS,
  GET_FOLLOWERS_FAILURE,
  GET_FOLLOWERS_REQUEST,
  GET_FOLLOWEES_SUCCESS,
  GET_FOLLOWEES_FAILURE,
  GET_FOLLOWEES_REQUEST,
  INVALID_REQUEST
} from "./types";

describe("followerActions", () => {
  let store;

  describe("getFollowees", () => {
    const followees = {
      friends: {
        docs: ["followee1", "followee2"],
        totalDocs: 10,
        hasNextpage: false,
        page: 1
      }
    };

    describe("getFollowees successful call", () => {
      beforeEach(() => {
        mockAxios.get.mockResolvedValue({ data: followees });

        store = getMockStore({});
        store.dispatch(getFollowees("5ecf54d2343c90783cffb06e", 1));
      });

      it("makes the correct network call", () => {
        expect(mockAxios.get).toHaveBeenCalledWith("/followers/get_followees/5ecf54d2343c90783cffb06e", {
          params: { page: 1, pageSize: 12 }
        });
      });

      it("dispatches the correct actions", () => {
        expect(store.getActions()).toEqual([
          { type: GET_FOLLOWEES_REQUEST },
          {
            type: GET_FOLLOWEES_SUCCESS,
            page: 1,
            payload: ["followee1", "followee2"],
            pagination: { totalDocs: 10, hasNextpage: false, page: 1 }
          }
        ]);
      });
    });

    describe("getFollowees failed call", () => {
      const error = {
        response: {
          status: 422
        }
      };
      beforeEach(() => {
        mockAxios.get.mockRejectedValue(error);

        store = getMockStore({});
        store.dispatch(getFollowees("5ecf54d2343c90783cffb06q"));
      });

      it("dispatches the correct actions", () => {
        expect(store.getActions()).toEqual([
          { type: GET_FOLLOWEES_REQUEST },
          { type: GET_FOLLOWEES_FAILURE },
          { type: INVALID_REQUEST, responseCode: 422 }
        ]);
      });
    });
  });

  describe("getFollowers", () => {
    const followers = {
      friends: {
        docs: ["follower1", "follower2"],
        totalDocs: 10,
        hasNextpage: false,
        page: 1
      }
    };

    describe("getFollower successful call", () => {
      beforeEach(() => {
        mockAxios.get.mockResolvedValue({ data: followers });

        store = getMockStore({});
        store.dispatch(getFollowers("5ecf54d2343c90783cffb06e", 1));
      });

      it("makes the correct network call", () => {
        expect(mockAxios.get).toHaveBeenCalledWith("/followers/get_followers/5ecf54d2343c90783cffb06e", {
          params: { page: 1, pageSize: 12 }
        });
      });

      it("dispatches the correct actions", () => {
        expect(store.getActions()).toEqual([
          { type: GET_FOLLOWERS_REQUEST },
          {
            type: GET_FOLLOWERS_SUCCESS,
            page: 1,
            payload: ["follower1", "follower2"],
            pagination: { totalDocs: 10, hasNextpage: false, page: 1 }
          }
        ]);
      });
    });

    describe("getFollowers failed call", () => {
      const error = {
        response: {
          status: 422
        }
      };
      beforeEach(() => {
        mockAxios.get.mockRejectedValue(error);

        store = getMockStore({});
        store.dispatch(getFollowers("5ecf54d2343c90783cffb06q"));
      });

      it("dispatches the correct actions", () => {
        expect(store.getActions()).toEqual([
          { type: GET_FOLLOWERS_REQUEST },
          { type: GET_FOLLOWERS_FAILURE },
          { type: INVALID_REQUEST, responseCode: 422 }
        ]);
      });
    });
  });
});
