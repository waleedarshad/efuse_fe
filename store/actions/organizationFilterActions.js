import {
  ADD_ORGANIZATION_FILTER,
  CLEAR_ORGANIZATION_FILTERS,
  UPDATE_ORGANIZATIONS,
  UPDATE_ORGANIZATION_FILTER_VALUE
} from "./types";
import { getOrganizations } from "./organizationActions";
import { search } from "./commonActions";

import Router from "next/router";

/**
 @module organizationFilterActions
 @category Actions
 */

export const searchOrganization = (searchObject, filters) => dispatch => {
  dispatch(search(Router.router.pathname, searchObject, filters, UPDATE_ORGANIZATIONS, "organizations", updateFilters));
};

export const searchPublicOrganizations = (searchObject, filters) => dispatch => {
  dispatch(
    search("/public/organizations", searchObject, filters, UPDATE_ORGANIZATIONS, "organizations", updateFilters)
  );
};

const updateFilters = filters => dispatch => {
  dispatch({
    type: ADD_ORGANIZATION_FILTER,
    payload: filters
  });
};

export const clearFilters = (isPublic = false) => dispatch => {
  dispatch(
    isPublic
      ? getOrganizations(1, 9, {}, true, false, "public/organizations")
      : getOrganizations(1, 9, {}, true, false, Router.router.pathname)
  );
  dispatch({
    type: CLEAR_ORGANIZATION_FILTERS
  });
};

export const updateFilterValue = (key, value) => dispatch => {
  dispatch({
    type: UPDATE_ORGANIZATION_FILTER_VALUE,
    key,
    value
  });
};

export const removeFilter = key => dispatch => {
  dispatch({
    type: UPDATE_ORGANIZATION_FILTER_VALUE,
    key,
    value: ""
  });
};
