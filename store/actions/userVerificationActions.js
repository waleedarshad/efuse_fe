import axios from "axios";

import { toggleLoader } from "./loaderActions";
import { REQUEST_VERIFICATION } from "./types";

/**
 @module userVerificationActions
 @category Actions
 */

export const requestVerification = () => dispatch => {
  dispatch(toggleLoader(true));
  axios
    .post("/users/request_verification", {})
    .then(response => {
      dispatch({ type: REQUEST_VERIFICATION });
      dispatch(toggleLoader(false));
    })
    .catch(error => dispatch(toggleLoader(false)));
};
