import axios from "axios";
import capitalize from "lodash/capitalize";

import { initializeApollo } from "../../config/apolloClient";
import { toggleLoader } from "./loaderActions";
import { setErrors } from "./errorActions";
import { sendNotification } from "../../helpers/FlashHelper";
import { getPaginatedCollection } from "./commonActions";
import {
  GET_APPLICANTS,
  BECOME_APPLICANT,
  UPDATE_APPLICATION_STATUS,
  CLEAR_APPLICANTS,
  CHECK_USER_OPPORTUNITY_REQUIREMENTS,
  GET_APPLICANTS_REQUEST,
  GET_APPLICANTS_SUCCESS,
  GET_APPLICANTS_FAILURE,
  INVALID_REQUEST
} from "./types";
import { UPDATE_APPLICANT_STATUS, GET_APPLICANTS_QUERY } from "../../graphql/ApplicantsQuery";

const apolloClient = initializeApollo();

/**
 @module applicantActions
 @category Actions
 */
export const apply = (opportunityType, opportunityId, questions, source, promocode) => dispatch => {
  dispatch(toggleLoader(true));
  let candidateQuestions = [];
  // if opportunity has questions, prepare the array and send in the body
  if (questions) {
    candidateQuestions = Object.keys(questions).map(key => questions[key]);
  }

  candidateQuestions = JSON.stringify(candidateQuestions);

  axios
    .post(`/applicants/${opportunityId}`, {
      candidateQuestions,
      source,
      promocode
    })
    .then(response => {
      analytics.track("OPPORTUNITY_USER_APPLIED", { opportunityId, opportunityType });
      sendNotification("Your application has been received!", "success", "Success");
      const { applicant } = response.data;

      dispatch({ type: BECOME_APPLICANT, applicant, opportunityId });
      dispatch(toggleLoader(false));

      // eslint-disable-next-line no-restricted-globals
      location.reload();
    })
    .catch(error => {
      dispatch(toggleLoader(false));
      dispatch(setErrors(error.response.data));
    });
};

export const updateStatus = (opportunityId, applicantId, status, sendEmail, filterType) => dispatch => {
  axios
    .patch(`applicants/${opportunityId}/${applicantId}/status`, { status, sendEmail })
    .then(response => {
      if (response.data.success) {
        const defaultSuccessMsg = "Application status updated";
        const successMsg =
          filterType === "all"
            ? defaultSuccessMsg
            : `${defaultSuccessMsg} and application has been moved to '${capitalize(status)} Queue'`;
        sendNotification(successMsg, "success", "Status");
        dispatch({
          type: UPDATE_APPLICATION_STATUS,
          userId: response.data.applicant.user._id,
          status,
          filterType
        });
      } else {
        sendNotification("You are not allowed to perform this action!", "danger", "Error");
      }
    })
    .catch(error => {
      dispatch(setErrors(error.response.data));
    });
};

export const getApplicants = (opportunityId, page = 1, displayLoader = true, filters = {}, query) => dispatch => {
  dispatch(toggleLoader(false));
  dispatch(
    getPaginatedCollection(
      page,
      9,
      filters,
      displayLoader,
      `/applicants/${opportunityId}`,
      "users",
      GET_APPLICANTS,
      toggleLoader,
      setErrors,
      "applicants",
      true,
      false,
      query
    )
  );
};

/**
 * @summary Gets the user applicants
 * @method
 * @param {number} page The page number
 * @param {*} id Id of the opportunity or tournament to get its applicants
 * @param {string} query The type of applicant
 * @param {object} filters Extra Filters
 * @module applicantActions
 * @category Actions
 */
export const getApplicantsNew = (page, id, query, filters = {}) => dispatch => {
  dispatch({ type: GET_APPLICANTS_REQUEST, payload: page === 1 });
  axios
    .get(`/applicants/${id}`, { params: { page, pageSize: 9, limit: 10, filters, query } })
    .then(response => {
      const { docs, ...rest } = response.data.users;
      dispatch({ type: GET_APPLICANTS_SUCCESS, page, payload: docs, pagination: rest });
    })
    .catch(error => {
      dispatch({ type: GET_APPLICANTS_FAILURE });
      if (error.response?.status === 422 || error.response?.status === 400) {
        dispatch({
          type: INVALID_REQUEST,
          responseCode: error.response.status
        });
      }
      setErrors(error.response ? error.response.data : error);
    });
};

export const clearApplicants = () => dispatch => {
  dispatch({ type: CLEAR_APPLICANTS });
};

export const checkUserOpportunityRequirements = opportunityId => dispatch => {
  axios
    .get(`/applicants/${opportunityId}/validate_requirements`)
    .then(response => {
      dispatch({
        type: CHECK_USER_OPPORTUNITY_REQUIREMENTS,
        payload: {
          opportunityId,
          data: response.data
        }
      });
    })
    .catch(error => {
      setErrors(error.response ? error.response.data : error);
    });
};

export const updateApplicantStatus = (opportunityId, applicantId, status, sendEmail, filterType) => dispatch => {
  return apolloClient
    .mutate({
      mutation: UPDATE_APPLICANT_STATUS,
      variables: { opportunityId, applicantId, status, sendEmail }
    })
    .then(response => {
      const defaultSuccessMsg = "Application status updated";
      const successMsg =
        filterType === "all"
          ? defaultSuccessMsg
          : `${defaultSuccessMsg} and application has been moved to '${capitalize(status)} Queue'`;
      sendNotification(successMsg, "success", "Status");
      dispatch({
        type: UPDATE_APPLICATION_STATUS,
        userId: response.data.updateApplicantStatus.user._id,
        status,
        filterType
      });
    })
    .catch(error => {
      setErrors(error.response ? error.response.data : error);
    });
};

export const getApplicantsWithGraphql = (idOrShortName, page, status) => dispatch => {
  return apolloClient
    .query({
      query: GET_APPLICANTS_QUERY,
      variables: { idOrShortName, page, limit: 9, status }
    })
    .then(response => {
      const { docs, ...rest } = response.data.getOpportunityByIdOrShortName.paginatedApplicants;
      dispatch({ type: GET_APPLICANTS_SUCCESS, page, payload: docs, pagination: rest });
    })
    .catch(error => {
      setErrors(error.response ? error.response.data : error);
    });
};
