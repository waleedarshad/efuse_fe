import axios from "axios";

import { GET_GAME_REQUIREMENT, GET_GAME_REQUIREMENTS } from "./types";
import { toggleLoader } from "./loaderActions";
import { setErrors } from "./errorActions";

/**
 @module gameRequirementActions
 @category Actions
 */

export const getGameRequirements = url => dispatch => {
  dispatch(toggleLoader(true));
  axios
    .get(url)
    .then(res => {
      dispatch({
        type: GET_GAME_REQUIREMENTS,
        payload: res.data
      });
      dispatch(toggleLoader(false));
    })
    .catch(error => {
      dispatch(setErrors(error.response.data));
      dispatch(toggleLoader(false));
    });
};

// Gets a Game Req record.
export const getGameRequirement = (url, router = null, redirectPath = null) => dispatch => {
  axios
    .get(url)
    .then(res => {
      dispatch({
        type: GET_GAME_REQUIREMENT,
        payload: res.data
      });
      dispatch(toggleLoader(false));
    })
    .catch(error => {
      dispatch(setErrors(error.response));
      dispatch(toggleLoader(false));
    });
};
