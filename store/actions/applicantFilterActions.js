import {
  UPDATE_APPLICANTS,
  ADD_APPLICANT_FILTER,
  CLEAR_APPLICANT_FILTERS,
  UPDATE_APPLICANT_FILTER_VALUE
} from "./types";
import { getApplicants } from "./applicantActions";
import { search } from "./commonActions";

/**
 @module applicantFilterActions
 @category Actions
 */

export const searchApplicant = (opportunityId, searchObject, filters) => dispatch => {
  analytics.track("OPPORTUNITY_APPLICANT_SEARCHED");
  dispatch(search(`/applicants/${opportunityId}`, searchObject, filters, UPDATE_APPLICANTS, "users", updateFilters));
};

const updateFilters = filters => dispatch => {
  dispatch({
    type: ADD_APPLICANT_FILTER,
    payload: filters
  });
};

export const clearFilters = opportunityId => dispatch => {
  analytics.track("OPPORTUNITY_APPLICANT_FILTERS_CLEARED");
  dispatch(getApplicants(opportunityId));
  dispatch({
    type: CLEAR_APPLICANT_FILTERS
  });
};

export const updateFilterValue = (key, value) => dispatch => {
  dispatch({
    type: UPDATE_APPLICANT_FILTER_VALUE,
    key,
    value
  });
};

export const removeFilter = key => dispatch => {
  dispatch({
    type: UPDATE_APPLICANT_FILTER_VALUE,
    key,
    value: key.includes("roles") ? [] : ""
  });
};
