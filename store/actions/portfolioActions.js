import axios from "axios";

import { setErrors } from "./errorActions";
import { TOGGLE_IN_PROGRESS, GET_CURRENT_USER, TOGGLE_PORTFOLIO_MODAL, MOCK_EXTERNAL_USER_VIEW } from "./types";

/**
 @module portfolioActions
 @category Actions
 */

export const manageData = (url, experienceData) => dispatch => {
  dispatch(toggleInProgress(true));
  axios
    .post(url, { ...experienceData })
    .then(response => {
      dispatch(toggleInProgress(false));
      dispatch({ type: GET_CURRENT_USER, payload: response.data.updatedUser });
      dispatch(togglePortfolioModal(false));
    })
    .catch(error => {
      dispatch(setErrors(error.response.data));
      dispatch(toggleInProgress(false));
    });
};

export const removeData = (url, id) => dispatch => {
  axios
    .delete(`${url}/${id}`)
    .then(response => {
      dispatch(toggleInProgress(false));
      dispatch({ type: GET_CURRENT_USER, payload: response.data.updatedUser });
    })
    .catch(error => {
      dispatch(toggleInProgress(false));
      dispatch(setErrors(error.response.data));
    });
};

export const manageExperience = (url, expData) => dispatch => {
  dispatch(toggleInProgress(true));
  axios({
    method: "POST",
    config: { headers: { "Content-Type": "multipart/form-data" } },
    url,
    data: expData
  })
    .then(response => {
      dispatch(toggleInProgress(false));
      dispatch({ type: GET_CURRENT_USER, payload: response.data.updatedUser });
      dispatch(togglePortfolioModal(false));
    })
    .catch(error => {
      dispatch(setErrors(error.response.data));
      dispatch(toggleInProgress(false));
    });
};

export const updateVideoInfo = userData => dispatch => {
  axios
    .put("/portfolio/update_video_info/", userData)
    .then(response => {
      dispatch(toggleInProgress(false));
      dispatch({ type: GET_CURRENT_USER, payload: response.data.updatedUser });
      dispatch(togglePortfolioModal(false));
    })
    .catch(error => {
      dispatch(setErrors(error.response.data));
      dispatch(toggleInProgress(false));
    });
};

export const toggleInProgress = inProgress => dispatch => dispatch({ type: TOGGLE_IN_PROGRESS, inProgress });

export const togglePortfolioModal = (modal, modalSection = "") => dispatch =>
  dispatch({ type: TOGGLE_PORTFOLIO_MODAL, modal, modalSection });

export const manageSkills = (url, data) => dispatch => {
  dispatch(toggleInProgress(true));
  axios
    .post(url, data)
    .then(response => {
      dispatch(toggleInProgress(false));
      dispatch({ type: GET_CURRENT_USER, payload: response.data.updatedUser });
      dispatch(togglePortfolioModal(false));
    })
    .catch(error => {
      dispatch(setErrors(error.response.data));
      dispatch(toggleInProgress(false));
    });
};

export const deleteSkill = (url, id) => dispatch => {
  dispatch(toggleInProgress(true));
  axios
    .delete(`${url}/${id}`)
    .then(response => {
      dispatch(toggleInProgress(false));
      dispatch({ type: GET_CURRENT_USER, payload: response.data.updatedUser });
    })
    .catch(error => {
      dispatch(toggleInProgress(false));
      dispatch(setErrors(error.response.data));
    });
};

export const addPubgAccount = data => dispatch => {
  axios
    .patch("/pubg", data)
    .then(response => {
      dispatch({ type: GET_CURRENT_USER, payload: response.data.updatedUser });
      dispatch(togglePortfolioModal(false));
    })
    .catch(error => {
      dispatch(setErrors(error.response));
    });
};

export const removePubgAccount = platformId => dispatch => {
  axios
    .delete(`/pubg/${platformId}`)
    .then(response => {
      dispatch({ type: GET_CURRENT_USER, payload: response.data.updatedUser });
    })
    .catch(error => {
      dispatch(setErrors(error.response));
    });
};

export const mockExternalUserView = mockExternalUser => dispatch =>
  dispatch({ type: MOCK_EXTERNAL_USER_VIEW, mockExternalUser });

export const addSnapchatUsername = data => dispatch => {
  axios
    .put("/portfolio/snapchat", data)
    .then(response => {
      dispatch({ type: GET_CURRENT_USER, payload: response.data.updatedUser });
    })
    .catch(error => {
      dispatch(setErrors(error.response));
    });
};

export const removeSnapchatUsername = () => dispatch => {
  axios
    .delete("/portfolio/snapchat")
    .then(response => {
      dispatch({ type: GET_CURRENT_USER, payload: response.data.updatedUser });
    })
    .catch(error => {
      dispatch(setErrors(error.response));
    });
};

export const addTiktokUsername = data => dispatch => {
  axios
    .put("/portfolio/tiktok", data)
    .then(response => {
      dispatch({ type: GET_CURRENT_USER, payload: response.data.updatedUser });
    })
    .catch(error => {
      dispatch(setErrors(error.response));
    });
};

export const removeTiktokUsername = () => dispatch => {
  axios
    .delete("/portfolio/tiktok")
    .then(response => {
      dispatch({ type: GET_CURRENT_USER, payload: response.data.updatedUser });
    })
    .catch(error => {
      dispatch(setErrors(error.response));
    });
};

export const addYoutubeUsername = data => dispatch => {
  axios
    .put("/portfolio/youtube", data)
    .then(response => {
      dispatch({ type: GET_CURRENT_USER, payload: response.data.updatedUser });
    })
    .catch(error => {
      dispatch(setErrors(error.response));
    });
};

export const removeYoutubeUsername = () => dispatch => {
  axios
    .delete("/portfolio/youtube")
    .then(response => {
      dispatch({ type: GET_CURRENT_USER, payload: response.data.updatedUser });
    })
    .catch(error => {
      dispatch(setErrors(error.response));
    });
};

export const addLinkedinUsername = data => dispatch => {
  axios
    .put("/portfolio/linkedin", data)
    .then(response => {
      dispatch({ type: GET_CURRENT_USER, payload: response.data.updatedUser });
    })
    .catch(error => {
      dispatch(setErrors(error.response));
    });
};

export const removeLinkedinUsername = () => dispatch => {
  axios
    .delete("/portfolio/linkedin")
    .then(response => {
      dispatch({ type: GET_CURRENT_USER, payload: response.data.updatedUser });
    })
    .catch(error => {
      dispatch(setErrors(error.response));
    });
};
