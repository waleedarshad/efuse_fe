import { initializeApollo } from "../../../config/apolloClient";

import { FEED } from "../types";
import {
  ADD_FEED_COMMENTS,
  CommentBody,
  CommentBodyVar,
  CommentUpdateBodyVar,
  DELETE_FEED_COMMENTS,
  GET_POST_COMMENTS,
  GET_POST_THREAD_COMMENTS,
  PostCommentBody,
  PostCommentData,
  PostCommentThreadData,
  PostCommentVars,
  PostUpdateBody,
  UPDATE_FEED_COMMENTS
} from "../../../graphql/comments/CommentQuery";

import { hideContentLoader } from "../loaderActions";

import {
  CommentHypeData,
  CommentHypeVar,
  HYPE_COMMENT,
  UnHypeCommentData,
  UnHypeCommentVar,
  UNHYPE_COMMENT,
  HypeUserData,
  HypeUserVar,
  WHO_HYPED_COMMENT
} from "../../../graphql/hypes/HypeQuery";

import { CREATE_REPORT } from "../../../graphql/report/reportQuery";
import { setErrors } from "../errorActions";
import { CommentModel } from "../../../graphql/comments/Models";
import { sendNotification } from "../../../helpers/FlashHelper";

const apolloClient = initializeApollo();

export const toggleCommentCreation = (feedId: string, key?: string) => dispatch => {
  dispatch({
    type: FEED.COMMENTS.ENABLE.REQUEST,
    payload: { feedId, key }
  });
};

export const commentLoaderForFeed = (feedId, value) => dispatch => {
  dispatch({
    type: FEED.COMMENTS.LOADER,
    payload: { feedId, value }
  });
};

export const getPaginatedComments = (feedId: string, page: number, limit: number) => dispatch => {
  dispatch({ type: FEED.COMMENTS.GET.FEED_COMMENT.REQUEST });
  dispatch(commentLoaderForFeed(feedId, true));

  return apolloClient
    .query<PostCommentData, PostCommentVars>({
      query: GET_POST_COMMENTS,
      variables: { feedId, page, limit },
      fetchPolicy: "no-cache"
    })
    .then(result => {
      const { docs, ...paginationParams } = result.data.GetPaginatedComments;
      dispatch({
        type: FEED.COMMENTS.GET.FEED_COMMENT.SUCCESS,
        payload: {
          feedId,
          comments: {
            docs,
            pagination: { ...paginationParams }
          }
        }
      });
      dispatch(commentLoaderForFeed(feedId, false));
    })
    .catch(error => {
      dispatch({
        type: FEED.COMMENTS.GET.FEED_COMMENT.FAILURE,
        error
      });
      dispatch(hideContentLoader("comments"));
      dispatch(setErrors(error));
    });
};

export const getPaginatedThreadComment = (
  feedId: string,
  parentCommentId: string,
  page: number,
  limit: number
) => dispatch => {
  dispatch({
    type: FEED.COMMENTS.GET.FEED_THREAD_COMMENT.REQUEST,
    payload: {
      feedId,
      parentCommentId
    }
  });

  return apolloClient
    .query<PostCommentThreadData, PostCommentVars>({
      query: GET_POST_THREAD_COMMENTS,
      variables: { feedId, parentId: parentCommentId, page, limit }
    })
    .then(result => {
      const { docs, ...paginationParams } = result.data.GetPaginatedThreadComments;
      dispatch({
        type: FEED.COMMENTS.GET.FEED_THREAD_COMMENT.SUCCESS,
        payload: {
          feedId,
          parentCommentId,
          commentThread: {
            docs,
            pagination: { ...paginationParams }
          }
        }
      });
    })
    .catch(error => {
      dispatch({
        type: FEED.COMMENTS.GET.FEED_THREAD_COMMENT.FAILURE,
        error
      });
      dispatch(setErrors(error));
    });
};

export const hypeComment = (
  feedId: string,
  commentId: string,
  hypeCount: number,
  parentCommentId: string
) => dispatch => {
  dispatch({
    type: FEED.COMMENTS.HYPE.REQUEST
  });

  return apolloClient
    .mutate<CommentHypeData, CommentHypeVar>({
      mutation: HYPE_COMMENT,
      variables: { commentId, hypeCount, platform: "web" }
    })
    .then(result => {
      const hype = result.data.HypeComment;
      dispatch({
        type: FEED.COMMENTS.HYPE.SUCCESS,
        payload: {
          feedId,
          parentCommentId,
          commentId,
          hype
        }
      });

      analytics.track("COMMENT_HYPE", {
        commentId
      });
    })
    .catch(error => {
      dispatch({
        type: FEED.COMMENTS.HYPE.FAILURE,
        error
      });
      dispatch(setErrors(error));
    });
};

export const unhypeComment = (feedId: string, commentId: string, parentCommentId: string) => dispatch => {
  dispatch({
    type: FEED.COMMENTS.UNHYPE.REQUEST
  });

  return apolloClient
    .mutate<UnHypeCommentData, UnHypeCommentVar>({
      mutation: UNHYPE_COMMENT,
      variables: { commentId }
    })
    .then(result => {
      const hype = result.data.UnHypeComment;
      dispatch({
        type: FEED.COMMENTS.UNHYPE.SUCCESS,
        payload: {
          feedId,
          parentCommentId,
          commentId,
          hype
        }
      });

      analytics.track("COMMENT_UNHYPE", {
        commentId
      });
    })
    .catch(error => {
      dispatch({
        type: FEED.COMMENTS.UNHYPE.FAILURE,
        error
      });
      dispatch(setErrors(error));
    });
};

export const toggleCommentEditBtn = (feedId: string, commentId: string, parent: string) => dispatch => {
  dispatch({
    type: FEED.COMMENTS.EDIT.SUCCESS,
    payload: {
      feedId,
      commentId,
      parent
    }
  });
};

export const toggleCommentReply = (feedId: string, commentId: string, callback?: () => void) => dispatch => {
  dispatch({
    type: FEED.COMMENTS.ENABLE_THREAD.SUCCESS,
    payload: {
      feedId,
      commentId,
      callback
    }
  });
};

export const addComment = (body: PostCommentBody) => dispatch => {
  dispatch({
    type: FEED.COMMENTS.CREATE.REQUEST
  });

  return apolloClient
    .mutate<CommentBody, CommentBodyVar>({
      mutation: ADD_FEED_COMMENTS,
      variables: { body }
    })
    .then(result => {
      const comment = result.data.CreateFeedComment;
      dispatch({
        type: FEED.COMMENTS.CREATE.SUCCESS,
        payload: {
          feedId: comment.commentable.feedId,
          parent: comment.parent,
          comment
        }
      });
      analytics.track("COMMENT_CREATE", {
        feedId: comment.commentable.feedId
      });
    })
    .catch(error => {
      dispatch({
        type: FEED.COMMENTS.CREATE.FAILURE,
        error
      });
      dispatch(setErrors(error));
    });
};

export const updateComment = (feedId: string, body: PostUpdateBody) => dispatch => {
  return apolloClient
    .mutate<CommentBody, CommentUpdateBodyVar>({
      mutation: UPDATE_FEED_COMMENTS,
      variables: { body }
    })
    .then(result => {
      const comment = result.data.UpdateFeedComment;
      dispatch({
        type: FEED.COMMENTS.UPDATE.SUCCESS,
        payload: {
          feedId,
          comment
        }
      });
    })
    .catch(error => {
      dispatch({
        type: FEED.COMMENTS.UPDATE.FAILURE,
        error
      });
      dispatch(setErrors(error));
    });
};

export const deleteComment = (feedId: string, commentId: string) => dispatch => {
  dispatch({
    type: FEED.COMMENTS.DELETE.REQUEST
  });

  return apolloClient
    .mutate<CommentBody, CommentBodyVar>({
      mutation: DELETE_FEED_COMMENTS,
      variables: { commentId }
    })
    .then(result => {
      const comment = result.data.DeleteFeedComment;
      dispatch({
        type: FEED.COMMENTS.DELETE.SUCCESS,
        payload: {
          feedId: comment.commentable.feedId,
          parent: comment.parent,
          comment
        }
      });
      analytics.track("COMMENT_DELETE", { commentId });
    })
    .catch(error => {
      dispatch({
        type: FEED.COMMENTS.DELETE.FAILURE,
        error
      });
      dispatch(setErrors(error));
    });
};

export const getCommentHypedUsers = (
  feedId: string,
  comment: CommentModel,
  page: number,
  limit: number
) => dispatch => {
  const commentId = comment._id;
  const { parent } = comment;

  return apolloClient
    .mutate<HypeUserData, HypeUserVar>({
      mutation: WHO_HYPED_COMMENT,
      variables: { feedId, commentId, page, limit }
    })
    .then(result => {
      const { docs, ...paginationParams } = result.data.getCommentHypedUsers;
      dispatch({
        type: FEED.COMMENTS.HYPE_USER.SUCCESS,
        payload: {
          feedId,
          parentId: parent,
          commentId,
          hypeUsers: { docs, pagination: { ...paginationParams } }
        }
      });
    })
    .catch(error => {
      dispatch({
        type: FEED.COMMENTS.HYPE_USER.FAILURE,
        error
      });
      dispatch(setErrors(error));
    });
};

export const toggleCommentHypeUserRequest = (feedId: string, comment: CommentModel) => dispatch => {
  dispatch({
    type: FEED.COMMENTS.HYPE_USER.REQUEST,
    payload: {
      feedId,
      commentId: comment._id,
      parentId: comment.parent
    }
  });
};

export const reportComment = (commentId, reason) => dispatch => {
  apolloClient
    .mutate({
      mutation: CREATE_REPORT,
      variables: { reportItem: commentId, reportKind: "comments", reportReason: reason }
    })
    .then(() => {
      dispatch({ type: FEED.COMMENTS.CREATE_REPORT.SUCCESS, reportId: commentId });
      sendNotification("Comment reported successfully", "success", "Report Comment");
    })
    .catch(error => {
      dispatch(setErrors(error));
    });
};
