import mockAxios from "axios";
import { getMockStore } from "../../common/testUtils";
import { getFollowers } from "./organizationFollowerActions";
import {
  GET_ORGANIZATION_FOLLOWERS_REQUEST,
  GET_ORGANIZATION_FOLLOWERS_SUCCESS,
  GET_ORGANIZATION_FOLLOWERS_FAILURE,
  INVALID_REQUEST
} from "./types";

describe("organizationFollowerActions", () => {
  let mockStore;

  describe("getFollowers", () => {
    const followersData = {
      followers: {
        docs: ["follower1", "follower2"],
        hasNextPage: true,
        totalDocs: 15,
        page: 1
      }
    };

    describe("getFollowers successful call", () => {
      beforeEach(() => {
        mockAxios.get.mockResolvedValue({ data: followersData });

        mockStore = getMockStore({});
        mockStore.dispatch(getFollowers("5d43772e344a340017a93e38"));
      });

      it("makes the correct network call", () => {
        expect(mockAxios.get).toHaveBeenCalledWith("/organizations/5d43772e344a340017a93e38/followers", {
          params: { page: 1, pageSize: 9 }
        });
      });

      it("dispatches the correct actions", () => {
        expect(mockStore.getActions()).toEqual([
          { type: GET_ORGANIZATION_FOLLOWERS_REQUEST },
          {
            type: GET_ORGANIZATION_FOLLOWERS_SUCCESS,
            payload: ["follower1", "follower2"],
            page: 1,
            pagination: { totalDocs: 15, hasNextPage: true, page: 1 }
          }
        ]);
      });
    });

    describe("getRecruits failed call", () => {
      const error = {
        response: {
          status: 422
        }
      };

      beforeEach(() => {
        mockAxios.get.mockRejectedValue(error);

        mockStore = getMockStore({});
        mockStore.dispatch(getFollowers("5d43772e344a340017a93e38"));
      });

      it("dispatches the correct actions", () => {
        expect(mockStore.getActions()).toEqual([
          { type: GET_ORGANIZATION_FOLLOWERS_REQUEST },
          { type: GET_ORGANIZATION_FOLLOWERS_FAILURE },
          { type: INVALID_REQUEST, responseCode: 422 }
        ]);
      });
    });
  });
});
