import axios from "axios";
import Cookies from "js-cookie";
import jwt_decode from "jwt-decode";
import cloneDeep from "lodash/cloneDeep";
import isEmpty from "lodash/isEmpty";
import { toggleLoader } from "../loaderActions";
import { GET_CURRENT_USER, INVALID_REQUEST, RESET_USER, SET_CURRENT_USER } from "../types";
import { setErrors } from "../errorActions";

/**
 * @summary Retrieves the current authed user
 * @method
 * @module userActions
 * @category Actions
 */
export const getCurrentUser = () => dispatch => {
  dispatch(setUser("/users/current_user"));
};

/**
 * @summary Reset User
 * @method
 * @module userActions
 * @category Actions
 */
export const resetUser = () => dispatch => {
  dispatch({
    type: RESET_USER
  });
};

/**
 * @summary Updates current user
 * @method
 * @param {*} url
 * @module userActions
 * @category Actions
 */
export const setUser = (url, isCurrentUser) => (dispatch, getState) => {
  dispatch(toggleLoader(true));
  axios
    .get(url)
    .then(response => {
      const { user } = response.data;
      dispatch({
        type: GET_CURRENT_USER,
        payload: user
      });
      // Whenever we get currentUser object from BE, we update our User Object Cookie
      const { currentUser } = getState().auth;

      if (isCurrentUser) {
        dispatch(storeCurrentUser(user));
      } else if (isEmpty(currentUser)) {
        const token = Cookies.get("token");
        const tokenUser = token && jwt_decode(token);
        if (tokenUser?.id) {
          dispatch(getUserById(tokenUser?.id, null, true));
        }
      } else if (currentUser.id === user._id) {
        dispatch(storeCurrentUser(user));
      }

      dispatch(toggleLoader(false));
    })
    .catch(error => {
      if (error.response && (error.response.status === 422 || error.response.status === 400)) {
        dispatch({
          type: INVALID_REQUEST,
          responseCode: error.response.status
        });
      }
      dispatch(setErrors(error.response ? error.response.data : error));
      dispatch(toggleLoader(false));
    });
};

/**
 * @summary Retrieves a user based on their id
 * @method
 * @param {*} id
 * @param {*} isPublic
 * @module userActions
 * @category Actions
 */
export const getUserById = (id, isPublic = false, isCurrentUser) => dispatch => {
  dispatch(setUser(`${isPublic ? "/public" : ""}/users/show/${id}`, isCurrentUser));
};

/**
 * @summary storeCurrentUser
 * @method
 * @module AuthAction
 * @category Actions
 */

export const storeCurrentUser = currentUser => dispatch => {
  // Only set currentUser object if token is available
  if (Cookies.get("token")) {
    const allowedFields = [
      "_id",
      "id",
      "email",
      "firstName",
      "lastName",
      "firstRun",
      "profilePicture",
      "streamChatToken",
      "bio",
      "roles",
      "traits",
      "motivations",
      "locale",
      "organizations",
      "createdAt",
      "dateOfBirth",
      "username",
      "goldSubscriber",
      "goldSubscriberSince",
      "pathway",
      "name"
    ];
    const clonedUser = cloneDeep(currentUser);
    const currentUserFields = Object.keys(clonedUser);

    for (let i = 0; i <= currentUserFields.length - 1; i++) {
      if (!allowedFields.includes(currentUserFields[i])) {
        delete clonedUser[currentUserFields[i]];
      }
    }

    clonedUser.id = clonedUser.id || clonedUser._id;
    Cookies.set("currentUser", JSON.stringify(clonedUser));

    dispatch(setCurrentUser(clonedUser));
  }
};

/**
 * @summary setCurrentUser
 * @method
 * @module AuthAction
 * @category Actions
 */

export const setCurrentUser = user => dispatch => {
  if (user.firstRun) {
    localStorage?.removeItem("email");
    localStorage?.removeItem("user");
    localStorage?.removeItem("professional");
    localStorage?.removeItem("profile");
    localStorage?.removeItem("student");
  }
  localStorage?.setItem("locale", user.locale);

  const updatedUser = user;
  if (user?._id) {
    updatedUser.id = user._id;
  }
  if (user?.id) {
    updatedUser._id = user.id;
  }

  dispatch({
    type: SET_CURRENT_USER,
    payload: updatedUser
  });
};
