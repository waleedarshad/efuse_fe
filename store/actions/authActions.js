import Cookies from "js-cookie";
import Router from "next/router";
import axios from "axios";
import { SET_SIGNUP_FORM_ERROR } from "./types";
import { resetUser, setCurrentUser, storeCurrentUser } from "./common/userAuthActions";
import { bindLoader, toggleLoader } from "./loaderActions";
import { setErrors } from "./errorActions";
import { removeTokens, getEfuseRefreshToken } from "../../helpers/Auth0Helper";
import { sendNotification } from "../../helpers/FlashHelper";
import { connectSocket } from "./globalAppLevelActions/websocketGlobalActions";

const PLATFORM = "web";

/**
 @module authActions
 @category Actions
 */

export const loginUser = (userData, redirectURL, authData = undefined) => dispatch => {
  dispatch(toggleLoader(true));
  if (authData) {
    // the user is already authorized via user creation
    handleAuthedUser(authData, redirectURL, dispatch, false);
  } else {
    axios
      .post("/auth/login", { ...userData, platform: PLATFORM })
      .then(res => {
        handleAuthedUser(res.data, redirectURL, dispatch);
      })
      .catch(err => {
        dispatch(setErrors(err?.response?.data ? err.response.data : ""));
        dispatch(toggleLoader(false));
      });
  }
};

export const loginUser3rdParty = (data, redirectURL) => dispatch => {
  dispatch(toggleLoader(true));
  try {
    const { token, message, user, refreshToken } = data.data;
    const session = { token, user, refreshToken };
    dispatch(bindToken(session, true, redirectURL));

    if (data?.twitch) {
      analytics.track("USER_LOGIN_TWITCH", {
        email: user.email
      });
      analytics.track("LOGIN_USER_TWITCH_SUCCESS", {
        email: user.email
      });
    }
    if (data?.discord) {
      analytics.track("USER_LOGIN_DISCORD", {
        email: user.email
      });
      analytics.track("LOGIN_USER_DISCORD_SUCCESS", {
        email: user.email
      });
    }
    analytics.track("USER_LOGIN", {
      email: user.email
    });
    sendNotification(message, "success", "Success");

    dispatch(toggleLoader(false));
  } catch (error) {
    dispatch(setErrors(error?.response?.data ? error.response.data : ""));
    dispatch(toggleLoader(false));
  }
};

export const bindToken = (session, redirect = false, redirectURL = "") => dispatch => {
  const { token, user, refreshToken } = session;

  Cookies.set("token", token, { expires: 14 });
  Cookies.set("refreshToken", refreshToken, { expires: 90 });

  dispatch(storeCurrentUser({ ...user }));
  connectSocket(token);

  if (redirect && redirectURL) {
    // Setting one second timer for step 4 of the signup animation to complete
    setTimeout(() => {
      window.location = redirectURL;
    }, 1000);
  } else if (redirect) {
    Router.push("/u/[u]", `/u/${user.username}`);
  }
};

export const logoutUser = () => dispatch => {
  dispatch(
    bindLoader(() => {
      analytics.track("USER_LOGOUT");

      axios
        .post("/users/logout", { refreshToken: getEfuseRefreshToken() })
        .then(() => {
          localStorage?.removeItem("ajs_user_id");
          localStorage?.removeItem("ajs_user_traits");

          removeTokens();

          // auth
          dispatch(setCurrentUser({}));

          // user
          dispatch(resetUser());

          Router.push("/");
        })
        .catch(error => dispatch(setErrors(error?.response?.data || "Something went wrong")));
    })
  );
  // eslint-disable-next-line global-require
  const { getSocket } = require("../../websockets");
  const socket = getSocket();
  socket.io.disconnect(true);
};

export const setLocale = language => (dispatch, getState) => {
  if (Cookies.get("token")) {
    const { currentUser } = getState().auth;

    axios
      .patch(`/users/set_locale/${currentUser.id}`, { locale: language })
      .then(res => {
        const { user } = res.data;
        dispatch(storeCurrentUser(user));
      })
      .catch(err => {
        dispatch(setErrors(err.response.data));
        dispatch(toggleLoader(false));
      });
  }
};

const handleAuthedUser = (userData, redirectURL, dispatch, showNotification = true) => {
  const { token, user, refreshToken, message } = userData;
  const session = { token, user, refreshToken };
  dispatch(bindToken(session, true, redirectURL));

  analytics.track("USER_LOGIN", {
    email: userData.user.email
  });
  if (showNotification) sendNotification(message, "success", "Success");

  dispatch(toggleLoader(false));
};

export const clearSignupValidationErrors = () => dispatch => {
  dispatch({
    type: SET_SIGNUP_FORM_ERROR,
    payload: {
      email: null,
      username: null
    }
  });
};

export const validateEmailInput = email => dispatch => {
  if (email.length === 0) {
    dispatch({
      type: SET_SIGNUP_FORM_ERROR,
      payload: {
        email: "Email is required"
      }
    });
  } else {
    axios
      .post("/auth/validate_email", { email })
      .then(res => {
        const { valid, error } = res.data;
        dispatch({
          type: SET_SIGNUP_FORM_ERROR,
          payload: {
            email: valid ? "" : error
          }
        });
      })
      .catch(err => {
        dispatch({
          type: SET_SIGNUP_FORM_ERROR,
          payload: {
            email: "Error validating email. Please try again later"
          }
        });
        dispatch(setErrors(err.response.data));
      });
  }
};

export const validateUsernameInput = username => dispatch => {
  if (username.length === 0) {
    dispatch({
      type: SET_SIGNUP_FORM_ERROR,
      payload: {
        username: "Username is required"
      }
    });
  } else {
    axios
      .post("/auth/validate_username", { username })
      .then(res => {
        const { valid, error } = res.data;
        dispatch({
          type: SET_SIGNUP_FORM_ERROR,
          payload: {
            username: valid ? "" : error
          }
        });
      })
      .catch(err => {
        dispatch({
          type: SET_SIGNUP_FORM_ERROR,
          payload: {
            username: "Error validating username. Please try again later"
          }
        });
        dispatch(setErrors(err.response.data));
      });
  }
};

export const subscribeToNewsletter = email => dispatch => {
  axios
    .post("/auth/subscribe_newsletter", { email })
    .then(() => {
      analytics.track("NEWSLETTER_SIGNUP");
      sendNotification("Subscribed to Newsletter", "success", "Success");
    })
    .catch(error => {
      dispatch(setErrors(error.response.data));
    });
};
