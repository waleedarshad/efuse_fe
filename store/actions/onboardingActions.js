import axios from "axios";
import { OB_UPDATE_STORE, SET_OAUTH_SIGNUP_VALUES, GET_PATHWAYS } from "./types";
import shuffle from "lodash/shuffle";

import { isUserLoggedIn } from "../../helpers/AuthHelper";
import { finishActions } from "../../helpers/OnboardingHelper";
import { setErrors } from "./errorActions";
import { toggleLoader } from "./loaderActions";

/**
 @module onboardingActions
 @category Actions
 */

export const getExternalSignupValues = (data, url, callback) => async dispatch => {
  try {
    const response = await axios.post(url, data);
    // dispatch({type: SET_OAUTH_SIGNUP_VALUES, payload:response.data})
    callback({ success: true, data: response.data });
  } catch (error) {
    dispatch(setErrors(error.response.data));
    callback({ success: false, data: error.response.data });
  }
};
export const setExternalSignupValues = data => dispatch => {
  dispatch({ type: SET_OAUTH_SIGNUP_VALUES, payload: data });
};
export const updateStoreFromLocalStorage = key => dispatch => {
  const object = localStorage[key];
  if (object !== undefined) {
    dispatch(setObjectInLocalStorage(JSON.parse(object), key));
  }
  dispatch(toggleLoader(false));
};

const setObjectInLocalStorage = (object, key) => {
  return {
    type: OB_UPDATE_STORE,
    object,
    key
  };
};

export const submitFormData = (
  formData,
  url,
  objectFields,
  key,
  finish = false,
  router = null,
  redirectPath = null
) => dispatch => {
  dispatch(toggleLoader(true));
  axios({
    method: "PATCH",
    config: { headers: { "Content-Type": "multipart/form-data" } },
    url: `${url}/${localStorage?.email}`,
    data: formData
  })
    .then(response => {
      const { user } = response.data;
      const object = {};
      objectFields.forEach(field => {
        object[field] = user[field];
      });
      dispatch(setObjectInLocalStorage(object, key));
      localStorage?.setItem(key, JSON.stringify(object));
      if (finish) {
        finishActions();
      } else {
        router.push(redirectPath);
      }
      dispatch(toggleLoader(false));
    })
    .catch(error => {
      dispatch(setErrors(error.response.data));
      dispatch(toggleLoader(false));
    });
};

export const getPathways = () => dispatch => {
  dispatch(toggleLoader(true));
  axios
    .get(`${isUserLoggedIn() ? "" : "/public"}/pathways`)
    .then(response => {
      dispatch({
        type: GET_PATHWAYS,
        payload: shuffle(response.data)
      });
      dispatch(toggleLoader(false));
    })
    .catch(error => {
      dispatch(setErrors(error));
      dispatch(toggleLoader(false));
    });
};

export const setUserpathway = data => dispatch => {
  dispatch(toggleLoader(true));
  axios
    .patch("/users/pathways/select", data)
    .then(response => {
      dispatch(toggleLoader(false));
    })
    .catch(error => {
      dispatch(setErrors(error));
      dispatch(toggleLoader(false));
    });
};
