import mockAxios from "axios";
import { getMockStore } from "../../common/testUtils";
import {
  getPromotedOpportunities,
  getOpportunitiesByType,
  getAdminOpportunities,
  getOpportunitiesNew
} from "./opportunityActions";
import {
  PROMOTED_OPPORTUNITIES,
  TOGGLE_LOADER,
  GET_RECENT_OPPORTUNITIES_BY_TYPE,
  GET_OPPORTUNITIES_REQUEST,
  GET_OPPORTUNITIES_SUCCESS,
  GET_OPPORTUNITIES_FAILURE,
  GET_ADMIN_OPPORTUNITIES_REQUEST,
  GET_ADMIN_OPPORTUNITIES_SUCCESS,
  GET_ADMIN_OPPORTUNITIES_FAILURE,
  INVALID_REQUEST
} from "./types";

describe("opportunityActions", () => {
  let mockStore;

  describe("getPromotedOpportunities", () => {
    beforeEach(() => {
      mockAxios.get.mockResolvedValue({ data: "data" });

      mockStore = getMockStore({});
      mockStore.dispatch(getPromotedOpportunities(true));
    });

    it("makes a network request for promoted opportunities with the correct route", () => {
      expect(mockAxios.get).toHaveBeenCalledWith("/public/opportunities/promoted");
    });

    it("dispatches the correct actions", () => {
      expect(mockStore.getActions()).toEqual([
        {
          type: TOGGLE_LOADER,
          show: true
        },
        {
          type: PROMOTED_OPPORTUNITIES,
          payload: "data"
        },
        {
          type: TOGGLE_LOADER,
          show: false
        }
      ]);
    });
  });

  describe("getOpportunitiesByType", () => {
    beforeEach(() => {
      mockAxios.get.mockResolvedValue({ data: "data" });

      mockStore = getMockStore({});
      mockStore.dispatch(getOpportunitiesByType(true));
    });

    it("makes a network request for recent 10 opportunities with the correct route", () => {
      expect(mockAxios.get).toHaveBeenCalledWith("/public/opportunities/recent");
    });

    it("dispatches the correct actions", () => {
      expect(mockStore.getActions()).toEqual([
        {
          type: TOGGLE_LOADER,
          show: true
        },
        {
          type: GET_RECENT_OPPORTUNITIES_BY_TYPE,
          payload: "data"
        },
        {
          type: TOGGLE_LOADER,
          show: false
        }
      ]);
    });
  });

  describe("getOpportunitiesNew", () => {
    const opportunitiesData = {
      opportunities: {
        docs: ["opportunity1", "opportunity2"],
        totalDocs: 10,
        hasNextPage: false,
        page: 1
      }
    };

    describe("getOpportunitiesNew successful call", () => {
      beforeEach(() => {
        mockAxios.get.mockResolvedValue({ data: opportunitiesData });

        mockStore = getMockStore({});
        mockStore.dispatch(getOpportunitiesNew());
      });

      it("makes the correct network call", () => {
        expect(mockAxios.get).toHaveBeenCalledWith("/opportunities", {
          params: { page: 1, pageSize: 9, filters: {}, id: "" }
        });
      });

      it("dispatches the correct actions", () => {
        expect(mockStore.getActions()).toEqual([
          { type: GET_OPPORTUNITIES_REQUEST, payload: true },
          {
            type: GET_OPPORTUNITIES_SUCCESS,
            page: 1,
            payload: ["opportunity1", "opportunity2"],
            pagination: { totalDocs: 10, hasNextPage: false, page: 1 }
          }
        ]);
      });
    });

    describe("getOpportunitiesNew failed call", () => {
      const error = {
        response: {
          status: 422
        }
      };
      beforeEach(() => {
        mockAxios.get.mockRejectedValue(error);

        mockStore = getMockStore({});
        mockStore.dispatch(getOpportunitiesNew());
      });

      it("dispatches the correct actions", () => {
        expect(mockStore.getActions()).toEqual([
          { type: GET_OPPORTUNITIES_REQUEST, payload: true },
          { type: GET_OPPORTUNITIES_FAILURE },
          { type: INVALID_REQUEST, responseCode: 422 }
        ]);
      });
    });
  });

  describe("getAdminOpportunities", () => {
    const opportunitiesData = {
      opportunities: {
        docs: ["AdminOpportunity1", "AdminOpportunity2"],
        totalDocs: 10,
        hasNextPage: false,
        page: 1
      }
    };

    describe("getAdminOpportunities successful call", () => {
      beforeEach(() => {
        mockAxios.get.mockResolvedValue({ data: opportunitiesData });

        mockStore = getMockStore({});
        mockStore.dispatch(getAdminOpportunities(1, { opportunityType: "Event" }, "5ecf54d2343c90783cffb06e"));
      });

      it("makes the correct network call", () => {
        expect(mockAxios.get).toHaveBeenCalledWith("/opportunities/owned", {
          params: { page: 1, pageSize: 20, filters: { opportunityType: "Event" }, ownerId: "5ecf54d2343c90783cffb06e" }
        });
      });

      it("dispatches the correct actions", () => {
        expect(mockStore.getActions()).toEqual([
          { type: GET_ADMIN_OPPORTUNITIES_REQUEST },
          {
            type: GET_ADMIN_OPPORTUNITIES_SUCCESS,
            page: 1,
            payload: ["AdminOpportunity1", "AdminOpportunity2"],
            pagination: { totalDocs: 10, hasNextPage: false, page: 1 }
          }
        ]);
      });
    });

    describe("getAdminOpportunities failed call", () => {
      const error = {
        response: {
          status: 422
        }
      };
      beforeEach(() => {
        mockAxios.get.mockRejectedValue(error);

        mockStore = getMockStore({});
        mockStore.dispatch(getAdminOpportunities());
      });

      it("dispatches the correct actions", () => {
        expect(mockStore.getActions()).toEqual([
          { type: GET_ADMIN_OPPORTUNITIES_REQUEST },
          { type: GET_ADMIN_OPPORTUNITIES_FAILURE },
          { type: INVALID_REQUEST, responseCode: 422 }
        ]);
      });
    });
  });
});
