import axios from "axios";

import {
  NOTIFICATIONS_BOX_TOGGLE,
  GET_NOTIFICATIONS,
  GET_NOTIFICATIONS_COUNT,
  CLEAR_NOTIFICATIONS_COUNT,
  MARK_ALL_AS_READ,
  MARK_AS_READ,
  ORG_JOIN_INVITE_ACTION,
  INVALID_REQUEST
} from "./types";

import { setErrors } from "./errorActions";

import { sendNotification } from "../../helpers/FlashHelper";

/**
 @module notificationsActions
 @category Actions
 */

export const notificationsBoxToggle = show => dispatch => {
  dispatch({ type: NOTIFICATIONS_BOX_TOGGLE, show });
};

export const getNotifications = (page = 1, pageSize = 10) => dispatch => {
  axios
    .get(`/notifications?page=${page}&pageSize=${pageSize}`)
    .then(response => {
      const { docs, ...rest } = response.data.notifications;
      dispatch({
        type: GET_NOTIFICATIONS,
        notifications: docs,
        pagination: { ...rest }
      });
    })
    .catch(error => {
      dispatch(setErrors(error.response.data));
    });
};

export const getNotificationsCount = () => dispatch => {
  axios
    .get("/notifications/count")
    .then(response =>
      dispatch({
        type: GET_NOTIFICATIONS_COUNT,
        count: response.data.newNotifications
      })
    )
    .catch(error => {
      if (error.response && (error.response.status === 422 || error.response.status === 400)) {
        dispatch({
          type: INVALID_REQUEST,
          responseCode: error.response.status
        });
      }
      dispatch(setErrors(error.response ? error.response.data : error));
    });
};

export const markAllAsRead = () => dispatch => {
  axios
    .get("/notifications/mark_all_as_read")
    .then(() => dispatch({ type: MARK_ALL_AS_READ }))
    .catch(err => {
      dispatch(setErrors(err.response ? err.response.data : err));
    });
};
export const markAsRead = notificationId => dispatch => {
  axios
    .put(`/notifications/mark_as_read/${notificationId}`)
    .then(() => dispatch({ type: MARK_AS_READ, notificationId }))
    .catch(err => {
      dispatch(setErrors(err.response ? err.response.data : err));
    });
};

export const clearAll = () => dispatch => {
  axios
    .get("/notifications/clear_count")
    .then(() => dispatch({ type: CLEAR_NOTIFICATIONS_COUNT }))
    .catch(err => {
      dispatch(setErrors(err.response ? err.response.data : err));
    });
};

export const acceptOrgJoinInvite = orgId => dispatch => {
  axios
    .put(`/organization_invitations/accept/${orgId}`)
    .then(response => {
      const { data } = response;
      if (data.invited) {
        dispatch({ type: ORG_JOIN_INVITE_ACTION, payload: data });
      }
      sendNotification(data.message, data.flashType, "Alert");
    })
    .catch(error => {
      dispatch(setErrors(error.response.data));
    });
};
export const rejectOrgJoinInvite = orgId => dispatch => {
  axios
    .put(`/organization_invitations/reject/${orgId}`)
    .then(response => {
      const { data } = response;
      dispatch({ type: ORG_JOIN_INVITE_ACTION, payload: data });
      sendNotification(data.message, data.flashType, "Alert");
    })
    .catch(error => {
      dispatch(setErrors(error.response.data));
    });
};
