import axios from "axios";
import isEmpty from "lodash/isEmpty";
import { FeaturedFeedData, FeaturedFeedVars, GET_FEATURED_FEED } from "../../graphql/feeds/FeaturedFeedQuery";
import { initializeApollo } from "../../config/apolloClient";
import { sendNotification } from "../../helpers/FlashHelper";
import { GET_VERIFIED_FEED, VerifiedFeedData, VerifiedFeedVars } from "../../graphql/feeds/VerifiedFeedQuery";
import { FollowingFeedData, FollowingFeedVars, GET_FOLLOWING_FEED } from "../../graphql/feeds/FollowingFeedQuery";
import { GET_USER_FEED, UserFeedData, UserFeedVars } from "../../graphql/feeds/UserFeedQuery";
import { FEED } from "./types";
import {
  GET_POST,
  UPDATE_POST,
  FeedPost,
  PostVars,
  CreatePostInput,
  PostInput,
  CREATE_POST,
  UpdatePostInput,
  CREATE_SCHEDULE_POST,
  DELETE_POST
} from "../../graphql/feeds/PostQuery";
import { Post } from "../../graphql/feeds/Models";
import { HypeUserData, HypeUserVar, WHO_HYPED_POST } from "../../graphql/hypes/HypeQuery";
import { setErrors } from "./errorActions";
import { OrgFeedData, OrgFeedVars, GET_ORG_FEED } from "../../graphql/feeds/OrgFeedQuery";

const apolloClient = initializeApollo();

export const clearFeed = () => ({ type: FEED.CLEAR });

export const getFeaturedFeed = (page: number, limit: number) => dispatch => {
  dispatch({ type: FEED.GET.FEATURED_FEED.REQUEST });

  return apolloClient
    .query<FeaturedFeedData, FeaturedFeedVars>({
      query: GET_FEATURED_FEED,
      variables: { page, limit }
    })
    .then(result => {
      const { docs, ...paginationParams } = result.data.FeaturedFeed;

      dispatch({
        type: FEED.GET.FEATURED_FEED.SUCCESS,
        payload: {
          docs,
          pagination: { ...paginationParams }
        }
      });
    })
    .catch(error => {
      dispatch({ type: FEED.GET.FEATURED_FEED.FAILURE, error });
    });
};

export const getVerifiedFeed = (page: number, limit: number) => dispatch => {
  dispatch({ type: FEED.GET.VERIFIED_FEED.REQUEST });

  return apolloClient
    .query<VerifiedFeedData, VerifiedFeedVars>({
      query: GET_VERIFIED_FEED,
      variables: { page, limit }
    })
    .then(result => {
      const { docs, ...paginationParams } = result.data.VerifiedFeed;

      dispatch({
        type: FEED.GET.VERIFIED_FEED.SUCCESS,
        payload: {
          docs,
          pagination: { ...paginationParams }
        }
      });
    })
    .catch(error => {
      dispatch({ type: FEED.GET.VERIFIED_FEED.FAILURE, error });
    });
};

export const getFollowingFeed = (page: number, limit: number) => dispatch => {
  dispatch({ type: FEED.GET.FOLLOWING_FEED.REQUEST });

  return apolloClient
    .query<FollowingFeedData, FollowingFeedVars>({
      query: GET_FOLLOWING_FEED,
      variables: { page, limit }
    })
    .then(result => {
      const { docs, ...paginationParams } = result.data.FollowingFeed;

      dispatch({
        type: FEED.GET.FOLLOWING_FEED.SUCCESS,
        payload: {
          docs,
          pagination: { ...paginationParams }
        }
      });
    })
    .catch(error => {
      dispatch({ type: FEED.GET.FOLLOWING_FEED.FAILURE, error });
    });
};

export const getUserFeed = (page: number, limit: number, userId: string) => dispatch => {
  dispatch({ type: FEED.GET.USER_FEED.REQUEST });

  return apolloClient
    .query<UserFeedData, UserFeedVars>({
      query: GET_USER_FEED,
      variables: { page, limit, userId }
    })
    .then(result => {
      const { docs, ...paginationParams } = result.data.UserFeed;

      dispatch({
        type: FEED.GET.USER_FEED.SUCCESS,
        payload: {
          docs,
          pagination: { ...paginationParams }
        }
      });
    })
    .catch(error => {
      dispatch({ type: FEED.GET.USER_FEED.FAILURE, error });
    });
};

export const getOrganizationFeed = (page: number, limit: number, orgId: string) => dispatch => {
  dispatch({ type: FEED.GET.ORG_FEED.REQUEST });

  return apolloClient
    .query<OrgFeedData, OrgFeedVars>({
      query: GET_ORG_FEED,
      variables: { page, limit, orgId }
    })
    .then(result => {
      const { docs, ...paginationParams } = result.data.OrganizationFeed;

      dispatch({
        type: FEED.GET.ORG_FEED.SUCCESS,
        payload: {
          docs,
          pagination: { ...paginationParams }
        }
      });
    })
    .catch(error => {
      dispatch({ type: FEED.GET.ORG_FEED.FAILURE, error });
    });
};

export const followUser = (followeeId: string, followeeName: string) => dispatch => {
  dispatch({ type: FEED.FOLLOW.USER.REQUEST });

  axios
    .get(`/followers/follow/${followeeId}`)
    .then(response => {
      if (response.data.isFollowed) {
        analytics.track("USER_FOLLOW", { location: "LOUNGE", id: followeeId });
        sendNotification(`You are now following ${followeeName}`, "success", "Follow");
        dispatch({
          type: FEED.FOLLOW.USER.SUCCESS,
          payload: {
            userId: followeeId,
            isFollowing: true
          }
        });
      } else {
        dispatch({ type: FEED.FOLLOW.USER.FAILURE });
      }
    })
    .catch(error => {
      dispatch({ type: FEED.FOLLOW.USER.FAILURE, error });
      dispatch(setErrors(error.response.data));
    });
};

export const unFollowUser = (followeeId: string, followeeName: string) => dispatch => {
  dispatch({ type: FEED.UNFOLLOW.USER.REQUEST });

  axios
    .delete(`/followers/unfollow/${followeeId}`)
    .then(response => {
      if (!response.data.isFollowed) {
        analytics.track("USER_UNFOLLOW", { location: "LOUNGE", id: followeeId });
        sendNotification(`You have stopped following ${followeeName}.`, "warning", "Unfollow");
        dispatch({
          type: FEED.UNFOLLOW.USER.SUCCESS,
          payload: { userId: followeeId, isFollowing: false }
        });
      } else {
        dispatch({ type: FEED.UNFOLLOW.USER.FAILURE });
      }
    })
    .catch(error => {
      dispatch({ type: FEED.UNFOLLOW.USER.FAILURE, error });
      dispatch(setErrors(error.response.data));
    });
};

export const followOrganization = (organizationId: string, organizationName: string) => dispatch => {
  dispatch({ type: FEED.FOLLOW.ORGANIZATION.REQUEST });

  axios
    .post(`/organizations/${organizationId}/follow`, {})
    .then(response => {
      if (response.data.isFollowed) {
        analytics.track("ORGANIZATION_FOLLOW", { organizationId });
        sendNotification(`You are now following ${organizationName}`, "success", "Follow");
        dispatch({
          type: FEED.FOLLOW.ORGANIZATION.SUCCESS,
          payload: {
            organizationId,
            isFollowing: true
          }
        });
      }
    })
    .catch(error => {
      dispatch({ type: FEED.FOLLOW.ORGANIZATION.FAILURE, error });
      dispatch(setErrors(error.response.data));
    });
};

export const unFollowOrganization = (organizationId: string, organizationName: string) => dispatch => {
  dispatch({ type: FEED.UNFOLLOW.ORGANIZATION.REQUEST });

  axios
    .delete(`/organizations/${organizationId}/unfollow`)
    .then(response => {
      if (!response.data.isFollowed) {
        analytics.track("ORGANIZATION_UNFOLLOW", { organizationId });
        sendNotification(`You have stopped following ${organizationName}.`, "warning", "Unfollow");
        dispatch({
          type: FEED.UNFOLLOW.ORGANIZATION.SUCCESS,
          payload: {
            organizationId,
            isFollowing: false
          }
        });
      }
    })
    .catch(error => {
      dispatch({ type: FEED.UNFOLLOW.ORGANIZATION.FAILURE, error });
      dispatch(setErrors(error.response.data));
    });
};

export const hypePost = (feedId: string, hypeCount: number) => dispatch => {
  dispatch({ type: FEED.POST.HYPE.REQUEST });

  axios
    .post(`/hypes/feed/${feedId}`, { hypeCount })
    .then(response => {
      if (response.data.newHype) {
        dispatch({ type: FEED.POST.HYPE.SUCCESS });
      } else {
        dispatch({ type: FEED.POST.HYPE.FAILURE });
      }
    })
    .catch(error => {
      dispatch({ type: FEED.POST.HYPE.FAILURE, error });
      dispatch(setErrors(error.response.data));
    });
};

export const deletePost = (feedId: string) => dispatch => {
  dispatch({ type: FEED.POST.DELETE.REQUEST });

  return apolloClient
    .mutate<boolean, { feedId: string }>({
      mutation: DELETE_POST,
      variables: { feedId }
    })
    .then(() => {
      analytics.track("POST_DELETE", {
        feedId
      });

      dispatch({
        type: FEED.POST.DELETE.SUCCESS,
        payload: {
          feedId
        }
      });

      sendNotification("Post deleted successfully ", "success", "Post");
    })
    .catch(error => {
      dispatch({ type: FEED.POST.DELETE.FAILURE, error });
      dispatch(setErrors(error));
    });
};

export const removePostFromHome = (parentFeedId: string, feedId: string) => dispatch => {
  dispatch({ type: FEED.POST.REMOVE_FROM_HOME.REQUEST });

  axios
    .delete(`/feeds/${parentFeedId}/remove_from_timeline`)
    .then(() => {
      dispatch({
        type: FEED.POST.REMOVE_FROM_HOME.SUCCESS,
        payload: { feedId }
      });
      analytics.track("POST_HIDE", {
        feedId
      });
    })
    .catch(error => {
      dispatch({ type: FEED.POST.REMOVE_FROM_HOME.FAILURE, error });
    });
};

export const hidePostFromLounge = (parentFeedId: string, feedId: string) => dispatch => {
  dispatch({ type: FEED.POST.HIDE_FROM_LOUNGE.REQUEST });

  axios
    .put(`/feeds/lounge/hide/${parentFeedId}`)
    .then(response => {
      if (response.data.canAccess) {
        dispatch({
          type: FEED.POST.HIDE_FROM_LOUNGE.SUCCESS,
          payload: { feedId }
        });
      } else {
        sendNotification("You are not authorized to hide this post", "danger", "Hide Lounge Post");
      }
    })
    .catch(error => {
      dispatch({ type: FEED.POST.HIDE_FROM_LOUNGE.FAILURE, error });
    });
};

export const updatePostBoost = (feedId: string, isBoosted: boolean) => dispatch => {
  dispatch({ type: FEED.POST.UPDATE_BOOST.REQUEST });

  axios
    .patch(`/feeds/${feedId}/boost`, { boosted: isBoosted })
    .then(() => {
      const notificationMessage = isBoosted ? "Post has been boosted" : "Boost has been removed from post";
      sendNotification(notificationMessage, "success", "Boost Post");

      dispatch({ type: FEED.POST.UPDATE_BOOST.SUCCESS, payload: { feedId, isBoosted } });
    })
    .catch(error => {
      dispatch({ type: FEED.POST.UPDATE_BOOST.FAILURE, error });
    });
};

export const reportPost = (parentFeedId: string, feedId: string, description: string) => dispatch => {
  dispatch({ type: FEED.POST.REPORT.REQUEST });

  axios
    .put("/feeds/reportLoungePost", { reportPost: true, loungeFeedId: parentFeedId, description })
    .then(response => {
      const { data } = response;
      if (data.success) {
        sendNotification(data.message ? data.message : "", data.flashType, "Post Reported");
        dispatch({
          type: FEED.POST.REPORT.SUCCESS,
          payload: {
            feedId
          }
        });
      } else {
        dispatch({
          type: FEED.POST.REPORT.FAILURE
        });
        sendNotification("Error", "danger", "Something went wrong while reporting the post. Please try again later");
      }
    })
    .catch(error => {
      dispatch({
        type: FEED.POST.REPORT.FAILURE,
        error
      });
    });
};

export const getPostToEdit = feedId => dispatch => {
  return apolloClient
    .query<FeedPost, PostVars>({
      query: GET_POST,
      variables: { feedId },
      fetchPolicy: "no-cache"
    })
    .then(result => {
      dispatch({
        type: FEED.GET.EDIT_POST.REQUEST,
        payload: {
          post: result.data.getPostById
        }
      });
    })
    .catch(error => {
      dispatch({ type: FEED.GET.EDIT_POST.FAILURE, error });
      dispatch(setErrors(error));
    });
};

export const closePostEdit = () => dispatch => {
  dispatch({
    type: FEED.GET.EDIT_POST.SUCCESS
  });
};

export const createPost = (body: PostInput) => dispatch => {
  dispatch({
    type: FEED.POST.CREATE_POST.REQUEST
  });

  return apolloClient
    .mutate<FeedPost, CreatePostInput>({
      mutation: CREATE_POST,
      variables: { body }
    })
    .then(result => {
      dispatch({
        type: FEED.POST.CREATE_POST.SUCCESS,
        payload: {
          post: result.data.createPost
        }
      });
      analytics.track("POST_CREATE", {
        homeFeedId: result.data.createPost.parentFeedId
      });

      analytics.track("POST_CREATE", {
        mediaType: body?.file?.contentType || body?.giphy.type || "",
        giphy: !isEmpty(body?.giphy),
        mentions: body?.mentions?.length > 0,
        twitchClip: !!body?.twitchClip,
        youtubeClip: !!body?.youtubeVideo,
        taggedGame: body?.associatedGame || ""
      });
      sendNotification("Post created successfully ", "success", "Post");
    })
    .catch(error => {
      dispatch({ type: FEED.POST.CREATE_POST.FAILURE, error });
      dispatch(setErrors(error));
    });
};

export const createSchedulePost = (body: PostInput) => dispatch => {
  dispatch({
    type: FEED.POST.SCHEDULE_POST.REQUEST
  });

  return apolloClient
    .mutate<FeedPost, CreatePostInput>({
      mutation: CREATE_SCHEDULE_POST,
      variables: { body }
    })
    .then(result => {
      dispatch({
        type: FEED.POST.SCHEDULE_POST.SUCCESS,
        payload: {
          post: result.data.createSchedulePost
        }
      });
      sendNotification("Post scheduled successfully", "success", "Post");
    })
    .catch(error => {
      dispatch({ type: FEED.POST.SCHEDULE_POST.FAILURE, error });
      dispatch(setErrors(error));
    });
};

export const updatePost = (feedId: string, body: UpdatePostInput) => dispatch => {
  dispatch({
    type: FEED.POST.UPDATE_POST.REQUEST
  });

  return apolloClient
    .mutate<FeedPost, PostVars>({
      mutation: UPDATE_POST,
      variables: { feedId, body }
    })
    .then(result => {
      dispatch({
        type: FEED.POST.UPDATE_POST.SUCCESS,
        payload: {
          post: result.data.updatePost
        }
      });
      sendNotification("Post updated successfully", "success", "Post");
    })
    .catch(error => {
      dispatch({ type: FEED.POST.UPDATE_POST.FAILURE, error });
      dispatch(setErrors(error));
    });
};

export const setFeedpostToExpand = (post: Post) => dispatch => {
  dispatch({
    type: FEED.POST.EXPAND_POST.SUCCESS,
    payload: {
      post
    }
  });
};

export const toggleHypeUserRequest = (feedId: string) => dispatch => {
  dispatch({
    type: FEED.POST.HYPE_USER.REQUEST,
    payload: {
      feedId
    }
  });
};

export const getPostHypedUsers = (feedId: string, page: number, limit: number) => dispatch => {
  return apolloClient
    .mutate<HypeUserData, HypeUserVar>({
      mutation: WHO_HYPED_POST,
      variables: { feedId, page, limit }
    })
    .then(result => {
      const { docs, ...paginationParams } = result.data.getFeedHypedUsers;
      dispatch({
        type: FEED.POST.HYPE_USER.SUCCESS,
        payload: {
          feedId,
          hypeUsers: { docs, pagination: { ...paginationParams } }
        }
      });
    })
    .catch(error => {
      dispatch({
        type: FEED.POST.HYPE_USER.FAILURE,
        error
      });
      dispatch(setErrors(error));
    });
};

export const clearHypeUser = (feedId: string, commentId?: string) => dispatch => {
  dispatch({
    type: FEED.POST.HYPE_USER.CLEAR,
    payload: {
      feedId,
      commentId
    }
  });
};
