import { TOGGLE_FEATURE } from "./types";

/**
 @module flagSmithActions
 @category Actions
 */

export const toggleFeatures = features => dispatch => {
  dispatch({ type: TOGGLE_FEATURE, features });
};
