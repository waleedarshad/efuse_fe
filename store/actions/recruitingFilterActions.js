import {
  UPDATE_RECURITING,
  UPDATE_RECRUITING_SEARCH_OBJECT,
  UPDATE_RECRUITING_FILTER_VALUE,
  ADD_RECRUITING_FILTER,
  UPDATE_RECRUITING_FILTER,
  CLEAR_RECRUITING_FILTERS
} from "./types";
import { getRecruiting, getStarredRecruiting } from "./recruitingActions";
import { search } from "./commonActions";
/**
 @module recruitingFilterActions
 @category Actions
 */

export const updateSearchObject = searchObject => dispatch => {
  dispatch({
    type: UPDATE_RECRUITING_SEARCH_OBJECT,
    searchObject
  });
};
export const searchRecruits = (searchObject, filters, limit) => dispatch => {
  analytics.track("PIPELINE_PROSPECTS_SEARCHED");
  dispatch(
    search(
      "/recruitment_profile/all",
      searchObject,
      filters,
      UPDATE_RECURITING,
      "recruitmentProfiles",
      updateFilters,
      limit
    )
  );
};

export const searchStarredRecruits = (searchObject, filters, limit) => dispatch => {
  analytics.track("PIPELINE_PROSPECTS_SEARCHED");
  dispatch(
    search(
      "/recruitment_profile/star",
      searchObject,
      filters,
      UPDATE_RECURITING,
      "recruitmentProfiles",
      updateFilters,
      limit
    )
  );
};

const updateFilters = filters => dispatch => {
  dispatch({
    type: ADD_RECRUITING_FILTER,
    payload: filters
  });
};

export const clearFilters = () => dispatch => {
  analytics.track("PIPELINE_PROSPECTS_SEARCH_CLEARED");
  dispatch(getRecruiting());
  dispatch({
    type: CLEAR_RECRUITING_FILTERS
  });
};

export const clearStarredFilters = () => dispatch => {
  analytics.track("PIPELINE_PROSPECTS_SEARCH_CLEARED");
  dispatch(getStarredRecruiting());
  dispatch({
    type: CLEAR_RECRUITING_FILTERS
  });
};

export const updateFilterValue = (key, value) => dispatch => {
  dispatch({
    type: UPDATE_RECRUITING_FILTER_VALUE,
    key,
    value
  });
};

export const removeFilter = key => dispatch => {
  dispatch({
    type: UPDATE_RECRUITING_FILTER_VALUE,
    key,
    value: ""
  });
};

export const updateAppliedFilter = key => dispatch => {
  dispatch({
    type: UPDATE_RECRUITING_FILTER,
    key
  });
};
