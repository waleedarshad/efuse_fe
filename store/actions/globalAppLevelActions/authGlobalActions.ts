import { attemptToRefreshToken, getCurrentUser as getStoredCurrentUser } from "../../../helpers/Auth0Helper";
import { connectSocket } from "./websocketGlobalActions";
import { isUserLoggedIn } from "../../../helpers/AuthHelper";
import { getCurrentUser, setCurrentUser } from "../common/userAuthActions";

export const refreshAuthToken = async () => {
  const { token } = await attemptToRefreshToken();

  connectSocket(token);

  return { success: true, token };
};

export const verifyLogin = () => dispatch => {
  if (!isUserLoggedIn()) {
    return dispatch(setCurrentUser({}));
  }

  let storedCurrentUser = {};
  try {
    storedCurrentUser = JSON.parse(getStoredCurrentUser());
  } catch (error) {
    // if for some reason currentUser cookie is not parsable we get the user from BE
    return dispatch(getCurrentUser());
  }

  return dispatch(setCurrentUser(storedCurrentUser));
};
