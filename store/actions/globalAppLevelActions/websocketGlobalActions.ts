import { getToken } from "../../../helpers/Auth0Helper";

// eslint-disable-next-line import/prefer-default-export
export const connectSocket = (token: string) => {
  // eslint-disable-next-line global-require
  const { getSocket } = require("../../../websockets");

  const socket = getSocket();
  const splittedToken = token.split(" ")[1];

  socket.io.opts.query = { token: splittedToken };
  socket.io.connect();

  socket.on("reconnect_attempt", () => {
    socket.io.opts.query = {
      token: getToken()
    };
  });
};
