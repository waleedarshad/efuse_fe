import getConfig from "next/config";

import {
  GET_UNREAD_MESSAGE_COUNT_FAILED,
  GET_UNREAD_MESSAGE_COUNT_REQUEST,
  GET_UNREAD_MESSAGE_COUNT_SUCCESS
} from "../types";
import { StreamChatEnum } from "../../../enums/stream-chat.enum";

const { publicRuntimeConfig } = getConfig();

const { streamChatKey } = publicRuntimeConfig;

// eslint-disable-next-line import/prefer-default-export
export const getUnreadMessageCount = currentUser => async dispatch => {
  let client = null;

  const currentUserId = currentUser._id || currentUser.id;
  const userToken = currentUser.streamChatToken;
  dispatch({
    type: GET_UNREAD_MESSAGE_COUNT_REQUEST
  });

  try {
    analytics.track(StreamChatEnum.ANALYTICS_EVENT);

    const { StreamChat } = await import("stream-chat");

    client = new StreamChat(streamChatKey);

    const response = await client.setUser(
      {
        id: currentUserId,
        name: currentUser.name,
        image: currentUser.profilePicture.url
      },
      userToken
    );

    dispatch({
      type: GET_UNREAD_MESSAGE_COUNT_SUCCESS,
      payload: { unreadMessageCount: response.me.total_unread_count }
    });
  } catch (error) {
    dispatch({ type: GET_UNREAD_MESSAGE_COUNT_FAILED, error: error.message });
  } finally {
    if (client) {
      await client.disconnect();
    }
  }
};
