import Cookies from "js-cookie";
import { CHANGE_TO_LIGHT, CHANGE_TO_DARK } from "./types";
import { lightTheme, darkTheme } from "../../styles/themes";

/**
 @module themeActions
 @category Actions
 */

export const changeToLightTheme = () => dispatch => {
  Cookies.set("theme", "light");
  dispatch({
    type: CHANGE_TO_LIGHT,
    payload: lightTheme
  });
};

export const changeToDarkTheme = () => dispatch => {
  Cookies.set("theme", "dark");
  dispatch({
    type: CHANGE_TO_DARK,
    payload: darkTheme
  });
};
