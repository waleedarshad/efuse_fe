import { SET_VIDEO, REMOVE_VIDEO } from "./types";

/**
 @module hitmarkerActions
 @category Actions
 */

export const setVideo = video => dispatch => {
  if (video) {
    dispatch({
      type: SET_VIDEO,
      payload: video
    });
    // analytics.track("GLOBAL_VIDEO_PINNED", {
    //   video
    // });
  }
};

export const removeVideo = () => dispatch => {
  dispatch({
    type: REMOVE_VIDEO
  });
};
