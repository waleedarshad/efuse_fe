import axios from "axios";

import {
  ADD_OPPORTUNITY_FILTER,
  CLEAR_OPPORTUNITY_FILTERS,
  UPDATE_OPPORTUNITIES,
  UPDATE_OPPORTUNITY_FILTER_VALUE,
  SET_MAX_MONEY_INCLUDED,
  UPDATE_SEARCH_OBJECT
} from "./types";
import { toggleLoader } from "./loaderActions";
import { setErrors } from "./errorActions";
import { getOpportunities } from "./opportunityActions";
import { search } from "./commonActions";

/**
 @module opportunityFilterActions
 @category Actions
 */

export const updateSearchObject = searchObject => dispatch => {
  dispatch({
    type: UPDATE_SEARCH_OBJECT,
    searchObject
  });
};
export const searchOpportunity = (searchObject, filters) => dispatch => {
  dispatch(search("/opportunities", searchObject, filters, UPDATE_OPPORTUNITIES, "opportunities", updateFilters));
};

export const searchPublicOpportunities = (searchObject, filters) => dispatch => {
  dispatch(
    search("/public/opportunities", searchObject, filters, UPDATE_OPPORTUNITIES, "opportunities", updateFilters)
  );
};
// added separate action method for my opportunities to avoid multiple checks for the filter helper
export const searchMyOpportunity = (searchObject, filters) => dispatch => {
  dispatch(search("/opportunities/owned", searchObject, filters, UPDATE_OPPORTUNITIES, "opportunities", updateFilters));
};
export const searchAppliedOpportunity = (searchObject, filters) => dispatch => {
  dispatch(
    search("/opportunities/applied", searchObject, filters, UPDATE_OPPORTUNITIES, "opportunities", updateFilters)
  );
};
const updateFilters = filters => dispatch => {
  dispatch({
    type: ADD_OPPORTUNITY_FILTER,
    payload: filters
  });
};

export const clearFilters = () => dispatch => {
  dispatch(getOpportunities());
  dispatch({
    type: CLEAR_OPPORTUNITY_FILTERS
  });
};

export const updateFilterValue = (key, value) => dispatch => {
  dispatch({
    type: UPDATE_OPPORTUNITY_FILTER_VALUE,
    key,
    value
  });
};

export const removeFilter = key => dispatch => {
  dispatch({
    type: UPDATE_OPPORTUNITY_FILTER_VALUE,
    key,
    value: ""
  });
};

export const getMaxMoneyIncluded = () => dispatch => {
  dispatch(toggleLoader(true));
  axios
    .get("/opportunities/get_max_money_included")
    .then(response => {
      dispatch({
        type: SET_MAX_MONEY_INCLUDED,
        payload: response.data.opportunity
      });
      dispatch(toggleLoader(false));
    })
    .catch(error => {
      dispatch(setErrors(error.response.data));
      dispatch(toggleLoader(false));
    });
};
