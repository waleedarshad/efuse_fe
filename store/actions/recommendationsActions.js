import axios from "axios";
import { GET_FOLLOWER_RECOMMENDATIONS } from "./types";
import { setErrors } from "./errorActions";

/**
 @module recommendationsActions
 @category Actions
 */

export const getFollowerRecommendations = (isPublic = false) => dispatch => {
  const url = "recommendations/followers";
  const apiUrl = isPublic ? `public/${url}` : url;
  axios
    .get(apiUrl)
    .then(response => {
      dispatch({
        type: GET_FOLLOWER_RECOMMENDATIONS,
        payload: response.data.users
      });
    })
    .catch(error => {
      dispatch(setErrors(error));
    });
};
