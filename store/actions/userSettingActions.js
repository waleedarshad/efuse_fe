import axios from "axios";

import { setErrors } from "./errorActions";
import { GET_MEDIA_SETTINGS } from "./types";

/**
 * @summary Retrieve the current user's media settings
 * @method
 * @module userSettingActions
 * @category Actions
 */
export const getMediaSettings = () => async dispatch => {
  try {
    const response = await axios.get("/users/settings/media");
    dispatch({ type: GET_MEDIA_SETTINGS, mediaSettings: response.data.media });
  } catch (error) {
    setErrors(error.response?.data || error);
  }
};

/**
 * @summary Retrieve the current user's media settings
 * @method
 * @param {Object} mediaSettings
 * @module userSettingActions
 * @category Actions
 */
export const updateMediaSettings = mediaSettings => async dispatch => {
  try {
    const response = await axios.patch("/users/settings/media", { media: mediaSettings });
    dispatch({ type: GET_MEDIA_SETTINGS, mediaSettings: response.data.media });
  } catch (error) {
    setErrors(error.response?.data || error);
  }
};
