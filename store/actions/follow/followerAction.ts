import axios from "axios";
import { setErrors } from "../errorActions";
import { FEED } from "../types";
import { toggleFollowerBtn } from "../followerActions";

export const followHypedUser = (id, type, feedId, commentId, parentId) => dispatch => {
  dispatch(toggleFollowerBtn(true));
  axios
    .get(`/followers/follow/${id}`)
    .then(response => {
      dispatch({
        type: FEED.POST.FOLLOW_HYPE_USER.SUCCESS,
        payload: { userId: id, isFollowed: response.data.isFollowed, type, feedId, commentId, parentId }
      });
      dispatch(toggleFollowerBtn(false));
    })
    .catch(error => {
      dispatch(toggleFollowerBtn(false));
      dispatch(setErrors(error));
    });
};

export const unFollowHypedUser = (id, type, feedId, commentId, parentId) => dispatch => {
  dispatch(toggleFollowerBtn(true));

  axios
    .delete(`/followers/unfollow/${id}`)
    .then(response => {
      dispatch({
        type: FEED.POST.FOLLOW_HYPE_USER.SUCCESS,
        payload: { userId: id, isFollowed: response.data.isFollowed, type, feedId, commentId, parentId }
      });
      dispatch(toggleFollowerBtn(false));
    })
    .catch(error => {
      dispatch(setErrors(error));
      dispatch(toggleFollowerBtn(false));
    });
};
