import mockAxios from "axios";
import { getMockStore } from "../../common/testUtils";
import { getApplicantsNew } from "./applicantActions";
import { GET_APPLICANTS_REQUEST, GET_APPLICANTS_SUCCESS, GET_APPLICANTS_FAILURE, INVALID_REQUEST } from "./types";

describe("applicantActions", () => {
  let store;

  describe("getApplicantsNew", () => {
    const applicantsData = {
      users: {
        docs: ["applicant1", "applicant2"],
        totalDocs: 5,
        hasNextPage: true,
        page: 1
      }
    };

    describe("getApplicants successful call", () => {
      beforeEach(() => {
        mockAxios.get.mockResolvedValue({ data: applicantsData });

        store = getMockStore({});
        store.dispatch(getApplicantsNew(1, "5ef05e2a9973f72daf2315e9", "noaction"));
      });

      it("makes the correct network call", () => {
        expect(mockAxios.get).toHaveBeenCalledWith("/applicants/5ef05e2a9973f72daf2315e9", {
          params: { page: 1, pageSize: 9, limit: 10, filters: {}, query: "noaction" }
        });
      });

      it("dispatches the correct action", () => {
        expect(store.getActions()).toEqual([
          { type: GET_APPLICANTS_REQUEST, payload: true },
          {
            type: GET_APPLICANTS_SUCCESS,
            payload: ["applicant1", "applicant2"],
            page: 1,
            pagination: { totalDocs: 5, hasNextPage: true, page: 1 }
          }
        ]);
      });
    });

    describe("getApplicants failed call", () => {
      const error = {
        response: {
          status: 422
        }
      };

      beforeEach(() => {
        mockAxios.get.mockRejectedValue(error);

        store = getMockStore({});
        store.dispatch(getApplicantsNew(1, "5ef05e2a9973f72daf2315e2", "noaction"));
      });

      it("dispatches the correct actions", () => {
        expect(store.getActions()).toEqual([
          { type: GET_APPLICANTS_REQUEST, payload: true },
          { type: GET_APPLICANTS_FAILURE },
          { type: INVALID_REQUEST, responseCode: 422 }
        ]);
      });
    });
  });
});
