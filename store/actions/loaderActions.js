import { TOGGLE_LOADER, CONTENT_LOADER, CHAT_THREAD_LOADER } from "./types";

export const toggleLoader = show => dispatch => {
  dispatch({
    type: TOGGLE_LOADER,
    show
  });
};

export const bindLoader = method => dispatch => {
  dispatch(toggleLoader(true));
  dispatch(method);
  dispatch(toggleLoader(false));
};

export const toggleContentLoader = (show, section) => dispatch => {
  dispatch({
    type: CONTENT_LOADER,
    show,
    section
  });
};
export const toggleChatLoader = show => dispatch => {
  dispatch({
    type: CHAT_THREAD_LOADER,
    show
  });
};

export const showContentLoader = section => dispatch => {
  dispatch({
    type: CONTENT_LOADER,
    show: true,
    section
  });
};

export const hideContentLoader = section => dispatch => {
  dispatch({
    type: CONTENT_LOADER,
    show: false,
    section
  });
};
