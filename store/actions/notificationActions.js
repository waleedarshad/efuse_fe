import { SEND_NOTIFICATION, INIT_NOTIFICATION } from "./types";

/**
 @module notificationActions
 @category Actions
 */

export const dispatchNotification = (type, title, message) => dispatch => {
  // Suppress sending out status code 401 alerts.  Bad UX.
  if (message == "Request failed with status code 401") {
    console.error("Request failed with status code 401");
    return;
  }
  dispatch({
    type: SEND_NOTIFICATION,
    payload: {
      notify: true,
      title,
      type,
      message
    }
  });
  setTimeout(() => {
    dispatch({
      type: INIT_NOTIFICATION
    });
  }, 3000);
};
