import axios from "axios";
import { setErrors } from "./errorActions";
import { GET_HITMARKERS } from "./types";

/**
 @module hitmarkerActions
 @category Actions
 */

export const getHitmarkers = (page = 1, pageSize = 10) => dispatch => {
  axios
    .get(`/hitmarkers?page=${page}&pageSize=${pageSize}`)
    .then(response => {
      const {} = response.data;
      const { docs, ...paginationParams } = response.data;
      dispatch({
        type: GET_HITMARKERS,
        hitmarkers: docs,
        pagination: paginationParams
      });
    })
    .catch(error => {
      dispatch(setErrors(error.response));
    });
};
