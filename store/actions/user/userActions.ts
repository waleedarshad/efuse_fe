import { initializeApollo } from "../../../config/apolloClient";
import { BAN_USER } from "../../../graphql/UserQuery";
import { sendNotification } from "../../../helpers/FlashHelper";
import { setErrors } from "../errorActions";

const apolloClient = initializeApollo();

interface BanUserResponse {
  banUser?: {
    success: boolean;
  };
}
interface BanUserVar {
  idOrUsername: string;
}

// eslint-disable-next-line import/prefer-default-export
export const banUser = (userId: string) => dispatch => {
  return apolloClient
    .mutate<BanUserResponse, BanUserVar>({
      mutation: BAN_USER,
      variables: {
        idOrUsername: userId
      }
    })
    .then(result => {
      if (result.data.banUser.success) {
        sendNotification("User ban successfully", "success", "Ban User");
        window.location.reload();
      }
    })
    .catch(error => {
      dispatch(setErrors(error));
    });
};
