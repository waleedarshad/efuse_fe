import axios from "axios";
import { EXTERNAL_AUTH_SERVICES } from "../../../common/externalAuthServices";
import { sendNotification } from "../../../helpers/FlashHelper";
import { EXTERNAL_AUTH } from "./types";

const getAuthStatusForLoggedUser = service => (dispatch, getState) => {
  const loggedUserId = getState().auth.currentUser.id;

  dispatch({ type: EXTERNAL_AUTH.STATUS.LOGGED_USER.GET.REQUEST });

  axios
    .get(`/external_auth/status/${loggedUserId}/service/${service}`)
    .then(response => {
      dispatch({
        type: EXTERNAL_AUTH.STATUS.LOGGED_USER.GET.SUCCESS,
        payload: {
          service,
          isAuthenticated: response.data.items[0]
        }
      });
    })
    .catch(error => {
      dispatch({ type: EXTERNAL_AUTH.STATUS.LOGGED_USER.GET.FAILURE, error: error.message });
    });
};

const getAuthStatusForPortfolioUser = (service, userId) => (dispatch, getState) => {
  const portfolioUserId = userId || getState().user.currentUser._id;

  dispatch({ type: EXTERNAL_AUTH.STATUS.PORTFOLIO_USER.GET.REQUEST });

  axios
    .get(`/external_auth/status/${portfolioUserId}/service/${service}`)
    .then(response => {
      dispatch({
        type: EXTERNAL_AUTH.STATUS.PORTFOLIO_USER.GET.SUCCESS,
        payload: {
          service,
          isAuthenticated: response.data.items[0]
        }
      });
    })
    .catch(error => {
      dispatch({ type: EXTERNAL_AUTH.STATUS.PORTFOLIO_USER.GET.FAILURE, error: error.message });
    });
};

const deleteLinkForService = service => (dispatch, getState) => {
  const currentUserId = getState().auth.currentUser.id;

  dispatch({ type: EXTERNAL_AUTH.LINK.DELETE.REQUEST });

  axios
    .delete(`/external_auth/link/${currentUserId}/service/${service}`)
    .then(() => {
      sendNotification("Successfully Unlinked!", "warning", service.toUpperCase());
      dispatch({
        type: EXTERNAL_AUTH.LINK.DELETE.SUCCESS,
        payload: {
          service
        }
      });
    })
    .catch(error => {
      dispatch({ type: EXTERNAL_AUTH.LINK.DELETE.FAILURE, error: error.message });
    });
};

export const getAimlabAuthStatusForLoggedUser = () => dispatch => {
  dispatch(getAuthStatusForLoggedUser(EXTERNAL_AUTH_SERVICES.STATESPACE));
};

export const getValorantAuthStatusForLoggedUser = () => dispatch => {
  dispatch(getAuthStatusForLoggedUser(EXTERNAL_AUTH_SERVICES.RIOT));
};

export const getAimlabAuthStatusForPortfolioUser = userId => dispatch => {
  dispatch(getAuthStatusForPortfolioUser(EXTERNAL_AUTH_SERVICES.STATESPACE, userId));
};

export const getValorantAuthStatusForPortfolioUser = userId => dispatch => {
  dispatch(getAuthStatusForPortfolioUser(EXTERNAL_AUTH_SERVICES.RIOT, userId));
};

export const deleteLinkForAimlab = () => dispatch => {
  dispatch(deleteLinkForService(EXTERNAL_AUTH_SERVICES.STATESPACE));
};

export const deleteLinkForValorant = () => dispatch => {
  dispatch(deleteLinkForService(EXTERNAL_AUTH_SERVICES.RIOT));
};
