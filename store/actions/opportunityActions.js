import axios from "axios";
import getConfig from "next/config";
import { SHARE_LINK_KINDS } from "../../components/ShareLinkModal/ShareLinkModal";
import { isUserLoggedIn } from "../../helpers/AuthHelper";
import { sendNotification, setFlash } from "../../helpers/FlashHelper";
import { getFullOpportunityUrl } from "../../helpers/UrlHelper";
import { getPaginatedCollection } from "./commonActions";
import { setErrors } from "./errorActions";
import { toggleContentLoader, toggleLoader } from "./loaderActions";
import { shareLinkModal } from "./shareAction";
import {
  ADD_OPPORTUNITY,
  BOOST_OPPORTUNITY,
  CHANGE_OPPORTUNITY_PUBLISH_STATUS,
  CHANGE_OPPORTUNITY_STATUS,
  CLEAR_OPPORTUNITIES,
  CLEAR_OPPORTUNITY,
  GET_ADMIN_OPPORTUNITIES,
  GET_ADMIN_OPPORTUNITIES_FAILURE,
  GET_ADMIN_OPPORTUNITIES_REQUEST,
  GET_ADMIN_OPPORTUNITIES_SUCCESS,
  GET_OPPORTUNITIES,
  GET_OPPORTUNITIES_FAILURE,
  GET_OPPORTUNITIES_REQUEST,
  GET_OPPORTUNITIES_SUCCESS,
  GET_OPPORTUNITY,
  GET_OPPORTUNITY_REQUIRED_FIELDS,
  GET_RECENT_OPPORTUNITIES_BY_TYPE,
  INVALID_REQUEST,
  OPPORTUNITY_CURRENT_PAGE,
  OPP_SHORT_NAME_ERROR,
  PROMOTED_OPPORTUNITIES,
  SET_RECENT_OPPORTUNITIES,
  SET_RECOMMENDATIONS_OPPORTUNITIES
} from "./types";

const { publicRuntimeConfig } = getConfig();
const { feBaseUrl } = publicRuntimeConfig;

/**
 * @module opportunityActions
 * @category Actions
 */

export const createOpportunity = (opportunityData, router) => dispatch => {
  dispatch(toggleLoader(true));
  axios
    .post("/opportunities", opportunityData)
    .then(response => {
      const { message, opportunity } = response.data;

      dispatch({ type: ADD_OPPORTUNITY, opportunity });
      sendNotification(message, "success", "Success");

      analytics.track("OPPORTUNITY_CREATE", {
        opportunity
      });

      dispatch(toggleLoader(false));
      router.push(`/o/${opportunity.shortName}`);

      dispatch(
        shareLinkModal(
          true,
          "Share Opportunity",
          `${feBaseUrl}${getFullOpportunityUrl(opportunity)}`,
          SHARE_LINK_KINDS.OPPORTUNITY
        )
      );
    })
    .catch(error => {
      dispatch(setErrors(error.response ? error.response.data : error));
      dispatch(toggleLoader(false));
    });
};

export const getOpportunities = (
  page = 1,
  pageSize = 9,
  filters = {},
  displayLoader = true,
  useRootPath = false,
  id = "",
  customPath = "",
  customTypeReducer = ""
) => dispatch => {
  dispatch(
    getPaginatedCollection(
      page,
      pageSize,
      filters,
      displayLoader,
      "/opportunities",
      "opportunities",
      useRootPath ? GET_ADMIN_OPPORTUNITIES : GET_OPPORTUNITIES,
      toggleLoader,
      setErrors,
      "",
      useRootPath,
      false,
      "",
      false,
      id,
      customPath,
      customTypeReducer
    )
  );
};

export const getOpportunitiesNew = (page = 1, filters = {}, url = "/opportunities", id = "") => dispatch => {
  dispatch({ type: GET_OPPORTUNITIES_REQUEST, payload: page === 1 });
  axios
    .get(url, { params: { page, pageSize: 9, filters, id } })
    .then(response => {
      const { docs, ...rest } = response.data.opportunities;
      dispatch({ type: GET_OPPORTUNITIES_SUCCESS, page, payload: docs, pagination: rest });
    })
    .catch(error => {
      dispatch({ type: GET_OPPORTUNITIES_FAILURE });
      if (error.response?.status === 422 || error.response?.status === 400) {
        dispatch({
          type: INVALID_REQUEST,
          responseCode: error.response.status
        });
      }
      setErrors(error.response ? error.response.data : error);
    });
};

export const getAdminOpportunities = (page = 1, filters = {}, ownerId) => dispatch => {
  dispatch({ type: GET_ADMIN_OPPORTUNITIES_REQUEST });
  axios
    .get("/opportunities/owned", { params: { page, pageSize: 20, filters, ownerId } })
    .then(response => {
      const { docs, ...rest } = response.data.opportunities;
      dispatch({ type: GET_ADMIN_OPPORTUNITIES_SUCCESS, page, payload: docs, pagination: rest });
    })
    .catch(error => {
      dispatch({ type: GET_ADMIN_OPPORTUNITIES_FAILURE });
      if (error.response?.status === 422 || error.response?.status === 400) {
        dispatch({
          type: INVALID_REQUEST,
          responseCode: error.response.status
        });
      }
      setErrors(error.response ? error.response.data : error);
    });
};

export const getOpportunitiesByType = (isPublic = false) => dispatch => {
  dispatch(toggleLoader(true));
  axios
    .get(`${isPublic ? "/public" : ""}/opportunities/recent`)
    .then(response => {
      dispatch({
        type: GET_RECENT_OPPORTUNITIES_BY_TYPE,
        payload: response.data
      });
      dispatch(toggleLoader(false));
    })
    .catch(error => {
      dispatch(setErrors(error.response));
      dispatch(toggleLoader(false));
    });
};

export const getAllOpportunities = url => dispatch => {
  const promise = new Promise((resolve, reject) => {
    axios
      .get(url)
      .then(res => {
        dispatch(toggleLoader(false));
        return resolve(res.data.opportunities);
      })
      .catch(error => {
        dispatch(setErrors(error.response ? error.response.data : error));
        dispatch(toggleLoader(false));
        return reject();
      });
  });
  return promise;
};

// Delete a Opportunity record.
export const deleteOpportunity = (url, UpdateGrid) => dispatch => {
  axios
    .delete(url)
    .then(() => {
      analytics.track("OPPORTUNITY_DELETE", {
        url
      });
      UpdateGrid();
    })
    .catch(error => {
      dispatch(setErrors(error.response));
      dispatch(toggleLoader(false));
    });
};

export const getOpportunityById = id => dispatch => {
  dispatch(toggleLoader(true));
  axios
    .get(`${isUserLoggedIn() ? "" : "/public"}/opportunities/show/${id}`)
    .then(response => {
      dispatch({
        type: GET_OPPORTUNITY,
        payload: response.data.opportunity
      });
      dispatch(toggleLoader(false));
    })
    .catch(error => {
      dispatch(setErrors(error.response ? error.response.data : error));
      dispatch(toggleLoader(false));
    });
};

export const getOpportunityBySlug = slug => dispatch => {
  dispatch(toggleLoader(true));
  axios
    .get(`${isUserLoggedIn() ? "" : "/public"}/opportunities/byslug/${slug}`)
    .then(response => {
      dispatch({
        type: GET_OPPORTUNITY,
        payload: response.data.opportunity
      });
      dispatch(toggleLoader(false));
    })
    .catch(error => {
      dispatch(setErrors(error.response ? error.response.data : error));
      dispatch(toggleLoader(false));
    });
};

export const setOpportunity = opportunity => dispatch => {
  dispatch({
    type: GET_OPPORTUNITY,
    payload: opportunity
  });
};

export const getOpportunityToEdit = (id, router) => dispatch => {
  dispatch(toggleLoader(true));
  dispatch(clearOpportunity());
  axios
    .get(`/opportunities/edit/${id}`)
    .then(response => {
      const { canEdit, opportunity } = response.data;
      if (canEdit) {
        dispatch({ type: GET_OPPORTUNITY, payload: opportunity });
        dispatch(toggleLoader(false));
      } else {
        dispatch(toggleLoader(false));
        setFlash("admin");
        router.push("/opportunities");
      }
    })
    .catch(error => {
      dispatch(setErrors(error.response ? error.response.data : error));
      dispatch(toggleLoader(false));
    });
};

export const clearOpportunity = () => dispatch => dispatch({ type: CLEAR_OPPORTUNITY });

export const updateOpportunity = (id, formData, router) => dispatch => {
  const shortName = formData.get("shortName");
  dispatch(toggleLoader(true));
  axios
    .put(`/opportunities/update/${id}`, formData)
    .then(response => {
      const { canEdit, message } = response.data;
      if (canEdit) {
        sendNotification(message, "success", "Success");
        dispatch(toggleLoader(false));

        analytics.track("OPPORTUNITY_EDIT", { opportunity: formData });
        router.push(`/o/${shortName}`);
      } else {
        setFlash("admin");
        dispatch(toggleLoader(false));

        router.push(`/o/${shortName}`);
      }
    })
    .catch(error => {
      dispatch(setErrors(error.response ? error.response.data : error));
      dispatch(toggleLoader(false));
    });
};

export const recentOpportunitiesExcept = (id, pageSize) => dispatch => {
  dispatch(toggleContentLoader(true, "recentOpportunities"));
  const filters = id ? { _id: { $ne: id } } : {};
  axios
    .get(`/public/opportunities?pageSize=${pageSize || 5}&filters=${JSON.stringify(filters)}`)
    .then(response => {
      dispatch({
        type: SET_RECENT_OPPORTUNITIES,
        payload: response.data.opportunities.docs
      });
      dispatch(toggleContentLoader(false, "recentOpportunities"));
    })
    .catch(error => {
      dispatch(setErrors(error.response ? error.response.data : error));
      dispatch(toggleContentLoader(false, "recentOpportunities"));
    });
};

export const setCurrentPage = view => dispatch => {
  dispatch({ type: OPPORTUNITY_CURRENT_PAGE, view });
};

export const clearOpportunities = () => dispatch => {
  dispatch({ type: CLEAR_OPPORTUNITIES });
};
export const statusUpdate = (url, formData, statusType) => dispatch => {
  const promise = new Promise((resolve, reject) => {
    axios
      .put(url, formData)
      .then(response => {
        const { message } = response.data;
        dispatch({
          type: statusType === "status" ? CHANGE_OPPORTUNITY_STATUS : CHANGE_OPPORTUNITY_PUBLISH_STATUS,
          payload: response.data.updatedOpportunity
        });
        sendNotification(message, "success", "Status");

        return resolve();
      })
      .catch(error => {
        dispatch(setErrors(error.response));
        dispatch(toggleLoader(false));

        return reject();
      });
  });
  return promise;
};

export const toggleOppShortNameError = error => dispatch => {
  dispatch({
    type: OPP_SHORT_NAME_ERROR,
    error
  });
};

export const getPromotedOpportunities = (isPublic = false) => dispatch => {
  dispatch(toggleLoader(true));
  axios
    .get(`${isPublic ? "/public" : ""}/opportunities/promoted`)
    .then(response => {
      dispatch({
        type: PROMOTED_OPPORTUNITIES,
        payload: response.data
      });
      dispatch(toggleLoader(false));
    })
    .catch(error => {
      dispatch(setErrors(error.response ? error.response.data : error));
      dispatch(toggleLoader(false));
    });
};

export const getRecommendedOpportunities = (amount = 3) => dispatch => {
  axios
    .get("/recommendations/opportunities", {
      params: {
        amount
      }
    })
    .then(response => {
      dispatch({
        type: SET_RECOMMENDATIONS_OPPORTUNITIES,
        payload: response.data.opportunities
      });
    })
    .catch(error => {
      dispatch(setErrors(error.response ? error.response.data : error));
    });
};

export const getOpportunityRequiredFields = () => dispatch => {
  axios
    .get("/opportunity_requirements")
    .then(response => {
      dispatch({
        type: GET_OPPORTUNITY_REQUIRED_FIELDS,
        payload: response.data.requirements
      });
    })
    .catch(error => {
      dispatch(setErrors(error.response ? error.response.data : error));
    });
};

export const toggleOpportunityBoost = (opportunityId, boosted) => async dispatch => {
  try {
    await axios.patch(`/opportunities/${opportunityId}/boost`, { boosted });
    const msg = boosted ? "Opportunity has been boosted" : "Boost has been removed from opportunity";
    sendNotification(msg, "success", "Boost Opportunity");
    dispatch({ type: BOOST_OPPORTUNITY, opportunityId, boosted });
  } catch (error) {
    dispatch(setErrors(error?.response?.data));
  }
};
