import { getFlagForFeature } from "./featureFlagSelectors";

describe("featureFlagSelectors", () => {
  describe("getFlagForFeature", () => {
    it("gets the flag for the feature matching the feature name", () => {
      const state = {
        features: {
          superFeature: true
        }
      };

      expect(getFlagForFeature(state, "superFeature")).toBe(true);
    });
  });
});
