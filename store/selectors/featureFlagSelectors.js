// eslint-disable-next-line import/prefer-default-export
export const getFlagForFeature = (state, featureName) => state.features[featureName];
