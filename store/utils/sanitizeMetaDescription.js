import sanitizeHtml from "sanitize-html";

/**
 * This method is used to sanitize html of a string and apply limit of 120 characters to it.
 * @method
 * @param {string} description - String you want to sanitize and apply limit
 * @returns {string} String without html tags and of 120 characters
 */

const sanitizeMetaDescription = description => {
  let limitedSanitizedDescription;

  const sanitizedDescription = sanitizeHtml(description, {
    allowedTags: [],
    allowedAttributes: {}
  });

  if (sanitizedDescription.length > 160) {
    limitedSanitizedDescription = sanitizedDescription.substring(0, 160);
    limitedSanitizedDescription = limitedSanitizedDescription.concat("...");
  } else {
    limitedSanitizedDescription = sanitizedDescription;
  }

  return limitedSanitizedDescription;
};

export default sanitizeMetaDescription;
