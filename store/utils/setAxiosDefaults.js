import axios from "axios";

import getConfig from "next/config";

import { refreshAuthToken } from "../actions/globalAppLevelActions/authGlobalActions";
import { getToken, removeTokens } from "../../helpers/Auth0Helper";

const { publicRuntimeConfig } = getConfig();
const { defaultUrl } = publicRuntimeConfig;

const setAxiosDefaults = () => {
  let refreshTokenInProgress = false;
  let unauthorizedRequests = [];

  const onAccessTokenFetched = token => {
    unauthorizedRequests = unauthorizedRequests.filter(callback => callback(token));
  };

  const addUnauthorizedRequest = callback => {
    unauthorizedRequests.push(callback);
  };

  const retryRequest = originalRequest =>
    new Promise(resolve => {
      addUnauthorizedRequest(token => {
        const originalRequestParam = originalRequest;

        originalRequestParam.headers.Authorization = token;

        resolve(axios(originalRequestParam));
      });
    });

  const redirectUser = () => {
    // Since session is expired and refreshToken has failed, clearing cookies and localstorage would prevent the infinite loop.
    removeTokens();

    // Commented out by shuff because it is causing too many issues
    // router.push("/login");
  };

  // Setting Default URL
  axios.defaults.baseURL = defaultUrl;

  // Request Interceptor to set Bearer Token
  axios.interceptors.request.use(
    config => {
      const configParam = config;

      if (!config.headers.Authorization) {
        const token = getToken();
        if (token) {
          configParam.headers.Authorization = token;
        }
      }

      configParam.headers.AJS_ANONYMOUS_ID = localStorage?.ajs_anonymous_id; // eslint-disable-line camelcase
      configParam.headers.AJS_USER_ID = localStorage?.ajs_user_id; // eslint-disable-line camelcase

      return configParam;
    },
    error => Promise.reject(error)
  );

  // Response Interceptor to refresh token
  axios.interceptors.response.use(
    response => response,
    async errorParam => {
      const error = errorParam;

      const originalRequest = error.config;
      const errorResponse = error.response;
      const refreshTokenUrl = ["/api/auth/refreshToken", "/api/auth/refresh_auth0_token"];
      const unauthorizedRequest = errorResponse?.status === 401 && errorResponse?.data === "Unauthorized";

      // Rejecting if it is not a 401 error or not returned while refreshing token
      if (!unauthorizedRequest && !refreshTokenUrl.includes(originalRequest?.url)) {
        const errURL = originalRequest?.url || "Unknown URL";
        console.error(`Error getting response from backend: Message: ${error} / URL: ${errURL}`);

        return new Promise((resolve, reject) => {
          reject(error);
        });
      }
      let retryPromise = null;
      // Refreshing token when it is 401 && Unauthorized
      if (unauthorizedRequest) {
        retryPromise = retryRequest(originalRequest);

        // Refresh token only when refresh request in not made
        if (!refreshTokenInProgress) {
          // Set flag to store all failing request while token is refreshing
          refreshTokenInProgress = true;

          try {
            // Refresh Token
            const refreshResponse = await refreshAuthToken();

            // Update headers when token is successfully refreshed
            if (refreshResponse.success) {
              const { token } = refreshResponse;

              refreshTokenInProgress = false;

              axios.defaults.headers.common.Authorization = token;

              onAccessTokenFetched(token);
            }
          } catch (err) {
            // Refresh Token call fails and produces error
            if (!err.response?.data?.message) {
              error.response.data = { Session: "Expired!" };
            }

            // Clearing Cookies and redirect User to Login
            redirectUser();
            console.error("Error while refreshing token", err);
            return Promise.reject(error);
          }
        }
        return retryPromise;
      }

      return Promise.reject(error);
    }
  );
};

export default setAxiosDefaults;
