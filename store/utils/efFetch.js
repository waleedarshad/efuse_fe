import { configureRefreshFetch, fetchJSON } from "refresh-fetch";
import merge from "lodash/merge";
import getConfig from "next/config";

import { getToken as getAuthToken, attemptToRefreshToken } from "../../helpers/Auth0Helper";

const { publicRuntimeConfig } = getConfig();
const { defaultUrl } = publicRuntimeConfig;

const efFetch = async (context, apiURL, fetchOptions = {}) => {
  let refreshedToken = null;

  const getToken = () => refreshedToken || getAuthToken(context);

  const shouldRefreshToken = error => error.status === 401 && error.body === "Unauthorized";

  const fetchJSONWithToken = (url, options = {}) => {
    const optionsWithToken = merge({}, options, { headers: { Authorization: getToken() } });

    return fetchJSON(url, optionsWithToken);
  };

  const refreshToken = async () => {
    try {
      const session = await attemptToRefreshToken(context);

      refreshedToken = session.token;
    } catch (error) {
      console.error("Error while refreshing token in efFetch", error);
      throw error;
    }
  };

  return configureRefreshFetch({
    shouldRefreshToken,
    refreshToken,
    fetch: fetchJSONWithToken
  })(`${defaultUrl}${apiURL}`, fetchOptions);
};

export default efFetch;
