import {
  SET_TOURNAMENT,
  ADD_TEAM,
  REMOVE_TEAM,
  ADD_PLAYER,
  REMOVE_PLAYER,
  SET_SUBMITTING_NEW_EVENT_STATE,
  UPDATE_WINNER_MATCHES,
  TOGGLE_MOCK_EXTERNAL_ERENA_EVENT
} from "../actions/types";

const initialState = {
  tournament: null,
  submittingNewTournament: false,
  mockExternalErenaEvent: false
};

const erenaReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_TOURNAMENT:
      if (state?.tournament?.bracket) {
        return {
          ...state,
          tournament: { bracket: state.tournament.bracket, ...action.payload }
        };
      } else {
        return {
          ...state,
          tournament: action.payload
        };
      }
    case ADD_TEAM:
      return {
        ...state,
        tournament: {
          ...state.tournament,
          teamStats: [...state.tournament.teamStats, action.payload]
        }
      };
    case REMOVE_TEAM:
      // be is only returning the team, so we have to update the state for the new teams
      // duplicate the team stats array
      const array = [...state.tournament.teamStats];
      // splice the team from the team stats array
      array.splice(action.payload, 1);

      return {
        ...state,
        tournament: {
          ...state.tournament,
          teamStats: array
        }
      };
    case ADD_PLAYER:
      // be is only returning the player, so we have to update the state accordingly
      // duplicate current state array
      const finalStats = {
        ...state.tournament,
        teamStats: [...state.tournament.teamStats]
      };
      // update the team's players array with the new player
      finalStats.teamStats[action.index].players = [...finalStats.teamStats[action.index].players, action.payload];
      return {
        ...state,
        tournament: finalStats
      };
    case REMOVE_PLAYER:
      // be is only returning the team, so we have to update the state for the new teams
      const removePlayerArray = [...state.tournament.teamStats[action.teamIndex].players];
      // splice the player from the array
      removePlayerArray.splice(action.playerIndex, 1);
      // duplicate the current state array
      const finalStatsObject = {
        ...state.tournament,
        teamStats: [...state.tournament.teamStats]
      };
      // set the current players array for the team to the new array
      finalStatsObject.teamStats[action.teamIndex].players = removePlayerArray;

      return {
        ...state,
        tournament: finalStatsObject
      };
    case SET_SUBMITTING_NEW_EVENT_STATE:
      return {
        ...state,
        submittingNewTournament: action.payload
      };
    case UPDATE_WINNER_MATCHES:
      return {
        ...state,
        tournament: { ...state.tournament, bracket: action.payload }
      };
    case TOGGLE_MOCK_EXTERNAL_ERENA_EVENT:
      return {
        ...state,
        mockExternalErenaEvent: !state.mockExternalErenaEvent
      };

    default:
      return {
        ...state
      };
  }
};

export default erenaReducer;
