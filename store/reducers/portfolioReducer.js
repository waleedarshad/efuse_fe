import { TOGGLE_IN_PROGRESS, TOGGLE_PORTFOLIO_MODAL, MOCK_EXTERNAL_USER_VIEW } from "../actions/types";

const initialState = {
  inProgress: false,
  modal: false,
  modalSection: "",
  mockExternalUser: false
};

const portfolioReducer = (state = initialState, action) => {
  switch (action.type) {
    case TOGGLE_IN_PROGRESS:
      return {
        ...state,
        inProgress: action.inProgress
      };
    case TOGGLE_PORTFOLIO_MODAL:
      return {
        ...state,
        modal: action.modal,
        modalSection: action.modalSection
      };
    case MOCK_EXTERNAL_USER_VIEW:
      return {
        ...state,
        mockExternalUser: action.mockExternalUser
      };
    default:
      return {
        ...state
      };
  }
};

export default portfolioReducer;
