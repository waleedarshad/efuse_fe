import { OB_UPDATE_STORE, SET_OAUTH_SIGNUP_VALUES, GET_PATHWAYS } from "../actions/types";

const initialState = {
  user: null,
  professional: null,
  student: null,
  account: null,
  gamer: null,
  gameStats: null,
  profile: null,
  linkedAccounts: null,
  oauthSignup: {},
  pathways: null
};

const onboardingReducer = (state = initialState, action) => {
  switch (action.type) {
    case OB_UPDATE_STORE:
      return {
        ...state,
        [action.key]: action.object
      };
    case SET_OAUTH_SIGNUP_VALUES:
      return {
        ...state,
        oauthSignup: action.payload
      };
    case GET_PATHWAYS:
      return {
        ...state,
        pathways: action.payload
      };
    default:
      return {
        ...state
      };
  }
};

export default onboardingReducer;
