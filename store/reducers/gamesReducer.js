import { SET_GAMES } from "../actions/types";

const initialState = {
  games: []
};

const gamesReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_GAMES:
      return {
        ...state,
        games: action.payload
      };
    default:
      return {
        ...state
      };
  }
};

export default gamesReducer;
