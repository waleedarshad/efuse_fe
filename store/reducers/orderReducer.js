import { SET_PROMOCODE, SET_PROMOCODE_ERROR, CLEAR_PROMOCODE_ERROR } from "../actions/types";

const initialState = {
  promocode: null,
  promocodeError: null
};

const orderReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_PROMOCODE:
      return {
        ...state,
        promocode: action.payload
      };
    case SET_PROMOCODE_ERROR:
      return {
        ...state,
        promocodeError: action.payload
      };
    case CLEAR_PROMOCODE_ERROR:
      return {
        ...state,
        promocodeError: null
      };
    default:
      return {
        ...state
      };
  }
};

export default orderReducer;
