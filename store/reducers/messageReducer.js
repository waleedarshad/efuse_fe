import findIndex from "lodash/findIndex";
import cloneDeep from "lodash/cloneDeep";
import {
  CHAT_RESET_HASH,
  CHAT_SELECTED,
  CHAT_UNSELECTED,
  CLEAR_CHAT_FOLLOWERS,
  GET_CHAT_CHANNELS,
  GET_CHAT_FOLLOWERS,
  GET_UNREAD_MESSAGE_COUNT_SUCCESS,
  IS_WINDOW_VIEW,
  ONLINE_STATUS,
  RESET_UNREAD_MESSAGE_COUNT,
  SEND_MESSAGE,
  TOGGLE_FOLLOWING_LIST_MODAL
} from "../actions/types";

const initialState = {
  selectedChatId: null,
  selectedChatName: "",
  chat: [],
  channels: null,
  isWindowView: false,
  chatPagination: {},
  chatFollowers: [],
  followersPagination: {},
  otherPerson: {},
  messageSent: false,
  showFollowingListModal: false,
  unreadMessageCount: 0
};

const messageReducer = (state = initialState, action) => {
  switch (action.type) {
    case CHAT_SELECTED:
      return {
        ...state,
        selectedChatId: action.payload.chatId,
        selectedChatName: action.payload.name
      };
    case CHAT_UNSELECTED:
      return {
        ...state,
        selectedChatId: null,
        selectedChatName: null
      };
    case IS_WINDOW_VIEW:
      return {
        ...state,
        isWindowView: action.show
      };
    case CHAT_RESET_HASH:
      return {
        ...state,
        chat: []
      };
    case GET_CHAT_FOLLOWERS:
      return {
        ...state,
        chatFollowers: action.followers
      };
    case CLEAR_CHAT_FOLLOWERS:
      return {
        ...state,
        chatFollowers: []
      };
    case SEND_MESSAGE:
      return {
        ...state,
        chat: appendMessage(state, action),
        messageSent: true
      };
    case ONLINE_STATUS:
      return {
        ...state,
        ...toggleOnlineStatus(state, action)
      };
    case TOGGLE_FOLLOWING_LIST_MODAL:
      return {
        ...state,
        showFollowingListModal: action.modal
      };
    case GET_CHAT_CHANNELS:
      return {
        ...state,
        channels: action.payload.channels
      };
    case GET_UNREAD_MESSAGE_COUNT_SUCCESS:
      return {
        ...state,
        unreadMessageCount: action.payload.unreadMessageCount
      };
    case RESET_UNREAD_MESSAGE_COUNT:
      return {
        ...state,
        unreadMessageCount: initialState.unreadMessageCount
      };
    default:
      return {
        ...state
      };
  }
};

const appendMessage = (state, action) => {
  let chat = cloneDeep(state.chat);
  const { message } = action.payload;
  const _id = message.createdAt.split("T")[0];
  const index = findIndex(chat, { _id });
  if (index >= 0) {
    let { chats } = chat[index];
    chats = [...chats, { ...message }];
    chat[index].chats = chats;
  } else {
    chat = [
      {
        _id,
        chats: [{ ...message }]
      },
      ...chat
    ];
  }
  return chat;
};

const toggleOnlineStatus = (state, action) => {
  const chatFollowers = cloneDeep(state.chatFollowers);
  const { id, online } = action.payload;
  chatFollowers.filter(cf => {
    if (cf._id === id) {
      cf.online = online;
      return true;
    }
  });
  return { chatFollowers };
};

export default messageReducer;
