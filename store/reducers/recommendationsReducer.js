import { GET_FOLLOWER_RECOMMENDATIONS } from "../actions/types";

const initialState = {
  followers: []
};

const recommendationsReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_FOLLOWER_RECOMMENDATIONS:
      return {
        ...state,
        followers: action.payload
      };
    default:
      return {
        ...state
      };
  }
};

export default recommendationsReducer;
