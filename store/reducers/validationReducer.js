import { VALIDATE_EMAIL } from "../actions/types";

const initialState = {
  emailError: false
};

const validationReducer = (state = initialState, action) => {
  switch (action.type) {
    case VALIDATE_EMAIL:
      return {
        ...state,
        emailError: action.payload.emailError
      };
    default:
      return {
        ...state
      };
  }
};

export default validationReducer;
