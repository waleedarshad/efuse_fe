import { SEND_NOTIFICATION, INIT_NOTIFICATION } from "../actions/types";

const initialState = {
  notify: false,
  title: "",
  type: "",
  message: ""
};

const notificationReducer = (state = initialState, action) => {
  switch (action.type) {
    case INIT_NOTIFICATION:
      return {
        ...state,
        notify: false,
        title: "",
        message: "",
        type: ""
      };
    case SEND_NOTIFICATION:
      return {
        ...state,
        notify: true,
        title: action.payload.title,
        message: action.payload.message,
        type: action.payload.type
      };
    default:
      return {
        ...state
      };
  }
};

export default notificationReducer;
