import { SET_VIDEO, REMOVE_VIDEO } from "../actions/types";

const initialState = {
  video: {}
};

const globalVideoReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_VIDEO:
      return {
        video: action.payload
      };
    case REMOVE_VIDEO:
      return {
        video: {}
      };

    default:
      return state;
  }
};

export default globalVideoReducer;
