import {
  GET_LEADERBOARDS,
  GET_ENGAGEMENTS_LEADERBOARD,
  GET_STREAK_LEADERBOARD,
  GET_VIEWS_LEADERBOARD,
  GET_LEADERBOARDS_TOP,
  GET_LEADERBOARD_DATA,
  GET_VALORANT_LEADERBOARD_DATA,
  GET_AIMLAB_LEADERBOARD_DATA,
  GET_LEAGUE_OF_LEGENDS_LEADERBOARD_DATA,
  GET_USER_LEADERBOARD_DATA,
  SET_LEADERBOARD_DATA
} from "../actions/types";

const initialState = {
  streak: [],
  views: [],
  engagements: [],
  top: {},
  leaderboardData: []
};

const leaderboardReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_LEADERBOARDS:
      return {
        ...state,
        leaderboards: action.payload
      };
    case GET_VIEWS_LEADERBOARD:
      return {
        ...state,
        views: action.payload
      };
    case GET_STREAK_LEADERBOARD:
      return {
        ...state,
        streak: action.payload
      };
    case GET_ENGAGEMENTS_LEADERBOARD:
      return {
        ...state,
        engagements: action.payload
      };
    case GET_LEADERBOARDS_TOP:
      return {
        ...state,
        top: action.payload
      };
    case GET_LEADERBOARD_DATA:
      return {
        ...state,
        leaderboardData: action.payload.leaderboard
      };
    case GET_USER_LEADERBOARD_DATA:
      return {
        ...state,
        currentUserLeaderboardData: action.payload
      };
    case GET_VALORANT_LEADERBOARD_DATA:
      return {
        ...state,
        leaderboardData: action.payload
      };
    case GET_AIMLAB_LEADERBOARD_DATA:
      return {
        ...state,
        leaderboardData: action.payload
      };
    case GET_LEAGUE_OF_LEGENDS_LEADERBOARD_DATA:
      return {
        ...state,
        leaderboardData: action.payload
      };
    case SET_LEADERBOARD_DATA:
      return {
        ...state,
        leaderboardData: action.payload
      };
    default:
      return {
        ...state
      };
  }
};

export default leaderboardReducer;
