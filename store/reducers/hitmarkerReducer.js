import uniqBy from "lodash/uniqBy";
import { GET_HITMARKERS } from "../actions/types";

const initialState = {
  hitmarkers: [],
  pagination: {}
};

const hitmarkerReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_HITMARKERS:
      return {
        ...state,
        pagination: action.pagination,
        hitmarkers: uniqBy([...state.hitmarkers, ...action.hitmarkers], "_id")
      };
    default:
      return {
        ...state
      };
  }
};

export default hitmarkerReducer;
