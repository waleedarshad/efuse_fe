import { GET_RECRUITMENT_PROFILE_NOTES } from "../actions/types";
import recrutingReducer from "./recruitingReducer";

describe("recruitingReducer", () => {
  it("get recruiting profile notes", () => {
    const prevState = {
      notes: []
    };
    const action = {
      type: GET_RECRUITMENT_PROFILE_NOTES,
      payload: ["note1", "note2"]
    };
    const nextState = recrutingReducer(prevState, action);
    expect(nextState.notes).toEqual(["note1", "note2"]);
  });
});
