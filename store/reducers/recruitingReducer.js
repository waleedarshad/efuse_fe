import remove from "lodash/remove";
import findIndex from "lodash/findIndex";
import uniqBy from "lodash/uniqBy";
import cloneDeep from "lodash/cloneDeep";
import {
  SET_RECRUITING_PROFILE,
  GET_RECRUITS,
  UPDATE_RECRUITING_FILTER_VALUE,
  UPDATE_RECRUITING_SEARCH_OBJECT,
  UPDATE_RECURITING,
  ADD_RECRUITING_FILTER,
  CLEAR_RECRUITING_FILTERS,
  UPDATED_STARRED_RECRUIT,
  DELETE_RECRUIT_STARRED,
  SORT_BY,
  SET_SUBMITTING_QUESTIONNIARE_STATE,
  UPDATE_RECRUITING_FILTER,
  CLEAR_RECRUITING_PROFILES,
  GET_RECRUITED_PEOPLE,
  CREATE_RECRUITED_PEOPLE,
  UPDATE_RECRUITED_PEOPLE,
  DELETE_RECRUITED_PEOPLE,
  OPEN_RECRUITING_PEOPLE_MODAL,
  EDIT_RECRUITING_PEOPLE,
  GET_RECRUITMENT_PROFILE_NOTES,
  RECRUITING_SELECTION,
  TOGGLE_RECRUITING_SELECTION_MODAL,
  RECRUITING_SELECTION_ID,
  ONLOAD_RECRUITING_SELECTION,
  GET_STARRED_RECRUITS_FAILURE,
  GET_STARRED_RECRUITS_REQUEST,
  GET_STARRED_RECRUITS_SUCCESS,
  GET_RECRUITS_FAILURE,
  GET_RECRUITS_REQUEST,
  GET_RECRUITS_SUCCESS,
  UPDATE_RECRUIT_ACTION
} from "../actions/types";

const initialState = {
  profile: null,
  recruiting: [],
  filtersApplied: [],
  searchObject: {},
  searchFilters: {
    graduationClass: "",
    country: "",
    state: "",
    createdAt: null,
    gender: "",
    primaryGame: ""
  },
  sort: {},
  pagination: {},
  submittingPipelineQuestionniare: false,
  recruitedPeople: [],
  recruitingPeopleModal: false,
  editRecruitedPeople: {},
  notes: [],
  recruitType: null,
  recruitId: null,
  isSelectingRecruit: false,
  recruitingSelectionId: null,
  onLoadSelectingRecruit: true,
  isGettingFirstRecruit: false
};

const recruitingReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_RECRUITING_PROFILE:
      return {
        ...state,
        profile: action.payload
      };
    case GET_RECRUITS:
      if (action.reset) {
        return {
          ...state,
          recruiting: uniqBy([...action.recruitmentProfiles], "_id"),
          pagination: action.pagination
        };
      }
      return {
        ...state,
        recruiting: uniqBy([...state.recruiting, ...action.recruitmentProfiles], "_id"),
        pagination: action.pagination
      };

    case GET_STARRED_RECRUITS_REQUEST:
      return {
        ...state,
        isGettingFirstRecruit: action.payload
      };

    case GET_STARRED_RECRUITS_SUCCESS:
      return {
        ...state,
        recruiting: action.page === 1 ? action.payload : state.recruiting.concat(action.payload),
        pagination: action.pagination,
        isGettingFirstRecruit: false
      };

    case GET_STARRED_RECRUITS_FAILURE:
      return {
        ...state,
        isGettingFirstRecruit: false
      };

    case GET_RECRUITS_REQUEST:
      return {
        ...state,
        isGettingFirstRecruit: action.payload
      };

    case GET_RECRUITS_SUCCESS:
      return {
        ...state,
        recruiting: action.page === 1 ? action.payload : state.recruiting.concat(action.payload),
        pagination: action.pagination,
        isGettingFirstRecruit: false
      };

    case GET_RECRUITS_FAILURE:
      return {
        ...state,
        isGettingFirstRecruit: false
      };

    case UPDATE_RECRUITING_FILTER_VALUE:
      return {
        ...state,
        searchFilters: {
          ...state.searchFilters,
          [action.key]: action.value
        }
      };
    case UPDATE_RECRUITING_SEARCH_OBJECT:
      return {
        ...state,
        searchObject: action.searchObject
      };
    case UPDATE_RECURITING:
      return {
        ...state,
        recruiting: action.payload,
        pagination: action.pagination,
        searchObject: action.searchObject
      };
    case ADD_RECRUITING_FILTER:
      return {
        ...state,
        filtersApplied: action.payload
      };
    case CLEAR_RECRUITING_FILTERS:
      return {
        ...state,
        filtersApplied: [],
        searchObject: {},
        searchFilters: {
          country: "",
          graduationClass: "",
          state: "",
          createdAt: ""
        }
      };
    case UPDATED_STARRED_RECRUIT:
      return {
        ...state,
        recruiting: state.recruiting.map(recruit => {
          if (recruit._id === action.recruitId) {
            // eslint-disable-next-line no-param-reassign
            recruit.starred = true;
          }
          return recruit;
        })
      };
    case DELETE_RECRUIT_STARRED:
      return {
        ...state,
        recruiting: state.recruiting.map(recruit => {
          if (recruit._id === action.recruitId) {
            // eslint-disable-next-line no-param-reassign
            recruit.starred = false;
          }
          return recruit;
        })
      };
    case SORT_BY:
      return {
        ...state,
        sort: action.value
      };
    case SET_SUBMITTING_QUESTIONNIARE_STATE:
      return {
        ...state,
        submittingPipelineQuestionniare: action.payload
      };
    case UPDATE_RECRUITING_FILTER:
      return {
        ...state,
        filtersApplied: state.filtersApplied.filter(filter => filter.key !== action.key)
      };
    case CLEAR_RECRUITING_PROFILES:
      return initialState;
    case GET_RECRUITED_PEOPLE:
      return {
        ...state,
        recruitedPeople: action.payload
      };
    case CREATE_RECRUITED_PEOPLE:
      return {
        ...state,
        recruitedPeople: setRecruitedPeople(state, action),
        recruitingPeopleModal: false
      };
    case UPDATE_RECRUITED_PEOPLE:
      return {
        ...state,
        recruitedPeople: updateRecruitedPeople(state, action),
        recruitingPeopleModal: false
      };
    case DELETE_RECRUITED_PEOPLE:
      return {
        ...state,
        recruitedPeople: deleteRecruitedPeople(state, action),
        recruitingPeopleModal: false
      };
    case OPEN_RECRUITING_PEOPLE_MODAL:
      return {
        ...state,
        recruitingPeopleModal: action.isOpen
      };
    case EDIT_RECRUITING_PEOPLE:
      return {
        ...state,
        editRecruitedPeople: action.payload,
        recruitingPeopleModal: true
      };

    case GET_RECRUITMENT_PROFILE_NOTES:
      return {
        ...state,
        notes: action.payload
      };

    case RECRUITING_SELECTION:
      return {
        ...state,
        recruitType: action.payload.recruitType,
        recruitId: action.payload.recruitId
      };
    case TOGGLE_RECRUITING_SELECTION_MODAL:
      return {
        ...state,
        isSelectingRecruit: action.payload
      };
    case RECRUITING_SELECTION_ID:
      return {
        ...state,
        recruitingSelectionId: action.payload
      };
    case ONLOAD_RECRUITING_SELECTION:
      return {
        ...state,
        onLoadSelectingRecruit: action.payload
      };
    case UPDATE_RECRUIT_ACTION:
      return {
        ...state,
        recruiting: state.recruiting.map(recruit => {
          if (recruit._id === action.payload.recruitId) {
            // eslint-disable-next-line no-unused-expressions
            recruit.action === action.payload.recruitAction;
          }
          return recruit;
        })
      };
    default:
      return {
        ...state
      };
  }
};

const setRecruitedPeople = (state, action) => {
  const peoples = cloneDeep(state.recruitedPeople);
  peoples.push(action.payload.recruitPeople);
  return peoples;
};

const deleteRecruitedPeople = (state, action) => {
  const peoples = cloneDeep(state.recruitedPeople);
  remove(peoples, {
    _id: action.payload
  });
  return peoples;
};

const updateRecruitedPeople = (state, action) => {
  const peoples = cloneDeep(state.recruitedPeople);
  const index = findIndex(peoples, { _id: action.payload.recruitPeople._id });
  peoples.splice(index, 1, action.payload.recruitPeople);
  return peoples;
};

export default recruitingReducer;
