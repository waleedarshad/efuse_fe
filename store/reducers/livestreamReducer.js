import { SET_STREAM_INFO } from "../actions/types";

const initialState = {
  stream: {}
};

const livestreamReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_STREAM_INFO:
      return {
        ...state,
        stream: action.payload
      };
    default:
      return {
        ...state
      };
  }
};

export default livestreamReducer;
