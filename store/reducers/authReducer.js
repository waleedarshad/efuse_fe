import isEmpty from "lodash/isEmpty";
import { EXTERNAL_AUTH_SERVICES } from "../../common/externalAuthServices";
import { EXTERNAL_AUTH } from "../actions/externalAuth/types";

import {
  SET_CURRENT_USER,
  TIMELINE_UPDATE_FLAG,
  STEAM_VALIDATED,
  LINKEDIN_VALIDATED,
  TWITTER_VALIDATED,
  BIND_STREAM_CHAT_TOKEN,
  SET_SIGNUP_FORM_ERROR
} from "../actions/types";

const initialState = {
  currentUser: null,
  external: Object.values(EXTERNAL_AUTH_SERVICES).reduce((acc, serviceName) => {
    acc[serviceName] = false;
    return acc;
  }, {}),
  isAuthenticated: null,
  timelineUpdatesAvailable: false,
  signupFormValidationErrors: {
    email: null,
    username: null
  }
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_CURRENT_USER:
      return {
        ...state,
        currentUser: action.payload,
        isAuthenticated: !isEmpty(action.payload)
      };
    case TIMELINE_UPDATE_FLAG:
      return {
        ...state,
        timelineUpdatesAvailable:
          state.currentUser.id === action.payload.userId ? action.payload.flag : state.timelineUpdatesAvailable
      };
    case STEAM_VALIDATED:
      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          steamValidated: true
        }
      };

    case LINKEDIN_VALIDATED:
      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          linkedinValidated: true
        }
      };
    case TWITTER_VALIDATED:
      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          twitterVerified: true
        }
      };
    case BIND_STREAM_CHAT_TOKEN:
      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          streamChatToken: action.streamChatToken
        }
      };
    case SET_SIGNUP_FORM_ERROR:
      return {
        ...state,
        signupFormValidationErrors: {
          ...state.signupFormValidationErrors,
          ...action.payload
        }
      };
    case EXTERNAL_AUTH.STATUS.LOGGED_USER.GET.SUCCESS:
      return {
        ...state,
        external: {
          ...state.external,
          [action.payload.service]: action.payload.isAuthenticated
        }
      };
    case EXTERNAL_AUTH.LINK.DELETE.SUCCESS:
      return {
        ...state,
        external: {
          ...state.external,
          [action.payload.service]: false
        }
      };
    default:
      return { ...state };
  }
};

export default authReducer;
