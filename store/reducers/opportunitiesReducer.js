import isEmpty from "lodash/isEmpty";
import cloneDeep from "lodash/cloneDeep";
import uniqBy from "lodash/uniqBy";
import {
  ADD_OPPORTUNITY,
  ADD_OPPORTUNITY_FILTER,
  BECOME_APPLICANT,
  BOOST_OPPORTUNITY,
  CHANGE_OPPORTUNITY_PUBLISH_STATUS,
  CHANGE_OPPORTUNITY_STATUS,
  CLEAR_OPPORTUNITIES,
  CLEAR_OPPORTUNITY,
  CLEAR_OPPORTUNITY_FILTERS,
  GET_ADMIN_OPPORTUNITIES,
  GET_ADMIN_OPPORTUNITIES_SUCCESS,
  GET_OPPORTUNITIES,
  GET_OPPORTUNITIES_FAILURE,
  GET_OPPORTUNITIES_REQUEST,
  GET_OPPORTUNITIES_SUCCESS,
  GET_OPPORTUNITY,
  GET_OPPORTUNITY_REQUIRED_FIELDS,
  GET_RECENT_OPPORTUNITIES_BY_TYPE,
  OPPORTUNITY_CURRENT_PAGE,
  OPP_SHORT_NAME_ERROR,
  PROMOTED_OPPORTUNITIES,
  SET_MAX_MONEY_INCLUDED,
  SET_RECENT_OPPORTUNITIES,
  SET_RECOMMENDATIONS_OPPORTUNITIES,
  UPDATE_OPPORTUNITIES,
  UPDATE_OPPORTUNITY_FILTER_VALUE,
  UPDATE_SEARCH_OBJECT
} from "../actions/types";

const initialState = {
  opportunities: [],
  filtersApplied: [],
  searchObject: {},
  searchFilters: {
    state: "",
    city: "",
    "moneyIncluded?": "",
    opportunityType: "",
    game: "",
    "verified.status.retain.key": ""
  },
  opportunity: {},
  maxMoney: 0,
  moneySet: false,
  pagination: {},
  recentOpportunities: [],
  view: "",
  promotedOpportunities: [],
  recommendedOpportunities: null,
  requiredFormFields: [],
  recentOpportunitiesByTypes: {
    scholarships: [],
    events: [],
    teamOpenings: [],
    jobs: []
  },
  isGettingFirstOpportunity: false
};

const opportunitiesReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_OPPORTUNITIES:
      return {
        ...state,
        opportunities: uniqBy([...state.opportunities, ...action.opportunities], "_id"),
        pagination: action.pagination
      };
    case GET_OPPORTUNITIES_REQUEST:
      return {
        ...state,
        isGettingFirstOpportunity: action.payload
      };
    case GET_OPPORTUNITIES_SUCCESS:
      return {
        ...state,
        opportunities: action.page === 1 ? action.payload : state.opportunities.concat(action.payload),
        pagination: action.pagination,
        isGettingFirstOpportunity: false
      };
    case GET_OPPORTUNITIES_FAILURE:
      return {
        ...state,
        isGettingFirstOpportunity: false
      };
    case GET_ADMIN_OPPORTUNITIES:
      return {
        ...state,
        opportunities: action.opportunities,
        pagination: action.pagination
      };
    case GET_ADMIN_OPPORTUNITIES_SUCCESS:
      return {
        ...state,
        opportunities: action.page === 1 ? action.payload : state.opportunities.concat(action.payload),
        pagination: action.pagination
      };
    case ADD_OPPORTUNITY_FILTER:
      return {
        ...state,
        filtersApplied: action.payload
      };
    case CLEAR_OPPORTUNITY_FILTERS:
      return {
        ...state,
        filtersApplied: [],
        searchObject: {},
        searchFilters: {
          state: "",
          city: "",
          "moneyIncluded?": "",
          opportunityType: "",
          game: "",
          "verified.status.retain.key": ""
        }
      };
    case UPDATE_SEARCH_OBJECT:
      return {
        ...state,
        searchObject: action.searchObject
      };
    case UPDATE_OPPORTUNITIES:
      return {
        ...state,
        opportunities: action.payload,
        pagination: action.pagination,
        searchObject: action.searchObject
      };
    case UPDATE_OPPORTUNITY_FILTER_VALUE:
      return {
        ...state,
        searchFilters: {
          ...state.searchFilters,
          [action.key]: action.value
        }
      };
    case GET_OPPORTUNITY:
      return {
        ...state,
        opportunity: action.payload
      };
    case PROMOTED_OPPORTUNITIES:
      return {
        ...state,
        promotedOpportunities: action.payload
      };
    case SET_MAX_MONEY_INCLUDED:
      return {
        ...state,
        maxMoney: action.payload.money,
        moneySet: true
      };
    case SET_RECENT_OPPORTUNITIES:
      return {
        ...state,
        recentOpportunities: action.payload
      };
    case BECOME_APPLICANT:
      return {
        ...state,
        ...addApplicant(state, action)
      };
    case OPPORTUNITY_CURRENT_PAGE:
      return {
        ...state,
        view: action.view
      };
    case CLEAR_OPPORTUNITIES:
      return {
        ...state,
        opportunities: [],
        pagination: {}
      };
    case ADD_OPPORTUNITY:
      return {
        ...state,
        opportunities: [action.opportunity, ...state.opportunities]
      };
    case CLEAR_OPPORTUNITY:
      return {
        ...state,
        opportunity: {}
      };
    case CHANGE_OPPORTUNITY_STATUS:
      return {
        ...state,
        opportunities: changeStatus(state, action, "status")
      };
    case CHANGE_OPPORTUNITY_PUBLISH_STATUS:
      return {
        ...state,
        opportunities: changeStatus(state, action, "publishStatus")
      };
    case OPP_SHORT_NAME_ERROR:
      return {
        ...state,
        shortNameError: action.error
      };
    case SET_RECOMMENDATIONS_OPPORTUNITIES:
      return {
        ...state,
        recommendedOpportunities: action.payload
      };
    case GET_OPPORTUNITY_REQUIRED_FIELDS:
      return {
        ...state,
        requiredFormFields: action.payload
      };
    case GET_RECENT_OPPORTUNITIES_BY_TYPE:
      return {
        ...state,
        recentOpportunitiesByTypes: action.payload
      };
    case BOOST_OPPORTUNITY:
      return {
        ...state,
        ...toggleBoost(state, action)
      };
    default:
      return {
        ...state
      };
  }
};

const toggleBoost = (state, action) => {
  const { opportunityId, boosted } = action;
  const { opportunities, opportunity } = state;
  const clonedOpportunities = cloneDeep(opportunities);
  const clonedOpportunity = cloneDeep(opportunity);

  const targetedOpportunity = clonedOpportunities.find(opp => opp._id === opportunityId);
  if (targetedOpportunity) {
    targetedOpportunity.metadata.boosted = boosted;
  }

  if (!isEmpty(clonedOpportunity)) {
    clonedOpportunity.metadata.boosted = boosted;
  }

  return {
    opportunities: clonedOpportunities,
    opportunity: clonedOpportunity
  };
};

const changeStatus = (state, action, statusType) => {
  const clonedOpportunities = cloneDeep(state.opportunities);
  const oppIndex = clonedOpportunities.findIndex(opt => opt._id === action.payload._id);
  clonedOpportunities[oppIndex][statusType] = action.payload[statusType];
  return clonedOpportunities;
};
const addApplicant = (state, action) => {
  const opportunities = cloneDeep(state.opportunities);
  opportunities.filter(opportunity => {
    if (opportunity._id === action.opportunityId) {
      opportunity.applicant = action.applicant;
      return true;
    }
  });
  const opportunity = cloneDeep(state.opportunity);
  if (!isEmpty(opportunity)) {
    opportunity.applicant = action.applicant;
  }
  return {
    opportunities,
    opportunity
  };
};

export default opportunitiesReducer;
