import { GET_GAME_REQUIREMENTS, GET_GAME_REQUIREMENT } from "../actions/types";

const initialState = {
  gameRequirement: {},
  gameRequirements: []
};

const gameRequirementReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_GAME_REQUIREMENTS:
      return {
        ...state,
        gameRequirements: action.payload
      };
    case GET_GAME_REQUIREMENT: {
      return {
        ...state,
        gameRequirement: action.payload
      };
    }
    default:
      return state;
  }
};

export default gameRequirementReducer;
