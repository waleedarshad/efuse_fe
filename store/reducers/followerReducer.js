import capitalize from "lodash/capitalize";
import omit from "lodash/omit";
import cloneDeep from "lodash/cloneDeep";
import uniqBy from "lodash/uniqBy";
import pluralize from "pluralize";

import {
  IS_FOLLOWED,
  TOGGLE_FOLLOWER_BTN,
  FRIENDS_COUNT,
  GET_FOLLOWERS,
  GET_FOLLOWEES,
  INC_FOLLOWER_COUNT,
  DEC_FOLLOWER_COUNT,
  FOLLOW_FROM_OTHER_PROFILE,
  SEARCH_FOLLOWERS_TO_MENTION,
  GET_FOLLOWEE_IDS,
  FOLLOWS_YOU,
  GET_FOLLOWEES_SUCCESS,
  GET_FOLLOWERS_SUCCESS
} from "../actions/types";

const initialState = {
  isFollowed: null,
  disable: null,
  totalFollowers: null,
  totalFollowees: 0,
  followers: [],
  followees: [],
  followeeIds: [],
  pagination: {},
  mentionableFollowers: [],
  followsYou: null
};

const followerReducer = (state = initialState, action) => {
  switch (action.type) {
    case IS_FOLLOWED:
      return {
        ...state,
        isFollowed: action.payload
      };
    case TOGGLE_FOLLOWER_BTN:
      return {
        ...state,
        disable: action.payload
      };
    case FRIENDS_COUNT:
      return {
        ...state,
        totalFollowees: action.followees,
        totalFollowers: action.followers
      };
    case GET_FOLLOWERS:
      return {
        ...state,
        followers: uniqBy([...state.followers, ...action.followers], "_id"),
        pagination: action.pagination
      };
    case GET_FOLLOWERS_SUCCESS:
      return {
        ...state,
        followers: action.page === 1 ? action.payload : state.followers.concat(action.payload),
        pagination: action.pagination
      };
    case GET_FOLLOWEES:
      // Use this reducer on portfolio Followers and Followee tabs - this is needed for infinite scroll feature
      return {
        ...state,
        followees: uniqBy([...state.followees, ...action.followees], "_id"),
        pagination: action.pagination
      };
    case GET_FOLLOWEES_SUCCESS:
      return {
        ...state,
        followees: action.page === 1 ? action.payload : state.followees.concat(action.payload),
        pagination: action.pagination
      };
    case INC_FOLLOWER_COUNT:
      return {
        ...state,
        totalFollowers: state.totalFollowers + 1,
        followers: [action.friend, ...state.followers]
      };
    case DEC_FOLLOWER_COUNT:
      return {
        ...state,
        totalFollowers: state.totalFollowers - 1,
        followers: state.followers.filter(friend => friend.follower !== action.follower)
      };
    case FOLLOW_FROM_OTHER_PROFILE:
      return {
        ...state,
        [action.key]: updateOtherFollowers(action, state)
      };
    case SEARCH_FOLLOWERS_TO_MENTION:
      return {
        ...state,
        mentionableFollowers: action.followers
      };
    case GET_FOLLOWEE_IDS:
      return {
        ...state,
        followeeIds: action.payload,
        totalFollowees: action.payload.length
      };
    case FOLLOWS_YOU:
      return {
        ...state,
        followsYou: action.payload
      };
    default:
      return {
        ...state
      };
  }
};

const updateOtherFollowers = (action, state) => {
  const { key } = action;
  const singularFriendKey = pluralize.singular(key);
  const capitalizeFriendKey = capitalize(singularFriendKey);
  const isFriendKey = `is${capitalizeFriendKey}`;
  const associatedFriendKey = `associated${capitalizeFriendKey}`;
  const friends = cloneDeep(state[key]);

  friends.filter(friend => {
    if (friend[singularFriendKey] === action.id) {
      action.follow
        ? (friend[associatedFriendKey][isFriendKey] = {
            followed: true
          })
        : (friend[associatedFriendKey] = omit(friend[associatedFriendKey], [isFriendKey]));
      return true;
    }
  });
  return friends;
};

export default followerReducer;
