import { SET_WEBVIEW_STATUS } from "../actions/types";

const initialState = {
  status: false
};

const webviewReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_WEBVIEW_STATUS:
      return {
        ...state,
        status: action.payload.status
      };
    default:
      return {
        ...state
      };
  }
};

export default webviewReducer;
