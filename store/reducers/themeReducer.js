import { CHANGE_TO_LIGHT, CHANGE_TO_DARK } from "../actions/types";
import { lightTheme } from "../../styles/themes";

const initialState = {
  theme: lightTheme,
  currentThemeType: "light"
};

const themeReducer = (state = initialState, action) => {
  switch (action.type) {
    case CHANGE_TO_LIGHT:
      return {
        ...state,
        theme: action.payload,
        currentThemeType: "light"
      };
    case CHANGE_TO_DARK:
      return {
        ...state,
        theme: action.payload,
        currentThemeType: "dark"
      };
    default:
      return {
        ...state
      };
  }
};

export default themeReducer;
