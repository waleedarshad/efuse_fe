import { OPPORTUNITIES, SET_OPPORTUNITY_PAGINATION } from "../../actions/opportunitiesv2/types";

const initialState = {
  isFetchingOpportunities: false,
  opportunities: [],
  filters: [],
  isFilterModalOpen: false,
  isCompactViewOn: false,
  pagination: null
};

const opportunitiesv2Reducer = (state = initialState, action) => {
  switch (action.type) {
    case OPPORTUNITIES.SEARCH.REQUEST:
      return {
        ...state,
        isFetchingOpportunities: true
      };
    case OPPORTUNITIES.SEARCH.SUCCESS:
      return {
        ...state,
        isFetchingOpportunities: false,
        opportunities: [...action.payload.opportunities]
      };
    case OPPORTUNITIES.SEARCH.FAILURE:
      return {
        ...state,
        isFetchingOpportunities: false
      };
    case OPPORTUNITIES.SEARCH.APPLIED.REQUEST:
      return {
        ...state,
        isFetchingOpportunities: true
      };
    case OPPORTUNITIES.SEARCH.APPLIED.SUCCESS:
      return {
        ...state,
        isFetchingOpportunities: false,
        opportunities: [...action.payload.opportunities]
      };
    case OPPORTUNITIES.SEARCH.APPLIED.FAILURE:
      return {
        ...state,
        isFetchingOpportunities: false
      };
    case OPPORTUNITIES.FILTER.MODAL.TOGGLE:
      return {
        ...state,
        isFilterModalOpen: !state.isFilterModalOpen
      };
    case OPPORTUNITIES.FILTER.SET:
      return {
        ...state,
        filters: action.payload.filters
      };
    case SET_OPPORTUNITY_PAGINATION:
      return {
        ...state,
        pagination: action.payload
      };
    case OPPORTUNITIES.COMPACT.TOGGLE:
      return {
        ...state,
        isCompactViewOn: !state.isCompactViewOn
      };
    default:
      return state;
  }
};

export default opportunitiesv2Reducer;
