import {
  SET_ERENA_EVENT,
  SET_ERENA_EVENT_USER_ROLES,
  SET_TEAMS,
  TOGGLE_MOCK_EXTERNAL_ERENA_EVENT,
  SET_SUBMITTING_NEW_EVENT_STATE,
  UPDATE_TEAM,
  CREATE_TEAM,
  DELETE_TEAM,
  SET_PLAYERS,
  CREATE_PLAYER,
  DELETE_PLAYER,
  UPDATE_PLAYER,
  SET_ERENA_SCORES,
  CREATE_SCORE,
  UPDATE_SCORE,
  SET_POINT_RACE_TEAMS,
  SET_POINT_RACE_PLAYERS,
  SET_BRACKET,
  SET_MATCHES,
  UPDATE_MATCH,
  SET_STAFFS,
  DELETE_STAFF,
  CREATE_STAFF
} from "../../actions/types";
import { ERENA_V2 } from "../../actions/erenaV2/types";

const initialState = {
  tournament: null,
  tournaments: [],
  allTeams: [],
  allScores: [],
  allPlayers: [],
  allMatches: [],
  allStaff: [],
  pointRace: { teams: [], players: [] },
  bracket: {},
  submittingNewTournament: false,
  mockExternalErenaEvent: false,
  pagination: {}
};

const erenaReducerV2 = (state = initialState, action) => {
  const findTeamIndex = teamId => {
    return state.allTeams.findIndex(x => x._id === teamId);
  };

  const findPlayerIndex = (teamIndex, playerId) => {
    const { players } = state.allTeams[teamIndex];
    return players.findIndex(x => x._id === playerId);
  };

  const findScoreIndex = scoreId => {
    return state.allScores.findIndex(x => x._id === scoreId);
  };

  const findMatchIndex = matchId => {
    return state.allMatches.findIndex(x => x._id === matchId);
  };

  const findStaffIndex = staffId => {
    return state.allStaff.findIndex(x => x._id === staffId);
  };

  switch (action.type) {
    // ========================
    // ||     TOURNAMENT     ||
    // ========================
    case SET_ERENA_EVENT:
      return {
        ...state,
        tournament: action.payload
      };
    case ERENA_V2.GET_TOURNAMENTS.SUCCESS:
      return {
        ...state,
        tournaments: action.page === 1 ? action.payload : state.tournaments.concat(action.payload),
        pagination: action.pagination
      };
    case SET_ERENA_EVENT_USER_ROLES:
      return {
        ...state,
        userRoles: action.payload
      };
    case TOGGLE_MOCK_EXTERNAL_ERENA_EVENT:
      return {
        ...state,
        mockExternalErenaEvent: !state.mockExternalErenaEvent
      };
    case SET_SUBMITTING_NEW_EVENT_STATE:
      return {
        ...state,
        submittingNewTournament: action.payload
      };

    // ========================
    // ||       TEAMS        ||
    // ========================
    case SET_TEAMS:
      return {
        ...state,
        allTeams: action.payload || []
      };
    case UPDATE_TEAM: {
      // find the team's position in the array
      const setTeamIndex = findTeamIndex(action.payload._id);
      // initialize the current array of teams
      const setTeamArray = [...state.allTeams];

      // update the team from the array
      setTeamArray[setTeamIndex] = action.payload;
      return {
        ...state,
        allTeams: [...setTeamArray]
      };
    }
    case CREATE_TEAM:
      return {
        ...state,
        allTeams: [...state.allTeams, { ...action.payload, players: [] }]
      };
    case DELETE_TEAM: {
      // find the team's position in the array
      const removeTeamIndex = findTeamIndex(action.payload);
      // initialize the current array of teams
      const removeTeamArray = [...state.allTeams];

      // remove the team from the array
      removeTeamArray.splice(removeTeamIndex, 1);

      return {
        ...state,
        allTeams: [...removeTeamArray]
      };
    }

    // ========================
    // ||      PLAYERS       ||
    // ========================
    case SET_PLAYERS:
      return {
        ...state,
        allPlayers: action.payload
      };
    case CREATE_PLAYER: {
      // find the team's position in the array
      const createPlayerTeamIndex = findTeamIndex(action.payload.team);
      // initialize the current array of teams
      const createPlayerTeams = [...state.allTeams];

      // add the new player to the end of the array
      createPlayerTeams[createPlayerTeamIndex].players = [
        ...createPlayerTeams[createPlayerTeamIndex].players,
        action.payload
      ];
      return {
        ...state,
        allTeams: [...createPlayerTeams]
      };
    }
    case DELETE_PLAYER: {
      // find the team's position in the array
      const deletePlayerTeamIndex = findTeamIndex(action.payload.team);
      // initialize the array of players
      const deletePlayersArray = [...state.allTeams[deletePlayerTeamIndex].players];
      // find the player's position in the array
      const deletePlayerIndex = findPlayerIndex(deletePlayerTeamIndex, action.payload._id);
      // delete the player from the player's array
      deletePlayersArray.splice(deletePlayerIndex, 1);

      // initialize the current array of teams
      const deletePlayerTeamsArray = [...state.allTeams];
      // set the players array with the new players array
      deletePlayerTeamsArray[deletePlayerTeamIndex].players = [...deletePlayersArray];
      return {
        ...state,
        allTeams: [...deletePlayerTeamsArray]
      };
    }
    case UPDATE_PLAYER: {
      // find the team's position in the array
      const updatePlayerTeamIndex = findTeamIndex(action.payload.team);
      // initialize the array of players
      const updatePlayersArray = [...state.allTeams[updatePlayerTeamIndex].players];
      // find the player's position in the array
      const updatePlayerIndex = findPlayerIndex(updatePlayerTeamIndex, action.payload._id);
      // update the player from the player's array
      updatePlayersArray[updatePlayerIndex] = action.payload;

      // initialize the current array of teams
      const updatePlayerTeamsArray = [...state.allTeams];
      // set the players array with the new players array
      updatePlayerTeamsArray[updatePlayerTeamIndex].players = [...updatePlayersArray];
      return {
        ...state,
        allTeams: [...updatePlayerTeamsArray]
      };
    }

    // ========================
    // ||       SCORES       ||
    // ========================
    case SET_ERENA_SCORES:
      return {
        ...state,
        allScores: action.payload
      };
    case CREATE_SCORE:
      return {
        ...state,
        allScores: [...state.allScores, action.payload]
      };
    case UPDATE_SCORE: {
      // grab the index of the score in the all scores array
      const updateScoreIndex = findScoreIndex(action.payload._id);
      // initialize a new array from all scores
      const updateScoreArray = [...state.allScores];
      // set the new score payload to the correct position in the array
      updateScoreArray[updateScoreIndex] = action.payload;

      return {
        ...state,
        allScores: [...updateScoreArray]
      };
    }

    // ========================
    // ||     POINT RACE     ||
    // ========================
    case SET_POINT_RACE_TEAMS:
      return {
        ...state,
        pointRace: { ...state.pointRace, teams: [...action.payload] }
      };
    case SET_POINT_RACE_PLAYERS:
      return {
        ...state,
        pointRace: { ...state.pointRace, players: [...action.payload] }
      };

    // ========================
    // ||      BRACKET       ||
    // ========================
    case SET_BRACKET:
      return {
        ...state,
        bracket: action.payload
      };

    // ========================
    // ||      MATCHES       ||
    // ========================
    case SET_MATCHES:
      return {
        ...state,
        allMatches: [...action.payload]
      };
    case UPDATE_MATCH: {
      // grab the index of the score in the all scores array
      const updateMatchIndex = findMatchIndex(action.payload._id);
      // initialize a new array from all scores
      const updateMatchArray = [...state.allMatches];
      // set the new score payload to the correct position in the array
      updateMatchArray[updateMatchIndex] = action.payload;
      return {
        ...state,
        allMatches: [...updateMatchArray]
      };
    }

    // ========================
    // ||      Staff       ||
    // ========================

    case SET_STAFFS:
      return {
        ...state,
        allStaff: action.payload ? [...action.payload] : []
      };
    case DELETE_STAFF: {
      // find the staff's position in the array
      const removeStaffIndex = findStaffIndex(action.payload);
      // initialize the current array of staffs
      const removeStaffArray = [...state.allStaff];

      // remove the staff from the array
      removeStaffArray.splice(removeStaffIndex, 1);

      return {
        ...state,
        allStaff: [...removeStaffArray]
      };
    }
    case CREATE_STAFF: {
      return {
        ...state,
        allStaff: [...state.allStaff, action.payload]
      };
    }
    default:
      return {
        ...state
      };
  }
};

export default erenaReducerV2;
