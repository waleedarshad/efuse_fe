import { SET_OAUTH_ERROR, TOGGLE_OAUTH_BTN_DISABLE } from "../actions/types";

const initialState = {
  error: null,
  disabled: false
};

const oauthReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_OAUTH_ERROR:
      return {
        ...state,
        error: action.error
      };
    case TOGGLE_OAUTH_BTN_DISABLE:
      return {
        ...state,
        disabled: action.disabled
      };
    default:
      return {
        ...state
      };
  }
};

export default oauthReducer;
