import { TOGGLE_FEATURE } from "../actions/types";

const initialState = {
  // Special boolean to determine if actual flags have been loaded into redux.
  // Used for FeatureFlag component to know when to trigger $experiment_started event.
  flags_loaded: false,

  accolades_section: true,
  battlenet_section: true,
  business_experience: true,
  business_skills_section: true,
  csgo_section: true,
  education_experience: true,
  experience_section: true,
  event_section: true,
  events_section: true,
  fortnite_section: true,
  gaming_skills_section: true,
  honor_section: true,
  leagueoflegends_section: true,
  overwatch_section: true,
  platforms_section: true,
  skills_section: true,
  steam_section: true,
  stream_section: true,
  twitch_section: true,
  user_queue: false,
  youtube_section: true,
  messaging: false,
  portfolio_videoclips_component: false,
  portfolio_badges: false,
  portfolio_discord_server: false,
  internationalization: false,
  efuse_verification: false,
  education_section: false,
  education_tab: false,
  socialcards_section: false,
  work_section: true,
  work_tab: true,
  social_links: false,
  portfolio_progress: false,
  portfolio_drag_buttons: false,
  // opportunity feature flags
  allow_opportunity_creation: false,
  opportunity_creation_giveaway_type: false,
  opportunity_small_list: false,
  opportunity_large_list: false,
  mocking_portfolio_button: false,
  promoted_opportunities: false,
  display_external_source_opportunity: false,
  opportunity_landing_view: false,
  opportunity_applicants_list: false,
  opportunity_application_cost: false,
  opportunity_filter_modal: true,
  // learning portal
  learning_section: false,
  allow_learning_article_creation: false,
  learning_portal_filter_skill_development: false,
  learning_portal_filter_interview_of_the_week: false,
  learning_portal_filter_international_esports: false,
  learning_portal_filter_efuse: false,
  learning_portal_filter_business: false,
  learning_section_grid: false,
  // organization feature flags
  organizations: false,
  organization_section: false,
  organization_accolades_section: false,
  organization_event_section: false,
  organization_honor_section: false,
  organization_create_buttons: false,
  organization_socials: false,
  organization_feed: false,
  organization_opportunity_creation: false,
  organization_discord_server: false,
  organization_video_carousel: false,
  transfer_organization_ownership: false,
  organization_teams: false,
  // lounge feature flags
  lounge_hype_counter: false,
  lounge_comments_counter: false,
  lounge_section: false,
  lounge_verified_filter: false,
  lounge_twitch_stream_highlight: false,
  // applicants flags
  applicants_role_filter: false,
  opportunity_applicant_card_design_revamp: false,
  // game stats section
  games_section: true,
  callofduty_section: true,
  pubg_section: true,
  aimlab_section: true,
  valorant_section: true,
  // creator tooks flags
  creator_tools: false,
  creator_tools_share: false,
  creator_tools_streaming: false,
  creator_tools_schedule: false,
  creator_tools_efuse_options: false,
  creator_tools_twitter: false,
  creator_tools_discord: false,
  creator_tools_facebook: false,
  creator_tools_automate: false,
  posting_twitch_clips: false,
  // Google Adsense
  enable_google_ads: false,
  // Global Cloud Search
  enable_user_org_global_search: false,

  enabled_google_ads_lounge_featured: false,
  pinned_video: false,
  // OneSignal Flags
  enable_web_push_notifications: false,
  // Post As Organization
  enable_post_as_organization: false,
  create_post_modal_from_lounge: false,
  // Comment Media
  enable_giphy: false,
  enable_media_upload_comment: false,
  fandom_signup: false,
  fandom_stream: false,
  // oauth third party signup
  third_party_signup: false,
  signup_login_enable: true,
  // Events
  events_stream_button: false,
  events_login: false,
  rich_presence_live: false,
  // post gihpy
  add_giphy_to_post: false,
  // recommendations
  recommendations_users: false,
  recommendations_learning: false,
  recommendations_opportunities: false,
  recommendations_organizations: false,
  recommendations_show_promoted_label: false,
  // ads
  lounge_top_banner: false,
  lounge_right_square: false,
  // experiences
  onboarding_flow: false,
  enable_views_counter: false,
  enable_views_counter_for_owner: false,
  // eFuse Gold
  enable_gold_image_in_feed: false,
  enable_gold_image_in_user_bio_card: false,
  efuse_gold: false,
  efuse_gold_opportunity_setting: false,

  // discover
  discover_landing: false,
  social_impact_scores: false,
  badges: false,
  streaks_daily_icon: false,
  auto_twitch_stream: false,
  discover_graphql_crousal: false,
  portfolio_graphql: false,
  xbox_live_auth_btn: false,
  enable_snapchat_oauth_btn: false,
  "killswitch:global_disable_lounge_feeds": false,
  youtube_oauth_btn: false,
  aimlab_oauth_btn: false,
  valorant_oauth_btn: false,
  web_erena_in_discover_navigation: false,

  // eRena
  erena_leaderboard: false,
  allow_erena_creation: false,
  feature_web_erena_call_to_action: true,
  web_erena_navigation_pipeline: false,
  web_erena_navigation_watch: false,
  web_erena_navigation_compete: false,
  web_erena_navigation_news: false,
  temp_event_card_kebab_button: false,
  web_erena_games_filter: false,
  temp_video_detail_bar_web: false,
  exp_web_erena_teep_opportunity: false,

  // pipeline
  enable_pipeline_home_page: false,
  temp_pipeline_onboarding: false,

  // Settings page
  recruiting_settings_page: false,
  web_payment_settings_page: false,

  // clippy
  feature_web_clippy: false,
  feature_web_clippy_nav_layout: false,
  exp_web_clippy_landing_page_text: "",

  // leaderboards
  leaderboard_streak: false,
  show_league_of_legends_leaderboard: true,
  show_valorant_leaderboard: true,
  show_aimlab_leaderboard: true,
  show_overwatch_leaderboard: false,

  // atlas search
  enable_atlas_search: false,

  // mulitple hypes
  multiple_hypes: false,

  // New Featured lounge feed on the basis of ranking algorithm
  enable_ranking_based_lounge_featured_feed: false,
  // landing Page
  revamp_landing_page: true,
  complete_portfolio_component: false,

  enable_twitter_auth: false,
  enable_discord_auth: false,
  enable_twitch_auth: false,
  enable_linkedin_auth: false,
  enable_facebook_auth: false,
  enable_snapchat_auth: false,
  enable_youtube_auth: false,
  enable_tiktok_auth: false,
  enable_instagram_auth: false,
  opportunity_additional_information_section: false,

  create_post_modal_revamp: false,
  dark_theme_web: false,
  stream_chat_notification_web: false,
  "killswitch:global_disable_autoplay": false,
  killswitch_disable_cross_posting_create_post_modal: false,
  block_user: false,
  temp_clout_card_web: false,
  user_recruiting: false,
  temp_web_select_games_orgs: false,
  feature_both_affiliate_badge: false,
  feature_web_create_post_composer: false,
  feature_both_partner_button: false,

  // Hype Mutation
  enable_hype_mutation: false,

  feature_web_global_games_create_post: false,
  featured_feed_graphql: false,
  revamp_feeds: false,
  exp_app_download_landing_page: false,

  // Leagues
  feature_league_creation: false,
  exp_web_content_creator_onboarding: "control_no_onboarding",

  // Esports
  web_esports_page: false,

  // New Article Games
  feature_web_add_games_to_new_articles: false
};

const flagSmithReducer = (state = initialState, action) => {
  switch (action.type) {
    case TOGGLE_FEATURE:
      // flags_loaded: tell redux that all feature flags have set in store.
      return Object.assign(state, action.features, { flags_loaded: true });
    default:
      return {
        ...state
      };
  }
};

export default flagSmithReducer;
