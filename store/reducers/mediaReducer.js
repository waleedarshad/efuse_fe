import uniqBy from "lodash/uniqBy";

import { GET_MEDIA, EXPAND_MEDIA, CLOSE_MEDIA } from "../actions/types";

const initialState = {
  media: [],
  pagination: {},
  expandMedia: {},
  showModal: false
};

const mediaReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_MEDIA:
      return {
        ...state,
        media: uniqBy([...state.media, ...action.media], "_id"),
        pagination: action.pagination
      };
    case EXPAND_MEDIA:
      return {
        ...state,
        expandMedia: action.media,
        showModal: true
      };
    case CLOSE_MEDIA:
      return {
        ...state,
        showModal: false,
        expandMedia: {}
      };
    default:
      return {
        ...state
      };
  }
};

export default mediaReducer;
