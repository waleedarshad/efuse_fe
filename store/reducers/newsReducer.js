import uniqBy from "lodash/uniqBy";
import {
  SET_ARTICLE_GAMES,
  GET_NEWS_CATEGORIES,
  SET_CURRENT_ARTICLE,
  GET_NEWS_ARTICLES,
  SET_RECOMMENDATIONS_NEWS,
  SET_SUBMITTING_NEW_ARTICLE_STATE,
  GET_OWNED_NEWS_ARTICLES_FAILURE,
  GET_OWNED_NEWS_ARTICLES_REQUEST,
  GET_OWNED_NEWS_ARTICLES_SUCCESS,
  GET_NEWS_ARTICLES_SUCCESS
} from "../actions/types";

const initialState = {
  articles: null,
  currentArticle: null,
  pagination: {},
  filters: [],
  query: {},
  categories: [],
  recommendedNewsArticles: null,
  submittingNewArticle: false,
  isGettingFirstArticle: false,
  games: []
};

const learningReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_ARTICLE_GAMES:
      return {
        ...state,
        currentArticle: { ...state.currentArticle, associatedGames: action.payload }
      };
    case SET_CURRENT_ARTICLE:
      return {
        ...state,
        currentArticle: action.payload
      };
    case GET_OWNED_NEWS_ARTICLES_REQUEST:
      return {
        ...state,
        isGettingFirstArticle: action.payload
      };
    case GET_OWNED_NEWS_ARTICLES_SUCCESS:
      return {
        ...state,
        articles: action.page === 1 ? action.payload : state.articles.concat(action.payload),
        pagination: action.pagination,
        isGettingFirstArticle: false
      };
    case GET_OWNED_NEWS_ARTICLES_FAILURE:
      return {
        ...state,
        isGettingFirstArticle: false
      };
    case GET_NEWS_ARTICLES_SUCCESS:
      return {
        ...state,
        articles: action.page === 1 ? action.payload : state.articles.concat(action.payload),
        pagination: action.pagination
      };
    case GET_NEWS_ARTICLES:
      return {
        ...state,
        articles: state.articles ? uniqBy([...state.articles, ...action.articles], "_id") : action.articles,
        pagination: action.pagination
      };
    case GET_NEWS_CATEGORIES:
      return {
        ...state,
        categories: action.payload
      };
    case SET_RECOMMENDATIONS_NEWS:
      return {
        ...state,
        recommendedNewsArticles: action.payload
      };
    case SET_SUBMITTING_NEW_ARTICLE_STATE:
      return {
        ...state,
        submittingNewArticle: action.payload
      };

    default:
      return {
        ...state
      };
  }
};

export default learningReducer;
