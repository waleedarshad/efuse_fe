import SOCIAL_LINKING from "../../actions/socialLinkingActions/types";

const initialState = {
  twitterLinked: false,
  youTubeLinked: false,
  twitchLinked: false,
  twitterAccountInfo: {},
  youTubeAccountInfo: {},
  twitchAccountInfo: {}
};

const socialLinkingReducer = (state = initialState, action) => {
  switch (action.type) {
    case SOCIAL_LINKING.TWITTER.LINKED.SUCCESS:
      return {
        ...state,
        twitterLinked: action.payload
      };
    case SOCIAL_LINKING.TWITTER.ACCOUNT_INFO.SUCCESS:
      return {
        ...state,
        twitterAccountInfo: action.payload
      };
    case SOCIAL_LINKING.TWITTER.UNLINK.SUCCESS:
      return {
        ...state,
        twitterLinked: !action.payload,
        twitterAccountInfo: {}
      };
    case SOCIAL_LINKING.YOUTUBE.LINKED.SUCCESS:
      return {
        ...state,
        youTubeLinked: action.payload
      };
    case SOCIAL_LINKING.YOUTUBE.ACCOUNT_INFO.SUCCESS:
      return {
        ...state,
        youTubeAccountInfo: action.payload
      };
    case SOCIAL_LINKING.YOUTUBE.UNLINK.SUCCESS:
      return {
        ...state,
        youTubeLinked: !action.payload,
        youTubeAccountInfo: {}
      };
    case SOCIAL_LINKING.TWITCH.LINKED.SUCCESS:
      return {
        ...state,
        twitchLinked: action.payload
      };
    case SOCIAL_LINKING.TWITCH.ACCOUNT_INFO.SUCCESS:
      return {
        ...state,
        twitchAccountInfo: action.payload
      };
    case SOCIAL_LINKING.TWITCH.UNLINK.SUCCESS:
      return {
        ...state,
        twitchLinked: !action.payload,
        twitchAccountInfo: {}
      };
    default:
      return {
        ...state
      };
  }
};

export default socialLinkingReducer;
