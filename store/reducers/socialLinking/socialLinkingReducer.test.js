import socialLinkingReducer from "./socialLinkingReducer";
import SOCIAL_LINKING from "../../actions/socialLinkingActions/types";

describe("socialLinkingReducer", () => {
  describe("twitter", () => {
    it("sets twitter linking status", () => {
      const prevState = {
        twitterLinked: false
      };

      const action = {
        type: SOCIAL_LINKING.TWITTER.LINKED.SUCCESS,
        payload: true
      };

      const nextState = socialLinkingReducer(prevState, action);

      expect(nextState.twitterLinked).toEqual(true);
    });

    it("sets twitter account info", () => {
      const prevState = {
        twitterAccountInfo: {}
      };

      const action = {
        type: SOCIAL_LINKING.TWITTER.ACCOUNT_INFO.SUCCESS,
        payload: {
          socialAccountName: "twitterUsername",
          supporterCount: 1500
        }
      };

      const nextState = socialLinkingReducer(prevState, action);

      expect(nextState.twitterAccountInfo).toEqual({
        socialAccountName: "twitterUsername",
        supporterCount: 1500
      });
    });

    it("unlinks the twitter account info", () => {
      const prevState = {
        twitterLinked: true,
        twitterAccountInfo: {
          accountName: "aUsername",
          supporterCount: 123
        }
      };

      const action = {
        type: SOCIAL_LINKING.TWITTER.UNLINK.SUCCESS,
        payload: true
      };

      const nextState = socialLinkingReducer(prevState, action);

      expect(nextState.twitterLinked).toEqual(false);
      expect(nextState.twitterAccountInfo).toEqual({});
    });
  });

  describe("youTube", () => {
    it("sets youTube linking status", () => {
      const prevState = {
        youTubeLinked: false
      };

      const action = {
        type: SOCIAL_LINKING.YOUTUBE.LINKED.SUCCESS,
        payload: true
      };

      const nextState = socialLinkingReducer(prevState, action);

      expect(nextState.youTubeLinked).toEqual(true);
    });

    it("sets youTube account info", () => {
      const prevState = {
        youTubeAccountInfo: {}
      };

      const action = {
        type: SOCIAL_LINKING.YOUTUBE.ACCOUNT_INFO.SUCCESS,
        payload: {
          socialAccountName: "youTubeUsername",
          supporterCount: 4000
        }
      };

      const nextState = socialLinkingReducer(prevState, action);

      expect(nextState.youTubeAccountInfo).toEqual({
        socialAccountName: "youTubeUsername",
        supporterCount: 4000
      });
    });

    it("unlinks the youTube account info", () => {
      const prevState = {
        youTubeLinked: true,
        youTubeAccountInfo: {
          accountName: "aUsername",
          supporterCount: 123
        }
      };

      const action = {
        type: SOCIAL_LINKING.YOUTUBE.UNLINK.SUCCESS,
        payload: true
      };

      const nextState = socialLinkingReducer(prevState, action);

      expect(nextState.youTubeLinked).toEqual(false);
      expect(nextState.youTubeAccountInfo).toEqual({});
    });
  });

  describe("Twitch", () => {
    it("sets twitch linking status", () => {
      const prevState = {
        twitchLinked: false
      };

      const action = {
        type: SOCIAL_LINKING.TWITCH.LINKED.SUCCESS,
        payload: true
      };

      const nextState = socialLinkingReducer(prevState, action);

      expect(nextState.twitchLinked).toEqual(true);
    });

    it("sets twitch account info", () => {
      const prevState = {
        twitchAccountInfo: {}
      };

      const action = {
        type: SOCIAL_LINKING.TWITCH.ACCOUNT_INFO.SUCCESS,
        payload: {
          socialAccountName: "twitchName",
          supporterCount: 300
        }
      };

      const nextState = socialLinkingReducer(prevState, action);

      expect(nextState.twitchAccountInfo).toEqual({
        socialAccountName: "twitchName",
        supporterCount: 300
      });
    });

    it("unlinks the twitch account info", () => {
      const prevState = {
        twitchLinked: true,
        twitchAccountInfo: {
          accountName: "aUsername",
          supporterCount: 123
        }
      };

      const action = {
        type: SOCIAL_LINKING.TWITCH.UNLINK.SUCCESS,
        payload: true
      };

      const nextState = socialLinkingReducer(prevState, action);

      expect(nextState.twitchLinked).toEqual(false);
      expect(nextState.twitchAccountInfo).toEqual({});
    });
  });
});
