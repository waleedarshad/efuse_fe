import { GET_ENGAGEMENTS_LEADERBOARD, GET_STREAK_LEADERBOARD, GET_VIEWS_LEADERBOARD } from "../actions/types";
import leaderboardReducer from "./leaderboardReducer";

describe("leaderboardReducer", () => {
  it("sets leaderboard views", () => {
    const prevState = {
      views: []
    };
    const action = {
      type: GET_VIEWS_LEADERBOARD,
      payload: ["user1", "user2"]
    };
    const nextState = leaderboardReducer(prevState, action);

    expect(nextState.views).toEqual(["user1", "user2"]);
  });

  it("sets leaderboard streak", () => {
    const prevState = {
      streak: []
    };
    const action = {
      type: GET_STREAK_LEADERBOARD,
      payload: ["user123", "user321"]
    };
    const nextState = leaderboardReducer(prevState, action);

    expect(nextState.streak).toEqual(["user123", "user321"]);
  });
  it("sets leaderboard engagements", () => {
    const prevState = {
      engagements: []
    };
    const action = {
      type: GET_ENGAGEMENTS_LEADERBOARD,
      payload: ["testUser1", "testUser2"]
    };
    const nextState = leaderboardReducer(prevState, action);

    expect(nextState.engagements).toEqual(["testUser1", "testUser2"]);
  });
});
