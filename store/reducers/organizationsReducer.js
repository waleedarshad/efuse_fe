import omit from "lodash/omit";
import isEmpty from "lodash/isEmpty";
import cloneDeep from "lodash/cloneDeep";
import uniqBy from "lodash/uniqBy";
import {
  GET_ORGANIZATIONS,
  GET_ORGANIZATIONS_REQUEST,
  GET_ORGANIZATIONS_SUCCESS,
  GET_ORGANIZATIONS_FAILURE,
  ORG_SEARCH_BY_NAME,
  GET_ORGANIZATION_SUCCESS,
  ADD_ORGANIZATION_FILTER,
  CLEAR_ORGANIZATION_FILTERS,
  UPDATE_ORGANIZATIONS,
  UPDATE_ORGANIZATION_FILTER_VALUE,
  SET_RECENT_ORGANIZATIONS,
  GET_ADMIN_ORGANIZATIONS,
  JOIN_MEMBER,
  UNJOIN_MEMBER,
  JOIN_ORGANIZATION_MEMBER,
  UNJOIN_ORGANIZATION_MEMBER,
  GET_MEMBERS,
  CHANGE_OWNER,
  JOIN_MEMBER_REQUEST,
  APPROVE_ORG_JOIN_REQUEST,
  REJECT_ORG_JOIN_REQUEST,
  MAKE_CAPTAIN,
  REMOVE_CAPTAIN,
  KICK_ORGANIZATION_MEMBER_SUCCESS,
  BAN_MEMBER,
  UNBAN_MEMBER,
  ORGANIZATION_CURRENT_PAGE,
  CLEAR_ORGANIZATIONS,
  TOGGLE_MODAL_SHOW,
  INVITATION,
  TOGGLE_FOLLOW_DISABLE,
  TOGGLE_ORGANIZATION_FOLLOW,
  GET_ORGANIZATION_FOLLOWERS_SUCCESS,
  ADD_ORGANIZATION,
  CLEAR_MEMBERS,
  CLEAR_ORGANIZATION,
  ORGANIZATION_GAMES,
  ORGANIZATION_SECTION_UPDATED,
  CHANGE_ORGANIZATION_STATUS,
  DELETE_ORGANIZATION,
  TOGGLE_IN_PROGRESS,
  ORGANIZATION_SLUG_ERROR,
  CHANGE_SINGLE_ORGANIZATION_PUBLISH_STATUS,
  MOCK_EXTERNAL_ORGANIZATION_VIEW,
  SET_ORGANIZATION_LOADING,
  SET_RECOMMENDATIONS_ORGANIZATIONS,
  TOGGLE_ORGANIZATION_MODAL,
  GET_ORGANIZATION_REQUESTS_SUCCESS,
  GET_ORGANIZATION_MEMBERS_REQUEST,
  GET_ORGANIZATION_MEMBERS_SUCCESS,
  GET_ORGANIZATION_MEMBERS_FAILURE,
  UPDATE_ORGANIZATION_MEMBER_TITLE_SUCCESS,
  SET_ORGANIZATIONS
} from "../actions/types";

const initialState = {
  organizations: [],
  members: [],
  searchedOrganizations: [],
  organization: {},
  filtersApplied: [],
  searchObject: {},
  pagination: {},
  searchFilters: {
    location: "",
    game: "",
    scale: "",
    "verified.status.retain.key": "",
    organizationType: "",
    status: ""
  },
  recentOrganizations: [],
  requesters: [],
  approved: false,
  view: "",
  modalShow: false,
  invitation: {},
  modalOrgId: null,
  followers: [],
  inProgress: false,
  slugError: "",
  mockExternalOrganization: false,
  loading: false,
  recommendedOrganizations: null,
  modal: false,
  modalSection: "",
  isGettingFirstOrganizations: false,
  hasNextPage: false,
  isGettingFirstMembers: false
};

const organizationReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_ORGANIZATIONS:
      return {
        ...state,
        organizations: uniqBy([...state.organizations, ...action.organizations], "_id"),
        pagination: action.pagination
      };
    case GET_ORGANIZATIONS_REQUEST:
      return {
        ...state,
        isGettingFirstOrganizations: action.payload
      };
    case GET_ORGANIZATIONS_SUCCESS:
      return {
        ...state,
        organizations: action.payload === 1 ? action.payload : state.organizations.concat(action.payload),
        hasNextPage: action.hasNextPage,
        isGettingFirstOrganizations: false
      };
    case GET_ORGANIZATIONS_FAILURE:
      return {
        ...state,
        isGettingFirstOrganizations: false
      };
    case GET_ADMIN_ORGANIZATIONS:
      return {
        ...state,
        organizations: action.organizations,
        pagination: action.pagination
      };
    case ORG_SEARCH_BY_NAME:
      return {
        ...state,
        searchedOrganizations: action.payload
      };
    case GET_ORGANIZATION_SUCCESS:
      return {
        ...state,
        organization: action.payload
      };
    case JOIN_ORGANIZATION_MEMBER:
      return {
        ...state,
        organization: joinOrgMember(state, action.payload)
      };
    case UNJOIN_ORGANIZATION_MEMBER:
      return {
        ...state,
        organization: unjoinOrgMember(state, action.payload),
        members: action.payload
      };
    case ADD_ORGANIZATION_FILTER:
      return {
        ...state,
        filtersApplied: action.payload
      };
    case CLEAR_ORGANIZATION_FILTERS:
      return {
        ...state,
        filtersApplied: [],
        searchObject: {},
        searchFilters: {
          location: "",
          game: "",
          scale: "",
          "verified.status.retain.key": "",
          organizationType: "",
          status: ""
        }
      };
    case UPDATE_ORGANIZATIONS:
      return {
        ...state,
        organizations: action.payload,
        pagination: action.pagination,
        searchObject: action.searchObject
      };
    case UPDATE_ORGANIZATION_FILTER_VALUE:
      return {
        ...state,
        searchFilters: {
          ...state.searchFilters,
          [action.key]: action.value
        }
      };
    case SET_RECENT_ORGANIZATIONS:
      return {
        ...state,
        recentOrganizations: action.payload
      };
    case JOIN_MEMBER:
      return {
        ...state,
        organizations: joinMember(state, action.payload),
        organization: joinOrgMember(state, action)
      };
    case JOIN_MEMBER_REQUEST:
      return {
        ...state,
        organizations: createOrganizationsRequest(state, action.payload),
        organization: createOrganizationRequest(state, action.payload)
      };
    case UNJOIN_MEMBER:
      return {
        ...state,
        organizations: unjoinMember(state, action.payload)
      };
    case GET_MEMBERS:
      return {
        ...state,
        members: uniqBy([...state.members, ...action.members], "_id"),
        pagination: action.pagination
      };
    case GET_ORGANIZATION_MEMBERS_REQUEST:
      return {
        ...state,
        isGettingFirstMembers: true
      };
    case GET_ORGANIZATION_MEMBERS_SUCCESS:
      return {
        ...state,
        members: action.page === 1 ? action.payload : state.members.concat(action.payload),
        hasNextPage: action.hasNextPage,
        isGettingFirstMembers: false
      };
    case GET_ORGANIZATION_MEMBERS_FAILURE:
      return {
        ...state,
        isGettingFirstMembers: false
      };
    case GET_ORGANIZATION_REQUESTS_SUCCESS:
      return {
        ...state,
        requesters: action.page === 1 ? action.payload : state.requesters.concat(action.payload),
        hasNextPage: action.hasNextPage
      };
    case APPROVE_ORG_JOIN_REQUEST:
      return {
        ...state,
        requesters: state.requesters.filter(r => r._id !== action.payload.updatedRequest._id)
      };
    case REJECT_ORG_JOIN_REQUEST:
      return {
        ...state,
        requesters: updateRejectedRequests(state, action)
      };
    case CHANGE_OWNER:
      return {
        ...state,
        organization: {
          ...state.organization,
          user: action.payload
        }
      };
    case MAKE_CAPTAIN:
      return {
        ...state,
        organization: updatedWithCaptains(state, action)
      };
    case REMOVE_CAPTAIN:
      return {
        ...state,
        organization: updatedWithCaptains(state, action)
      };
    case KICK_ORGANIZATION_MEMBER_SUCCESS:
      return {
        ...state,
        members: action.payload
      };
    case BAN_MEMBER:
      return {
        ...state,
        members: updateBannedMember(state, action)
      };
    case UNBAN_MEMBER:
      return {
        ...state,
        members: updateBannedMember(state, action)
      };

    case ORGANIZATION_CURRENT_PAGE:
      return {
        ...state,
        view: action.view
      };
    case CLEAR_ORGANIZATIONS:
      return {
        ...state,
        organizations: [],
        pagination: {}
      };
    case TOGGLE_MODAL_SHOW:
      return {
        ...state,
        modalShow: !state.modalShow,
        modalOrgId: action.payload
      };
    case INVITATION:
      return {
        ...state,
        invitation: action.payload
      };
    case TOGGLE_FOLLOW_DISABLE:
      return {
        ...state,
        ...disableOrganization(state, action)
      };
    case TOGGLE_ORGANIZATION_FOLLOW:
      return {
        ...state,
        ...toggleOrganizationFollow(state, action)
      };
    case GET_ORGANIZATION_FOLLOWERS_SUCCESS:
      return {
        ...state,
        followers: action.page === 1 ? action.payload : state.followers.concat(action.payload),
        pagination: action.pagination
      };
    case ADD_ORGANIZATION:
      return {
        ...state,
        organizations: [action.organization, ...state.organizations]
      };
    case CLEAR_MEMBERS:
      return {
        ...state,
        members: []
      };
    case CLEAR_ORGANIZATION:
      return {
        ...state,
        organization: {}
      };
    case ORGANIZATION_GAMES:
      return {
        ...state,
        organization: { ...state.organization, associatedGames: action.games }
      };
    case ORGANIZATION_SECTION_UPDATED:
      return {
        ...state,
        organization: action.organization
      };
    case CHANGE_ORGANIZATION_STATUS:
      // this reducer is used for the organizations browse page
      return {
        ...state,
        organizations: changePublishStatus(state, action)
      };
    case CHANGE_SINGLE_ORGANIZATION_PUBLISH_STATUS:
      // this reducer is used for the organization view page. This will update a single organization.
      return {
        ...state,
        organization: {
          ...state.organization,
          publishStatus: action.organization.publishStatus
        }
      };
    case DELETE_ORGANIZATION:
      return {
        ...state,
        organizations: [...state.organizations.filter(org => org._id !== action.payload.organizationId)]
      };
    case TOGGLE_IN_PROGRESS:
      return {
        ...state,
        inProgress: action.inProgress
      };
    case ORGANIZATION_SLUG_ERROR:
      return {
        ...state,
        slugError: action.error
      };
    case MOCK_EXTERNAL_ORGANIZATION_VIEW:
      return {
        ...state,
        mockExternalOrganization: action.mockExternalOrganization
      };
    case SET_ORGANIZATION_LOADING:
      return {
        ...state,
        loading: action.payload.loading
      };
    case SET_RECOMMENDATIONS_ORGANIZATIONS:
      return {
        ...state,
        recommendedOrganizations: action.payload
      };
    case TOGGLE_ORGANIZATION_MODAL:
      return {
        ...state,
        modal: action.modal,
        modalSection: action.modalSection
      };
    case UPDATE_ORGANIZATION_MEMBER_TITLE_SUCCESS:
      return {
        ...state,
        members: updateMemberTitle(state, action)
      };
    case SET_ORGANIZATIONS:
      return {
        ...state,
        organizations: uniqBy(action.payload, "_id")
      };
    default:
      return {
        ...state
      };
  }
};

const changePublishStatus = (state, action) => {
  const clonedOrganizations = cloneDeep(state.organizations);
  const orgIndex = state.organizations.findIndex(org => org._id === action.organization._id);
  clonedOrganizations[orgIndex].publishStatus = action.organization.publishStatus;
  return clonedOrganizations;
};
const toggleOrganizationFollow = (state, action) => {
  const { organizations, index } = findOrganization(state, action);
  const organization = { ...state.organization };
  let targettedOrganization = organizations[index] || organization;
  targettedOrganization.disableFollow = false;
  targettedOrganization.followersCount += action.increment;
  if (!action.isFollowed) {
    targettedOrganization = omit(targettedOrganization, ["isCurrentUserAFollower"]);
  } else {
    targettedOrganization.isCurrentUserAFollower = action.followObject;
  }
  if (index >= 0) {
    organizations[index] = targettedOrganization;
  }
  return isEmpty(organization) ? { organizations } : { organizations, organization: { ...targettedOrganization } };
};

const disableOrganization = (state, action) => {
  const { organizations, index } = findOrganization(state, action);
  const organization = { ...state.organization };
  if (index >= 0) {
    organizations[index].disableFollow = action.value;
  }
  return isEmpty(organization)
    ? { organizations }
    : {
        organization: {
          ...organization,
          disableFollow: action.value
        },
        organizations
      };
};

const findOrganization = (state, action) => {
  const organizations = cloneDeep(state.organizations);
  const index = organizations.findIndex(org => org._id === action.organizationId);
  return {
    organizations,
    index
  };
};

const updateBannedMember = (state, action) => {
  const members = cloneDeep(state.members);
  const index = members.findIndex(m => m._id === action.payload._id);
  members[index] = action.payload;
  return members;
};
const updatedWithCaptains = (state, action) => {
  const org = cloneDeep(state.organization);
  org.captains = action.payload.captains;
  return org;
};
const updateRejectedRequests = (state, action) => {
  const requesters = cloneDeep(state.requesters);
  const index = requesters.findIndex(r => r._id === action.payload.rejRequest._id);
  requesters[index] = action.payload.rejRequest;
  return requesters;
};
const createOrganizationsRequest = (state, orgRequest) => {
  const organizations = cloneDeep(state.organizations);
  organizations.filter(org => {
    if (org._id === orgRequest.organization) {
      org.currentUserRequestToJoin = orgRequest;
      return true;
    }
  });
  return organizations;
};

const createOrganizationRequest = (state, orgRequest) => {
  const organization = cloneDeep(state.organization);
  organization.currentUserRequestToJoin = orgRequest;
  return organization;
};
const joinOrgMember = (state, action) => {
  const organization = cloneDeep(state.organization);
  organization.member = action;
  return organization;
};
const unjoinOrgMember = state => {
  const organization = cloneDeep(state.organization);
  delete organization.member;
  return organization;
};
const joinMember = (state, action) => {
  const organizations = cloneDeep(state.organizations);
  organizations.filter(organization => {
    if (organization._id === action.member.organization) {
      organization.member = action.member;
      organization.currentUserRequestToJoin = action.orgRequest;
      return true;
    }
  });
  return organizations;
};

const unjoinMember = (state, action) => {
  const organizations = cloneDeep(state.organizations);
  organizations.filter(organization => {
    if (organization._id === action.id) {
      delete organization.member;
      return true;
    }
  });
  return organizations;
};

const updateMemberTitle = (state, action) => {
  const index = state.members.findIndex(member => member._id === action.payload._id);
  const newMembers = [...state.members];
  newMembers[index].title = action.payload.title;
  return newMembers;
};

export default organizationReducer;
