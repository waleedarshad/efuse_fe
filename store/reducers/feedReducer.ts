import uniqBy from "lodash/uniqBy";
import cloneDeep from "lodash/cloneDeep";

import { FEED, SET_FEED_HYPES, SET_FEED_USER_HYPES } from "../actions/types";
import { Post, PaginatedComments, PaginatedHypeUser } from "../../graphql/feeds/Models";
import { CommentModel } from "../../graphql/comments/Models";
import { Hype } from "../../graphql/interface/Hype";

interface FeedReducer {
  posts: Post[];
  isGettingFeed: boolean;
  pagination: {
    hasNextPage: boolean;
    page: number;
  };
  postToExpand: Post;
  disableHypes: boolean;
  postToEdit: null;
}

const initialState: FeedReducer = {
  posts: [],
  isGettingFeed: false,
  pagination: {
    hasNextPage: false,
    page: 0
  },
  postToExpand: null,
  disableHypes: null,
  postToEdit: null
};

const feedReducer = (state = initialState, action) => {
  switch (action.type) {
    case FEED.CLEAR:
      return initialState;
    case FEED.GET.FEATURED_FEED.REQUEST:
    case FEED.GET.FOLLOWING_FEED.REQUEST:
    case FEED.GET.VERIFIED_FEED.REQUEST:
    case FEED.GET.USER_FEED.REQUEST:
    case FEED.GET.ORG_FEED.REQUEST:
      return {
        ...state,
        isGettingFeed: true
      };
    case FEED.GET.FEATURED_FEED.SUCCESS:
    case FEED.GET.FOLLOWING_FEED.SUCCESS:
    case FEED.GET.VERIFIED_FEED.SUCCESS:
    case FEED.GET.USER_FEED.SUCCESS:
    case FEED.GET.ORG_FEED.SUCCESS:
      return {
        ...state,
        posts: uniqBy([...state.posts, ...action.payload.docs], "parentFeedId"),
        pagination: action.payload.pagination,
        isGettingFeed: false
      };
    case FEED.GET.FEATURED_FEED.FAILURE:
    case FEED.GET.FOLLOWING_FEED.FAILURE:
    case FEED.GET.VERIFIED_FEED.FAILURE:
    case FEED.GET.USER_FEED.FAILURE:
    case FEED.GET.ORG_FEED.FAILURE:
      return {
        ...state,
        isGettingFeed: false
      };
    case SET_FEED_HYPES:
      return {
        ...state,
        posts: state.posts.map(post => {
          if (post.feedId === action.payload.feedId) {
            return { ...post, hypes: action.payload.hypes };
          }
          return post;
        })
      };
    case SET_FEED_USER_HYPES:
      return {
        ...state,
        posts: state.posts.map(post => {
          if (post.feedId === action.payload.feedId) {
            return { ...post, currentUserHypes: action.payload.userHypes };
          }
          return post;
        })
      };
    case FEED.FOLLOW.USER.SUCCESS:
    case FEED.UNFOLLOW.USER.SUCCESS:
      return {
        ...state,
        posts: state.posts.map(post => {
          if (post.author._id === action.payload.userId) {
            return { ...post, currentUserFollows: action.payload.isFollowing };
          }
          return post;
        })
      };
    case FEED.FOLLOW.ORGANIZATION.SUCCESS:
    case FEED.UNFOLLOW.ORGANIZATION.SUCCESS:
      return {
        ...state,
        posts: state.posts.map(post => {
          if (post.kind === "orgPost" && post.organization.id === action.payload.organizationId) {
            return { ...post, currentUserFollows: action.payload.isFollowing };
          }
          return post;
        })
      };
    case FEED.POST.REMOVE_FROM_HOME.SUCCESS:
    case FEED.POST.HIDE_FROM_LOUNGE.SUCCESS:
    case FEED.POST.DELETE.SUCCESS:
    case FEED.POST.REPORT.SUCCESS:
      return {
        ...state,
        posts: state.posts.filter(post => post.feedId !== action.payload.feedId)
      };

    case FEED.POST.UPDATE_BOOST.SUCCESS:
      return {
        ...state,
        posts: state.posts.map(post => {
          if (post.feedId === action.payload.feedId) {
            return { ...post, boosted: action.payload.isBoosted };
          }
          return post;
        })
      };
    case FEED.POST.CREATE_POST.SUCCESS: {
      return {
        ...state,
        ...createFeedPost(state, action)
      };
    }
    case FEED.POST.UPDATE_POST.SUCCESS:
      return {
        ...state,
        ...updateFeedPost(state, action)
      };

    case FEED.POST.EXPAND_POST.SUCCESS:
      return {
        ...state,
        postToExpand: action.payload.post
      };
    case FEED.COMMENTS.ENABLE.REQUEST:
      return {
        ...state,
        ...togglePostComments(state, action)
      };
    case FEED.COMMENTS.LOADER:
      return {
        ...state,
        ...toggleFeedCommentsLoader(state, action)
      };
    case FEED.COMMENTS.GET.FEED_COMMENT.SUCCESS:
      return {
        ...state,
        ...updatePostWithComments(state, action)
      };
    case FEED.COMMENTS.GET.FEED_THREAD_COMMENT.REQUEST:
      return {
        ...state,
        ...updateCommentThreadLoadingStatus(state, action)
      };
    case FEED.COMMENTS.GET.FEED_THREAD_COMMENT.SUCCESS:
      return {
        ...state,
        ...updatePostCommentWithThreads(state, action)
      };
    case FEED.COMMENTS.HYPE.REQUEST:
      return {
        ...state,
        disableHypes: true
      };
    case FEED.COMMENTS.HYPE.SUCCESS:
      return {
        ...state,
        ...updatePostcommentWithHype(state, action),
        disableHypes: false
      };
    case FEED.COMMENTS.UNHYPE.REQUEST:
      return {
        ...state,
        disableHypes: true
      };
    case FEED.COMMENTS.UNHYPE.SUCCESS:
      return {
        ...state,
        ...updatePostcommentWithUnHype(state, action),
        disableHypes: false
      };
    case FEED.COMMENTS.CREATE.SUCCESS:
      return {
        ...state,
        ...addPostComment(state, action)
      };
    case FEED.COMMENTS.UPDATE.SUCCESS:
      return {
        ...state,
        ...updatePostComment(state, action)
      };
    case FEED.COMMENTS.DELETE.SUCCESS:
      return {
        ...state,
        ...deletePostComment(state, action)
      };
    case FEED.COMMENTS.EDIT.SUCCESS:
      return {
        ...state,
        ...togglePostCommentEdit(state, action)
      };
    case FEED.COMMENTS.ENABLE_THREAD.SUCCESS: {
      return {
        ...state,
        ...togglePostCommentReply(state, action)
      };
    }
    case FEED.POST.HYPE_USER.REQUEST: {
      return {
        ...state,
        ...toggleHypeUserRequest(state, action)
      };
    }
    case FEED.POST.HYPE_USER.SUCCESS: {
      return {
        ...state,
        ...hypeUserSuccess(state, action)
      };
    }
    case FEED.POST.HYPE_USER.CLEAR: {
      return {
        ...state,
        ...clearHypeUser(state, action)
      };
    }
    case FEED.COMMENTS.HYPE_USER.REQUEST: {
      return {
        ...state,
        ...toggleCommentHypeUserRequest(state, action)
      };
    }
    case FEED.COMMENTS.HYPE_USER.SUCCESS: {
      return {
        ...state,
        ...commentHypeUserSuccess(state, action)
      };
    }
    case FEED.POST.FOLLOW_HYPE_USER.SUCCESS: {
      return {
        ...state,
        ...toggleHypePostButton(state, action)
      };
    }
    case FEED.GET.EDIT_POST.REQUEST: {
      return {
        ...state,
        postToEdit: action.payload.post
      };
    }
    case FEED.GET.EDIT_POST.SUCCESS: {
      return {
        ...state,
        postToEdit: null
      };
    }
    default:
      return state;
  }
};

export default feedReducer;

const createFeedPost = (state: FeedReducer, action: { payload: { post: Post } }) => {
  const { posts } = getObjectsFromPostCollection(state, null);
  const { post } = action.payload;
  post.isNewPost = true;

  posts.unshift(post);

  return { posts };
};

const updateFeedPost = (state: FeedReducer, action: { payload: { post: Post } }) => {
  const { post: updatedPost } = action.payload;

  const { post, posts } = getObjectsFromPostCollection(state, updatedPost.feedId);

  if (post) {
    const { mentions, text, media, giphy } = updatedPost;

    post.mentions = mentions;
    post.text = text;
    post.media = media;
    post.giphy = giphy;
  }
  post.isNewPost = updatedPost.isNewPost;

  return { post, posts };
};

const togglePostComments = (
  state: FeedReducer,
  action: { type: string; payload: { feedId: string; key: string } }
): { posts: Post[]; postToExpand: Post } => {
  const {
    payload: { feedId, key }
  } = action;

  const { post, posts } = getObjectsFromPostCollection(state, feedId);

  if (post) {
    if (key) {
      post.enableCommentCreation = key === "enable";
    } else {
      // eslint-disable-next-line no-param-reassign
      post.enableCommentCreation = !post.enableCommentCreation;
    }
  }

  return { posts, postToExpand: setExpandedPost(state, post) };
};

const toggleFeedCommentsLoader = (
  state: FeedReducer,
  action: { type: string; payload: { feedId: string; value: boolean } }
): { posts: Post[]; postToExpand: Post } => {
  const {
    payload: { feedId, value }
  } = action;

  const { posts, post } = getObjectsFromPostCollection(state, feedId);
  if (post) {
    post.isFetchingPostComments = value;
    post.enableCommentCreation = true;
  }
  return { posts, postToExpand: setExpandedPost(state, post) };
};

const setExpandedPost = (state: FeedReducer, post: Post): Post => {
  const updatedPost = post || state.postToExpand;
  return state.postToExpand ? updatedPost : state.postToExpand;
};

const updatePostWithComments = (
  state: FeedReducer,
  action: { type: string; payload: { feedId: string; comments: PaginatedComments } }
): { posts: Post[]; postToExpand: Post } => {
  const {
    payload: { feedId, comments }
  } = action;

  const { post, posts } = getObjectsFromPostCollection(state, feedId);

  if (post) {
    if (!post.postComments) {
      post.postComments = comments;
    } else {
      post.postComments.docs = uniqBy([...post.postComments.docs, ...action.payload.comments.docs], "_id");
    }

    post.postComments.pagination = action.payload.comments.pagination;
  }

  return { posts, postToExpand: setExpandedPost(state, post) };
};

const updatePostCommentWithThreads = (
  state: FeedReducer,
  action: { type: string; payload: { feedId: string; parentCommentId: string; commentThread: PaginatedComments } }
): { posts: Post[]; postToExpand: Post } => {
  const { feedId, parentCommentId, commentThread } = action.payload;

  const { post, comment, posts } = getObjectsFromPostCollection(state, feedId, parentCommentId);

  if (comment) {
    const { pagination, docs } = commentThread;

    comment.thread = {
      docs: uniqBy([...comment.thread.docs, ...docs], "_id"),
      totalDocs: pagination.totalDocs,
      hasNextPage: pagination.hasNextPage,
      page: pagination.page
    };
    comment.threadLoading = false;
  }

  return { posts, postToExpand: setExpandedPost(state, post) };
};

const updateCommentThreadLoadingStatus = (
  state: FeedReducer,
  action: { type: string; payload: { feedId: string; parentCommentId: string } }
): { posts: Post[]; postToExpand: Post } => {
  const { feedId, parentCommentId } = action.payload;

  const { post, comment, posts } = getObjectsFromPostCollection(state, feedId, parentCommentId);

  if (comment) {
    comment.threadLoading = true;
  }

  return { posts, postToExpand: setExpandedPost(state, post) };
};

const updatePostcommentWithHype = (
  state: FeedReducer,
  action: {
    payload: {
      feedId: string;
      parentCommentId: string;
      commentId: string;
      hype: {
        newHype: boolean;
        hype: Hype;
      };
    };
  }
): { posts: Post[]; postToExpand: Post } => {
  const { feedId, parentCommentId, commentId, hype } = action.payload;

  if (parentCommentId) {
    const { post, commentReply, posts } = getObjectsFromPostCollection(state, feedId, parentCommentId, commentId);
    if (commentReply && hype.newHype) {
      commentReply.likes += 1;
      commentReply.hype = hype.hype;
    }
    return { posts, postToExpand: setExpandedPost(state, post) };
  }

  const { post, comment, posts } = getObjectsFromPostCollection(state, feedId, commentId);
  if (comment && hype.newHype) {
    comment.likes += 1;
    comment.hype = hype.hype;
  }

  return { posts, postToExpand: setExpandedPost(state, post) };
};

const updatePostcommentWithUnHype = (
  state: FeedReducer,
  action: { payload: { feedId: string; parentCommentId: string; commentId: string; hype: Hype } }
): { posts: Post[]; postToExpand: Post } => {
  const { feedId, parentCommentId, commentId } = action.payload;

  if (parentCommentId) {
    const { post, commentReply, posts } = getObjectsFromPostCollection(state, feedId, parentCommentId, commentId);
    if (commentReply) {
      commentReply.likes -= 1;
      commentReply.hype = null;
    }
    return { posts, postToExpand: setExpandedPost(state, post) };
  }

  const { post, comment, posts } = getObjectsFromPostCollection(state, feedId, commentId);
  if (comment) {
    comment.likes -= 1;
    comment.hype = null;
  }
  return { posts, postToExpand: setExpandedPost(state, post) };
};

const addCommentToPost = (
  state: FeedReducer,
  feedId: string,
  newComment: CommentModel
): { posts: Post[]; postToExpand: Post } => {
  const { post, posts } = getObjectsFromPostCollection(state, feedId);

  post.comments += 1;
  if (post.postComments) {
    const { docs } = post.postComments;
    post.postComments.docs = uniqBy([newComment, ...docs], "_id");
  } else {
    post.postComments = { docs: [newComment], pagination: { hasNextPage: false, page: 1, totalDocs: 1 } };
  }
  return { posts, postToExpand: setExpandedPost(state, post) };
};

const addCommentReply = (
  state: FeedReducer,
  feedId: string,
  parentCommentId: string,
  newComment: CommentModel
): { posts: Post[]; postToExpand: Post } => {
  const { post, comment: parentComment, posts } = getObjectsFromPostCollection(state, feedId, parentCommentId);

  post.comments += 1;

  if (parentComment.thread) {
    const { thread } = parentComment;
    thread.docs = uniqBy([...thread.docs, newComment], "_id");
  } else {
    parentComment.thread = {
      docs: [newComment],
      hasNextPage: false,
      page: 1,
      totalDocs: 1
    };
  }
  return { posts, postToExpand: setExpandedPost(state, post) };
};

const addPostComment = (
  state: FeedReducer,
  action: {
    payload: {
      feedId: string;
      parent: string;
      comment: CommentModel;
    };
  }
): { posts: Post[]; postToExpand: Post } => {
  const { feedId, parent, comment: newComment } = action.payload;
  if (parent) {
    return addCommentReply(state, feedId, parent, newComment);
  }
  return addCommentToPost(state, feedId, newComment);
};

const updatePostComment = (
  state: FeedReducer,
  action: {
    payload: {
      feedId: string;
      comment: CommentModel;
    };
  }
): { posts: Post[]; postToExpand: Post } => {
  const { feedId, comment: updatedComment } = action.payload;

  if (updatedComment.parent) {
    return updateCommentReplyContent(state, feedId, updatedComment.parent, updatedComment);
  }

  return updateCommentContent(state, feedId, updatedComment);
};

const updateCommentReplyContent = (
  state: FeedReducer,
  feedId: string,
  parentId: string,
  updatedComment: CommentModel
): { posts: Post[]; postToExpand: Post } => {
  const { post, commentReply, posts } = getObjectsFromPostCollection(state, feedId, parentId, updatedComment._id);

  if (commentReply) {
    const { content, mentions, media, giphy } = updatedComment;
    commentReply.content = content;
    commentReply.mentions = mentions;
    commentReply.giphy = giphy;

    if (media) {
      commentReply.media = media;
    } else {
      commentReply.media = null;
    }
  }

  commentReply.enableEdit = !commentReply.enableEdit;
  return { posts, postToExpand: setExpandedPost(state, post) };
};

const updateCommentContent = (
  state: FeedReducer,
  feedId: string,
  updatedComment: CommentModel
): { posts: Post[]; postToExpand: Post } => {
  const { post, comment, posts } = getObjectsFromPostCollection(state, feedId, updatedComment._id);

  if (comment) {
    const { content, mentions, media, giphy } = updatedComment;
    comment.content = content;
    comment.mentions = mentions;
    comment.giphy = giphy;

    if (media) {
      comment.media = media;
    } else {
      comment.media = null;
    }
  }

  comment.enableEdit = !comment.enableEdit;
  return { posts, postToExpand: setExpandedPost(state, post) };
};

const deletePostComment = (
  state: FeedReducer,
  action: {
    payload: {
      parent: string;
      feedId: string;
      comment: CommentModel;
    };
  }
): { posts: Post[]; postToExpand: Post } => {
  const { feedId, parent, comment: deletedComment } = action.payload;

  if (parent) {
    return deleteCommentReply(state, feedId, parent, deletedComment);
  }

  return deleteCommentFromPost(state, feedId, deletedComment);
};

const togglePostCommentEdit = (
  state: FeedReducer,
  action: {
    payload: {
      feedId: string;
      commentId: string;
      parent: string;
    };
  }
): { posts: Post[]; postToExpand: Post } => {
  const { feedId, parent, commentId } = action.payload;
  if (parent) {
    const { post, commentReply, posts } = getObjectsFromPostCollection(state, feedId, parent, commentId);
    commentReply.enableEdit = !commentReply.enableEdit;
    return { posts, postToExpand: setExpandedPost(state, post) };
  }

  const { post, comment, posts } = getObjectsFromPostCollection(state, feedId, commentId);
  if (comment) {
    comment.enableEdit = !comment.enableEdit;
  }
  return { posts, postToExpand: setExpandedPost(state, post) };
};

const togglePostCommentReply = (
  state: FeedReducer,
  action: {
    payload: {
      feedId: string;
      commentId: string;
      callback?: () => void;
    };
  }
): { posts: Post[]; postToExpand: Post } => {
  const { feedId, commentId, callback } = action.payload;

  const { post, comment, posts } = getObjectsFromPostCollection(state, feedId, commentId);

  if (comment) {
    if (comment.enableThreadReply) {
      callback();
    } else {
      comment.enableThreadReply = !comment.enableThreadReply;
    }
  }

  return { posts, postToExpand: setExpandedPost(state, post) };
};

const toggleHypeUserRequest = (
  state: FeedReducer,
  action: {
    payload: {
      feedId: string;
    };
  }
): { posts: Post[]; postToExpand: Post } => {
  const { feedId } = action.payload;

  const { post, posts } = getObjectsFromPostCollection(state, feedId);

  post.loadHypeUser = !post.loadHypeUser;

  return { posts, postToExpand: setExpandedPost(state, post) };
};

const hypeUserSuccess = (
  state: FeedReducer,
  action: {
    payload: {
      feedId: string;
      hypeUsers: PaginatedHypeUser;
    };
  }
): { posts: Post[]; postToExpand: Post } => {
  const { feedId, hypeUsers } = action.payload;

  const { post, posts } = getObjectsFromPostCollection(state, feedId);

  if (post.hypedUser) {
    post.hypedUser = {
      docs: uniqBy([...post.hypedUser.docs, ...hypeUsers.docs], "id"),
      pagination: hypeUsers.pagination
    };
  } else {
    post.hypedUser = {
      docs: hypeUsers.docs,
      pagination: hypeUsers.pagination
    };
  }

  return { posts, postToExpand: setExpandedPost(state, post) };
};

const toggleCommentHypeUserRequest = (
  state: FeedReducer,
  action: {
    payload: {
      feedId: string;
      parentId: string;
      commentId: string;
    };
  }
): { posts: Post[]; postToExpand: Post } => {
  const { feedId, parentId, commentId } = action.payload;
  let postCommentId = commentId;

  if (parentId) {
    postCommentId = parentId;
  }

  const { post, comment, commentReply, posts } = getObjectsFromPostCollection(state, feedId, postCommentId, commentId);

  if (commentReply) {
    commentReply.loadHypeUser = !commentReply.loadHypeUser;
  } else {
    comment.loadHypeUser = !comment.loadHypeUser;
  }

  return { posts, postToExpand: setExpandedPost(state, post) };
};

const commentHypeUserSuccess = (
  state: FeedReducer,
  action: {
    payload: {
      feedId: string;
      commentId: string;
      parentId: string;
      hypeUsers: PaginatedHypeUser;
    };
  }
): { posts: Post[]; postToExpand: Post } => {
  const { feedId, parentId, commentId, hypeUsers } = action.payload;

  if (parentId) {
    return populateReplyHypeUser(state, feedId, parentId, commentId, hypeUsers);
  }
  return populateCommentHypeUser(state, feedId, commentId, hypeUsers);
};

const populateCommentHypeUser = (
  state: FeedReducer,
  feedId: string,
  commentId: string,
  hypeUsers: PaginatedHypeUser
): {
  posts: Post[];
  postToExpand: Post;
} => {
  const { post, comment, posts } = getObjectsFromPostCollection(state, feedId, commentId);
  if (comment) {
    if (comment.hypedUser) {
      comment.hypedUser = {
        docs: uniqBy([...comment.hypedUser.docs, ...hypeUsers.docs], "id"),
        pagination: hypeUsers.pagination
      };
    } else {
      comment.hypedUser = {
        docs: hypeUsers.docs,
        pagination: hypeUsers.pagination
      };
    }
  }
  return { posts, postToExpand: setExpandedPost(state, post) };
};

const populateReplyHypeUser = (
  state: FeedReducer,
  feedId: string,
  parentId: string,
  commentId: string,
  hypeUsers: PaginatedHypeUser
): {
  posts: Post[];
  postToExpand: Post;
} => {
  const { post, commentReply, posts } = getObjectsFromPostCollection(state, feedId, parentId, commentId);
  if (commentReply) {
    if (commentReply.hypedUser) {
      commentReply.hypedUser = {
        docs: uniqBy([...commentReply.hypedUser.docs, ...hypeUsers.docs], "id"),
        pagination: hypeUsers.pagination
      };
    } else {
      commentReply.hypedUser = {
        docs: hypeUsers.docs,
        pagination: hypeUsers.pagination
      };
    }
  }
  return { posts, postToExpand: setExpandedPost(state, post) };
};

const clearHypeUser = (
  state: FeedReducer,
  action: {
    payload: {
      feedId: string;
      commentId?: string;
    };
  }
): { posts: Post[]; postToExpand: Post } => {
  const { feedId, commentId } = action.payload;
  const { post, comment, posts } = getObjectsFromPostCollection(state, feedId, commentId);

  post.hypedUser = {
    docs: []
  };

  if (comment) {
    comment.hypedUser = { docs: [], pagination: null };
  }
  return { posts, postToExpand: setExpandedPost(state, post) };
};

const toggleHypePostButton = (
  state: FeedReducer,
  action: {
    payload: {
      userId: string;
      isFollowed: boolean;
      type: string;
      feedId: string;
      commentId: string;
      parentId: string;
    };
  }
): { posts: Post[]; postToExpand: Post } => {
  const { userId, isFollowed, type, feedId, commentId, parentId } = action.payload;

  if (type === "posts") {
    return followUserPost(state, feedId, userId, isFollowed);
  }
  if (parentId) {
    return followReplyUser(state, feedId, parentId, commentId, userId, isFollowed);
  }
  return followCommentUser(state, feedId, commentId, userId, isFollowed);
};

const followUserPost = (state, feedId, userId, isFollowed) => {
  const { post, posts } = getObjectsFromPostCollection(state, feedId);

  toggleFollowUser(post, userId, isFollowed);

  return { posts, postToExpand: setExpandedPost(state, post) };
};

const followCommentUser = (state, feedId, commentId, userId, isFollowed) => {
  const { post, comment, posts } = getObjectsFromPostCollection(state, feedId, commentId, commentId);

  toggleFollowUser(comment, userId, isFollowed);

  return { posts, postToExpand: setExpandedPost(state, post) };
};

const followReplyUser = (state, feedId, parentId, commentId, userId, isFollowed) => {
  const { post, commentReply, posts } = getObjectsFromPostCollection(state, feedId, parentId, commentId);

  toggleFollowUser(commentReply, userId, isFollowed);

  return { posts, postToExpand: setExpandedPost(state, post) };
};

const toggleFollowUser = (hypedObj, userId, isFollowed) => {
  if (!hypedObj) {
    return;
  }

  hypedObj.hypedUser.docs.forEach(user => {
    if (user.id === userId) {
      // eslint-disable-next-line no-param-reassign
      user.currentUserFollows = isFollowed;
    }
  });
};

const deleteCommentReply = (
  state: FeedReducer,
  feedId: string,
  parentCommentId: string,
  deletedComment: CommentModel
): { posts: Post[]; postToExpand: Post } => {
  const { post, comment, commentReply, posts } = getObjectsFromPostCollection(
    state,
    feedId,
    parentCommentId,
    deletedComment._id
  );
  if (comment) {
    const commentDocs = comment.thread.docs.filter(doc => doc._id !== commentReply._id);
    comment.thread.docs = commentDocs;
  }
  return { posts, postToExpand: setExpandedPost(state, post) };
};

const deleteCommentFromPost = (
  state: FeedReducer,
  feedId: string,
  deletedComment: CommentModel
): { posts: Post[]; postToExpand: Post } => {
  const { post, comment, posts } = getObjectsFromPostCollection(state, feedId, deletedComment._id);
  if (comment) {
    const commentDocs = post.postComments.docs.filter(doc => doc._id !== comment._id);
    post.postComments.docs = commentDocs;
  }
  return { posts, postToExpand: setExpandedPost(state, post) };
};

interface PostObjectResponse {
  posts: Post[];
  post?: Post;
  comment?: CommentModel;
  commentReply?: CommentModel;
}

const getObjectsFromPostCollection = (
  state: FeedReducer,
  feedId: string,
  commentId?: string,
  commentReplyId?: string
): PostObjectResponse => {
  const feed: PostObjectResponse = {
    posts: [],
    post: undefined,
    comment: undefined,
    commentReply: undefined
  };

  const feedPosts = cloneDeep(state.posts);

  feedPosts.find(post => {
    let comment: CommentModel;
    let commentReply: CommentModel;
    feed.posts = feedPosts;

    if (post.feedId === feedId) {
      if (commentId && post.postComments) {
        const { docs } = post.postComments;
        comment = getCommenById(docs, commentId);
      }

      if (comment && comment.thread && commentReplyId) {
        const { docs } = comment.thread;
        commentReply = getCommenById(docs, commentReplyId);
      }

      feed.post = post;
      feed.comment = comment;
      feed.commentReply = commentReply;

      return post;
    }

    return false;
  });

  return feed;
};

const getCommenById = (docs: CommentModel[], commentId: string) => {
  return docs.find(comment => comment._id === commentId);
};
