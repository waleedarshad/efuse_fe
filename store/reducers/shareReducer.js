import { SHARE_LINK_MODAL } from "../actions/types";
import { SHARE_LINK_EVENTS } from "../../common/analyticEvents";

const initialState = {
  shareLinkInfo: { show: false, title: null, link: null, kind: null }
};
const shareReducer = (state = initialState, action) => {
  switch (action.type) {
    case SHARE_LINK_MODAL:
      if (action.show) {
        analytics.track(SHARE_LINK_EVENTS.MODAL_OPENED, {
          kind: action.kind,
          link: action.link
        });
      } else {
        analytics.track(SHARE_LINK_EVENTS.MODAL_CLOSED, {
          kind: action.kind,
          link: action.link
        });
      }

      return {
        ...state,
        shareLinkInfo: {
          show: action.show,
          title: action.title,
          link: action.link,
          kind: action.kind
        }
      };
    default:
      return state;
  }
};

export default shareReducer;
