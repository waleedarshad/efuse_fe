import { SET_ERRORS, INVALID_REQUEST } from "../actions/types";

const initialState = {
  errors: {},
  responseCode: null
};

const errorReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_ERRORS:
      return {
        ...state,
        errors: action.payload
      };
    case INVALID_REQUEST:
      return {
        ...state,
        responseCode: action.responseCode
      };
    default:
      return {
        ...state
      };
  }
};

export default errorReducer;
