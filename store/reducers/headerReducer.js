import { TOGGLE_HEADER_MENU, TOGGLE_HEADER_CREATE_DROPDOWN } from "../actions/types";

const initialState = {
  isMenuOpen: false,
  isCreateDropdownOpen: false
};

const themeReducer = (state = initialState, action) => {
  switch (action.type) {
    case TOGGLE_HEADER_MENU: {
      return {
        ...state,
        isMenuOpen: !state.isMenuOpen,
        isCreateDropdownOpen: false
      };
    }
    case TOGGLE_HEADER_CREATE_DROPDOWN:
      return {
        ...state,
        isCreateDropdownOpen: !state.isCreateDropdownOpen,
        isMenuOpen: false
      };
    default:
      return {
        ...state
      };
  }
};

export default themeReducer;
