import map from "lodash/map";
import cloneDeep from "lodash/cloneDeep";
import uniqBy from "lodash/uniqBy";

import {
  NOTIFICATIONS_BOX_TOGGLE,
  GET_NOTIFICATIONS,
  GET_NOTIFICATIONS_COUNT,
  BUMP_NOTIFICATION_COUNT,
  CLEAR_NOTIFICATIONS_COUNT,
  ORG_JOIN_INVITE_ACTION,
  MARK_ALL_AS_READ,
  MARK_AS_READ
} from "../actions/types";

const initialState = {
  show: false,
  notifications: [],
  pagination: {},
  count: 0
};

const notificationsReducer = (state = initialState, action) => {
  switch (action.type) {
    case NOTIFICATIONS_BOX_TOGGLE:
      return {
        ...state,
        show: action.show
      };
    case GET_NOTIFICATIONS:
      return {
        ...state,
        notifications: uniqBy([...state.notifications, ...action.notifications], "_id"),
        pagination: action.pagination
      };
    case GET_NOTIFICATIONS_COUNT:
      return {
        ...state,
        count: action.count
      };
    case BUMP_NOTIFICATION_COUNT:
      return {
        ...state,
        count: state.count + 1
      };
    case CLEAR_NOTIFICATIONS_COUNT:
      return {
        ...state,
        count: 0
      };
    case MARK_ALL_AS_READ:
      return {
        ...state,
        count: 0,
        notifications: readAll(state.notifications)
      };
    case MARK_AS_READ:
      return {
        ...state,
        notifications: markRead(state.notifications)
      };
    case ORG_JOIN_INVITE_ACTION:
      return {
        ...state,
        notifications: OrgJoinAction(state, action)
      };
    default:
      return {
        ...state
      };
  }
};

const markRead = (state, notificationId) =>
  map(state.notifications, obj => {
    if (obj._id === notificationId) {
      obj.read = true;
      return obj;
    }
    return obj;
  });

const readAll = state =>
  map(state.notifications, obj => {
    obj.read = true;
    return obj;
  });

const OrgJoinAction = (state, action) => {
  const { invitation } = action.payload;
  const clonedNotifications = cloneDeep(state.notifications);
  const updatedNotificationIndex = state.notifications.findIndex(n => n.notifyable._id === invitation._id);
  clonedNotifications[updatedNotificationIndex].notifyable.status = invitation.status;
  return clonedNotifications;
};

export default notificationsReducer;
