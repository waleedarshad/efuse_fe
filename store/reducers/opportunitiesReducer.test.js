import { GET_RECENT_OPPORTUNITIES_BY_TYPE } from "../actions/types";
import opportunitiesReducer from "./opportunitiesReducer";

describe("opportunitiesReducer", () => {
  it("get Recent opportunity by types", () => {
    const prevState = { recentOpportunitiesByTypes: { scholarships: [], events: [], teamOpenings: [], jobs: [] } };

    const action = {
      type: GET_RECENT_OPPORTUNITIES_BY_TYPE,
      payload: {
        scholarships: ["scholarship1", "scholarship2"],
        events: ["event1", "event2", "event3", "event4"],
        teamOpenings: ["teamOpening1"],
        jobs: ["job1", "job2", "job3"]
      }
    };

    const nextState = opportunitiesReducer(prevState, action);
    expect(nextState.recentOpportunitiesByTypes.scholarships).toEqual(["scholarship1", "scholarship2"]);
    expect(nextState.recentOpportunitiesByTypes.events).toEqual(["event1", "event2", "event3", "event4"]);
    expect(nextState.recentOpportunitiesByTypes.teamOpenings).toEqual(["teamOpening1"]);
    expect(nextState.recentOpportunitiesByTypes.jobs).toEqual(["job1", "job2", "job3"]);
  });
});
