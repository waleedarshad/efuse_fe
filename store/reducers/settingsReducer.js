import { CROPPER_MODAL } from "../actions/types";

const initialState = {
  authWindow: {},
  cropperModal: false,
  cropperModalName: ""
};

const settingsReducer = (state = initialState, action) => {
  switch (action.type) {
    case CROPPER_MODAL:
      return {
        ...state,
        cropperModal: action.modal,
        cropperModalName: action.name
      };
    default:
      return {
        ...state
      };
  }
};

export default settingsReducer;
