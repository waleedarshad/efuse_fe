import { CHAT_THREAD_LOADER, CONTENT_LOADER, TOGGLE_LOADER, TOGGLE_SWITCH_LOADER } from "../actions/types";

const initialState = {
  loading: true,
  contentLoading: false,
  contentInstances: [],
  instances: 0,
  switchLoader: ""
};

const loaderReducer = (state = initialState, action) => {
  switch (action.type) {
    case TOGGLE_LOADER:
      return {
        ...state,
        ...toggleLoader(state, action)
      };
    case CONTENT_LOADER:
      return {
        ...state,
        ...toggleContentLoader(state, action)
      };
    case CHAT_THREAD_LOADER:
      return {
        ...state,
        chatThreadLoading: action.show
      };
    case TOGGLE_SWITCH_LOADER:
      return {
        ...state,
        switchLoader: action.switch
      };
    default:
      return {
        ...state
      };
  }
};

const toggleContentLoader = (state, action) => {
  const { show, section } = action;
  let contentInstances = [...state.contentInstances];
  show ? contentInstances.push(section) : (contentInstances = contentInstances.filter(ci => ci !== section));
  return {
    contentLoading: show,
    contentInstances
  };
};

const toggleLoader = (state, action) => {
  const { show } = action;
  let { instances } = state;
  if (show) {
    instances += 1;
    return {
      instances,
      loading: true
    };
  }
  instances -= instances > 0 ? 1 : 0;
  return {
    instances,
    loading: instances > 0
  };
};

export default loaderReducer;
