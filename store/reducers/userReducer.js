import findIndex from "lodash/findIndex";
import cloneDeep from "lodash/cloneDeep";
import uniqBy from "lodash/uniqBy";
import { EXTERNAL_AUTH } from "../actions/externalAuth/types";
import {
  ALTER_USER_FIELDS,
  BNET_VERIFIED,
  CU_ORGANIZATIONS,
  FACEBOOK_PAGES,
  FILTER_USERS,
  GET_ALIASED_NAME_SUCCESS,
  GET_CURRENT_USER,
  GET_MEDIA_SETTINGS,
  GET_NOTIFICATION_SETTINGS,
  GET_POSTABLE_ORGANIZATIONS,
  REMOVE_USER_GAME_PLATFORM,
  REQUEST_VERIFICATION,
  RESET_USER,
  SCHEDULED_POSTS,
  SET_PAYOUT_ACCOUNT,
  SET_RECOMMENDATIONS_USERS,
  SET_SNAPCHAT_VERIFIED,
  SET_TWITCH_CLIPS,
  SET_USER_GAME_PLATFORM,
  SET_USER_PAYMENT_METHODS,
  SET_USER_SETUP_INTENT,
  TOGGLE_ACTION_PROGRESS,
  UNLINK_ACCOUNT_SUCCESS,
  UPDATE_ACCOUNT_STATS,
  UPDATE_NOTIFICATION_SETTINGS,
  UPDATE_PUBLIC_TRAITS_MOTIVATIONS,
  UPDATE_USER,
  USER_LIVE_INFO,
  XBOX_LIVE_AUTH
} from "../actions/types";

const initialState = {
  organizations: [],
  users: [],
  currentUser: {
    riot: false,
    statespace: false
  },
  pagination: {},
  filteredUsers: [],
  userLiveInfo: {},
  inProgress: false,
  scheduledPosts: [],
  postableOrganizations: [],
  postableOrgPagination: {},
  recommendedUsers: null,
  paymentMethods: null,
  setupIntent: null,
  payoutAccount: null,
  notificationSettings: null,
  twitchClips: [],
  mediaSettings: null
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case FACEBOOK_PAGES:
      return {
        ...state,
        facebookPages: action.payload
      };
    case CU_ORGANIZATIONS:
      return {
        ...state,
        organizations: action.payload
      };
    case GET_CURRENT_USER: {
      let currentUser = action.payload;

      // sometimes we get back id sometimes we get back _id.
      // let's make them both the same
      if (currentUser.id) {
        currentUser._id = currentUser.id;
      } else if (currentUser._id) {
        currentUser.id = currentUser._id;
      }

      // if the old user id is null, the new and old are the same user, or the new doesn't have an id join them
      if (!state.currentUser?._id || !action.payload?._id || state.currentUser._id === action.payload._id) {
        currentUser = { ...state.currentUser, ...action.payload };
      }

      return {
        ...state,
        currentUser
      };
    }
    case FILTER_USERS:
      return {
        ...state,
        filteredUsers: action.payload
      };
    case USER_LIVE_INFO:
      return {
        ...state,
        userLiveInfo: { ...state.userLiveInfo, ...action.payload }
      };
    case REQUEST_VERIFICATION:
      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          verifyAccountRequest: true
        }
      };
    case UPDATE_USER:
      return {
        ...state,
        users: updateUserStatus(state, action.payload)
      };
    case BNET_VERIFIED:
      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          bnetVerified: true
        }
      };
    case RESET_USER:
      return {
        ...state,
        currentUser: {}
      };
    case TOGGLE_ACTION_PROGRESS:
      return {
        ...state,
        inProgress: action.inProgress
      };
    case GET_POSTABLE_ORGANIZATIONS:
      return {
        ...state,
        postableOrganizations: uniqBy([...state.postableOrganizations, ...action.organizations], "_id"),
        postableOrgPagination: action.pagination
      };
    case SCHEDULED_POSTS:
      return {
        ...state,
        scheduledPosts: action.payload
      };
    case SET_RECOMMENDATIONS_USERS:
      return {
        ...state,
        recommendedUsers: action.payload
      };
    case SET_USER_PAYMENT_METHODS:
      return {
        ...state,
        paymentMethods: action.payload.data
      };
    case SET_USER_SETUP_INTENT:
      return {
        ...state,
        setupIntent: action.payload.client_secret
      };
    case SET_PAYOUT_ACCOUNT:
      return {
        ...state,
        payoutAccount: action.payload
      };
    case SET_USER_GAME_PLATFORM:
      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          gamePlatforms: [...state.currentUser.gamePlatforms, action.payload]
        }
      };
    case REMOVE_USER_GAME_PLATFORM:
      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          gamePlatforms: [...action.payload]
        }
      };
    case GET_NOTIFICATION_SETTINGS:
      return {
        ...state,
        notificationSettings: action.payload
      };
    case UPDATE_NOTIFICATION_SETTINGS:
      return {
        ...state,
        notificationSettings: action.newNotificationSettings
      };
    case SET_TWITCH_CLIPS:
      return {
        ...state,
        twitchClips: action.payload
      };
    case XBOX_LIVE_AUTH:
      return {
        ...state,
        currentUser: action.user
      };
    case SET_SNAPCHAT_VERIFIED:
      return {
        ...state,
        currentUser: { ...state.currentUser, snapchatVerified: true }
      };
    case UPDATE_PUBLIC_TRAITS_MOTIVATIONS:
      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          publicTraitsMotivations: action.payload
        }
      };
    case GET_ALIASED_NAME_SUCCESS:
      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          name: action.name
        },
        isAliasing: false
      };
    case GET_MEDIA_SETTINGS:
      return {
        ...state,
        mediaSettings: action.mediaSettings
      };
    case UPDATE_ACCOUNT_STATS:
      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          [action.payload.accountName]: action.payload.stats
        }
      };
    case UNLINK_ACCOUNT_SUCCESS:
      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          [action.payload]: updateStats(action.payload)
        }
      };
    case ALTER_USER_FIELDS:
      return {
        ...state,
        currentUser: { ...state.currentUser, ...action.payload }
      };
    case EXTERNAL_AUTH.STATUS.PORTFOLIO_USER.GET.SUCCESS:
      return {
        ...state,
        currentUser: { ...state.currentUser, [action.payload.service]: action.payload.isAuthenticated }
      };
    case EXTERNAL_AUTH.LINK.DELETE.SUCCESS:
      return {
        ...state,
        currentUser: { ...state.currentUser, [action.payload.service]: false }
      };
    default:
      return state;
  }
};

const updateStats = game => {
  if (game === "fortnite") {
    return {
      epicNickname: "",
      accountId: "",
      stats: [],
      platform: null
    };
  }
  return {};
};

const updateUserStatus = (state, action) => {
  const clonedUsers = cloneDeep(state.users);
  const updatedUserId = action.updatedUser._id;
  const index = findIndex(state.users, { _id: updatedUserId });
  clonedUsers[index] = action.updatedUser;
  return clonedUsers;
};

export default userReducer;
