import { UPDATE_PUBLIC_TRAITS_MOTIVATIONS } from "../actions/types";
import userReducer from "./userReducer";

describe("userReducer", () => {
  it("updates public traits and motivations", () => {
    const prevState = {
      currentUser: {
        publicTraitsMotivations: false
      }
    };
    const action = {
      type: UPDATE_PUBLIC_TRAITS_MOTIVATIONS,
      payload: true
    };
    const nextState = userReducer(prevState, action);

    expect(nextState.currentUser.publicTraitsMotivations).toEqual(true);
  });
});
