import { combineReducers } from "redux";
import applicantReducer from "./applicantReducer";
import authReducer from "./authReducer";
import flagSmithReducer from "./flagSmithReducer";
import erenaReducer from "./erenaReducer";
import erenaReducerV2 from "./erenaV2/erenaReducerV2";
import errorReducer from "./errorReducer";
import experiencesReducer from "./experiencesReducer";
import followerReducer from "./followerReducer";
import gameRequirementReducer from "./gameRequirementReducer";
import gamesReducer from "./gamesReducer";
import globalVideoReducer from "./globalVideoReducer";
import headerReducer from "./headerReducer";
import hitmarkerReducer from "./hitmarkerReducer";
import leaderboardReducer from "./leaderboardReducer";
import newsReducer from "./newsReducer";
import livestreamReducer from "./livestreamReducer";
import loadingReducer from "./loaderReducer";
import mediaReducer from "./mediaReducer";
import messageReducer from "./messageReducer";
import notificationReducer from "./notificationReducer";
import notificationsReducer from "./notificationsReducer";
import oAuthReducer from "./oauthReducer";
import onboardingReducer from "./onboardingReducer";
import opportunitiesReducer from "./opportunitiesReducer";
import opportunitiesv2Reducer from "./opportunitiesv2/opportunitiesv2Reducer";
import orderReducer from "./orderReducer";
import organizationsReducer from "./organizationsReducer";
import portfolioReducer from "./portfolioReducer";
import recommendationsReducer from "./recommendationsReducer";
import recruitingReducer from "./recruitingReducer";
import settingsReducer from "./settingsReducer";
import shareReducer from "./shareReducer";
import themeReducer from "./themeReducer";
import userReducer from "./userReducer";
import validationReducer from "./validationReducer";
import webviewReducer from "./webviewReducer";
import socialLinkingReducer from "./socialLinking/socialLinkingReducer";
import feedReducer from "./feedReducer";
import teamReducer from "./teamReducer";

export default combineReducers({
  auth: authReducer,
  onboarding: onboardingReducer,
  loader: loadingReducer,
  errors: errorReducer,
  gameRequirements: gameRequirementReducer,
  opportunities: opportunitiesReducer,
  organizations: organizationsReducer,
  user: userReducer,
  settings: settingsReducer,
  followers: followerReducer,
  applicants: applicantReducer,
  messages: messageReducer,
  news: newsReducer,
  notification: notificationReducer,
  media: mediaReducer,
  notifications: notificationsReducer,
  features: flagSmithReducer,
  validation: validationReducer,
  portfolio: portfolioReducer,
  recommendations: recommendationsReducer,
  feed: feedReducer,
  hitmarker: hitmarkerReducer,
  webview: webviewReducer,
  oauth: oAuthReducer,
  share: shareReducer,
  orders: orderReducer,
  experiences: experiencesReducer,
  stream: livestreamReducer,
  erena: erenaReducer,
  erenaV2: erenaReducerV2,
  recruiting: recruitingReducer,
  globalVideo: globalVideoReducer,
  leaderboard: leaderboardReducer,
  theme: themeReducer,
  header: headerReducer,
  opportunitiesv2: opportunitiesv2Reducer,
  games: gamesReducer,
  socialLinking: socialLinkingReducer,
  teams: teamReducer
});
