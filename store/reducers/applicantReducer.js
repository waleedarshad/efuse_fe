import cloneDeep from "lodash/cloneDeep";
import uniqBy from "lodash/uniqBy";

import {
  GET_APPLICANTS,
  UPDATE_APPLICANT_FILTER_VALUE,
  ADD_APPLICANT_FILTER,
  UPDATE_APPLICANTS,
  CLEAR_APPLICANT_FILTERS,
  UPDATE_APPLICATION_STATUS,
  CLEAR_APPLICANTS,
  CHECK_USER_OPPORTUNITY_REQUIREMENTS,
  GET_APPLICANTS_SUCCESS,
  GET_APPLICANTS_REQUEST,
  GET_APPLICANTS_FAILURE,
  MARK_APPLICANT_FAVORITE,
  REMOVE_APPLICANT_FAVORITE
} from "../actions/types";

const initialState = {
  applicants: [],
  pagination: {},
  filtersApplied: [],
  searchObject: {},
  userRequirements: {},
  searchFilters: {
    "email.$regex": "",
    "address.$regex": "",
    "roles.$in": []
  },
  isGettingFirstApplicant: false
};

const applicantReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_APPLICANTS:
      return {
        ...state,
        applicants: uniqBy([...state.applicants, ...action.applicants], "_id"),
        pagination: action.pagination
      };
    case GET_APPLICANTS_REQUEST:
      return {
        ...state,
        isGettingFirstApplicant: action.payload
      };
    case GET_APPLICANTS_SUCCESS:
      return {
        ...state,
        applicants: action.page === 1 ? action.payload : state.applicants.concat(action.payload),
        pagination: action.pagination,
        isGettingFirstApplicant: false
      };
    case GET_APPLICANTS_FAILURE:
      return {
        ...state,
        isGettingFirstApplicant: false
      };
    case ADD_APPLICANT_FILTER:
      return {
        ...state,
        filtersApplied: action.payload
      };
    case UPDATE_APPLICANT_FILTER_VALUE:
      return {
        ...state,
        searchFilters: {
          ...state.searchFilters,
          [action.key]: action.value
        }
      };
    case UPDATE_APPLICANTS:
      return {
        ...state,
        applicants: action.payload,
        pagination: action.pagination,
        searchObject: action.searchObject
      };
    case UPDATE_APPLICATION_STATUS:
      return {
        ...state,
        applicants: updateStatus(action, state.applicants)
      };
    case CLEAR_APPLICANT_FILTERS:
      return {
        ...state,
        searchFilters: {
          "email.$regex": "",
          "address.$regex": "",
          "roles.$in": []
        },
        filtersApplied: [],
        searchObject: {}
      };
    case CLEAR_APPLICANTS:
      return {
        ...state,
        applicants: [],
        pagination: {}
      };
    case CHECK_USER_OPPORTUNITY_REQUIREMENTS:
      return {
        ...state,
        userRequirements: {
          ...state.userRequirements,
          [action.payload.opportunityId]: action.payload.data
        }
      };
    case MARK_APPLICANT_FAVORITE:
      return {
        ...state,
        applicants: state.applicants.map(applicant => {
          let updatedApplicant = { ...applicant };
          if (applicant._id === action.recruitId) {
            updatedApplicant.favorite = true;
          }
          return updatedApplicant;
        })
      };
    case REMOVE_APPLICANT_FAVORITE:
      return {
        ...state,
        applicants: state.applicants.map(applicant => {
          let updatedApplicant = { ...applicant };
          if (applicant._id === action.recruitId) {
            updatedApplicant.favorite = false;
          }
          return updatedApplicant;
        })
      };
    default:
      return {
        ...state
      };
  }
};

const updateStatus = (action, applicants) => {
  const { userId, status, filterType } = action;
  let applicantsData = cloneDeep(applicants);
  if (filterType === "all") {
    applicantsData.filter(applicant => {
      if (applicant._id === userId) {
        applicant.applicant.status = status;
        return true;
      }
    });
  } else {
    applicantsData = applicantsData.filter(applicant => applicant._id !== userId);
  }
  return applicantsData;
};

export default applicantReducer;
