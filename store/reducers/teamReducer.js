import { GET_TEAMS_BY_OWNER_BY_ID } from "../actions/types";

const initialState = {
  teams: null
};

const teamReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_TEAMS_BY_OWNER_BY_ID:
      return {
        ...state,
        teams: action.payload
      };

    default:
      return {
        ...state
      };
  }
};

export default teamReducer;
